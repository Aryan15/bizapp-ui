import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-alerts';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActionPanelComponent, DisclaimerBottomSheet,  } from '../action-panel/action-panel.component';
import { SecuredImageComponent } from '../secured-image/secured-image.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OnlyNumber } from '../onlynumber.directive';
import { MaterialModule } from '../module/material.module';

import { NgxChartsModule } from 'ngx-charts-8';
import { SubHeadingComponent } from '../sub-heading/sub-heading.component';
import { HoldableDirective } from '../utils/holdable.directive';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { SecurePipe } from '../pipes/secure.pipe';
import { HelpVideosComponent } from '../help-videos/help-videos.component';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';

//import { HttpClient } from 'selenium-webdriver/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
//import { HttpLoaderFactory } from '../app.module';


export function HttpLoaderFactory(http: HttpClient) {
   return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
   // MasterSubHeadingComponent,
    AlertModule.forRoot({maxMessages: 5, timeout: 5000}),
    NgxDatatableModule,
    FlexLayoutModule,
    MaterialModule,

     TranslateModule.forRoot({
      
     loader: {
    provide: TranslateLoader,
   useFactory: HttpLoaderFactory,
     deps: [HttpClient]
   
     },
 
      
    }),

    NgxChartsModule,
    NgxExtendedPdfViewerModule,
  ],
  declarations: [
    // MasterSubHeadingComponent,
    SubHeadingComponent,
    ActionPanelComponent,
    SecuredImageComponent,
    OnlyNumber,
    HoldableDirective,
    DisclaimerBottomSheet,
    HelpVideosComponent,
    
    // SecurePipe
  ],
  exports:[
    // MasterSubHeadingComponent,
    SubHeadingComponent,
    ActionPanelComponent,
    SecuredImageComponent,
    OnlyNumber,
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    AlertModule,
    NgxDatatableModule,
    FlexLayoutModule,
    MaterialModule,
   TranslateModule,
    NgxChartsModule,
    HoldableDirective,
    DisclaimerBottomSheet,
    NgxExtendedPdfViewerModule,
    HelpVideosComponent,
    
   
  ],
  entryComponents: [DisclaimerBottomSheet]
})
export class SharedModule { }
// export function HttpLoaderFactory(http: HttpClient) {
//    return new TranslateHttpLoader(http);

// }