


export var single = [
    {
      "name": "Tool Material",
      "value": 8940000
    },
    {
      "name": "Test material",
      "value": 5000000
    },
    {
      "name": "Testing material2",
      "value": 7200000
    }
  ];
  
  export var multi = [
    {
      "name": "Tool Material",
      "series": [
        {
          "name": "2017-2018",
          "value": 7300000
        },
        {
          "name": "2016-2017",
          "value": 7300000
        },
        {
          "name": "2015-2016",
          "value": 8940000
        }
      ]
    },
  
    {
      "name": "Test Material",
      "series": [
        {
          "name": "2010",
          "value": 7870000
        },
        {
          "name": "2011",
          "value": 8270000
        }
      ]
    },
  
    {
      "name": "Testing Material",
      "series": [
        {
          "name": "2010",
          "value": 5000002
        },
        {
          "name": "2011",
          "value": 5800000
        }
      ]
    }
  ];
  