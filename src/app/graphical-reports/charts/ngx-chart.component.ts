import { Component, OnInit, Input } from '@angular/core';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { single } from './data';
// import { multi } from './data';
import { Material } from '../../data-model/material-model';
import { InvoiceHeader, InvoiceItem } from '../../data-model/invoice-model';
import { InvoiceService } from '../../services/invoice.service';
@Component({
    selector: 'app-ngx-chart',
    templateUrl: './ngx-chart.component.html',
    styleUrls: ['./ngx-chart.component.scss']
})
export class NgxChartComponent implements OnInit {

    invoice: InvoiceHeader[];

    // single: any[] = [];
    // multi: any[] = [];
    //sales: any[] = [];
    data: any[] = [];
    view: any[] = [700, 400];
    material: Material;
    // @Input() animations: boolean = true;
    ngOnInit() {
        this.getAllInvoices();
        //console.log("this.invoice", this.invoice)
    }
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    //animations = true;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Month';
    showYAxisLabel = true;
    yAxisLabel = 'Sales';
    animations = true;

    colorScheme = {
        domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };

    constructor(
        private invoiceService: InvoiceService, ) {
        // this.invoice = invoice;
        Object.assign(this, this.single)
        Object.assign(this, this.single, this.multi)
        this.data = this.single;
        //console.log("this.invoice", this.invoice)

        // Object.assign(this, { single })
        // Object.assign(this, { single, multi })
        // this.data = single;
        // //console.log("this.invoice", this.invoice)
        // this.invoiceService.getAllMaterials().subscribe(data => this.data = data);
        //   this.getdata = spservice.getData();
        //     this.invoiceService.getAllInvoices()
        //         .subscribe(data => data.forEach((item, i, data) =>
        //          { this.invoice.push({ name: (item.invoiceNumber), value: (item.grandTotal) }) }));
        //     this.invoice.map(data => //console.log(data));
        //    Object.assign(this,  this.invoice );
        //    Object.assign(this, this.invoice, multi)
        this.data = this.invoice;
    }
    getAllInvoices() {
        this.invoiceService.getAllInvoices().subscribe(response => {
            this.invoice = response;
            //console.log("respos", response)
        });
    }
    onSelect(event) {
        //console.log(event);
    }

    // pie
    showLabels = true;
    explodeSlices = false;
    doughnut = false;

    // line, area
    autoScale = true;


    sales = [
        {
            "name": "Jan",
            "value": 3000000
        },
        {
            "name": "Feb",
            "value": 3500000
        },
        {
            "name": "Mar",
            "value": 2500000
        },
        {
            "name": "Apr",
            "value": 1500000
        }

    ]
    single =
        // {
        // return 
        [
            {
                "name": "Tool Material",
                "value": 7870000
                //    "value": this.invoice.
            },
            {
                "name": "Test material",
                "value": 7870000
                //  "value": this.invoice.grandTotal
            },
            {
                "name": "Testing material2",
                "value": 7870000
                // "value": this.invoice.grandTotal
            }
        ];

    // }
    multi =
        [
            {
                "name": "Tool Material",
                "series": [
                    {
                        "name": "2017-2018",
                        "value": 7870000
                        // "value": this.invoice.grandTotal
                    },
                    {
                        "name": "2016-2017",
                        "value": 7870000
                        // "value": this.invoice.grandTotal
                    },
                    {
                        "name": "2015-2016",
                        "value": 7870000
                        // "value": this.invoice.grandTotal
                    }
                ]
            },

            {
                "name": "Test Material",
                "series": [
                    {
                        "name": "2010",
                        "value": 7870000
                    },
                    {
                        "name": "2011",
                        "value": 8270000
                    }
                ]
            },

            {
                "name": "Testing Material",
                "series": [
                    {
                        "name": "2010",
                        "value": 5000002
                    },
                    {
                        "name": "2011",
                        "value": 5800000
                    }
                ]
            }
        ];

}

