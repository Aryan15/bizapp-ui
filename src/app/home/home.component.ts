import { Component, OnInit } from '@angular/core';
import { Activity, Menu } from '../data-model/activity-model';
import { UserModel, RoleModel } from '../data-model/user-model';
import { UserService } from '../services/user.service';
import { AppSettings } from '../app.settings';
import { DashboardReportService } from '../services/dashboard-report.service';
import { DashboardTransactionSummary } from '../data-model/dashboard-transaction-summary-model';
import { AuthService } from '../auth/auth.service';
import { Subject, Observable, combineLatest, forkJoin, zip } from 'rxjs';
import { take } from 'rxjs/operators';
import { CombineLatestOperator } from 'rxjs/internal/observable/combineLatest';
import { FinancialYearService } from '../services/financial-year.service';
import { UtilityService } from '../utils/utility-service';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    salesActivities: Activity[] = [];
    purchaseActivities: Activity[] = [];
    public menus: Menu[];
    currentUser: UserModel = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    transactionComplete: Subject<boolean> = new Subject();
    salesPayemetsComplete: Subject<boolean> = new Subject();
    jobworkPayemetsComplete: Subject<boolean> = new Subject();
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Month';
    showYAxisLabel: boolean = true;
    yAxisLabel: string = 'Sales';
    showLegend: boolean = true;
    allowJwInvoice: boolean = false;
    financialYearDate:string="";
    isActive:number=0;
    totalTransaction: DashboardTransactionSummary[] = [];
    salesArray: DashboardTransactionSummary[] = [];
    purchaseArray: DashboardTransactionSummary[] = [];
    jobworkArray: DashboardTransactionSummary[] = [];
    subcontractArray: DashboardTransactionSummary[] = [];
    totalReciveble: DashboardTransactionSummary[] = [];
    jobworkReceivableArray: DashboardTransactionSummary[] = [];
    subcontratPayableArray: DashboardTransactionSummary[] = [];
    receivableArray: DashboardTransactionSummary[] = [];
    payableArray: DashboardTransactionSummary[] = [];
    totalPurchase: DashboardTransactionSummary[] = [];
    finaltotalPurchase: DashboardTransactionSummary[] = [];
    finaltotalSales: DashboardTransactionSummary[] = [];
    finaltotalIncome: DashboardTransactionSummary[] = [];
    finaltotalOutgoing: DashboardTransactionSummary[] = [];
    totalPayements: DashboardTransactionSummary[] = [];
    datePipe = new DatePipe('en-US');
    defaultDate: string;
    public periodOfDashBoard: dashboardData[] = [
        { id: 2, name: "3 MONTHS" },
        { id: 5, name: "6 Months" },
       { id: 3, name: "Financial Year" },
        
      ]

    quotationBarChart: any = [
        // {
        //     "name": "Jan",
        //     "value": 3000000
        // },
        // {
        //     "name": "Feb",
        //     "value": 3500000
        // },
        // {
        //     "name": "Mar",
        //     "value": 2500000
        // },
        // {
        //     "name": "Apr",
        //     "value": 1500000
        // }
    ];
    poBarChart: any = []

    purchaseGroupedBarChart: any = [];

    groupedBarChart: any = [
        // {
        //     name: "March",
        //     series: [
        //         {
        //             name: "Customer Receipts",
        //             value: 1000
        //         },
        //         {
        //             name: "Customer Invoice",
        //             value: 1500
        //         },
        //     ]

        // },
        // {
        //     name: "April",
        //     series: [
        //         {
        //             name: "Customer Receipts",
        //             value: 2000
        //         },
        //         {
        //             name: "Customer Invoice",
        //             value: 5000
        //         },
        //     ]

        // },
        // {
        //     name: "May",
        //     series: [
        //         {
        //             name: "Customer Receipts",
        //             value: 7000
        //         },
        //         {
        //             name: "Customer Invoice",
        //             value: 9000
        //         },
        //     ]

        // }
    ];
    jobworkGroupedBarChat: any = [];
    subcontractGroupedBarChat: any = [];
    totalGroupedBarChat: any = [];
    totalPurchaseGroupedBarChat: any = [];
    lineChart: any = [
        //     {
        //     name: "Customer Receipts",
        //     series: [
        //         {
        //             "name": "March",
        //             "value": 1000
        //         },
        //         {
        //             "name": "April",
        //             "value": 15000
        //         },
        //         {
        //             "name": "May",
        //             "value": 20000
        //         }
        //     ]
        // }
    ];

    purchaseLineChart: any = [];
    jobworkLineChart: any = [];
    subcontratLineChart: any = [];
    totalLineChart: any = [];
    totalPurchaseLineChart: any = [];
    stackedBarChart: any = [
        {
            name: "March",
            series: [
                {
                    name: "Customer Receipts",
                    value: 10000,
                },
                {
                    name: "Customer Invoice",
                    value: 15000,
                }
            ]
        },
        {
            name: "April",
            series: [
                {
                    name: "Customer Receipts",
                    value: 12000,
                },
                {
                    name: "Customer Invoice",
                    value: 25000,
                }
            ]
        },
        {
            name: "March",
            series: [
                {
                    name: "Customer Receipts",
                    value: 10000,
                },
                {
                    name: "Customer Invoice",
                    value: 15000,
                }
            ]
        },
        {
            name: "April",
            series: [
                {
                    name: "Customer Receipts",
                    value: 12000,
                },
                {
                    name: "Customer Invoice",
                    value: 25000,
                }
            ]
        }
    ];
    // colorScheme = {
    //     domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    // };
    colorScheme = {
        domain: ['#673ab7', '#9ccc65', '#C7B42C', '#AAAAAA']
    };

    supplierColorScheme = {
        domain: ['#ef5350', '#64b5f6', '#C7B42C', '#AAAAAA']
    };

    view: any[] = [700, 400];
    customerInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.CUSTOMER_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };
    purchaseInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.PURCHASE_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };
    postDatedChequesCustomerSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: "Post Dated cheques received",
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };
    postDatedChequesSupplierSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: "Post Dated cheques issued",
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    openQuotationSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: "Open Quotations",
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    openPOSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: "Open Purchase Orders",
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    customerReceivableSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.CUSTOMER_RECEIVABLE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    jobowrkReceivableSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.CUSTOMER_RECEIVABLE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    supplierPaymentSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.SUPPLIER_PAYABLE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    subcontratPaymentSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.SUPPLIER_PAYABLE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };


    jobworkInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.INCOMING_JOBWORK_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };


    subcontractInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.OUTGOING_JOBWORK_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };

    salesInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.CUSTOMER_JOBWORK_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };


    totalReceivableSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.CUSTOMER_JOBWORK_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };




    totalPurchaseInvoiceSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.SUPPLIER_SUBCONTRACT_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };


    totalPayableSummary: DashboardTransactionSummary = {
        month: "",
        transactionName: AppSettings.SUPPLIER_SUBCONTRACT_INVOICE,
        transactionAmount: 0.0,
        transactionCount: 0,
        dueAmount: 0.0
    };






    selectedMonth: number =2; // to return past three months data keep this as 2

    months: number[] = [3, 4, 6];

    isAdmin: boolean = false;

  

    constructor(private userService: UserService
        , private dashboardReportService: DashboardReportService
        ,private financialYearService: FinancialYearService,
        private authService: AuthService) { }

    ngOnInit() {
        this.getFinancialYear();
        this.getTransactionType();
        this.userService.getMenusForUser(this.currentUser.username)
            .subscribe(menus => {
                this.menus = menus;
                //console.log("Got menus: ", this.menus.length);
                this.getSalesActivities()
                //console.log('called getSalesActivities()');
                this.getData();

                zip(this.transactionComplete, this.salesPayemetsComplete, this.jobworkPayemetsComplete)

                    .subscribe(response => {

                        if (response) {

                            this.totalIncomeTransaction();
                            this.totalOutgoingTransaction();
                        }

                    });










            });



        // let adminUserRole: RoleModel = this.currentUser.roles.find(rl => rl.roleType.name === AppSettings.ADMIN_ROLE_TYPE);
        // //console.log("userRole: ", adminUserRole);

        // if (adminUserRole) {
        //     this.isAdmin = true;
        // } else {
        //     this.isAdmin = false;
        // }

    }

    getData() {
        let isLocal: boolean = false;
        if (this.userService.homePageLoadCounter === 0) {
            isLocal = false;

        }
        else {
            isLocal = true;

        }
        this.isAdmin = this.authService.isDashboardFound();

        if (this.isAdmin) {


            this.getInvoiceWithPayments(isLocal);


            // this.getPostDatedCheques();
            this.getOpenQuotations(isLocal);
            this.getOpenPOs(isLocal);


            this.userService.homePageLoadCounter++;

        }
    }

    onRefresh() {
        this.lineChart = [];
        this.purchaseLineChart = [];
        this.jobworkLineChart = [];
        this.subcontratLineChart = [];
        this.quotationBarChart = [];
        this.poBarChart = [];

        this.totalLineChart = [];
        this.totalGroupedBarChat = [];

        this.totalPurchaseGroupedBarChat = [];
        this.totalPurchaseLineChart = [];
        this.totalTransaction = [];
        this.totalReciveble = [];
        this.totalPurchase = [];
        this.totalPayements = [];
        this.dashboardReportService.localInvoices = [];
        this.dashboardReportService.localPayReceives = [];
        this.dashboardReportService.localQuotations = [];
        this.dashboardReportService.localPOs = [];
        this.getInvoiceWithPayments(false);

        // this.getPostDatedCheques();
        this.getOpenQuotations(false);
        this.getOpenPOs(false);
        //this.getPayReceive(false);
        //this.getJWSCPayReceive(false);


    }

    onSelect(_event: any) {
        if (_event.value === 3) {
            if (this.isActive === 1) {
              let curDate: Date = new Date();
                let month = curDate.getMonth();
                let FDate: Date = new Date(this.financialYearDate);
                let Fmonth = FDate.getMonth();  
                let monthBetween = curDate.getMonth() - FDate.getMonth() + (12 * (curDate.getFullYear() - FDate.getFullYear()));
                this.selectedMonth = monthBetween;
                this.onRefresh();

            }

        }
        else {
            this.selectedMonth = _event.value;
            this.onRefresh();
        }
    }

    getPayReceive(isLocal: boolean) {
        if (isLocal) {
            let report = this.dashboardReportService.localPayReceives;
            this.receivableArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_RECEIVABLE);
            this.payableArray = report.filter(invRep => invRep.transactionName === AppSettings.SUPPLIER_PAYABLE);


            this.getLineChart(this.receivableArray, "SALES");
            this.getLineChart(this.payableArray, "BUY");

            if (this.receivableArray.length > 0) {
                let customerReceivableSummary = this.getSummaryRow(this.receivableArray);
                if (customerReceivableSummary) this.customerReceivableSummary = customerReceivableSummary;
            }

            if (this.payableArray.length > 0) {
                let supplierPaymentSummary = this.getSummaryRow(this.payableArray);
                if (supplierPaymentSummary) this.supplierPaymentSummary = supplierPaymentSummary;
            }


            let invoiceArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE);
            let purchaseArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.PURCHASE_INVOICE);

            this.getGroupedBarChart(invoiceArray.concat(this.receivableArray), "SALES");
            this.getGroupedBarChart(purchaseArray.concat(this.payableArray), "BUY");

        } else {
            this.dashboardReportService.getPayReceive(this.selectedMonth, "JW-SC")
                .subscribe(report => {

                    //console.log('got report : ', report);
                    if (report) {
                        this.receivableArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_RECEIVABLE);
                        this.payableArray = report.filter(invRep => invRep.transactionName === AppSettings.SUPPLIER_PAYABLE);


                        this.getLineChart(this.receivableArray, "SALES");
                        this.getLineChart(this.payableArray, "BUY");


                        if (this.receivableArray.length > 0) {
                            let customerReceivableSummary = this.getSummaryRow(this.receivableArray);
                            if (customerReceivableSummary) this.customerReceivableSummary = customerReceivableSummary;
                        }

                        if (this.payableArray.length > 0) {
                            let supplierPaymentSummary = this.getSummaryRow(this.payableArray);
                            if (supplierPaymentSummary) this.supplierPaymentSummary = supplierPaymentSummary;
                        }



                        this.dashboardReportService.localPayReceives = report;
                        // if(!this.customerInvoiceSummary){
                        //     this.customerInvoiceSummary = {

                        //     }
                        // }

                        let invoiceArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE);
                        let purchaseArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.PURCHASE_INVOICE);
                        invoiceArray.forEach(r => {



                        })
                        this.getGroupedBarChart(invoiceArray.concat(this.receivableArray), "SALES");
                        this.getGroupedBarChart(purchaseArray.concat(this.payableArray), "BUY");

                        this.salesPayemetsComplete.next(true);
                    }


                })
        }

    }

    getJWSCPayReceive(isLocal: boolean) {
        if (isLocal) {
            let report = this.dashboardReportService.localPayReceives;
            this.jobworkReceivableArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_RECEIVABLE);
            this.subcontratPayableArray = report.filter(invRep => invRep.transactionName === AppSettings.SUPPLIER_PAYABLE);
            this.getLineChart(this.jobworkReceivableArray, "JOBWORK");
            this.getLineChart(this.subcontratPayableArray, "SUBCONTRACT");

            if (this.jobworkReceivableArray.length > 0) {
                let jobworkReceivableSummary = this.getSummaryRow(this.jobworkReceivableArray);
                if (jobworkReceivableSummary) this.jobowrkReceivableSummary = jobworkReceivableSummary;
            }

            if (this.subcontratPayableArray.length > 0) {
                let subcontratPaymentSummary = this.getSummaryRow(this.subcontratPayableArray);
                if (subcontratPaymentSummary) this.subcontratPaymentSummary = subcontratPaymentSummary;
            }

            let jobworkArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.INCOMING_JOBWORK_INVOICE);
            let subcontractArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.OUTGOING_JOBWORK_INVOICE);
            this.getGroupedBarChart(jobworkArray.concat(this.jobworkReceivableArray), "JOBWORK");
            this.getGroupedBarChart(subcontractArray.concat(this.subcontratPayableArray), "SUBCONTRAT");

        }
        else {



            this.dashboardReportService.getPayReceive(this.selectedMonth, "BUY-SELL")
                .subscribe(report => {
                    // console.log('got report : ', report);

                    if (report) {
                        this.jobworkReceivableArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_RECEIVABLE);
                        this.subcontratPayableArray = report.filter(invRep => invRep.transactionName === AppSettings.SUPPLIER_PAYABLE);

                        this.getLineChart(this.jobworkReceivableArray, "JOBWORK");
                        this.getLineChart(this.subcontratPayableArray, "SUBCONTRACT");

                        if (this.jobworkReceivableArray.length > 0) {
                            let jobworkReceivableSummary = this.getSummaryRow(this.jobworkReceivableArray);
                            if (jobworkReceivableSummary) this.jobowrkReceivableSummary = jobworkReceivableSummary;
                        }

                        if (this.subcontratPayableArray.length > 0) {
                            let subcontratPaymentSummary = this.getSummaryRow(this.subcontratPayableArray);
                            if (subcontratPaymentSummary) this.subcontratPaymentSummary = subcontratPaymentSummary;
                        }
                        this.dashboardReportService.localPayReceives = report;
                        let jobworkArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.INCOMING_JOBWORK_INVOICE);
                        let subcontractArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.OUTGOING_JOBWORK_INVOICE);

                        this.getGroupedBarChart(jobworkArray.concat(this.jobworkReceivableArray), "JOBWORK");
                        this.getGroupedBarChart(subcontractArray.concat(this.subcontratPayableArray), "SUBCONTRACT");

                        this.jobworkPayemetsComplete.next(true);
                    }

                });

        }
    }

    getSummaryRow(rows: DashboardTransactionSummary[]): DashboardTransactionSummary {

        if (rows.length > 0) {

            let lrows: DashboardTransactionSummary[] = [...rows];


            let retValue: DashboardTransactionSummary = <DashboardTransactionSummary>{};

            retValue.transactionName = lrows.length > 0 ? lrows[0].transactionName : null;
            retValue.transactionCount = lrows.map(r => r.transactionCount).reduce((acc, value) => acc + value, 0);

            lrows.forEach(r => {

            })
            retValue.transactionAmount = lrows.map(r => r.transactionAmount).reduce((acc, value) => acc + value, 0);




            retValue.dueAmount = lrows.map(r => r.dueAmount).reduce((acc, value) => acc + value, 0);
            //console.log("retValue.dueAmount: " + retValue.dueAmount);

            return retValue;
        } else {
            return null;
        }

    }

    getBarChart(rows: DashboardTransactionSummary[], type: string) {
        if (rows.length > 0) {
            rows.forEach(r => {
                let barChartObject: any = {};

                barChartObject.name = r.month;
                barChartObject.value = r.transactionAmount;

                if (type === "QUOTATION") {
                    this.quotationBarChart.push(barChartObject)
                } else {
                    this.poBarChart.push(barChartObject);
                }

            });
        } else {
            //console.log("no rows");
        }

        // //console.log("barChart: ", this.barChart);
    }

    getGroupedBarChart(rows: DashboardTransactionSummary[], type: string) {

        if (rows.length > 0) {
            let barChartObjectSeries: any[] = [];

            let months: string[] = rows.map(r => r.month);
            months = Array.from(new Set(months));
            let transactionCount: number = 0;

            months
                .forEach(month => {
                    let barChartObject: any = {};
                    barChartObject.name = month;
                    barChartObject.series = [];
                    barChartObjectSeries.push(barChartObject);
                });



            rows.forEach(row => {

                let barChartSeriesObject: any = {};
                barChartSeriesObject.name = row.transactionName;
                barChartSeriesObject.value = row.transactionAmount;
                barChartObjectSeries.find(os => os.name === row.month).series.push(barChartSeriesObject);

            });



            if (type === "SALES") {
                this.groupedBarChart = barChartObjectSeries;
            }
            else if (type === "BUY") {
                this.purchaseGroupedBarChart = barChartObjectSeries;
            }

            else if (type === "JOBWORK") {
                this.jobworkGroupedBarChat = barChartObjectSeries;

            }

            else if (type === "SUBCONTRACT") {
                this.subcontractGroupedBarChat = barChartObjectSeries;

            }

            else if (type == "Customer and JobWok Invoice") {
                this.totalGroupedBarChat = barChartObjectSeries;
            }


            else if (type == "Supplier and Subcontract Invoice") {
                this.totalPurchaseGroupedBarChat = barChartObjectSeries;
            }


        } else {
            //console.log("no rows");
        }
    }

    getLineChart(rows: DashboardTransactionSummary[], type: string) {

        if (rows.length > 0) {
            let lineChartObject: any = {};
            let lineChartSeries: any[] = [];
            rows.forEach(r => {


                let lineChartSeriesObject: any = {};
                lineChartSeriesObject.name = r.month;
                lineChartSeriesObject.value = r.transactionAmount;
                lineChartSeries.push(lineChartSeriesObject);

            });

            lineChartObject.name = rows.length > 0 ? rows[0].transactionName : null;
            lineChartObject.series = lineChartSeries;

            if (type === "SALES") {
                this.lineChart.push(lineChartObject);
            } else if (type === "BUY") {
                this.purchaseLineChart.push(lineChartObject);
            }

            else if (type === "JOBWORK") {
                this.jobworkLineChart.push(lineChartObject);
            }

            else if (type === "Customer and JobWok Invoice") {
                this.totalLineChart.push(lineChartObject)
            }

            else if (type === "Supplier and Subcontract Invoice") {
                this.totalPurchaseLineChart.push(lineChartObject)
            }

            else {
                this.subcontratLineChart.push(lineChartObject);
            }


        } else {

        }
    }

    // getStackedBarChart(rows: DashboardTransactionSummary[]){

    //     let barChartObject: any = {
    //         name: null,
    //         series: []
    //     };
    //     let barChartSeries: any[] = [];
    //     rows.forEach(r => {

    //         let seriesObject: any = {};
    //         barChartObject.name = r.month;
    //         seriesObject.name = r.transactionName;
    //         seriesObject.value = r.transactionAmount;
    //         barChartObject.series.push(seriesObject);


    //     });


    //     this.stackedBarChart.push(barChartObject);
    //     //console.log("barChart: ", this.barChart);
    // }

    getInvoiceWithPayments(isLocal: boolean) {
        if (isLocal) {
            let report = this.dashboardReportService.localInvoices;
            this.salesArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE);
            this.purchaseArray = report.filter(invRep => invRep.transactionName === AppSettings.PURCHASE_INVOICE);
            this.jobworkArray = report.filter(invRep => invRep.transactionName === AppSettings.INCOMING_JOBWORK_INVOICE);
            this.subcontractArray = report.filter(invRep => invRep.transactionName === AppSettings.OUTGOING_JOBWORK_INVOICE);
            // this.getBarChart(report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE));
            this.getLineChart(this.salesArray, "SALES");
            this.getLineChart(this.purchaseArray, "BUY");
            this.getLineChart(this.jobworkArray, "JOBWORK");
            this.getLineChart(this.subcontractArray, "SUBCONTRACT");
            if (this.salesArray.length > 0) {
                let customerInvoiceSummary = this.getSummaryRow(this.salesArray);

                if (customerInvoiceSummary) this.customerInvoiceSummary = customerInvoiceSummary;
            }

            if (this.purchaseArray.length > 0) {
                let purchaseInvoiceSummary = this.getSummaryRow(this.purchaseArray);
                if (purchaseInvoiceSummary) this.purchaseInvoiceSummary = purchaseInvoiceSummary;
            }

            if (this.jobworkArray.length > 0) {
                let jobworkInvoiceSummary = this.getSummaryRow(this.jobworkArray);
                if (jobworkInvoiceSummary) this.jobworkInvoiceSummary = jobworkInvoiceSummary;
            }

            if (this.subcontractArray.length > 0) {
                let subcontractInvoiceSummary = this.getSummaryRow(this.subcontractArray);
                if (subcontractInvoiceSummary) this.subcontractInvoiceSummary = subcontractInvoiceSummary;
            }
            this.getPayReceive(isLocal);
            this.getJWSCPayReceive(isLocal);

        } else {
            this.dashboardReportService.getInvoiceWithPayments(this.selectedMonth)
                .subscribe(report => {

                    if (report) {
                        this.salesArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE);
                        this.purchaseArray = report.filter(invRep => invRep.transactionName === AppSettings.PURCHASE_INVOICE);
                        this.jobworkArray = report.filter(invRep => invRep.transactionName === AppSettings.INCOMING_JOBWORK_INVOICE);
                        this.subcontractArray = report.filter(invRep => invRep.transactionName === AppSettings.OUTGOING_JOBWORK_INVOICE);



                        //this.getBarChart(report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_INVOICE));
                        this.getLineChart(this.salesArray, "SALES");
                        this.getLineChart(this.purchaseArray, "BUY");
                        this.getLineChart(this.jobworkArray, "JOBWORK");
                        this.getLineChart(this.subcontractArray, "SUBCONTRACT");
                        if (this.salesArray) {

                            let customerInvoiceSummary = this.getSummaryRow(this.salesArray);
                            if (customerInvoiceSummary) this.customerInvoiceSummary = customerInvoiceSummary;
                        }

                        if (this.purchaseArray) {
                            let purchaseInvoiceSummary = this.getSummaryRow(this.purchaseArray);
                            if (purchaseInvoiceSummary) this.purchaseInvoiceSummary = purchaseInvoiceSummary;
                        }

                        if (this.jobworkArray) {
                            let jobworkInvoiceSummary = this.getSummaryRow(this.jobworkArray);
                            if (jobworkInvoiceSummary) this.jobworkInvoiceSummary = jobworkInvoiceSummary;
                        }

                        if (this.subcontractArray) {
                            let subcontractInvoiceSummary = this.getSummaryRow(this.subcontractArray);
                            if (subcontractInvoiceSummary) this.subcontractInvoiceSummary = subcontractInvoiceSummary;
                        }
                        this.dashboardReportService.localInvoices = this.dashboardReportService.localInvoices.concat(report);

                        this.getPayReceive(isLocal);
                        this.getJWSCPayReceive(isLocal);

                        this.transactionComplete.next(true);


                    }

                });

        }

    }

    getOpenQuotations(isLocal: boolean) {

        if (isLocal) {
            let report = this.dashboardReportService.localQuotations;
            let quotationArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_QUOTATION);
            let openQuotationSummary = this.getSummaryRow(quotationArray);
            this.getBarChart(quotationArray, "QUOTATION");
            if (openQuotationSummary) this.openQuotationSummary = openQuotationSummary;
            this.openQuotationSummary.transactionName = "Open Quotations";

        } else {
            this.dashboardReportService.getOpenQuotations(this.selectedMonth)
                .subscribe(report => {
                    //console.log('got report : ', report);
                    if (report) {
                        // let openQuotationSummary = report.find(invRep => invRep.transactionName === AppSettings.CUSTOMER_QUOTATION);
                        let quotationArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_QUOTATION);
                        let openQuotationSummary = this.getSummaryRow(quotationArray);
                        this.getBarChart(quotationArray, "QUOTATION");
                        if (openQuotationSummary) this.openQuotationSummary = openQuotationSummary;
                        this.openQuotationSummary.transactionName = "Open Quotations";
                        this.dashboardReportService.localQuotations = report;
                    }
                })
        }
    }

    getOpenPOs(isLocal: boolean) {
        if (isLocal) {
            let report = this.dashboardReportService.localPOs;
            //console.log('got report : ', report);

            let poArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_PO);
            this.getBarChart(poArray, "PO");
            let openPOSummary = this.getSummaryRow(poArray);
            if (openPOSummary) this.openPOSummary = openPOSummary;
            this.openPOSummary.transactionName = "Open Purchase Orders";

        } else {
            this.dashboardReportService.getOpenPurchaseOrders(this.selectedMonth)
                .subscribe(report => {
                    //console.log('got report : ', report);
                    if (report) {
                        //let openPOSummary = report.find(invRep => invRep.transactionName === AppSettings.CUSTOMER_PO);
                        let poArray = report.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_PO);
                        this.getBarChart(poArray, "PO");
                        let openPOSummary = this.getSummaryRow(poArray);
                        if (openPOSummary) this.openPOSummary = openPOSummary;
                        this.openPOSummary.transactionName = "Open Purchase Orders";
                        this.dashboardReportService.localPOs = report;
                    }
                })
        }
    }

    // getPostDatedCheques(){
    //     this.dashboardReportService.getPostDatedCheques()
    //     .subscribe(report => {
    //         //console.log('got report : ', report);            
    //         let postDatedChequesCustomerSummary = report.find(cheq => cheq.transactionName === AppSettings.CUSTOMER_RECEIVABLE);
    //         if(postDatedChequesCustomerSummary) this.postDatedChequesCustomerSummary = postDatedChequesCustomerSummary;

    //         let postDatedChequesSupplierSummary = report.find(cheq => cheq.transactionName === AppSettings.SUPPLIER_PAYABLE);

    //         if(postDatedChequesSupplierSummary) this.postDatedChequesSupplierSummary = postDatedChequesSupplierSummary;

    //         this.postDatedChequesSupplierSummary.transactionName = "Post Dated cheques issued";

    //         this.postDatedChequesCustomerSummary.transactionName = "Post Dated cheques received";

    //     })
    // }

    getSalesActivities() {
        //let salesMenuId: number = this.menus.find(menu => menu.name === 'Sales').id;
        let salesMenu: Menu = this.menus.find(menu => menu.name === 'Sales');
        let purMenu: Menu = this.menus.find(menu => menu.name === 'Purchases');
        this.salesActivities = salesMenu ? salesMenu.activities : [];
        this.purchaseActivities = purMenu ? purMenu.activities : [];
    }





    changeMonth() {

        this.getInvoiceWithPayments(false);
    }

    totalIncomeTransaction() {
        this.totalTransaction = [];
      

        this.salesArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.CUSTOMER_JOBWORK_INVOICE,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalTransaction.push(item));


        this.jobworkArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.CUSTOMER_JOBWORK_INVOICE,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalTransaction.push(item));










        let totalInvoiceSummary = this.getSummaryRow(this.totalTransaction);
        if (totalInvoiceSummary) this.salesInvoiceSummary = totalInvoiceSummary;
        this.getLineChart(this.totalTransaction, "Customer and JobWok Invoice");

        this.dashboardReportService.localInvoices = this.dashboardReportService.localInvoices.concat(this.totalTransaction);
        this.getTotalPayements();

    }

    getTotalPayements() {
        this.totalReciveble = [];
       
        this.receivableArray.map(item => {

            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.CUSTOMER_TOTAL_RECEIVED,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalReciveble.push(item));


        this.jobworkReceivableArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName:AppSettings.CUSTOMER_TOTAL_RECEIVED,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalReciveble.push(item));

        this.getLineChart(this.totalReciveble, "Total_Received");

        if (this.totalReciveble.length > 0) {
            let totalReceivableSummary = this.getSummaryRow(this.totalReciveble);
            if (totalReceivableSummary) this.totalReceivableSummary = totalReceivableSummary;
        }
        this.dashboardReportService.localPayReceives = this.totalReciveble;
        let jobworkArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.CUSTOMER_JOBWORK_INVOICE);
        let jobworkArrayDistinct: DashboardTransactionSummary[] = [];
        let incomeArray: DashboardTransactionSummary[] = [];
        jobworkArray.forEach(ja => {
            if (jobworkArrayDistinct.length > 0) {
                if (jobworkArrayDistinct.find(jaD => jaD.month === ja.month)) {
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).dueAmount += ja.dueAmount;
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).transactionAmount += ja.transactionAmount;
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).transactionCount += ja.transactionCount;
                } else {
                    jobworkArrayDistinct.push(ja);
                }
            } else {
                jobworkArrayDistinct.push(ja);
            }
        })


        this.totalReciveble.forEach(ja => {
            if (incomeArray.length > 0) {
                if (incomeArray.find(jaD => jaD.month === ja.month)) {
                    incomeArray.find(jaD => jaD.month === ja.month).dueAmount += ja.dueAmount;
                    incomeArray.find(jaD => jaD.month === ja.month).transactionAmount += ja.transactionAmount;
                    incomeArray.find(jaD => jaD.month === ja.month).transactionCount += ja.transactionCount;
                } else {
                    incomeArray.push(ja);
                }
            } else {
                incomeArray.push(ja);
            }
        })
        this.getGroupedBarChart(jobworkArrayDistinct.concat(incomeArray), "Customer and JobWok Invoice");
    }


    totalOutgoingTransaction() {
        this.totalPurchase = [];
       

        this.purchaseArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.SUPPLIER_SUBCONTRACT_INVOICE,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalPurchase.push(item));


        this.subcontractArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.SUPPLIER_SUBCONTRACT_INVOICE,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalPurchase.push(item));












        let totalInvoiceSummary = this.getSummaryRow(this.totalPurchase);
        if (totalInvoiceSummary) this.totalPurchaseInvoiceSummary = totalInvoiceSummary;
        this.getLineChart(this.totalPurchase, "Supplier and Subcontract Invoice");
        this.dashboardReportService.localInvoices = this.dashboardReportService.localInvoices.concat(this.totalPurchase);
        this.getTotalPurchasePayements();

    }
    getTotalPurchasePayements() {
        this.totalPayements = [];
        this.payableArray.map(item => {

            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.SUPPLIER_TOTAL_PAID,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalPayements.push(item));


        this.subcontratPayableArray.map(item => {
            return {
                dueAmount: item.dueAmount,
                month: item.month,
                transactionName: AppSettings.SUPPLIER_TOTAL_PAID,
                transactionAmount: item.transactionAmount,
                transactionCount: item.transactionCount,
            }
        }).forEach(item => this.totalPayements.push(item));





        this.getLineChart(this.totalPayements, "Total Paid");

        if (this.totalPayements.length > 0) {
            let totalReceivableSummary = this.getSummaryRow(this.totalPayements);
            if (totalReceivableSummary) this.totalPayableSummary = totalReceivableSummary;
        }
        this.dashboardReportService.localPayReceives = this.totalPayements;
        let jobworkArray: DashboardTransactionSummary[] = this.dashboardReportService.localInvoices.filter(invRep => invRep.transactionName === AppSettings.SUPPLIER_SUBCONTRACT_INVOICE);


        let jobworkArrayDistinct: DashboardTransactionSummary[] = [];
        let outgoingArray: DashboardTransactionSummary[] = [];
        jobworkArray.forEach(ja => {
            if (jobworkArrayDistinct.length > 0) {
                if (jobworkArrayDistinct.find(jaD => jaD.month === ja.month)) {
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).dueAmount += ja.dueAmount;
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).transactionAmount += ja.transactionAmount;
                    jobworkArrayDistinct.find(jaD => jaD.month === ja.month).transactionCount += ja.transactionCount;
                } else {
                    jobworkArrayDistinct.push(ja);
                }
            } else {
                jobworkArrayDistinct.push(ja);
            }
        })


        this.totalPayements.forEach(ja => {
            if (outgoingArray.length > 0) {
                if (outgoingArray.find(jaD => jaD.month === ja.month)) {
                    outgoingArray.find(jaD => jaD.month === ja.month).dueAmount += ja.dueAmount;
                    outgoingArray.find(jaD => jaD.month === ja.month).transactionAmount += ja.transactionAmount;
                    outgoingArray.find(jaD => jaD.month === ja.month).transactionCount += ja.transactionCount;
                } else {
                    outgoingArray.push(ja);
                }
            } else {
                outgoingArray.push(ja);
            }
        })
        this.getGroupedBarChart(jobworkArrayDistinct.concat(outgoingArray), "Supplier and Subcontract Invoice");





    }



    getTransactionType() {

        this.dashboardReportService.getJwTransactionType()
            .subscribe(response => {

                if (response.responseStatus === 1) {
                    this.allowJwInvoice = true;

                }


                else {
                    this.allowJwInvoice = false;
                }

            });
    }

    getFinancialYear(){
        this.financialYearService.getFinancialYear().subscribe(response=>{
         if(response){
             this.isActive=response.isActive;
             this.financialYearDate=response.startDate;
         }

        });

    }
}  
export interface dashboardData {
    id: number,
    name: string,
  }