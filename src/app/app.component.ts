import { Component, OnInit } from '@angular/core';
import { RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';
import { CompanyService } from './services/company.service';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

   spinStart: boolean = false;

   // isChrome: boolean = !!window['chrome']&& !!window['chrome']['webstore'] 
   objAgent = navigator.userAgent;
   isChrome: boolean = false;
   constructor(private router: Router
      , private companyService: CompanyService) {

      //For non chrome browsers...Alert

      if ((this.objAgent.indexOf("Chrome")) != -1) {
         this.isChrome = true;
      }

      if (!this.isChrome) {
         alert('Suktha will work only on Google Chrome browser!');
      }
      // override the route reuse strategy
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
         return false;
      }

      // this.router.events.subscribe((evt) => {
         // if (evt instanceof NavigationEnd) {
         //    // trick the Router into believing it's last link wasn't previously loaded
         //    this.router.navigated = false;
         //    // if you need to scroll back to top, here is the right place
         //    window.scrollTo(0, 0);
         // }

         //  if (evt instanceof RouteConfigLoadStart) {
         //     //console.log('evt instanceof RouteConfigLoadStart ')
         //    this.spinStart = true;
         //  } else if (evt instanceof RouteConfigLoadEnd) {
         //    //console.log('evt instanceof RouteConfigLoadEnd ')
         //    this.spinStart = false;
         //  }

      // });

      // this.companyService.getCompanyLogoAsBase64(1)
      // .subscribe(response => {
      //    this.companyService.companyLogoBase64 = response;
      // })

   }

   ngOnInit() {
      this.router.events.subscribe((evt) => {
         if (evt instanceof RouteConfigLoadStart) {
            //console.log('evt instanceof RouteConfigLoadStart ')
            this.spinStart = true;
         } else if (evt instanceof RouteConfigLoadEnd) {
            //console.log('evt instanceof RouteConfigLoadEnd ')
            this.spinStart = false;
         }
      })
   }
}
