import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormControl, Validators } from '@angular/forms';
import { AlertService } from 'ngx-alerts';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  //   localUser = {
  //     userName: '',
  //     otp: '',
  //     newPassword: '',
  //     confirmPassword: ''

  // }

  public username = new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.noWhitespaceValidator]));
  public otp = new FormControl('', Validators.required);
  public newPassword = new FormControl('',Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(12), this.noWhitespaceValidator]));
  public confirmPassword = new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(12), this.noWhitespaceValidator]));

  public forgetCard: boolean = true;
  public otpCard: boolean = false;
  public passwordResetCard: boolean = false;

  public errorMessage: string;

  constructor(private userService: UserService,
    private alertService: AlertService,
    private router: Router,
    public snackBar: MatSnackBar) { }


  ngOnInit() {
    //console.log("in forget password")
  }


  forgotPassword() {

    //console.log("username..........", this.username.value);

    if(this.username.value!=""){
      this.userService.generateOtp(this.username.value).subscribe(response => {
        //console.log("response :", response);
        if (response.responseStatus === 1) {
          this.alertService.success( "OTP Sent to user registered email");
          this.otpCard = true;
          this.forgetCard = false;
          this.errorMessage = "";
        } else {
          this.forgetCard = true;
          this.errorMessage = response.responseString;
          this.alertService.danger(this.errorMessage);
        }
      });
    }
    else{
      this.alertService.danger( "Username can not be null");
    }


  }

  submitOtp() {
    this.forgetCard = false;
    this.otpCard = false; 


    //console.log("otp......", this.otp.value, this.username.value);


    this.userService.otpSubmit(this.otp.value, this.username.value).subscribe(response => {
      //console.log("response : ", response);
      if (response.responseStatus === 1) {
        this.alertService.success( "Verified. Please change your password");
        this.passwordResetCard = true;
        this.errorMessage = "";
      } else {
        this.otpCard = true;
        this.passwordResetCard = false;
        this.errorMessage = response.responseString;
        this.alertService.success(this.errorMessage);
      }

    })
  }

  validatePassword() {
    //console.log("confirmPassword" + this.confirmPassword.value)

    if (this.confirmPassword.value !== "") {
      if (this.newPassword.value !== this.confirmPassword.value) {
        //console.log('Not matching: ', this.newPassword.value, this.confirmPassword);
        this.newPassword.setErrors({ 'validateEqual': true });
        this.newPassword.setErrors({ 'incorrect': true });
        this.confirmPassword.setErrors({ 'incorrect': true });
        this.newPassword.markAsTouched();

      }
      else {
        //console.log('Matching');
      
        this.newPassword.setErrors(null);
        this.newPassword.setErrors(null);
      }
    }
    
    if(this.newPassword.value === this.username.value){
      this.newPassword.setErrors({ 'sameAsUsername': true });
    }else{
      this.newPassword.setErrors(null);
    }

  }

  passwordReset() {
    // var passwordpattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{9,}$/;
    // if ( passwordpattern.test(this.newPassword.value ) == false || 
    //     passwordpattern.test(this.confirmPassword.value) == false) {
    //       this.alertService.danger("password should have Lowercase letters, Uppercase letters, Numbers, Special characters and minimum lenght should be 10.");
    // }
    // else{
    //console.log("newPassword...........", this.newPassword.value);
    //console.log("confirmPassword...........", this.confirmPassword.value);
    // if (this.newPassword.value != null || this.confirmPassword.value != "") {
    //   if (this.newPassword.value === this.confirmPassword.value) {
    //     this.userService.passwordReset(this.username.value, this.newPassword.value).subscribe(response => {
    //       //console.log("response : ", response);
    //       this.snackBar.open("succes", "Password reset successfully ", {
    //         duration: 2000
    //       });
    //       this.router.navigate(['/login']);
    //     })
    //   }
    //   else {
    //     this.alertService.danger( "Password mismatch");
    //   }
    // }
    // }

    if(this.newPassword.valid && this.confirmPassword.valid){
        this.userService.passwordReset(this.username.value, this.newPassword.value).subscribe(response => {
          //console.log("response : ", response);
          this.snackBar.open("succes", "Password reset successfully ", {
            duration: 2000
          });

          this.userService.passwordResetComplete(this.username.value, 0)
          .subscribe(response => {
            //console.log("IS COMPLETE? :"+response);
          });

          this.router.navigate(['/login']);
        })
    }


  }

  public noWhitespaceValidator(control: FormControl) {
    // const isWhitespace = (control.value || '').indexOf(' ') > 0
    const isWhitespace = /\s/g.test(control.value || '');
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
}
