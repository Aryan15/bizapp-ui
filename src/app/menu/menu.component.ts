import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from '../data-model/activity-model';
import { UserService } from '../services/user.service';
import { AuthService } from '../auth/auth.service';
import { UserModel } from '../data-model/user-model';
import { AppSettings } from '../app.settings';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../utils/confirmation-dialog';

@Component({
    selector:'menu-comp',
    templateUrl:'./menu.component.html',
    styleUrls:['./menu.component.scss']
})
export class MenuComponent implements OnInit{

    currentUser : UserModel;
    dialogRef: MatDialogRef<ConfirmationDialog>;

    // public menu: Menu;

    public menus: Menu[];
    themeClass: any;
    incorrectMenu: boolean = false;

    constructor(private _router: Router
                , public dialog: MatDialog
                , private userService: UserService
                , private authService: AuthService){
        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
        this.themeClass = this.currentUser.userThemeName;
    }

    ngOnInit(){

        //console.debug('In on init of menu component');
        this.userService.getMenusForUser(this.currentUser.username)
            .subscribe(menus => {
                this.menus = menus;
                ////console.log("Got menus: ", this.menus);

                if(!menus || menus.length === 0){
                    this.incorrectMenu = true;
                }

                if(this.menus && menus.length > 0){
                    this.incorrectMenu = true;
                    this.menus.forEach(menu => {                    
                        if(menu.activities.find(act => act.name === 'Logout')){
                            this.incorrectMenu = false;
                        }
                    })
                }

            }, error =>{
                this.incorrectMenu = true;
            });

            
    }

    // logout(){
    //     localStorage.removeItem('currentUser');
    //     localStorage.removeItem('menuForUser');
    //     localStorage.removeItem('transactionTypes');
    //     this._router.navigate(['login']);
    // }

    onLogout(){
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = AppSettings.LOG_OUT_CONFIRMATION_MESSAGE;
        this.dialogRef.afterClosed().subscribe(result => {

            if (result) {
                this.authService.logOut();
            }

        });
      
      }

    levelFilter(menuList: Menu[]) : Menu[]{

        let outMenuList : Menu[];

        outMenuList = this.menus ? this.menus.filter(menu => menu.menuLevel === 1) : null;

        return outMenuList;
    }
}