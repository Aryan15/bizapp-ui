import {Component, Input} from '@angular/core';
import { Menu } from '../data-model/activity-model';
import { UserService } from '../services/user.service';
import { UserModel } from '../data-model/user-model';
import { AppSettings } from '../app.settings';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'menu-item-component',
    templateUrl:'menu-item.component.html',
    styleUrls: ['menu-item.component.scss']
})
export class MenuItemComponent{

    @Input() menu: Menu;
    currentUser : UserModel;
    dialogRef: MatDialogRef<ConfirmationDialog>;
    themeClass: any;
    constructor(private userService: UserService
        , public dialog: MatDialog
        , private authService: AuthService){
        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
        this.themeClass = this.currentUser.userThemeName;
    }


    onMenuClick(menuName: string, activityName: string){
        //console.log('activityName:', activityName, "menuName :",menuName);
        this.userService.currentActivity =  menuName + " > ";
        this.userService.reportHeading =  "Report" + " > " +menuName + " > ";
        // this.userService.reportHeading =  ">" + menuName + " > " + activityName;
        this.userService.reportName = activityName;
        this.userService.activityName = activityName;
    }

    onLogout(){
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = AppSettings.LOG_OUT_CONFIRMATION_MESSAGE;
        this.dialogRef.afterClosed().subscribe(result => {

            if (result) {
                this.authService.logOut();
            }

        });
      
      }
    
      getUrlLink(pageUrl: string, urlParams: string): any[] {
          let urlParamsArray: string[] = urlParams.split(",");
          ////console.log("urlParamsArray: "+urlParamsArray);
          return ['/'+pageUrl, ...urlParamsArray];
      }
}



