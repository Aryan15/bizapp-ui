import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { TransactionReportRequest } from "../data-model/transaction-report-request-model";
import { Page } from "../reports/model/page";
import { B2BGSTReportWrapper, B2BGSTReport } from "../data-model/invoice-model";
import { Observable } from "rxjs";
import { PagedData } from "../reports/model/paged-data";
import { map } from "rxjs/operators";


@Injectable({
    providedIn: 'root'
  })
export class GstReportService {
    
    apiGetB2BUrl : string ;

    constructor(private http : HttpClient){
        this.apiGetB2BUrl = environment.baseServiceUrl + environment.apiGetB2BUrl;
    }

    getB2BReport(invoiceTypeId: number, invoiceReportRequest : TransactionReportRequest, page: Page): Observable<PagedData<B2BGSTReport>>{

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
      
        return this.http.post<B2BGSTReportWrapper>(
            this.apiGetB2BUrl+ "/" + 
            invoiceTypeId + "/" +
            pageNumber + "/" +
            pageSize + "/" + 
            pageSortDirection+ "/" + 
            pageSortColumn,invoiceReportRequest ).pipe(
            map(b2bData => this.getPagedDatas(page, b2bData)));
    }


    private getPagedDatas(page: Page, b2bData: B2BGSTReportWrapper): PagedData<B2BGSTReport> {
        
        //console.log("b2bData: ",b2bData);
        let pagedData = new PagedData<B2BGSTReport>();

        if(page){
        page.totalElements = b2bData.totalCount;
        page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = b2bData.b2bGSTReports;
        //console.log('b2bData.b2bGSTReports: ', b2bData.b2bGSTReports);
        //console.log("page ", page);
        return pagedData;
    }
}