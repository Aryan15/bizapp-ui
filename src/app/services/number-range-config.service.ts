

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { NumberRangeConfiguration, PrintTemplate } from '../data-model/number-range-config-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { PrintTemplateWrapper } from '../settings/print-template/print-template.component';

@Injectable()
export class NumberRangeConfigService {

    numberRangeConfigApiUrl : string;
    updatePrintTemplateApiUrl : string;
    updatePrintTopMarginApiUrl : string;
    numberRangeConfigApiUrlType: string;
    printTemplateApiUrl: string;

    constructor(private http: HttpClient){
        this.numberRangeConfigApiUrl = environment.baseServiceUrl + environment.numberRangeConfigApiUrl;
        this.updatePrintTemplateApiUrl = environment.baseServiceUrl + environment.updatePrintTemplateApiUrl;
        this.updatePrintTopMarginApiUrl = environment.baseServiceUrl + environment.updatePrintTopMarginApiUrl;
        this.numberRangeConfigApiUrlType = environment.baseServiceUrl + environment.numberRangeConfigApiUrlByType;
        this.printTemplateApiUrl = environment.baseServiceUrl + environment.printTemplateApiUrl;
        
    }

    getAllConfig() : Observable<NumberRangeConfiguration[]> {
        return this.http.get<NumberRangeConfiguration[]>(this.numberRangeConfigApiUrl)
                        // .map(response => <NumberRangeConfiguration[]>response.json());
    }

    getNumberRangeConfigurationForTransactionType(transactionTypeId:number): Observable<NumberRangeConfiguration>{
      
        return this.http
        .get<NumberRangeConfiguration>(this.numberRangeConfigApiUrlType +"/"+transactionTypeId)
        // .pipe(
        //     shareAndCache(AppSettings.LOCAL_STORAGE_NRC_FOR_USER)
        // )
        // .map(response => <NumberRangeConfiguration>response.json());
    }

    saveAllConfig(models : NumberRangeConfiguration[]) : Observable<NumberRangeConfiguration[]> {
        return this.http.post<NumberRangeConfiguration[]>(this.numberRangeConfigApiUrl, models)
                        // .map(response => <NumberRangeConfiguration[]>response.json());
    }

    delete(id: number) : Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.numberRangeConfigApiUrl+"/"+id)
                    // .map(response => <TransactionResponse>response.json() );
    }

    updatePrintTemplate(list: NumberRangeConfiguration[]): Observable<NumberRangeConfiguration[]>{

        let model: PrintTemplateWrapper = {
            templates: list
        }
        return this.http.post<NumberRangeConfiguration[]>(this.updatePrintTemplateApiUrl, model)
        
    }


    updatePrintTopMargin(id: number, topMargin: number,printHeaderText: string,allowShipAddress:number): Observable<boolean>{

        let model: NumberRangeConfiguration= <NumberRangeConfiguration> {};
        
        model.id = id;
        model.printTemplateTopSize = topMargin;
        model.printHeaderText = printHeaderText;
        model.allowShipingAddress=allowShipAddress;

        return this.http.post<boolean>(this.updatePrintTopMarginApiUrl, model)
        
    }

    getAllPrintTemplates(): Observable<PrintTemplate[]> {
        return this.http.get<PrintTemplate[]>(this.printTemplateApiUrl);
    }
}