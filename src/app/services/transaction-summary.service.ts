import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TransactionItem } from '../data-model/transaction-item';
import { Tax } from '../data-model/tax-model';

@Injectable()
export class TransactionSummaryService {


    patchAmounts(items: TransactionItem[]
        , inForm: FormGroup
        , isItemLevelTax: boolean
        , isIgst: boolean
        , headerTax: Tax
        , isInclusiveTax: boolean) 
    {
        let totalDiscount: number = 0;
        let totalTaxableAmount: number = 0;
        let totalSgstTax: number = 0;
        let totalCgstTax: number = 0;
        let totalIgstTax: number = 0;
        let grandTotal: number = 0;
        let totalSubTotal: number = 0;
        let ltotalAmount: number = 0;
        items.forEach(item => {
        
            totalDiscount += +item.discountAmount;
            if(isInclusiveTax){
                totalTaxableAmount += item.amount;
            }else{
                totalTaxableAmount += item.amountAfterDiscount;    
            }            
            totalSubTotal += item.amount;
            totalSgstTax += !isIgst ? item.sgstTaxAmount : 0;
            totalCgstTax += !isIgst ? item.cgstTaxAmount : 0;
            totalIgstTax += isIgst ? item.igstTaxAmount : 0;
            grandTotal += item.amountAfterTax;
            ltotalAmount += item.price * item.quantity;
        })
        //console.log('grand total:', grandTotal);

        // if(isItemLevelTax){
        
            totalDiscount = +totalDiscount.toFixed(2);
            totalTaxableAmount = +totalTaxableAmount.toFixed(2);
            totalSgstTax = +totalSgstTax.toFixed(2);
            totalCgstTax = +totalCgstTax.toFixed(2);
            totalIgstTax = +totalIgstTax.toFixed(2);
            grandTotal = +grandTotal.toFixed(2);
            totalSubTotal = +totalSubTotal.toFixed(2);
            //if(isItemLevelTax||totalDiscount === 0){
            inForm.controls['totalDiscount'].patchValue(totalDiscount);
            //}
            inForm.controls['totalTaxableAmount'].patchValue(totalTaxableAmount);
            inForm.controls['subTotalAmount'].patchValue(totalSubTotal);
        // }
        if (!isItemLevelTax) {
       
            let ltotalTaxableAmount: number = inForm.controls['totalTaxableAmount'].value;

            let lheaderDiscountPercent: number = inForm.controls['discountPercent'].value;
            let lheaderDiscountAmount: number = 0;
            if(isInclusiveTax){
               
                lheaderDiscountAmount = lheaderDiscountPercent ? (+ltotalAmount * (+lheaderDiscountPercent/100)) : 0;
            }else{
             
                lheaderDiscountAmount = lheaderDiscountPercent ? (+ltotalTaxableAmount * (+lheaderDiscountPercent/100)) : 0;
            }

  
            inForm.controls['totalDiscount'].patchValue(lheaderDiscountAmount.toFixed(2));

            let ltotalSgstTax: number = !isIgst ? (ltotalTaxableAmount * (headerTax ? headerTax.rate / 100 : 0)) / 2 : 0;
            let ltotalCgstTax: number = !isIgst ? (ltotalTaxableAmount * (headerTax ? headerTax.rate / 100 : 0)) / 2 : 0;
            let ltotalIgstTax: number = isIgst ? ltotalTaxableAmount * (headerTax ? headerTax.rate / 100 : 0) : 0;

            //let lgrandTotal: number = ltotalTaxableAmount + ltotalSgstTax + ltotalCgstTax + ltotalIgstTax;
            let lgrandTotal: number = (ltotalTaxableAmount - lheaderDiscountAmount) + ltotalSgstTax + ltotalCgstTax + ltotalIgstTax;
       
            ltotalSgstTax = +ltotalSgstTax.toFixed(2);
            ltotalCgstTax = +ltotalCgstTax.toFixed(2);
            ltotalIgstTax = +ltotalIgstTax.toFixed(2);
            lgrandTotal = +lgrandTotal.toFixed(2);

            inForm.controls['sgstTaxRate'].patchValue(!isIgst ? (headerTax ? headerTax.rate/2 : 0) : 0);
            inForm.controls['cgstTaxRate'].patchValue(!isIgst ? (headerTax ? headerTax.rate/2 : 0) : 0);
            inForm.controls['igstTaxRate'].patchValue(isIgst ? (headerTax ? headerTax.rate : 0) : 0);

            inForm.controls['sgstTaxAmount'].patchValue(ltotalSgstTax);
            inForm.controls['cgstTaxAmount'].patchValue(ltotalCgstTax);
            inForm.controls['igstTaxAmount'].patchValue(ltotalIgstTax);
            inForm.controls['grandTotal'].patchValue(lgrandTotal);

        } else {
            inForm.controls['sgstTaxAmount'].patchValue(totalSgstTax);
            inForm.controls['cgstTaxAmount'].patchValue(totalCgstTax);
            inForm.controls['igstTaxAmount'].patchValue(totalIgstTax);
            inForm.controls['grandTotal'].patchValue(grandTotal);
        
        }

    }

}