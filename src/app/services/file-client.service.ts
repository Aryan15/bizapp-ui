import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { TransactionResponse } from '../data-model/transaction-response';






@Injectable()
export class FileUploadClientService {
    apiBaseURL : string = environment.baseServiceUrl+environment.filesCompanyApi;
    fileDeleteUrl: string;
    certImageDeleteUrl: string;
    sigantureImageDeleteUrl: string;
    constructor(private http: HttpClient){ 
      this.fileDeleteUrl = environment.baseServiceUrl+environment.fileDeleteUrl;
      this.certImageDeleteUrl = environment.baseServiceUrl+environment.certImageDeleteUrl;
      this.sigantureImageDeleteUrl = environment.baseServiceUrl+environment.sigantureImageDeleteUrl;
    }

    fileUpload(url:string,fileItem:File, extraData?:object):any{
      const formData: FormData = new FormData();
       
      formData.append('image', fileItem, fileItem.name);
      if (extraData) {
        for(let key in extraData){
            // iterate and set other form data
          formData.append(key, extraData[key])
        }
      }

      const req = new HttpRequest('POST', url, formData, {
        reportProgress: true // for progress data
      });
      return this.http.request(req)
    }
  
    deleteFile(fileName: string, companyId: number){
      return this.http.delete<TransactionResponse>(this.fileDeleteUrl+"/"+fileName+"/"+companyId);
    }

    deleteCertImage(fileName: string, companyId: number){
      return this.http.delete<TransactionResponse>(this.certImageDeleteUrl+"/"+fileName+"/"+companyId);
    }

    deleteSignatureImage(fileName: string, companyId: number){
      return this.http.delete<TransactionResponse>(this.sigantureImageDeleteUrl+"/"+fileName+"/"+companyId);
    }



  optionalFileUpload(fileItem?:File, extraData?:object):any{
      
      const formData: FormData = new FormData(); //?
       let fileName;
      if (extraData) {
        for(let key in extraData){
            // iterate and set other form data
            if (key == 'fileName'){
              fileName = extraData[key]
            }
          formData.append(key, extraData[key])
        }
      }

      if (fileItem){
        if (!fileName){
           fileName = fileItem.name
        }
        formData.append('image', fileItem, fileName);
      }
      const req = new HttpRequest('POST', this.apiBaseURL, formData, {
        reportProgress: true // for progress data
      });
      return this.http.request(req)
  }
    // list(): Observable<any>{
    //   const listEndpoint = `${this.apiBaseURL}files/`
    //   return this.http.get(listEndpoint)
    // }

}