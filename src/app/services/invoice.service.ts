
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { InvoiceHeader, InvoiceItemReport, InvoiceReport, InvoiceReportItemModel, InvoiceReportModel, InvoiceTaxReport, InvoiceTaxReportModel, InvoiceWrapper, RecentInvoice } from '../data-model/invoice-model';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { Page } from "../reports/model/page";
import { PagedData } from "../reports/model/paged-data";
import { TokenService } from './token.service';
import { TallyExportTracker, TallyExportTrackerReport } from '../data-model/misc-model';
import { AutoTransactionGenerationList, AutoTransactionGeneration, RecentAutoTransaction } from '../data-model/autotransaction-popup-model';

@Injectable()
export class InvoiceService {

    apiGetAllUrl: string;
    apiPostUrl: string;
    apiGetUrl: string;
    apiGetInvoiceNumberUrl: string;
    apiGetByPartyAndTransactionType: string;
    apiGetNoteByPartyAndTransactionType: string;
    apiGetAllCustomerInvoicesUrl: string;
    apiGetAllSupplierInvoicesUrl: string;
    token: string;
    apiDeleteUrl: string;
    apiDeleteInvoiceItemUrl: string;
    recentInvoiceApiUrl: string;
    invoiceReportApiUrl: string;
    invoiceTaxReportApiUrl: string;
    invoiceItemReportApiUrl: string;
    apiGetProformaInvoicesForParty: string;
    checkInvNumberApiUrl: string;
    apiGetInvoicesForNotesWithParty: string;
    apiGetInvoiceCount: string;
    tallyVoucherApiUrl: string;
    tallyExportTrackerReportApiUrl: string;
    tallyMasterApiUrl: string;
    tallyOpenLogApiUrl: string;
    //options : RequestOptions;
    autoTransactionApiUrl: string;
    apiGetInvoiceNumber: string;

    apiDeleteTransaction: string;
    apiRecentAutoTransaction: string;
    apiStopTransaction: string;
    apiGetAutoTransaction: string;
    apiGetAutoTransactionLog: string;
    apiGetAutoTransactionForInvoice: string;
    apiGetInvoiceIdUrl:string;

    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.apiGetAllUrl = environment.baseServiceUrl + environment.invoiceApiGetAllUrl;//'http://localhost:8081/api/country';
        this.apiPostUrl = environment.baseServiceUrl + environment.invoiceApiPostUrl;//'http://localhost:8081/api/country';
        this.apiGetUrl = environment.baseServiceUrl + environment.invoiceApiGetUrl;
        this.apiGetInvoiceNumberUrl = environment.baseServiceUrl + environment.apiGetInvoiceNumberUrl;
        this.apiGetByPartyAndTransactionType = environment.baseServiceUrl + environment.apiGetByPartyAndTransactionType;
        this.apiGetAllCustomerInvoicesUrl = environment.baseServiceUrl + environment.invoiceApiGetAllCustomerUrl;
        this.apiGetAllSupplierInvoicesUrl = environment.baseServiceUrl + environment.invoiceApiGetAllSupplierUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.invoiceApiGetUrl;
        this.apiDeleteInvoiceItemUrl = environment.baseServiceUrl + environment.invoiceItemDeleteApiUrl;
        this.recentInvoiceApiUrl = environment.baseServiceUrl + environment.recentInvoiceApiUrl;
        this.invoiceReportApiUrl = environment.baseServiceUrl + environment.invoiceReportApiUrl;
        this.invoiceTaxReportApiUrl = environment.baseServiceUrl + environment.invoiceTaxReportApiUrl;
        this.invoiceItemReportApiUrl = environment.baseServiceUrl + environment.invoiceItemReportApiUrl;
        this.apiGetProformaInvoicesForParty = environment.baseServiceUrl + environment.apiGetProformaInvoicesForParty;
        this.checkInvNumberApiUrl = environment.baseServiceUrl + environment.checkInvNumberApiUrl;
        this.apiGetInvoicesForNotesWithParty = environment.baseServiceUrl + environment.apiGetInvoicesForNotesWithParty;
        this.apiGetNoteByPartyAndTransactionType = environment.baseServiceUrl + environment.apiGetNoteByPartyAndTransactionType;
        this.apiGetInvoiceCount = environment.baseServiceUrl + environment.apiGetInvoiceCount;
        this.tallyVoucherApiUrl = environment.baseServiceUrl + environment.tallyVoucherApiUrl;

        this.tallyExportTrackerReportApiUrl = environment.baseServiceUrl + environment.tallyExportTrackerReportApiUrl;
        this.tallyMasterApiUrl = environment.baseServiceUrl + environment.tallyMasterApiUrl;
        this.tallyOpenLogApiUrl = environment.baseServiceUrl + environment.tallyOpenLogApiUrl;
        this.autoTransactionApiUrl = environment.baseServiceUrl + environment.autoTransactionApiUrl;
        this.apiGetInvoiceNumber = environment.baseServiceUrl + environment.invoiceNumberApiUrl;
        this.apiDeleteTransaction = environment.baseServiceUrl + environment.apiDeleteTransaction;
        this.apiRecentAutoTransaction = environment.baseServiceUrl + environment.apiRecentAutoTransaction;
        this.apiStopTransaction = environment.baseServiceUrl + environment.apiStopTransaction;
        this.apiGetAutoTransaction = environment.baseServiceUrl + environment.apiGetAutoTransaction;
        this.apiGetAutoTransactionLog = environment.baseServiceUrl + environment.apiGetAutoTransactionLog;
        this.apiGetAutoTransactionForInvoice = environment.baseServiceUrl + environment.apiGetAutoTransactionCancel;
        this.apiGetInvoiceIdUrl = environment.baseServiceUrl + environment.apiGetInvoiceByNumber;
    }


    getAllInvoices(): Observable<InvoiceHeader[]> {



        return this.http.get<InvoiceHeader[]>(this.apiGetAllUrl)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    getCustomerInvoices(): Observable<InvoiceHeader[]> {

        return this.http.get<InvoiceHeader[]>(this.apiGetAllCustomerInvoicesUrl)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    getSupplierInvoices(): Observable<InvoiceHeader[]> {

        return this.http.get<InvoiceHeader[]>(this.apiGetAllSupplierInvoicesUrl)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    getInvoice(headerInvoiceId: string): Observable<InvoiceHeader> {

        return this.http.get<InvoiceHeader>(this.apiGetUrl + "/" + headerInvoiceId)
        // .map(response => <InvoiceHeader>response.json());

    }

    getInvoiceByInvoiceNumber(headerInvoiceNumber: string): Observable<InvoiceHeader> {

        return this.http.get<InvoiceHeader>(this.apiGetInvoiceNumberUrl + "/" + headerInvoiceNumber)
        // .map(response => <InvoiceHeader>response.json());

    }

    getInvoicesByPartyAndTransactionType(partyId: number, transactionTypeId: number): Observable<InvoiceHeader[]> {
        //console.log("dfs...................");

        return this.http.get<InvoiceHeader[]>(this.apiGetByPartyAndTransactionType + "/" + partyId + "/" + transactionTypeId)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    getOpenNotesByPartyAndTransactionType(partyId: number, transactionTypeId: number): Observable<InvoiceHeader[]> {
        return this.http.get<InvoiceHeader[]>(this.apiGetNoteByPartyAndTransactionType + "/" + partyId + "/" + transactionTypeId)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    getInvoicesByPartyAndTransactionTypeForNote(partyId: number, transactionTypeName: string): Observable<InvoiceHeader[]> {
        return this.http.get<InvoiceHeader[]>(this.apiGetInvoicesForNotesWithParty + "/" + partyId + "/" + transactionTypeName)
        // .map(response => <InvoiceHeader[]>response.json());
    }

    create(invoiceWrapper: InvoiceWrapper): Observable<InvoiceHeader> {

        ////console.log("In create...", invoiceHeader.invoiceDate);
        return this.http.post<InvoiceHeader>(this.apiPostUrl, invoiceWrapper)
        // .map(response => <InvoiceHeader>response.json());
    }

    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }
    deleteItem(id: string): Observable<TransactionResponse> {

        return this.http.delete<TransactionResponse>(this.apiDeleteInvoiceItemUrl + "/" + id);
    }

    getRecentInvoices(transactionType: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentInvoice> {
        //console.log("getRecentInvoices...!!", filterText );

        sortColumn = sortColumn ? sortColumn : 'invoiceDate';
        sortColumn = (sortColumn === "invoiceNumber") ? "invId" : sortColumn;
        sortDirection = sortDirection ? sortDirection : "desc";
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : "null";

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        //console.log("filterText: ", filterText);
        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        return this.http
            .get<RecentInvoice>(this.recentInvoiceApiUrl + "/" + transactionType + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, { params: params });
    }



    // getInvoiceReport(invoiceReportRequest : InvoiceReportRequest): Observable<InvoiceReportModel[]>{
    //     //console.log('before post: ', invoiceReportRequest);
    //     let ret: Observable<InvoiceReportModel[]> = this.http.post<InvoiceReportModel[]>(this.invoiceReportApiUrl,invoiceReportRequest );
    //     //console.log('returning: ')
    //     ret.subscribe(response => {
    //         //console.log('response: ', response);
    //     })
    //     return ret;
    // }


    getInvoiceReport(invoiceTypeId: number, invoiceReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<InvoiceReportModel>> {


        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
        pageSortColumn = (pageSortColumn === "invoiceNumber") ? "invId" : pageSortColumn;
        //  pageSortColumn = pageSortColumn ? pageSortColumn : 'invoiceDate';


        return this.http.post<InvoiceReport>(
            this.invoiceReportApiUrl + "/" +
            invoiceTypeId + "/" +
            pageNumber + "/" +
            pageSize + "/" +
            pageSortDirection + "/" +
            pageSortColumn, invoiceReportRequest).pipe(
                map(invoiceData => this.getPagedDatas(page, invoiceData)));
    }

    getTaxInvoiceReport(invoiceTypeId: number, invoiceReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<InvoiceTaxReportModel>> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
        //console.log("pageSortColumn",pageSortColumn)
        pageSortColumn = (pageSortColumn === "invoiceNumber") ? "invId" : pageSortColumn;

        return this.http.post<InvoiceTaxReport>(
            this.invoiceTaxReportApiUrl + "/" +
            invoiceTypeId + "/" +
            pageNumber + "/" +
            pageSize + "/" +
            pageSortDirection + "/" +
            pageSortColumn, invoiceReportRequest).pipe(
                map(invoiceData => this.getTaxReportPagedDatas(page, invoiceData)));
    }

    private getPagedDatas(page: Page, invoiceData: InvoiceReport): PagedData<InvoiceReportModel> {

        //console.log("invoiceItemData: ",invoiceData);
        let pagedData = new PagedData<InvoiceReportModel>();

        if (page) {
            page.totalElements = invoiceData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = invoiceData.invoiceReports;
        //console.log('invoiceItemData.invoiceReportItems: ', invoiceData.invoiceReports);
        //console.log("page ", page);
        return pagedData;
    }

    private getTaxReportPagedDatas(page: Page, invoiceData: InvoiceTaxReport): PagedData<InvoiceTaxReportModel> {

        //console.log("invoiceItemData: ",invoiceData);
        let pagedData = new PagedData<InvoiceTaxReportModel>();

        if (page) {
            page.totalElements = invoiceData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = invoiceData.invoiceTaxReports;
        //console.log('invoiceItemData.invoiceTaxReports: ', invoiceData.invoiceTaxReports);
        //console.log("page ", page);
        return pagedData;
    }

    getTallyMastersXML(partyTypeId: number): Observable<any> {
        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.get<any>(this.tallyMasterApiUrl + "/" + partyTypeId, httpOption)
            .pipe(
                map(res => new Blob([res['body']], { type: 'application/xml' }))
            )
    }



    getTallyVoucherXML(transactionTypeId: string, invoiceReportRequest: TransactionReportRequest): Observable<any> {


        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.post<any>(this.tallyVoucherApiUrl + "/" + transactionTypeId, invoiceReportRequest, httpOption)
            .pipe(
                map(res => new Blob([res['body']], { type: 'application/xml' }))
            )

    }


    makeEntryInTallyLog(transactionTypeId: string, invoiceReportRequest: TransactionReportRequest): Observable<any> {


        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.post<any>(this.tallyOpenLogApiUrl + "/" + transactionTypeId, invoiceReportRequest, httpOption)
            .pipe(
                map(res => new Blob([res['body']], { type: 'application/xml' }))
            )

    }



    getInvoiceItemReport(transactionTypeId: number, invoiceReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<InvoiceReportItemModel>> {
        //let ret: Observable<InvoiceReportItemModel[]> = this.http.post<InvoiceReportItemModel[]>(this.invoiceItemReportApiUrl+ "/" + page + "/" + pageSize,invoiceReportRequest );
        // //console.log('returning: ')
        // ret.subscribe(response => {
        //     //console.log('response: ', response);
        // })

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? (page.sortColumn ? page.sortColumn : "invId") : null;
        pageSortColumn = (pageSortColumn === "invoiceNumber") ? "invId" : pageSortColumn;

        return this.http.post<InvoiceItemReport>(this.invoiceItemReportApiUrl + "/"
            + transactionTypeId + "/"
            + pageNumber + "/"
            + pageSize + "/"
            + pageSortDirection + "/"
            + pageSortColumn, invoiceReportRequest).pipe(
                map(invoiceItemData => this.getPagedData(page, invoiceItemData)));
    }


    private getPagedData(page: Page, invoiceItemData: InvoiceItemReport): PagedData<InvoiceReportItemModel> {

        //console.log("invoiceItemData: ",invoiceItemData);
        let pagedData = new PagedData<InvoiceReportItemModel>();

        if (page) {
            page.totalElements = invoiceItemData.totalCount ? invoiceItemData.totalCount : 0;
            page.totalPages = page.totalElements / page.size;
        }
        // let start = page.pageNumber * page.size;
        // let end = Math.min((start + page.size), page.totalElements);
        // for (let i = start; i < end; i++){
        //     let jsonObj = companyData[i];
        //     let employee = new CorporateEmployee(jsonObj.name, jsonObj.gender, jsonObj.company, jsonObj.age);
        //     pagedData.data.push(employee);
        // }
        pagedData.page = page;
        pagedData.data = invoiceItemData.invoiceReportItems;
        //console.log('invoiceItemData.invoiceReportItems: ', invoiceItemData.invoiceReportItems);
        //console.log("page ", page);
        return pagedData;
    }
    getProformaInvoicesForParty(partyId: number, invType: string): Observable<InvoiceHeader[]> {
        return this.http.get<InvoiceHeader[]>(this.apiGetProformaInvoicesForParty + "/" + partyId + "/" + invType)
        // .map(response => response.json());
    }

    checkInvNumAvailability(invNumber: string, transactionTypeId: number, partyId: number): Observable<TransactionResponse> {
        //console.log("invnumber :", invNumber)
        //console.log("partyId :", partyId)
        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("invNumber", invNumber);

        return this.http.get<TransactionResponse>(this.checkInvNumberApiUrl + "/" + transactionTypeId + "/" + partyId, { params: params })
        // .map(response => <TransactionResponse>response.json());

    }
    getTransactionCount(): Observable<number> {
        //console.log("in transaction count")
        return this.http.get<number>(this.apiGetInvoiceCount);
    }


    getTallyExportTracker(filterForm: TransactionReportRequest, page: Page): Observable<PagedData<TallyExportTracker>> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? (page.sortColumn ? page.sortColumn : "createdDateTime") : null;


        return this.http.post<TallyExportTrackerReport>(this.tallyExportTrackerReportApiUrl + "/"
            + pageNumber + "/"
            + pageSize + "/"
            + pageSortDirection + "/"
            + pageSortColumn, filterForm).pipe(
                map(invoiceItemData => this.getPagedTallyExportTrackerData(page, invoiceItemData)));
    }


    private getPagedTallyExportTrackerData(page: Page, trackerData: TallyExportTrackerReport): PagedData<TallyExportTracker> {

        let pagedData = new PagedData<TallyExportTracker>();

        if (page) {
            page.totalElements = trackerData.totalCount ? trackerData.totalCount : 0;
            page.totalPages = page.totalElements / page.size;
        }

        pagedData.page = page;
        pagedData.data = trackerData.tallyExportTrackerItems;
        return pagedData;
    }

    public saveAutoTransactionDetail(data: AutoTransactionGenerationList): Observable<AutoTransactionGenerationList> {
       // data.autoTransactionGenerationDTO.invoiceHeaderId = id;
        return this.http.post<AutoTransactionGenerationList>(this.autoTransactionApiUrl, data);
    }



    deleteTransaction(id: number): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteTransaction + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }


    public getInvoiceByTransactionType(partyId:number,transactionTypeId:number): Observable<InvoiceHeader[]> {
        return this.http.get<InvoiceHeader[]>(this.apiGetInvoiceNumber +"/" + partyId +"/" +transactionTypeId);
    }


    getRecentTransaction(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentAutoTransaction> {

        sortColumn = sortColumn ? sortColumn : 'startDate';
        if(sortColumn==='partyName'){
            sortColumn = 'partyId'
        }
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';



        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        //console.log("filterText: ", filterText, transactionTypeId , this.recentVoucherApiUrl );

        return this.http
            .get<RecentAutoTransaction>(this.apiRecentAutoTransaction + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, { params: params });
    }

    public stopAutoTransaction(data: AutoTransactionGeneration): Observable<AutoTransactionGeneration> {


        return this.http.post<AutoTransactionGeneration>(this.apiStopTransaction, data);

    }

    public getAutoTransaction(id: String): Observable<AutoTransactionGeneration> {
        return this.http.get<AutoTransactionGeneration>(this.apiGetAutoTransaction + "/" + id)
    }

    public getAutoTransactionInvoice(id: String): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.apiGetAutoTransactionLog + "/" + id)
    }

    public getAutoTransactionInvoiceForCancel(id: String): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.apiGetAutoTransactionForInvoice + "/" + id)
    }

    getInvoiceByInvoiceId(invoiceNumber: string,type:number): Observable<InvoiceHeader[]> {

        if (!invoiceNumber) {
            //console.log("if......");  
            return new Observable<InvoiceHeader[]>(null);
        }
        else {

            let params = new HttpParams();
            params = params.append("searchText", invoiceNumber);
            
            return this.http
                .get<InvoiceHeader[]>(this.apiGetInvoiceIdUrl + "/" + type, {params: params})
        }

    }
}
