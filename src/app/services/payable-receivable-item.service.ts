import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PayableReceivableItem } from '../data-model/pr-model';
import { AppSettings } from '../app.settings';

@Injectable()
export class PayableReceivableItemService {

    constructor(private fb: FormBuilder) {

    }

    mapItem(index: number, item: any, formSource: string, paymentMode: number, eventSource: string): PayableReceivableItem {
        let pItem: PayableReceivableItem = {
            slNo: index,
            id: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.id : null,
            payableReceivableHeaderId: null,
            invoiceHeaderId: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.invoiceHeaderId : item.id,
            invoiceNumber: item.invoiceNumber,
            invoiceDate: item.invoiceDate,
            paymentDueDate: item.paymentDueDate,
            invoiceAmount: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.invoiceAmount : item.grandTotal,
            paidAmount: formSource === AppSettings.PR_ITEM_INVOICE_ITEM ? (item.grandTotal - item.dueAmount)-item.preTdsAmount : item.paidAmount,
            dueAmount: formSource === AppSettings.PR_ITEM_INVOICE_ITEM ? (paymentMode === AppSettings.PAYMENT_METHOD.manual ? item.dueAmount : (eventSource ? item.dueAmount : 0) ) : item.dueAmount,
            //dueAmount: item.dueAmount,
            payingAmount: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.payingAmount : (paymentMode === AppSettings.PAYMENT_METHOD.auto ? item.dueAmount : 0),
            //payingAmount: AppSettings.PR_ITEM_RECENT_ITEM ? item.pay:
            invoiceStatus: item.status,
            paymentItemStatus: null,
            tdsPercentage: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.tdsPercentage : 0,
            tdsAmount: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.tdsAmount : 0,
            originalDueAmount: item.dueAmount,
            amountAfterTds: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.amountAfterTds : (item.preTdsAmount ? item.totalTaxableAmount-item.preTdsAmount:item.totalTaxableAmount),
            debitAmount: item.debitAmount,
            creditDebitHeaderId: item.creditDebitHeaderId,
            originalDebitAmount: item.originalDebitAmount,
            taxableAmount: formSource === AppSettings.PR_ITEM_RECENT_ITEM ? item.taxableAmount : item.totalTaxableAmount,
            taxAmount:formSource === AppSettings.PR_ITEM_RECENT_ITEM ?item.taxAmount:item.taxAmount,
            preTdsPer:item.preTdsPer,
            preTdsAmount:item.preTdsAmount,
            roundOff:formSource === AppSettings.PR_ITEM_RECENT_ITEM ?item.roundOff:item.roundOffAmount,
        
        }
        return pItem;
    }

    makeItem(payableReceivableItem: PayableReceivableItem): FormGroup {
        return this.fb.group({
            slNo: [payableReceivableItem.slNo],
            id: [payableReceivableItem.id],
            payableReceivableHeaderId: [payableReceivableItem.payableReceivableHeaderId],
            invoiceHeaderId: [payableReceivableItem.invoiceHeaderId],
            invoiceNumber: [payableReceivableItem.invoiceNumber],
            invoiceDate: [payableReceivableItem.invoiceDate],
            paymentDueDate: [payableReceivableItem.paymentDueDate],
            invoiceAmount: [payableReceivableItem.invoiceAmount],
            paidAmount: [payableReceivableItem.paidAmount],
            dueAmount: [payableReceivableItem.dueAmount],
            payingAmount: [payableReceivableItem.payingAmount, Validators.required],
            invoiceStatus: [payableReceivableItem.invoiceStatus],
            paymentItemStatus: [payableReceivableItem.paymentItemStatus],
            tdsPercentage: [payableReceivableItem.tdsPercentage],
            tdsAmount: [payableReceivableItem.tdsAmount],
            originalDueAmount: [payableReceivableItem.originalDueAmount],
            amountAfterTds: [payableReceivableItem.amountAfterTds],
            debitAmount: [payableReceivableItem.debitAmount],
            creditDebitHeaderId: [payableReceivableItem.creditDebitHeaderId],
            originalDebitAmount: [payableReceivableItem.originalDebitAmount],
            taxableAmount: [payableReceivableItem.taxableAmount],
            taxAmount:[payableReceivableItem.taxAmount],
            preTdsPer:[payableReceivableItem.preTdsPer],
            preTdsAmount:[payableReceivableItem.preTdsAmount],
            roundOff:[payableReceivableItem.roundOff]
        });
    }

}