import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { GrnHeader, RecentGrn, GrnWrapper } from '../data-model/grn-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';

@Injectable()
export class GrnService {

    apiGetAllUrl : string ;
    apiPostUrl : string;
    apiGetUrl : string;
    apiGetGrnsForParty : string;
    apiGetForGrnNumberUrl : string;
    grnNumberCheckApiUrl : string;
    apiDeleteUrl: string;
    recentGrnApiUrl: string;
    GrnNumberApiUrl: string;
    private headers = new Headers({'Content-Type': 'application/json'});
    
        constructor(private http : HttpClient){
            this.apiGetAllUrl = environment.baseServiceUrl + environment.grnApiGetAllUrl;//'http://localhost:8081/api/country';
            this.apiPostUrl = environment.baseServiceUrl + environment.grnApiPostUrl;//'http://localhost:8081/api/country';
            this.apiGetUrl = environment.baseServiceUrl + environment.grnApiGetUrl; 
            this.apiGetGrnsForParty = environment.baseServiceUrl + environment.grnApiGetForPartyUrl;          
            this.apiGetForGrnNumberUrl = environment.baseServiceUrl + environment.grnApiGetForGrnNumberUrl;
            this.grnNumberCheckApiUrl = environment.baseServiceUrl + environment.grnNumberCheckApiUrl;
            this.apiDeleteUrl = environment.baseServiceUrl + environment.grnApiGetUrl;
            this.recentGrnApiUrl = environment.baseServiceUrl + environment.recentGrnApiUrl;
            // this.GrnNumberApiUrl = environment.baseServiceUrl + environment.GrnNumberApiUrl
        }
    

    getAllGrnss() : Observable<GrnHeader[]>{

        return this.http.get<GrnHeader[]>(this.apiGetAllUrl)
                        // .map(response => <GrnHeader[]>response.json());
    }

    getGrn(headerGrnId: string) : Observable<GrnHeader>{
        
        return this.http.get<GrnHeader>(this.apiGetUrl+"/"+headerGrnId)
                        // .map(response => <GrnHeader>response.json());

    }

    getGrnForAParty(partyId : number) : Observable<GrnHeader[]>{

        return this.http.get<GrnHeader[]>(this.apiGetGrnsForParty+"/"+partyId)
                        // .map(response => <GrnHeader[]>response.json());
    }

    getGrnByGrnNumber(grnNumber : string) : Observable<GrnHeader[]>{
        return this.http.get<GrnHeader[]>(this.apiGetForGrnNumberUrl+"/"+grnNumber)
                        // .map(response => <GrnHeader[]>response.json());
    }

    create(grnWrapper : GrnWrapper) : Observable<GrnHeader>{
        
        //console.log("In create...");
        return this.http.post<GrnHeader>(this.apiPostUrl, grnWrapper)
                        // .map(response => <GrnHeader>response.json());
    }    

    delete(id : string) : Observable<TransactionResponse>{
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl+"/"+id )
            // .map(response => <TransactionResponse>response.json());
    }


    checkGrnNumAvailability(grnNumber: string, partyId: number,id: string): Observable<TransactionResponse> {
        //console.log("Grnnumber in serice", grnNumber);

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("grnNumber", grnNumber);
        params = params.append("id", id ? id :'');
        
       return this.http.get<TransactionResponse>(this.grnNumberCheckApiUrl + "/" + partyId, {params: params})

    }

    getGrnsForParty(partyId: number, grnType: string ,transactionTypeName: string): Observable<GrnHeader[]> {
        return this.http.get<GrnHeader[]>(this.apiGetGrnsForParty + "/" + partyId + "/" + grnType + "/" + transactionTypeName)
        // .map(response => response.json());
    
    }
    getRecentGrns(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentGrn> {
        sortColumn = sortColumn ? sortColumn : 'grnDate';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        //console.log("filterText: ", filterText);

        return this.http
            .get<RecentGrn>(this.recentGrnApiUrl + "/" + transactionTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }
}