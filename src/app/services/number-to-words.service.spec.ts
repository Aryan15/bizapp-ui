import { TestBed, inject } from '@angular/core/testing';

import { NumberToWordsService } from './number-to-words.service';

describe('NumberToWordsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NumberToWordsService]
    });
  });

  it('should be created', inject([NumberToWordsService], (service: NumberToWordsService) => {
    expect(service).toBeTruthy();
  }));
});
