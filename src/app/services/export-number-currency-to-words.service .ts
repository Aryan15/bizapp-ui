import { Injectable } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Currency } from '../data-model/currency-model';
import { CurrencyService } from './currency.service';
import { PartyService } from './party.service';

@Injectable()
export class ExportNumberToWordsService {

  currency:Currency;
    currencySymbol:any;
    currencyName:any;
    currencyRate:any;
    
    public currencys: Currency[] = [];
    
       currencyFraction:string;
      currencyDecimal:string;
       currId:string;
  constructor(
    private currencyService: CurrencyService,
  ) {
    this.getCurrencys(); 
   }

  ngOnInit() {

    this.getCurrencys();  
}

  exportNmber2text(value: number,currencyId:number): string {
  
   this.getCurrencyData(currencyId);
    if(value==null){
      
      return null;
    }
    else{
    var fraction: number = Math.round(this.frac(value) * 100);
    var f_text: string = "";


    if (fraction > 0) {
      f_text = " " + this.convert_number(fraction) + " "+this.currencyDecimal+" ";
    }

    if(value<2){
      return this.convert_number(value) +" "+this.currencyFraction.substring(0,this.currencyFraction.length-1)+" "+ f_text;
    }
    else{
      return this.convert_number(value) + " "+this.currencyFraction+" "+ f_text;
    }
    
  }
  }

  private frac(f: number): number {
    return f % 1;
  }

  private convert_number(number: number): string {
    if ((number < 0) || (number > 999999999)) {
      return "NUMBER OUT OF RANGE!";
    }
    var Gn = Math.floor(number / 10000000);  /* Billion */
    number -= Gn * 10000000;
    var kn = Math.floor(number / 1000000);     /* Million */
    number -= kn * 1000000;
    var Hn = Math.floor(number / 1000);      /* thousand */
    number -= Hn * 1000;
    var Dn = Math.floor(number / 100);       /* Tens (deca) */
    number = number % 100;               /* Ones */
    var tn = Math.floor(number / 10);
    var one = Math.floor(number % 10);
    var res = "";

    if (Gn > 0) {
      res += (this.convert_number(Gn) + " BILLION");
    }
    if (kn > 0) {
      res += (((res == "") ? "" : " ") +
        this.convert_number(kn) + " MILLION");
    }
    if (Hn > 0) {
      res += (((res == "") ? "" : " ") +
        this.convert_number(Hn) + " THOUSAND");
    }

    if (Dn) {
      res += (((res == "") ? "" : " ") +
        this.convert_number(Dn) + " HUNDRED");
    }


    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
    var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");

    if (tn > 0 || one > 0) {
      if (!(res == "")) {
        res += "  ";
      }
      if (tn < 2) {
        res += ones[tn * 10 + one];
      }
      else {

        res += tens[tn];
        if (one > 0) {
          res += ("-" + ones[one]);
        }
      }
    }

    if (res == "") {
      res = "zero";
    }
    return res;
  }


  getCurrencys() {
   
    this.currencyService.getAllCurrency().subscribe(
        response => {
            this.currencys = response;
            this.currencys.forEach(currencys => {
              this.currencyName=currencys.currencyName;
              this.currencyFraction=currencys.currencyFraction;
              this.currencyDecimal=currencys.currencyDecimal;
              //this.currencySymbol=currencys.currencySymbol;
             // console.log("="+this.currencyFraction)
            
        });
        
      
  
})
  }
  getCurrencyData(currId:number){
          
    console.log(" currId "+currId)
    
    if(+currId!=0)
    {
    this.currency= this.currencys.filter(data => data.id===+currId)[0];
    
    this.currencyFraction=this.currency.currencyFraction;
    this.currencyDecimal=this.currency.currencyDecimal;
    this.currencySymbol = this.currency.currencySymbol;
    this.currencyName=this.currency.currencyName;
    //console.log("currencyFraction = "+this.currencyFraction +"currencyDecimal = "+this.currencyDecimal+"currencySymbol"+this.currencySymbol);
    }
  
  } 
}
