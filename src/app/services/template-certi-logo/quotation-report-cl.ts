import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from "../../data-model/print-copies-model";
import { QuotationHeader, QuotationItem } from '../../data-model/quotation-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { NumberToWordsService } from '../../services/number-to-words.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { CompanyService } from '../company.service';
import { GlobalSettingService } from '../global-setting.service';
import { PrintService } from '../print.service';
declare var jsPDF: any;
//var imgData;
var totalPagesExp = "{total_pages_count_string}";

@Injectable({
    providedIn: 'root'
})
export class QuotationReportCLService {

    quotation: QuotationHeader;
    company: Company;
    imgData: any;
    imageMetadata: ImageMetadata;
    certData: any;
    certMetadata: ImageMetadata;
    globalSetting: GlobalSetting;
    datePipe = new DatePipe('en-US');


    doc = new jsPDF('p', 'pt');
    // totalPagesExp = "{total_pages-count-string}";


    titleText: string = "QUOTATION";
    copyTypeText: string = "ORIGINAL";


    //topMargin: number = 100;

    custTableLastY: number;
    summaryTableStartY: number = 600;//500;
    summaryTableLeftMargin: number = 350;
    signData: any;
    signMetadata: ImageMetadata;

    totalAmount: string;
    discountAmount: string;
    amountAfterDiscount: string;
    cgstAmount: string;
    sgstAmount: string;
    igstAmount: string;
    roundOffAmount: string;
    grandTotal: string;

    isItemLevelTax: boolean = false;


    constructor(@Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService,
        private companyService: CompanyService,
        private printService: PrintService,
        private numberFormatterService: NumberFormatterService,
        private globalSettingService: GlobalSettingService) { }


    ngOnInit() {

        
    }
    // ..............................................................
    download(quotation: QuotationHeader, company: Company, copyText: string, printCopys: PrintCopy[] = [],printHeaderText:string,signatureValue:number) {

        Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
        // //console.log("company[k]..............",company[k]);
       if(printHeaderText){
           this.titleText=printHeaderText;

           }

        // //console.log("in downd...purchaseOrder: ", purchaseOrder);
        this.company = company;
        this.summaryTableStartY = 600;

        // Object.keys(quotation).forEach(k => quotation[k] = quotation[k] === null ? '' : quotation[k])
        Object.keys(quotation).forEach(k => {
            quotation[k] = quotation[k] === null ? '' : quotation[k];
            if (typeof quotation[k] === "number") {
                quotation[k] = quotation[k] === null ? '' : quotation[k].toFixed(2)
              }
        })
        //console.log("quotation, k", quotation)
        quotation.quotationItems.forEach(item => {
            Object.keys(item).forEach(k => {
                ////console.log("item, k", item, k)
                item[k] = item[k] === null ? '' : item[k]
                if (typeof item[k] === "number" && k !== "slNo" && k !== "discountPercentage" && k !== "igstTaxPercentage" && k !== "cgstTaxPercentage" && k !== "sgstTaxPercentage") {
                    item[k] = item[k] === null ? '' : item[k].toFixed(2)
                }
            }
            )
        })


        this.quotation = quotation;
        this.company = company;

        forkJoin(
            // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
            this.companyService.getCompanyLogoAsBase64(1),
            this.companyService.getImageMetadata(),
            this.companyService.getCertImageAsBase64(1),
            this.companyService.getCertMetadata(),
            this.companyService.getSignatureImageAsBase64(1, signatureValue),
            this.companyService.getSignatureMetadata(signatureValue)
          )
          .subscribe(([
                companyLogoBase64, 
                companyLogoMetadata, 
                certImageAsBase64,
                certImageMetadata,
                signatureImageAsBase64,
                signatureImageMetadata]) => {
            //console.log("returned from geetImages...", certImageMetadata )
            this.imgData = companyLogoBase64;
            this.imageMetadata = companyLogoMetadata
            this.certData = certImageAsBase64;
            this.certMetadata = certImageMetadata
            this.signData = signatureImageAsBase64
            this.signMetadata = signatureImageMetadata
          
            this.doc = new jsPDF('p', 'pt');

                    this.globalSettingService.getGlobalSetting()
                    .subscribe(gsResponse => {
                        if (gsResponse.itemLevelTax) {
                            this.isItemLevelTax = true;
                        } else {
                            this.isItemLevelTax = false;
                        }
                        switch (copyText) {
                            case AppSettings.PRINT_ALL:
                                    // printCopys.forEach(pCopy => {
                                    //     this.getCopy(pCopy.name);
                                    // })
                                    printCopys.forEach((value, index) => {

                                        if (value.name != AppSettings.PRINT_ALL) {
                                          this.getCopy(value.name);
                                          if (index != printCopys.length - 2) {
                                            this.doc.addPage();
                                          }
                                        }
                    
                    
                                      })
                                break;
                            default:
                                //console.log("in default: ", copyText);
                                this.getCopy(copyText);
                                break;
                        }
                        
    
    
                        this.printService.writeFooter(this.doc, copyText);
                        // this.openInNewTab();
                        //console.log("before return");
                        // return this.doc.output('datauristring');
                        // this.printService.printData.next(this.doc.output('datauristring'));
                        this.printService.printData.next(this.doc.output('blob'));
                    })

        },
            error => {


            })
        //; )


    }
    // ...........................................
    getCopy(copyT: string) {

        this.copyTypeText = copyT;
        this.fillPage();

    }

    fillPage() {
        //console.log("quotation company", this.company);

        this.printService.topSectionWithCertiImage(this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.certMetadata
            , this.certData
            , this.copyTypeText
            , this.titleText
            , this.quotation
            , this.printService
            , this.quotation.quotationNumber
            , this.quotation.quotationDate);
        // this.doc.rect(8, 5, 580, 805);
        this.doc.setDrawColor(215, 235, 252);
        //if(imgData)  this.doc.addImage(imgData, 'JPEG', 12, 10, 180, 100)
        // this.printService.writeImage(this.imageMetadata, imgData, this.doc);
        //Set Title and copy type
        // this.afterTop = this.doc.autoTable.previous.finalY + 5;
        this.writeCustomerDetails();
        // this.printService.drawLine(this.doc);
        //this.writeSubjectDetails();
        //this.topMargin = this.
        //quotation Items

        if (this.quotation.igstTaxAmount > 0) {
            this.printService.writeTransactionItemsIGST(
                this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.copyTypeText
                , this.titleText
                , this.quotation
                , this.getQuotationItemColumnHeadersIGST()
                , this.getQuotationItemColumnsIGST()
                , this.printService.afterTopCL
                , this.custTableLastY + 10
                , this.quotation.quotationNumber
                , this.quotation.quotationDate
                , "Customer Quotation"
                , this.isItemLevelTax
                , this.certMetadata
                , this.certData
                , true     
            );
        }
        else {
            // this.writeQuotationItems();


            this.printService.writeTransactionItems(this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.copyTypeText
                , this.titleText
                , this.quotation
                , this.getQuotationItemColumnHeaders()
                , this.getQuotationItemColumns()
                , this.printService.afterTopCL
                , this.custTableLastY + 10
                , this.quotation.quotationNumber
                , this.quotation.quotationDate
                , "Customer Quotation"
                , this.isItemLevelTax
                , this.certMetadata
                , this.certData
                , true)

        }
        this.printService.drawLine(this.doc, this.summaryTableStartY - 2);
        // this.printService.drawLine(this.doc);
        //Summary section with Amount in words
        this.writeSummary();

        //Terms and conditions, Signature
        // this.writeTermsAndConditionsAndSignature();


        //Open pdf in a tab
        // this.openInNewTab();
    }



    drawLineVertical(xStart: number, yStart: number, yEnd: number) {
        this.doc.setLineWidth(1.5);
        this.doc.setDrawColor(63, 81, 181); // draw red lines

        this.doc.line(xStart, yStart, xStart, yEnd);
    }

    // writeTitleAndCopyType(copyTypeText: string, titleText: string) {

    //     var rectangleStartX: number = 400;
    //     var rectangleStartY: number = 10;
    //     var rectangleWidth: number = 150;
    //     var rectangleHeight: number = 20;
    //     this.doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

    //     this.doc.setFontSize(12);


    //     var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (this.doc.getStringUnitWidth(copyTypeText) * this.doc.internal.getFontSize() / 2));
    //     var copyTypeTextOffsetY: number = rectangleStartY + this.doc.internal.getFontSize() / 2; //14;
    //     //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
    //     //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
    //     this.doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);

    //     this.doc.setFontSize(20);
    //     this.doc.setTextColor(40);
    //     this.doc.setFontStyle('normal');

    //     var xOffset: number = (this.doc.internal.pageSize.width / 2) - (this.doc.getStringUnitWidth(titleText) * this.doc.internal.getFontSize() / 2);

    //     this.doc.text(titleText, xOffset, rectangleStartY + (copyTypeTextOffsetY / 2) + this.doc.internal.getFontSize() / 2);


    // }

    // openInNewTab() {

        

    //     // this.doc.setProperties({
    //     //     title: "QOT" + this.quotation.quotationNumber
    //     // });

    //     // if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    //     //     window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "QOT.pdf");
    //     // } else {

    //     //     // For other browsers:
    //     //     // Create a link pointing to the ObjectURL containing the blob.
    //     //     this.doc.autoPrint();
    //     //     window.open(
    //     //         this.doc.output("bloburl"),
    //     //         "_blank"
    //     //     );
    //     //     //,"height=650,width=500,scrollbars=yes,location=yes"
    //     //     // For Firefox it is necessary to delay revoking the ObjectURL
    //     //     setTimeout(() => {
    //     //         window.URL.revokeObjectURL(this.doc.output("bloburl"));
    //     //     }, 100);
    //     // }

    // }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(): any[] {

        return [
            {
                "companyName": this.company.name, //"Uthkrushta Technologies",
            }
        ];
    }


    getAddressHeader(): any[] {

        var headerColumns = [

            { title: "Company Address", dataKey: "companyAddress" },
        ];

        return headerColumns;

    }


    getAddress(): any[] {

        return [
            {
                "companyAddress": this.company.address, //"Nagendra Block, BSK 3rd Stage, 29203882038"
            }
        ];
    }

    getGSTHeader(): any[] {
        var headerColumns = [

            { title: "GST", dataKey: "GST" },
        ];

        return headerColumns;
    }

    getGST(): any[] {
        return [
            {

                "GST": this.company.gstNumber, //"XXXX"
            }
        ];
    }

    writeCompanyAndAddress() {

        var startCompanyY: number = 55;
        //var startAddressY: number = 80;

        this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

            startY: startCompanyY,
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'bold',
                fontSize: 12,
            }

        });

        this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });


        this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });

    }




    getCustomerAddressHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {

        let retValue = [
            {
                "columnHead": "Quotation To: ",
                "columnContent": this.quotation.partyName,
            },
            {
                "columnHead": "Address:",
                "columnContent": this.quotation.address,
            },
            {
                "columnHead": "Customer GSTIN:",
                "columnContent": this.quotation.gstNumber,
            }

        ];

        return retValue.filter(r => r.columnContent)
    }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getCustomerDeliveryAddress(): any[] {
        let retValue = [
            {
                "columnHead": "Quot Number:",
                "columnContent": this.quotation.quotationNumber,
            },
            {
                "columnHead": "Quot Date:",
                "columnContent": this.quotation.quotationDate ? this.datePipe.transform(this.quotation.quotationDate, 'dd/MM/yyyy') : null,
            },
            {
                "columnHead": "Enq Number",
                "columnContent": this.quotation.enquiryNumber,
            },
            {
                "columnHead": "Enq Date",
                "columnContent": this.quotation.enquiryDate ? this.datePipe.transform(this.quotation.enquiryDate, 'dd/MM/yyyy') : null,
            }

        ];

        return retValue.filter(r => r.columnContent);

    }

    getSubject(): any[]{
        let retValue = [{
            "columnHead": "Subject: ",
            "columnContent": this.quotation.quotationSubject,
        },
        {
            "columnHead": "Kind attention, ",
            "columnContent": this.quotation.kindAttention,
        }];

        return retValue.filter(r => r.columnContent)
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 10;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.printService.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 80, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 100 }
            }


            /**
             * columnStyles: {
                slNo: { cellWidth: slNoWidth, halign: 'right' },
                // partNumber: { cellWidth: partNumberWidth },
                materialName: { cellWidth: materialNameWidth },
             */
        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //Draw vertical line



        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin + 100, right: this.printService.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 72, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 68 }
            }
        });

        

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getSubject(), {
            startY: this.custTableLastY+10,
            margin: { left: this.printService.leftMargin, right: this.printService.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth:20, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 350 }
            }
        });

        this.custTableLastY = this.doc.autoTable.previous.finalY;
        // this.drawLineVertical(custAddressTableRightMargin, startY, this.custTableLastY);

        // this.doc.setDrawColor(215, 235, 252);
        // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        // this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

    }

    getSubjectHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getSubjectDetails(): any[] {

        return [
            {
                "columnHead": "Customer GSTN",
                "columnContent": this.quotation.gstNumber,
            },


            {
                "columnHead": "Kind Attention ",
                "columnContent": this.quotation.kindAttention,

            },

            {
                "columnHead": "Subject :",
                "columnContent": this.quotation.quotationSubject,
            },


        ];

    }

    writeSubjectDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 5;




        this.doc.autoTable(this.getSubjectHeader(), this.getSubjectDetails(), {

            startY: startY,
            margin: { left: this.printService.leftMargin, right: this.printService.rightMargin },
            // margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 100 }
            }

        });
    }

    getQuotationItemColumnHeaders(): any[] {
        if(this.isItemLevelTax){
            return [
                { title: "SL", dataKey: "slNo" },
                // { title: "Part Number", dataKey: "partNumber" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                // { title: "Unit", dataKey: "uom" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },
                { title: "Disc", dataKey: "discountAmount" },
                { title: "CGST \nAmt", dataKey: "cgstAmount" },
                // { title: "SGST \n%", dataKey: "sgstPercent" },
                { title: "SGST \nAmt", dataKey: "sgstAmount" },
                // {title: "IGST%", dataKey: "igstPercent"},
                // {title: "IGST Amt", dataKey: "igstAmount"},
                { title: "Total", dataKey: "totalAmount" },
            ];
        }else{
            return [
                { title: "SL", dataKey: "slNo" },
                // { title: "Part Number", dataKey: "partNumber" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                // { title: "Unit", dataKey: "uom" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },                
            ];
        }
        
    }

    makeQuotationItem(quo: QuotationItem): any {
        let partNumber = quo.partNumber ?"Part-No: "+ quo.partNumber + "\r\n" : "";
        let specification = quo.specification ? "Spec: " + quo.specification + "\r\n" : "";
        if(this.isItemLevelTax){
            return {
                "slNo": quo.slNo,
                // "partNumber": quo.partNumber,
                // "materialName": quo.partName + "\r\n" + quo.partNumber + "\r\n" + quo.specification + "\r\n",
                "materialName": quo.partName + "\r\n" + partNumber + specification,
                "hsnCode": quo.hsnOrSac,
                "quantity": quo.quantity + "\r\n" + quo.uom,
                // "uom": quo.uom,
                "price": this.numberFormatterService.numberF(quo.price),
                "amount": this.numberFormatterService.numberF(quo.amount),
                "discountAmount": this.numberFormatterService.numberF(quo.discountAmount ? quo.discountAmount : 0) + "\r\n" + "@" + quo.discountPercentage + "%",
                "cgstAmount": this.numberFormatterService.numberF(quo.cgstTaxAmount ? quo.cgstTaxAmount : 0) + "\r\n" + "@" + quo.cgstTaxPercentage + "%",
                // "sgstPercent": this.numberFormatterService.numberF(quo.sgstTaxPercentage ? quo.sgstTaxPercentage : 0),
                "sgstAmount": this.numberFormatterService.numberF(quo.sgstTaxAmount ? quo.sgstTaxAmount : 0) + "\r\n" + "@" + quo.sgstTaxPercentage + "%",
                "totalAmount": this.numberFormatterService.numberF(quo.amountAfterTax ? quo.amountAfterTax : 0)
            }
        }else{
            return {
                "slNo": quo.slNo,
                // "partNumber": quo.partNumber,
                // "materialName": quo.partName + "\r\n" + quo.partNumber + "\r\n" + quo.specification + "\r\n",
                "materialName": quo.partName + "\r\n" + partNumber + specification,
                "hsnCode": quo.hsnOrSac,
                "quantity": quo.quantity + "\r\n" + quo.uom,
                // "uom": quo.uom,
                "price": this.numberFormatterService.numberF(quo.price),
                "amount": this.numberFormatterService.numberF(quo.amount),                
            }
        }
        
    }

    getQuotationItemColumns(): any[] {

        var QuotationItem: any[] = [];
        this.quotation.quotationItems.forEach(quo => {
            //console.log("quo1", quo);
            QuotationItem.push(this.makeQuotationItem(quo));
        });


        return QuotationItem;
    }

    // writeQuotationItems() {

    //     var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    //     var slNoWidth: number = pageWidth * 0.04; //5%
    //     // var partNumberWidth: number = pageWidth * 0.07; //10%
    //     var materialNameWidth: number = pageWidth * 0.24; //30% of total width
    //     var hsnWidth: number = pageWidth * 0.07; //6%
    //     var qtyWidth: number = pageWidth * 0.08; //6%
    //     //var uomWidth: number = pageWidth * 0.05; //6%
    //     var priceWidth: number = pageWidth * 0.09; // 8%
    //     var amountWidth: number = pageWidth * 0.1; //8%
    //     var discountWidth: number = pageWidth * 0.08; //5%
    //     var cgstAmountWidth: number = pageWidth * 0.1; //8%
    //     //var sgstPercentWidth: number = pageWidth * 0.05; //5%
    //     var sgstAmountWidth: number = pageWidth * 0.1; //8%
    //     var totalAmountWidth: number = pageWidth * 0.1; //10%

    //     //console.log('page width: ', this.doc.internal.pageSize.width);

    //     // var pageContent = function (data) {

    //     //     // FOOTER
    //     //     if (this.doc) {
    //     //         var str = "Page " + data.pageCount;
    //     //         if (typeof this.doc.putTotalPages === 'function') {
    //     //             str = str + " of " + this.totalPagesExp;
    //     //         }
    //     //         this.doc.setFontSize(10);
    //     //         this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

    //     //     }
    //     // };

    //     var docl = this.doc;
    //     var leftMarginl = this.leftMargin
    //     var topSectionl = this.topSection;
    //     var printServicel = this.printService;

    //     var companyl = this.company;
    //     var imageMetadatal = this.imageMetadata;
    //     var imgDatal = imgData;
    //     var copyTypeTextl = this.copyTypeText;
    //     var titleTextl = this.titleText;
    //     var quotationl = this.quotation;

    //     var pageContent = function (data) {
    //         // HEADER
    //         // docl.setFontSize(20);
    //         // docl.setTextColor(40);
    //         // docl.setFontStyle('normal');

    //         // docl.text("From Autotable", leftMarginl, 10);
    //         //console.log('data: ', data);
    //         if(data.pageNumber > 1){
    //             topSectionl(docl
    //                 , printServicel
    //                 , companyl
    //                 , imageMetadatal
    //                 , imgDatal
    //                 , copyTypeTextl
    //                 , titleTextl
    //                 , quotationl);
    //         }

    //         // FOOTER
    //         var str = "Page " + data.pageCount;
    //         // Total page number plugin only available in jspdf v1.0+
    //         if (typeof docl.putTotalPages === 'function') {
    //             str = str + " of " + totalPagesExp;
    //         }
    //         docl.setFontSize(10);
    //         docl.text(str, data.settings.margin.left, docl.internal.pageSize.height - 10);
    //     };


    //     this.doc.autoTable(this.getQuotationItemColumnHeaders(), this.getQuotationItemColumns(), {
    //         //addPageContent: pageContent,
    //         // startY: this.custTableLastY + 95,
    //         didDrawPage:pageContent,
    //         startY: this.doc.autoTable.previous.finalY + 5,
    //         margin: { left: this.leftMargin, right: this.rightMargin + 5, top: this.afterTop, bottom: this.bottomMargin },
    //         //theme:'grid',
    //         theme: 'plain',
    //         tableWidth: 'auto',
    //         //tableLineColor: [0,0,0],
    //         //tableLineWidth: 0.5,
    //         styles: {
    //             //cellPadding: 0.5,
    //             fontSize: 8,
    //             lineColor: [0, 0, 0],
    //             fillStyle: 'DF',
    //             //halign: 'right',
    //             overflow: 'linebreak',
    //             overflowColumns: false
    //         },
    //         headStyles: {
    //             fillColor: [215, 235, 252],
    //             lineColor: [0, 0, 0],
    //             fontSize: 9,
    //             textColor: 0,
    //             fontStyle: 'normal',
    //             halign: 'center',
    //         },
    //         columnStyles: {
    //             slNo: { cellWidth: slNoWidth, halign: 'right' },
    //             // partNumber: { cellWidth: partNumberWidth },
    //             materialName: { cellWidth: materialNameWidth },
    //             hsnCode: { cellWidth: hsnWidth },
    //             quantity: { cellWidth: qtyWidth, halign: 'right' },
    //             // uom: { cellWidth: uomWidth },
    //             price: { cellWidth: priceWidth, halign: 'right' },
    //             amount: { cellWidth: amountWidth, halign: 'right' },
    //             discountAmount: { cellWidth: discountWidth, halign: 'right' },
    //             cgstAmount: { cellWidth: cgstAmountWidth, halign: 'right' },
    //             // sgstPercent: { cellWidth: sgstPercentWidth, halign: 'right' },
    //             sgstAmount: { cellWidth: sgstAmountWidth, halign: 'right' },
    //             totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

    //         },

    //         // didDrawPage: function(data) {

    //         //     //console.log('in drawPage: ', data);
    //         //     // data.doc.addPage();
    //         //     super.topSection();

    //         // }
    //         // drawCell: function (cell, data) {
    //         //   //console.log('cell before:: ', cell);
    //         //   if (data.column.index % 2 === 1) {
    //         //     cell.styles.fillColor = "[215, 235, 252]";
    //         //   }
    //         //   //console.log('cell:: ', cell);
    //         // },
    //     });

    //     // this.doc.setLineWidth(1);
    //     // this.doc.setDrawColor(215, 235, 252);
    //     //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

    //     //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
    //     // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
    //     // Total page number plugin only available in jspdf v1.0+
    //     if (typeof this.doc.putTotalPages === 'function') {
    //         this.doc.putTotalPages(totalPagesExp);
    //     }



    // }


    getQuotationItemColumnHeadersIGST(): any[] {
        if (this.isItemLevelTax) {
            return [
                { title: "SL", dataKey: "slNo" },
                // { title: "Part Number", dataKey: "partNumber" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                // { title: "Unit", dataKey: "uom" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },
                { title: "Discount", dataKey: "discountAmount" },
                { title: "IGST \nAmt", dataKey: "igstAmount" },
                // {title: "IGST%", dataKey: "igstPercent"},
                // {title: "IGST Amt", dataKey: "igstAmount"},
                { title: "Total", dataKey: "totalAmount" },
            ];
        }
        else {
            return [
                { title: "SL", dataKey: "slNo" },
                // { title: "Part Number", dataKey: "partNumber" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                // { title: "Unit", dataKey: "uom" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },
            ];
        }
    }

    makeQuotationItemIGST(quo: QuotationItem): any {
        let partNumber = quo.partNumber ? quo.partNumber + "\r\n" : "";
        let specification = quo.specification ? quo.specification + "\r\n" : "";
        if(this.isItemLevelTax){
            return {
                "slNo": quo.slNo,
                // "partNumber": quo.partNumber,
                "materialName": quo.partName + "\r\n" + partNumber + specification,
                "hsnCode": quo.hsnOrSac,
                "quantity": quo.quantity + "\r\n" + quo.uom,
                // "uom": quo.uom,
                "price": this.numberFormatterService.numberF(quo.price),
                "amount": this.numberFormatterService.numberF(quo.amount),
                "discountAmount": this.numberFormatterService.numberF(quo.discountAmount ? quo.discountAmount : 0) + "\r\n" + "@" + quo.discountPercentage + "%",
                "igstAmount": this.numberFormatterService.numberF(quo.igstTaxAmount ? quo.igstTaxAmount : 0) + "\r\n" + "@" + quo.igstTaxPercentage + "%",
                "totalAmount": this.numberFormatterService.numberF(quo.amountAfterTax ? quo.amountAfterTax : 0)
            }
        }else{
            return {
                "slNo": quo.slNo,
                // "partNumber": quo.partNumber,
                "materialName": quo.partName + "\r\n" + partNumber + specification,
                "hsnCode": quo.hsnOrSac,
                "quantity": quo.quantity + "\r\n" + quo.uom,
                // "uom": quo.uom,
                "price": this.numberFormatterService.numberF(quo.price),
                "amount": this.numberFormatterService.numberF(quo.amount),                
            }
        }
        
    }

    getQuotationItemColumnsIGST(): any[] {

        var QuotationItem: any[] = [];
        this.quotation.quotationItems.forEach(quo => {
            QuotationItem.push(this.makeQuotationItemIGST(quo));
        });


        return QuotationItem;
    }



    getSummaryColumnHeaders(): any[] {
        return [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];
    }

    getSummaryColumns(): any[] {


        this.totalAmount = this.numberFormatterService.numberF(this.quotation.subTotalAmount ? this.quotation.subTotalAmount : this.quotation.totalTaxableAmount); //"232392382.00";
        this.discountAmount = this.numberFormatterService.numberF(this.quotation.totalDiscount ? this.quotation.totalDiscount : 0); // "0.00";
        this.amountAfterDiscount = this.numberFormatterService.numberF(this.quotation.totalTaxableAmount); //"232392382.00";
        this.cgstAmount = this.numberFormatterService.numberF(this.quotation.cgstTaxAmount);
        this.sgstAmount = this.numberFormatterService.numberF(this.quotation.sgstTaxAmount);
        this.igstAmount = this.numberFormatterService.numberF(this.quotation.igstTaxAmount);
        this.roundOffAmount = this.numberFormatterService.numberF(this.quotation.roundOffAmount ? this.quotation.roundOffAmount : 0);
        this.grandTotal = this.numberFormatterService.numberF(this.quotation.grandTotal);
        
        let itemDiscPer: number[] = this.quotation.quotationItems.map(item => item.discountPercentage);
        let distinctPer = Array.from( new Set(itemDiscPer))
    
        let printDiscount = 0;
        if(distinctPer.length === 1 || distinctPer.length === 0){
          printDiscount = distinctPer.pop();
        }else{
          printDiscount = this.quotation.discountPercent;
        }
        if(this.quotation.discountPercent > 0){
          printDiscount =  this.quotation.discountPercent;
        }

        let discountAmount: string = "Discount @"+ printDiscount+ "%"


        let sgstHead: string = this.isItemLevelTax ? "SGST" : "SGST @"+this.quotation.sgstTaxRate+" %"
        let cgstHead: string = this.isItemLevelTax ? "CGST" : "CGST @"+this.quotation.cgstTaxRate+" %"
        let igstHead: string = this.isItemLevelTax ? "IGST" : "IGST @"+this.quotation.igstTaxRate+" %"
          

        
        if(this.quotation.inclusiveTax && +this.quotation.inclusiveTax > 0 && this.quotation.totalDiscount > 0){
            this.totalAmount = this.numberFormatterService.numberF(+this.quotation.totalTaxableAmount +  +this.quotation.totalDiscount);
        }

        return [
            {
                "key": "Total Amount",
                "data": this.totalAmount
            },
            {
                "key":  discountAmount,
                "data": this.discountAmount
            },
            {
                "key": "Amount After Discount",
                "data": this.amountAfterDiscount
            },
            {
                "key": cgstHead,
                "data": this.cgstAmount
            },
            {
                "key": sgstHead,
                "data": this.sgstAmount
            },
            {
                "key": igstHead,
                "data": this.igstAmount
            },
            {
                "key": "Round Off",
                "data": this.roundOffAmount
            },
            {
                "key": "GRAND TOTAL",
                "data": this.grandTotal
            },
        ];
    }

    writeSummary() {

        //console.log("this.summaryTableStartY: ", this.summaryTableStartY);
        //console.log("this.doc.autoTable.previous.finalY: ", this.doc.autoTable.previous.finalY);
        if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {

            this.doc.addPage();
            // //Set Title and copy type
            // this.writeTitleAndCopyType(this.copyTypeText, this.titleText);


            // //Company And address in the header 
            // this.writeCompanyAndAddress();


            //Bill number and date
            // this.writeBillNumberAndDate();
            this.printService.topSectionWithCertiImage(this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.certMetadata
                , this.certData
                , this.copyTypeText
                , this.titleText
                , this.quotation
                , this.printService
                , this.quotation.quotationNumber
                , this.quotation.quotationDate);
                this.printService.writeJustTransNo(this.doc, this.quotation.quotationNumber,AppSettings.QUOT_NAME);
                this.printService.writeJustTransDate(this.doc, this.quotation.quotationDate,AppSettings.QUOT_DATE);
            this.summaryTableStartY = 250;

            if (typeof this.doc.putTotalPages === 'function') {
                this.doc.putTotalPages(totalPagesExp);
            }
        }

        this.doc.autoTable(this.getSummaryColumnHeaders(), this.getSummaryColumns(), {
            startY: this.summaryTableStartY,//this.doc.autoTable.previous.finalY + 25,
            margin: { left: this.summaryTableLeftMargin, right: this.printService.rightMargin },
            showHead: 'never',
            theme: 'plain',
            styles: {
                halign: 'right',
                valign: 'bottom',
                fontStyle: 'bold',
                cellPadding: 0,
            },
            columnStyles: {
                // key: { fillColor: [215, 235, 252] }
            },
        });

        var tableHeight: number = this.doc.autoTable.previous.finalY - this.summaryTableStartY;
        this.printService.writeTermsAndConditionAndDeliveryAndPaymentTerms(this.quotation.deliveryTerms, this.quotation.paymentTerms, this.quotation.termsAndConditions, this.numberToWordsService.number2text(this.quotation.grandTotal), this.doc, tableHeight, this.summaryTableStartY, this.summaryTableLeftMargin, this.company.name,this.company, false, this.signMetadata, this.signData, this.printService);



    }


}
