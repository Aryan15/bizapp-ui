import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { InvoiceHeader, InvoiceItem } from '../../data-model/invoice-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from "../../data-model/print-copies-model";
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { NumberToWordsService } from '../../services/number-to-words.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { CompanyService } from '../company.service';
import { GlobalSettingService } from '../global-setting.service';
import { PrintService } from '../print.service';

declare var jsPDF: any;

// var imgData = 'D:/angular project/src/app/services/template2/download.jpg/jpeg;base64,verylongbase64;';
//var imgData;
var totalPagesExp = "{total_pages_count_string}";



@Injectable({
  providedIn: 'root'
})
export class InvoiceReportCLService {

  //constructor() { }


  invoice: InvoiceHeader;
  transactionTypeName: string;
  company: Company;
  imgData: any;
  imageMetadata: ImageMetadata;

  certData: any;
  certMetadata: ImageMetadata;

  signData: any;
  signMetadata: ImageMetadata;

  datePipe = new DatePipe('en-US');

  doc = new jsPDF('p', 'pt');
  totalPagesExp = "{total_pages_count_string}";

  afterTop: number = 20;

  GSTIN: string = "";
  customerLabel: string = "";
  titleText: string = "";
  copyTypeText: string = "ORIGINAL";


  leftMargin: number = 15;
  rightMargin: number = 15;

  custTableLastY: number;
  summaryTableStartY: number = 600;//500;
  summaryTableLeftMargin: number = 350;


  totalAmount: string;
  discountAmount: string;
  amountAfterDiscount: string;
  cgstAmount: string;
  sgstAmount: string;
  igstAmount: string;
  roundOffAmount: string;
  grandTotal: string;
  // globalSetting: GlobalSetting;
  //img = new Image;
  isItemLevelTax: boolean = false;
  tcsAmount:string;
  constructor(@Inject('Window') private window: Window,
    private numberToWordsService: NumberToWordsService,
    private companyService: CompanyService,
    private printService: PrintService,
    private numberFormatterService: NumberFormatterService,
    private globalSettingService: GlobalSettingService) {


  }


  ngOnInit() {


  }


  fillPage() {
    //console.log("invoice............!!!!!", this.invoice);

    this.printService.topSectionWithCertiImage(this.doc
      , this.company
      , this.imageMetadata
      , this.imgData
      , this.certMetadata
      , this.certData
      , this.copyTypeText
      , this.titleText
      , this.invoice
      , this.printService
      , this.invoice.invoiceNumber
      , this.invoice.invoiceDate);


    this.writeCustomerDetails();

    if (this.invoice.igstTaxAmount > 0) {  // this.writeInvoiceItemsIGST();
      // this.printService.writeItemsIGST(this.doc, this.invoice, this.totalPagesExp , this.custTableLastY);
      this.printService.writeTransactionItemsIGST(
        this.doc
        , this.company
        , this.imageMetadata
        , this.imgData
        , this.copyTypeText
        , this.titleText
        , this.invoice
        , this.getInvoiceItemColumnHeadersIGST()
        , this.getInvoiceItemColumnsIGST()
        , this.printService.afterTopCL
        , this.custTableLastY + 10
        , this.invoice.invoiceNumber
        , this.invoice.invoiceDate
        , this.transactionTypeName
        , this.isItemLevelTax
        , this.certMetadata
        , this.certData
        , true
      );
    }
    else {
      this.printService.writeTransactionItems(this.doc
        , this.company
        , this.imageMetadata
        , this.imgData
        , this.copyTypeText
        , this.titleText
        , this.invoice
        , this.getInvoiceItemColumnHeaders()
        , this.getInvoiceItemColumns()
        , this.printService.afterTopCL
        , this.custTableLastY + 10
        , this.invoice.invoiceNumber
        , this.invoice.invoiceDate
        , this.transactionTypeName
        , this.isItemLevelTax
        , this.certMetadata
        , this.certData
        , true)
    }

    this.printService.drawLine(this.doc);
    //Summary section with Amount in words
    this.writeSummary();


  }



  getCopy(copyT: string) {

    this.copyTypeText = copyT;
    this.fillPage();

  }

  download(transactionTypeName: string
    , invoice: InvoiceHeader
    , company: Company
    , copyText: string
    // , globalSetting: GlobalSetting
    , isItemLevelTax: boolean
    , printCopys: PrintCopy[] = []
    , printHeaderText: string
    , signatureValue: number) {

    this.summaryTableStartY = 600;
    // this.globalSetting = globalSetting;
    this.isItemLevelTax = isItemLevelTax;
    Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
    // //console.log("in downd...purchaseOrder: ", purchaseOrder);
    this.company = company;

    Object.keys(invoice).forEach(k => {
      invoice[k] = invoice[k] === null ? '' : invoice[k]
      if (typeof invoice[k] === "number" && k !== "tcsPercentage") {
        invoice[k] = invoice[k] === null ? '' : invoice[k].toFixed(2)
      }
    }
    )

    invoice.invoiceItems.forEach(item => {
      Object.keys(item).forEach(k => {
        ////console.log("item, k", item, k)
        item[k] = item[k] === null ? '' : item[k]
        if (typeof item[k] === "number" && k !== "slNo" && k !== "discountPercentage" && k !== "igstTaxPercentage" && k !== "cgstTaxPercentage" && k !== "sgstTaxPercentage") {
          item[k] = item[k] === null ? '' : item[k].toFixed(2)
        }
      }
      );

      //console.log(' part name: ', item.partName);
      //console.log(' part number: ', item.partNumber);
    })

    this.invoice = invoice;
    this.transactionTypeName = transactionTypeName

    if (printHeaderText) {
      this.titleText = printHeaderText;
    }

    else {
      if (this.transactionTypeName === AppSettings.CUSTOMER_INVOICE) {
        this.titleText = AppSettings.TAX_INVOICE_TEXT;
      } else
        if (this.transactionTypeName === AppSettings.PROFORMA_INVOICE) {
          this.titleText = AppSettings.PROFORMA_INVOICE;
        } else if (this.transactionTypeName === AppSettings.CREDIT_NOTE) {
          this.titleText = AppSettings.CREDIT_NOTE;
        } else if (this.transactionTypeName === AppSettings.DEBIT_NOTE) {
          this.titleText = AppSettings.DEBIT_NOTE;
        } else if (this.transactionTypeName === AppSettings.INCOMING_JOBWORK_INVOICE) {
          this.titleText = AppSettings.TAX_INVOICE_TEXT
        } else if (this.transactionTypeName === AppSettings.OUTGOING_JOBWORK_INVOICE) {
          this.titleText = AppSettings.SUBCONTRACT_INVOICE_TEXT
        }
    }
    //console.log("in downd...copyNumber: ", copyText);

    forkJoin(
      // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
      this.companyService.getCompanyLogoAsBase64(1),
      this.companyService.getImageMetadata(),
      this.companyService.getCertImageAsBase64(1),
      this.companyService.getCertMetadata(),
      this.companyService.getSignatureImageAsBase64(1, signatureValue),
      this.companyService.getSignatureMetadata(signatureValue)
    )
      .subscribe(([
        companyLogoBase64,
        companyLogoMetadata,
        certImageAsBase64,
        certImageMetadata,
        signatureImageAsBase64,
        signatureImageMetadata
      ]) => {
        //console.log("returned from geetImages...", certImageMetadata )
        this.imgData = companyLogoBase64;
        this.imageMetadata = companyLogoMetadata
        this.certData = certImageAsBase64;
        this.certMetadata = certImageMetadata
        this.signData = signatureImageAsBase64
        this.signMetadata = signatureImageMetadata





        this.doc = new jsPDF('p', 'pt');


        // this.globalSettingService.getGlobalSetting()
        // .subscribe(gsResponse => {
        this.isItemLevelTax = this.printService.getIsItemLevelTax(isItemLevelTax, invoice, transactionTypeName);
        switch (copyText) {
          case AppSettings.PRINT_ALL:
            // printCopys.forEach(pCopy => {
            //     this.getCopy(pCopy.name);
            // })
            printCopys.forEach((value, index) => {

              if (value.name != AppSettings.PRINT_ALL) {
                this.getCopy(value.name);
                if (index != printCopys.length - 2) {
                  this.doc.addPage();
                }
              }


            })
            break;
          default:
            //console.log("in default: ", copyText);
            this.getCopy(copyText);
            break;
        }
        // switch (copyNumber) {
        //   case 1:
        //     this.getCopy(AppSettings.PRINT_ORIGINAL);
        //     break;
        //   case 2:
        //     this.getCopy(AppSettings.PRINT_DUPLICATE);
        //     break;
        //   case 3:
        //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
        //     break;
        //   case 4:
        //     this.getCopy(AppSettings.PRINT_ORIGINAL);
        //     this.doc.addPage();
        //     this.getCopy(AppSettings.PRINT_DUPLICATE);
        //     this.doc.addPage();
        //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
        //     break;
        //   default:
        //     this.getCopy(AppSettings.PRINT_ORIGINAL);
        //     break;
        // }



        this.printService.writeFooter(this.doc, copyText);

        // this.openInNewTab();
        this.printService.printData.next(this.doc.output('blob'));


      })

  }


  // getBase64Image(img) {

  //       var canvas = document.createElement("canvas");

  //       canvas.width = img.width;
  //       canvas.height = img.height;
  //       var ctx = canvas.getContext("2d");

  //       ctx.drawImage(img, 0, 0);

  //       var dataURL = canvas.toDataURL("image/jpeg");

  //       return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

  //   }







  // openInNewTab() {

  //   var string = this.doc.output('datauristring');
  //   var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
  //   var x = window.open();
  //   x.document.open();
  //   x.document.write(iframe);
  //   x.document.close();
  //   return this.doc;
  // }

  openInNewTab() {

    this.doc.setProperties({
      title: "INVOICE" + this.invoice.invoiceNumber
    });
    // var dataSrc = this.doc.output('bloburl');
    // // var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
    // // var x = window.open();
    // // x.document.open();
    // // x.document.write(iframe);
    // // x.document.close();
    // // return this.doc;
    // let w= window.open("", "MsgWindow");
    // w.document.write("<html><head><title>INVOICE</title></head><body><embed src=" + 
    // dataSrc + " width='100%' height='100%'></embed></body></html>");

    // w.document.close();
    // this.doc.output('dataurlnewwindow'); 
    // var string = this.doc.output('datauristring');
    // var x = window.open();
    // x.document.open();
    // x.document.location=string;

    // return this.doc;
    // window.open(this.doc.output('bloburl'), '_blank');


    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "INVOICE.pdf");
    } else {

      // For other browsers:
      // Create a link pointing to the ObjectURL containing the blob.
      this.doc.autoPrint();
      window.open(
        this.doc.output("bloburl"),
        "_blank"
      );
      //,"height=650,width=500,scrollbars=yes,location=yes"
      // For Firefox it is necessary to delay revoking the ObjectURL
      setTimeout(() => {
        window.URL.revokeObjectURL(this.doc.output("bloburl"));
      }, 100);
    }


  }

  getCompanyHeader(): any[] {

    var headerColumns = [
      { title: "Company Name", dataKey: "companyName" },
    ];

    return headerColumns;

  }

  getCompany(): any[] {

    return [
      {
        "companyName": this.company.name || " ", //"Uthkrushta Technologies",
      }
    ];
  }


  getAddressHeader(): any[] {

    var headerColumns = [

      { title: "Company Address", dataKey: "companyAddress" },
    ];

    return headerColumns;

  }


  getAddress(): any[] {

    return [
      {

        "companyAddress": this.company.address || " ", //"Nagendra Block, BSK 3rd Stage, 29203882038"
      }
    ];
  }

  getGSTHeader(): any[] {
    var headerColumns = [

      { title: "GST", dataKey: "GST" },
    ];

    return headerColumns;
  }

  getGST(): any[] {
    return [
      {

        "GST": this.company.gstNumber || " ", //"XXXX"
      }
    ];
  }

  getCompanyEmail(): any[] {
    var headerColumns = [

      { title: "Email", dataKey: "email" },
    ];

    return headerColumns;
  }
  getEmail(): any[] {
    return [
      {

        "email": this.company.email || " ", //"XXXX"
      }
    ];
  }
  writeCompanyGST() {
    this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHeader: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });
  }
  writeCompanyAndAddress() {

    var startCompanyY: number = 55;
    //var startAddressY: number = 80;
    this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

      startY: startCompanyY,
      theme: 'plain',
      showHeader: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'bold',
        fontSize: 12,
      }

    });

    this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHeader: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });




    this.doc.autoTable(this.getCompanyEmail(), this.getEmail(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHeader: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });

  }


  getBillHeader(): any[] {

    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

  }
  getBill(): any[] {

    let tranHeader: String = "";
    let tranHeaderDate: String = "";

    if (this.transactionTypeName === AppSettings.CREDIT_NOTE || this.transactionTypeName === AppSettings.DEBIT_NOTE) {
      tranHeader = "Note No:";
      tranHeaderDate = "Note Date:";
    } else {
      tranHeader = "Invoice No:";
      tranHeaderDate = "Invoice Date:";
    }
    let returnArray: any[] = [
      {
        "columnHead": tranHeader,
        "columnContent": this.invoice.invoiceNumber,
      },
      {
        "columnHead": tranHeaderDate,
        "columnContent": this.invoice.invoiceDate ? this.datePipe.transform(this.invoice.invoiceDate, 'dd-MM-yyyy') : null,
      },
      {
        "columnHead": "DC No:",
        "columnContent": this.invoice.deliveryChallanNumber,
      },
      {
        "columnHead": "DC Date:",
        "columnContent": this.invoice.deliveryChallanDateString,
      },
      {
        "columnHead": "PO No :",
        "columnContent": this.invoice.purchaseOrderNumber,
      },
      {
        "columnHead": "PO Date :",
        "columnContent": this.invoice.purchaseOrderDate ? this.datePipe.transform(this.invoice.purchaseOrderDate, 'dd-MM-yyyy') : null,
      },
      {
        "columnHead": "Reference No :",
        "columnContent": this.invoice.internalReferenceNumber,
      },
      {
        "columnHead": "Reference Date :",
        "columnContent": this.invoice.internalReferenceDate,
      },
      {
        "columnHead": "Vehicle No :",
        "columnContent": this.invoice.vehicleNumber,

      },
      {
        "columnHead": "E-Way Bill No :",
        "columnContent": this.invoice.eWayBillNumber,
      },

    ]

    return returnArray.filter(data => data.columnContent);

  }

  getInvBillHeader(): any[] {

    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

  }
  getInvBill(): any[] {
    let retValue = [
      {
        "columnHead": "Note No:",
        "columnContent": this.invoice.invoiceNumber,
      },
      {
        "columnHead": "Note date:",
        "columnContent": this.invoice.invoiceDate ? this.datePipe.transform(this.invoice.invoiceDate, 'dd/MM/yyyy') : null,
      },
      {
        "columnHead": "Invoice No:",
        "columnContent": this.invoice.sourceInvoiceNumber,
      },
      {
        "columnHead": "Invoice date:",
        "columnContent": this.invoice.sourceInvoiceDate ? this.datePipe.transform(this.invoice.sourceInvoiceDate, 'dd/MM/yyyy') : null,
      },
    ];

    return retValue.filter(ret => ret.columnContent)
  }

  getCustomerAddressHeader(): any[] {

    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

  }

  getCustomerAddress(): any[] {
    this.getLable()

    let retValue = [
      {
        "columnHead": this.customerLabel,
        "columnContent": this.invoice.partyName,
      },
      {
        "columnHead": "Address :",
        "columnContent": this.invoice.billToAddress,
      },
      {
        "columnHead": this.GSTIN,
        "columnContent": this.invoice.gstNumber,
      },
    ];

    if (this.transactionTypeName !== AppSettings.CREDIT_NOTE && this.transactionTypeName !== AppSettings.DEBIT_NOTE) {
      retValue.push(
        {
          "columnHead": "Shipping Address:",
          "columnContent": this.invoice.shipToAddress,
        }
      )
    }
    return retValue.filter(v => v.columnContent);

  }

  getLable() {

    if (this.transactionTypeName === AppSettings.CUSTOMER_INVOICE || this.transactionTypeName === AppSettings.CREDIT_NOTE || this.transactionTypeName === AppSettings.PROFORMA_INVOICE) {
      this.GSTIN = "Customer GSTIN:";

    }
    else {
      this.GSTIN = "Supplier GSTIN:";
    }

    if (this.transactionTypeName === AppSettings.CREDIT_NOTE) {
      this.customerLabel = "Note Received From :"
    } else if (this.transactionTypeName === AppSettings.DEBIT_NOTE) {
      this.customerLabel = "Note Issued To:"
    }
    else {
      this.customerLabel = "Bill To :"
    }

  }
  getCustomerDeliveryAddressHeader(): any[] {
    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];
  }

  getCustomerDeliveryAddress(): any[] {
    return [
      {
        "columnHead": "Shipping Address:",
        "columnContent": this.invoice.shipToAddress,
      },


    ];
  }
  // writeBillNumberAndDate() {
  //   var startY: number = this.doc.autoTable.previous.finalY + 25;

  //   this.doc.autoTable(this.getBillHeader(), this.getBill(), {

  //     columnStyles: { billDate: { halign: 'right' } },
  //     startY: startY,
  //     margin: { left: this.leftMargin, right: this.rightMargin },
  //     theme: 'plain',
  //     showHeader: 'never',
  //     styles: {
  //       halign: 'left',
  //       fontSize: 12,
  //       fillColor: [215, 235, 252],
  //     },

  //   });
  // }

  writeCustomerDetails() {
    //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    var startY: number = this.doc.autoTable.previous.finalY + 10;
    var custAddressTableRightMargin: number = 300;



    this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

      startY: startY,
      margin: { left: this.leftMargin, right: custAddressTableRightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false,
        cellPadding: 0
      },
      columnStyles: {
        columnHead: { fontStyle: 'bold', cellWidth: 68, halign: 'left' },
        columnContent: { halign: 'left', cellWidth: 122 }
      },
      didParseCell: function (data) {
        if (data.row.index === 0) {
          data.cell.styles.fontStyle = 'bold';
        }
      }
    });

    var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    if (this.transactionTypeName === AppSettings.CREDIT_NOTE || this.transactionTypeName === AppSettings.DEBIT_NOTE) {
      this.doc.autoTable(this.getInvBillHeader(), this.getInvBill(), {
        startY: startY,
        margin: { left: custAddressTableRightMargin + 100, right: this.rightMargin },
        theme: 'plain',
        showHead: 'never',
        styles: {
          halign: 'left',
          fontSize: 10,
          overflow: 'linebreak',
          overflowColumns: false,
          cellPadding: 0
        },
        columnStyles: {
          columnHead: { fontStyle: 'bold', cellWidth: 50, halign: 'left' },
          columnContent: { halign: 'left', cellWidth: 90}
        },
        //allSectionHooks: true,

      });
    }
    else {
      this.doc.autoTable(this.getBillHeader(), this.getBill(), {
        startY: startY,
        margin: { left: custAddressTableRightMargin + 100, right: this.rightMargin },
        theme: 'plain',
        showHead: 'never',
        styles: {
          halign: 'left',
          fontSize: 10,
          overflow: 'linebreak',
          overflowColumns: false,
          cellPadding: 0
        },
        columnStyles: {
          columnHead: { fontStyle: 'bold', cellWidth: 71, halign: 'left' },
          columnContent: { halign: 'left', cellWidth: 69 }
        },

        allSectionHooks: true,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = 'bold';
          }
        }
      });
    }

    var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    //console.log('startY: ', startY);

    // var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

    this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

    // this.doc.setDrawColor(215, 235, 252);
    // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
    // this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

  }

  getPOHeader(): any[] {
    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];
  }

  getPODetails(): any[] {
    return [
      {
        "columnHead": "PO No :",
        "columnContent": this.invoice.purchaseOrderNumber || " ",
      },
      {
        "columnHead": "PO Date :",
        "columnContent": this.invoice.purchaseOrderDate ? this.datePipe.transform(this.invoice.purchaseOrderDate, 'dd-MM-yyyy') : " ",
      },
      {
        "columnHead": "Vehicle No :",
        "columnContent": this.invoice.vehicleNumber || " ",

      },

      {
        "columnHead": "E-Way Bill No :",
        "columnContent": this.invoice.eWayBillNumber || " ",
      },

    ];
  }

  writeShippingAndPODetails() {
    //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    var startY: number = this.doc.autoTable.previous.finalY + 20;
    var custAddressTableRightMargin: number = 300;

    // this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {

    //   startY: startY,
    //   margin: { left: this.leftMargin, right: custAddressTableRightMargin },
    //   theme: 'plain',
    //   showHead: 'never',
    //   styles: {
    //     halign: 'left',
    //     fontSize: 10,
    //     overflow: 'linebreak',
    //     overflowColumns: false
    //   },
    //   columnStyles: {
    //     columnHead: { fontStyle: 'bold', cellWidth: 100 }
    //   }

    // });

    var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

    this.doc.autoTable(this.getPOHeader(), this.getPODetails(), {
      startY: startY - 20,
      margin: { left: custAddressTableRightMargin, right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      columnStyles: {
        columnHead: { fontStyle: 'bold', cellWidth: 100 }
      }
    });

    var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    //console.log('startY: ', startY);

    var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

    this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

    // this.doc.setDrawColor(215, 235, 252);
    // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
    // this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);


  }



  getInvoiceItemColumnHeaders(): any[] {
    if (this.isItemLevelTax) {
      return [
        { title: "SL", dataKey: "slNo" },
        { title: "Material", dataKey: "materialName" },
        { title: "HSN/ SAC", dataKey: "hsnCode" },
        { title: "Qty", dataKey: "quantity" },
        { title: "Price", dataKey: "price" },
        { title: "Amount", dataKey: "amount" },
        { title: "Disc", dataKey: "discountAmount" },
        { title: "CGST \nAmt", dataKey: "cgstAmount" },
        { title: "SGST \nAmt", dataKey: "sgstAmount" },
        { title: "Total", dataKey: "totalAmount" },
      ];
    } else {
      return [
        { title: "SL", dataKey: "slNo" },
        { title: "Material", dataKey: "materialName" },
        { title: "HSN/ SAC", dataKey: "hsnCode" },
        { title: "Qty", dataKey: "quantity" },
        { title: "Price", dataKey: "price" },
        { title: "Amount", dataKey: "amount" },
      ];
    }

  }

  makeInvoiceItem(inv: InvoiceItem, idx: number): any {
    let partNumber = inv.partNumber ? "Part-No: " + inv.partNumber + "\r\n" : "";
    let specification = inv.specification ? "Spec: " + inv.specification + "\r\n" : "";
    if (this.isItemLevelTax) {
      return {
        "slNo": inv.slNo,
        "materialName": inv.partName + "\r\n" + partNumber + specification,
        "hsnCode": inv.hsnOrSac,
        "quantity": inv.quantity + "\r\n" + inv.uom,
        "price": this.numberFormatterService.numberF(inv.price),
        "amount": this.numberFormatterService.numberF(inv.amount),
        "discountAmount": this.numberFormatterService.numberF(inv.discountAmount ? inv.discountAmount : 0) + "\r\n" + "@" + inv.discountPercentage + "%",
        "cgstAmount": this.numberFormatterService.numberF(inv.cgstTaxAmount ? inv.cgstTaxAmount : 0) + "\r\n" + "@" + inv.cgstTaxPercentage + "%",
        "sgstAmount": this.numberFormatterService.numberF(inv.sgstTaxAmount ? inv.sgstTaxAmount : 0) + "\r\n" + "@" + inv.sgstTaxPercentage + "%",
        "totalAmount": this.numberFormatterService.numberF(inv.amountAfterTax ? inv.amountAfterTax : 0)
      }
    } else {
      return {
        "slNo": inv.slNo,
        "materialName": inv.partName + "\r\n" + partNumber + specification,
        "hsnCode": inv.hsnOrSac,
        "quantity": inv.quantity + "\r\n" + inv.uom,
        "price": this.numberFormatterService.numberF(inv.price),
        "amount": this.numberFormatterService.numberF(inv.amount),
      }
    }

  }
  getInvoiceItemColumns(): any[] {

    var invoiceItem: any[] = [];
    this.invoice.invoiceItems.forEach((inv, idx) => {
      invoiceItem.push(this.makeInvoiceItem(inv, idx));
    });


    return invoiceItem;
  }


  // writeItemsWithoutTax(){
  //   var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
  //   var slNoWidth: number = pageWidth * 0.05; //5%
  //   // var partNumberWidth: number = pageWidth * 0.07; //10%
  //   var materialNameWidth: number = pageWidth * 0.50; //30% of total width
  //   var hsnWidth: number = pageWidth * 0.20; //6%
  //   var qtyWidth: number = pageWidth * 0.10; //6%
  //   var uomWidth: number = pageWidth * 0.05; //6%
  //   var priceWidth: number = pageWidth * 0.10; 

  //   //console.log('page width: ', this.doc.internal.pageSize.width);

  //   var pageContent = function (data) {

  //     // FOOTER
  //     if (this.doc) {
  //       var str = "Page " + data.pageCount;
  //       if (typeof this.doc.putTotalPages === 'function') {
  //         str = str + " of " + this.totalPagesExp;
  //       }
  //       this.doc.setFontSize(10);
  //       this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

  //     }
  //   };


  //   this.doc.autoTable(this.getInvoiceItemColumnWithNoTax(), this.getInvoiceItemColumns(), {
  //     //addPageContent: pageContent,
  //     startY: this.custTableLastY + 5,
  //     margin: { left: this.leftMargin, right: this.rightMargin },
  //     //theme:'grid',
  //     theme: 'grid',
  //     tableWidth: 'auto',
  //     //tableLineColor: [0,0,0],
  //     //tableLineWidth: 0.5,
  //     styles: {
  //       // cellPadding: 0.5,
  //       fontSize: 10,
  //       lineColor: [0, 0, 0],
  //       fillStyle: 'DF',
  //       //halign: 'right',
  //       overflow: 'linebreak',
  //       overflowColumns: false
  //     },
  //     headerStyles: {
  //       fillColor: [215, 235, 252],
  //       lineColor: [0, 0, 0],
  //       fontSize: 10,
  //       textColor: 0,
  //       fontStyle: 'normal',
  //       halign: 'center',
  //     },
  //     columnStyles: {
  //       slNo: { cellWidth: slNoWidth, halign: 'right' },
  //       // partNumber: { cellWidth: partNumberWidth },
  //       materialName: { cellWidth: materialNameWidth },
  //       hsnCode: { cellWidth: hsnWidth },
  //       quantity: { cellWidth: qtyWidth, halign: 'right' },
  //       uom: { cellWidth: uomWidth },
  //       price: { cellWidth: priceWidth, halign: 'right' },        
  //     }
  //     // drawCell: function (cell, data) {
  //     //   //console.log('cell before:: ', cell);
  //     //   if (data.column.index % 2 === 1) {
  //     //     cell.styles.fillColor = "[215, 235, 252]";
  //     //   }
  //     //   //console.log('cell:: ', cell);
  //     // },
  //   });

  //   this.doc.setLineWidth(1);
  //   this.doc.setDrawColor(215, 235, 252);
  //   //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

  //   //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
  //   this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
  //   // Total page number plugin only available in jspdf v1.0+
  //   if (typeof this.doc.putTotalPages === 'function') {
  //     this.doc.putTotalPages(this.totalPagesExp);
  //   }

  // }

  // writeInvoiceItems() {

  //   var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
  //   var slNoWidth: number = pageWidth * 0.04; //5%
  //   // var partNumberWidth: number = pageWidth * 0.07; //10%
  //   var materialNameWidth: number = pageWidth * 0.25; //30% of total width
  //   var hsnWidth: number = pageWidth * 0.07; //6%
  //   var qtyWidth: number = pageWidth * 0.05; //6%
  //   var uomWidth: number = pageWidth * 0.05; //6%
  //   var priceWidth: number = pageWidth * 0.07; // 8%
  //   var amountWidth: number = pageWidth * 0.09; //8%
  //   var cgstPercentWidth: number = pageWidth * 0.05; //5%
  //   var cgstAmountWidth: number = pageWidth * 0.09; //8%
  //   var sgstPercentWidth: number = pageWidth * 0.05; //5%
  //   var sgstAmountWidth: number = pageWidth * 0.09; //8%
  //   var totalAmountWidth: number = pageWidth * 0.1; //10%

  //   //console.log('page width: ', this.doc.internal.pageSize.width);

  //   var pageContent = function (data) {

  //     // FOOTER
  //     if (this.doc) {
  //       var str = "Page " + data.pageCount;
  //       if (typeof this.doc.putTotalPages === 'function') {
  //         str = str + " of " + this.totalPagesExp;
  //       }
  //       this.doc.setFontSize(10);
  //       this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

  //     }
  //   };


  //   this.doc.autoTable(this.getInvoiceItemColumnHeaders(), this.getInvoiceItemColumns(), {
  //     //addPageContent: pageContent,
  //     startY: this.custTableLastY + 5,
  //     margin: { left: this.leftMargin, right: this.rightMargin },
  //     //theme:'grid',
  //     theme: 'grid',
  //     tableWidth: 'auto',
  //     //tableLineColor: [0,0,0],
  //     //tableLineWidth: 0.5,
  //     styles: {
  //       //cellPadding: 0.5,
  //       fontSize: 8,
  //       lineColor: [0, 0, 0],
  //       fillStyle: 'DF',
  //       //halign: 'right',
  //       overflow: 'linebreak',
  //       overflowColumns: false
  //     },
  //     headerStyles: {
  //       fillColor: [215, 235, 252],
  //       lineColor: [0, 0, 0],
  //       fontSize: 9,
  //       textColor: 0,
  //       fontStyle: 'normal',
  //       halign: 'center',
  //     },
  //     columnStyles: {
  //       slNo: { cellWidth: slNoWidth, halign: 'right' },
  //       // partNumber: { cellWidth: partNumberWidth },
  //       materialName: { cellWidth: materialNameWidth },
  //       hsnCode: { cellWidth: hsnWidth },
  //       quantity: { cellWidth: qtyWidth, halign: 'right' },
  //       uom: { cellWidth: uomWidth },
  //       price: { cellWidth: priceWidth, halign: 'right' },
  //       amount: { cellWidth: amountWidth, halign: 'right' },
  //       cgstPercent: { cellWidth: cgstPercentWidth, halign: 'right' },
  //       cgstAmount: { cellWidth: cgstAmountWidth, halign: 'right' },
  //       sgstPercent: { olumnWidth: sgstPercentWidth, halign: 'right' },
  //       sgstAmount: { cellWidth: sgstAmountWidth, halign: 'right' },
  //       totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

  //     }
  //     // drawCell: function (cell, data) {
  //     //   //console.log('cell before:: ', cell);
  //     //   if (data.column.index % 2 === 1) {
  //     //     cell.styles.fillColor = "[215, 235, 252]";
  //     //   }
  //     //   //console.log('cell:: ', cell);
  //     // },
  //   });

  //   this.doc.setLineWidth(1);
  //   this.doc.setDrawColor(215, 235, 252);
  //   //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

  //   //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
  //   this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
  //   // Total page number plugin only available in jspdf v1.0+
  //   if (typeof this.doc.putTotalPages === 'function') {
  //     this.doc.putTotalPages(this.totalPagesExp);
  //   }



  // }


  getInvoiceItemColumnHeadersIGST(): any[] {
    if (this.isItemLevelTax) {
      return [
        { title: "SL", dataKey: "slNo" },
        { title: "Material", dataKey: "materialName" },
        { title: "HSN/ SAC", dataKey: "hsnCode" },
        { title: "Qty", dataKey: "quantity" },
        { title: "Price", dataKey: "price" },
        { title: "Amount", dataKey: "amount" },
        { title: "Discount", dataKey: "discountAmount" },
        { title: "IGST \nAmt", dataKey: "igstAmount" },
        { title: "Total", dataKey: "totalAmount" },
      ];
    } else {
      return [
        { title: "SL", dataKey: "slNo" },
        { title: "Material", dataKey: "materialName" },
        { title: "HSN/ SAC", dataKey: "hsnCode" },
        { title: "Qty", dataKey: "quantity" },
        { title: "Price", dataKey: "price" },
        { title: "Amount", dataKey: "amount" },
      ];
    }

  }

  getInvoiceItemColumnWithNoTax(): any[] {
    return [
      { title: "NO", dataKey: "slNo" },
      // { title: "Part Number", dataKey: "partNumber" },
      { title: "Material", dataKey: "materialName" },
      { title: "HSN/ SAC", dataKey: "hsnCode" },
      { title: "Qty", dataKey: "quantity" },
      { title: "Unit", dataKey: "uom" },
      { title: "Price", dataKey: "price" },
    ];
  }

  makeInvoiceItemIGST(inv: InvoiceItem): any {
    let partNumber = inv.partNumber ? inv.partNumber + "\r\n" : "";
    let specification = inv.specification ? inv.specification + "\r\n" : "";
    if (this.isItemLevelTax) {
      return {
        "slNo": inv.slNo,
        "materialName": inv.partName + "\r\n" + partNumber + specification,
        "hsnCode": inv.hsnOrSac,
        "quantity": inv.quantity + "\r\n" + inv.uom,
        "price": this.numberFormatterService.numberF(inv.price),
        "amount": this.numberFormatterService.numberF(inv.amount),
        "discountAmount": this.numberFormatterService.numberF(inv.discountAmount ? inv.discountAmount : 0) + "\r\n" + "@" + inv.discountPercentage + "%",
        "igstAmount": this.numberFormatterService.numberF(inv.igstTaxAmount ? inv.igstTaxAmount : 0) + "\r\n" + "@" + inv.igstTaxPercentage + "%",
        "totalAmount": this.numberFormatterService.numberF(inv.amountAfterTax ? inv.amountAfterTax : 0)
      }
    } else {
      return {
        "slNo": inv.slNo,
        "materialName": inv.partName + "\r\n" + partNumber + specification,
        "hsnCode": inv.hsnOrSac,
        "quantity": inv.quantity + "\r\n" + inv.uom,
        "price": this.numberFormatterService.numberF(inv.price),
        "amount": this.numberFormatterService.numberF(inv.amount),
      }
    }

  }

  getInvoiceItemColumnsIGST(): any[] {

    var invoiceItem: any[] = [];
    this.invoice.invoiceItems.forEach(inv => {
      invoiceItem.push(this.makeInvoiceItemIGST(inv));
    });


    return invoiceItem;
  }

  // writeInvoiceItemsIGST() {

  //   var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
  //   var slNoWidth: number = pageWidth * 0.05; //5%
  //   // var partNumberWidth: number = pageWidth * 0.07; //10%
  //   var materialNameWidth: number = pageWidth * 0.35; //30% of total width
  //   var hsnWidth: number = pageWidth * 0.07; //6%
  //   var qtyWidth: number = pageWidth * 0.05; //6%
  //   var uomWidth: number = pageWidth * 0.05; //6%
  //   var priceWidth: number = pageWidth * 0.09; // 8%
  //   var amountWidth: number = pageWidth * 0.09; //8%
  //   var igstPercentWidth: number = pageWidth * 0.06; //5%
  //   var igstAmountWidth: number = pageWidth * 0.09; //8%
  //   //var sgstPercentWidth: number = pageWidth * 0.05; //5%
  //   //var sgstAmountWidth: number = pageWidth * 0.09; //8%
  //   var totalAmountWidth: number = pageWidth * 0.1; //10%

  //   //console.log('page width: ', this.doc.internal.pageSize.width);

  //   var pageContent = function (data) {

  //     // FOOTER
  //     if (this.doc) {
  //       var str = "Page " + data.pageCount;
  //       if (typeof this.doc.putTotalPages === 'function') {
  //         str = str + " of " + this.totalPagesExp;
  //       }
  //       this.doc.setFontSize(10);
  //       this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

  //     }
  //   };


  //   this.doc.autoTable(this.getInvoiceItemColumnHeadersIGST(), this.getInvoiceItemColumnsIGST(), {
  //     //addPageContent: pageContent,
  //     startY: this.custTableLastY + 5,
  //     margin: { left: this.leftMargin, right: this.rightMargin },
  //     //theme:'grid',
  //     theme: 'grid',
  //     tableWidth: 'auto',
  //     //tableLineColor: [0,0,0],
  //     //tableLineWidth: 0.5,
  //     styles: {
  //       //cellPadding: 0.5,
  //       fontSize: 8,
  //       lineColor: [0, 0, 0],
  //       fillStyle: 'DF',
  //       //halign: 'right',
  //       overflow: 'linebreak',
  //       overflowColumns: false
  //     },
  //     headerStyles: {
  //       fillColor: [215, 235, 252],
  //       lineColor: [0, 0, 0],
  //       fontSize: 9,
  //       textColor: 0,
  //       fontStyle: 'normal',
  //       halign: 'center',
  //     },
  //     columnStyles: {
  //       slNo: { cellWidth: slNoWidth, halign: 'right' },
  //       // partNumber: { cellWidth: partNumberWidth },
  //       materialName: { cellWidth: materialNameWidth },
  //       hsnCode: { cellWidth: hsnWidth },
  //       quantity: { cellWidth: qtyWidth, halign: 'right' },
  //       uom: { cellWidth: uomWidth },
  //       price: { cellWidth: priceWidth, halign: 'right' },
  //       amount: { cellWidth: amountWidth, halign: 'right' },
  //       igstPercent: { cellWidth: igstPercentWidth, halign: 'right' },
  //       igstAmount: { cellWidth: igstAmountWidth, halign: 'right' },
  //       totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

  //     }
  //     // drawCell: function (cell, data) {
  //     //   //console.log('cell before:: ', cell);
  //     //   if (data.column.index % 2 === 1) {
  //     //     cell.styles.fillColor = "[215, 235, 252]";
  //     //   }
  //     //   //console.log('cell:: ', cell);
  //     // },
  //   });

  //   this.doc.setLineWidth(1);
  //   this.doc.setDrawColor(215, 235, 252);
  //   //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

  //   //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
  // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
  //   // Total page number plugin only available in jspdf v1.0+
  //   if (typeof this.doc.putTotalPages === 'function') {
  //     this.doc.putTotalPages(this.totalPagesExp);
  //   }



  // }

  getSummaryColumnHeaders(): any[] {
    return [
      { title: "dummy", dataKey: "key" },
      { title: "dummy", dataKey: "data" },
    ];
  }

  getSummaryColumns(): any[] {


    //console.log("this.invoice: ", this.invoice);
    // this.totalAmount = this.invoice.netAmount ? this.invoice.netAmount : this.invoice.totalTaxableAmount; //"232392382.00";
    // this.discountAmount = this.invoice.discountAmount ? this.invoice.discountAmount : 0; // "0.00";
    // this.amountAfterDiscount = this.invoice.totalTaxableAmount; //"232392382.00";
    // this.cgstAmount = this.invoice.cgstTaxAmount;
    // this.sgstAmount = this.invoice.sgstTaxAmount;
    // this.igstAmount = this.invoice.igstTaxAmount;
    // this.roundOffAmount = this.invoice.roundOffAmount ? this.invoice.roundOffAmount : this.invoice.grandTotal;
    // this.grandTotal = this.invoice.grandTotal;
    this.totalAmount = this.numberFormatterService.numberF(this.invoice.subTotalAmount ? this.invoice.subTotalAmount : this.invoice.totalTaxableAmount); //"232392382.00";
    this.discountAmount = this.numberFormatterService.numberF(this.invoice.totalDiscount ? this.invoice.totalDiscount : 0); // "0.00";
    this.amountAfterDiscount = this.numberFormatterService.numberF(this.invoice.totalTaxableAmount); //"232392382.00";
    this.cgstAmount = this.numberFormatterService.numberF(this.invoice.cgstTaxAmount);
    this.sgstAmount = this.numberFormatterService.numberF(this.invoice.sgstTaxAmount);
    this.igstAmount = this.numberFormatterService.numberF(this.invoice.igstTaxAmount);
    this.roundOffAmount = this.numberFormatterService.numberF(this.invoice.roundOffAmount ? this.invoice.roundOffAmount : 0);
    this.grandTotal = this.numberFormatterService.numberF(this.invoice.grandTotal);
    this.tcsAmount=this.numberFormatterService.numberF(this.invoice.tcsAmount ? this.invoice.tcsAmount:0);

    let sgstTaxRate: number = this.invoice.sgstTaxRate ? this.invoice.sgstTaxRate : this.invoice.invoiceItems[0].sgstTaxPercentage;
    let cgstTaxRate: number = this.invoice.cgstTaxRate ? this.invoice.cgstTaxRate : this.invoice.invoiceItems[0].cgstTaxPercentage;
    let igstTaxRate: number = this.invoice.igstTaxRate ? this.invoice.igstTaxRate : this.invoice.invoiceItems[0].igstTaxPercentage;
    let tcsTaxRate: number = this.invoice.tcsPercentage ? this.invoice.tcsPercentage :0;
    let itemDiscPer: number[] = this.invoice.invoiceItems.map(item => item.discountPercentage);
    let distinctPer = Array.from(new Set(itemDiscPer))

    let printDiscount = 0;
    if (distinctPer.length === 1 || distinctPer.length === 0) {
      printDiscount = distinctPer.pop();
    } else {
      printDiscount = this.invoice.discountPercent;
    }
    if (this.invoice.discountPercent > 0) {
      printDiscount = this.invoice.discountPercent;
    }
    let discountHead: string = "Discount @" + printDiscount + "%"
    let sgstHead: string = this.isItemLevelTax ? "SGST" : "SGST @" + sgstTaxRate + " %"
    let cgstHead: string = this.isItemLevelTax ? "CGST" : "CGST @" + cgstTaxRate + " %"
    let igstHead: string = this.isItemLevelTax ? "IGST" : "IGST @" + igstTaxRate + " %"
    let tcsHead: string;
    if(this.invoice.tcsPercentage && this.invoice.tcsPercentage!=0){
      tcsHead =  "TCS @"+ +tcsTaxRate + "%"
      }
      else
      {
        
        tcsHead =  "TCS"
      }
    if (this.invoice.inclusiveTax && +this.invoice.inclusiveTax > 0 && this.invoice.totalDiscount > 0) {
      this.totalAmount = this.numberFormatterService.numberF(+this.invoice.totalTaxableAmount + +this.invoice.totalDiscount);
    }
    if(this.invoice.tcsAmount && this.invoice.tcsAmount!=0){
    return [
      {
        "key": "Total Amount",
        "data": this.totalAmount
      },
      {
        "key": discountHead,
        "data": this.discountAmount
      },
      {
        "key": "Amount After Discount",
        "data": this.amountAfterDiscount
      },
      {
        "key": cgstHead,
        "data": this.cgstAmount
      },
      {
        "key": sgstHead,
        "data": this.sgstAmount
      },
      {
        "key": igstHead,
        "data": this.igstAmount
      },
      {
        "key": tcsHead,
        "data": this.tcsAmount
      },
      {
        "key": "Round Off",
        "data": this.roundOffAmount
      },
      {
        "key": "GRAND TOTAL",
        "data": this.grandTotal
      },
    ];
  }
  else{
    return [
      {
        "key": "Total Amount",
        "data": this.totalAmount
      },
      {
        "key": discountHead,
        "data": this.discountAmount
      },
      {
        "key": "Amount After Discount",
        "data": this.amountAfterDiscount
      },
      {
        "key": cgstHead,
        "data": this.cgstAmount
      },
      {
        "key": sgstHead,
        "data": this.sgstAmount
      },
      {
        "key": igstHead,
        "data": this.igstAmount
      },
      
      {
        "key": "Round Off",
        "data": this.roundOffAmount
      },
      {
        "key": "GRAND TOTAL",
        "data": this.grandTotal
      },
    ];

  }

  }

  writeSummary() {

    if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {

      this.doc.addPage();
      // Set Title and copy type
      this.printService.topSectionWithCertiImage(
        this.doc
        , this.company
        , this.imageMetadata
        , this.imgData
        , this.certMetadata
        , this.certData
        , this.copyTypeText
        , this.titleText
        , this.invoice
        , this.printService
        , this.invoice.invoiceNumber
        , this.invoice.invoiceDate
      );

      this.printService.writeJustTransNo(this.doc, this.invoice.invoiceNumber,AppSettings.INV_NAME);
      this.printService.writeJustTransDate(this.doc, this.invoice.invoiceDate,AppSettings.INV_DATE);
      //  Company And address in the header 
      // this.writeCompanyAndAddress();
      this.summaryTableStartY = 250;

      if (typeof this.doc.putTotalPages === 'function') {
        this.doc.putTotalPages(totalPagesExp);
      }

      //  Bill number and date
      //this.writeBillNumberAndDate();
      // this.writeCustomerDetails();
      // this.writeShippingAndPODetails();
    }
    //this.summaryTableStartY =this.doc.autoTable.previous.finalY + 25;
    this.doc.autoTable(this.getSummaryColumnHeaders(), this.getSummaryColumns(), {
      startY: this.summaryTableStartY,// this.doc.autoTable.previous.finalY + 25, // this.summaryTableStartY,//this.doc.autoTable.previous.finalY + 25,
      margin: { left: this.summaryTableLeftMargin, right: this.rightMargin },
      showHead: 'never',
      theme: 'plain',
      styles: {
        halign: 'right',
        valign: 'bottom',
        fontStyle: 'bold',
        cellPadding: 0,
      },
      columnStyles: {
        // key: { fillColor: [215, 235, 252] }
      },

    });

    var tableHeight: number = this.doc.autoTable.previous.finalY - this.summaryTableStartY;

    //console.log("tableHeight ", tableHeight, this.summaryTableStartY);
    //console.log("before calling summary: doc.autoTable.previous.finalY: ",this.doc.autoTable.previous.finalY);
    //this.writeBankDetails(this.summaryTableStartY, this.summaryTableLeftMargin);
    let printBank = true;
    if (this.transactionTypeName === AppSettings.CREDIT_NOTE || this.transactionTypeName === AppSettings.DEBIT_NOTE) {
      printBank = false;
    }

    this.printService.writeTermsAndConditionAndDeliveryAndPaymentTerms(this.invoice.deliveryTerms, this.invoice.paymentTerms, this.invoice.termsAndConditions, this.numberToWordsService.number2text(this.invoice.grandTotal), this.doc, tableHeight, this.summaryTableStartY, this.summaryTableLeftMargin, this.company.name, this.company, printBank, this.signMetadata, this.signData, this.printService);


  }

  getBankHeader() {
    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];
  }

  getBankDetail(): any[] {


    return [
      {
        "columnHead": "Bank Name:",
        "columnContent": "SBI",
      },
      {
        "columnHead": "Branch:",
        "columnContent": "Peenya",
      },
      {
        "columnHead": "Bank A/C:",
        "columnContent": "23232314433232",
      },
      {
        "columnHead": "Bank IFSC",
        "columnContent": "CNR008899",
      },
    ];

  }
  // writeBankDetails(startY: number, leftMargin: number){

  //   let rightMargin: number = 300;
  //   this.doc.autoTable(this.getBankHeader(), this.getBankDetail(), {

  //     startY: startY,
  //     margin: { left: leftMargin, right: rightMargin },
  //     theme: 'plain',
  //     showHead: 'never',
  //     styles: {
  //       halign: 'left',
  //       fontSize: 10,
  //       overflow: 'linebreak',
  //       overflowColumns: false,
  //       cellPadding: 0
  //     },
  //     columnStyles: {
  //       columnHead: { fontStyle: 'bold', cellWidth: 40, halign: 'left' },
  //       columnContent: { halign: 'left', cellWidth: 150 }
  //     }

  //   });

  // }

}