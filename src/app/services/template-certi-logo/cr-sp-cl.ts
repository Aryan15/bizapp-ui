import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PayableReceivableHeader } from '../../data-model/pr-model';
import { PrintCopy } from '../../data-model/print-copies-model';
import { TransactionType } from '../../data-model/transaction-type';
import { CompanyService } from '../company.service';
import { GlobalSettingService } from '../global-setting.service';
import { NumberToWordsService } from '../number-to-words.service';
import { PrintService } from '../print.service';

declare var jsPDF: any;

@Injectable({
  providedIn: 'root'
})
export class CustomerReceiptstAndSupplierPaymentCLReportService {

  payableReceivableHeader: PayableReceivableHeader;
  company: Company;
  transactionType: TransactionType;
  imgData: any
  imageMetadata: ImageMetadata;

  certData: any;
  certMetadata: ImageMetadata;

  // toWards: string = "";
  datePipe = new DatePipe('en-US');

  doc = new jsPDF('p', 'pt');
  totalPagesExp = "{total_pages_count_string}";

  isItemLevelTax: boolean = false;


  titleText: string = "";
  copyTypeText: string = "ORIGINAL";


  leftMargin: number = 15;
  rightMargin: number = 15;

  custTableLastY: number;
  summaryTableStartY: number = 500;
  summaryTableLeftMargin: number = 350;

  constructor(@Inject('Window') private window: Window,
    private numberToWordsService: NumberToWordsService,
    private companyService: CompanyService,
    private printService: PrintService,
    private globalSettingService: GlobalSettingService) {

  }

  ngOnInit() {
  }


  fillPage() {

    this.printService.topSectionWithCertiImage(this.doc
      , this.company
      , this.imageMetadata
      , this.imgData
      , this.certMetadata
      , this.certData
      , this.copyTypeText
      , this.titleText
      , this.payableReceivableHeader
      , this.printService
      , this.payableReceivableHeader.payReferenceNumber
      , this.payableReceivableHeader.payReferenceDate);



    this.writeCustomerDetails();

  }


  getCopy(copyT: string) {

    this.copyTypeText = copyT;
    this.fillPage();

  }

  download(payableReceivableHeader: PayableReceivableHeader, company: Company, copyText: string, transactionType: TransactionType, printCopys: PrintCopy[] = [],printHeaderText:string) {

    Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
    //console.log("in downd...payableReceivableHeader: ", payableReceivableHeader, transactionType);
    this.company = company;
    this.summaryTableStartY = 500;

    Object.keys(payableReceivableHeader).forEach(k => {
      payableReceivableHeader[k] = payableReceivableHeader[k] === null ? '' : payableReceivableHeader[k]
      if (typeof payableReceivableHeader[k] === "number") {
        payableReceivableHeader[k] = payableReceivableHeader[k] === null ? '' : payableReceivableHeader[k].toFixed(2)
      }
    }
    )

    this.payableReceivableHeader = payableReceivableHeader;
    this.transactionType = transactionType;
    if(printHeaderText){
      this.titleText =printHeaderText;
    }
else{
    if (transactionType.name == AppSettings.CUSTOMER_RECEIVABLE) {
      this.titleText = "Payment Reciept";
    }
    else if (transactionType.name == AppSettings.SUPPLIER_PAYABLE) {
      this.titleText = "Payment Advice";
    }
    else {
      this.titleText = "Expenses"
    }
  }
    forkJoin(
      // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
      this.companyService.getCompanyLogoAsBase64(1),
      this.companyService.getImageMetadata(),
      this.companyService.getCertImageAsBase64(1),
      this.companyService.getCertMetadata()
    )
      .subscribe(([
        companyLogoBase64,
        companyLogoMetadata,
        certImageAsBase64,
        certImageMetadata]) => {
        //console.log("returned from geetImages...", certImageMetadata)
        this.imgData = companyLogoBase64;
        this.imageMetadata = companyLogoMetadata
        this.certData = certImageAsBase64;
        this.certMetadata = certImageMetadata

        this.doc = new jsPDF('p', 'pt');

        this.globalSettingService.getGlobalSetting()
          .subscribe(gsResponse => {
            if (gsResponse.itemLevelTax) {
              this.isItemLevelTax = true;
            } else {
              this.isItemLevelTax = false;
            }

            switch (copyText) {
              case AppSettings.PRINT_ALL:
                      // printCopys.forEach(pCopy => {
                      //     this.getCopy(pCopy.name);
                      // })
                      printCopys.forEach((value, index) => {

                        if (value.name != AppSettings.PRINT_ALL) {
                          this.getCopy(value.name);
                          if (index != printCopys.length - 2) {
                            this.doc.addPage();
                          }
                        }
    
    
                      })
                  break;
              default:
                  //console.log("in default: ", copyText);
                  this.getCopy(copyText);
                  break;
          }
            // switch (copyNumber) {
            //   case 1:
            //     this.getCopy(AppSettings.PRINT_ORIGINAL);
            //     break;
            //   case 2:
            //     this.getCopy(AppSettings.PRINT_DUPLICATE);
            //     break;
            //   case 3:
            //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
            //     break;
            //   case 4:
            //     this.getCopy(AppSettings.PRINT_ORIGINAL);
            //     this.doc.addPage();
            //     this.getCopy(AppSettings.PRINT_DUPLICATE);
            //     this.doc.addPage();
            //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
            //     break;
            //   default:
            //     this.getCopy(AppSettings.PRINT_ORIGINAL);
            //     break;
            // }

            // this.openInNewTab();
            this.printService.printData.next(this.doc.output('blob'));

          })
      },
        error => {


        })

  }



  openInNewTab() {
    this.doc.setProperties({
      title: "VOC" + this.payableReceivableHeader.payReferenceNumber
    });

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "VOC.pdf");
    } else {

      // For other browsers:
      // Create a link pointing to the ObjectURL containing the blob.
      this.doc.autoPrint();
      window.open(
        this.doc.output("bloburl"),
        "_blank"
      );
      //,"height=650,width=500,scrollbars=yes,location=yes"
      // For Firefox it is necessary to delay revoking the ObjectURL
      setTimeout(() => {
        window.URL.revokeObjectURL(this.doc.output("bloburl"));
      }, 100);
    }
  }

  getCompanyHeader(): any[] {

    var headerColumns = [
      { title: "Company Name", dataKey: "companyName" },
    ];

    return headerColumns;

  }

  getCompany(): any[] {

    return [
      {
        "companyName": this.company.name || " ", //"Uthkrushta Technologies",
      }
    ];
  }


  getAddressHeader(): any[] {

    var headerColumns = [

      { title: "Company Address", dataKey: "companyAddress" },
    ];

    return headerColumns;

  }


  getAddress(): any[] {

    return [
      {

        "companyAddress": this.company.address || " ", //"Nagendra Block, BSK 3rd Stage, 29203882038"
      }
    ];
  }

  getGSTHeader(): any[] {
    var headerColumns = [

      { title: "GST", dataKey: "GST" },
    ];

    return headerColumns;
  }

  getGST(): any[] {
    return [
      {

        "GST": this.company.gstNumber || " ", //"XXXX"
      }
    ];
  }

  getCompanyEmail(): any[] {
    var headerColumns = [

      { title: "Email", dataKey: "email" },
    ];

    return headerColumns;
  }
  getEmail(): any[] {
    return [
      {

        "email": this.company.email || " ", //"XXXX"
      }
    ];
  }
  writeCompanyGST() {
    this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });
  }
  writeCompanyAndAddress() {

    var startCompanyY: number = 55;
    //var startAddressY: number = 80;
    this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

      startY: startCompanyY,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'bold',
        fontSize: 12,
      }

    });

    this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });




    this.doc.autoTable(this.getCompanyEmail(), this.getEmail(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });

  }


  getCustomerAddressHeader(): any[] {

    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

  }


  getSummaryColumnHeaders(): any[] {
    return [
      { title: "dummy", dataKey: "key" },
      { title: "dummy", dataKey: "data" },
    ];
  }


  getCustomerAddress(): any[] {

    if (this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      return [
        {
          "columnHead": "Reciept No : ",
          "columnContent": this.payableReceivableHeader.payReferenceNumber,
        },
        {
          "columnHead": "Customer Name : ",
          "columnContent": this.payableReceivableHeader.partyName,
        },
        {
          "columnHead": "Payment Date : ",
          "columnContent": this.datePipe.transform(this.payableReceivableHeader.payReferenceDate, 'dd-MM-yyyy')
        },
        {
          "columnHead": "Payment Amount : ",
          "columnContent": this.payableReceivableHeader.paymentAmount
        },
        {
          "columnHead": "Payment Mode : ",
          "columnContent": this.payableReceivableHeader.paymentMode
        },
        {
          "columnHead": "Payment Reference : ",
          "columnContent": this.payableReceivableHeader.paymentDocumentNumber
        },
        {
          "columnHead": "A Sum of Rupees : ",
          "columnContent": this.numberToWordsService.number2text(this.payableReceivableHeader.paymentAmount),
        },

      ];
    }
    else {
      return [
        {
          "columnHead": "Payment No : ",
          "columnContent": this.payableReceivableHeader.payReferenceNumber,
        },
        {
          "columnHead": "Supplier Name : ",
          "columnContent": this.payableReceivableHeader.partyName,
        },
        {
          "columnHead": "Payment Date : ",
          "columnContent": this.datePipe.transform(this.payableReceivableHeader.payReferenceDate, 'dd-MM-yyyy')
        },
        {
          "columnHead": "Payment Amount : ",
          "columnContent": this.payableReceivableHeader.paymentAmount
        },
        {
          "columnHead": "Payment Mode : ",
          "columnContent": this.payableReceivableHeader.paymentMode
        },
        {
          "columnHead": "Payment Reference : ",
          "columnContent": this.payableReceivableHeader.paymentDocumentNumber
        },
        {
          "columnHead": "A Sum of Rupees : ",
          "columnContent": this.numberToWordsService.number2text(this.payableReceivableHeader.paymentAmount),
        },
      ];

    }

  }

  writeCustomerDetails() {
    //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    var startY: number = this.doc.autoTable.previous.finalY + 5;
    var custAddressTableRightMargin: number = 300;



    this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

      startY: startY,
      margin: { left: this.leftMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      columnStyles: {
        columnHead: { fontStyle: 'bold', cellWidth: 100, halign: 'right' }
      }

    });

    var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);



    var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    //console.log('startY: ', startY);

    var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

    this.doc.rect(8, 5, 580, 805);



    let recivedHeader = [
      { title: "key", dataKey: "data" },
    ];

    let recived = [

      {
        "data": "",
      },
      {
        "data": "Received by"
      }

    ];

    let strat = this.doc.autoTable.previous.finalY;

    this.doc.autoTable(recivedHeader, recived, {
      startY: strat + 50,
      margin: { left: (this.leftMargin), right: this.rightMargin + 120 },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'right',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      pageContent: function (row, data) {
        if (row.index === 0) {
          //console.log(data);
          data.doc.setFontStyle('bold');
          data.settings.styles = { fontStyle: 'bold' };
        }
      }

    });


    let authorHeader = [
      { title: "key", dataKey: "data" },
    ];

    let author = [

      {
        "data": "",
      },
      {
        "data": "Authorised by"
      }

    ];


    this.doc.autoTable(authorHeader, author, {
      startY: strat + 50,
      margin: { left: (this.leftMargin + 30), right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      pageContent: function (row, data) {
        if (row.index === 0) {
          //console.log(data);
          data.doc.setFontStyle('bold');
          data.settings.styles = { fontStyle: 'bold' };
        }
      }


    });

    this.doc.setDrawColor(215, 235, 252);
    //console.log("Parameterr" + this.leftMargin, startY, this.doc.autoTable.previous.finalY, tableHeight);
    // if (this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
    //   this.doc.rect(this.leftMargin, startY, this.doc.autoTable.previous.finalY + 200, tableHeight + 100);
    // }
    // else {
    //   this.doc.rect(this.leftMargin, startY, this.doc.autoTable.previous.finalY + 220, tableHeight + 140);
    // }
    this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin), tableHeight + 100);


  }



}
