import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { GrnHeader, GrnItem } from '../../data-model/grn-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from "../../data-model/print-copies-model";
import { NumberToWordsService } from '../../services/number-to-words.service';
import { CompanyService } from '../company.service';
import { PrintService } from '../print.service';

declare var jsPDF: any;
//var imgData;
var totalPagesExp = "{total_pages_count_string}";

@Injectable({
    providedIn: 'root'
})
export class GrnReportCLService {

    grn : GrnHeader;
    company: Company;

    // shippingAddress : string = "";
    imgData: any;
    imageMetadata: ImageMetadata;
    certData: any;
    certMetadata: ImageMetadata;
    datePipe = new DatePipe('en-US');

    doc = new jsPDF('p', 'pt');
 
    titleText: string = "GRN";
    copyTypeText: string = "ORIGINAL";

    leftMargin: number = 15;
    rightMargin: number = 20;
    afterTop: number = 80;
    bottomMargin: number = 50;

    custTableLastY: number;
    summaryTableStartY: number = 670;
    summaryTableLeftMargin: number = 350;



    constructor( @Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService,
        private printService: PrintService,
        private companyService: CompanyService) { }


    ngOnInit() {

    }

    download(grn: GrnHeader, company: Company, copyText: string, printCopys: PrintCopy[] = [],printHeaderText:string) {
        // this.shippingAddress = billaddress;

        Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
        // //console.log("company[k]..............",company[k]);

        this.company = company;
        this.summaryTableStartY = 670;
if(printHeaderText){
    this.titleText=printHeaderText;
}
        Object.keys(grn).forEach(k => {
            grn[k] = grn[k] === null ? '' : grn[k]
            if (typeof grn[k] === "number") {
                grn[k] = grn[k] === null ? '' : grn[k].toFixed(2)
            }
        })
        //console.log("quotation, k", grn)
        grn.grnItems.forEach(item => {
            Object.keys(item).forEach(k => {
                ////console.log("item, k", item, k)
                item[k] = item[k] === null ? '' : item[k]
                if (typeof item[k] === "number" && k !== "slNo" && k !== "discountPercentage" && k !== "igstTaxPercentage" && k !== "cgstTaxPercentage" && k !== "sgstTaxPercentage") {
                    item[k] = item[k] === null ? '' : item[k].toFixed(2)
                }
            }
            )
        })

        this.grn = grn;
        this.company = company;
        //console.log("height ", this.doc.internal.pageSize.height)
        //console.log("in downd...copyNumber: ", copyText);
        forkJoin(
            // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
            this.companyService.getCompanyLogoAsBase64(1),
            this.companyService.getImageMetadata(),
            this.companyService.getCertImageAsBase64(1),
            this.companyService.getCertMetadata()
        )
            .subscribe(([
                companyLogoBase64,
                companyLogoMetadata,
                certImageAsBase64,
                certImageMetadata]) => {
                //console.log("returned from geetImages...", certImageMetadata)
                this.imgData = companyLogoBase64;
                this.imageMetadata = companyLogoMetadata
                this.certData = certImageAsBase64;
                this.certMetadata = certImageMetadata

                this.doc = new jsPDF('p', 'pt');

                switch (copyText) {
                    case AppSettings.PRINT_ALL:
                            // printCopys.forEach(pCopy => {
                            //     this.getCopy(pCopy.name);
                            // })
                            printCopys.forEach((value, index) => {

                                if (value.name != AppSettings.PRINT_ALL) {
                                  this.getCopy(value.name);
                                  if (index != printCopys.length - 2) {
                                    this.doc.addPage();
                                  }
                                }
            
            
                              })
                        break;
                    default:
                        //console.log("in default: ", copyText);
                        this.getCopy(copyText);
                        break;
                }
                    // switch (copyNumber) {
                    //     case 1:
                    //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                    //         break;
                    //     case 2:
                    //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                    //         break;
                    //     case 3:
                    //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                    //         break;
                    //     case 4:
                    //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                    //         this.doc.addPage();
                    //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                    //         this.doc.addPage();
                    //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                    //         break;
                    //     default:
                    //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                    //         break;
                    // }


                    this.printService.writeFooter(this.doc, copyText);

                    // this.openInNewTab();
                    this.printService.printData.next(this.doc.output('blob'));
                
        },
            error => {


            })
        //; )


    }
    getCopy(copyT: string) {

        this.copyTypeText = copyT;
        this.fillPage();

    }
    fillPage() {
        //console.log("grn..................................!!!!!",this.grn);

        this.printService.topSectionWithCertiImage(this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.certMetadata
            , this.certData
            , this.copyTypeText
            , this.titleText
            , this.grn
            , this.printService
            , this.grn.grnNumber
            , this.grn.grnDate);

        // this.doc.rect(8, 5, 580, 805);
        this.doc.setDrawColor(215, 235, 252);



        this.writeCustomerDetails();

        this.writeDeliveryItems(
            this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.copyTypeText
            , this.titleText
            , this.grn
            , this.grn.deliveryChallanNumber
            , this.grn.deliveryChallanDate
            , this.certMetadata
            , this.certData
            , true
        );
        this.printService.drawLine(this.doc);
        // this.writeDeliveryFooterItems();
        // this.afterTop = this.doc.autoTable.previous.finalY + 5;

        this.writeSummary();

    }



    openInNewTab() {

        this.doc.setProperties({
            title: "DC"+this.grn.deliveryChallanNumber
          });

          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "DC.pdf");
          } else {
          
            // For other browsers:
            // Create a link pointing to the ObjectURL containing the blob.
            this.doc.autoPrint();
            window.open(
              this.doc.output("bloburl"),
              "_blank"
            );
            //,"height=650,width=500,scrollbars=yes,location=yes"
            // For Firefox it is necessary to delay revoking the ObjectURL
            setTimeout(() => {    
              window.URL.revokeObjectURL(this.doc.output("bloburl"));
            }, 100);
          }
    }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(): any[] {

        return [
            {
                "companyName": this.company.name, //"Uthkrushta Technologies",
            }
        ];
    }


    getAddressHeader(): any[] {

        var headerColumns = [

            { title: "Company Address", dataKey: "companyAddress" },
        ];

        return headerColumns;

    }


    getAddress(): any[] {

        return [
            {

                "companyAddress": this.company.address, //"Nagendra Block, BSK 3rd Stage, 29203882038"
            }
        ];
    }

    getGSTHeader(): any[] {
        var headerColumns = [

            { title: "GST", dataKey: "GST" },
        ];

        return headerColumns;
    }

    getGST(): any[] {
        return [
            {

                "GST": this.company.gstNumber, //"XXXX"
            }
        ];
    }



    getBillHeader(): any[] {
        var headerColumns = [

            { title: "Bill Number", dataKey: "billNumber" },
            { title: "Date", dataKey: "billDate" },

        ];

        return headerColumns;
    }

    getBill(): any[] {
        return [
            {

                "billNumber": "Bill Number: " + this.grn.deliveryChallanNumber,
                "billDate": "Date: " + this.datePipe.transform(this.grn.deliveryChallanDate, 'dd-MM-yyyy'),

            }
        ];
    }


    getCustomerAddressHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {

        return [
            {
                "columnHead": "Supplier Name",
                "columnContent":  this.grn.supplierName,
            },
            {
                "columnHead": "Supplier Address :",
                "columnContent": this.grn.supplierAddress,
            },
            {
                "columnHead": "Supplier GSTN :",
                "columnContent": this.grn.supplierGSTN,
            },

        ];

    }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getCustomerDeliveryAddress(): any[] {

        return [
            {
                "columnHead": "GRN No :",
                "columnContent": this.grn.grnNumber,
            },
            {
                "columnHead": "GRN Date :",
                "columnContent": this.datePipe.transform(this.grn.grnDate, 'dd/MM/yyyy'),
    
            },
            {
                "columnHead": "DC No :",
                "columnContent": this.grn.deliveryChallanNumber,
            },
            {
                "columnHead": "DC Date :",
                "columnContent": this.datePipe.transform(this.grn.deliveryChallanDate, 'dd/MM/yyyy'),
    
            },

        ];
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 10;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 68, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 122 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin + 100, right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 70, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 70 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

    }

    getEwayBillHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }


    getVehicleHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getVehicleDetails(): any[] {
        return [


            {
                "columnHead": "Mode of dispatch :",
                "columnContent": "",
                // "columnContent": this.deliveryChallan.modeOfDispatch,
            },
            {
                "columnHead": "Dispatch date :",
                "columnContent": "",
                // "columnContent": this.deliveryChallan.dispatchDate,
            },
            {
                "columnHead": "Dispatch time :",
                "columnContent": "",
                // "columnContent": this.deliveryChallan.dispatchTime,
            },
        ];
    }

    writeEwayAndModeOfDispatch() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 5;
        var custAddressTableRightMargin: number = 300;




        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getVehicleHeader(), this.getVehicleDetails(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin, right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 100 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

    }



    getDcItemColumnHeaders(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            // { title: "Part Number", dataKey: "partNumber" },
            { title: "Description of Materials", dataKey: "materialName" },
            { title: "HSN/" + "\r\n" + " SAC", dataKey: "hsnCode" },
            { title: "DC Qty", dataKey: "quantity" },
            { title: "Accepted" + "\r\n" + " Quantity", dataKey: "acceptequantity" },
            { title: "Rejected" + "\r\n" + " Quantity", dataKey: "rejquant" },
            { title: "Remarks", dataKey: "remarks" },
        ];
    }

    makeDcItem(dc: GrnItem): any {
        // let partNumber =  dc.partNumber? dc.partNumber+ "\r\n" : "";
        let specification =dc.materialSpecification ? "Spec: " + dc.materialSpecification+ "\r\n" : "";
            return {
            
            "slNo": dc.slNo,
            "materialName": dc.materialName + "\r\n"  + specification ,
            // "partNumber": dc.partNumber,
            // "materialName": dc.partName + "(" + dc.partNumber + ")",
            "hsnCode": dc.hsnOrSac,
            "quantity": dc.deliveryChallanQuantity,
            "acceptequantity" : dc.acceptedQuantity,
            "rejquant" : dc.rejectedQuantity,
            "uom": dc.uom,
            "remarks": dc.remarks
        }
    }

    getDcItemColumns(): any[] {

        var GrnItem: any[] = [];
        this.grn.grnItems.forEach(dc => {
            GrnItem.push(this.makeDcItem(dc));
        });


        return GrnItem;
    }

    writeDeliveryItems(
        doc: any
        , company: Company
        , imageMetadata: ImageMetadata
        , imgData: string
        , copyTypeText: string
        , titleText: string
        , transaction: any
        , transactionNumber: string
        , transactionDate: string
        , certMetadata: ImageMetadata = null
        , certData: string = null
        , isCertLogo: boolean = false
    ) {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.05; //5%
        var materialNameWidth: number = pageWidth * 0.30; //30% of total width
        var hsnWidth: number = pageWidth * 0.10; //6%
        var qtyWidth: number = pageWidth * 0.10; //6%
        var acptquantWidth: number = pageWidth * 0.10; 
        var rejquantWidth: number = pageWidth * 0.10; 
        var remarks: number = pageWidth * 0.25;

        //console.log('page width: ', this.doc.internal.pageSize.width);

        var docl = doc;
        var leftMarginl = this.leftMargin
        var topSectionl = this.printService.topSectionWithoutCompany;
        var topSectionCL = this.printService.topSectionWithCertiImage;
        var printServicel = this.printService;

        var companyl = company;
        var imageMetadatal = imageMetadata;
        var imgDatal = imgData;
        var copyTypeTextl = copyTypeText;
        var titleTextl = titleText;
        var transactionl = transaction;
        var thisService = this;
        //console.log("before data", pageContent);
        var pageContent = function (data) {

            //console.log('data: ', data);
            if (data.pageNumber > 1) {
                //console.log("INMDNDD");
                if(!isCertLogo){
                topSectionl(docl
                    , companyl
                    , imageMetadatal
                    , imgDatal
                    , copyTypeTextl
                    , titleTextl
                    , transactionl
                    , printServicel
                    , transactionNumber
                    , transactionDate
                ,"");
                }
                else{
                    topSectionCL(
                        docl
                        , companyl
                        , imageMetadatal
                        , imgDatal
                        , certMetadata
                        , certData
                        , copyTypeTextl
                        , titleTextl
                        , transactionl
                        , thisService
                        , transactionNumber
                        , transactionDate
                        , leftMarginl
                      )
                }
            }
        };


        this.doc.autoTable(this.getDcItemColumnHeaders(), this.getDcItemColumns(), {
            didDrawPage: pageContent,
            startY: this.custTableLastY + 10,
            margin: { left: this.leftMargin, right: this.rightMargin + 5, top: this.printService.afterTopCL, bottom: 100 },

            theme: 'plain',
            tableWidth: 'auto',
            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headStyles: {
                fillColor: [63, 81, 181],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: [255, 255, 255],
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                slNo: { cellWidth: slNoWidth, halign: 'center' },
                // partNumber: { cellWidth: partNumberWidth },
                materialName: { cellWidth: materialNameWidth },
                hsnCode: { cellWidth: hsnWidth },
                quantity: { cellWidth: qtyWidth, halign: 'center' },
                acceptequantity: { cellWidth: acptquantWidth, halign: 'center' },
                rejquant: { cellWidth: rejquantWidth, halign: 'center' },
                remarks: { cellWidth: remarks }

            }
        });



    }


    getDcItemFooterHeaders(): any[] {
        return [
            { title: "", dataKey: "dummy1" },
            { title: "", dataKey: "dummy2" },
            { title: "", dataKey: "dummy3" },
            { title: "", dataKey: "columnHead" },
            { title: "", dataKey: "columnContent" },
            { title: "", dataKey: "dummy4" },
        ];
    }

    makeDcFooterItem(): any[] {
        return [


            {
                "dummy1": " ",
                "dummy2": " ",
                "dummy3": " ",
                "columnHead": "Total Quantity",
                "columnContent": "",
                "dummy4": " ",
            },

        ];
    }


    getSummaryColumnHeaders(): any[] {
        return [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];
    }

    getSummaryColumns(): any[] {


        return [{}, {}, {}, {}, {}, {}, {}];
    }


    // .......................................
    writeSummary() {
        //console.log("this.summaryTableStartY: ", this.summaryTableStartY);
        //console.log("this.doc.autoTable.previous.finalY: ", this.doc.autoTable.previous.finalY);
        if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {
            //console.log("in")
            this.doc.addPage();

            this.printService.topSectionWithCertiImage(this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.certMetadata
                , this.certData
                , this.copyTypeText
                , this.titleText
                , this.grn
                , this.printService
                , this.grn.grnNumber
                , this.grn.grnDate);

                this.printService.writeJustTransNo(this.doc, this.grn.grnNumber,AppSettings.GRN_NAME);
            this.printService.writeJustTransDate(this.doc, this.grn.grnDate,AppSettings.GRN_DATE);
              //  this.summaryTableStartY = 250;
                
                            if (typeof this.doc.putTotalPages === 'function') {
                                this.doc.putTotalPages(totalPagesExp);
                            }
        }
        
        this.writeSignatureAndQualityOrStockTester(this.grn.stockCheckedByName, this.grn.qualityCheckedByName);

    }


    

    writeSignatureAndQualityOrStockTester(stockCheckedByName: String, qualityCheckedByName : String){


        let qualityHeader = [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];

        let quality = [
            {
                "key": "Quality Checked By:",
                "data": qualityCheckedByName
            },


        ]
        this.doc.autoTable(qualityHeader, quality, {
            startY: this.summaryTableStartY,
            margin: { left: this.leftMargin, right: this.summaryTableLeftMargin+this.bottomMargin + this.leftMargin},
            showHead: 'never',
            theme: 'plain',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                key: { fontStyle: 'bold', }
            },
        })

  
      let signatureHeader = [
        { title: "key", dataKey: "data" },
      ];
  
      let signature = [
        {
          "data": "For " + this.company.name,
        }
      ];
  
  
      //tableStart += 20;
      // tableStart = doc.autoTable.previous.finalY + 5;
  
      let summaryTableEnd: number = this.doc.autoTable.previous.finalY + 10 ;

      this.doc.autoTable(signatureHeader, signature, {
        startY: summaryTableEnd,
        margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
        theme: 'plain',
        showHead: 'never',
        styles: {
          halign: 'right',
          fontSize: 10,
          overflow: 'linebreak',
          overflowColumns: false
        }, 
      });


      let SignatureHeader = [
        { title: "key", dataKey: "data" },
      ];
  
      let Signature = [
  
        {
          "data": "",
        },
        {
          "data": "Authorised Signatory"
        }
  
      ];
  
      // let strat = doc.autoTable.previous.finalY;
  
      this.doc.autoTable(SignatureHeader, Signature, {
        startY: summaryTableEnd + 40,
        margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
        theme: 'plain',
        showHead: 'never',
        styles: {
          halign: 'right',
          fontSize: 10,
          overflow: 'linebreak',
          overflowColumns: false
        },
      });


      let StockHeader = [
        { title: "dummy", dataKey: "key" },
        { title: "dummy", dataKey: "data" },
    ];

    let stock = [
        {
            "key": "Stock Checked By:",
            "data": stockCheckedByName
        },

    ]
    this.doc.autoTable(StockHeader, stock, {
        startY: summaryTableEnd + 20,
        margin: { left: this.leftMargin, right: this.summaryTableLeftMargin+this.bottomMargin + this.leftMargin},
        showHead: 'never',
        theme: 'plain',
        styles: {
            halign: 'left',
            fontSize: 10,
            overflow: 'linebreak',
            overflowColumns: false
        },
        columnStyles: {
            key: { fontStyle: 'bold', }
        },
    })
    }

}