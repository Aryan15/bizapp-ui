import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { DeliveryChallanHeader, DeliveryChallanItem } from '../../data-model/delivery-challan-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from "../../data-model/print-copies-model";
import { NumberToWordsService } from '../../services/number-to-words.service';
import { CompanyService } from '../company.service';
import { PrintService } from '../print.service';

declare var jsPDF: any;
//var imgData;
var totalPagesExp = "{total_pages_count_string}";

@Injectable({
    providedIn: 'root'
})
export class DeliveryChallanCLReportService {

    deliveryChallan: DeliveryChallanHeader;
    company: Company;

    shippingAddress: string = "";

    imgData: any;
    imageMetadata: ImageMetadata;
    certData: any;
    certMetadata: ImageMetadata;
    datePipe = new DatePipe('en-US');

    doc = new jsPDF('p', 'pt');
    // totalPagesExp = "{total_pages-count-string}";

    titleText: string = "DELIVERY CHALLAN";
    copyTypeText: string = "ORIGINAL";

    leftMargin: number = 15;
    rightMargin: number = 20;
    afterTop: number = 80;
    bottomMargin: number = 50;

    custTableLastY: number;
    summaryTableStartY: number = 500;
    summaryTableLeftMargin: number = 350;
    allowShippingAddress:number;


    constructor(@Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService,
        private printService: PrintService,
        private companyService: CompanyService) { }


    ngOnInit() {

    }

    download(billaddress, deliveryChallan: DeliveryChallanHeader, company: Company, copyText: string, printCopys: PrintCopy[] = [],printHeaderText:string,allowShippingAddress:number) {
        this.shippingAddress = billaddress;
        this.allowShippingAddress=allowShippingAddress;
        Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
        // //console.log("company[k]..............",company[k]);

        this.company = company;
        this.summaryTableStartY = 500;

        if(printHeaderText){
            this.titleText=printHeaderText;
        }

        // Object.keys(deliveryChallan).forEach(k => deliveryChallan[k] = deliveryChallan[k] === null ? '' : deliveryChallan[k])
        Object.keys(deliveryChallan).forEach(k => {
            deliveryChallan[k] = deliveryChallan[k] === null ? '' : deliveryChallan[k];
            if (typeof deliveryChallan[k] === "number") {
                deliveryChallan[k] = deliveryChallan[k] === null ? '' : deliveryChallan[k].toFixed(2)
              }
        })
        //console.log("quotation, k", deliveryChallan)
        deliveryChallan.deliveryChallanItems.forEach(item => {
            Object.keys(item).forEach(k => {
                ////console.log("item, k", item, k)
                item[k] = item[k] === null ? '' : item[k]
                if (typeof item[k] === "number" && k !== "slNo" && k !== "discountPercentage" && k !== "igstTaxPercentage" && k !== "cgstTaxPercentage" && k !== "sgstTaxPercentage") {
                    item[k] = item[k] === null ? '' : item[k].toFixed(2)
                }
            }
            )
        })

        this.deliveryChallan = deliveryChallan;
        this.company = company;

        // this.companyService.getCompanyLogoAsBase64(1).subscribe(response => {
        //     imgData = response;
        //     this.doc = new jsPDF('p', 'pt');
        //     this.doc = new jsPDF('p', 'pt');

        //     this.imageMetadata = response;
        //console.log("in downd...copyText: ", copyText);
        forkJoin(
            // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
            this.companyService.getCompanyLogoAsBase64(1),
            this.companyService.getImageMetadata(),
            this.companyService.getCertImageAsBase64(1),
            this.companyService.getCertMetadata()
        )
            .subscribe(([
                companyLogoBase64,
                companyLogoMetadata,
                certImageAsBase64,
                certImageMetadata]) => {
                //console.log("returned from geetImages...", certImageMetadata)
                this.imgData = companyLogoBase64;
                this.imageMetadata = companyLogoMetadata
                this.certData = certImageAsBase64;
                this.certMetadata = certImageMetadata

                this.doc = new jsPDF('p', 'pt');

                switch (copyText) {
                    case AppSettings.PRINT_ALL:
                            // printCopys.forEach(pCopy => {
                            //     this.getCopy(pCopy.name);
                            // })
                            printCopys.forEach((value, index) => {

                                if (value.name != AppSettings.PRINT_ALL) {
                                  this.getCopy(value.name);
                                  if (index != printCopys.length - 2) {
                                    this.doc.addPage();
                                  }
                                }
            
            
                              })
                        break;
                    default:
                        //console.log("in default: ", copyText);
                        this.getCopy(copyText);
                        break;
                }
                // switch (copyNumber) {
                //     case 1:
                //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                //         break;
                //     case 2:
                //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                //         break;
                //     case 3:
                //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                //         break;
                //     case 4:
                //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                //         this.doc.addPage();
                //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                //         this.doc.addPage();
                //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                //         break;
                //     default:
                //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                //         break;
                // }




                this.printService.writeFooter(this.doc, copyText);

                // this.openInNewTab();
                this.printService.printData.next(this.doc.output('blob'));


            },
                error => {


                })
        //; )


    }
    getCopy(copyT: string) {

        this.copyTypeText = copyT;
        this.fillPage();

    }
    fillPage() {
        // //console.log("deliveryChallan...!!!!!",this.deliveryChallan);

        this.printService.topSectionWithCertiImage(this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.certMetadata
            , this.certData
            , this.copyTypeText
            , this.titleText
            , this.deliveryChallan
            , this.printService
            , this.deliveryChallan.deliveryChallanNumber
            , this.deliveryChallan.deliveryChallanDate);

        // this.doc.rect(8, 5, 580, 805);
        this.doc.setDrawColor(215, 235, 252);
        

        this.writeCustomerDetails();
        
        this.writeDeliveryItems(
            this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.copyTypeText
            , this.titleText
            , this.deliveryChallan
            , this.deliveryChallan.deliveryChallanNumber
            , this.deliveryChallan.deliveryChallanDate
            , this.certMetadata
            , this.certData
            , true
        );
        this.printService.drawLine(this.doc);
        // this.writeDeliveryFooterItems();
        // this.afterTop = this.doc.autoTable.previous.finalY + 5;

        this.writeSummary();
        
    }



    openInNewTab() {


        this.doc.setProperties({
            title: "DC" + this.deliveryChallan.deliveryChallanNumber
        });

        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "DC.pdf");
        } else {

            // For other browsers:
            // Create a link pointing to the ObjectURL containing the blob.
            this.doc.autoPrint();
            window.open(
                this.doc.output("bloburl"),
                "_blank"
            );
            //,"height=650,width=500,scrollbars=yes,location=yes"
            // For Firefox it is necessary to delay revoking the ObjectURL
            setTimeout(() => {
                window.URL.revokeObjectURL(this.doc.output("bloburl"));
            }, 100);
        }
    }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(): any[] {

        return [
            {
                "companyName": this.company.name, //"Uthkrushta Technologies",
            }
        ];
    }


    getAddressHeader(): any[] {

        var headerColumns = [

            { title: "Company Address", dataKey: "companyAddress" },
        ];

        return headerColumns;

    }


    getAddress(): any[] {

        return [
            {

                "companyAddress": this.company.address, //"Nagendra Block, BSK 3rd Stage, 29203882038"
            }
        ];
    }

    getGSTHeader(): any[] {
        var headerColumns = [

            { title: "GST", dataKey: "GST" },
        ];

        return headerColumns;
    }

    getGST(): any[] {
        return [
            {

                "GST": this.company.gstNumber, //"XXXX"
            }
        ];
    }


    // writeCompanyAndAddress() {

    //     var startCompanyY: number = 55;
    //     //var startAddressY: number = 80;

    //     this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

    //         startY: startCompanyY,
    //         theme: 'plain',
    //         showHead: 'never',
    //         styles: {
    //             halign: 'center',
    //             overflow: 'linebreak',
    //             overflowColumns: false,
    //             fontStyle: 'bold',
    //             fontSize: 12,
    //         }

    //     });

    //     this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

    //         startY: this.doc.autoTable.previous.finalY - 5,
    //         theme: 'plain',
    //         showHead: 'never',
    //         styles: {
    //             halign: 'center',
    //             overflow: 'linebreak',
    //             overflowColumns: false,
    //             fontStyle: 'normal',
    //             fontSize: 10,
    //         }
    //     });


    //     this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

    //         startY: this.doc.autoTable.previous.finalY - 5,
    //         theme: 'plain',
    //         showHead: 'never',
    //         styles: {
    //             halign: 'center',
    //             overflow: 'linebreak',
    //             overflowColumns: false,
    //             fontStyle: 'normal',
    //             fontSize: 10,
    //         }
    //     });

    // }


    getBillHeader(): any[] {
        var headerColumns = [

            { title: "Bill Number", dataKey: "billNumber" },
            { title: "Date", dataKey: "billDate" },

        ];

        return headerColumns;
    }

    getBill(): any[] {
        return [
            {

                "billNumber": "Bill Number: " + this.deliveryChallan.deliveryChallanNumber,
                "billDate": "Date: " + this.datePipe.transform(this.deliveryChallan.deliveryChallanDate, 'dd-MM-yyyy'),
                // "qoDate": "Quotation Date: " + this.purchaseOrder.quotationDate,

            }
        ];
    }
    // writeBillNumberAndDate() {
    //     var startY: number = this.doc.autoTable.previous.finalY + 25;

    //     this.doc.autoTable(this.getBillHeader(), this.getBill(), {

    //         columnStyles: { billDate: { halign: 'right' } },
    //         startY: startY,
    //         margin: { left: this.leftMargin, right: this.rightMargin },
    //         theme: 'plain',
    //         showHead: 'never',
    //         styles: {
    //             halign: 'left',
    //             fontSize: 12,
    //             fillColor: [215, 235, 252],
    //         },

    //     });
    // }

    getCustomerAddressHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {

        let retValue = [
            {
                "columnHead": "Bill To :",
                "columnContent": this.deliveryChallan.partyName,
            },
            {
                "columnHead": "Address :",
                "columnContent": this.deliveryChallan.billToAddress,
            },
            {
                "columnHead": "Customer GSTIN :",
                "columnContent": this.deliveryChallan.gstNumber,
            },
        
            {
                "columnHead": "E-way Bill :",
                "columnContent": this.deliveryChallan.eWayBill,
            },

            {
                "columnHead": "Vehicle No :",
                "columnContent": this.deliveryChallan.vehicleNumber,

            },

            {
                "columnHead": "No of packages :",
                "columnContent": this.deliveryChallan.numberOfPackages,
            }

        ];

        if(this.allowShippingAddress===1){
            retValue.push(
                {
            
                "columnHead": "Shipping Address:",
                "columnContent": this.deliveryChallan.shipToAddress ? this.deliveryChallan.shipToAddress : this.deliveryChallan.billToAddress,
            }
        )
        }

        return retValue.filter(ret => ret.columnContent);

    }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getCustomerDeliveryAddress(): any[] {
        let retValue = [
            {
                "columnHead": "DC No :",
                "columnContent": this.deliveryChallan.deliveryChallanNumber,
            },
            {
                "columnHead": "DC Date :",
                "columnContent": this.datePipe.transform(this.deliveryChallan.deliveryChallanDate, 'dd-MM-yyyy'),
            },
            {
                "columnHead": "PO NO :",
                "columnContent": this.deliveryChallan.purchaseOrderNumber,
            },
            {
                "columnHead": "PO Date :",
                "columnContent": this.datePipe.transform(this.deliveryChallan.purchaseOrderDate, 'dd-MM-yyyy')
            },
            {
                "columnHead": "Mode of dispatch :",
                "columnContent": this.deliveryChallan.modeOfDispatch,
            },
            {
                "columnHead": "Dispatch   date :",
                "columnContent": this.datePipe.transform(this.deliveryChallan.dispatchDate, 'dd-MM-yyyy')
            },
            {
                "columnHead": "Dispatch   time :",
                "columnContent": this.deliveryChallan.dispatchTime,
            }

        ];

        return retValue.filter(ret => ret.columnContent);
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 10;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 67, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 123 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin + 100, right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 50, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 90 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        // this.doc.setDrawColor(215, 235, 252);
        // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        // this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

    }

    getEwayBillHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getEwayBillDetails(): any[] {

        return [
            {
                "columnHead": "E-way Bill :",
                "columnContent": this.deliveryChallan.eWayBill,
            },


            {
                "columnHead": "Vehicle No :",
                "columnContent": this.deliveryChallan.vehicleNumber,

            },

            {
                "columnHead": "No of packages :",
                "columnContent": this.deliveryChallan.numberOfPackages,
            },


        ];

    }

    getVehicleHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getVehicleDetails(): any[] {
        return [


            {
                "columnHead": "Mode of dispatch :",
                "columnContent": this.deliveryChallan.modeOfDispatch,
            },
            {
                "columnHead": "Dispatch date :",
                "columnContent": this.deliveryChallan.dispatchDate,
            },
            {
                "columnHead": "Dispatch time :",
                "columnContent": this.deliveryChallan.dispatchTime,
            },
        ];
    }

    writeEwayAndModeOfDispatch() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 5;
        var custAddressTableRightMargin: number = 300;



        // this.doc.autoTable(this.getEwayBillHeader(), this.getEwayBillDetails(), {

        //     startY: startY,
        //     margin: { left: this.leftMargin, right: custAddressTableRightMargin },
        //     theme: 'plain',
        //     showHead: 'never',
        //     styles: {
        //         halign: 'left',
        //         fontSize: 10,
        //         overflow: 'linebreak',
        //         overflowColumns: false
        //     },
        //     columnStyles: {
        //         columnHead: { fontStyle: 'bold', cellWidth: 100 }
        //     }

        // });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getVehicleHeader(), this.getVehicleDetails(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin, right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 100 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        // this.doc.setDrawColor(215, 235, 252);
        // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        // this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);


    }



    getDcItemColumnHeaders(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            // { title: "Part Number", dataKey: "partNumber" },
            { title: "Description of Materials", dataKey: "materialName" },
            { title: "HSN/" + "\r\n" + " SAC", dataKey: "hsnCode" },
            { title: "Qty", dataKey: "quantity" },
            { title: "Remarks", dataKey: "remarks" },
        ];
    }

    makeDcItem(dc: DeliveryChallanItem): any {
        console.log("dc item: ", dc);
        let partNumber = dc.partNumber ?"Part-No: " + dc.partNumber + "\r\n" : "";
        let specification = dc.specification ?"Spec: " + dc.specification + "\r\n" : "";
        let dcDate = dc.sourceDeliveryChallanDate ? " DC Date." + this.datePipe.transform(dc.sourceDeliveryChallanDate, 'dd/MM/yyyy') : "";
        let dcNumbers = dc.sourceDeliveryChallanNumber ? "DC No." + dc.sourceDeliveryChallanNumber : "";
        let processNames = dc.processName ? "Process: "+dc.processName+ "\r\n" : "";
        return {

            "slNo": dc.slNo,
            "materialName": dc.partName + "\r\n" + partNumber + specification + processNames + dcNumbers + dcDate,
            // "partNumber": dc.partNumber,
            // "materialName": dc.partName + "(" + dc.partNumber + ")",
            "hsnCode": dc.hsnOrSac,
            "quantity": dc.quantity+ "\r\n" +dc.uom,
            "uom": dc.uom,
            "remarks": dc.remarks
        }
    }

    getDcItemColumns(): any[] {

        var DeliveryChallanItem: any[] = [];
        this.deliveryChallan.deliveryChallanItems.forEach(dc => {
            DeliveryChallanItem.push(this.makeDcItem(dc));
        });


        return DeliveryChallanItem;
    }

    writeDeliveryItems(
        doc: any
        , company: Company
        , imageMetadata: ImageMetadata
        , imgData: string
        , copyTypeText: string
        , titleText: string
        , transaction: any
        , transactionNumber: string
        , transactionDate: string
        , certMetadata: ImageMetadata = null
        , certData: string = null
        , isCertLogo: boolean = false
    ) {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.05; //5%
        // var partNumberWidth: number = pageWidth * 0.10; //10%
        var materialNameWidth: number = pageWidth * 0.34; //30% of total width
        var hsnWidth: number = pageWidth * 0.10; //6%
        var qtyWidth: number = pageWidth * 0.15; //6%
        var remarks: number = pageWidth * 0.36;

        // var headerLineColor = [215, 235, 252];
        //console.log('page width: ', this.doc.internal.pageSize.width);
        // var thisService = this;
        // var docl = doc;
        // var topSectionl = this.printService.topSectionWithoutCompany;

        var docl = doc;
        var leftMarginl = this.leftMargin
        var topSectionl = this.printService.topSectionWithoutCompany;
        var topSectionCL = this.printService.topSectionWithCertiImage;
        var printServicel = this.printService;

        var companyl = company;
        var imageMetadatal = imageMetadata;
        var imgDatal = imgData;
        var copyTypeTextl = copyTypeText;
        var titleTextl = titleText;
        var transactionl = transaction;
        //var thisService = this;
        //console.log("before data", pageContent);
        var pageContent = function (data) {
            // HEADER
            // docl.setFontSize(20);
            // docl.setTextColor(40);
            // docl.setFontStyle('normal');

            // docl.text("From Autotable", leftMarginl, 10);
            //console.log('data: ', data);
            if (data.pageNumber > 1) {
                //console.log("INMDNDD");
                if(!isCertLogo){
                topSectionl(docl
                    , companyl
                    , imageMetadatal
                    , imgDatal
                    , copyTypeTextl
                    , titleTextl
                    , transactionl
                    , printServicel
                    , transactionNumber
                    , transactionDate
                      ,"");
                }else{
                    topSectionCL(
                        docl
                        , companyl
                        , imageMetadatal
                        , imgDatal
                        , certMetadata
                        , certData
                        , copyTypeTextl
                        , titleTextl
                        , transactionl
                        , printServicel
                        , transactionNumber
                        , transactionDate
                        , leftMarginl
                      )
                }
            }
            
        };


        this.doc.autoTable(this.getDcItemColumnHeaders(), this.getDcItemColumns(), {
            didDrawPage: pageContent,
            startY: this.custTableLastY + 10,
            margin: { left: this.leftMargin, right: this.rightMargin + 5, top: this.printService.afterTopCL, bottom: 100 },

            // margin: { left: this.leftMargin, right: this.rightMargin - 10 },
            // theme:'grid',
            theme: 'striped',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headStyles: {
                fillColor: [63, 81, 181],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: [255, 255, 255],
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                slNo: { cellWidth: slNoWidth, halign: 'center' },
                // partNumber: { cellWidth: partNumberWidth },
                materialName: { cellWidth: materialNameWidth },
                hsnCode: { cellWidth: hsnWidth },
                quantity: { cellWidth: qtyWidth, halign: 'center' },
                remarks: { cellWidth: remarks }

            }
        });

        // this.doc.setLineWidth(1);
        //  this.doc.setDrawColor(215, 235, 252);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        // //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // // Total page number plugin only available in jspdf v1.0+
        // if (typeof this.doc.putTotalPages === 'function') {
        //     this.doc.putTotalPages(totalPagesExp);
        // }



    }


    getDcItemFooterHeaders(): any[] {
        return [
            { title: "", dataKey: "dummy1" },
            { title: "", dataKey: "dummy2" },
            { title: "", dataKey: "dummy3" },
            { title: "", dataKey: "columnHead" },
            { title: "", dataKey: "columnContent" },
            { title: "", dataKey: "dummy4" },
        ];
    }

    makeDcFooterItem(): any[] {
        return [


            {
                "dummy1": " ",
                "dummy2": " ",
                "dummy3": " ",
                "columnHead": "Total Quantity",
                "columnContent": this.deliveryChallan.totalQuantity,
                "dummy4": " ",
            },

        ];
    }

    // writeDeliveryFooterItems() {

    //     var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    //     var dummy1: number = pageWidth * 0.05; //5%
    //     var dummy2: number = pageWidth * 0.10; //10%
    //     var dummy3: number = pageWidth * 0.34; //30% of total width
    //     var columnHead: number = pageWidth * 0.10; //6%
    //     var columnContent: number = pageWidth * 0.05; //6%
    //     var dummy4: number = pageWidth * 0.36;

    //     //console.log('page width: ', this.doc.internal.pageSize.width);
    //     this.doc.autoTable(this.getDcItemFooterHeaders(), this.makeDcFooterItem(), {
    //         //addPageContent: pageContent,
    //         startY: this.doc.autoTable.previous.finalY + 5,
    //         margin: { left: this.leftMargin, right: this.rightMargin },
    //         //theme:'grid',
    //         theme: 'plain',
    //         tableWidth: 'auto',
    //         showHead: 'never',

    //         styles: {
    //             cellPadding: 0.5,
    //             fontSize: 8,
    //             lineColor: [0, 0, 0],
    //             fillStyle: 'DF',
    //             //halign: 'right',
    //             overflow: 'linebreak',
    //             overflowColumns: false
    //         },
    //         headStyles : {
    //             fillColor: [215, 235, 252],
    //             lineColor: [0, 0, 0],
    //             fontSize: 9,
    //             textColor: 0,
    //             fontStyle: 'normal',
    //             halign: 'center',
    //         },
    //         columnStyles: {
    //             dummy1: { fillColor: [215, 235, 252], cellWidth: dummy1, halign: 'right' },
    //             dummy2: { fillColor: [215, 235, 252], cellWidth: dummy2 },
    //             dummy3: { fillColor: [215, 235, 252], cellWidth: dummy3 },
    //             columnHead: { fillColor: [215, 235, 252], cellWidth: columnHead, fontStyle: 'bold', },
    //             columnContent: { fillColor: [215, 235, 252], cellWidth: columnContent, halign: 'right' },
    //             dummy4: { fillColor: [215, 235, 252], cellWidth: dummy4 }

    //         }
    //     });

    //     this.doc.setLineWidth(1);
    //     this.doc.setDrawColor(215, 235, 252);

    // }


    // writeTermsAndconditionsAndSignature() {

    //     var tableHeight = this.doc.internal.pageSize.height - (this.doc.autoTable.previous.finalY + 50);

    //     // this.doc.setDrawColor(215, 235, 252);
    //     // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.finalY + 10, (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin)), tableHeight);

    //     let receiverSignatureHeader = [
    //         { title: "", dataKey: "data" },
    //     ];

    //     let receiverSignature = [


    //         {
    //             "data": " ",
    //         },
    //         {
    //             "data": "",
    //         },
    //         {
    //             "data": "Receiver's Signature"
    //         }
    //     ];



    //     let tableStart: number = this.doc.autoTable.previous.finalY + 10;

    //     this.doc.autoTable(receiverSignatureHeader, receiverSignature, {
    //         startY: tableStart,
    //         margin: { left: this.leftMargin, right: (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + this.summaryTableLeftMargin)) },
    //         theme: 'plain',
    //         styles: {
    //             halign: 'left',
    //             fontSize: 10,
    //             overflow: 'linebreak',
    //             overflowColumns: false
    //         },
    //         drawRow: function (row, data) {
    //             if (row.index === 0) {
    //                 //this.doc.setFontStyle('bold');
    //                 //console.log(data);
    //                 data.doc.setFontStyle('bold');
    //                 data.settings.styles = { fontStyle: 'bold' };
    //             }
    //         }
    //     });

    //     let signatureHeader = [
    //         { title: "key", dataKey: "data" },
    //     ];

    //     let signature = [
    //         {
    //             "data": "For " + this.company.name,
    //         },
    //         {
    //             "data": "",
    //         },
    //         {
    //             "data": "Authorised Signature"
    //         }
    //     ];


    //     tableStart += 20;

    //     this.doc.autoTable(signatureHeader, signature, {
    //         startY: tableStart,
    //         margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
    //         theme: 'plain',
    //         showHead: 'never',
    //         styles: {
    //             halign: 'center',
    //             fontSize: 10,
    //             overflow: 'linebreak',
    //             overflowColumns: false
    //         },
    //         drawRow: function (row, data) {
    //             if (row.index === 0) {
    //                 //this.doc.setFontStyle('bold');
    //                 //console.log(data);
    //                 data.doc.setFontStyle('bold');
    //                 data.settings.styles = { fontStyle: 'bold' };
    //             }
    //         }


    //     });

    // }
    // ................................


    getSummaryColumnHeaders(): any[] {
        return [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];
    }

    getSummaryColumns(): any[] {


        return [{}, {}, {}, {}, {}, {}, {}];
    }


    // .......................................
    writeSummary() {
        //console.log("this.summaryTableStartY: ", this.summaryTableStartY);
        //console.log("this.doc.autoTable.previous.finalY: ", this.doc.autoTable.previous.finalY);
        if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {
            //console.log("in")
            this.doc.addPage();
            
            this.printService.topSectionWithCertiImage(this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.certMetadata
                , this.certData
                , this.copyTypeText
                , this.titleText
                , this.deliveryChallan
                , this.printService
                , this.deliveryChallan.deliveryChallanNumber
                , this.deliveryChallan.deliveryChallanDate);

            //  this.summaryTableStartY = 250;
            this.printService.writeJustTransNo(this.doc, this.deliveryChallan.deliveryChallanNumber,AppSettings.DC_NAME);
            this.printService.writeJustTransDate(this.doc, this.deliveryChallan.deliveryChallanDate,AppSettings.DC_DATE);
            if (typeof this.doc.putTotalPages === 'function') {
                this.doc.putTotalPages(totalPagesExp);
            }
        }


        this.writeSignature(this.deliveryChallan.termsAndConditions);

    }


    writeSignature(termsAndCondition: string) {

        let tncHeader = [
            { title: "Terms And Conditions", dataKey: "tnc" },
        ];

        let tncDetails = [
            {
                "tnc": termsAndCondition ? termsAndCondition : "",

            }
        ];



        this.doc.autoTable(tncHeader, tncDetails, {
            startY: this.summaryTableStartY,
            margin: {
                left: this.leftMargin
                , right: (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + this.summaryTableLeftMargin))
            },
            theme: 'plain',
            styles: {
                halign: 'left',
                fontSize: 8,
                fillStyle: 'DF',
                overflow: 'linebreak',
                overflowColumns: false,
            },
            headStyles: {
                fontSize: 10,
            },
        });

        let signatureHeader = [
            { title: "key", dataKey: "data" },
        ];

        let signature = [
            {
                "data": "For " + this.company.name,
            }
        ];


        //tableStart += 20;
        // tableStart = doc.autoTable.previous.finalY + 5;

        let summaryTableEnd: number = this.doc.autoTable.previous.finalY + 10;

        this.doc.autoTable(signatureHeader, signature, {
            startY: summaryTableEnd,
            margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'right',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            // didDrawCell: function (row, data) {
            //   if (row.index === 0) {
            //     //this.doc.setFontStyle('bold');
            //     //console.log(data);
            //     data.doc.setFontStyle('bold');
            //     data.settings.styles = { fontStyle: 'bold' };
            //   }
            // }


        });

        let SignatureHeader = [
            { title: "key", dataKey: "data" },
        ];

        let Signature = [

            {
                "data": "",
            },
            {
                "data": "Authorised Signatory"
            }

        ];

        // let strat = doc.autoTable.previous.finalY;

        this.doc.autoTable(SignatureHeader, Signature, {
            startY: summaryTableEnd + 40,
            margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'right',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            // didDrawCell: function (row, data) {
            //   if (row.index === 0) {
            //     //console.log(data);
            //     data.doc.setFontStyle('bold');
            //     data.settings.styles = { fontStyle: 'bold' };
            //   }
            // }


        });


        let recvSignatureHeader = [
            { title: "key", dataKey: "data" },
        ];

        let recvSignature = [
            {
                "data": "",
            },
            {
                "data": "Receiver's Signature"
            }
        ];


        this.doc.autoTable(recvSignatureHeader, recvSignature, {
            startY: summaryTableEnd + 40,
            //margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                valign: 'middle',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            // didDrawCell: function (row, data) {
            //   if (row.index === 0) {
            //     //this.doc.setFontStyle('bold');
            //     //console.log(data);
            //     data.doc.setFontStyle('bold');
            //     data.settings.styles = { fontStyle: 'bold' };
            //   }
            // }


        });
    }


}