import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { QuotationHeader, QuotationWrapper, RecentQuotation, QuotationReport, QuotationSummaryReport } from '../data-model/quotation-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { TokenService } from './token.service';
import { Page } from '../reports/model/page';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { PagedData } from '../reports/model/paged-data';
import { map } from 'rxjs/operators';

@Injectable()
export class QuotationService{
    apiGetAllUrl : string ;
    apiPostUrl : string;
    apiGetUrl : string;
    apiDeleteUrl : string;
    apiDeleteItemUrl: string;
    apiGetQuotationNumberUrl : string;
    //apiGetQuotationNumUrl : string;
    apiGetQuotationsForParty : string;
    recentQuotationApiUrl: string;
    checkQuatationNumberApiUrl: string;
    token : string;
    quotationReportApiUrl: string;
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http : HttpClient, private tokenService : TokenService){
        this.apiGetAllUrl = environment.baseServiceUrl + environment.quotationApiGetAllUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.quotationApiPostUrl;
        this.apiGetUrl = environment.baseServiceUrl + environment.quotationApiGetUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.quotationApiGetUrl;
        this.apiDeleteItemUrl = environment.baseServiceUrl + environment.quotationItemDeleteApiUrl;
        this.apiGetQuotationNumberUrl = environment.baseServiceUrl + environment.apiGetQuotationNumberUrl;
        //this.apiGetQuotationNumUrl = environment.baseServiceUrl + environment.apiGetQuotationNumUrl;
        this.apiGetQuotationsForParty = environment.baseServiceUrl + environment.quotationsForPartyApiGetUrl;
        this.recentQuotationApiUrl = environment.baseServiceUrl + environment.recentQuotationApiUrl;
        this.checkQuatationNumberApiUrl = environment.baseServiceUrl + environment.checkQuatationNumberApiUrl;
        this.quotationReportApiUrl = environment.baseServiceUrl + environment.quotationReportApiUrl;
    }

    getAllQuotations() : Observable<QuotationHeader[]>{

        return this.http.get<QuotationHeader[]>(this.apiGetAllUrl)
                        // .map(response => <QuotationHeader[]>response.json());
    }

    getQuotation(headerQuotationId: string) : Observable<QuotationHeader>{
        
        return this.http.get<QuotationHeader>(this.apiGetUrl+"/"+headerQuotationId)
        
                        // .map(response => <QuotationHeader>response.json());

    }

    // getQuotationNumber(): Observable<TransactionResponse>{
    //     return this.http.get<TransactionResponse>(this.apiGetQuotationNumUrl)
    //     .pipe(
    //         keepFresh(1000 * 30 )
    //     );
    // }

    getQuotationByQuotationNumber(headerQuotationNumber: string) : Observable<QuotationHeader[]>{
        
        return this.http.get<QuotationHeader[]>(this.apiGetQuotationNumberUrl+"/"+headerQuotationNumber)
                        // .map(response => <QuotationHeader[]>response.json());

    }

    create(quotationWrapper : QuotationWrapper) : Observable<QuotationHeader>{

        ////console.log("In create...", quotationHeader.id);
        return this.http.post<QuotationHeader>(this.apiPostUrl, quotationWrapper )
                        // .map(response => <QuotationHeader>response.json());
    }
    getQuotationsForParty(partyId : number) : Observable<QuotationHeader[]>{
        return this.http.get<QuotationHeader[]>(this.apiGetQuotationsForParty+"/"+partyId )
                        // .map(response => response.json());
    }

    delete(id : string) : Observable<TransactionResponse>{
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl+"/"+id )
            // .map(response => <TransactionResponse>response.json());
    }

    deleteItem(id: string) : Observable<TransactionResponse>{

        return this.http.delete<TransactionResponse>(this.apiDeleteItemUrl+"/"+id);
    }

    getRecentQuotations(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number):  Observable<RecentQuotation>{
      //console.log("in transactionTypeId",transactionTypeId)
        sortColumn = sortColumn ? sortColumn : "quotationDate";
        sortColumn = (sortColumn === "quotationNumber") ? "quotationId" : sortColumn;
       
        sortDirection = sortDirection ? sortDirection : "desc";
        page = page ? page+1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : "null";

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re,"-");

        //console.log("filterText: ",filterText);
        
        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        
        return this.http
            .get<RecentQuotation>(this.recentQuotationApiUrl + "/" + transactionTypeId + "/"+sortColumn+"/"+sortDirection+"/"+page+"/"+pageSize, {params: params});
    }

    checkQtnNumAvailability(quotationNumber, qtnTyp): Observable<TransactionResponse> {
        //console.log("quatationNumber in serice", quotationNumber)
        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("quotationNumber", quotationNumber);

        return this.http.get<TransactionResponse>(this.checkQuatationNumberApiUrl + "/" +qtnTyp, {params: params})
        // .map(response => <TransactionResponse>response.json());

    }

    getQuotationReport(quotationTypeId: number, quotationReportRequest : TransactionReportRequest, page: Page): Observable<PagedData<QuotationSummaryReport>>{
        

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
        pageSortColumn = (pageSortColumn === "quotationNumber") ? "quotationId" : pageSortColumn;
      //  pageSortColumn = pageSortColumn ? pageSortColumn : 'invoiceDate';

      
        return this.http.post<QuotationReport>(
            this.quotationReportApiUrl+ "/" + 
            quotationTypeId + "/" +
            pageNumber + "/" +
            pageSize + "/" + 
            pageSortDirection+ "/" + 
            pageSortColumn,quotationReportRequest ).pipe(
            map(quotationData => this.getPagedDatas(page, quotationData)));
    }

    private getPagedDatas(page: Page, quotationData: QuotationReport): PagedData<QuotationSummaryReport> {
        
        
        let pagedData = new PagedData<QuotationSummaryReport>();

        if(page){
        page.totalElements = quotationData.totalCount;
       
        page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = quotationData.quotationSummaryReports;
      
        return pagedData;
    }
}