import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';4
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { TransactionType } from '../data-model/transaction-type';
import { TokenService } from './token.service';
import { PaymentMethod } from '../data-model/payment-method';
import { CardType } from '../data-model/card-model';

@Injectable()
export class PaymentMethodService{


    paymentMethodApiUrl: string;
    cardTypeApiUrl: string;
    constructor(private http: HttpClient) {
    this.paymentMethodApiUrl = environment.baseServiceUrl + environment.paymentMethodApiUrl;
    this.cardTypeApiUrl = environment.baseServiceUrl + environment.cardTypeApiUrl;
    }
    getAllPaymentMethods(): Observable<PaymentMethod[]> {
        
                return this.http
                    .get<PaymentMethod[]>(this.paymentMethodApiUrl)
            }

            getCardTypes(): Observable<CardType[]> {
                return this.http
                .get<CardType[]>(this.cardTypeApiUrl)
            }
}