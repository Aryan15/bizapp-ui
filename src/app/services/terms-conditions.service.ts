import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { TermsAndCondition } from '../data-model/terms-condition-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';
import { shareAndCache } from 'http-operators';
import { AppSettings } from '../app.settings';

@Injectable()
export class TermsAndConditionsService{
     tncApiUrl : string;
     tncAllApiUrl : string;

     private headers = new Headers({'Content-Type': 'application/json'});


    constructor(private http: HttpClient, private tokenService : TokenService){
        this.tncApiUrl = environment.baseServiceUrl + environment.tncApiUrl;
        this.tncAllApiUrl = environment.baseServiceUrl + environment.tncAllApiUrl;
    }

    getAllTncs() : Observable<TermsAndCondition[]>{

        return this.http
            .get<TermsAndCondition[]>(this.tncApiUrl)
            
            // .map(response => <TermsAndCondition[]>response.json());

    }

    getByTransactionTypeAndDefault(transactionTypeId: number): Observable<TermsAndCondition>{
        return this.http
            .get<TermsAndCondition>(this.tncApiUrl+"/"+transactionTypeId)
            
            // .map(response => <TermsAndCondition>response.json());
    }

    getByTransactionType(transactionTypeId: number): Observable<TermsAndCondition[]>{
        return this.http
            .get<TermsAndCondition[]>(this.tncAllApiUrl+"/"+transactionTypeId)
            
            // .map(response => <TermsAndCondition>response.json());
    }

    parseJSON(response: any) {
            //console.log('parseJSON: response: ', response.text());
          return response.text() ? JSON.parse(response) : {}
      }

      delete(id: number) : Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.tncApiUrl+"/"+id)
                    // .map(response => <TransactionResponse>response.json() );
    }
}