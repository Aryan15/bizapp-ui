import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TokenService } from './token.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { EwayBillTracking, EwayBillInvoiceResponseDTO, EWayBillRecentDTO, EWayBillReport, EWayBillReportModel, InvoiceReportItemModel } from '../data-model/invoice-model';
import { Page } from '../reports/model/page';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { map } from 'rxjs/operators';
import { PagedData } from '../reports/model/paged-data';
 
@Injectable({
  providedIn: 'root'
})
export class EWayBillService {

  eWayBillGenerationApiUrl: string;
  eWayBillApiUrl: string;
  eWayBillCancellationApiUrl: string;
  eWayBillVehicleUpdateApiUrl: string;
  eWayBillDetailsByInvoiceApiUrl: string;
  eWayBillByInvoiceApiUrl: string;
  eWayBillExtendValidityApiUrl: string;
  eWayBillByNumberApiUrl: string;
  eWayBillTransporterIDUpdateApiUrl: string;
  eWayBillReportApiUrl: string;

   constructor(private http: HttpClient) {
      this.eWayBillGenerationApiUrl = environment.baseServiceUrl + environment.eWayBillGenerationApiUrl;
      this.eWayBillApiUrl = environment.baseServiceUrl + environment.eWayBillApiUrl;
      this.eWayBillCancellationApiUrl = environment.baseServiceUrl + environment.eWayBillCancellationApiUrl;
      this.eWayBillVehicleUpdateApiUrl = environment.baseServiceUrl + environment.eWayBillVehicleUpdateApiUrl;
      this.eWayBillDetailsByInvoiceApiUrl = environment.baseServiceUrl + environment.eWayBillDetailsByInvoiceApiUrl
      this.eWayBillExtendValidityApiUrl = environment.baseServiceUrl + environment.eWayBillExtendValidityApiUrl;
      this.eWayBillByNumberApiUrl = environment.baseServiceUrl + environment.eWayBillByNumberApiUrl;
      this.eWayBillTransporterIDUpdateApiUrl = environment.baseServiceUrl + environment.eWayBillTransporterIDUpdateApiUrl;
      this.eWayBillReportApiUrl = environment.baseServiceUrl + environment.eWayBillReportApiUrl;
      this.eWayBillByInvoiceApiUrl = environment.baseServiceUrl + environment.eWayBillByInvoiceApiUrl

    }

    generateEWayBill(ewaybillTracking : EwayBillTracking): Observable<EwayBillInvoiceResponseDTO[]> {
     return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillGenerationApiUrl,ewaybillTracking);
    }

     cancelEWayBill(ewaybillTracking : EwayBillTracking): Observable<EwayBillInvoiceResponseDTO[]> {
      return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillCancellationApiUrl,ewaybillTracking);
    }

    getEWayBills(headerInvoiceId: string): Observable<EwayBillTracking[]> {
      return this.http.get<EwayBillTracking[]>(this.eWayBillDetailsByInvoiceApiUrl + "/" + headerInvoiceId)
    }

    getEWayBill(headerInvoiceId: string): Observable<EwayBillTracking> {
      return this.http.get<EwayBillTracking>(this.eWayBillByInvoiceApiUrl + "/" + headerInvoiceId)
    }

    getEWayBillByEwayNum(eWayBillNum: string): Observable<EwayBillTracking> {
      return this.http.get<EwayBillTracking>(this.eWayBillApiUrl + "/" + eWayBillNum)
    }

    getAllActiveEWayBills(): Observable<EwayBillTracking[]> {
      return this.http.get<EwayBillTracking[]>(this.eWayBillApiUrl)
    }

    eWayBillVehicleUpdate(ewaybillTracking : EwayBillTracking,type: string): Observable<EwayBillInvoiceResponseDTO[]> {
      if(type=="1"){
        return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillVehicleUpdateApiUrl,ewaybillTracking);

      }else if(type=="2"){
        return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillTransporterIDUpdateApiUrl,ewaybillTracking);

      }
      
    }

    eWayBillExtendValidity(ewaybillTracking : EwayBillTracking): Observable<EwayBillInvoiceResponseDTO[]> {
      return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillExtendValidityApiUrl,ewaybillTracking);
    }

    getEWayBillByNumber(eWayBillNum : string): Observable<EwayBillInvoiceResponseDTO[]> {
      return this.http.post<EwayBillInvoiceResponseDTO[]>(this.eWayBillByNumberApiUrl,eWayBillNum);
    }

    getRecentEWayBills(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<EWayBillRecentDTO> {
      sortColumn = sortColumn ? sortColumn : 'eWayBillNo';
      sortDirection = sortDirection ? sortDirection : 'desc';
      page = page ? page + 1 : 1;
      pageSize = pageSize ? pageSize : 30;
      filterText = filterText ? filterText : 'null';
 
      let params = new HttpParams();
      // params = params.append("searchText", encodeURIComponent(filterText));
      params = params.append("searchText", filterText);

      return this.http
          .get<EWayBillRecentDTO>(this.eWayBillApiUrl + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
  }

  
  getEwayBillReport(transactionReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<EWayBillReportModel>> {
  

    let pageNumber = page ? page.pageNumber : -99;
    let pageSize = page ? page.size : -99;
    let pageSortDirection = page ? page.sortDirection : null;
    let pageSortColumn = page ? (page.sortColumn ? page.sortColumn : "eWayBillDate") : null;
    pageSortColumn = (pageSortColumn === "eWayBillDate") ? "eWayBillDate" : pageSortColumn;

    return this.http
            .post<EWayBillReport>(this.eWayBillReportApiUrl
            + "/" + pageSortColumn
            + "/" + pageSortDirection
            + "/" + pageNumber
            + "/" + pageSize,transactionReportRequest).pipe(
            map(ewayData => this.getPagedData(page, ewayData)));
}


private getPagedData(page: Page, ewayBillReportData: EWayBillReport): PagedData<EWayBillReportModel> {

    //console.log("invoiceItemData: ",invoiceItemData);
    let pagedData = new PagedData<EWayBillReportModel>();

    if (page) {
        page.totalElements = ewayBillReportData.totalCount ? ewayBillReportData.totalCount : 0;
        page.totalPages = page.totalElements / page.size;
    }
   
    pagedData.page = page;
    pagedData.data = ewayBillReportData.ewayBillReports;
    return pagedData;
}
}
