import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { BankModel, RecentBank } from '../data-model/bank-model';
import { TransactionResponse } from '../data-model/transaction-response';


@Injectable()
export class BankService {

    bankApiUrl: string;
    banksApiUrl: string;
    bankByBankNameApiUrl: string;
    apiDeleteUrl: string;
    recentBankUrl: string;
    token: string;

    httpOptions: any;

    constructor(private http: HttpClient) {
        this.banksApiUrl = environment.baseServiceUrl + environment.banksApiUrl;
        this.bankByBankNameApiUrl = environment.baseServiceUrl + environment.bankByBankNameApiUrl;
        this.bankApiUrl = environment.baseServiceUrl + environment.bankApiUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.bankApiUrl;
        this.recentBankUrl = environment.baseServiceUrl + environment.recentBankUrl;

    }

    save(bank: BankModel): Observable<BankModel> {
        //console.log("saving....", bank);

        return this.http.post<BankModel>(this.bankApiUrl, bank);

    }

    createBanks(banks: BankModel[]): Observable<BankModel[]>{
        return this.http.post<BankModel[]>(this.banksApiUrl, banks);
    }

    deleteBanks(ids: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.bankApiUrl+"/"+ids)
    }

    get(id: number): Observable<BankModel> {
        return this.http
            .get<BankModel>(this.bankApiUrl + "/" + id);
        //.map(response => <BankModel>response.json());
    }

    getAllBanks(): Observable<BankModel[]> {
        return this.http
            .get<BankModel[]>(this.banksApiUrl);

    }

    delete(id: string): Observable<BankModel> {
        //console.log("Deleting...", id);
        return this.http.delete<BankModel>(this.apiDeleteUrl + "/" + id)

    }

    getBankByBankName(bankName: string): Observable<BankModel[]> {

        return this.http.get<BankModel[]>(this.bankByBankNameApiUrl + "/" + bankName)

    }

    getRecentBanks(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentBank> {
        sortColumn = sortColumn ? sortColumn : 'bankName';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        //console.log("filterText: ", filterText);

        return this.http
            .get<RecentBank>(this.recentBankUrl + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }
}