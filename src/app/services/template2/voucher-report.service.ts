import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from '../../data-model/print-copies-model';
import { TransactionType } from '../../data-model/transaction-type';
import { VoucherHeader } from '../../data-model/voucher-model';
import { NumberToWordsService } from '../../services/number-to-words.service';
import { CompanyService } from '../company.service';
import { PrintService } from '../print.service';


declare var jsPDF: any;

// var imgData = 'D:/angular project/src/app/services/template2/download.jpg/jpeg;base64,verylongbase64;';
var imgData;




@Injectable()
export class VoucherSecondReportService {

  //constructor() { }


  voucher: VoucherHeader;
  company: Company;
  transactionType: TransactionType;
  imageMetadata: ImageMetadata;
  toWards: string = "";
  paymentMode: string = "";
  chequeNumber: string ="";
  chequeDate: string="";
  bankName: string = "";
  datePipe = new DatePipe('en-US');

  doc = new jsPDF('p', 'pt');
  totalPagesExp = "{total_pages_count_string}";




  titleText: string = "";
  copyTypeText: string = "ORIGINAL";


  leftMargin: number = 15;
  rightMargin: number = 15;

  custTableLastY: number;
  summaryTableStartY: number = 500;
  summaryTableLeftMargin: number = 350;



  constructor( @Inject('Window') private window: Window,
    private numberToWordsService: NumberToWordsService,
    private companyService: CompanyService,
    private printService: PrintService) {


  }


  ngOnInit() {


  }


  fillPage() {

    // this.printService.writeCompanyGST(this.company.gstNumber, this.doc);
    this.printService.writeImage(this.imageMetadata, imgData, this.doc);

    //Set Title and copy type
    //this.doc.setDrawColor(215, 235, 252);
    this.printService.writeTitleAndCopyType(this.copyTypeText, this.titleText, this.doc);

    // this.printService.writeCompanyPhoneAndMobileNumber(this.company.primaryMobile, this.company.primaryTelephone, this.doc);
    //Company And address in the header 
     this.writeCompanyAndAddress();

    this.writeCustomerDetails();
    


  }

 
  getCopy(copyT: string) {

    this.copyTypeText = copyT;
    this.fillPage();

  }

  download(voucher: VoucherHeader, company: Company, copyText: string, transactionType: TransactionType, printCopys: PrintCopy[] = [],printHeaderText:string) {

    Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
    //console.log("in downd...voucher: ", voucher , transactionType);
    this.company = company;
    this.summaryTableStartY = 500;

    Object.keys(voucher).forEach(k => {
      voucher[k] = voucher[k] === null ? '' : voucher[k]
      if (typeof voucher[k] === "number") {
        voucher[k] = voucher[k] === null ? '' : voucher[k].toFixed(2)
      }
    }
    )

    this.voucher = voucher;
    this.transactionType = transactionType;

    if(printHeaderText){
      this.titleText = printHeaderText;
    }

else{
    if (transactionType.name == AppSettings.CHEQUE_VOUCHER) {
      this.titleText = AppSettings.CHEQUE_VOUCHER;
    }
    else if(transactionType.name == AppSettings.CASH_VOUCHER) {
      this.titleText = AppSettings.CASH_VOUCHER;
    }
    else
    {
      this.titleText = "Expenses"
    }
  }
    this.companyService.getCompanyLogoAsBase64(1).subscribe(response => {
      ////console.log("Response", response)

      imgData = response;
      this.doc = new jsPDF('p', 'pt');

      this.companyService.getCurrentCompanyLogo(1).subscribe(response => {
        //console.log('logo name: ', response);
        this.companyService.getCompanyLogoMetadata(response.responseString).subscribe(response => {
          //console.log('logo dimension: ', response);
          this.imageMetadata = response;

          switch (copyText) {
            case AppSettings.PRINT_ALL:
                    // printCopys.forEach(pCopy => {
                    //     this.getCopy(pCopy.name);
                    // })
                    printCopys.forEach((value, index) => {

                      if (value.name != AppSettings.PRINT_ALL) {
                        this.getCopy(value.name);
                        if (index != printCopys.length - 2) {
                          this.doc.addPage();
                        }
                      }
  
  
                    })
                break;
            default:
                //console.log("in default: ", copyText);
                this.getCopy(copyText);
                break;
        }
          // switch (copyNumber) {
          //   case 1:
          //     this.getCopy(AppSettings.PRINT_ORIGINAL);
          //     break;
          //   case 2:
          //     this.getCopy(AppSettings.PRINT_DUPLICATE);
          //     break;
          //   case 3:
          //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
          //     break;
          //   case 4:
          //     this.getCopy(AppSettings.PRINT_ORIGINAL);
          //     this.doc.addPage();
          //     this.getCopy(AppSettings.PRINT_DUPLICATE);
          //     this.doc.addPage();
          //     this.getCopy(AppSettings.PRINT_TRIPLICATE);
          //     break;
          //   default:
          //     this.getCopy(AppSettings.PRINT_ORIGINAL);
          //     break;
          // }





          // this.openInNewTab();
          this.printService.printData.next(this.doc.output('blob'));

        })
      })





    },
      error => {


      })
    //; )


  }



  openInNewTab() {

    // var string = this.doc.output('datauristring');
    // var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
    // var x = window.open();
    // x.document.open();
    // x.document.write(iframe);
    // x.document.close();
    // return this.doc;

    this.doc.setProperties({
      title: "VOC"+this.voucher.voucherNumber
    });

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "VOC.pdf");
    } else {
    
      // For other browsers:
      // Create a link pointing to the ObjectURL containing the blob.
      this.doc.autoPrint();
      window.open(
        this.doc.output("bloburl"),
        "_blank"
      );
      //,"height=650,width=500,scrollbars=yes,location=yes"
      // For Firefox it is necessary to delay revoking the ObjectURL
      setTimeout(() => {    
        window.URL.revokeObjectURL(this.doc.output("bloburl"));
      }, 100);
    }
  }

  getCompanyHeader(): any[] {

    var headerColumns = [
      { title: "Company Name", dataKey: "companyName" },
    ];

    return headerColumns;

  }

  getCompany(): any[] {

    return [
      {
        "companyName": this.company.name || " ", //"Uthkrushta Technologies",
      }
    ];
  }


  getAddressHeader(): any[] {

    var headerColumns = [

      { title: "Company Address", dataKey: "companyAddress" },
    ];

    return headerColumns;

  }


  getAddress(): any[] {

    return [
      {
        "companyAddress": this.company.tagLine 
      },
      {
        "companyAddress": this.company.address || " ", //"Nagendra Block, BSK 3rd Stage, 29203882038"
      }
    ];
  }

  getGSTHeader(): any[] {
    var headerColumns = [

      { title: "GST", dataKey: "GST" },
    ];

    return headerColumns;
  }

  getGST(): any[] {
    return [
      {

        "GST": this.company.gstNumber || " ", //"XXXX"
      }
    ];
  }

  getCompanyEmail(): any[] {
    var headerColumns = [

      { title: "Email", dataKey: "email" },
    ];

    return headerColumns;
  }
  getEmail(): any[] {
    return [
      {

        "email": this.company.email || " ", //"XXXX"
      }
    ];
  }
  writeCompanyGST() {
    this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });
  }
  writeCompanyAndAddress() {

    var startCompanyY: number = 55;
    //var startAddressY: number = 80;
    this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

      startY: startCompanyY,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'bold',
        fontSize: 12,
      }

    });

    this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });




    this.doc.autoTable(this.getCompanyEmail(), this.getEmail(), {

      startY: this.doc.autoTable.previous.finalY - 5,
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'center',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
      }
    });

  }


  getCustomerAddressHeader(): any[] {

    return [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

  }


  getSummaryColumnHeaders(): any[] {
    return [
      { title: "dummy", dataKey: "key" },
      { title: "dummy", dataKey: "data" },
    ];
  }


  getToWards() {
    this.voucher.voucherItems.forEach(item => {
      //console.log("item :",item);
      
      this.toWards = item.expenseName; // +  (item.description !== null)  ? (" - " + item.description) : " ";
      this.toWards += (item.description)  ? (" - " + item.description) : " ";
      //console.log("this.toWards: "+this.toWards)
    })
  }
  getPaymentMode() {
    if (this.transactionType.name === AppSettings.CHEQUE_VOUCHER) {
      this.paymentMode = "Cheque";
      this.chequeNumber = this.voucher.chequeNumber;
      this.chequeDate = this.datePipe.transform(this.voucher.chequeDate, AppSettings.DATE_FORMAT);
      this.bankName =  this.voucher.bankName;
    }
   else if (this.transactionType.name === AppSettings.CASH_VOUCHER){
      this.paymentMode = "Cash";
    }
    else{

    }
  }
  getCustomerAddress(): any[] { 
    this.getToWards();
    this.getPaymentMode();
    if (this.transactionType.name === AppSettings.CHEQUE_VOUCHER) {
      return [
        {
          "columnHead": "Voucher Number : ",
          "columnContent": this.voucher.voucherNumber,
        },
        {
          "columnHead": "Voucher Date : ",
          "columnContent": this.datePipe.transform(this.voucher.voucherDate, 'dd-MM-yyyy')
        },
        {
          "columnHead": "Paid To : ",
          "columnContent": this.voucher.paidTo,
        },
        {
          "columnHead": "Amount In Rs : ",
          "columnContent": this.voucher.amount,
        },
        {
          "columnHead": "Towards : ",
          "columnContent": this.toWards,
        },
        {
          "columnHead": "Payment Mode : ",
          "columnContent": this.paymentMode,
        },
        {
          "columnHead": "Cheque Number : ",
          "columnContent": this.chequeNumber,
        },
        {
          "columnHead": "Cheque Date : ",
          "columnContent": this.datePipe.transform(this.chequeDate, 'dd-MM-yyyy'),
        },
        {
          "columnHead": "Bank Name : ",
          "columnContent": this.bankName,
        },
        {
          "columnHead": "A Sum of Rupees : ",
          "columnContent": this.numberToWordsService.number2text(this.voucher.amount),
        }
  
      ];
  }
  else
  {
    return [
      {
        "columnHead": "Voucher Number : ",
        "columnContent": this.voucher.voucherNumber,
      },
      {
        "columnHead": "Voucher Date : ",
        "columnContent": this.datePipe.transform(this.voucher.voucherDate, 'dd-MM-yyyy')
      },
      {
        "columnHead": "Paid To : ",
        "columnContent": this.voucher.paidTo,
      },
      {
        "columnHead": "Amount In Rs : ",
        "columnContent": this.voucher.amount,
      },
      {
        "columnHead": "Towards : ",
        "columnContent": this.toWards , 
      },
      {
        "columnHead": "Payment Mode : ",
        "columnContent": this.paymentMode,
      },
     
      {
        "columnHead": "A Sum of Rupees : ",
        "columnContent": this.numberToWordsService.number2text(this.voucher.amount),
      }

    ];
   
  }

  }
  
  writeCustomerDetails() {
    //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    var startY: number = this.doc.autoTable.previous.finalY + 5;
    var custAddressTableRightMargin: number = 300;



    this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

      startY: startY,
      margin: { left: this.leftMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      columnStyles: {
        columnHead: { fontStyle: 'bold', cellWidth: 100, halign: 'right' }
      }

    });

    var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);



    var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
    //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
    //console.log('startY: ', startY);

    var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

    this.doc.rect(8, 5, 580, 805);
  

        // this.doc.rect(this.leftMargin+10 ,this.doc.autoTable.previous.finalY+10 , 100, 30);
     
        // this.doc.rect(tableHeight +270 ,this.doc.autoTable.previous.finalY+10 , 70, 50,);
    

        
             let recivedHeader = [
               { title: "key", dataKey: "data" },
             ];
         
             let recived = [
             
               {
                 "data": "",
               },
               {
                 "data": "Received by"
               }
              
             ];
         
             let strat = this.doc.autoTable.previous.finalY ;
             
             this.doc.autoTable(recivedHeader, recived, {
               startY: strat +50 ,
               margin: { left: (this.leftMargin), right: this.rightMargin +120 },
               theme: 'plain',
               showHead: 'never',
               styles: {
                 halign: 'right',
                 fontSize: 10,
                 overflow: 'linebreak',
                 overflowColumns: false
               },
               pageContent: function (row, data) {
                 if (row.index === 0) {
                   //console.log(data);
                   data.doc.setFontStyle('bold');
                   data.settings.styles = { fontStyle: 'bold' };
                 }
               }
         
         
             });
             let authorHeader = [
              { title: "key", dataKey: "data" },
            ];
        
            let author = [
            
              {
                "data": "",
              },
              {
                "data": "Authorised by"
              }
             
            ];
        
            
            this.doc.autoTable(authorHeader, author, {
              startY: strat +50 ,
              margin: { left: (this.leftMargin +30), right: this.rightMargin },
              theme: 'plain',
              showHead: 'never',
              styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
              },
              pageContent: function (row, data) {
                if (row.index === 0) {
                  //console.log(data);
                  data.doc.setFontStyle('bold');
                  data.settings.styles = { fontStyle: 'bold' };
                }
              }
        
        
            });

   
             this.doc.setDrawColor(215, 235, 252);
             //console.log("Parameterr"+this.leftMargin, startY, this.doc.autoTable.previous.finalY, tableHeight);
            //  if(this.transactionType.name === AppSettings.CHEQUE_VOUCHER){
            //  this.doc.rect(this.leftMargin, startY, this.doc.autoTable.previous.finalY+140, tableHeight+100);
            //  }
            //  else
            //  {
            //   this.doc.rect(this.leftMargin, startY, this.doc.autoTable.previous.finalY+200, tableHeight+140);
            //  }
            this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin), tableHeight + 100);

  }



}
