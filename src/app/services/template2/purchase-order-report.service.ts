import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { AppSettings } from '../../app.settings';
import { Company } from '../../data-model/company-model';
import { ImageMetadata } from '../../data-model/misc-model';
import { PrintCopy } from "../../data-model/print-copies-model";
import { PurchaseOrderHeader, PurchaseOrderItem } from '../../data-model/purchase-order-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { NumberToWordsService } from '../../services/number-to-words.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { CompanyService } from '../company.service';
import { GlobalSettingService } from '../global-setting.service';
import { PrintService } from '../print.service';
import { forkJoin } from 'rxjs';
import { Currency } from '../../data-model/currency-model';
import { CurrencyService } from '../currency.service';
import { ExportNumberToWordsService } from '../export-number-currency-to-words.service ';
declare var jsPDF: any;
var imgData;
var totalPagesExp = "{total_pages_count_string}";
@Injectable()
export class PurchaseOrderSecondReportService {

    purchaseOrder: PurchaseOrderHeader;
    company: Company;
    
    datePipe = new DatePipe('en-US');

    doc = new jsPDF('p', 'pt');
   // totalPagesExp = "{total_pages-count-string}";

    titleText: string = "PURCHASE ORDER";
    copyTypeText: string = "ORIGINAL";

    leftMargin: number = 15;
    rightMargin: number = 15;
    afterTop: number = 20;
    custTableLastY: number;
    summaryTableStartY: number = 600; //500;
    summaryTableLeftMargin: number = 350;

    transactionTypeName: string;
    imgData: any;
    signData: any;
    signMetadata: ImageMetadata;
    GSTIN :String ="";
    totalAmount: string;
    discountAmount: string;
    amountAfterDiscount: string;
    cgstAmount: string;
    sgstAmount: string;
    igstAmount: string;
    roundOffAmount: string;
    grandTotal: string;
    //globalSetting: GlobalSetting;
    imageMetadata: ImageMetadata;
    isItemLevelTax: boolean = false;
    currency:Currency;
    currencySymbol:any;
    currencyName:any;
    currencyRate:any;
    
    public currencys: Currency[] = [];
    // currencyName:any;
       currencyFraction:string;
      currencyDecimal:string;
       currId:string;

    constructor(@Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService,
        private companyService: CompanyService,
        private numberFormatterService: NumberFormatterService,
        private printService: PrintService,
        private currencyService: CurrencyService,
        private exportNumberToWordsService:ExportNumberToWordsService,
        private globalSettingService: GlobalSettingService) {
            this.getCurrencys();  
         }
       


    ngOnInit() {
        this.getCurrencys();  
    }

    download(transactionTypeName: string,
       purchaseOrder: PurchaseOrderHeader,
         company: Company,
         copyText: string,
         //globalSetting: GlobalSetting,
         isItemLevelTax: boolean,
         printCopys: PrintCopy[] = [],
        printHeaderText: string,
         signatureValue: number) {
            //console.log("transactionTypeName.......!!!!", transactionTypeName);

        //this.globalSetting = globalSetting;
        this.isItemLevelTax = isItemLevelTax;
        this.summaryTableStartY = 600;
        if(printHeaderText){
      this.titleText=  printHeaderText;
        }
        //console.log("in downd...copyNumber: ", copyText);
        //console.log("in downd...purchaseOrder: ", purchaseOrder);
        //console.log("in downd...company: ", company);
        //console.log("in downd...globalSetting: ", globalSetting);

        Object.keys(company).forEach(k => company[k] = company[k] === null ? '' : company[k])
        
       
                // //console.log("in downd...purchaseOrder: ", purchaseOrder);
                this.company = company;
        
        // Object.keys(purchaseOrder).forEach(k => purchaseOrder[k] = purchaseOrder[k] === null ? '' : purchaseOrder[k])
        Object.keys(purchaseOrder).forEach(k => {
            purchaseOrder[k] = purchaseOrder[k] === null ? '' : purchaseOrder[k];
            if (typeof purchaseOrder[k] === "number") {
                purchaseOrder[k] = purchaseOrder[k] === null ? '' : purchaseOrder[k].toFixed(2)
              }
        })
        purchaseOrder.purchaseOrderItems.forEach(item => {
            Object.keys(item).forEach(k => {
               // //console.log("item, k", item, k)
                item[k] = item[k] === null ? '' : item[k]
                if (typeof item[k] === "number" && k !== "slNo" && k !== "discountPercentage" && k!== "igstTaxPercentage" && k !== "cgstTaxPercentage" && k !== "sgstTaxPercentage") {
                    item[k] = item[k] === null ? '' : item[k].toFixed(2)
                }
            }
            )
        })
  
        this.purchaseOrder = purchaseOrder;
        this.currId=this.numberFormatterService.numberI(this.purchaseOrder.partyCurrencyId)
        if(+this.currId!=0)
        {
         this.getCurrencyData(+this.currId);
     
        }
       
        this.transactionTypeName = transactionTypeName;
        forkJoin(
            // this.companyService.loadImage(this.companyService.companyLogoUrl+"/"+"image.png"),
            this.companyService.getCompanyLogoAsBase64(1),
            this.companyService.getImageMetadata(),
            this.companyService.getSignatureImageAsBase64(1, signatureValue),
            this.companyService.getSignatureMetadata(signatureValue)
          )
          .subscribe(([
            companyLogoBase64,
            companyLogoMetadata,
            signatureImageAsBase64,
            signatureImageMetadata
          ]) => {
            this.imgData = companyLogoBase64;
            this.imageMetadata = companyLogoMetadata
            this.signData = signatureImageAsBase64
            this.signMetadata = signatureImageMetadata
    
            this.doc = new jsPDF('p', 'pt');

     
                 
                        switch (copyText) {
                            case AppSettings.PRINT_ALL:
                                    // printCopys.forEach(pCopy => {
                                    //     this.getCopy(pCopy.name);
                                    // })
                                    printCopys.forEach((value, index) => {

                                        if (value.name != AppSettings.PRINT_ALL) {
                                          this.getCopy(value.name);
                                          if (index != printCopys.length - 2) {
                                            this.doc.addPage();
                                          }
                                        }
                    
                    
                                      })
                                break;
                            default:
                                //console.log("in default: ", copyText);
                                this.getCopy(copyText);
                                break;
                        }
                        // switch (copyNumber) {
                        //     case 1:
                        //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                        //         break;
                        //     case 2:
                        //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                        //         break;
                        //     case 3:
                        //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                        //         break;
                        //     case 4:
                        //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                        //         this.doc.addPage();
                        //         this.getCopy(AppSettings.PRINT_DUPLICATE);
                        //         this.doc.addPage();
                        //         this.getCopy(AppSettings.PRINT_TRIPLICATE);
                        //         break;
                        //     default:
                        //         this.getCopy(AppSettings.PRINT_ORIGINAL);
                        //         break;
                        // }
    
    
                        this.printService.writeFooter(this.doc, copyText);
                        // this.openInNewTab();
                        this.printService.printData.next(this.doc.output('blob'));
                  
                    
              

       
                    })


    }
    fillPage() {
     
        this.printService.topSection(this.doc
            ,  this.company
            , this.imageMetadata
            , this.imgData
            , this.copyTypeText
            , this.titleText
            , this.purchaseOrder
            , this.printService
            , this.purchaseOrder.purchaseOrderNumber
            , this.purchaseOrder.purchaseOrderDate
            ,this.transactionTypeName );


        //this.doc.rect(8, 5, 580, 805);
    this.doc.setDrawColor(215, 235, 252);
        // this.printService.writeCompanyGST(this.company.gstNumber, this.doc);
        
        //if (imgData) this.doc.addImage(imgData, 'JPEG', 12, 10, 180, 100)
        //this.printService.writeImage(this.imageMetadata, imgData, this.doc);
        // //console.log("sssssss......", this.printService.writeImage(this.imageMetadata, imgData, this.doc));
        this.afterTop = this.doc.autoTable.previous.finalY + 5;
        //Set Title and copy type
        // //console.log("this.copyTypeText.....",this.copyTypeText);
        // //console.log("this.titleText.....",this.titleText);
        // //console.log("this.doc.....",this.doc);
        
        //this.printService.writeTitleAndCopyType(this.copyTypeText, this.titleText, this.doc);
        // this.printService.writeCompanyPhoneAndMobileNumber(this.company.primaryMobile, this.company.primaryTelephone, this.doc);
        

        //Company And address in the header 
        // this.printService.writeCompanyAndAddress(this.company.name, this.company.address,this.company.email, this.company.website, this.company.primaryMobile, this.company.primaryTelephone,  this.doc);

        this.writeCustomerDetails();

        //Customer Delivery address
        //State

        //Invoice Items

        // //console.log("this.globalSetting.itemLevelTax....",this.globalSetting.itemLevelTax);
        // //console.log("this.purchaseOrder.igstTaxAmount....",this.purchaseOrder.igstTaxAmount);
        
        // if (this.globalSetting.itemLevelTax) {
        //     if (this.purchaseOrder.igstTaxAmount > 0) {
        //         this.writePoItemsIGST();
        //     }
        //     else {
        //         this.writePoItems();
        //     }
        // } else {
        //     this.writeItemsWithoutTax();
        // }


        if (this.purchaseOrder.igstTaxAmount > 0) {
            this.printService.writeTransactionItemsIGST(
                this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.copyTypeText
                , this.titleText
                , this.purchaseOrder
                , this.getPoItemColumnHeadersIGST()
                , this.getPoItemColumnsIGST()          
                , this.printService.afterTop
                , this.custTableLastY + 10
                , this.purchaseOrder.purchaseOrderNumber
                , this.purchaseOrder.purchaseOrderDate
                , this.transactionTypeName
                , this.isItemLevelTax
            );
        }
        else {
            // this.writeQuotationItems();


            this.printService.writeTransactionItems(this.doc
                , this.company
                , this.imageMetadata
                , this.imgData
                , this.copyTypeText
                , this.titleText
                , this.purchaseOrder
                , this.getPoItemColumnHeaders()
                , this.getPoItemColumns()
                , this.printService.afterTop
                , this.custTableLastY + 10
                , this.purchaseOrder.purchaseOrderNumber
                , this.purchaseOrder.purchaseOrderDate
                , this.transactionTypeName
                , this.isItemLevelTax)

        }
        this.printService.drawLine(this.doc);
        //Summary section with Amount in words
        this.writeSummary();

        //Terms and conditions, Signature
        //this.writeTermsAndConditionsAndSignature();

    }
    // ........................................................


    // ........................................................

    getCopy(copyT: string) {

        this.copyTypeText = copyT;
        //console.log("copyTypeText....",this.copyTypeText);
        
        this.fillPage();

    }
    // writeTitleAndCopyType(copyTypeText: string, titleText: string) {

    //     var rectangleStartX: number = 400;
    //     var rectangleStartY: number = 10;
    //     var rectangleWidth: number = 150;
    //     var rectangleHeight: number = 20;
    //     this.doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

    //     this.doc.setFontSize(12);


    //     var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (this.doc.getStringUnitWidth(copyTypeText) * this.doc.internal.getFontSize() / 2));
    //     var copyTypeTextOffsetY: number = rectangleStartY + this.doc.internal.getFontSize() / 2; //14;
    //     //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
    //     //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
    //     this.doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);

    //     this.doc.setFontSize(20);
    //     this.doc.setTextColor(40);
    //     this.doc.setFontStyle('normal');

    //     var xOffset: number = (this.doc.internal.pageSize.width / 2) - (this.doc.getStringUnitWidth(titleText) * this.doc.internal.getFontSize() / 2);

    //     this.doc.text(titleText, xOffset, rectangleStartY + (copyTypeTextOffsetY / 2) + this.doc.internal.getFontSize() / 2);


    // }

    openInNewTab() {

        // var string = this.doc.output('datauristring');
        // var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
        // var x = window.open();
        // x.document.open();
        // x.document.write(iframe);
        // x.document.close();
        // return this.doc;


        this.doc.setProperties({
            title: "PO"+this.purchaseOrder.purchaseOrderNumber
          });

          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(this.doc.output("blob"), "PO.pdf");
          } else {
          
            // For other browsers:
            // Create a link pointing to the ObjectURL containing the blob.
            this.doc.autoPrint();
            window.open(
              this.doc.output("bloburl"),
              "_blank"
            );
            //,"height=650,width=500,scrollbars=yes,location=yes"
            // For Firefox it is necessary to delay revoking the ObjectURL
            setTimeout(() => {    
              window.URL.revokeObjectURL(this.doc.output("bloburl"));
            }, 100);
          }

    }














    getBillHeader(): any[] {
        var headerColumns = [

            { title: "PO Number: ", dataKey: "billNumber" },
            { title: "PO Date: ", dataKey: "billDate" },
            { title: "Quotation Number: ", dataKey: "qoNumber" },
            { title: "Quotation Date: ", dataKey: "qoDate" },
            { title: "Ref Doc Number: ", dataKey: "refNumber" },
            { title: "Ref Doc Date: ", dataKey: "refDate" },
        ];

        return headerColumns;
    }

    getBill(): any[] {
        return [
            {

                "billNumber": "PO Number: " + this.purchaseOrder.purchaseOrderNumber,
                "billDate": "PO Date: " + this.datePipe.transform(this.purchaseOrder.purchaseOrderDate, 'dd-MM-yyyy'),
                "qoNumber": "Quotation Number: " + this.purchaseOrder.quotationNumber,
                "qoDate": "Quotation Date: " + this.purchaseOrder.quotationDate,
                "refNumber": "Ref Doc Number: " + this.purchaseOrder.internalReferenceNumber,
                "refDate": "Ref Doc Date: " + this.purchaseOrder.internalReferenceDate,
            }
        ];
    }

    getCustomerAddressHeader(): any[] {

        // var headerColumns = [

        //     { title: "To,", dataKey: "address" },
        // ];
        // return headerColumns;

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {
        this.getLable()
        // return [
        //     {

        //         "address": "To, " + this.purchaseOrder.address || "",
        //     }
        // ];

        return [
            {
                "columnHead": "To:",
                "columnContent": this.purchaseOrder.partyName,
            },
            {
                "columnHead": "Address:",
                "columnContent": this.purchaseOrder.address,
            },
            {
                "columnHead": this.GSTIN,
                "columnContent": this.purchaseOrder.gstNumber,
            }

        ];

    }

    getLable(){

        if(this.transactionTypeName === AppSettings.SUPPLIER_PO) {
            
            this.GSTIN = "Supplier GSTIN:";
        } 
        else
        {
            this.GSTIN = "Customer GSTIN:";
        }
      }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerDeliveryAddress(): any[] {
        let retValue = [
            // {
            //     "columnHead": "PO Number:",
            //     "columnContent": this.purchaseOrder.purchaseOrderNumber,
            // },
            // {
            //     "columnHead": "PO Date: ",
            //     "columnContent": this.datePipe.transform(this.purchaseOrder.purchaseOrderDate, 'dd-MM-yyyy'),
            // },
            {
                "columnHead": "Quot Number: ",
                "columnContent": this.purchaseOrder.quotationNumber,
            },
            {
                "columnHead": "Quot Date: ",
                "columnContent": this.datePipe.transform(this.purchaseOrder.quotationDate, 'dd/MM/yyyy'),
            },
            {
                "columnHead": "Ref number",
                "columnContent": this.purchaseOrder.internalReferenceNumber,
            },
            {
                "columnHead": "Ref date ",
                "columnContent": this.datePipe.transform(this.purchaseOrder.internalReferenceDate, 'dd/MM/yyyy'),
            },
        ];

        return retValue.filter(ret => ret.columnContent);

    }
    writeBillNumberAndDate() {
        var startY: number = this.doc.autoTable.previous.finalY + 25;

        this.doc.autoTable(this.getBillHeader(), this.getBill(), {

            columnStyles: { billDate: { halign: 'right' } },
            startY: startY,
            margin: { left: this.leftMargin, right: this.rightMargin },
            theme: 'plain',
            showHead : 'never',
            styles: {
                halign: 'left',
                fontSize: 12,
                fillColor: [215, 235, 252],
            },

        });
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 10;
        var custAddressTableRightMargin: number = 300;


        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHead : 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 68, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 122 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin + 100, right: this.rightMargin },
            theme: 'plain',
            showHead : 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 0,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 70, halign: 'left' },
                columnContent: { halign: 'left', cellWidth: 70 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

      //this.doc.setDrawColor(215, 235, 252);
       // this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
      //  this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

    }

    getPoItemColumnWithNoTax(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            // { title: "Part Number", dataKey: "partNumber" },
            { title: "Material", dataKey: "materialName" },
            { title: "HSN/ SAC", dataKey: "hsnCode" },
            { title: "Qty", dataKey: "quantity" },
            { title: "Unit", dataKey: "uom" },
            { title: "Price", dataKey: "price" },
        ];
    }

    getPoItemColumnHeaders(): any[] {
        if (this.isItemLevelTax && this.purchaseOrder.partyCountryId==1) {
            return [
                { title: "SL", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },
                { title: "Disc", dataKey: "discountAmount" },
                { title: "CGST \nAmt", dataKey: "cgstAmount" },
                { title: "SGST \nAmt", dataKey: "sgstAmount" },
                { title: "Total", dataKey: "totalAmount" },
            ];
        }
        else if(this.isItemLevelTax && this.purchaseOrder.partyCountryId!=1 && this.purchaseOrder.partyCurrencyId!=0)
        {//items lvel exportInv
            return [
                { title: "SL", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "exportPrice" },
            
                { title: "Disc", dataKey: "discountAmount" },
                { title: "CGST \nAmt", dataKey: "ntApplicableCgst" },
                { title: "SGST \nAmt", dataKey: "ntApplicableIgst" },
                { title: "Amount", dataKey: "exportAmt" },
            ];
        }
        else if(this.purchaseOrder.partyCountryId!=1 && this.purchaseOrder.partyCurrencyId!=0)
        { //header level export inv
            return [
                { title: "SL", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "exportPrice" },
                { title: "Amount", dataKey: "exportAmt" },                
            ]; 
        }
        else{
            return [
                { title: "SL", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },                
            ];
        }
        
    }

    makePoItem(po: PurchaseOrderItem): any {
        let partNumber =  po.partNumber? "Part-No: "+po.partNumber+ "\r\n" : "";
        let specification =po.specification ? "Spec: " + po.specification+ "\r\n" : "";
        let processNames = po.processName ? "Process: "+ po.processName+ "\r\n" : "";
        if(this.isItemLevelTax){
            console.log("makePoItem1")
            return {
                "slNo": po.slNo,
                "materialName": po.partName + "\r\n" +partNumber + specification + processNames ,
                "hsnCode": po.hsnOrSac,
                "quantity": po.quantity + "\r\n" +po.uom,
                "price": this.numberFormatterService.numberF(po.price),
                "amount": this.numberFormatterService.numberF(po.amount),
                "discountAmount": this.numberFormatterService.numberF(po.discountAmount ? po.discountAmount : 0) + "\r\n" + "@"+po.discountPercentage+"%",
                "cgstAmount": this.numberFormatterService.numberF(po.cgstTaxAmount ? po.cgstTaxAmount : 0) + "\r\n" + "@"+po.cgstTaxPercentage+"%",
                "sgstAmount": this.numberFormatterService.numberF(po.sgstTaxAmount ? po.sgstTaxAmount : 0) + "\r\n" + "@"+po.sgstTaxPercentage+"%",
                "totalAmount": this.numberFormatterService.numberF(po.amountAfterTax ? po.amountAfterTax : 0),
                "currencyName":this.currencyName,
                "exportPrice":this.currencySymbol+" "+this.numberFormatterService.numberF(po.price),

                "exportAmt":this.currencySymbol+" "+this.numberFormatterService.numberF(po.amount),
                "ntApplicableCgst":"N/A",
                "ntApplicableIgst":"N/A",
            }
        }
        else{
            console.log("makePoItem2")
            return {
                "slNo": po.slNo,
                "materialName": po.partName + "\r\n" +partNumber + specification + processNames,
                "hsnCode": po.hsnOrSac,
                "quantity": po.quantity + "\r\n" +po.uom,
                "price": this.numberFormatterService.numberF(po.price),
                "amount": this.numberFormatterService.numberF(po.amount), 
                //items level export
                "exportPrice":this.currencySymbol+" "+this.numberFormatterService.numberF(po.price),

                "exportAmt":this.currencySymbol+" "+this.numberFormatterService.numberF(po.amount),
                "ntApplicableCgst":"N/A",
                "ntApplicableIgst":"N/A",
               
            }
        }
            
    }
    getPoItemColumns(): any[] {

        var purchaseOrderItem: any[] = [];
        this.purchaseOrder.purchaseOrderItems.forEach(po => {
            purchaseOrderItem.push(this.makePoItem(po));
        });


        return purchaseOrderItem;
    }

    writeItemsWithoutTax() {
        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.05; //5%
        // var partNumberWidth: number = pageWidth * 0.07; //10%
        var materialNameWidth: number = pageWidth * 0.50; //30% of total width
        var hsnWidth: number = pageWidth * 0.20; //6%
        var qtyWidth: number = pageWidth * 0.10; //6%
        var uomWidth: number = pageWidth * 0.05; //6%
        var priceWidth: number = pageWidth * 0.10;

        //console.log('page width: ', this.doc.internal.pageSize.width);

        var pageContent = function (data) {

            // FOOTER
            if (this.doc) {
                var str = "Page " + data.pageCount;
                if (typeof this.doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }
                this.doc.setFontSize(10);
                this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

            }
        };


        this.doc.autoTable(this.getPoItemColumnWithNoTax(), this.getPoItemColumns(), {
            //addPageContent: pageContent,
            startY: this.custTableLastY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            //theme:'grid',
            theme: 'grid',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                // cellPadding: 0.5,
                fontSize: 10,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                fillColor: [215, 235, 252],
                lineColor: [0, 0, 0],
                fontSize: 10,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                slNo: { columnWidth: slNoWidth, halign: 'right' },
                // partNumber: { columnWidth: partNumberWidth },
                materialName: { columnWidth: materialNameWidth },
                hsnCode: { columnWidth: hsnWidth },
                quantity: { columnWidth: qtyWidth, halign: 'right' },
                uom: { columnWidth: uomWidth },
                price: { columnWidth: priceWidth, halign: 'right' },
            }
            // drawCell: function (cell, data) {
            //   //console.log('cell before:: ', cell);
            //   if (data.column.index % 2 === 1) {
            //     cell.styles.fillColor = "[215, 235, 252]";
            //   }
            //   //console.log('cell:: ', cell);
            // },
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(0, 0, 0);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        //this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // Total page number plugin only available in jspdf v1.0+
        if (typeof this.doc.putTotalPages === 'function') {
            this.doc.putTotalPages(totalPagesExp);
        }

    }

    // writePoItems() {

    //     var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    //     // var slNoWidth: number = pageWidth * 0.02; //5%
    //     // //var partNumberWidth: number = pageWidth * 0.07; //10%
    //     // var materialNameWidth: number = pageWidth * 0.20; //30% of total width
    //     // var hsnWidth: number = pageWidth * 0.07; //6%
    //     // var qtyWidth: number = pageWidth * 0.03; //6%
    //     // var uomWidth: number = pageWidth * 0.05; //6%
    //     // var priceWidth: number = pageWidth * 0.09; // 8%
    //     // var amountWidth: number = pageWidth * 0.09; //8%
    //     // var cgstPercentWidth: number = pageWidth * 0.05; //5%
    //     // var cgstAmountWidth: number = pageWidth * 0.09; //8%
    //     // var sgstPercentWidth: number = pageWidth * 0.05; //5%
    //     // var sgstAmountWidth: number = pageWidth * 0.09; //8%
    //     // var totalAmountWidth: number = pageWidth * 0.1; //10%

    //     var slNoWidth: number = pageWidth * 0.04; //5%
    //     // var partNumberWidth: number = pageWidth * 0.07; //10%
    //     var materialNameWidth: number = pageWidth * 0.25; //30% of total width
    //     var hsnWidth: number = pageWidth * 0.07; //6%
    //     var qtyWidth: number = pageWidth * 0.05; //6%
    //     var uomWidth: number = pageWidth * 0.05; //6%
    //     var priceWidth: number = pageWidth * 0.07; // 8%
    //     var amountWidth: number = pageWidth * 0.09; //8%
    //     var cgstPercentWidth: number = pageWidth * 0.05; //5%
    //     var cgstAmountWidth: number = pageWidth * 0.09; //8%
    //     var sgstPercentWidth: number = pageWidth * 0.05; //5%
    //     var sgstAmountWidth: number = pageWidth * 0.09; //8%
    //     var totalAmountWidth: number = pageWidth * 0.1; //10%

    //     //console.log('page width: ', this.doc.internal.pageSize.width);

    //     var pageContent = function (data) {

    //         // FOOTER
    //         if (this.doc) {
    //             var str = "Page " + data.pageCount;
    //             if (typeof this.doc.putTotalPages === 'function') {
    //                 str = str + " of " + totalPagesExp;
    //             }
    //             this.doc.setFontSize(10);
    //             this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

    //         }
    //     };


    //     this.doc.autoTable(this.getPoItemColumnHeaders(), this.getPoItemColumns(), {
    //         //addPageContent: pageContent,
    //         startY: this.custTableLastY + 5,
    //         margin: { left: this.leftMargin, right: this.rightMargin },
    //         //theme:'grid',
    //         theme: 'grid',
    //         tableWidth: 'auto',
    //         //tableLineColor: [0,0,0],
    //         //tableLineWidth: 0.5,
    //         styles: {
    //             // cellPadding: 0.5,
    //             fontSize: 8,
    //             lineColor: [0, 0, 0],
    //             fillStyle: 'DF',
    //             //halign: 'right',
    //             overflow: 'linebreak',
    //             overflowColumns: false
    //         },
    //         headerStyles: {
    //             fillColor: [215, 235, 252],
    //             lineColor: [0, 0, 0],
    //             fontSize: 9,
    //             textColor: 0,
    //             fontStyle: 'normal',
    //             halign: 'center',
    //         },
    //         columnStyles: {
    //             slNo: { columnWidth: slNoWidth, halign: 'right' },
    //             //partNumber: { columnWidth: partNumberWidth },
    //             materialName: { columnWidth: materialNameWidth },
    //             hsnCode: { columnWidth: hsnWidth },
    //             quantity: { columnWidth: qtyWidth, halign: 'right' },
    //             uom: { columnWidth: uomWidth },
    //             price: { columnWidth: priceWidth, halign: 'right' },
    //             amount: { columnWidth: amountWidth, halign: 'right' },
    //             cgstPercent: { columnWidth: cgstPercentWidth, halign: 'right' },
    //             cgstAmount: { columnWidth: cgstAmountWidth, halign: 'right' },
    //             sgstPercent: { columnWidth: sgstPercentWidth, halign: 'right' },
    //             sgstAmount: { columnWidth: sgstAmountWidth, halign: 'right' },
    //             totalAmount: { columnWidth: totalAmountWidth, halign: 'right' }

    //         }
    //         // drawCell: function (cell, data) {
    //         //   //console.log('cell before:: ', cell);
    //         //   if (data.column.index % 2 === 1) {
    //         //     cell.styles.fillColor = "[215, 235, 252]";
    //         //   }
    //         //   //console.log('cell:: ', cell);
    //         // },
    //     });

    //     this.doc.setLineWidth(1);
    //     this.doc.setDrawColor(215, 235, 252);
    //     //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

    //     //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
    //     this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
    //     // Total page number plugin only available in jspdf v1.0+
    //     if (typeof this.doc.putTotalPages === 'function') {
    //         this.doc.putTotalPages(totalPagesExp);
    //     }



    // }


    getPoItemColumnHeadersIGST(): any[] {
        if(this.isItemLevelTax){
            return [
                { title: "NO", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },
                { title: "Discount", dataKey: "discountAmount" },
                { title: "IGST \nAmt", dataKey: "igstAmount" },
                { title: "Total", dataKey: "totalAmount" },
            ];
        }else{
            return [
                { title: "NO", dataKey: "slNo" },
                { title: "Material", dataKey: "materialName" },
                { title: "HSN/ SAC", dataKey: "hsnCode" },
                { title: "Qty", dataKey: "quantity" },
                { title: "Price", dataKey: "price" },
                { title: "Amount", dataKey: "amount" },              
            ];
        }
    }

    makePoItemIGST(po: PurchaseOrderItem): any {
        let partNumber =  po.partNumber? "Part-No: "+ po.partNumber+ "\r\n" : "";
        let specification =po.specification ? "Spec: " + po.specification+ "\r\n" : "";
        let processNames = po.processName ? "Process: "+po.processName+ "\r\n" : "";
        if(this.isItemLevelTax){
            return {
                "slNo": po.slNo,
                "materialName": po.partName + "\r\n" +partNumber + specification + processNames ,
                "hsnCode": po.hsnOrSac,
                "quantity": po.quantity+ "\r\n" + po.uom,  // ? po.quantity.toPrecision() : 0,
                "price":  po.price, //? po.price.toPrecision():0,
                "amount": po.amount, //? po.amount.toPrecision():0,
                "discountAmount": this.numberFormatterService.numberF(po.discountAmount ? po.discountAmount : 0) + "\r\n" + "@" + po.discountPercentage + "%",
                "igstAmount": this.numberFormatterService.numberF(po.igstTaxAmount ? po.igstTaxAmount : 0) + "\r\n" + "@"+ po.igstTaxPercentage +"%",    
                "totalAmount": po.amountAfterTax //? po.amountAfterTax.toPrecision() : 0
            }
        }else{
            return {
                "slNo": po.slNo,
                "materialName": po.partName + "\r\n" +partNumber + specification + processNames ,
                "hsnCode": po.hsnOrSac,
                "quantity": po.quantity+ "\r\n" + po.uom,  // ? po.quantity.toPrecision() : 0,
                "price":  po.price, //? po.price.toPrecision():0,
                "amount": po.amount, //? po.amount.toPrecision():0,                
            }
        }
            
    }

    getPoItemColumnsIGST(): any[] {

        var purchaseOrderItem: any[] = [];
        this.purchaseOrder.purchaseOrderItems.forEach(po => {
            purchaseOrderItem.push(this.makePoItemIGST(po));
        });


        return purchaseOrderItem;
    }

    writePoItemsIGST() {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        // var slNoWidth: number = pageWidth * 0.02; //5%
        // var partNumberWidth: number = pageWidth * 0.07; //10%
        // var materialNameWidth: number = pageWidth * 0.34; //30% of total width
        // var hsnWidth: number = pageWidth * 0.07; //6%
        // var qtyWidth: number = pageWidth * 0.03; //6%
        // var uomWidth: number = pageWidth * 0.05; //6%
        // var priceWidth: number = pageWidth * 0.09; // 8%
        // var amountWidth: number = pageWidth * 0.09; //8%
        // var igstPercentWidth: number = pageWidth * 0.05; //5%
        // var igstAmountWidth: number = pageWidth * 0.09; //8%
        // //var sgstPercentWidth: number = pageWidth * 0.05; //5%
        // //var sgstAmountWidth: number = pageWidth * 0.09; //8%
        // var totalAmountWidth: number = pageWidth * 0.1; //10%

        var slNoWidth: number = pageWidth * 0.04; //5%
        // var partNumberWidth: number = pageWidth * 0.07; //10%
        var materialNameWidth: number = pageWidth * 0.35; //30% of total width
        var hsnWidth: number = pageWidth * 0.08; //6%
        var qtyWidth: number = pageWidth * 0.05; //6%
        var uomWidth: number = pageWidth * 0.05; //6%
        var priceWidth: number = pageWidth * 0.09; // 8%
        var amountWidth: number = pageWidth * 0.09; //8%
        var igstPercentWidth: number = pageWidth * 0.06; //5%
        var igstAmountWidth: number = pageWidth * 0.09; //8%
        //var sgstPercentWidth: number = pageWidth * 0.05; //5%
        //var sgstAmountWidth: number = pageWidth * 0.09; //8%
        var totalAmountWidth: number = pageWidth * 0.1; //10%


        //console.log('page width: ', this.doc.internal.pageSize.width);

        var pageContent = function (data) {

            // FOOTER
            if (this.doc) {
                var str = "Page " + data.pageCount;
                if (typeof this.doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }
                this.doc.setFontSize(10);
                this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

            }
        };


        this.doc.autoTable(this.getPoItemColumnHeadersIGST(), this.getPoItemColumnsIGST(), {
            //addPageContent: pageContent,
            startY: this.custTableLastY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            //theme:'grid',
            theme: 'grid',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                //cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                fillColor: [215, 235, 252],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'center',
            },
            // ......................................................................
            columnStyles: {
                slNo: { columnWidth: slNoWidth, halign: 'right' },
                // partNumber: { columnWidth: partNumberWidth },
                materialName: { columnWidth: materialNameWidth },
                hsnCode: { columnWidth: hsnWidth },
                quantity: { columnWidth: qtyWidth, halign: 'right' },
                uom: { columnWidth: uomWidth },
                price: { columnWidth: priceWidth, halign: 'right' },
                amount: { columnWidth: amountWidth, halign: 'right' },
                igstPercent: { columnWidth: igstPercentWidth, halign: 'right' },
                igstAmount: { columnWidth: igstAmountWidth, halign: 'right' },
                totalAmount: { columnWidth: totalAmountWidth, halign: 'right' }

            }
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(215, 235, 252);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // Total page number plugin only available in jspdf v1.0+
        if (typeof this.doc.putTotalPages === 'function') {
            this.doc.putTotalPages(totalPagesExp);
        }



    }

    getSummaryColumnHeaders(): any[] {
        return [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];
    }

    getSummaryColumns(): any[] {


        if(this.purchaseOrder.partyCountryId!=1)
        {
            this.totalAmount = this.numberFormatterService.numberC(this.purchaseOrder.subTotalAmount ? this.purchaseOrder.subTotalAmount : this.purchaseOrder.totalTaxableAmount); //"232392382.00";

         this.discountAmount = this.numberFormatterService.numberC(this.purchaseOrder.totalDiscount ? this.purchaseOrder.totalDiscount : 0); // "0.00";
        this.amountAfterDiscount = this.numberFormatterService.numberC(this.purchaseOrder.totalTaxableAmount); //"232392382.00";
        this.grandTotal = this.numberFormatterService.numberC(this.purchaseOrder.grandTotal);

        }
        else{
            this.totalAmount = this.numberFormatterService.numberF(this.purchaseOrder.subTotalAmount ? this.purchaseOrder.subTotalAmount : this.purchaseOrder.totalTaxableAmount); //"232392382.00";

            this.discountAmount = this.numberFormatterService.numberF(this.purchaseOrder.totalDiscount ? this.purchaseOrder.totalDiscount : 0); // "0.00";
        this.amountAfterDiscount = this.numberFormatterService.numberF(this.purchaseOrder.totalTaxableAmount); //"232392382.00";
        this.grandTotal = this.numberFormatterService.numberF(this.purchaseOrder.grandTotal);

        }
        this.cgstAmount = this.numberFormatterService.numberF(this.purchaseOrder.cgstTaxAmount);
        this.sgstAmount = this.numberFormatterService.numberF(this.purchaseOrder.sgstTaxAmount);
        this.igstAmount = this.numberFormatterService.numberF(this.purchaseOrder.igstTaxAmount);
        this.roundOffAmount = this.numberFormatterService.numberF(this.purchaseOrder.roundOffAmount ? this.purchaseOrder.roundOffAmount :0);
        let itemDiscPer: number[] = this.purchaseOrder.purchaseOrderItems.map(item => item.discountPercentage);
        let distinctPer = Array.from( new Set(itemDiscPer))
    
        let printDiscount = 0;
        if(distinctPer.length === 1 || distinctPer.length === 0){
          printDiscount = distinctPer.pop();
        }else{
          printDiscount = this.purchaseOrder.discountPercent;
        }
        if(this.purchaseOrder.discountPercent > 0){
            printDiscount =  this.purchaseOrder.discountPercent;
          }
        let discountHead : string =  "Discount @"+ + printDiscount+ "%"
        let sgstHead: string ;
        let igstHead: string ;
        let cgstHead: string ;
        if(this.purchaseOrder.partyCountryId==1)
        {
        sgstHead = this.isItemLevelTax ? "SGST" : "SGST @"+this.purchaseOrder.sgstTaxRate+" %"
      
        igstHead  = this.isItemLevelTax ? "CGST" : "CGST @"+this.purchaseOrder.cgstTaxRate+" %"
       
        cgstHead = this.isItemLevelTax ? "IGST" : "IGST @"+this.purchaseOrder.igstTaxRate+" %"
        }

        if(this.purchaseOrder.inclusiveTax && +this.purchaseOrder.inclusiveTax > 0 && this.purchaseOrder.totalDiscount > 0){
            this.totalAmount = this.numberFormatterService.numberF(+this.purchaseOrder.totalTaxableAmount + +this.purchaseOrder.totalDiscount);
        }
        if(this.purchaseOrder.partyCountryId==1)
        {

        
        return [
            {
                "key": "Total Amount",
                "data": this.totalAmount
            },
            {
                "key": discountHead,
                "data": this.discountAmount
            },
            {
                "key": "Amount After Discount",
                "data": this.amountAfterDiscount
            },
            {
                "key": cgstHead,
                "data": this.cgstAmount
            },
            {
                "key": sgstHead,
                "data": this.sgstAmount
            },
            {
                "key": igstHead,
                "data": this.igstAmount
            },
            {
                "key": "Round Off",
                "data": this.roundOffAmount
            },
            {
                "key": "GRAND TOTAL",
                "data": this.grandTotal
            },
        ];
    }
    else
    {
        return [
            {
                "key": "Total Amount",
                "data": this.currencySymbol+" "+this.totalAmount
            },
            {
                "key": discountHead,
                "data": this.currencySymbol+" "+this.discountAmount
            },
            {
                "key": "Amount After Discount",
                "data": this.currencySymbol+" "+this.amountAfterDiscount
            },
            {
                "key": "Round Off",
                "data": this.roundOffAmount
            },
            {
                "key": "GRAND TOTAL",
                "data":this.currencySymbol+" "+this.grandTotal
            },
        ];

    }
    }

    writeSummary() {

        if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {

            this.doc.addPage();
            //Set Title and copy type
           // this.printService.writeTitleAndCopyType(this.copyTypeText, this.titleText, this.doc);

           this.printService.topSectionWithoutCompany(this.doc
            , this.company
            , this.imageMetadata
            , this.imgData
            , this.copyTypeText
            , this.titleText
            , this.purchaseOrder
            , this.printService
            , this.purchaseOrder.purchaseOrderNumber
            , this.purchaseOrder.purchaseOrderDate
            ,this.transactionTypeName);
           
            //Company And address in the header 
            //this.writeCompanyAndAddress();
            // this.printService.writeCompanyAndAddress(this.company.name, this.company.address,  this.company.email,this.company.website, this.company.primaryMobile, this.company.primaryTelephone, this.doc);
            this.summaryTableStartY = 250;
            
                        if (typeof this.doc.putTotalPages === 'function') {
                            this.doc.putTotalPages(totalPagesExp);
                        }

            //Bill number and date
           // this.writeBillNumberAndDate();
        }

        this.doc.autoTable(this.getSummaryColumnHeaders(), this.getSummaryColumns(), {
            startY: this.summaryTableStartY,//this.doc.autoTable.previous.finalY + 25,
            margin: { left: this.summaryTableLeftMargin, right: this.rightMargin },
            showHead : 'never',
            theme: 'plain',
            styles: {
                halign: 'right',
                valign: 'bottom',
                fontStyle: 'bold',
                cellPadding: 0,
            },
            columnStyles: {
                // key: { fillColor: [215, 235, 252] }
            },
        });

        this.currId=this.numberFormatterService.numberI(this.purchaseOrder.partyCurrencyId)

      // let words :string  =this.exportNumberToWordsService.exportNmber2text(this.purchaseOrder.grandTotal,+this.currId);
       //console.log("words"+words)
        if(+this.currId!=0)
        {
          this.getCurrencyData(+this.currId);
         
          this.printService.writeTermsAndConditionAndDeliveryAndPaymentTerms(this.purchaseOrder.deliveryTerms, this.purchaseOrder.paymentTerms, this.purchaseOrder.termsAndConditions,this.exportNumberToWordsService.exportNmber2text(this.purchaseOrder.grandTotal,+this.currId),this.doc, tableHeight,this.summaryTableStartY, this.summaryTableLeftMargin, this.company.name,this.company, false, this.signMetadata, this.signData, this.printService);

        }
        else{
            this.printService.writeTermsAndConditionAndDeliveryAndPaymentTerms(this.purchaseOrder.deliveryTerms, this.purchaseOrder.paymentTerms, this.purchaseOrder.termsAndConditions,  this.numberToWordsService.number2text(this.purchaseOrder.grandTotal),this.doc, tableHeight,this.summaryTableStartY, this.summaryTableLeftMargin, this.company.name,this.company, false, this.signMetadata, this.signData, this.printService);

        } 
          

      
       
          //console.log("amountInwords = "+amountInwords);
      
         

  
        var tableHeight: number = this.doc.autoTable.previous.finalY - this.summaryTableStartY;
       // this.printService.writeTermsAndConditionAndDeliveryAndPaymentTerms(this.doc, this.purchaseOrder, tableHeight, this.summaryTableStartY,this.summaryTableLeftMargin);
       
      
        
          }
          getCurrencys() {
           
            this.currencyService.getAllCurrency().subscribe(
                response => {
                    this.currencys = response;
                    this.currencys.forEach(currencys => {
                      this.currencyName=currencys.currencyName;
                      this.currencyFraction=currencys.currencyFraction;
                      this.currencyDecimal=currencys.currencyDecimal;
                      this.currencySymbol=currencys.currencySymbol;
                     // console.log("="+this.currencyFraction)
                    
                });
                
              
          
        })
          }
         getCurrencyData(currId:number){
          
          console.log(" currId "+currId)
          
          if(+currId!=0)
          {
          this.currency= this.currencys.filter(data => data.id===+currId)[0];
          
          this.currencyFraction=this.currency.currencyFraction;
          this.currencyDecimal=this.currency.currencyDecimal;
          this.currencySymbol = this.currency.currencySymbol;
          this.currencyName=this.currency.currencyName;
          }
        
        }
    }
        