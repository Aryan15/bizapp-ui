import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Company } from '../../data-model/company-model';
import { PurchaseOrderHeader, PurchaseOrderItem } from '../../data-model/purchase-order-model';
import { NumberToWordsService } from '../../services/number-to-words.service';

declare var jsPDF: any;


@Injectable()
export class PurchaseOrderReportService {

    purchaseOrder: PurchaseOrderHeader;
    company: Company;

    datePipe = new DatePipe('en-US');

    doc = new jsPDF('p', 'pt');
    totalPagesExp = "{total_pages-count-string}";

    titleText: string = "PURCHASE ORDER";
    copyTypeText: string = "ORIGINAL";

    leftMargin: number = 15;
    rightMargin: number = 20;

    custTableLastY: number;
    summaryTableStartY: number = 500;
    summaryTableLeftMargin: number = 350;


    totalAmount: number;
    discountAmount: number;
    amountAfterDiscount: number;
    cgstAmount: number;
    sgstAmount: number;
    igstAmount: number;
    roundOffAmount: number;
    grandTotal: number;

    constructor( @Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService) { }


    ngOnInit() {

    }

    download(purchaseOrder: PurchaseOrderHeader, company: Company) {
        this.doc = new jsPDF('p', 'pt');

        this.purchaseOrder = purchaseOrder;
        this.company = company;

        this.doc.setDrawColor(215, 235, 252);

        //Set Title and copy type
        this.writeTitleAndCopyType(this.copyTypeText, this.titleText);


        //Company And address in the header 
        this.writeCompanyAndAddress();


        //Bill number and date
        this.writeBillNumberAndDate();

        //Customer Details
        //Name
        //Address
        //State
        //GSTN
        this.writeCustomerDetails();

        //Customer Delivery address
        //State

        //Invoice Items

        if (this.purchaseOrder.igstTaxAmount > 0) {
            this.writePoItemsIGST();
        }
        else {
            this.writePoItems();
        }
        //Summary section with Amount in words
        this.writeSummary();

        //Terms and conditions, Signature
        this.writeTermsAndConditionsAndSignature();


        //Open pdf in a tab
        this.openInNewTab();

    }
    writeTitleAndCopyType(copyTypeText: string, titleText: string) {

        var rectangleStartX: number = 400;
        var rectangleStartY: number = 10;
        var rectangleWidth: number = 150;
        var rectangleHeight: number = 20;
        this.doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

        this.doc.setFontSize(12);


        var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (this.doc.getStringUnitWidth(copyTypeText) * this.doc.internal.getFontSize() / 2));
        var copyTypeTextOffsetY: number = rectangleStartY + this.doc.internal.getFontSize() / 2; //14;
        //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
        //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
        this.doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);

        this.doc.setFontSize(20);
        this.doc.setTextColor(40);
        this.doc.setFontStyle('normal');

        var xOffset: number = (this.doc.internal.pageSize.width / 2) - (this.doc.getStringUnitWidth(titleText) * this.doc.internal.getFontSize() / 2);

        this.doc.text(titleText, xOffset, rectangleStartY + (copyTypeTextOffsetY / 2) + this.doc.internal.getFontSize() / 2);


    }

    openInNewTab() {

        var string = this.doc.output('datauristring');
        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
        var x = window.open();
        x.document.open();
        x.document.write(iframe);
        x.document.close();
        return this.doc;
    }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(): any[] {

        return [
            {
                "companyName": this.company.name, //"Uthkrushta Technologies",
            }
        ];
    }


    getAddressHeader(): any[] {

        var headerColumns = [

            { title: "Company Address", dataKey: "companyAddress" },
        ];

        return headerColumns;

    }


    getAddress(): any[] {

        return [
            {

                "companyAddress": this.company.address, //"Nagendra Block, BSK 3rd Stage, 29203882038"
            }
        ];
    }

    getGSTHeader(): any[] {
        var headerColumns = [

            { title: "GST", dataKey: "GST" },
        ];

        return headerColumns;
    }

    getGST(): any[] {
        return [
            {

                "GST": this.company.gstNumber, //"XXXX"
            }
        ];
    }

    writeCompanyAndAddress() {

        var startCompanyY: number = 55;
        //var startAddressY: number = 80;

        this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

            startY: startCompanyY,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'bold',
                fontSize: 12,
            }

        });

        this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });


        this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });

    }

    getBillHeader(): any[] {
        var headerColumns = [

            { title: "Bill Number", dataKey: "billNumber" },
            { title: "Date", dataKey: "billDate" },
        ];

        return headerColumns;
    }

    getBill(): any[] {
        return [
            {

                "billNumber": "Bill Number: " + this.purchaseOrder.purchaseOrderNumber,
                "billDate": "Date: " + this.datePipe.transform(this.purchaseOrder.purchaseOrderDate, 'dd-MM-yyyy'),
            }
        ];
    }

    getCustomerAddressHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {

        return [
            {
                "columnHead": "Bill To",
                "columnContent": this.purchaseOrder.partyName,
            },
            {
                "columnHead": "Address",
                "columnContent": this.purchaseOrder.address,
            },
            {
                "columnHead": "Customer GSTN",
                "columnContent": this.purchaseOrder.gstNumber,
            }

        ];

    }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getCustomerDeliveryAddress(): any[] {
        return [
            {
                "columnHead": "Shipping Address",
                "columnContent": this.purchaseOrder.shipToAddress ? this.purchaseOrder.shipToAddress : this.purchaseOrder.address,
            },


        ];
    }
    writeBillNumberAndDate() {
        var startY: number = this.doc.autoTable.previous.finalY + 25;

        this.doc.autoTable(this.getBillHeader(), this.getBill(), {

            columnStyles: { billDate: { halign: 'right' } },
            startY: startY,
            margin: { left: this.leftMargin, right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 12,
                fillColor: [215, 235, 252],
            },

        });
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 5;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin, right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

    }

    getPoItemColumnHeaders(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            { title: "Part Number", dataKey: "partNumber" },
            { title: "Material", dataKey: "materialName" },
            { title: "HSN/ SAC", dataKey: "hsnCode" },
            { title: "Qty", dataKey: "quantity" },
            { title: "Unit", dataKey: "uom" },
            { title: "Price", dataKey: "price" },
            { title: "Amount", dataKey: "amount" },
            { title: "CGST \n%", dataKey: "cgstPercent" },
            { title: "CGST \nAmt", dataKey: "cgstAmount" },
            { title: "SGST \n%", dataKey: "sgstPercent" },
            { title: "SGST \nAmt", dataKey: "sgstAmount" },
            // {title: "IGST%", dataKey: "igstPercent"},
            // {title: "IGST Amt", dataKey: "igstAmount"},
            { title: "Total", dataKey: "totalAmount" },
        ];
    }

    makePoItem(po: PurchaseOrderItem): any {

        return {
            "slNo": po.slNo,
            "partNumber": po.partNumber,
            "materialName": po.partName,
            "hsnCode": po.hsnOrSac,
            "quantity": po.quantity.toPrecision(2),
            "uom": po.uom,
            "price": po.price.toPrecision(),
            "amount": po.amount.toPrecision(),
            "cgstPercent": po.cgstTaxPercentage ? po.cgstTaxPercentage.toPrecision() : 0,
            "cgstAmount": po.cgstTaxAmount ? po.cgstTaxAmount.toPrecision() : 0,
            "sgstPercent": po.sgstTaxPercentage ? po.sgstTaxPercentage.toPrecision() : 0,
            "sgstAmount": po.sgstTaxAmount ? po.sgstTaxAmount.toPrecision() : 0,
            "totalAmount": po.amountAfterTax ? po.amountAfterTax.toPrecision() : 0
        }
    }
    getPoItemColumns(): any[] {

        var purchaseOrderItem: any[] = [];
        this.purchaseOrder.purchaseOrderItems.forEach(po => {
            purchaseOrderItem.push(this.makePoItem(po));
        });


        return purchaseOrderItem;
    }

    writePoItems() {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.02; //5%
        var partNumberWidth: number = pageWidth * 0.07; //10%
        var materialNameWidth: number = pageWidth * 0.20; //30% of total width
        var hsnWidth: number = pageWidth * 0.07; //6%
        var qtyWidth: number = pageWidth * 0.03; //6%
        var uomWidth: number = pageWidth * 0.05; //6%
        var priceWidth: number = pageWidth * 0.09; // 8%
        var amountWidth: number = pageWidth * 0.09; //8%
        var cgstPercentWidth: number = pageWidth * 0.05; //5%
        var cgstAmountWidth: number = pageWidth * 0.09; //8%
        var sgstPercentWidth: number = pageWidth * 0.05; //5%
        var sgstAmountWidth: number = pageWidth * 0.09; //8%
        var totalAmountWidth: number = pageWidth * 0.1; //10%

        //console.log('page width: ', this.doc.internal.pageSize.width);

        var pageContent = function (data) {

            // FOOTER
            if (this.doc) {
                var str = "Page " + data.pageCount;
                if (typeof this.doc.putTotalPages === 'function') {
                    str = str + " of " + this.totalPagesExp;
                }
                this.doc.setFontSize(10);
                this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

            }
        };


        this.doc.autoTable(this.getPoItemColumnHeaders(), this.getPoItemColumns(), {
            //addPageContent: pageContent,
            startY: this.custTableLastY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            //theme:'grid',
            theme: 'plain',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                fillColor: [215, 235, 252],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                slNo: { fillColor: [215, 235, 252], columnWidth: slNoWidth, halign: 'right' },
                partNumber: { columnWidth: partNumberWidth },
                materialName: { fillColor: [232, 241, 248], columnWidth: materialNameWidth },
                hsnCode: { columnWidth: hsnWidth },
                quantity: { fillColor: [232, 241, 248], columnWidth: qtyWidth, halign: 'right' },
                uom: { columnWidth: uomWidth },
                price: { fillColor: [232, 241, 248], columnWidth: priceWidth, halign: 'right' },
                amount: { columnWidth: amountWidth, halign: 'right' },
                cgstPercent: { fillColor: [232, 241, 248], columnWidth: cgstPercentWidth, halign: 'right' },
                cgstAmount: { columnWidth: cgstAmountWidth, halign: 'right' },
                sgstPercent: { fillColor: [232, 241, 248], columnWidth: sgstPercentWidth, halign: 'right' },
                sgstAmount: { columnWidth: sgstAmountWidth, halign: 'right' },
                totalAmount: { fillColor: [215, 235, 252], columnWidth: totalAmountWidth, halign: 'right' }

            }
            // drawCell: function (cell, data) {
            //   //console.log('cell before:: ', cell);
            //   if (data.column.index % 2 === 1) {
            //     cell.styles.fillColor = "[215, 235, 252]";
            //   }
            //   //console.log('cell:: ', cell);
            // },
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(215, 235, 252);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // Total page number plugin only available in jspdf v1.0+
        if (typeof this.doc.putTotalPages === 'function') {
            this.doc.putTotalPages(this.totalPagesExp);
        }



    }


    getPoItemColumnHeadersIGST(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            { title: "Part Number", dataKey: "partNumber" },
            { title: "Material", dataKey: "materialName" },
            { title: "HSN/ SAC", dataKey: "hsnCode" },
            { title: "Qty", dataKey: "quantity" },
            { title: "Unit", dataKey: "uom" },
            { title: "Price", dataKey: "price" },
            { title: "Amount", dataKey: "amount" },
            { title: "IGST \n%", dataKey: "igstPercent" },
            { title: "IGST \nAmt", dataKey: "igstAmount" },
            // {title: "IGST%", dataKey: "igstPercent"},
            // {title: "IGST Amt", dataKey: "igstAmount"},
            { title: "Total", dataKey: "totalAmount" },
        ];
    }

    makePoItemIGST(po: PurchaseOrderItem): any {

        return {
            "slNo": po.slNo,
            "partNumber": po.partNumber,
            "materialName": po.partName,
            "hsnCode": po.hsnOrSac,
            "quantity": po.quantity.toPrecision(2),
            "uom": po.uom,
            "price": po.price.toPrecision(),
            "amount": po.amount.toPrecision(),
            "igstPercent": po.igstTaxPercentage ? po.igstTaxPercentage.toPrecision() : 0,
            "igstAmount": po.igstTaxAmount ? po.igstTaxAmount.toPrecision() : 0,
            "totalAmount": po.amountAfterTax ? po.amountAfterTax.toPrecision() : 0
        }
    }

    getPoItemColumnsIGST(): any[] {

        var purchaseOrderItem: any[] = [];
        this.purchaseOrder.purchaseOrderItems.forEach(po => {
            purchaseOrderItem.push(this.makePoItemIGST(po));
        });


        return purchaseOrderItem;
    }

    writePoItemsIGST() {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.02; //5%
        var partNumberWidth: number = pageWidth * 0.07; //10%
        var materialNameWidth: number = pageWidth * 0.34; //30% of total width
        var hsnWidth: number = pageWidth * 0.07; //6%
        var qtyWidth: number = pageWidth * 0.03; //6%
        var uomWidth: number = pageWidth * 0.05; //6%
        var priceWidth: number = pageWidth * 0.09; // 8%
        var amountWidth: number = pageWidth * 0.09; //8%
        var igstPercentWidth: number = pageWidth * 0.05; //5%
        var igstAmountWidth: number = pageWidth * 0.09; //8%
        //var sgstPercentWidth: number = pageWidth * 0.05; //5%
        //var sgstAmountWidth: number = pageWidth * 0.09; //8%
        var totalAmountWidth: number = pageWidth * 0.1; //10%

        //console.log('page width: ', this.doc.internal.pageSize.width);

        var pageContent = function (data) {

            // FOOTER
            if (this.doc) {
                var str = "Page " + data.pageCount;
                if (typeof this.doc.putTotalPages === 'function') {
                    str = str + " of " + this.totalPagesExp;
                }
                this.doc.setFontSize(10);
                this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

            }
        };


        this.doc.autoTable(this.getPoItemColumnHeadersIGST(), this.getPoItemColumnsIGST(), {
            //addPageContent: pageContent,
            startY: this.custTableLastY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            //theme:'grid',
            theme: 'plain',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                fillColor: [215, 235, 252],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                slNo: { fillColor: [215, 235, 252], columnWidth: slNoWidth, halign: 'right' },
                partNumber: { columnWidth: partNumberWidth },
                materialName: { fillColor: [232, 241, 248], columnWidth: materialNameWidth },
                hsnCode: { columnWidth: hsnWidth },
                quantity: { fillColor: [232, 241, 248], columnWidth: qtyWidth, halign: 'right' },
                uom: { columnWidth: uomWidth },
                price: { fillColor: [232, 241, 248], columnWidth: priceWidth, halign: 'right' },
                amount: { columnWidth: amountWidth, halign: 'right' },
                igstPercent: { fillColor: [232, 241, 248], columnWidth: igstPercentWidth, halign: 'right' },
                igstAmount: { columnWidth: igstAmountWidth, halign: 'right' },
                totalAmount: { fillColor: [215, 235, 252], columnWidth: totalAmountWidth, halign: 'right' }

            }
            // drawCell: function (cell, data) {
            //   //console.log('cell before:: ', cell);
            //   if (data.column.index % 2 === 1) {
            //     cell.styles.fillColor = "[215, 235, 252]";
            //   }
            //   //console.log('cell:: ', cell);
            // },
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(215, 235, 252);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // Total page number plugin only available in jspdf v1.0+
        if (typeof this.doc.putTotalPages === 'function') {
            this.doc.putTotalPages(this.totalPagesExp);
        }



    }

    getSummaryColumnHeaders(): any[] {
        return [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];
    }

    getSummaryColumns(): any[] {


        this.totalAmount = this.purchaseOrder.totalTaxableAmount ? this.purchaseOrder.totalTaxableAmount : this.purchaseOrder.totalTaxableAmount; //"232392382.00";
        this.discountAmount = this.purchaseOrder.totalDiscount ? this.purchaseOrder.totalDiscount : 0; // "0.00";
        this.amountAfterDiscount = this.purchaseOrder.totalTaxableAmount; //"232392382.00";
        this.cgstAmount = this.purchaseOrder.cgstTaxAmount;
        this.sgstAmount = this.purchaseOrder.sgstTaxAmount;
        this.igstAmount = this.purchaseOrder.igstTaxAmount;
        this.roundOffAmount = this.purchaseOrder.roundOffAmount ? this.purchaseOrder.roundOffAmount : this.purchaseOrder.grandTotal;
        this.grandTotal = this.purchaseOrder.grandTotal;

        return [
            {
                "key": "Total Amount:",
                "data": this.totalAmount
            },
            {
                "key": "Discount Amount:",
                "data": this.discountAmount
            },
            {
                "key": "Amount After Discount",
                "data": this.amountAfterDiscount
            },
            {
                "key": "CGST",
                "data": this.cgstAmount
            },
            {
                "key": "SGST",
                "data": this.sgstAmount
            },
            {
                "key": "IGST",
                "data": this.igstAmount
            },
            {
                "key": "Round Off",
                "data": this.roundOffAmount
            },
            {
                "key": "GRAND TOTAL",
                "data": this.grandTotal
            },
        ];
    }

    writeSummary() {

        if (this.summaryTableStartY < this.doc.autoTable.previous.finalY + 25) {

            this.doc.addPage();
            //Set Title and copy type
            this.writeTitleAndCopyType(this.copyTypeText, this.titleText);


            //Company And address in the header 
            this.writeCompanyAndAddress();


            //Bill number and date
            this.writeBillNumberAndDate();
        }

        this.doc.autoTable(this.getSummaryColumnHeaders(), this.getSummaryColumns(), {
            startY: this.summaryTableStartY,//this.doc.autoTable.previous.finalY + 25,
            margin: { left: this.summaryTableLeftMargin, right: this.rightMargin },
            showHeader: 'never',
            theme: 'plain',
            styles: {
                halign: 'right',
                valign: 'bottom',
                fontStyle: 'bold',
            },
            columnStyles: {
                // key: { fillColor: [215, 235, 252] }
            },
        });

        var tableHeight: number = this.doc.autoTable.previous.finalY - this.summaryTableStartY;

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.summaryTableLeftMargin, this.summaryTableStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.summaryTableLeftMargin), tableHeight);

        this.doc.setFontSize(10);
        this.doc.rect(this.leftMargin, this.summaryTableStartY, (this.summaryTableLeftMargin - this.leftMargin), tableHeight);
        // this.doc.setFontStyle('bold');
        // this.doc.text("Amount in Words: ", this.leftMargin + 5, this.doc.autoTable.previous.finalY - 65 )
        // this.doc.setFontStyle('normal');
        // this.doc.text(this.numberToWordsService.number2text(this.purchaseOrder.grandTotal), this.leftMargin + 5, this.doc.autoTable.previous.finalY - 45 );

        let amountHeader = [
            { title: "dummy", dataKey: "key" },
            { title: "dummy", dataKey: "data" },
        ];

        let amount = [
            {
                "key": "Amount in Words:",
                "data": this.numberToWordsService.number2text(this.purchaseOrder.grandTotal)
            }
        ]
        this.doc.autoTable(amountHeader, amount, {
            startY: this.doc.autoTable.previous.finalY - 45,
            margin: { left: this.leftMargin, right: (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + this.summaryTableLeftMargin)) },
            showHeader: 'never',
            theme: 'plain',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                key: { fontStyle: 'bold', }
            },
        })

    }


    writeTermsAndConditionsAndSignature() {

        var tableHeight = this.doc.internal.pageSize.height - (this.doc.autoTable.previous.finalY + 50);

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.leftMargin, this.doc.autoTable.previous.finalY + 10, (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin)), tableHeight);

        let tncHeader = [
            { title: "Terms And Conditions", dataKey: "tnc" },
        ];

        let tncDetails = [
            {
                "tnc": this.purchaseOrder.termsAndConditions ? this.purchaseOrder.termsAndConditions : "",

            }
        ];

        let tableStart: number = this.doc.autoTable.previous.finalY + 10;

        this.doc.autoTable(tncHeader, tncDetails, {
            startY: tableStart,
            margin: { left: this.leftMargin, right: (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + this.summaryTableLeftMargin)) },
            theme: 'plain',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },

        });

        let signatureHeader = [
            { title: "key", dataKey: "data" },
        ];

        let signature = [
            {
                "data": "For " + this.company.name,
            },
            {
                "data": "",
            },
            {
                "data": "Authorised Signature"
            }
        ];


        tableStart += 20;

        this.doc.autoTable(signatureHeader, signature, {
            startY: tableStart,
            margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            drawRow: function (row, data) {
                if (row.index === 0) {
                    //this.doc.setFontStyle('bold');
                    //console.log(data);
                    data.doc.setFontStyle('bold');
                    data.settings.styles = { fontStyle: 'bold' };
                }
            }


        });

    }
}
