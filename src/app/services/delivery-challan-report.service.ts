import { DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Company } from '../data-model/company-model';
import { DeliveryChallanHeader, DeliveryChallanItem } from '../data-model/delivery-challan-model';
import { NumberToWordsService } from '../services/number-to-words.service';


declare var jsPDF: any;


@Injectable()
export class DeliveryChallanReportService {

    deliveryChallan: DeliveryChallanHeader;
    company: Company;

    datePipe = new DatePipe('en-US');

    doc = new jsPDF('p', 'pt');
    totalPagesExp = "{total_pages-count-string}";

    titleText: string = "DELIVERY CHALLAN";
    copyTypeText: string = "ORIGINAL";

    leftMargin: number = 15;
    rightMargin: number = 20;

    custTableLastY: number;
    summaryTableStartY: number = 500;
    summaryTableLeftMargin: number = 350;



    constructor( @Inject('Window') private window: Window,
        private numberToWordsService: NumberToWordsService) { }


    ngOnInit() {

    }

    download(deliveryChallan: DeliveryChallanHeader, company: Company) {
        this.doc = new jsPDF('p', 'pt');

        this.deliveryChallan = deliveryChallan;
        this.company = company;

        this.doc.setDrawColor(215, 235, 252);

        //Set Title and copy type
        this.writeTitleAndcopyType(this.copyTypeText, this.titleText);


        //Company And address in the header 
        this.writeCompanyAndAddress();


        //Bill number and date
        this.writeBillNumberAndDate();

        //Customer Details
        //Name
        //Address
        //State
        //GSTN



        this.writeCustomerDetails();


        this.writeEwayAndModeOfDispatch();

        //Customer Delivery address
        //State


        this.writeDeliveryItems();
        this.writeDeliveryFooterItems();

        //Invoice Items

        // if (this.deliveryChallan.igstTaxAmount > 0) {
        // this.writedcItemsIGST();
        // }
        // else {
        //     this.writedcItems();
        // }
        //Summary section with Amount in words
        // this.writeSummary();

        //Terms and conditions, Signature
        this.writeTermsAndconditionsAndSignature();


        //Open pdf in a tab
        this.openInNewTab();

    }
    writeTitleAndcopyType(copyTypeText: string, titleText: string) {

        var rectangleStartX: number = 400;
        var rectangleStartY: number = 10;
        var rectangleWidth: number = 150;
        var rectangleHeight: number = 20;
        this.doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

        this.doc.setFontSize(12);


        var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (this.doc.getStringUnitWidth(copyTypeText) * this.doc.internal.getFontSize() / 2));
        var copyTypeTextOffsetY: number = rectangleStartY + this.doc.internal.getFontSize() / 2; //14;
        //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
        //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
        this.doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);

        this.doc.setFontSize(20);
        this.doc.setTextColor(40);
        this.doc.setFontStyle('normal');

        var xOffset: number = (this.doc.internal.pageSize.width / 2) - (this.doc.getStringUnitWidth(titleText) * this.doc.internal.getFontSize() / 2);

        this.doc.text(titleText, xOffset, rectangleStartY + (copyTypeTextOffsetY / 2) + this.doc.internal.getFontSize() / 2);


    }

    openInNewTab() {

        var string = this.doc.output('datauristring');
        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
        var x = window.open();
        x.document.open();
        x.document.write(iframe);
        x.document.close();
        return this.doc;
    }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(): any[] {

        return [
            {
                "companyName": this.company.name, //"Uthkrushta Technologies",
            }
        ];
    }


    getAddressHeader(): any[] {

        var headerColumns = [

            { title: "Company Address", dataKey: "companyAddress" },
        ];

        return headerColumns;

    }


    getAddress(): any[] {

        return [
            {

                "companyAddress": this.company.address, //"Nagendra Block, BSK 3rd Stage, 29203882038"
            }
        ];
    }

    getGSTHeader(): any[] {
        var headerColumns = [

            { title: "GST", dataKey: "GST" },
        ];

        return headerColumns;
    }

    getGST(): any[] {
        return [
            {

                "GST": this.company.gstNumber, //"XXXX"
            }
        ];
    }


    writeCompanyAndAddress() {

        var startCompanyY: number = 55;
        //var startAddressY: number = 80;

        this.doc.autoTable(this.getCompanyHeader(), this.getCompany(), {

            startY: startCompanyY,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'bold',
                fontSize: 12,
            }

        });

        this.doc.autoTable(this.getAddressHeader(), this.getAddress(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });


        this.doc.autoTable(this.getGSTHeader(), this.getGST(), {

            startY: this.doc.autoTable.previous.finalY - 5,
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'normal',
                fontSize: 10,
            }
        });

    }


    getBillHeader(): any[] {
        var headerColumns = [

            { title: "Bill Number", dataKey: "billNumber" },
            { title: "Date", dataKey: "billDate" },
        ];

        return headerColumns;
    }

    getBill(): any[] {
        return [
            {

                "billNumber": "Bill Number: " + this.deliveryChallan.deliveryChallanNumber,
                "billDate": "Date: " + this.datePipe.transform(this.deliveryChallan.deliveryChallanDate, 'dd-MM-yyyy'),
            }
        ];
    }
    writeBillNumberAndDate() {
        var startY: number = this.doc.autoTable.previous.finalY + 25;

        this.doc.autoTable(this.getBillHeader(), this.getBill(), {

            columnStyles: { billDate: { halign: 'right' } },
            startY: startY,
            margin: { left: this.leftMargin, right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 12,
                fillColor: [215, 235, 252],
            },

        });
    }

    getCustomerAddressHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getCustomerAddress(): any[] {

        return [
            {
                "columnHead": "Bill To",
                "columnContent": this.deliveryChallan.partyName,
            },
            {
                "columnHead": "Address",
                "columnContent": this.deliveryChallan.address,
            },
            {
                "columnHead": "Customer GSTN",
                "columnContent": this.deliveryChallan.gstNumber,
            }

        ];

    }

    getCustomerDeliveryAddressHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getCustomerDeliveryAddress(): any[] {
        return [
            {
                "columnHead": "Delivery Address",
                "columnContent": this.deliveryChallan.deliveryAddress ? this.deliveryChallan.deliveryAddress : this.deliveryChallan.address,
            },


        ];
    }

    writeCustomerDetails() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 5;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getCustomerAddressHeader(), this.getCustomerAddress(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getCustomerDeliveryAddressHeader(), this.getCustomerDeliveryAddress(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin, right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);

    }

    getEwayBillHeader(): any[] {

        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

    }

    getEwayBillDetails(): any[] {

        return [
            {
                "columnHead": "E-way Bill",
                "columnContent": this.deliveryChallan.eWayBill,
            },
            {
                "columnHead": "Mode of dispatch",
                "columnContent": this.deliveryChallan.modeOfDispatch,
            },

        ];

    }

    getVehicleHeader(): any[] {
        return [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];
    }

    getVehicleDetails(): any[] {
        return [
            {
                "columnHead": "Vehicle No",
                "columnContent": this.deliveryChallan.vehicleNumber,

            },

            {
                "columnHead": "No of packages",
                "columnContent": this.deliveryChallan.numberOfPackages,
            },

        ];
    }

    writeEwayAndModeOfDispatch() {
        //console.log('1 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        var startY: number = this.doc.autoTable.previous.finalY + 55;
        var custAddressTableRightMargin: number = 300;



        this.doc.autoTable(this.getEwayBillHeader(), this.getEwayBillDetails(), {

            startY: startY,
            margin: { left: this.leftMargin, right: custAddressTableRightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }

        });

        var firstTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('2 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);

        this.doc.autoTable(this.getVehicleHeader(), this.getVehicleDetails(), {
            startY: startY,
            margin: { left: custAddressTableRightMargin, right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', columnWidth: 100 }
            }
        });

        var secondTableFinalY: number = this.doc.autoTable.previous.finalY;
        //console.log('3 this.doc.autoTable.previous.finalY: ', this.doc.autoTable.previous.finalY);
        //console.log('startY: ', startY);

        var tableHeight: number = (firstTableFinalY > secondTableFinalY) ? (firstTableFinalY - startY) : (secondTableFinalY - startY);

        this.custTableLastY = (firstTableFinalY > secondTableFinalY) ? firstTableFinalY : secondTableFinalY;

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.leftMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);
        this.doc.rect(custAddressTableRightMargin, startY, (this.doc.internal.pageSize.width - custAddressTableRightMargin - this.leftMargin), tableHeight);


    }



    getDcItemColumnHeaders(): any[] {
        return [
            { title: "NO", dataKey: "slNo" },
            { title: "Part Number", dataKey: "partNumber" },
            { title: "Description of Materials", dataKey: "materialName" },
            { title: "HSN/ SAC", dataKey: "hsnCode" },
            { title: "Qty", dataKey: "quantity" },
            { title: "Remarks", dataKey: "remarks" },
        ];
    }

    makeDcItem(dc: DeliveryChallanItem): any {

        return {
            // "slNo": dc.slNo,
            "partNumber": dc.partNumber,
            "materialName": dc.partName,
            "hsnCode": dc.hsnOrSac,
            "quantity": dc.quantity,
            "uom": dc.uom,
            "remarks": dc.remarks
        }
    }

    getDcItemColumns(): any[] {

        var DeliveryChallanItem: any[] = [];
        this.deliveryChallan.deliveryChallanItems.forEach(dc => {
            DeliveryChallanItem.push(this.makeDcItem(dc));
        });


        return DeliveryChallanItem;
    }

    writeDeliveryItems() {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var slNoWidth: number = pageWidth * 0.05; //5%
        var partNumberWidth: number = pageWidth * 0.10; //10%
        var materialNameWidth: number = pageWidth * 0.34; //30% of total width
        var hsnWidth: number = pageWidth * 0.10; //6%
        var qtyWidth: number = pageWidth * 0.05; //6%
        var remarks: number = pageWidth * 0.36;
        var headerLineColor = [215, 235, 252];
        //console.log('page width: ', this.doc.internal.pageSize.width);

        var pageContent = function (data) {

            // FOOTER
            if (this.doc) {
                var str = "Page " + data.pageCount;
                if (typeof this.doc.putTotalPages === 'function') {
                    str = str + " of " + this.totalPagesExp;
                }
                this.doc.setFontSize(10);
                this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

            }
        };


        this.doc.autoTable(this.getDcItemColumnHeaders(), this.getDcItemColumns(), {
            //addPageContent: pageContent,
            startY: this.custTableLastY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            // theme:'grid',
            theme: 'plain',
            tableWidth: 'auto',
            //tableLineColor: [0,0,0],
            //tableLineWidth: 0.5,
            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                // fillColor: [215, 235, 252],
                fillColor: headerLineColor,
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'left',
            },
            columnStyles: {
                slNo: { fillColor: [232, 241, 248], columnWidth: slNoWidth, halign: 'right' },
                partNumber: { fillColor: [232, 241, 248], columnWidth: partNumberWidth },
                materialName: { fillColor: [232, 241, 248], columnWidth: materialNameWidth },
                hsnCode: { fillColor: [232, 241, 248], columnWidth: hsnWidth },
                quantity: { fillColor: [232, 241, 248], columnWidth: qtyWidth, halign: 'right' },
                remarks: { fillColor: [232, 241, 248], columnWidth: remarks }

            }
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(215, 235, 252);
        //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

        // //console.log('this.doc.autoTable.previous ', this.doc.autoTable.previous);
        // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
        // // Total page number plugin only available in jspdf v1.0+
        // if (typeof this.doc.putTotalPages === 'function') {
        //     this.doc.putTotalPages(this.totalPagesExp);
        // }



    }


    getDcItemFooterHeaders(): any[] {
        return [
            { title: "", dataKey: "dummy1" },
            { title: "", dataKey: "dummy2" },
            { title: "", dataKey: "dummy3" },
            { title: "", dataKey: "columnHead" },
            { title: "", dataKey: "columnContent" },
            { title: "", dataKey: "dummy4" },
        ];
    }

    makeDcFooterItem(): any[] {
        return [


            {
                "dummy1": " ",
                "dummy2": " ",
                "dummy3": " ",
                "columnHead": "Total Quantity",
                "columnContent": this.deliveryChallan.totalQuantity,
                "dummy4": " ",
            },

        ];
    }

    writeDeliveryFooterItems() {

        var pageWidth: number = this.doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
        var dummy1: number = pageWidth * 0.05; //5%
        var dummy2: number = pageWidth * 0.10; //10%
        var dummy3: number = pageWidth * 0.34; //30% of total width
        var columnHead: number = pageWidth * 0.10; //6%
        var columnContent: number = pageWidth * 0.05; //6%
        var dummy4: number = pageWidth * 0.36;

        //console.log('page width: ', this.doc.internal.pageSize.width);
        this.doc.autoTable(this.getDcItemFooterHeaders(), this.makeDcFooterItem(), {
            //addPageContent: pageContent,
            startY: this.doc.autoTable.previous.finalY + 5,
            margin: { left: this.leftMargin, right: this.rightMargin },
            //theme:'grid',
            theme: 'plain',
            tableWidth: 'auto',
            showHeader: 'never',

            styles: {
                cellPadding: 0.5,
                fontSize: 8,
                lineColor: [0, 0, 0],
                fillStyle: 'DF',
                //halign: 'right',
                overflow: 'linebreak',
                overflowColumns: false
            },
            headerStyles: {
                fillColor: [215, 235, 252],
                lineColor: [0, 0, 0],
                fontSize: 9,
                textColor: 0,
                fontStyle: 'normal',
                halign: 'center',
            },
            columnStyles: {
                dummy1: { fillColor: [215, 235, 252], columnWidth: dummy1, halign: 'right' },
                dummy2: { fillColor: [215, 235, 252], columnWidth: dummy2 },
                dummy3: { fillColor: [215, 235, 252], columnWidth: dummy3 },
                columnHead: { fillColor: [215, 235, 252], columnWidth: columnHead, fontStyle: 'bold', },
                columnContent: { fillColor: [215, 235, 252], columnWidth: columnContent, halign: 'right' },
                dummy4: { fillColor: [215, 235, 252], columnWidth: dummy4 }

            }
        });

        this.doc.setLineWidth(1);
        this.doc.setDrawColor(215, 235, 252);

    }


    writeTermsAndconditionsAndSignature() {

        var tableHeight = this.doc.internal.pageSize.height - (this.doc.autoTable.previous.finalY + 50);

        this.doc.setDrawColor(215, 235, 252);
        this.doc.rect(this.leftMargin, this.doc.autoTable.previous.finalY + 10, (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin)), tableHeight);

        let receiverSignatureHeader = [
            { title: "", dataKey: "data" },
        ];

        let receiverSignature = [


            {
                "data": " ",
            },
            {
                "data": "",
            },
            {
                "data": "Receiver's Signature"
            }
        ];



        let tableStart: number = this.doc.autoTable.previous.finalY + 10;

        this.doc.autoTable(receiverSignatureHeader, receiverSignature, {
            startY: tableStart,
            margin: { left: this.leftMargin, right: (this.doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + this.summaryTableLeftMargin)) },
            theme: 'plain',
            styles: {
                halign: 'left',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            drawRow: function (row, data) {
                if (row.index === 0) {
                    //this.doc.setFontStyle('bold');
                    //console.log(data);
                    data.doc.setFontStyle('bold');
                    data.settings.styles = { fontStyle: 'bold' };
                }
            }
        });

        let signatureHeader = [
            { title: "key", dataKey: "data" },
        ];

        let signature = [
            {
                "data": "For " + this.company.name,
            },
            {
                "data": "",
            },
            {
                "data": "Authorised Signature"
            }
        ];


        tableStart += 20;

        this.doc.autoTable(signatureHeader, signature, {
            startY: tableStart,
            margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
            theme: 'plain',
            showHeader: 'never',
            styles: {
                halign: 'center',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false
            },
            drawRow: function (row, data) {
                if (row.index === 0) {
                    //this.doc.setFontStyle('bold');
                    //console.log(data);
                    data.doc.setFontStyle('bold');
                    data.settings.styles = { fontStyle: 'bold' };
                }
            }


        });

    }
}