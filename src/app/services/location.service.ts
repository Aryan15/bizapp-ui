import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Country } from '../data-model/country-model';
import { State } from '../data-model/state-model';
import { City } from '../data-model/city-model';
import { Area } from '../data-model/area-model';
import { environment } from '../../environments/environment';
import { TransactionResponse } from '../data-model/transaction-response';
import { Currency } from '../data-model/currency-model';


@Injectable()
export class LocationService {

    countryApiUrl: string;
    countriesApiUrl: string;
    stateApiUrl: string;
    statesApiUrl: string;
    cityApiUrl: string;
    citiesApiUrl: string;
    areaApiUrl: string;
    areasApiUrl: string;
    currencyApiUrl: string;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient) {
        this.countryApiUrl = environment.baseServiceUrl + environment.countryApiUrl;//'http://localhost:8081/api/location';
        this.countriesApiUrl = environment.baseServiceUrl + environment.countriesApiUrl;//'http://localhost:8081/api/locations';
        this.stateApiUrl = environment.baseServiceUrl + environment.stateApiUrl;
        this.statesApiUrl = environment.baseServiceUrl + environment.statesApiUrl;
        this.cityApiUrl = environment.baseServiceUrl + environment.cityApiUrl;
        this.citiesApiUrl = environment.baseServiceUrl + environment.citiesApiUrl;
        this.areaApiUrl = environment.baseServiceUrl + environment.areaApiUrl;
        this.areasApiUrl = environment.baseServiceUrl + environment.areasApiUrl;
        this.currencyApiUrl = environment.baseServiceUrl + environment.currencyApiUrl;
    }

    getCountries(): Observable<Country[]> {
        ////console.log('In getCountries');
        return this.http
            .get<Country[]>(this.countryApiUrl)
            // .map(response => <Country[]>response.json());
    }

    getStatesForCountry(countryId: number): Observable<State[]> {

        return this.http
            .get<State[]>(this.stateApiUrl + "/" + countryId)
            // .map(response => <State[]>response.json());
    }

    getCitiesForState(stateId: number): Observable<City[]> {

        return this.http
            .get<City[]>(this.cityApiUrl + "/" + stateId)
            // .map(response => <City[]>response.json());
    }

    getAreasForCities(cityId: number): Observable<Area[]> {
        
        return this.http
            .get<Area[]>(this.areaApiUrl + "/" + cityId)
            // .map(response => <Area[]>response.json());
    }

    createCountries(countries: Country[]): Observable<Country[]> {

        return this.http
            .post<Country[]>(this.countriesApiUrl,countries)
            // .map(response => <Country[]>response.json());

    }

    deleteCountry(ids: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.countryApiUrl+"/"+ids);
    }

    deleteState(idList: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.stateApiUrl+"/"+idList);
    }

    deleteCity(idList: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.cityApiUrl+"/"+idList);
    }

    deleteArea(idList: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.areaApiUrl+"/"+idList);
    }

    getStates(): Observable<State[]> {
        ////console.log('In getCountries');
        return this.http
            .get<State[]>(this.stateApiUrl)
            // .map(response => <State[]>response.json());
    }


    createStates(states: State[]): Observable<State[]> {
        //console.log("incoming states", states)
        //console.log("url: ", this.statesApiUrl)
        return this.http
            .post<State[]>(this.statesApiUrl,states)
            // .map(response => <State[]>response.json());

    }

    getCities(): Observable<City[]> {
        ////console.log('In getCountries');
        return this.http
            .get<City[]>(this.cityApiUrl)
            // .map(response => <City[]>response.json());
    }


    createCities(cities: City[]): Observable<City[]> {

        return this.http
            .post<City[]>(this.citiesApiUrl,cities )
            // .map(response => <City[]>response.json());

    }

    getAreas(): Observable<Area[]> {
        ////console.log('In getCountries');
        return this.http
            .get<Area[]>(this.areaApiUrl)
            // .map(response => <Area[]>response.json());
    }


    createAreas(areas: Area[]): Observable<Area[]> {

        return this.http
            .post<Area[]>(this.areasApiUrl, areas) // .map(response => <Area[]>response.json());

    }

    getCurrencys(): Observable<Currency[]> {
        ////console.log('In getCountries');
        return this.http
            .get<Currency[]>(this.currencyApiUrl)
            // .map(response => <City[]>response.json());
    }

}