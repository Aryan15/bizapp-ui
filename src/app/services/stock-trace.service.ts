
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { StockTraceModel, StockTraceRequest, StockTraceReport} from '../data-model/stock-trace-model';
import { TokenService } from './token.service';
import {PagedData} from "../reports/model/paged-data";
import {Page} from "../reports/model/page";

@Injectable()
export class StockTraceService {

    stockTraceApiUrl : string;
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: HttpClient, private tokenService : TokenService){
        this.stockTraceApiUrl = environment.baseServiceUrl + environment.stockTraceApiUrl;
    }

    getStockTrace(stockTraceRequest : StockTraceRequest) : Observable<StockTraceModel[]>{
        return this.http.post<StockTraceModel[]>(this.stockTraceApiUrl,stockTraceRequest )
                // .map(response => <StockTraceModel[]>response.json());
    }


    getStockTraceReport(stockTraceRequest : StockTraceRequest): Observable<StockTraceModel[]>{
        //console.log('before post: ', stockTraceRequest);
        let ret: Observable<StockTraceModel[]> = this.http.post<StockTraceModel[]>(this.stockTraceApiUrl,stockTraceRequest );
        //console.log('returning: ')
        ret.subscribe(response => {
            //console.log('response: ', response);
        })
        return ret;
    }

    getStockTraceReports(invoiceTypeId: number, stockTraceRequest : StockTraceRequest, page: Page): Observable<PagedData<StockTraceModel>>{
     
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        // let pageSortColumn = page ? page.sortColumn : null;

        return this.http.post<StockTraceReport>(this.stockTraceApiUrl+ "/" 
        // + pageNumber + "/" 
        // + pageSize + "/" 
        // + pageSortDirection
        //  + "/"  + pageSortColumn
         ,stockTraceRequest ).pipe(
            map(stockItemData => this.getPagedData(page, stockItemData)));
    }


    private getPagedData(page: Page, stockItemData: StockTraceReport): PagedData<StockTraceModel> {

        //console.log("stockItemData: ..................>>>>>>>",stockItemData);
        let pagedData = new PagedData<StockTraceModel>();

        // if(page){
        //     page.totalElements = stockItemData.totalCount ? stockItemData.totalCount : 0 ;
        //     page.totalPages = page.totalElements / page.size;
        /// }
        pagedData.page = page;
        // pagedData.data = stockItemData.stockReports;
        //console.log('stockItemData.stockReports: ', stockItemData.stockReports);
        //console.log("page ", page);
        return pagedData;
    }

}