import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { AppSettings } from '../app.settings';
import { Material, Process } from '../data-model/material-model';
import { GlobalSetting } from '../data-model/settings-wrapper';
import { Tax } from '../data-model/tax-model';
import { TransactionItem } from '../data-model/transaction-item';
import { Uom } from '../data-model/uom-model';
import { Party } from '../data-model/party-model';
import { Company } from '../data-model/company-model';
import { State } from '../data-model/state-model';
import { LocationService } from './location.service';


@Injectable()
export class TransactionItemService {

    public materials: Material[] = [];
    states: State;

    constructor(private fb: FormBuilder) {

    }

    private mapAbstractItem(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = {
            id: null,
            headerId: null,
            slNo: null,
            materialId: item.materialId,
            quantity: item.quantity,
            // balanceQuantity: item.balanceQuantity,
            price: item.price,
            amount: item.amount,
            discountPercentage: item.discountPercentage,
            discountAmount: item.discountAmount,
            amountAfterDiscount: item.amountAfterDiscount,
            transportationAmount: item.transportationAmount,
            cessPercentage: item.cessPercentage,
            cessAmount: item.cessAmount,
            igstTaxPercentage: item.igstTaxPercentage,
            igstTaxAmount: item.igstTaxAmount,
            sgstTaxPercentage: item.sgstTaxPercentage,
            sgstTaxAmount: item.sgstTaxAmount,
            cgstTaxPercentage: item.cgstTaxPercentage,
            cgstTaxAmount: item.cgstTaxAmount,
            amountAfterTax: item.amountAfterTax,
            remarks: item.remarks,
            quotationHeaderId: null,
            quotationItemId: null,
            purchaseOrderHeaderId: null,
            purchaseOrderItemId: isOutMaterial ? item.purchaseOrderItemId:null,
            deliveryChallanHeaderId: null,
            deliveryChallanItemId: null,
            grnHeaderId: null,
            grnItemId: null,
            proformaInvoiceHeaderId: null,
            proformaInvoiceItemId: null,
            sourceInvoiceHeaderId: null,
            sourceInvoiceItemId: null,            
            unitOfMeasurementId: null,
            unitOfMeasurementName: null,
            taxId: item.taxId,
            partNumber: isOutMaterial && item.outPartNumber ? item.outPartNumber : item.partNumber,
            hsnOrSac: item.hsnOrSac,
            // partName: item.partName,
            partName: isOutMaterial && item.outName ? item.outName : item.partName, // + (item.processName ? "("+item.processName+")" : ''),
            specification : item.specification,
            buyingPrice: item.buyingPrice,
            uom: item.uom,
            inclusiveTax: item.inclusiveTax,
            invoiceBalanceQuantity: null,
            dcBalanceQuantity: null,
            grnBalanceQuantity: null,
            sourceDeliveryChallanHeaderId: item.sourceDeliveryChallanHeaderId,
            sourceDeliveryChallanItemId: item.sourceDeliveryChallanItemId,
            processId: item.processId,
            processName: item.processName,
            incomingQuantity: item.incomingQuantity,
            rejectedQuantity: item.incomingQuantity - item.quantity,
            isContainer: item.isContainer,
            outName: item.outName,
            outPartNumber: item.outPartNumber,
            originalDCBalanceQuantity: item.dcBalanceQuantity,
            sourceDeliveryChallanDate: item.sourceDeliveryChallanDate,
            sourceDeliveryChallanNumber: item.sourceDeliveryChallanNumber
        };

        return tItem;
    }


    private mapRecentPOItem(item: any): TransactionItem {

        let tItem: TransactionItem = this.mapAbstractItem(item);

        tItem.id = item.id;
        tItem.headerId = item.headerId;
        tItem.slNo = item.slNo;
        tItem.quotationItemId = item.quotationItemId;
        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;

        ////console.log('in mapRecentPOItem returning: ', tItem);
        return tItem;
    }

    // private mapPOItem(item: any): TransactionItem {
    //     let tItem: TransactionItem = this.mapAbstractItem(item);

    //     tItem.purchaseOrderHeaderId = item.headerId;
    //     tItem.purchaseOrderItemId = item.id;
    //     tItem.unitOfMeasurementId = item.unitOfMeasurementId;
    //     tItem.unitOfMeasurementName = item.unitOfMeasurementName;
    //     tItem.quantity = item.balanceQuantity;
    //     return tItem;
    // }

    // private mapDCItem(item: any): TransactionItem {
    ////     /////console.log('in mapDCItem');
    //     let tItem: TransactionItem = this.mapAbstractItem(item);


    //     tItem.deliveryChallanHeaderId = item.headerId;
    //     tItem.deliveryChallanItemId = item.id;
    //     tItem.unitOfMeasurementId = item.unitOfMeasurementId;
    //     tItem.unitOfMeasurementName = item.unitOfMeasurementName;
    //     tItem.quantity = item.quantity - item.balanceQuantity;
    ////     ////console.log('mapped item: ', tItem);
    //     return tItem;
    // }


    private mapNewInvoiceItem(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        return tItem;
    }

    private mapRecentQuotationItemForPO(item: any): TransactionItem {
        //////console.log('in mapRecentQuotationItemForPO');

        let tItem: TransactionItem = this.mapAbstractItem(item);

        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.quotationItemId = item.id;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;

        //////console.log('mapped item ',tItem);
        return tItem;
    }

    private mapRecentQuotationItem(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);

        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.quotationItemId = item.id;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;

        return tItem;
    }

    //////////////////// comment from above ////////////////////////
    private mapItemFromQuotationInPO(item: any): TransactionItem {

        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.slNo = item.slNo;
        tItem.quotationItemId = item.id;
        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;

        return tItem;
    }

    private mapItemFromPOInDC(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.slNo = item.slNo;
        tItem.purchaseOrderItemId = item.id;
        tItem.purchaseOrderHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        //tItem.dcBalanceQuantity = item.dcBalanceQuantity;
        tItem.quantity = item.dcBalanceQuantity;
        ////console.log("tItem.dcBalanceQuantity", tItem.dcBalanceQuantity)
        return tItem;
    }

    private mapItemFromPOInJWDC(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.slNo = item.slNo;
        tItem.purchaseOrderItemId = item.id;
        tItem.purchaseOrderHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        //tItem.dcBalanceQuantity = item.dcBalanceQuantity;
        tItem.quantity = item.dcBalanceQuantity;
        tItem.incomingQuantity = item.dcBalanceQuantity;
        tItem.rejectedQuantity = 0;
        console.log("tItem", tItem)
        return tItem;
    }

    private mapItemFromDCInIncomingJWOUTDC(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.sourceDeliveryChallanItemId = item.id;
        tItem.sourceDeliveryChallanHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        //tItem.dcBalanceQuantity = item.dcBalanceQuantity;
        tItem.quantity = item.dcBalanceQuantity;
        tItem.incomingQuantity = item.incomingQuantity;
        tItem.dcBalanceQuantity = 0; //item.dcBalanceQuantity;
        tItem.slNo = item.slNo;
        tItem.purchaseOrderItemId = isOutMaterial ? item.purchaseOrderItemId:null;
        //////console.log("tItem", tItem)
        return tItem;
    }

    private mapItemFromDCInDC(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.slNo = item.slNo;
        tItem.sourceDeliveryChallanItemId = item.id;
        tItem.sourceDeliveryChallanHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        //tItem.dcBalanceQuantity = item.dcBalanceQuantity;
        tItem.quantity = item.dcBalanceQuantity;
        ////console.log("tItem.dcBalanceQuantity", tItem.dcBalanceQuantity)
        return tItem;
    }

    private mapItemFromPOInInvoice(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.slNo = item.slNo;
        tItem.purchaseOrderItemId = item.id;
        tItem.purchaseOrderHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.quantity = item.invoiceBalanceQuantity;


        return tItem;
    }

    private  mapItemFromPIInInvoice(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.slNo = item.slNo;
        tItem.proformaInvoiceItemId = item.id;
        tItem.proformaInvoiceHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        if (item.purchaseOrderHeaderId) {
            tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId;
            tItem.purchaseOrderItemId = item.purchaseOrderItemId;
        }
        if (item.deliveryChallanHeaderId) {
            tItem.deliveryChallanHeaderId = item.deliveryChallanHeaderId;
            tItem.deliveryChallanItemId= item.deliveryChallanItemId;
        }
        // tItem.quantity = item.invoiceBalanceQuantity;


        return tItem;
    }

    private  mapItemFromSourceInInvoice(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.slNo = item.slNo;
        tItem.sourceInvoiceItemId = item.id;
        tItem.sourceInvoiceHeaderId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        if (item.purchaseOrderHeaderId) {
            tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId;
            tItem.purchaseOrderItemId = item.purchaseOrderItemId;
        }
        if (item.deliveryChallanHeaderId) {
            tItem.deliveryChallanHeaderId = item.deliveryChallanHeaderId;
            tItem.deliveryChallanItemId= item.deliveryChallanItemId;
        }
        // tItem.quantity = item.invoiceBalanceQuantity;
        tItem.quantity = item.noteBalanceQuantity;


        return tItem;
    }

    private mapItemFromDCInInvoice(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.slNo = item.slNo;
        tItem.deliveryChallanItemId = item.id;
        tItem.deliveryChallanHeaderId = item.headerId;
        if (item.purchaseOrderHeaderId) {
            tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId
            tItem.purchaseOrderItemId = item.purchaseOrderItemId
        }
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.quantity = item.invoiceBalanceQuantity;
        //   tItem.quantity = item.balanceQuantity;

        return tItem;
    }
    private mapItemFromGRNInInvoice(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.slNo = item.slNo;
        tItem.grnHeaderId = item.grnHeaderId;
        tItem.grnItemId = item.id;
        if (item.deliveryChallanHeaderId) {
            tItem.deliveryChallanHeaderId = item.deliveryChallanHeaderId;
           // tItem.deliveryChallanItemId = item.deliveryChallanItemId;
        }
        if(item.purchaseOrderHeaderId){
            tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId;
           // tItem.purchaseOrderItemId = item.purchaseOrderItemId;
        }
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
     //   tItem.quantity = item.balanceQuantity;
        tItem.quantity = item.invoiceBalanceQuantity;
        tItem.price =item.unitPrice;
        tItem.partName =item.materialName;
        tItem.specification = item.materialSpecification

        return tItem;
    }

    private mapItemFromRecentQuotation(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.id = item.id;
        tItem.slNo = item.slNo;
        tItem.headerId = item.headerId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.quantity = item.quantity;


        return tItem;
    }

    private mapItemFromRecentPO(item: any): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item);
        tItem.id = item.id;
        tItem.slNo = item.slNo;
        tItem.headerId = item.headerId;
        tItem.quotationItemId = item.quotationItemId;
        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.quantity = item.quantity;

        ////console.log('in mapItemFromRecentPO returning: ', tItem);
        return tItem;
    }

    private mapItemFromRecentDC(item: any, isOutMaterial: boolean = false): TransactionItem {
        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);
        tItem.id = item.id;
        tItem.slNo = item.slNo;
        tItem.headerId = item.headerId;
        tItem.purchaseOrderItemId = item.purchaseOrderItemId;
        tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.quantity = item.quantity;
        tItem.dcBalanceQuantity = item.dcBalanceQuantity;
        //////console.log("tItem: ",tItem);
        //////console.log("tItem: "+tItem.slNo);
        return tItem;
    }

    private mapItemFromRecentInvoice(item: any, isOutMaterial: boolean = false): TransactionItem {

        let tItem: TransactionItem = this.mapAbstractItem(item, isOutMaterial);

        tItem.id = item.id;
        tItem.headerId = item.headerId;
        tItem.slNo = item.slNo;
        tItem.quotationHeaderId = item.quotationHeaderId;
        tItem.purchaseOrderHeaderId = item.purchaseOrderHeaderId;
        tItem.purchaseOrderItemId = item.purchaseOrderItemId;
        tItem.deliveryChallanHeaderId = item.deliveryChallanHeaderId;
        tItem.deliveryChallanItemId = item.deliveryChallanItemId;
        tItem.grnHeaderId = item.grnHeaderId;
        tItem.grnItemId = item.grnItemId;
        tItem.unitOfMeasurementId = item.unitOfMeasurementId;
        tItem.unitOfMeasurementName = item.unitOfMeasurementName;
        tItem.proformaInvoiceHeaderId = item.proformaInvoiceHeaderId;
        tItem.proformaInvoiceItemId = item.proformaInvoiceItemId;
        tItem.sourceInvoiceItemId = item.sourceInvoiceItemId;

        ////console.log('in mapItemFromRecentInvoice returning: ', tItem);
        return tItem;
    }

    mapItem(item: any, formSource: string, isOutMaterial: boolean = false): TransactionItem {
        // console.log("formSource: "+formSource);
        // if (formSource === AppSettings.TXN_ITEM_OF_RECENT_INVOICE){
        //     return this.mapItemFromRecentInvoice(item);
        // }else if(formSource === this.itemOfPO){
        //     return this.mapPOItem(item);
        // }else if(formSource === this.itemOfDC) {
        //     return this.mapDCItem(item);
        // }else if(formSource === this.itemOfNewInvoice){
        //     return this.mapNewInvoiceItem(item);
        // }else if(formSource === this.itemOfQuotationInPO){
        //     return this.mapRecentQuotationItemForPO(item);
        // }else if(formSource === this.itemOfRecentQuotation){
        //     return this.mapRecentQuotationItem(item);
        // }else if(formSource === this.itemOfRecentPO){
        //     return this.mapRecentPOItem(item);
        // }
        // else {
        //     let tItem : TransactionItem = this.mapAbstractItem(item);
        //     return tItem;
        // }
        if (formSource === AppSettings.TXN_ITEM_OF_QUOTATION_IN_PO) {
            return this.mapItemFromQuotationInPO(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_PO_IN_DC) {
            return this.mapItemFromPOInDC(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_PO_IN_INVOICE) {
            return this.mapItemFromPOInInvoice(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_DC_IN_INVOICE) {
            return this.mapItemFromDCInInvoice(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_GRN_IN_INVOICE) {
            return this.mapItemFromGRNInInvoice(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_RECENT_QUOTATION) {
            return this.mapItemFromRecentQuotation(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_RECENT_PO) {
            return this.mapItemFromRecentPO(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_RECENT_DC) {
            return this.mapItemFromRecentDC(item, isOutMaterial);
        }else if (formSource === AppSettings.TXN_ITEM_OF_PI_IN_INVOICE) {
            return this.mapItemFromPIInInvoice(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_SOURCE_INVOICE) {
            return this.mapItemFromSourceInInvoice(item);
        } else if (formSource === AppSettings.TXN_ITEM_OF_RECENT_INVOICE) {
            return this.mapItemFromRecentInvoice(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_DC_IN_DC){
            return this.mapItemFromDCInDC(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_PO_IN_JW_DC){
            return this.mapItemFromPOInJWDC(item, isOutMaterial);
        } else if (formSource === AppSettings.TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC){
            return this.mapItemFromDCInIncomingJWOUTDC(item, isOutMaterial);
        }

        else {
            let tItem: TransactionItem = this.mapAbstractItem(item);
            return tItem;
        }


    }
    makeItem(transactionItem: TransactionItem): FormGroup {

        // console.log(''+transactionItem.id); 
        // console.log(''+transactionItem.incomingQuantity);
        // console.log(''+transactionItem.originalDCBalanceQuantity);
        // console.log(''+transactionItem.quantity) 
        // console.log(''+transactionItem.incomingQuantity)
        // console.log(''+transactionItem.originalDCBalanceQuantity)
        let val = transactionItem.id ? (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity - transactionItem.quantity) : (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity);
        // console.log(''+val);

        return this.fb.group({
            id: transactionItem.id,
            headerId: transactionItem.headerId,
            slNo: [transactionItem.slNo],
            materialId: [transactionItem.materialId, [Validators.required]],
            quantity: [transactionItem.quantity,Validators.compose([Validators.required,Validators.min(0.01)])],
            //   balanceQuantity: [transactionItem.balanceQuantity],
            price: [transactionItem.price, Validators.compose([Validators.required, transactionItem.isContainer ? Validators.max(0) : Validators.min(0.01)])], 
         //   price: transactionItem.price ? materialIn.discountPercentage : 0, 
            amount: [transactionItem.amount, [Validators.required]],
            discountPercentage: [transactionItem.discountPercentage],
           
            discountAmount: [transactionItem.discountAmount],
            amountAfterDiscount: [transactionItem.amountAfterDiscount],
            specification:[transactionItem.specification],
            transportationAmount: null,
            cessPercentage: null,
            cessAmount: null,
            igstTaxPercentage: [transactionItem.igstTaxPercentage],
            igstTaxAmount: [transactionItem.igstTaxAmount],
            sgstTaxPercentage: [transactionItem.sgstTaxPercentage],
            sgstTaxAmount: [transactionItem.sgstTaxAmount],
            cgstTaxPercentage: [transactionItem.cgstTaxPercentage],
            cgstTaxAmount: [transactionItem.cgstTaxAmount],
            amountAfterTax: [transactionItem.amountAfterTax],
            remarks: [transactionItem.remarks],
            quotationHeaderId: [transactionItem.quotationHeaderId],
            quotationItemId: [transactionItem.quotationItemId],
            purchaseOrderHeaderId: [transactionItem.purchaseOrderHeaderId],
            purchaseOrderItemId: [transactionItem.purchaseOrderItemId],
            deliveryChallanHeaderId: [transactionItem.deliveryChallanHeaderId],
            deliveryChallanItemId: [transactionItem.deliveryChallanItemId],
            proformaInvoiceHeaderId: [transactionItem.proformaInvoiceHeaderId],
            proformaInvoiceItemId: [transactionItem.proformaInvoiceItemId],
            sourceInvoiceHeaderId: [transactionItem.sourceInvoiceHeaderId],
            sourceInvoiceItemId: [transactionItem.sourceInvoiceItemId],
            unitOfMeasurementId: [transactionItem.unitOfMeasurementId],
            taxId: [transactionItem.taxId], //[Validators.required] removed for header level tax material with no tax
            partNumber: [transactionItem.partNumber],
            hsnOrSac: [transactionItem.hsnOrSac],
            partName: [transactionItem.partName],
            buyingPrice: [transactionItem.buyingPrice],
            uom: [transactionItem.uom],
            inclusiveTax: [transactionItem.inclusiveTax],
            invoiceBalanceQuantity: [transactionItem.invoiceBalanceQuantity],
            dcBalanceQuantity: [transactionItem.dcBalanceQuantity],
            grnBalanceQuantity: [transactionItem.grnBalanceQuantity],
            sourceDeliveryChallanHeaderId: [transactionItem.sourceDeliveryChallanHeaderId],
            sourceDeliveryChallanItemId: [transactionItem.sourceDeliveryChallanItemId],
            processId: [transactionItem.processId],
            processName: [transactionItem.processName],
            incomingQuantity: [transactionItem.incomingQuantity],
            rejectedQuantity: [transactionItem.rejectedQuantity],
            grnHeaderId: [transactionItem.grnHeaderId],
            grnItemId: [transactionItem.grnItemId],
            deliveredQuantity: [transactionItem.id ? (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity - transactionItem.quantity) : (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity)],
            isContainer: [transactionItem.isContainer],
            originalDCBalanceQuantity: [transactionItem.dcBalanceQuantity],
            sourceDeliveryChallanNumber: [transactionItem.sourceDeliveryChallanNumber],
            sourceDeliveryChallanDate: [transactionItem.sourceDeliveryChallanDate],
        });
    }

    initTransactionItem(slNoIn: number
        , materialIn: Material
        , uomIn: Uom
        , quantityIn: number
        , taxIn: Tax
        , isIgst: boolean
        , buyOrSell: string
        , globalSetting: GlobalSetting
        , isInclusiveTax: boolean
        , process?: Process[]
        , isOutMaterial: boolean = false): FormGroup {

        let price: number = 0;
        if (buyOrSell === AppSettings.TXN_PURCHASE) {

            if (globalSetting.purchasePrice === AppSettings.MATERIAL_PRICE_TYPE.buyingPrice) {
                price = materialIn.buyingPrice
            } else if (globalSetting.purchasePrice === AppSettings.MATERIAL_PRICE_TYPE.MRP) {
                price = materialIn.mrp;
            }

        } else if (buyOrSell === AppSettings.TXN_SELL) {
            if (globalSetting.salePrice === AppSettings.MATERIAL_PRICE_TYPE.sellingPrice) {
                price = materialIn.price;
            } else if (globalSetting.salePrice == AppSettings.MATERIAL_PRICE_TYPE.MRP) {
                price = materialIn.mrp;
            }
        }

        ////console.log('materialIn.discountPercentage ', materialIn);
        ////console.log('materialIn.quantityIn ', quantityIn);

        let transactionItem: TransactionItem = {
            id: null,
            headerId: null,
            slNo: slNoIn,
            materialId: materialIn.id,
            quantity: quantityIn,
            // balanceQuantity: null,
            price: price,
            amount: 0,
            discountPercentage: materialIn.discountPercentage ? materialIn.discountPercentage : 0,
            discountAmount: 0,
            amountAfterDiscount: 0,
            transportationAmount: 0,
            cessPercentage: 0,
            cessAmount: 0,
           //igstTaxPercentage: isIgst ? taxIn.rate : 0, 
        //    taxIn:(taxIn.rate) ?taxIn.rate:0,
             igstTaxPercentage : taxIn ? ((isIgst ? taxIn.rate : 0 )) : 0,

            igstTaxAmount: 0,
            sgstTaxPercentage: isIgst ? 0 : (taxIn ? taxIn.rate / 2 : 0),
            sgstTaxAmount: 0,
            cgstTaxPercentage: isIgst ? 0 : (taxIn ? taxIn.rate / 2 : 0),
            cgstTaxAmount: 0,
            amountAfterTax: 0,
            remarks: '',
            quotationHeaderId: null,
            quotationItemId: null,
            purchaseOrderHeaderId: null,
            purchaseOrderItemId: null,
            deliveryChallanHeaderId: null,
            deliveryChallanItemId: null,
            proformaInvoiceHeaderId: null,
            proformaInvoiceItemId: null,
            grnHeaderId: null,
            grnItemId: null,
            sourceInvoiceHeaderId: null,
            sourceInvoiceItemId: null,           
            unitOfMeasurementId: uomIn ? uomIn.id: null,
            unitOfMeasurementName: uomIn ? uomIn.name : null,
            taxId: taxIn ? taxIn.id : null,
            partNumber: isOutMaterial ? materialIn.outPartNumber : materialIn.partNumber,
            specification: materialIn.specification,
            hsnOrSac: materialIn.hsnCode,
            partName: isOutMaterial ? materialIn.outName : materialIn.name,
            buyingPrice: materialIn.buyingPrice,
            uom: uomIn ? uomIn.name : null,
            inclusiveTax: isInclusiveTax ? 1 : 0,
            invoiceBalanceQuantity: null,
            dcBalanceQuantity: null,
            grnBalanceQuantity: null,
            sourceDeliveryChallanHeaderId: null,
            sourceDeliveryChallanItemId: null,
            processId: process ? process.map(p => p.id).toString() : null,
            processName: process ? process.map(p => p.name).toString() : null,
            incomingQuantity: quantityIn,
            rejectedQuantity: null,
            isContainer: materialIn.isContainer,
            outName: materialIn.outName,
            outPartNumber: materialIn.outPartNumber,
            originalDCBalanceQuantity: null,
            sourceDeliveryChallanDate: null,
            sourceDeliveryChallanNumber: null
        }

        ////console.log(transactionItem.invoiceBalanceQuantity);
        return this.makeItem(transactionItem);


    }

    patchValueFromCompanyAndParty(model, party :Party, company : Company, isParty: boolean = true) {
  
        if(isParty){

            //console.log("enterd the loop") 
            model = this.patchValueFromParty(model, party);
        }
        model.companyName = company.name;
        model.companyGstRegistrationTypeId = company.gstRegistrationTypeId;
        model.companyPinCode = company.pinCode;
        model.companyStateId = company.stateId;
        model.companyStateName = company.stateName;
        model.companyCountryId = company.countryId;
        model.companyPrimaryMobile = company.primaryMobile;
        model.companySecondaryMobile = company.secondaryMobile;
        model.companyContactPersonNumber = company.contactPersonNumber;
        model.companyContactPersonName = company.contactPersonName;
        model.companyPrimaryTelephone = company.primaryTelephone;
        model.companySecondaryTelephone = company.secondaryTelephone;
        model.companyWebsite = company.website;
        model.companyEmail = company.email;
        model.companyFaxNumber = company.faxNumber;
        model.companyAddress = company.address;
        model.companyTagLine = company.tagLine;
        model.companyGstNumber = company.gstNumber;
        model.companyPanNumber = company.panNumber;
        model.companyPanDate = company.panDate;
        model.companyCeritificateImagePath = company.ceritificateImagePath;
        model.companyLogoPath = company.companyLogoPath;

        return model;

    }

    patchValueFromParty(model, party :Party ) { 
        
        model.address = party.address;
        model.partyBillToAddress = party.address;
        model.shipToAddress = party.billAddress;
        model.gstNumber = party.gstNumber;
        //console.log(model.gstNumber+"model gstNumber");
        model.partyName = party.name;
        model.partyContactPersonNumber = party.contactPersonNumber;
        model.partyPinCode = party.pinCode;
        model.partyAreaId = party.areaId;
        model.partyCityId = party.cityId;
        model.partyStateId = party.stateId;
        model.partyCountryId = party.countryId;
        model.partyPrimaryTelephone = party.primaryTelephone;
        model.partySecondaryTelephone = party.secondaryTelephone;
        model.partyPrimaryMobile = party.primaryMobile;
        model.partySecondaryMobile = party.secondaryMobile;
        model.partyEmail = party.email;
        model.partyWebsite = party.webSite;
        model.partyContactPersonName = party.contactPersonName;
        model.partyDueDaysLimit = party.dueDaysLimit;
        model.partyGstRegistrationTypeId = party.gstRegistrationTypeId;
        model.partyPanNumber = party.panNumber;
      
        // model.isIgst = party.isIgst;
        model.isIgst = model.isIgst;

        return model;

    }



    // private mapItemForPriceCheck(item : any) : TransactionItem {
    ////     //console.log("in making items")
    //             let tItem : TransactionItem = this.mapAbstractItem(item);

    //             tItem.id = item.id;
    //             tItem.headerId = item.headerId;
    //             tItem.slNo = item.slNo;
    //             tItem.buyingPrice = item.buyingPrice;

    ////             //console.log("tiitemmm",tItem)
    //             return tItem;

    //         }

    // priceCheck(item: InvoiceHeader): string {

    //             let textToBeAppend: string = "";
    //             let returnResult: boolean = false;

    //             item.invoiceItems.forEach(item => {

    ////                 //console.log(item.materialId);
    ////                 //console.log(item.price);

    //                 let material: Material = this.materials.find(materialIn => materialIn.id === item.materialId);

    ////                 //console.log('material got: ', material);
    ////                 //console.log('item.price: ', item.price);
    ////                 //console.log('material.buyingPrice: ', material.buyingPrice);
    //                 if (item.price < material.buyingPrice) {
    //                     textToBeAppend += material.name;
    ////                     //console.log("strCount value " + textToBeAppend)
    //                 }

    //             });
    //             return textToBeAppend;
    //         }
}