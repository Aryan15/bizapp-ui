import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Currency } from '../data-model/currency-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';

@Injectable()
export class CurrencyService{


    currencyApiUrl : string;
    private headers = new Headers({'Content-Type': 'application/json'});


    constructor(private http: HttpClient, private tokenService : TokenService){
        this.currencyApiUrl = environment.baseServiceUrl + environment.currencyApiUrl
    }

    getAllCurrency() : Observable<Currency[]>{
                return this.http
                .get<Currency[]>(this.currencyApiUrl)
                // .map(response => <Uom[]>response.json());


    }

   // delete(id: number) : Observable<TransactionResponse>{
    //    return this.http        
    //            .delete<TransactionResponse>(this.currencyApiUrl+"/"+id)
     //           // .map(response => <TransactionResponse>response.json());

   // }
}