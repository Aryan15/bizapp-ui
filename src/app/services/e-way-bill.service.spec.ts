import { TestBed } from '@angular/core/testing';

import { EWayBillService } from './e-way-bill.service';

describe('EWayBillService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EWayBillService = TestBed.get(EWayBillService);
    expect(service).toBeTruthy();
  });
});
