
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Party, PartyWithPriceList, PartyType, MaterialPriceList, PartyReport, RecentParty,PartyPriceListReport, PartyMaterialPriceListDTO } from '../data-model/party-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { Page } from "../reports/model/page";
import { PagedData } from "../reports/model/paged-data";
import { shareAndCache } from 'http-operators';
import { AppSettings } from '../app.settings';
import { CompanyService } from './company.service';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { PayableReceivableReportRequest } from '../data-model/pr-model';
import { Currency } from '../data-model/currency-model';

@Injectable()
export class PartyService {

    customerApiUrl: string;
    
    currencyApiUrl:string;
    customersApiUrl: string;
    partiesByNameApiUrl: string;
    suppliersApiUrl: string;
    supplierApiUrl: string;
    partyWithPriceListApiUrl: string;
    partyTypeApiUrl: string;
    partyWithPriceListUrl: string;
    materialPriceListForPartyApiUrl: string;
    apiDeleteUrl: string;
    partyReportApiUrl: string;
    recentPartiesUrl: string;
    partyStatusApiUrl: string;
    partyCountApiUrl:string;
    partyNumberCheckApiUrl:string;
    partyPriceListApiUrl:string;
    getCurrencyApiUrl:string;
    // tallyPartyApiUrl: string;
    // tallyUomApiUrl: string;
    // tallyGstApiUrl: string;
    // tallyStockApiUrl: string;
    // tallyBankApiUrl: string;

    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient, private companyService: CompanyService) {
        this.customerApiUrl = environment.baseServiceUrl + environment.customerApiUrl;
        this.getCurrencyApiUrl=environment.baseServiceUrl + environment.getCurrencyApiUrl;
        this.customersApiUrl = environment.baseServiceUrl + environment.customersApiUrl;
        this.partiesByNameApiUrl = environment.baseServiceUrl + environment.partiesByNameApiUrl;
        this.suppliersApiUrl = environment.baseServiceUrl + environment.suppliersApiUrl;
        this.supplierApiUrl = environment.baseServiceUrl + environment.supplierApiUrl;
        this.partyWithPriceListApiUrl = environment.baseServiceUrl + environment.partyWithPriceListApiUrl;
        this.partyTypeApiUrl = environment.baseServiceUrl + environment.partyTypeApiUrl;
        this.partyWithPriceListUrl = environment.baseServiceUrl + environment.partyWithPriceListUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.partyWithPriceListApiUrl;
        this.materialPriceListForPartyApiUrl = environment.baseServiceUrl + environment.materialPriceListForPartyApiUrl;
        this.partyReportApiUrl = environment.baseServiceUrl + environment.partyReportApiUrl;
        this.recentPartiesUrl = environment.baseServiceUrl + environment.recentPartiesUrl;
        this.partyStatusApiUrl = environment.baseServiceUrl + environment.partyStatusApiUrl;
        this.partyCountApiUrl = environment.baseServiceUrl + environment.partyCountApiUrl;
        this.partyNumberCheckApiUrl = environment.baseServiceUrl + environment.partyNumberApiUrl;
        this.partyPriceListApiUrl = environment.baseServiceUrl + environment.partyPriceListApiUrl;
        // this.tallyPartyApiUrl = environment.baseServiceUrl + environment.tallyPartyApiUrl
        // this.tallyUomApiUrl = environment.baseServiceUrl + environment.tallyUomApiUrl
        // this.tallyGstApiUrl = environment.baseServiceUrl + environment.tallyGstApiUrl
        // this.tallyStockApiUrl = environment.baseServiceUrl + environment.tallyStockApiUrl
        // this.tallyBankApiUrl = environment.baseServiceUrl + environment.tallyBankApiUrl
    }

    getCustomers(): Observable<Party[]> {
        return this.http
            .get<Party[]>(this.customersApiUrl)
    }

    getPartiesByNameLike(type: string, name: string): Observable<Party[]> {
        //console.log('in getPartiesByNameLike');
        if (!name) {
            //console.log("if......");  
            return new Observable<Party[]>(null);
        }
        else {

            let params = new HttpParams();
            params = params.append("searchText", name);
            return this.http
                .get<Party[]>(this.partiesByNameApiUrl + "/" + type, {params: params})
        }

    }

    getIsCustomers(party: Party): boolean {
        //derive if IGST required
        //Karnataka state code is 29
        //console.log('Party in getIgstApplicable: ', party);
        let isCust = false;
        if (party !== undefined) {
            if (party.partyTypeId === 2) {
                isCust = false;
            } else {
                isCust = true;
            }
        }
        return isCust;
    }

    getIgstApplicable(party: Party): boolean {
        //derive if IGST required
        //Karnataka state code is 29
        //console.log('Party in getIgstApplicable: ', party);
        //console.log("party.stateCode",party.stateCode);
        //console.log("this.companyService.getCompany",this.companyService.getCompany());

        //console.log("compan"+this.companyService.getCompany().stateCode)
        let isIgst = false;
        if (party !== undefined) {
            if (party.stateCode === this.companyService.getCompany().stateCode) {
                isIgst = false;
            } else {
                isIgst = true;
            }
        }
        if (party.isIgst === 1) {
            isIgst = true;
        }
        return isIgst;
    }

    // getHeaders(): Headers {
    //     return this.tokenService.getMultiPartOptions().headers;
    // }


    getSuppliers(): Observable<Party[]> {
        return this.http
            .get<Party[]>(this.suppliersApiUrl)
        // .pipe(
        //     shareAndCache(AppSettings.LOCAL_STORAGE_ALL_SUPPLIERS_FOR_USER)
        // );

    }

    getCustomer(customerId: number): Observable<Party> {
        //console.log("party service...................................>>>>>");

        return this.http
            .get<Party>(this.customerApiUrl + "/" + customerId)

    }
    getCurrency(currencyId: number): Observable<Currency> {
        //console.log("party service...................................>>>>>");

        return this.http
            .get<Currency>(this.getCurrencyApiUrl + "/" + currencyId)

    }
    
    createCustomer(customer: Party): void {
        this.http.post(this.customerApiUrl, customer)
            .subscribe(data => {

            }, error => {
                console.error('Error Saving Customer', error);
            })
    }

    get(id: number): Observable<Party> {
        return this.http
            .get<Party>(this.supplierApiUrl + "/" + id);
    }
    save(party: Party): Observable<Party> {
        //console.log("saving....", party);

        return this.http.post<Party>(this.supplierApiUrl, party)

    }

    getPartyWithPriceList(id: number): Observable<PartyWithPriceList> {

        return this.http.get<PartyWithPriceList>(this.partyWithPriceListApiUrl + "/" + id)

    }

    getMaterialPriceListForParty(partyId: number): Observable<MaterialPriceList[]> {

        return this.http.get<MaterialPriceList[]>(this.materialPriceListForPartyApiUrl + "/" + partyId);

    }

    savePartyWithPriceList(data: PartyWithPriceList): Observable<PartyWithPriceList> {

        //console.log("saving....", data);

        return this.http.post<PartyWithPriceList>(this.partyWithPriceListApiUrl, data)

    }

    getAllPartyTypes(): Observable<PartyType[]> {

        return this.http.get<PartyType[]>(this.partyTypeApiUrl)

    }

    getPartyBypartyTypeOrPartyName(Name: string): Observable<PartyWithPriceList[]> {
        return this.http
            .get<PartyWithPriceList[]>(this.partyWithPriceListApiUrl + "/" + Name)
    }



    getAllPartiesWithPriceList(): Observable<PartyWithPriceList[]> {
        //console.log('calling: ', this.partyWithPriceListUrl);

        return this.http
            .get<PartyWithPriceList[]>(this.partyWithPriceListUrl)
    }

    deleteParty(partyId: number): Observable<TransactionResponse> {

        return this.http
            .delete<TransactionResponse>(this.partyWithPriceListApiUrl + "/" + partyId)
    }


    delete(id: string): Observable<PartyWithPriceList> {
        //console.log("Deleting...", id);
        return this.http.delete<PartyWithPriceList>(this.apiDeleteUrl + "/" + id)
    }

    // getAllPartiesForReport(partyTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<PartyReport> {
    //     sortColumn = sortColumn ? sortColumn : 'name';
    //     sortDirection = sortDirection ? sortDirection : 'asc';
    //     page = page ? page + 1 : 1;
    //     pageSize = pageSize ? pageSize : 30;
    //     filterText = filterText ? filterText : 'null';

    //     return this.http
    //         .get<PartyReport>(this.partyReportApiUrl + "/" + partyTypeId + "/" + filterText + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize);
    // }

    getAllPartiesForReport(partyTypeId: number, partyReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<Party>> {
        page.sortColumn = page.sortColumn ? page.sortColumn : "name"

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        //console.log("partyReportRequest: ", partyReportRequest);

        return this.http
            .post<PartyReport>(this.partyReportApiUrl
                + "/" + partyTypeId
                + "/" + pageSortColumn
                + "/" + pageSortDirection
                + "/" + pageNumber
                + "/" + pageSize
                , partyReportRequest).pipe(
                    map(partyData => this.getPagedData(page, partyData)));
    }

    private getPagedData(page: Page, partyReport: PartyReport): PagedData<Party> {

        ////console.log("materialReport: ",materialReport);
        let pagedData = new PagedData<Party>();

        if (page) {
            page.totalElements = partyReport.totalCount ? partyReport.totalCount : 0;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = partyReport.parties;
        ////console.log('materialReport.materials: ', materialReport.materials);
        ////console.log("page ", page);
        return pagedData;
    }

    getRecentgetParties(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentParty> {
        sortColumn = sortColumn ? sortColumn : 'name';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        //console.log("filterText: ", filterText);

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        return this.http
            .get<RecentParty>(this.recentPartiesUrl + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, { params: params });
    }


    checkPartyStatus(id: Number): Observable<boolean> {
        //console.log("id....", id);
        return this.http.get<boolean>(this.partyStatusApiUrl + "/" + id)

    }

    checkPartyCount(): Observable<TransactionResponse> {
        //console.log("id....", id);
        return this.http.get<TransactionResponse>(this.partyCountApiUrl)

    }

    checkPartyNumber(partyCode: string, partyTypeId: number,): Observable<TransactionResponse> {
       let params = new HttpParams();
       params = params.append("partyCode", partyCode);
        return this.http.get<TransactionResponse>(this.partyNumberCheckApiUrl+ "/" + partyTypeId, { params: params })

    }


    // getTallyPartiesXML(partyTypeId: string): Observable<any>{
    //     const httpOption: Object = {       
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };

    //     return this.http.get<any>(this.tallyPartyApiUrl + "/" +partyTypeId, httpOption)
    //     .pipe(
    //         map(res => new Blob([res['body']],{ type: 'application/xml' }))
    //     )
    // }

    // getTallyUomXML(): Observable<any>{
    //     const httpOption: Object = {       
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };

    //     return this.http.get<any>(this.tallyUomApiUrl, httpOption)
    //     .pipe(            
    //         map(res => new Blob([res['body']],{ type: 'application/xml' }))
    //     )
    // }

    // getTallyGstXML(): Observable<any>{
    //     const httpOption: Object = {       
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };

    //     return this.http.get<any>(this.tallyGstApiUrl, httpOption)
    //     .pipe(            
    //         map(res => new Blob([res['body']],{ type: 'application/xml' }))
    //     )
    // }


    // getTallyStockXML(): Observable<any>{
    //     const httpOption: Object = {       
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };

    //     return this.http.get<any>(this.tallyStockApiUrl, httpOption)
    //     .pipe(            
    //         map(res => new Blob([res['body']],{ type: 'application/xml' }))
    //     )
    // }

    // getTallyBankXML(): Observable<any>{
    //     const httpOption: Object = {       
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };

    //     return this.http.get<any>(this.tallyBankApiUrl, httpOption)
    //     .pipe(            
    //         map(res => new Blob([res['body']],{ type: 'application/xml' }))
    //     )
    // }


   

    getPriceListReport(partyTypeId: number, partyReportRequest: TransactionReportRequest, page: Page,allowAllMaterial:number): Observable<PagedData<PartyPriceListReport>> {
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection
        if(partyReportRequest.partyId){
            pageSortDirection  = page ? "desc" : "desc";
        }
        else{
            pageSortDirection  = page ? page.sortDirection : "desc"; 
        }
    console.log(partyReportRequest.materialId+"-----")
        partyReportRequest.materialId=allowAllMaterial;
        let pageSortColumn = page ? page.sortColumn : null;
        return this.http
            .post<PartyMaterialPriceListDTO>(this.partyPriceListApiUrl
                + "/" + partyTypeId
                + "/" + pageSortColumn
                + "/" + pageSortDirection
                + "/" + pageNumber
                + "/" + pageSize
                , partyReportRequest).pipe(
                    map(priceListData => this.getPagedDatas(page, priceListData)));



    }



    private getPagedDatas(page: Page, priceListData: PartyMaterialPriceListDTO): PagedData<PartyPriceListReport> {

        //console.log("payableReceivableData: ", payableReceivableData);
        let pagedData = new PagedData<PartyPriceListReport>();
        if (page) {
            page.totalElements = priceListData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = priceListData.partyPriceListReport;

        return pagedData;
    }


}