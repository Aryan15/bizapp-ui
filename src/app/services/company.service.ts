import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Company, CompanyWithBankListDTO, AboutCompany } from '../data-model/company-model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageMetadata } from '../data-model/misc-model';
import { AppSettings } from '../app.settings';
import { flatMap, map } from 'rxjs/operators';

@Injectable()
export class CompanyService {

    // apiUrl : string;
    companyApiUrl: string;
    companyWithBankApiUrl: string;
    companyLogoApiUrl: string;
    certImageApiUrl: string;
    companyLogoMetadataUrl: string;
    companyLogoBase64: string;
    certImageBase64: string;
    signatureLogoBase64: string;
    aboutCompanyApiUrl: string;
    companyTransactionsApiUrl: string;
    signatureImageApiUrl: string;
    public companyLogoUrl: string = environment.baseServiceUrl + environment.filesCompanyApi;
    filesCompanyBase64Api: string = environment.baseServiceUrl + environment.filesCompanyBase64Api;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient, private tokenService: TokenService
        , private domSanitizer: DomSanitizer) {
        // this.apiUrl = environment.baseServiceUrl + environment.countryApiUrl;//'http://localhost:8081/api/country';
        this.companyApiUrl = environment.baseServiceUrl + environment.companyApiUrl;//'http://localhost:8081/api/countries';
        this.companyWithBankApiUrl = environment.baseServiceUrl + environment.companyWithBankApiUrl;//'http://localhost:8081/api/countries';
        this.companyLogoApiUrl = environment.baseServiceUrl + environment.companyLogoApiUrl;
        this.certImageApiUrl = environment.baseServiceUrl + environment.certImageApiUrl;
        this.companyLogoMetadataUrl = environment.baseServiceUrl + environment.companyLogoMetadataUrl;
        this.aboutCompanyApiUrl = environment.baseServiceUrl + environment.aboutCompanyApiUrl;
        this.companyTransactionsApiUrl = environment.baseServiceUrl + environment.companyTransactionsApiUrl;
        this.signatureImageApiUrl = environment.baseServiceUrl + environment.signatureImageApiUrl;
    }


    // getHeaders(): Headers {
    //     return this.tokenService.getMultiPartOptions().headers;
    // }

    getCurrentCompanyLogo(id: number): Observable<TransactionResponse> {

        //let currentCompanyLogoName : string;

        return this.http
            .get<TransactionResponse>(this.companyLogoApiUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
        // .subscribe(response => {
        //     currentCompanyLogoName = response;
        // });

    }

    getCurrentCompanyCertImage(id: number): Observable<TransactionResponse> {

        //let currentCompanyLogoName : string;

        return this.http
            .get<TransactionResponse>(this.certImageApiUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
        // .subscribe(response => {
        //     currentCompanyLogoName = response;
        // });

    }


    getSignatureImage(id: number): Observable<TransactionResponse> {

        //let currentCompanyLogoName : string;

        return this.http
            .get<TransactionResponse>(this.signatureImageApiUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
        // .subscribe(response => {
        //     currentCompanyLogoName = response;
        // });

    }

    getCompanyTransactions(): Observable<TransactionResponse> {
        return this.http
            .get<TransactionResponse>(this.companyTransactionsApiUrl);
    }
    getCompanyLogoUrl(): string {
        return this.companyLogoApiUrl;
    }

    save(company: CompanyWithBankListDTO): Observable<CompanyWithBankListDTO> {
        //console.log("saving....", company);

        return this.http.post<CompanyWithBankListDTO>(this.companyApiUrl, company)
        // return this.http.post(this.companyApiUrl, JSON.stringify(company), this.tokenService.getOptions())
        //     .map(response => <Company>response.json());
    }

    get(id: number): Observable<Company> {
        return this.http
            .get<Company>(this.companyApiUrl + "/" + id)

        // return this.http
        //     .get(this.companyApiUrl + "/" + id, this.tokenService.getOptions())
        //     .map(response => <Company>response.json());
    }

    getCompany(): Company {
        return JSON.parse(localStorage.getItem(AppSettings.CURRENT_COMPANY)).companyDTO;
    }


    getWithBankMap(id: number): Observable<CompanyWithBankListDTO> {
        return this.http
            .get<CompanyWithBankListDTO>(this.companyWithBankApiUrl + "/" + id)
        // return this.http
        //     .get(this.companyApiUrl + "/" + id, this.tokenService.getOptions())
        //     .map(response => <Company>response.json());
    }

    getAboutCompany(): Observable<AboutCompany> {
        return this.http.get<AboutCompany>(this.aboutCompanyApiUrl)
    }


    loadImage(url: string): Observable<Blob> {
        return this.http
            // load the image as a blob
            .get(url, { responseType: 'blob' })
        //.do(response => //console.log("in loadImage: ",response))
        // create an object url of that blob that we can use in the src attribute
        //.map(e => URL.createObjectURL(e))
        //.map(e => this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(e)));
    }

    loadImageBase64(url: string): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(url)

    }

    getCompanyLogoMetadata(imageName: String): Observable<ImageMetadata> {
        //console.log("getCompanyLogoMetadata: ", imageName);
        return this.http.get<ImageMetadata>(this.companyLogoMetadataUrl + "/" + imageName + "/" + 1)
    }

    getImageMetadata(): Observable<ImageMetadata> {

        return this.getCurrentCompanyLogo(1)
            .pipe(
                flatMap(result => {
                    if(result.responseStatus !== 0){
                        return this.http.get<ImageMetadata>(this.companyLogoMetadataUrl + "/" + result.responseString + "/" + 1)
                    }else{
                        return new Observable<ImageMetadata>(observer => {
                            observer.next(null),
                                observer.complete()
                        })
                    }
                })
            )

    }

    getCertMetadata(): Observable<ImageMetadata> {

        return this.getCurrentCompanyCertImage(1)
            .pipe(
                flatMap(result => {
                    if(result.responseStatus !=0 ){
                        return this.http.get<ImageMetadata>(this.companyLogoMetadataUrl + "/" + result.responseString + "/" + 1)
                    }else{
                        return new Observable<ImageMetadata>(observer => {
                            observer.next(null),
                                observer.complete()
                        })
                    }
                })
            )

    }


    getSignatureMetadata(isSignatureRequired: number): Observable<ImageMetadata> {

        if (isSignatureRequired === 1) {
            return this.getSignatureImage(1)
                .pipe(
                    flatMap(result => {
                        if(result.responseStatus !== 0){
                            return this.http.get<ImageMetadata>(this.companyLogoMetadataUrl + "/" + result.responseString + "/" + 1)
                        }else{
                            return new Observable<ImageMetadata>(observer => {
                                observer.next(null),
                                    observer.complete()
                            })
                        }

                    })

                )
        } else {
            return new Observable(observer => {
                observer.next(null),
                    observer.complete()
            })
        }
    }






    getCompanyLogoAsBase64(companyId: number): Observable<any> {


        return this.getCurrentCompanyLogo(companyId)
            .pipe(
                flatMap(result => {
                    if(result.responseStatus !== 0){
                        return this.loadImageBase64(this.filesCompanyBase64Api + "/" + result.responseString)
                    }else{
                        return new Observable<any>(observer => {
                            observer.next({responseStatus: 0, responseString: null}),
                                observer.complete()
                        })
                    }
                }),
                map(result => result.responseString)
            )


    }


    getCertImageAsBase64(companyId: number): Observable<any> {

        return this.getCurrentCompanyCertImage(companyId)
            .pipe(
                flatMap(result => {
                    if(result.responseStatus !== 0){
                        return this.loadImageBase64(this.filesCompanyBase64Api + "/" + result.responseString)
                    }else{
                        return new Observable<any>(observer => {
                            observer.next({responseStatus: 0, responseString: null}),
                                observer.complete()
                        })
                    }
                }),
                map(result => result.responseString)
            )
    }


    getSignatureImageAsBase64(companyId: number, isSignatureRequired: number): Observable<any> {
        if (isSignatureRequired === 1) {
            return this.getSignatureImage(companyId)
                .pipe(
                    flatMap(result => {
                        if(result.responseStatus !== 0){
                            return this.loadImageBase64(this.filesCompanyBase64Api + "/" + result.responseString)
                        }else{
                            return new Observable<any>(observer => {
                                observer.next({responseStatus: 0, responseString: null}),
                                    observer.complete()
                            }) 
                        }
                    }),
                    map(result => result.responseString)
                )
        } else {
            return new Observable(observer => {
                observer.next(null),
                    observer.complete()
            })
        }
    }





}