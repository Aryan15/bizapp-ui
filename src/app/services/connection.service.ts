import { Injectable } from '@angular/core';
import { Headers, RequestOptions, RequestOptionsArgs, ResponseContentType  } from '@angular/http';
import { AppSettings } from '../app.settings';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class ConnectionService{

    connectionInfo = new BehaviorSubject<String>(null);

    connectionInfoData = this.connectionInfo.asObservable();


    warningInfo = new BehaviorSubject<String>(null);

    warningInfoData = this.warningInfo.asObservable();
}