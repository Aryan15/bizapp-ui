import { Injectable } from '@angular/core';
import { Headers, RequestOptions, RequestOptionsArgs, ResponseContentType  } from '@angular/http';
import { AppSettings } from '../app.settings';

@Injectable()
export class TokenService{

    token : string;
    options : RequestOptions;
    headers = new Headers({'Content-Type': 'application/json'});
    currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));

    getOptions() : RequestOptions{
        //var currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));

        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
        this.token = this.currentUser && this.currentUser.token;

        let token : string = this.token?'Bearer '+this.token:null;
        
        let headers = new Headers({ 'Authorization': token
                                            ,'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: headers });

        return this.options;
    }   

    getToken(): string {

        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
        this.token = this.currentUser && this.currentUser.token;

        let token : string = this.token?'Bearer '+this.token:null;

        return token;
    }

    getOptionsWithContentTypeImage(): RequestOptions{

        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
        this.token = this.currentUser && this.currentUser.token;

        let token : string = this.token?'Bearer '+this.token:null;
        
        let headers = new Headers({ 'Authorization': token
                                            ,'Content-Type': 'image/jpg' });
        this.options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

        return this.options;
    }   
    
    getMultiPartOptions() : RequestOptions{

        this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
        this.token = this.currentUser && this.currentUser.token;

        let token : string = this.token?'Bearer '+this.token:null;
        
        let headers = new Headers({ 'Authorization': token });
        this.options = new RequestOptions({ headers: headers });

        return this.options;
    }   
}