
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DeliveryChallanHeader, RecentDeliveryChallan, DeliveryChallanReportRequest, DeliveryChallanItemModel, DeliveryChallanItemReport, DeliveryChallanHeaderWrapper } from '../data-model/delivery-challan-model';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { PagedData } from "../reports/model/paged-data";
import { Page } from "../reports/model/page";
import { TransactionResponse } from '../data-model/transaction-response';
@Injectable()
export class DeliveryChallanService {
    apiGetAllUrl: string;
    apiPostUrl: string;
    apiGetUrl: string;
    apiGetDeliveryChallanByType: string;
    apiGetDeliveryChallansForParty: string;
    apiGetDeliveryChallanByDeliveryChallanNumber: string;
    apiGetDeliveryChallansForPartyAndTransactionType: string;
    apiDeleteUrl: string;
    apiDeleteDcItemUrl: string;
    recentDeliveryChallanApiUrl: string;
    deliveryChallanItemReportApiUrl: string;
    checkDCNumberApiUrl: string;
    // dcJasperReportApiUrl: string;
    // dcSubReportApiUrl: string;

    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.apiGetAllUrl = environment.baseServiceUrl + environment.deliveryChallanApiGetAllUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.deliveryChallanApiPostUrl;
        this.apiGetUrl = environment.baseServiceUrl + environment.deliveryChallanApiGetUrl;
        this.apiGetDeliveryChallanByType = environment.baseServiceUrl + environment.deliveryChallanApiGetByType;
        this.apiGetDeliveryChallansForParty = environment.baseServiceUrl + environment.deliveryChallanApiGetForParty;
        this.apiGetDeliveryChallanByDeliveryChallanNumber = environment.baseServiceUrl + environment.deliveryChallanApiGetByPurchaseOrderNumber;
        this.apiGetDeliveryChallansForPartyAndTransactionType = environment.baseServiceUrl + environment.deliveryChallanApiGetForPartyAndTransactionType;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.deliveryChallanApiGetUrl;
        this.apiDeleteDcItemUrl = environment.baseServiceUrl + environment.deliveryChallanItemDeleteApiUrl;
        this.recentDeliveryChallanApiUrl = environment.baseServiceUrl + environment.recentDeliveryChallanApiUrl;
        this.deliveryChallanItemReportApiUrl = environment.baseServiceUrl + environment.deliveryChallanItemReportApiUrl;
        this.checkDCNumberApiUrl = environment.baseServiceUrl + environment.checkDCNumberApiUrl;
        // this.dcJasperReportApiUrl = environment.baseServiceUrl + environment.dcJasperReportApiUrl;
        // this.dcSubReportApiUrl = environment.baseServiceUrl + environment.dcSubReportApiUrl;
    }

    getDeliveryChallansForParty(partyId: number, poType: string): Observable<DeliveryChallanHeader[]> {
        return this.http.get<DeliveryChallanHeader[]>(this.apiGetDeliveryChallansForParty + "/" + partyId + "/" + poType)
    }

    getDeliveryChallan(id: string): Observable<DeliveryChallanHeader> {
        return this.http.get<DeliveryChallanHeader>(this.apiGetUrl + "/" + id );
    }

    getDeliveryChallansForPartyAndTransactionType(partyId: number, transactionTypeName: string , ttype:string): Observable<DeliveryChallanHeader[]> {
        //console.log("partyId: ", partyId, " Transaction type name: ", transactionTypeName);
        return this.http.get<DeliveryChallanHeader[]>(this.apiGetDeliveryChallansForPartyAndTransactionType + "/" + partyId + "/" + transactionTypeName + "/" + ttype)
    }

    create(deliveryChallanHeaderWrapper: DeliveryChallanHeaderWrapper): Observable<DeliveryChallanHeader> {
        //return null;

        ////console.log("In create...", model.id);
        return this.http.post<DeliveryChallanHeader>(this.apiPostUrl, deliveryChallanHeaderWrapper)
    }

    getAllDeliveryChallans(): Observable<DeliveryChallanHeader[]> {
        return this.http.get<DeliveryChallanHeader[]>(this.apiGetAllUrl)
    }

    getDeliveryChallanByDeliveryChallanNumber(dcNumber: string): Observable<DeliveryChallanHeader> {
        return this.http.get<DeliveryChallanHeader>(this.apiGetDeliveryChallanByDeliveryChallanNumber + "/" + dcNumber)
        // .map(response => <DeliveryChallanHeader>response.json());
    }
    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }
    deleteItem(id: string): Observable<TransactionResponse> {

        return this.http.delete<TransactionResponse>(this.apiDeleteDcItemUrl + "/" + id);
    }

    getRecentDeliveryChallans(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentDeliveryChallan> {
        sortColumn = sortColumn ? sortColumn : 'deliveryChallanDate';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        sortColumn = (sortColumn === "deliveryChallanNumber") ? "deliveryChallanId" : sortColumn;

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        
        //console.log("filterText: ", filterText);

        return this.http
            .get<RecentDeliveryChallan>(this.recentDeliveryChallanApiUrl + "/" + transactionTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }

    getDeliveryChallanItemReport(transactionTypeId: number,deliveryChallanReportRequest: DeliveryChallanReportRequest, page: Page): Observable<PagedData<DeliveryChallanItemModel>> {
     
       // //console.log("dc rport "+page.sortColumn +deliveryChallanReportRequest+page);
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        page.sortColumn =page?  page.sortColumn ? page.sortColumn : "deliveryChallanDate" : null
        let pageSortColumn = page ? page.sortColumn : "deliveryChallanDate"; //Pass this some value l
        pageSortColumn = (pageSortColumn === "deliveryChallanNumber") ? "deliveryChallanId" : pageSortColumn;
        
        return this.http.post<DeliveryChallanItemReport>(this.deliveryChallanItemReportApiUrl
            + "/" + transactionTypeId + "/" + pageNumber + "/" + pageSize
              + "/" + pageSortDirection + "/" + pageSortColumn, deliveryChallanReportRequest).pipe(
            map(deliveryChallanItemData => this.getPagedData(page, deliveryChallanItemData)));
    }
    private getPagedData(page: Page, deliveryChallanItemData: DeliveryChallanItemReport): PagedData<DeliveryChallanItemModel> {

        //console.log("deliveryChallanItemData: ", deliveryChallanItemData);
        let pagedData = new PagedData<DeliveryChallanItemModel>();
        if(page){
        page.totalElements = deliveryChallanItemData.totalCount;
        page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = deliveryChallanItemData.deliveryChallanReportItems;
        //console.log('deliveryChallanItemData.invoiceReportItems: ', deliveryChallanItemData.deliveryChallanReportItems);
        //console.log("page ", page);
        return pagedData;
    }

    checkDcNumAvailability(dcNumber: string, transactionTypeId: number, partyId:number): Observable<TransactionResponse> {
        //console.log("quatationNumber in serice", dcNumber)

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("dcNumber", dcNumber);

        return this.http.get<TransactionResponse>(this.checkDCNumberApiUrl  + "/" + partyId + "/"+ transactionTypeId, {params: params})
        // .map(response => <TransactionResponse>response.json());

    }


    


    // subreport(): Observable<any>{

    //     const httpOption: Object = {
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer' //ResponseContentType.Blob 
    //       };
          
    //     return this.http.get<any>(this.dcSubReportApiUrl, httpOption)
    //     // .pipe(
    //     //     map(
    //     //     (res) => {
    //     //         return new Blob([res.blob()], { type: 'application/pdf' })
    //     //     })
    //     // )
    //     .pipe(
    //         //tap(res => //console.log("res: ", res)),
    //     map(res => new Blob([res['body']],{ type: 'application/pdf' }))
    //     )
    // }
}