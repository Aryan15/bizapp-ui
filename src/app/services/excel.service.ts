import { Injectable } from '@angular/core';
//import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { CellObject } from 'xlsx';

type AOA = any[][];

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  
    constructor(){ }

	// public exportAsExcelFile(json: any[], excelFileName: string, headersArray: any[]): void {
	// 	//Excel Title, Header, Data
	// 	const header = headersArray;
	// 	const data = json;
	// 	//Create workbook and worksheet
	// 	let workbook = new Workbook();
	// 	let worksheet = workbook.addWorksheet(excelFileName);
	// 	//Add Header Row
	// 	let headerRow = worksheet.addRow(header);
	// 	// Cell Style : Fill and Border
	// 	headerRow.eachCell((cell, number) => {
	// 	  cell.fill = {
	// 		type: 'pattern',
	// 		pattern: 'solid',
	// 		fgColor: { argb: 'FFFFFF00' },
	// 		bgColor: { argb: 'FF0000FF' }
	// 	  }
	// 	  cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
	// 	})
	// 	// Add Data and Conditional Formatting
	// 	data.forEach((element) => {
	// 	  let eachRow = [];
	// 	  headersArray.forEach((headers) => {
	// 		eachRow.push(element[headers])
	// 	  })
	// 	  if (element.isDeleted === "Y") {
	// 		let deletedRow = worksheet.addRow(eachRow);
	// 		deletedRow.eachCell((cell, number) => {
	// 		  cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
	// 		})
	// 	  } else {
	// 		worksheet.addRow(eachRow);
	// 	  }
	// 	})
	// 	worksheet.getColumn(3).width = 15;
	// 	worksheet.getColumn(4).width = 20;
	// 	worksheet.getColumn(5).width = 30;
	// 	worksheet.getColumn(6).width = 30;
	// 	worksheet.getColumn(7).width = 10;
	// 	worksheet.addRow([]);
	// 	//Generate Excel File with given name
	// 	workbook.xlsx.writeBuffer().then((data) => {
	// 	  let blob = new Blob([data], { type: EXCEL_TYPE });
	// 	  fs.saveAs(blob, excelFileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
	// 	})
	//   }
	

    // public exportAsExcelFile(json: any[], excelFileName: string, headersArray: any[]): void {
	// 	//Excel Title, Header, Data
	// 	const header = headersArray;
	// 	const data = json;
	// 	//Create workbook and worksheet
	// 	const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
	// 	/* generate workbook and add the worksheet */
	// 	const wb: XLSX.WorkBook = XLSX.utils.book_new();
	// 	const workbook: XLSX.WorkBook = {Sheets: {'data': wb}, SheetNames: ['data']};
	// 	let headerRow = ws.addRow(header);
	
	
	// 	headerRow.eachCell((cell, number) => {
	// 	  cell.fill = {
	// 		type: 'pattern',
	// 		pattern: 'solid',
	// 		fgColor: { argb: 'FFFFFF00' },
	// 		bgColor: { argb: 'FF0000FF' }
	// 	  }
	// 	  cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
	// 	})

	// 	XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
		
	// 		/* save to file */
	// 		XLSX.writeFile(wb, excelFileName+'_export_'+ new Date().getTime() + EXCEL_EXTENSION);
			
	//   }

    
    public exportFromJSON(json: any[], exceFileName: string): void {
		/* generate worksheet */
		const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
		//this.wrapAndCenterCell(ws.A1);
		
		/* generate workbook and add the worksheet */
		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		
		//const workbook: XLSX.WorkBook = {Sheets: {'data': wb}, SheetNames: ['data']};
		XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

		/* save to file */
		XLSX.writeFile(wb, exceFileName+'_export_'+ new Date().getTime() + EXCEL_EXTENSION);
	}
	
	private wrapAndCenterCell(cell: CellObject) {
		const wrapAndCenterCellStyle = { font: {bold:true}  };
		//console.log("wrapAndCenterCellStyle",wrapAndCenterCellStyle,cell)
		this.setCellStyle(cell, wrapAndCenterCellStyle);
	  }
	 
	  private setCellStyle(cell, style: {}) {
		cell.s = style;
		//console.log("	cell.s ",cell.s)
	  }
	 
    
}