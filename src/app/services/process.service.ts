import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { Tax } from '../data-model/tax-model';

import { SettingsWrapper, GlobalSetting, StockUpdateOn } from '../data-model/settings-wrapper';
import { PrintCopiesWrapper, PrintCopy } from '../data-model/print-copies-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { Process, ProcessWrapper } from '../data-model/process-model';

@Injectable({
    providedIn: 'root'
})
export class ProcessService {


    processApiUrl: string;
    printProcessApiUrl: string;
    processDeleteUrl: string;

    private headers = new Headers({ 'Content-Type': 'application/json' });

    returnGlobalSetting: Observable<GlobalSetting> = null;

    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.processApiUrl = environment.baseServiceUrl + environment.processApiUrl
        this.printProcessApiUrl = environment.baseServiceUrl + environment.printProcessApiUrl
        this.processDeleteUrl = environment.baseServiceUrl + environment.processDeleteUrl

    }


    save(processObject: ProcessWrapper): Observable<Process[]> {
        //console.log("processObject..........",processObject.processList);
        
        return this.http
            .post<Process[]>(this.printProcessApiUrl, processObject.processList)
    }

    delete(id: number) : Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.processDeleteUrl+"/"+id)
    }

    getAllProcess() : Observable<Process[]>{
        return this.http.get<Process[]>(this.processApiUrl)
    }
}