
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Material, MaterialReport, Process, RecentMaterial, MaterialStockCheckDTOWrapper } from '../data-model/material-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { Page } from "../reports/model/page";
import { PagedData } from "../reports/model/paged-data";
import { TokenService } from './token.service';

@Injectable()
export class MaterialService {

    materialsApiUrl: string;
    materialApiUrl: string;
    materialByMaterialTypeIdApiUrl: string;
    materialByNameAndSupplTypeAndPartyApiUrl: string;
    materialSearchApiUrl: string;
    materialReportApiUrl: string;
    materialForPartyApiUrl:string;
    apiDeleteUrl: string;
    recentMaterialUrl: string;
    materialCheckpartNumberApiUrl: string;
    materialCheckNameApiUrl: string;
    getAllProcessApiUrl: string;
    jobwokMaterialsApiUrl: string;
    minStockCheckApiUrl: string;
    materialExistsInTransactionApiUrl: string;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.materialsApiUrl = environment.baseServiceUrl + environment.materialsApiUrl;
        this.materialApiUrl = environment.baseServiceUrl + environment.materialApiUrl;
        this.materialByMaterialTypeIdApiUrl = environment.baseServiceUrl + environment.materialByMaterialTypeIdApiUrl;
        this.materialSearchApiUrl = environment.baseServiceUrl + environment.materialSearchApiUrl;
        this.materialByNameAndSupplTypeAndPartyApiUrl = environment.baseServiceUrl + environment.materialByNameAndSupplTypeAndPartyApiUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.materialApiUrl;
        this.materialReportApiUrl = environment.baseServiceUrl + environment.materialReportApiUrl;
        this.recentMaterialUrl = environment.baseServiceUrl + environment.recentMaterialUrl;
        this.materialCheckpartNumberApiUrl= environment.baseServiceUrl + environment.materialCheckpartNumberApiUrl;
        this.materialCheckNameApiUrl= environment.baseServiceUrl + environment.materialCheckNameApiUrl;
        this.materialForPartyApiUrl = environment.baseServiceUrl + environment.materialForPartyApiUrl;
        this.getAllProcessApiUrl = environment.baseServiceUrl + environment.getAllProcessApiUrl;
        this.jobwokMaterialsApiUrl = environment.baseServiceUrl + environment.jobwokMaterialsApiUrl;
        this.minStockCheckApiUrl = environment.baseServiceUrl + environment.minStockCheckApiUrl;
        this.materialExistsInTransactionApiUrl = environment.baseServiceUrl + environment.materialExistsInTransactionApiUrl;
    }

    getAllMaterials(): Observable<Material[]> {
        return this.http
            .get<Material[]>(this.materialsApiUrl)
            // .pipe(
            //     shareAndCache(AppSettings.LOCAL_STORAGE_ALL_MATERIALS_FOR_USER)
            // )
        // .map(response => <Material[]>response.json());
    }

    getJobworkMaterials(value: string): Observable<Material[]> {
        if(!value){
            return new Observable<Material[]>(null);
        }
        let params = new HttpParams();
        params = params.append("searchText", value);

        return this.http
            .get<Material[]>(this.jobwokMaterialsApiUrl, {params: params})
            // .pipe(
            //     filter(res => res.filter(r => r.name.toLowerCase().indexOf(value) >= 0).length > 0)

            // )
            // .pipe(
            //     shareAndCache(AppSettings.LOCAL_STORAGE_ALL_MATERIALS_FOR_USER)
            // )
        // .map(response => <Material[]>response.json());
    }

    getMaterialByPartNumberOrPartName(partNumberOrPartName: string): Observable<Material[]> {
        if(!partNumberOrPartName){
            return new Observable<Material[]>(null);
        }
        return this.http
            .get<Material[]>(this.materialSearchApiUrl + "/" + partNumberOrPartName)
        // .map(response => <Material[]>response.json());
    }


    getMaterialByNameAndSupplTypeAndParty(name: string, supplyTypeId: number, partyId: number, buyOrSell: string): Observable<Material[]> {
        //console.log('getMaterialByNameAndSupplTypeAndParty: ', name, supplyTypeId, partyId, buyOrSell )
        if(!name || !partyId || !buyOrSell){
            //console.log("name :", name ,"partyId :", partyId , "buyOrSell :",buyOrSell);
            
            return new Observable<Material[]>(null);
        }
        let params = new HttpParams();
        params = params.append("searchText", name);

        return this.http
            .get<Material[]>(this.materialByNameAndSupplTypeAndPartyApiUrl + "/"+ supplyTypeId + "/" + partyId + "/" + buyOrSell, {params: params});
        // .map(response => <Material[]>response.json());
    }

    getMaterial(id: number): Observable<Material> {
        return this.http
            .get<Material>(this.materialApiUrl)
        // .map(response => <Material>response.json());
    }

    getMaterialByMaterialTypeId(id: number): Observable<Material[]> {
        return this.http
            .get<Material[]>(this.materialByMaterialTypeIdApiUrl + "/" + id)
        // .map(response => <Material[]>response.json());
    }

   

    save(material: Material): Observable<Material> {

        return this.http
            .post<Material>(this.materialApiUrl, material)
        // .map(response => <Material>response.json());


    }

    delete(id: number): Observable<TransactionResponse> {
        // //console.log("Deleting...", id);
        // return this.http.delete(this.apiDeleteUrl+"/"+id, this.tokenService.getOptions())
        //     .map(response => <TransactionResponse>response.json());
        return this.http
            .delete<TransactionResponse>(this.materialApiUrl + "/" + id)
        // .map(response=> <TransactionResponse> response.json());
    }

    getRecentMaterials(supplyTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentMaterial> {
        sortColumn = sortColumn ? sortColumn : 'name';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        //console.log("filterText: ", filterText);

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        return this.http
            .get<RecentMaterial>(this.recentMaterialUrl + "/" + supplyTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }


    // getAllMaterialsForReport(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<MaterialReport>{
    //     sortColumn = sortColumn ? sortColumn : 'name';
    //     sortDirection = sortDirection ? sortDirection : 'asc';
    //     page = page ? page+1 : 1;
    //     pageSize = pageSize ? pageSize : 30;
    //     filterText = filterText ? filterText : 'null';

    //     return this.http
    //         .get<MaterialReport>(this.materialReportApiUrl+"/"+filterText+"/"+sortColumn+"/"+sortDirection+"/"+page+"/"+pageSize);
    // }

    getMaterilsForParty(partyId: number): Observable<Material[]>
    {
        return this.http
        .get<Material[]>(this.materialForPartyApiUrl + "/" + partyId)
    
    }



    getAllMaterialsForReport(supplyTypeId:number, filterText: string, page: Page): Observable<PagedData<Material>> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
        filterText = filterText ? filterText : 'null';

        // sortColumn = sortColumn ? sortColumn : 'name';
        // sortDirection = sortDirection ? sortDirection : 'asc';
        // page = page ? page+1 : 1;
        // pageSize = pageSize ? pageSize : 30;
        // filterText = filterText ? filterText : 'null';

        return this.http
            .get<MaterialReport>(this.materialReportApiUrl
            + "/" + supplyTypeId
            + "/" + filterText
            + "/" + pageSortColumn
            + "/" + pageSortDirection
            + "/" + pageNumber
            + "/" + pageSize).pipe(
            map(materialData => this.getPagedData(page, materialData)));
    }


    private getPagedData(page: Page, materialReport: MaterialReport): PagedData<Material> {

        //console.log("materialReport: ",materialReport);
        let pagedData = new PagedData<Material>();

        if (page) {
            page.totalElements = materialReport.totalCount ? materialReport.totalCount : 0;
            page.totalPages = page.totalElements / page.size;
        }
        // let start = page.pageNumber * page.size;
        // let end = Math.min((start + page.size), page.totalElements);
        // for (let i = start; i < end; i++){
        //     let jsonObj = companyData[i];
        //     let employee = new CorporateEmployee(jsonObj.name, jsonObj.gender, jsonObj.company, jsonObj.age);
        //     pagedData.data.push(employee);
        // }
        pagedData.page = page;
        pagedData.data = materialReport.materials;
        ////console.log('materialReport.materials: ', materialReport.materials);
        ////console.log("page ", page);
        return pagedData;
    }

    checkPartNumberAvailability(partNumber): Observable<TransactionResponse> {
        let params = new HttpParams();
        params = params.append("partNumber", partNumber);

        //console.log("partNumber in service", partNumber)
        return this.http.get<TransactionResponse>(this.materialCheckpartNumberApiUrl, {params: params})
        // .map(response => <TransactionResponse>response.json());

    }

    checkMaterialAvailability(name,id): Observable<Material[]> {

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("name", name);
        params = params.append("id", id ? id :'');

        return this.http.get<Material[]>(this.materialCheckNameApiUrl, {params: params})


    }

    getAllProcesses(): Observable<Process[]>{
        return this.http.get<Process[]>(this.getAllProcessApiUrl);
    }

    minStockValidate(materialStockCheckDTOWrapper: MaterialStockCheckDTOWrapper): Observable<MaterialStockCheckDTOWrapper>{

        return this.http.post<MaterialStockCheckDTOWrapper>(this.minStockCheckApiUrl, materialStockCheckDTOWrapper);
    }

    materialExistsinTransactions(materialId: number): Observable<number>{
        return this.http.get<number>(this.materialExistsInTransactionApiUrl+"/"+materialId)
    }
}
