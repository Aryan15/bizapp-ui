import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  selectedLanguage:BehaviorSubject<string> = new BehaviorSubject<string>('en');

  constructor() { }

  setLanguage(lng: string){
    this.selectedLanguage.next(lng);
  }

  getLanguage(): Observable<string>{
    return this.selectedLanguage.asObservable();
  }
}
