
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AppSettings } from '../app.settings';
import { Activity, ActivityRoleBinding, Menu, StatusBasedPermission } from '../data-model/activity-model';
import { Area } from '../data-model/area-model';
import { City } from '../data-model/city-model';
import { Country } from '../data-model/country-model';
import { State } from '../data-model/state-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { RecentUser, RegistrationModel, Role, RoleType, UserModel, UserRoleWrapper } from '../data-model/user-model';
import { HelpVideo, HelpVideoWrapper, HelpVideoRequest } from '../data-model/help-video-model';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { Page } from '../reports/model/page';
import { PagedData } from '../reports/model/paged-data';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    userPasswordApiUrl: string;
    usersApiUrl: string;
    rolesApiUrl: string;
    roleTypesApiUrl: string;
    userApiUrl: string;
    menusApiUrl: string;
    menusForRoleApiUrl: string;
    menusAllApiUrl: string;
    userRoleMappingApiUrl: string;
    apiDeleteUrl: string;
    countryApiUrl: string;
    stateApiUrl: string;
    cityApiUrl: string;
    areaApiUrl: string;
    userThemeApiUrl: string;
    userNewEmployeeApiUrl: string;
    activityRoleBindingForRoleUrl: string;
    apiGetStatusBasedPermissionUrl: string;
    userCheckUserNameApiUrl: string;
    userCheckUserNameOnRegistrationApiUrl: string;
    userCheckEmailOnRegistrationApiUrl: string;
    recentUserUrl: string;
    registerdUserApiUrl: string;
    registerdUserforCompanyApiUrl: string;
    userCheckEmpEmailApiUrl
    userCheckEmailApiUrl: string;
    userCheckPhoneNumberApiUrl: string;
    userCheckGstNumberApiUrl: string;
    returnMenus: Observable<Menu[]> = null;
    currentActivity: string;
    reportHeading: string;
    reportName: string;
    activityName: string;
    otpGenerateUrl: string;
    submitOtpUrl: string;
    resetPasswordUrl: string;
    resetCompleteUrl: string;
    disableLoginUrl: string;
    userLanguageApiUrl: string;
    helpVideoApiUrl:string;
    themeClass = new BehaviorSubject<string>(null);

    themeData = this.themeClass.asObservable();

    language = new BehaviorSubject<string>(null);

    languageData = this.language.asObservable();

    companyLogoPath = new BehaviorSubject<string>(null);

    companyLogoPathData = this.companyLogoPath.asObservable();

    private headers = new Headers({ 'Content-Type': 'application/json' });

    homePageLoadCounter: number = 0;

    constructor(private http: HttpClient) {
        this.usersApiUrl = environment.baseServiceUrl + environment.usersApiUrl;
        this.rolesApiUrl = environment.baseServiceUrl + environment.rolesApiUrl;
        this.roleTypesApiUrl = environment.baseServiceUrl + environment.roleTypesApiUrl;
        this.userApiUrl = environment.baseServiceUrl + environment.userApiUrl;
        this.menusApiUrl = environment.baseServiceUrl + environment.menusApiUrl;
        this.activityRoleBindingForRoleUrl = environment.baseServiceUrl + environment.activityRoleBindingForRoleUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.userApiUrl;
        this.apiGetStatusBasedPermissionUrl = environment.baseServiceUrl + environment.apiGetStatusBasedPermissionUrl;
        this.userNewEmployeeApiUrl = environment.baseServiceUrl + environment.userNewEmployeeApiUrl;
        this.userCheckUserNameApiUrl = environment.baseServiceUrl + environment.userCheckUserNameApiUrl;
        this.recentUserUrl = environment.baseServiceUrl + environment.recentUserUrl;
        this.registerdUserApiUrl = environment.baseServiceUrl + environment.registerdUserApiUrl;
        this.registerdUserforCompanyApiUrl = environment.baseServiceUrl + environment.registerdUserforCompanyApiUrl;
        this.countryApiUrl = environment.baseServiceUrl + environment.registrationCountryApiUrl;
        this.stateApiUrl = environment.baseServiceUrl + environment.registrationStateApiUrl;
        this.cityApiUrl = environment.baseServiceUrl + environment.registrationCityApiUrl;
        this.areaApiUrl = environment.baseServiceUrl + environment.registrationAreaApiUrl;
        this.userCheckUserNameOnRegistrationApiUrl = environment.baseServiceUrl + environment.userCheckUserNameOnRegistrationApiUrl;
        this.userCheckEmailOnRegistrationApiUrl = environment.baseServiceUrl + environment.userCheckEmailOnRegistrationApiUrl;
        this.menusForRoleApiUrl = environment.baseServiceUrl + environment.menusforRoleApiUrl;
        this.menusAllApiUrl = environment.baseServiceUrl + environment.menusAllApiUrl;
        this.userRoleMappingApiUrl = environment.baseServiceUrl + environment.userRoleMappingApiUrl;
        this.userCheckEmailApiUrl = environment.baseServiceUrl + environment.userCheckEmailApiUrl;
        this.userCheckPhoneNumberApiUrl = environment.baseServiceUrl + environment.userCheckPhoneNumberApiUrl;
        this.userCheckGstNumberApiUrl = environment.baseServiceUrl + environment.userCheckGstNumberApiUrl;
        this.userCheckEmpEmailApiUrl = environment.baseServiceUrl + environment.userCheckEmpEmailApiUrl;
        this.userThemeApiUrl = environment.baseServiceUrl + environment.userThemeApiUrl;
        this.userPasswordApiUrl = environment.baseServiceUrl + environment.userPasswordApiUrl
        this.otpGenerateUrl = environment.baseServiceUrl + environment.otpGenerateUrl;
        this.submitOtpUrl = environment.baseServiceUrl + environment.submitOtpUrl
        this.resetPasswordUrl = environment.baseServiceUrl + environment.resetPasswordUrl
        this.resetCompleteUrl = environment.baseServiceUrl + environment.resetCompleteUrl
        this.disableLoginUrl = environment.baseServiceUrl + environment.disableLoginUrl;
        this.userLanguageApiUrl = environment.baseServiceUrl + environment.userLanguageApiUrl;
        this.helpVideoApiUrl = environment.baseServiceUrl + environment.apiHelpVideoApiUrl;
    }

    getAllUsers(): Observable<UserModel[]> {

        return this.http
            .get<UserModel[]>(this.usersApiUrl)
        // .map(response => <UserModel[]>response.json());
    }

    getEmployeeByName(Name: string): Observable<UserModel[]> {
        return this.http
            .get<UserModel[]>(this.usersApiUrl + "/" + Name)
        // .map(response => <UserModel[]>response.json());
    }


    changePassword(user: UserModel): Observable<boolean> {
        //console.log("user.............", user);

        return this.http.post<boolean>(this.userPasswordApiUrl, user)
    }

    save(userModel: UserModel): Observable<UserModel> {
        //console.log("saving....", userModel);

        return this.http.post<UserModel>(this.userApiUrl, userModel)
        // .map(response => <UserModel>response.json())

    }

    generateOtp(userName: string): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.otpGenerateUrl + "/" + userName);
    }
    otpSubmit(otp: number, username: string): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.submitOtpUrl + "/" + otp + "/" + username);
    }

    passwordReset(username: string, password: string): Observable<boolean> {
        let user: UserModel = <UserModel>{};
        user.username = username;
        user.password = password;
        //console.log("user: ", user);
        return this.http.post<boolean>(this.resetPasswordUrl, user)
    }

    passwordResetComplete(username: string, resetValue: number): Observable<TransactionResponse>{
        return this.http.get<TransactionResponse>(this.resetCompleteUrl+"/"+username+"/"+resetValue);
    }
    saveUserRoleMapping(model: UserRoleWrapper, roleId: number): Observable<TransactionResponse> {
        //console.log('saving model, user:', this.userRoleMappingApiUrl);
        //console.log('saving model, user:', environment.userRoleMappingApiUrl);
        return this.http.post<TransactionResponse>(this.userRoleMappingApiUrl + "/" + roleId, model);
    }
    newEmployee(userModel: UserModel): Observable<TransactionResponse> {
        return this.http.post<TransactionResponse>(this.userNewEmployeeApiUrl, userModel);
        // .map(response => <TransactionResponse>response.json());

    }

    checkUsernameAvailability(username): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.userCheckUserNameApiUrl + "/" + username)
        // .map(response => <TransactionResponse>response.json());

    }

    checkUsernameAvailabilityOnRegistration(username): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.userCheckUserNameOnRegistrationApiUrl + "/" + username)
        // .map(response => <TransactionResponse>response.json());

    }

    checkEmailAvailabilityOnRegistration(username): Observable<TransactionResponse> {
        return this.http.get<TransactionResponse>(this.userCheckEmailOnRegistrationApiUrl + "/" + username)
        // .map(response => <TransactionResponse>response.json());

    }

    getAllRoleTypes(): Observable<RoleType[]> {
        return this.http
            .get<RoleType[]>(this.roleTypesApiUrl);
    }

    getAllRoles(): Observable<Role[]> {

        return this.http
            .get<Role[]>(this.rolesApiUrl)
        // .map(response => <Role[]>response.json());

    }

    createRole(roles: Role[]): Observable<Role[]> {
        return this.http.post<Role[]>(this.rolesApiUrl, roles);
    }
    deleteRole(id: number[]): Observable<TransactionResponse> {

        return this.http
            .delete<TransactionResponse>(this.rolesApiUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }

    // getAllMenus(): Observable<Menu[]> {

    //     return this.http
    //         .get<Menu[]>(this.menusApiUrl)
    //     // .map(response => <Menu[]>response.json());
    // }

    clearMenu() {
        this.returnMenus = null;
    }

    getMenuForRole(roleId: number): Observable<Menu[]> {
        return this.http
            .get<Menu[]>(this.menusForRoleApiUrl + "/" + roleId);
    }

    getAllMenu(): Observable<Menu[]> {

        return this.http
            .get<Menu[]>(this.menusAllApiUrl);
    }

    getMenusForUser(userName: string): Observable<Menu[]> {

        //console.log('looking menu for: ', userName);


        this.returnMenus = this.getLocalMenu(userName);

        ////console.log('this.returnMenus: ', this.returnMenus);

        if (!this.returnMenus) {
            //console.log('local menu are empty so getting from DB');
            this.returnMenus = this.http
                .get<Menu[]>(this.menusApiUrl + "/" + userName).pipe(
                    // .map(response => <Menu[]>response.json())
                    tap(response => this.storeLocalMenu(response)))

                ;
        }
        return this.returnMenus;
    }

    getLocalMenu(userName: string): Observable<Menu[]> {


        let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));

        let returnStoredMenus: Observable<Menu[]>;

        ////console.log('storedMenu: ', storedMenu);

        if (storedMenu) {
            returnStoredMenus = new Observable((observer) => {

                observer.next(storedMenu);
                observer.complete();
            });
        }

        return returnStoredMenus;

    }

    storeLocalMenu(menus: Menu[]) {

        try {
            localStorage.setItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER, JSON.stringify(menus));
        } catch (e) {
            //console.log("Local Storage is full, emptying menuForUser if any");
            localStorage.removeItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER);
        }
    }

    getActivityRoleBindingForRole(roleId: number): Observable<ActivityRoleBinding[]> {

        return this.http
            .get<ActivityRoleBinding[]>(this.activityRoleBindingForRoleUrl + "/" + roleId)
            ;
    }

    getActivityRoleBindingForUserLocal(username: string, pageUrl: string, urlParam: string): Observable<ActivityRoleBinding[]> {

        //console.log(username, pageUrl, urlParam);

        let returnARB: Observable<ActivityRoleBinding[]> = null;
        let userId: number;

        returnARB = new Observable(observer => {
            this.getMenusForUser(username).subscribe(menu => {

                menu.forEach(m => {
                    m.activities.forEach(act => {

                        if ((act.pageUrl ? act.pageUrl.includes(pageUrl) : false) && (act.urlParams ? (act.urlParams === urlParam) : true)) {
                            //observer.next(act.activityRoleBindings.filter(actrb => actrb.user.id === userId));
                            //console.log(act.pageUrl, act.urlParams);
                            observer.next(act.activityRoleBindings);
                        }
                    })
                })
            })
        });


        ////console.log('getActivityRoleBindingForUser returning: ', returnARB);
        return returnARB;

    }

    getActivityRoleBindingForUserLocal1(username: string, pageUrl: string, urlParam: string): ActivityRoleBinding[] {

        //console.log(username, pageUrl, urlParam);

        let returnARB: ActivityRoleBinding[] = null;
        let userId: number;


        this.getMenusForUser(username).subscribe(menu => {

            menu
                .filter(m => m.activities != null)
                .forEach(m => {
                    m.activities
                        .forEach(act => {

                            if ((act.pageUrl ? act.pageUrl.includes(pageUrl) : false) && (act.urlParams ? (act.urlParams === urlParam) : true)) {
                                //observer.next(act.activityRoleBindings.filter(actrb => actrb.user.id === userId));
                                //console.log(act.pageUrl, act.urlParams);
                                returnARB = act.activityRoleBindings;
                            }
                        })
                })
        })



        ////console.log('getActivityRoleBindingForUser returning: ', returnARB);
        return returnARB;

    }

    setActivityRoleBinding(route: ActivatedRoute): Observable<ActivityRoleBinding[]> {
        var currentUser: UserModel = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
        let returnARB: Observable<ActivityRoleBinding[]> = null;
        //console.log("currentUser", currentUser.username)
        returnARB = new Observable(observer => {
            //console.log("route.snapshot: ", route.snapshot.data.state);
            route.url.subscribe(urlSegment => {
                //console.log('here:', urlSegment);
                // this.getActivityRoleBindingForUserLocal(currentUser.username, urlSegment[0].path, urlSegment[1] ? urlSegment[1].path : null).subscribe(arb => {

                //     observer.next(arb);

                // });
                if(urlSegment.length > 0){
                    observer.next(this.getActivityRoleBindingForUserLocal1(currentUser.username, urlSegment[0].path, urlSegment[1] ? urlSegment[1].path : null))
                }else{
                    observer.next(this.getActivityRoleBindingForUserLocal1(currentUser.username, route.snapshot.data.state, null))
                }
                
            })
        });

        ////console.log('this.returnARB ', returnARB);
        return returnARB;
    }
    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }

    getStatusBasedPermission(statusId: number): Observable<StatusBasedPermission> {

        return this.http.get<StatusBasedPermission>(this.apiGetStatusBasedPermissionUrl + "/" + statusId)
        // .map(response => <StatusBasedPermission>response.json());

    }

    getRecentUser(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentUser> {
        sortColumn = sortColumn ? sortColumn : 'name';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        
        //console.log("filterText: ", filterText);

        return this.http
            .get<RecentUser>(this.recentUserUrl + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }


    saveRegisterdUser(registrationModel: RegistrationModel): Observable<TransactionResponse> {
        //console.log("saving....", registrationModel);
        //console.log("saving....", this.registerdUserApiUrl);

        return this.http.post<TransactionResponse>(this.registerdUserApiUrl, registrationModel);
        // .map(response => <UserModel>response.json())

    }

    createCompanyAndUser(registrationModel: RegistrationModel): Observable<TransactionResponse> {
        //console.log("saving....", registrationModel);


        return this.http.post<TransactionResponse>(this.registerdUserforCompanyApiUrl, registrationModel)
        // .map(response => <UserModel>response.json())

    }

    getCountries(): Observable<Country[]> {
        ////console.log('In getCountries');
        return this.http
            .get<Country[]>(this.countryApiUrl)
        // .map(response => <Country[]>response.json());
    }

    getStatesForCountry(countryId: number): Observable<State[]> {

        return this.http
            .get<State[]>(this.stateApiUrl + "/" + countryId)
        // .map(response => <State[]>response.json());
    }

    getCitiesForState(stateId: number): Observable<City[]> {

        return this.http
            .get<City[]>(this.cityApiUrl + "/" + stateId)
        // .map(response => <City[]>response.json());
    }

    getAreasForCities(cityId: number): Observable<Area[]> {

        return this.http
            .get<Area[]>(this.areaApiUrl + "/" + cityId)
        // .map(response => <Area[]>response.json());
    }

    checkEmailAvailability(email): Observable<TransactionResponse> {
        //console.log("email in ser", email)
        return this.http.get<TransactionResponse>(this.userCheckEmailApiUrl + "/" + email)
        // .map(response => <TransactionResponse>response.json());

    }

    checkEmpEmailAvailability(email): Observable<TransactionResponse> {
        //console.log("email in ser", email)
        return this.http.get<TransactionResponse>(this.userCheckEmpEmailApiUrl + "/" + email)
        // .map(response => <TransactionResponse>response.json());

    }

    checkPhoneNumberAvailability(primaryMobile): Observable<TransactionResponse> {
        //console.log("primaryMobile in ser", primaryMobile)
        return this.http.get<TransactionResponse>(this.userCheckPhoneNumberApiUrl + "/" + primaryMobile)
        // .map(response => <TransactionResponse>response.json());

    }

    checkValidGST(gstNumber): Observable<TransactionResponse> {
        //console.log("gstNumber in ser", gstNumber)
        return this.http.get<TransactionResponse>(this.userCheckGstNumberApiUrl + "/" + gstNumber)
        // .map(response => <TransactionResponse>response.json());

    }
    saveUserTheme(userId: number, theme: string): Observable<TransactionResponse> {
        //console.log("saving theme....", theme);
        //console.log("saving userid....", userId);
        return this.http.get<TransactionResponse>(this.userThemeApiUrl + "/" + userId + "/" + theme);
        // .map(response => <UserModel>response.json())

    }

    saveUserLanguage(userId: number, language: string): Observable<TransactionResponse> {
      
        return this.http.get<TransactionResponse>(this.userLanguageApiUrl + "/" + userId + "/" + language);
       

    }


    getActivityForRoute(route: ActivatedRoute): Observable<Activity> {

        // var currentUser: UserModel = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;


        let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));
        let returnAct: Observable<Activity> = null;

        returnAct = new Observable(observer => {
            route.url.subscribe(urlSegment => {
                //console.log('here:', urlSegment);

                storedMenu
                    .filter(m => m.activities != null)
                    .forEach(m => {
                        m.activities
                            .forEach(act => {
                                if(urlSegment.length < 3){
                                    if ((act.pageUrl ? act.pageUrl.includes(urlSegment[0].path) : false) && (act.urlParams ? (act.urlParams === urlSegment[1].path) : true)) {
                                        //console.log(act.pageUrl, act.urlParams);
                                        observer.next(act);
                                    }
                                }else{
                                    if ((act.pageUrl ? act.pageUrl.includes(urlSegment[0].path) : false) && (act.urlParams ? (act.urlParams === urlSegment[1].path + "," + urlSegment[2].path + "," + urlSegment[3].path ) : true)) {
                                        //console.log(act.pageUrl, act.urlParams);
                                        observer.next(act);
                                    }
                                }
                            })
                    })
            })
        });


        return returnAct;

    }


    disableLogin(user: UserModel): Observable<boolean> {
        return this.http.post<boolean>(this.disableLoginUrl, user)       
    }

  
    getAllHelpVideos(activityId:String) : Observable<HelpVideo[]>{

        return this.http
                .get<HelpVideo[]>(this.helpVideoApiUrl+"/"+activityId)
               
    }
  
}