import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { Tax } from '../data-model/tax-model';

import { SettingsWrapper, GlobalSetting, StockUpdateOn } from '../data-model/settings-wrapper';
import { PrintCopiesWrapper, PrintCopy } from '../data-model/print-copies-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { Email } from '../data-model/email-model';

@Injectable()
export class GlobalSettingService {

    wrapperSettingApiUrl: string;
    globalSettingApiUrl: string;
    stockIncreaseOnApiUrl: string;
    stockDecreaseOnApiUrl: string;
    printCopyApiUrl: string;
    pcApiUrl: string;
    printCopyDeleteUrl: string;
    AllSettingApiUrl: string;
    emailApiUrl:string;
    emailSaveUrl:string;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    returnGlobalSetting: Observable<GlobalSetting> = null;

    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.wrapperSettingApiUrl = environment.baseServiceUrl + environment.wrapperSettingApiUrl;
        this.globalSettingApiUrl = environment.baseServiceUrl + environment.globalSettingApiUrl;
        this.stockIncreaseOnApiUrl = environment.baseServiceUrl + environment.stockIncreaseOnApiUrl;
        this.stockDecreaseOnApiUrl = environment.baseServiceUrl + environment.stockDecreaseOnApiUrl;
        this.printCopyApiUrl = environment.baseServiceUrl + environment.printCopyApiUrl;
        this.pcApiUrl = environment.baseServiceUrl + environment.pcApiUrl;
        this.printCopyDeleteUrl = environment.baseServiceUrl + environment.printCopyDeleteUrl
        this.AllSettingApiUrl = environment.baseServiceUrl + environment.AllSettingApiUrl;
        this.emailApiUrl =environment.baseServiceUrl + environment.emailApiUrl;
        this.emailSaveUrl =environment.baseServiceUrl + environment.emailSaveApiUrl;
    
    }


    getDefaultTax(): Observable<Tax> {

        let t: Tax = {
            id: 1,
            name: 'GST-18',
            rate: 18,
            deleted: 'N',
            taxTypeId: 1
        }

        let retObs: Observable<Tax>;


        retObs = new Observable((observer) => {
            observer.next(t);
            observer.complete();
        })
        return retObs;
    }
    getStockIncreaseOn(): Observable<StockUpdateOn[]> {
        return this.http
            .get<StockUpdateOn[]>(this.stockIncreaseOnApiUrl)
        // .map(response => response ? <StockUpdateOn[]>response.json() : null);

    }

    getStockDecreaseOn(): Observable<StockUpdateOn[]> {
        return this.http
            .get<StockUpdateOn[]>(this.stockDecreaseOnApiUrl)
        // .map(response => response ? <StockUpdateOn[]>response.json() : null);

    }

    getGlobalSetting(): Observable<GlobalSetting> {
        //Get it from local storage
        this.returnGlobalSetting = this.getLocalGS();

        if (!this.returnGlobalSetting) {
            this.returnGlobalSetting = this.http
                .get<GlobalSetting>(this.globalSettingApiUrl)
                .pipe(tap(response => this.storeLocalGS(response)))
                // .map(response => response ? <GlobalSetting>response.json() : null)
                ;
        }

        return this.returnGlobalSetting;
    }

    storeLocalGS(gs: GlobalSetting) {
        try {
            localStorage.setItem('globalSetting', JSON.stringify(gs));
        } catch (e) {
            //console.log("Local Storage is full, emptying globalSetting if any");
            this.removeLocalGS()
        }
    }

    removeLocalGS() {
        localStorage.removeItem('globalSetting');
    }

    getLocalGS(): Observable<GlobalSetting> {

        let storedGS: GlobalSetting = JSON.parse(localStorage.getItem('globalSetting'));
        //console.log('got local GS: ', storedGS);
        let returnGS: Observable<GlobalSetting>;

        if (storedGS) {

            returnGS = new Observable((observer) => {
                observer.next(storedGS);
                observer.complete();
            })
        }

        return returnGS;

    }

    saveWrapperSetting(settingObject: SettingsWrapper): Observable<SettingsWrapper> {

        return this.http
            .post<SettingsWrapper>(this.wrapperSettingApiUrl, settingObject)
        // .map(response => response ? <SettingsWrapper>response.json() : null);

    }

    save(printObject: PrintCopiesWrapper): Observable<PrintCopiesWrapper> {
        return this.http
            .post<PrintCopiesWrapper>(this.printCopyApiUrl, printObject)
    }

    getAllPrintCopies() : Observable<PrintCopy[]>{
        return this.http.get<PrintCopy[]>(this.pcApiUrl)
    }

    delete(id: number) : Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.printCopyDeleteUrl+"/"+id)
    }



    getAllSettings(): Observable<SettingsWrapper>{
       this.returnGlobalSetting = this.getLocalGS();
        return this.http
        .get<SettingsWrapper>(this.AllSettingApiUrl)
        .pipe(tap(response => this.storeLocalGS(response.globalSetting)))
        // .map(response => <Tax[]>response.json());


}


getEmailSettings() : Observable<Email>{
    return this.http.get<Email>(this.emailApiUrl)
}
saveEmail(email: Email): Observable<Email> {
    return this.http
        .post<Email>(this.emailSaveUrl, email)
}

}