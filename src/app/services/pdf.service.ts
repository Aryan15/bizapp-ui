import { Injectable } from "@angular/core";
import { InvoiceReport, InvoiceReportModel } from "../data-model/invoice-model";
import { DatePipe } from "@angular/common";
import { PurchaseOrderReport } from "../data-model/purchase-order-model";
import { CompanyService } from "./company.service";
import { Company } from "../data-model/company-model";

declare var jsPDF: any;

@Injectable()
export class PdfService {


    datePipe = new DatePipe('en-US');

    doc = new jsPDF('l', 'pt');
    totalPagesExp = "{total_pages-count-string}";
    transactionDateHeader: string = "Date"
    leftMargin: number = 15;
    rightMargin: number = 20;
    custTableLastY: number;



    constructor(private companyService: CompanyService) {

    }

    public exportToPdf(title: String, reportHeader: any[] = [], reportItem: any[] = [], name: string) {
        this.doc = new jsPDF('l', 'pt');
        let company: Company = this.companyService.getCompany();
        this.writeTitle(title);
        this.writeCompanyDetails(company.name);

        this.writeCurrentDate();


        this.writeReport(reportHeader, reportItem);

        this.savePdf(name);

    }

    writeTitle(titleText: String) {
        var rectangleStartX: number = 400;
        var rectangleStartY: number = 10;
        var rectangleWidth: number = 180;
        var rectangleHeight: number = 20;
        this.doc.setDrawColor(150);
        this.doc.setFontSize(12);
        this.doc.setTextColor(150);


        this.doc.setTextColor(40);
        this.doc.setFontSize(16);
        var xOffset: number = (this.doc.internal.pageSize.width / 2) - (this.doc.getStringUnitWidth(titleText) * this.doc.internal.getFontSize() / 2);
        var titleTextYOffset: number = 18;

        this.doc.text(titleText, xOffset, rectangleStartY + this.doc.internal.getFontSize() / 2 + titleTextYOffset);
    }

    writeCompanyDetails(company: String) {

        var startCompanyY: number = 25;

        this.doc.autoTable(this.getCompanyHeader(), this.getCompany(company), {


            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'center',
                overflow: 'linebreak',
                overflowColumns: false,
                fontStyle: 'bold',
                fontSize: 15,
                textColor: [63, 81, 181],
                cellPadding: 0,
            }

        });




        // this.doc.autoTable(this.getAddressHeader(), this.getAddress(address), {

        //     startY: this.doc.autoTable.previous.finalY,

        //     theme: 'plain',
        //     showHead: 'never',
        //     styles: {
        //         halign: 'left',
        //         overflow: 'linebreak',
        //         overflowColumns: false,
        //         fontStyle: 'normal',
        //         fontSize: 10,
        //         cellPadding: 0,
        //     },
        //     columnStyles: {
        //         columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'left' },
        //         columnContent: { cellWidth: 300, halign: 'left' }
        //     }
        // });



    }


    getAddressHeader(): any[] {

        var headerColumns = [


            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

        return headerColumns;

    }







    getAddress(address: String
    ): any[] {

        let data: any[] = [

            {
                "columnHead": "Address",
                "columnContent": address,
            },

        ];


        return data.filter(row => row.columnContent);


    }

    getCompanyHeader(): any[] {

        var headerColumns = [
            { title: "Company Name", dataKey: "companyName" },
        ];

        return headerColumns;

    }

    getCompany(companyName: String): any[] {

        return [
            {
                "companyName": companyName || "", //"Uthkrushta Technologies",
            }
        ];
    }


    writeCurrentDate() {

        let startY: number = 35;
        let leftMargin: number = this.doc.internal.pageSize.width - 240;
        this.doc.autoTable(this.getDateHeader(), this.getcurrentDate(), {
            startY: startY,
            margin: { left: leftMargin, right: this.rightMargin },
            theme: 'plain',
            showHead: 'never',
            styles: {
                halign: 'right',
                fontSize: 10,
                overflow: 'linebreak',
                overflowColumns: false,
                cellPadding: 1,
            },
            columnStyles: {
                columnHead: { fontStyle: 'bold', cellWidth: 100 },
                columnContent: { halign: 'left', },
            }
        });


    }


    getcurrentDate() {

        return [
            {
                "columnHead": this.transactionDateHeader,
                "columnContent": this.datePipe.transform(new Date, 'dd/MM/yyyy'),
            },

        ];

    }
    getDateHeader() {
        var headerColumns = [
            { title: "Column Head", dataKey: "columnHead" },
            { title: "Column Content", dataKey: "columnContent" },
        ];

        return headerColumns;
    }




    writeReport(reportHeader: any[] = [], reportItem: any[] = []) {

        reportItem.forEach(item => {
            Object.keys(item).forEach(k => {
                ////console.log("item, k", item, k)
                item[k] = item[k] === null ? '' : item[k]
                if (typeof item[k] === "number" && k !== "slNo" && k !== "SL No" && k !== "discountPercentage" && k !== "cgstPerc" && k !== "sgstPerc" && k !== "igstPerc" && k !== "tdsPercentage") {
                    item[k] = item[k] === null ? '' : item[k].toFixed(2)
                }
            }
            );


        })

        this.doc.autoTable({
            head: reportHeader,
            body: reportItem,
            startY: 70,
            theme: 'grid',
            headStyles: {
                fillColor: [33, 33, 33],
            },
            columnStyles: {
                text: { cellWidth: 'auto' }, Quantity: { halign: 'right' }, Price: { halign: 'right' },
                Amount: { halign: 'right' },
                Discount: { halign: 'right' },
                AmountAfterDiscount: { halign: 'right' },
                CgstTaxAmount: { halign: 'right' },
                SgstTaxAmount: { halign: 'right' },
                IgstTaxAmount: { halign: 'right' },
                GrandTotal: { halign: 'right' },
                TotalAmount: { halign: 'right' },
                CgstTaxPercentage: { halign: 'right' },
                SgstTaxPercentage: { halign: 'right' },
                IgstTaxPercentage: { halign: 'right' },
                BalanceAmount: { halign: 'right' },
                BuyingPrice: { halign: 'right' },
                SellingPrice: { halign: 'right' },
                CurrentStock: { halign: 'right' },
                MRP: { halign: 'right' },
                InQuantity: { halign: 'right' },
                OutQuantity: { halign: 'right' },
                ClosingStock: { halign: 'right' },
                TotalTaxableAmount: { halign: 'right' },
                DiscountAmount: { halign: 'right' },
                InvoiceAmount: { halign: 'right' },
                TdsPercentage: { halign: 'right' },
                AmountAfterTds: { halign: 'right' },
                PaidAmount: { halign: 'right' },
                PayingAmount: { halign: 'right' },
                DueAmount: { halign: 'right' },
                TaxableAmount: { halign: 'right' },
                NetAmount: { halign: 'right' },
                "Cgst0.125": { halign: 'right' },
                "Sgst0.125": { halign: 'right' },
                "Igst0.25": { halign: 'right' },
                "Cgst2.5": { halign: 'right' },
                "Sgst2.5": { halign: 'right' },
                Igst5: { halign: 'right' },
                Cgst6: { halign: 'right' },
                Sgst6: { halign: 'right' },
                Igst12: { halign: 'right' },
                Cgst9: { halign: 'right' },
                Sgst9: { halign: 'right' },
                Igst18: { halign: 'right' },
                Cgst14: { halign: 'right' },
                Sgst14: { halign: 'right' },
                Igst28: { halign: 'right' }

            },

            styles: { fontSize: 7 },


        })





    }

    savePdf(name: string) {
        let date = this.datePipe.transform(new Date, 'dd/MM/yyyy')

        this.doc.save(name + '_export_' + date + '.pdf');
    }















}
