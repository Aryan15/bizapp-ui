import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TextToSpeechService {

  textInput: string = "Sample text";
  
  voices = speechSynthesis.getVoices();
  


  constructor() { 
   
    this.voices = speechSynthesis.getVoices();
  }

  speak(text: string){
    //console.log("voices: ", this.voices);
    this.textInput = text;
    //console.log(this.textInput);
    let message = new SpeechSynthesisUtterance(this.textInput);

    console.log("voices: ", this.voices);
    console.log("voices: ", speechSynthesis.getVoices());
    //voiceURI: "Google UK English Female"
    message.voice = speechSynthesis.getVoices().find(v => v.voiceURI === "Google UK English Female");
    // message.voice = speechSynthesis.getVoices()[4];
    //console.log('message: ', message);
    speechSynthesis.speak(message);
    //message = null;
  }

  pause(){
    speechSynthesis.pause();
  }

  resume(){
    speechSynthesis.resume();
  }
}
