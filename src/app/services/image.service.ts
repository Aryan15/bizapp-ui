
import { Ng2ImgMaxService } from 'ng2-img-max';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
 
@Injectable({
    providedIn: 'root'
  })
export class ImgMaxPXSizeService {
    constructor(private ng2ImgMaxService: Ng2ImgMaxService) {}

    resize(someFile: any[],
                newWidth: number, 
                newHeight: number): Observable<any>{

            return this.ng2ImgMaxService.resize(someFile, newWidth, newHeight)
    };

}