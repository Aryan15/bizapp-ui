import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Tax, TaxType } from '../data-model/tax-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';

@Injectable()
export class TaxService{


    taxesApiUrl : string;
    taxTypesApiUrl : string;
    private headers = new Headers({'Content-Type': 'application/json'});


    constructor(private http: HttpClient, private tokenService : TokenService){
        this.taxesApiUrl = environment.baseServiceUrl + environment.taxesApiUrl;
        this.taxTypesApiUrl = environment.baseServiceUrl + environment.taxTypesApiUrl;
    }

    getAllTaxes() : Observable<Tax[]>{
                return this.http
                .get<Tax[]>(this.taxesApiUrl)
                // .map(response => <Tax[]>response.json());


    }

    getAllTaxTypes() : Observable<TaxType[]>{

        return this.http
                .get<TaxType[]>(this.taxTypesApiUrl)
                // .map(response => <TaxType[]>response.json());
    }

    delete(id: number) : Observable<TransactionResponse>{

        return this.http
                .delete<TransactionResponse>(this.taxesApiUrl+"/"+id)
                // .map(response => <TransactionResponse>response.json());

    }
}