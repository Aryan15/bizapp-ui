import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';4
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { TransactionType, TransactionStatusChange, TransactionStatus } from '../data-model/transaction-type';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';
import { keepFresh } from 'http-operators';

@Injectable()
export class TransactionTypeService{

    transactionTypeApiUrl : string;
    sourceTransactionStatusChangeApiUrl: string;
    linkTransactionStatusChangeApiUrl: string;
    apiGetTransactionNumUrl: string;
    getAllStatusApiUrl: string;
    checkTransctionExistsApiUrl: string;
    allowStockIncreaseApiUrl: string;
    allowStockDecreaseApiUrl: string;
    checkHigherCountApiUrl: string;
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: HttpClient, private tokenService : TokenService){
        this.transactionTypeApiUrl = environment.baseServiceUrl + environment.transactionTypeApiUrl;
        this.sourceTransactionStatusChangeApiUrl = environment.baseServiceUrl + environment.sourceTransactionStatusChangeApiUrl;
        this.linkTransactionStatusChangeApiUrl = environment.baseServiceUrl + environment.linkTransactionStatusChangeApiUrl;
        this.apiGetTransactionNumUrl = environment.baseServiceUrl + environment.apiGetTransactionNumUrl;
        this.getAllStatusApiUrl = environment.baseServiceUrl + environment.getAllStatusApiUrl;
        this.checkTransctionExistsApiUrl = environment.baseServiceUrl + environment.checkTransctionExistsApiUrl;
        this.allowStockIncreaseApiUrl = environment.baseServiceUrl + environment.allowStockIncreaseApiUrl;
        this.allowStockDecreaseApiUrl = environment.baseServiceUrl + environment.allowStockDecreaseApiUrl;
        this.checkHigherCountApiUrl = environment.baseServiceUrl + environment.checkHigherCountApiUrl;
    }

    getAllTransactionTypes() : Observable<TransactionType[]>{

        return this.http.get<TransactionType[]>(this.transactionTypeApiUrl)
                        // .map(response => <TransactionType[]>response.json());
    }

    getAllTransactionStatuses() : Observable<TransactionStatus[]>{

        return this.http.get<TransactionStatus[]>(this.getAllStatusApiUrl)
                        // .map(response => <TransactionType[]>response.json());
    }

    getTransactionType(id : number) : Observable<TransactionType>{
        console.log(id+"jfhfhfhf")
        let tranTypes : TransactionType[] = JSON.parse(localStorage.getItem('transactionType'));

        if(tranTypes){

            const retObs = Observable.create(observer =>{
                setTimeout(()=>{
                    observer.next(tranTypes.find(id => id == id));
                    observer.complete();
                },0);
            })

            return retObs; //Observable.of(tranTypes.find(id => id == id));            
        }else{
            return this.http.get<TransactionType>(this.transactionTypeApiUrl+"/"+id)
                        // .map(response => <TransactionType>response.json());
        }
    }

    setTransactionType(){

        this.getAllTransactionTypes().subscribe(response => {
            localStorage.setItem('transactionTypes',JSON.stringify(response));
        })
    }

    getSourceTransactionStatuses(id: string,transactionTypeId:number) : Observable<TransactionStatusChange[]>{
        return this.http.get<TransactionStatusChange[]>(this.sourceTransactionStatusChangeApiUrl+"/"+id+"/"+transactionTypeId);
    }

    getLinkTransactionStatuses(id: string) : Observable<TransactionStatusChange[]>{
        return this.http.get<TransactionStatusChange[]>(this.linkTransactionStatusChangeApiUrl+"/"+id);
    }

    getTransactionNumber(transactionTypeName: string): Observable<TransactionResponse>{
        return this.http.get<TransactionResponse>(this.apiGetTransactionNumUrl + "/" + transactionTypeName)
        .pipe(
            keepFresh(1000 * 60 * 10 )
        );
    }

    checkTransactionExistis(transactionTypeIds: number[]): Observable<boolean>{
        return this.http.post<boolean>(this.checkTransctionExistsApiUrl, transactionTypeIds);
    }


    allowStockIncrease(): Observable<boolean>{
        return this.http.get<boolean>(this.allowStockIncreaseApiUrl);
    }

    allowStockDecrease(): Observable<boolean>{
        return this.http.get<boolean>(this.allowStockDecreaseApiUrl);
    }

    checkHigherCount(tranNumbers: string[]): Observable<number>{
        return this.http.post<number>(this.checkHigherCountApiUrl, tranNumbers);
    }
}