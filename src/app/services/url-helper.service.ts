import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable ,  Subscriber } from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import { AppSettings } from '../app.settings';

@Injectable()
export class UrlHelperService {

    sanitizer : DomSanitizer;

    constructor(
        private http: HttpClient, /* Your favorite Http requester */
        //private tokenService : TokenService,
    ) {
    }

    get(url: string): Observable<string> {
        return new Observable((observer: Subscriber<string>) => {
            let objectUrl: string = null;

            this.http
                .get(url, {
                    headers: this.getHeaders(),
                    responseType: 'blob'
                })
                .subscribe(m => {
                    //console.log("m: ", m);
                    objectUrl = URL.createObjectURL(m);//m.blob());
                    observer.next(objectUrl);
                });

            return () => {
                if (objectUrl) {
                    URL.revokeObjectURL(objectUrl);
                    objectUrl = null;
                }
            };



        });
    }

    getHeaders(): HttpHeaders {

        let currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
        let tokenl = currentUser && currentUser.token;

        let token : string = tokenl?'Bearer '+tokenl:null;

        let headers = new HttpHeaders();
 
        ////let token = this.authService.getCurrentToken();
        //let token = { access_token: 'ABCDEF' }; // Get this from your auth service.
        if (token) {
            headers.set('Authorization', token);
        }
 
        return headers;
    }


}