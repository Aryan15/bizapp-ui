
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PayableReceivableHeader, PayableReceivableItem, PayableReceivableReport, PayableReceivableReportModel, BalanceReportModel, PayableReceivableReportRequest, RecentPayableReceivable, PayableReceivableWrapper } from '../data-model/pr-model';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';
import { PagedData } from "../reports/model/paged-data";
import { Page } from "../reports/model/page";
import { RequestOptions, Headers, ResponseContentType } from '@angular/http';

@Injectable()
export class PayableReceivableService {

    apiGetAllUrl: string;
    apiPostUrl: string;
    apiGetUrl: string;
    apiDeleteUrl: string;
    apiGetPayableReceivablesByPayableReceivablesNumber: string;
    payableReceivableReportApiUrl: string;
    payableReceivableDownloadReportApiUrl: string;
    payableReceivableReportForBalanceReportApiUrl: string;
    recentPayableReceivableUrl: string;
    apiDeletePRItemUrl: string;
    checkDraftForPartyUrl: string;
    checkCancelledInvoicesApiUrl: string;
    apiGetPayUrl:string;
    constructor(private http: HttpClient) {

        this.apiGetAllUrl = environment.baseServiceUrl + environment.payableReceivableApiGetAllUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.payableReceivableApiPostUrl;
        this.apiGetUrl = environment.baseServiceUrl + environment.payableReceivableApiGetUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.payableReceivableApiGetUrl;
        this.apiGetPayableReceivablesByPayableReceivablesNumber = environment.baseServiceUrl + environment.payableReceivablesByPayableReceivablesNumberGetUrl;
        this.payableReceivableReportApiUrl = environment.baseServiceUrl + environment.payableReceivableReportApiUrl;
        this.payableReceivableDownloadReportApiUrl = environment.baseServiceUrl + environment.payableReceivableDownloadReportApiUrl;
        this.payableReceivableReportForBalanceReportApiUrl = environment.baseServiceUrl + environment.payableReceivableReportForBalanceReportApiUrl;
        this.recentPayableReceivableUrl = environment.baseServiceUrl + environment.recentPayableReceivableUrl;
        this.apiDeletePRItemUrl = environment.baseServiceUrl + environment.apiDeletePRItemUrl;
        this.checkDraftForPartyUrl = environment.baseServiceUrl + environment.checkDraftForPartyUrl;
        this.checkCancelledInvoicesApiUrl = environment.baseServiceUrl + environment.checkCancelledInvoicesApiUrl;
        this.apiGetPayUrl = environment.baseServiceUrl + environment.getPayApiUrl;
    }

    create(payableReceivableWrapper: PayableReceivableWrapper): Observable<PayableReceivableHeader> {
        ////console.log("In create...", model.id);
        return this.http.post<PayableReceivableHeader>(this.apiPostUrl, payableReceivableWrapper) // .map(response => <PayableReceivableHeader> response.json());

    }
    getAllPayableReceivables(): Observable<PayableReceivableHeader[]> {
        return this.http.get<PayableReceivableHeader[]>(this.apiGetAllUrl)
        // .map(response => response.json());
    }

    getPayableReceivablesByPayableReceivablesNumber(prNumber: string): Observable<PayableReceivableHeader[]> {
        return this.http.get<PayableReceivableHeader[]>(this.apiGetPayableReceivablesByPayableReceivablesNumber + "/" + prNumber)
        // .map(response => <PayableReceivableHeader[]>response.json());
    }

    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }

    getPayableReceivableItemReport(transactionTypeId: number, invoiceTypeId: number, payableReceivableReportRequest: PayableReceivableReportRequest, page: Page): Observable<PagedData<PayableReceivableReportModel>> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        return this.http.post<PayableReceivableReport>(this.payableReceivableReportApiUrl
            + "/" + transactionTypeId
            + "/" + invoiceTypeId
            + "/" + pageNumber
            + "/" + pageSize +
            "/" + pageSortDirection + "/"
            + pageSortColumn, payableReceivableReportRequest).pipe(
            map(payableReceivableData => this.getPagedData(page, payableReceivableData)));
    }


    downloadPayableReceivableItemReport(transactionTypeId: number, payableReceivableReportRequest: PayableReceivableReportRequest, page: Page): Observable<any> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = {
        //     headers: headers,
        //     responseType: 'arraybuffer'
        // };

        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
              'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer'
          };
          
        return this.http.post<any>(this.payableReceivableDownloadReportApiUrl
            + "/" + transactionTypeId
            + "/" + pageNumber
            + "/" + pageSize +
            "/" + pageSortDirection + "/"
            + pageSortColumn, payableReceivableReportRequest, httpOption)
            .pipe(
                //tap(res => //console.log("res: ", res)),
            map(res => new Blob([res['body']],{ type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }))
            );
    }

    getBalanceReport(invoiceTypeId: number, payableReceivableReportRequest: PayableReceivableReportRequest, page: Page): Observable<PagedData<BalanceReportModel>> {
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        return this.http.post<PayableReceivableReport>(this.payableReceivableReportForBalanceReportApiUrl +
            "/" + invoiceTypeId +
            "/" + pageNumber +
            "/" + pageSize +
            "/" + pageSortDirection + "/" + pageSortColumn, payableReceivableReportRequest).pipe(
            map(payableReceivableData => this.getPagedDatas(page, payableReceivableData)));
    }
    

    getBalanceReport1(invoiceTypeId: number, payableReceivableReportRequest: PayableReceivableReportRequest, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<PayableReceivableReport> {
        sortColumn = sortColumn ? sortColumn : 'invoiceNumber';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
   console.log("sortColumn  "+sortColumn+" sortDirection "+sortDirection +" page "+ page+" pageSize "+pageSize)
   console.log(this.payableReceivableReportForBalanceReportApiUrl + "/" + invoiceTypeId  + "/" + page + "/" + pageSize + "/" + sortDirection + "/" + sortColumn)

        return this.http
            .post<PayableReceivableReport>(this.payableReceivableReportForBalanceReportApiUrl + "/" + invoiceTypeId  + "/" + page + "/" + pageSize + "/" + sortDirection + "/" + sortColumn, payableReceivableReportRequest);
    }

    private getPagedDatas(page: Page, payableReceivableData: PayableReceivableReport): PagedData<BalanceReportModel> {

        //console.log("payableReceivableData: ", payableReceivableData);
        let pagedData = new PagedData<BalanceReportModel>();
        if (page) {
            page.totalElements = payableReceivableData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = payableReceivableData.balanceReportModels;
        //console.log('payableReceivableData.balanceReportModels: ', payableReceivableData.balanceReportModels);
        //console.log("page ", page);
        return pagedData;
    }

    private getPagedData(page: Page, payableReceivableData: PayableReceivableReport): PagedData<PayableReceivableReportModel> {

        //console.log("payableReceivableData: ", payableReceivableData);
        let pagedData = new PagedData<PayableReceivableReportModel>();
        if (page) {
            page.totalElements = payableReceivableData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = payableReceivableData.payableReceivableReportItems;
        //console.log('payableReceivableData.payableReceivableReportItems: ', payableReceivableData.payableReceivableReportItems);
        //console.log("page ", page);
        return pagedData;
    }

    getRecentPayableReceivables(transactionType: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPayableReceivable> {
        sortColumn = sortColumn ? sortColumn : "payReferenceDate";
        sortColumn = (sortColumn === "payReferenceNumber") ? "payReferenceId" : sortColumn;
        
        sortDirection = sortDirection ? sortDirection : "desc";
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : "null";

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        
        ////console.log("filterText: ", filterText);

        let url: string = this.recentPayableReceivableUrl + "/" + transactionType + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize;

        ////console.log('url: ', url);
        //return null;
        return this.http
            .get<RecentPayableReceivable>(url, {params: params});
    }

    deleteItem(id: string): Observable<TransactionResponse> {

        return this.http.delete<TransactionResponse>(this.apiDeletePRItemUrl + "/" + id);
    }

    checkDraftForParty(partyId: number, transactionTypeId: number):Observable<TransactionResponse>{
        return this.http.get<TransactionResponse>(this.checkDraftForPartyUrl+"/"+partyId+"/"+transactionTypeId);
    }

    checkCancelledInvoices(invoiceIds: string[]): Observable<string[]>{
        return this.http.post<string[]>(this.checkCancelledInvoicesApiUrl, invoiceIds);
    }

    getPayableRecivbleById(id: string): Observable<PayableReceivableHeader> {
        return this.http.get<PayableReceivableHeader>(this.apiGetPayUrl + "/" + id)
        // .map(response => <PurchaseOrderHeader[]>response.json());
    }

}