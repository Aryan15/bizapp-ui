import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import annyang from 'annyang';

@Injectable({
  providedIn: 'root'
})
export class SpeechService {

  commands$ : Subject<string> = new Subject<string>();
  errors$ : Subject<Error> = new Subject<Error>();
  listening: boolean = false;

  constructor(private zone: NgZone) { }

  get speechSupported(): boolean{

    ////console.log('is speech supported? ', annyang);

    return !!annyang;
  }


  init(){

    const commands = {      
      'my boss :command report': (command) => {
        this.zone.run( () => {
          this.commands$.next(command);
        })
      },
    };

    annyang.setLanguage('en-IN');

    annyang.addCommands(commands);

    annyang.addCallback('errorNetwork', (err) => {
      this._handleError('network', 'A network error occurred.', err);
    });
    annyang.addCallback('errorPermissionBlocked', (err) => {
      this._handleError('blocked', 'Browser blocked microphone permissions.', err);
    });
    annyang.addCallback('errorPermissionDenied', (err) => {
      this._handleError('denied', 'User denied microphone permissions.', err);
    });
    annyang.addCallback('resultNoMatch', (userSaid) => {
      this._handleError(
        'no match',
        'Spoken command not recognized. Say "noun [word]", "verb [word]", OR "adjective [word]".',
        { results: userSaid });
    });
  

  }

  private _handleError(error, msg, errObj) {
    this.zone.run(() => {
      this.errors$.next({
        error: error,
        message: msg,
        obj: errObj
      });
    });
  }


  startListening() {
    //console.log("Listening....")
    annyang.start();
    this.listening = true;
  }

  abort() {
    annyang.abort();
    this.listening = false;
    //console.log("Stopped Listening.")
  }

}

export interface Error{
  error: string,
  message: string,
  obj: any
}
