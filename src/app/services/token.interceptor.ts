import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { TokenService } from './token.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AlertService } from 'ngx-alerts';
import { tap } from 'rxjs/operators';
import { ConnectionService } from './connection.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(public tokenService: TokenService,
        private alertService: AlertService,
        private connectionService: ConnectionService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // //console.log('in interceptor ', request);

        if (request.url.search(environment.loginUrl) === -1)
        {
            ////console.log('Adding header: '+request.url);
            request = request.clone({
                setHeaders: {
                    Authorization: this.tokenService.getToken()
                }
            });
        }
        else {
            ////console.log('No header needed');
            request = request.clone();
        }
        ////console.log("after adding header: ", request);
        return next.handle(request)
        .pipe(
        tap((event: HttpEvent<any>) => {
            this.connectionService.connectionInfo.next(null);
        },(err: any)=>{
            if(err instanceof HttpErrorResponse){
                //console.log('error: ', err);
                
                var re = /DataIntegrityViolationException/gi;
                if(err.error && err.error.exception && err.error.exception.search(re) !== -1)
                {
                    this.alertService.danger("Cannot Delete. This Transaction has been referenced in other transactions");
                }
                if(err.error instanceof ErrorEvent){
                    //console.log("Client side error "+err.error.message)
                }else{
                    //console.log("Server side error "+err.status)
                    if(err.status === HttpError.NotFound || err.status === HttpError.connectionRefused){
                        this.connectionService.connectionInfo.next("Server not available");
                    }else if(err.status === HttpError.Unauthorized){
                        this.connectionService.connectionInfo.next("Session Expired. Please logout and re-login");
                    }
                    else{
                        this.connectionService.connectionInfo.next(null);
                    }
                    
                }
                // else
                //     this.alertService.danger(err.error.exception);
            }else{
                //console.log("reset connection error", err);
                this.connectionService.connectionInfo.next(null);
            }

        }));
    }
}

export class HttpError{
    static BadRequest = 400;
    static Unauthorized = 401;
    static Forbidden = 403;
    static NotFound = 404;
    static TimeOut = 408;
    static Conflict = 409;
    static InternalServerError = 500;
    static connectionRefused = 0;
}