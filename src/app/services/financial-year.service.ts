import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { FinancialYear, FinancialYearWrapper } from '../data-model/financial-year-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';

@Injectable()
export class FinancialYearService {


    financialYearApiUrl: string;
    financialYearsApiUrl: string; 
     apiPostUrl : string;
    private headers = new Headers({ 'Content-Type': 'application/json' });


    constructor(private http: HttpClient, private tokenService: TokenService) {
        this.financialYearApiUrl = environment.baseServiceUrl + environment.financialYearApiUrl;
        this.financialYearsApiUrl = environment.baseServiceUrl + environment.financialYearsAllApiUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.financialYearApiPostUrl;
    }

    getFinancialYear(): Observable<FinancialYear> {
        return this.http
            .get<FinancialYear>(this.financialYearApiUrl)
            // .map(response => response ? <FinancialYear>response.json() : null);


    }

    getFinancialYears(): Observable<FinancialYear[]> {
        return this.http
            .get<FinancialYear[]>(this.financialYearsApiUrl)
            // .map(response => response ? <FinancialYear[]>response.json() : null);

    }
    getAllFinancialYears(): Observable<FinancialYearWrapper[]> {
        return this.http
            .get<FinancialYearWrapper[]>(this.financialYearsApiUrl)
            // .map(response => response ? <FinancialYear[]>response.json() : null);

    }
    save(financialYear: FinancialYearWrapper): Observable<FinancialYearWrapper> {
        //console.log("saving.... gdfddfh", financialYear);

        return this.http.post<FinancialYearWrapper>(this.apiPostUrl, financialYear)
       
    }

    delete(id: number) : Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.apiPostUrl+"/"+id)
                    // .map(response => <TransactionResponse>response.json() );
    }

}