import { Injectable } from "@angular/core";
import { CompanyService } from "./company.service";
import { Company } from "../data-model/company-model";
import { QuotationSecondReportService } from "./template2/quotation-report.service";
import { NumberRangeConfigService } from "./number-range-config.service";
import { TransactionType } from "../data-model/transaction-type";
import { AppSettings } from "../app.settings";
import { QuotationReportService } from "./template1/quotation-report.service";
import { InvoiceReportService } from "./template1/invoice-report.service";
import { InvoiceReportSecondService } from "./template2/invoice-report.service";
import { GlobalSetting } from "../data-model/settings-wrapper";
import { PurchaseOrderReportService } from "./template1/purchase-order-report.service";
import { PurchaseOrderSecondReportService } from "./template2/purchase-order-report.service";
import { VoucherSecondReportService } from "./template2/voucher-report.service";
import { GrnReportService } from "./template2/grn-report.service";
//import { QuotationSecondReportImageService } from "./template2/quotation-report-image.service";
import { DeliveryChallanSecondReportService } from "./template2/delivery-challan-report.service";
import { CustomerReceiptstService } from "./template2/customer-receipts.service";
import { InvoiceReportCLService } from "./template-certi-logo/invoice-report-cl";
import { QuotationReportCLService } from "./template-certi-logo/quotation-report-cl";
import { PurchaseOrderCLReportService } from "./template-certi-logo/po-reprt-cl";
import { DeliveryChallanCLReportService } from "./template-certi-logo/dc-report-cl";
import { GrnReportCLService } from "./template-certi-logo/grn-report-cl";
import { QuotationReportLetterHeadService } from "./letter-head/quotation-report-letter-head";
import { PurchaseOrderReportLetterHeadService } from "./letter-head/po-report-letter-head";
import { GrnReportLetterHeadService } from "./letter-head/grn-report-letter-head";
import { InvoiceReportLetterHeadService } from "./letter-head/invoice-report-letter-head";
import { DeliveryChallanReportLetterHeadService } from "./letter-head/dc-report-letter-head";
import { CustomerReceiptstAndSupplierPaymentReportLetterHeadService } from "./letter-head/customer-reciept-supplier-payment-leter-head";
import { CashVoucherAndChequeVoucherLetterHeadService } from "./letter-head/voucher-letter-head";
import { NumberRangeConfiguration } from "../data-model/number-range-config-model";
import { CustomerReceiptstAndSupplierPaymentCLReportService } from "./template-certi-logo/cr-sp-cl";
import { CashVoucherAndCheckVoucherReportCLService } from "./template-certi-logo/voucher-report";
import { PrintCopy } from "../data-model/print-copies-model";
declare var jsPDF: any;
@Injectable()
export class PrintWrapperService {


    constructor(private companyService: CompanyService,
        private quotationSecondReportService: QuotationSecondReportService,
        private quotationReportCLService: QuotationReportCLService,
        private quotationReportService: QuotationReportService,
        private invoiceReportService: InvoiceReportService,
        private invoiceReportSecondService: InvoiceReportSecondService,
        private invoiceReportCLService: InvoiceReportCLService,
        private purchaseOrderReportService: PurchaseOrderReportService,
        private purchaseOrderSecondReportService: PurchaseOrderSecondReportService,
        private purchaseOrderCLReportService: PurchaseOrderCLReportService,
        private numberRangeConfigService: NumberRangeConfigService,
        private voucherSecondReportService: VoucherSecondReportService,
        private grnReportService: GrnReportService,
        private grnReportCLService: GrnReportCLService,
        //private quotationSecondReportImageService: QuotationSecondReportImageService,
        private deliveryChallanSecondReportService: DeliveryChallanSecondReportService,
        private deliveryChallanCLReportService: DeliveryChallanCLReportService,
        private customerReceiptstService: CustomerReceiptstService,
        private quotationReportLetterHeadService: QuotationReportLetterHeadService,
        private purchaseOrderReportLetterHeadService: PurchaseOrderReportLetterHeadService,
        private GrnReportLetterHeadService: GrnReportLetterHeadService,
        private invoiceReportLetterHeadService: InvoiceReportLetterHeadService,
        private deliveryChallanReportLetterHeadService: DeliveryChallanReportLetterHeadService,
        private customerReceiptstAndSupplierPaymentReportLetterHeadService: CustomerReceiptstAndSupplierPaymentReportLetterHeadService,
        private cashVoucherAndChequeVoucherLetterHeadService: CashVoucherAndChequeVoucherLetterHeadService,
        private customerReceiptstAndSupplierPaymentCLReportService: CustomerReceiptstAndSupplierPaymentCLReportService,
        private cashVoucherAndCheckVoucherReportCLService: CashVoucherAndCheckVoucherReportCLService,) {

    }


    print(transactionType: TransactionType
        , dataModel: any
        , printCopy: string
        , globalSetting: GlobalSetting
        , numberRangeConfiguration: NumberRangeConfiguration
        , billAddress?: string
        , topMargin: number = 150
        , printCopys: PrintCopy[] = []        
        , isItemLevelTax: boolean = false
        , printHeaderText?: string
        ,signatureValue?:number
        ,shipAddressValue?:number
    ) {


        //console.log('printCopys : ', printCopys);
        //console.log('transactionType: ', transactionType);
        // console.log('dataModel: ', dataModel);

        let company: Company = this.companyService.getCompany();
        //console.log("company :",company);
        let allowPartyCode:number = globalSetting ?  globalSetting.allowPartyCode : 0 ;
      
      
        //console.log("print response.................", numberRangeConfiguration);
        //console.log("transactionType.name.................", transactionType.name);
        //console.log("AppSettings.CUSTOMER_QUOTATION.................", AppSettings.CUSTOMER_QUOTATION);
        //console.log("response.printTemplateId.................", numberRangeConfiguration.printTemplateId);
        switch (transactionType.name) {

            case AppSettings.CUSTOMER_QUOTATION:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.quotationReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.quotationSecondReportService.download(transactionType.name,dataModel, company, printCopy, printCopys,printHeaderText,signatureValue);
                        break;
                    case 3:
                        this.quotationReportCLService.download(dataModel, company, printCopy, printCopys,printHeaderText,signatureValue);
                        break;

                    case 4:
                        this.quotationReportLetterHeadService.download(dataModel, company, printCopy, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.quotationSecondReportService.download(transactionType.name,dataModel, company, printCopy, printCopys,printHeaderText,signatureValue);
                        // this.quotationReportCLService.download(dataModel, company, printCopy);
                        // this.quotationSecondReportImageService.download(dataModel, company, printCopy);
                        break;
                }
                break;

            case AppSettings.CUSTOMER_INVOICE:
                //console.log('AppSettings.CUSTOMER_INVOICE: ', AppSettings.CUSTOMER_INVOICE);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.invoiceReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                    case 3:
                        this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                        break;
                    default:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        // this.invoiceReportCLService.download(transactionType.name,dataModel, company, printCopy, globalSetting);
                        break;
                }
                break;

            case AppSettings.SUPPLIER_PO:
                //console.log('AppSettings.SUPPLIER_PO: ', AppSettings.SUPPLIER_PO);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.purchaseOrderReportService.download(dataModel, company);
                        //console.log("case 1 ............");

                        break;
                    case 2:
                        this.purchaseOrderSecondReportService.download(transactionType.name,dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        //console.log("case 2 ............");
                        break;
                    case 3:
                        this.purchaseOrderCLReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        this.purchaseOrderReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText);
                        break;

                    default:
                        this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        //console.log("case 3 ............");
                        break;
                }
                break;

            case AppSettings.CUSTOMER_PO:
                //console.log('AppSettings.CUSTOMER_PO: ', AppSettings.CUSTOMER_PO);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.purchaseOrderReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 3:
                        this.purchaseOrderCLReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        this.purchaseOrderReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                }
                break;
            case AppSettings.PROFORMA_INVOICE:
                //console.log('AppSettings.PROFORMA_INVOICE: ', AppSettings.PROFORMA_INVOICE);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.invoiceReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                    case 3:
                        this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                        break;
                    default:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                }
                break;
            case AppSettings.CREDIT_NOTE:
                //console.log('AppSettings.CREDIT_NOTE: ', AppSettings.CREDIT_NOTE);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.invoiceReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                    case 3:
                        this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                         break;
                    case 4:
                        this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                        break;

                    default:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                }
                break;
            case AppSettings.DEBIT_NOTE:
                //console.log('AppSettings.DEBIT_NOTE: ', AppSettings.DEBIT_NOTE);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.invoiceReportService.download(dataModel, company);
                        break;
                    case 2:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                    case 3:
                        this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                        break;
                    default:
                        this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                        break;
                }
                break;

            // case AppSettings.CUSTOMER_RECEIVABLE :
            // this.customerReceiptstService.download(dataModel, company, printCopy, transactionType);
            // break;
            case AppSettings.CUSTOMER_RECEIVABLE:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 2:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 3:
                        this.customerReceiptstAndSupplierPaymentCLReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 4:
                        this.customerReceiptstAndSupplierPaymentReportLetterHeadService.download(dataModel, company, printCopy, transactionType, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                }
                break;

            // case AppSettings.SUPPLIER_PAYABLE :
            // this.customerReceiptstService.download(dataModel, company, printCopy, transactionType);
            // break;
            case AppSettings.SUPPLIER_PAYABLE:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 2:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 3:
                        this.customerReceiptstAndSupplierPaymentCLReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 4:
                        this.customerReceiptstAndSupplierPaymentReportLetterHeadService.download(dataModel, company, printCopy, transactionType, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.customerReceiptstService.download(transactionType.name,dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                }
                break;


            case AppSettings.CASH_VOUCHER:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 2:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 3:
                        this.cashVoucherAndCheckVoucherReportCLService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 4:
                        this.cashVoucherAndChequeVoucherLetterHeadService.download(dataModel, company, printCopy, transactionType, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                }
                break;

            // case AppSettings.CHEQUE_VOUCHER :
            // this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType);
            // break;

            case AppSettings.CHEQUE_VOUCHER:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 2:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 3:
                        this.cashVoucherAndCheckVoucherReportCLService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                    case 4:
                        this.cashVoucherAndChequeVoucherLetterHeadService.download(dataModel, company, printCopy, transactionType, topMargin, printCopys,printHeaderText);
                        break;
                    default:
                        this.voucherSecondReportService.download(dataModel, company, printCopy, transactionType, printCopys,printHeaderText);
                        break;
                }
                break;


            case AppSettings.GRN_TYPE_SUPPLIER:
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.grnReportService.download(transactionType.name,dataModel, company, printCopy, printCopys,printHeaderText);
                        break;
                    case 2:
                        this.grnReportService.download(transactionType.name,dataModel, company, printCopy, printCopys,printHeaderText);
                        break;
                    case 3:
                        this.grnReportCLService.download(dataModel, company, printCopy, printCopys,printHeaderText);
                        break;
                    case 4:
                        this.GrnReportLetterHeadService.download(dataModel, company, printCopy, topMargin, printCopys,printHeaderText);
                        break;

                    default:
                        this.grnReportService.download(transactionType.name,dataModel, company, printCopy, printCopys,printHeaderText);
                        break;
                }
                //this.grnReportService.download(dataModel, company, printCopy);
                break;

            case AppSettings.CUSTOMER_DC:

                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                        break;
                    case 2:
                        this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                        break;
                    case 3:
                        this.deliveryChallanCLReportService.download(billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue);
                        break;
                    case 4:
                        this.deliveryChallanReportLetterHeadService.download(billAddress, dataModel, company, printCopy, topMargin, printCopys,printHeaderText,shipAddressValue);
                        break;
                    default:
                        this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                        break;
                }

                break;
            case AppSettings.INCOMING_JOBWORK_IN_DC:
            //console.log("INCOMING_JOBWORK_IN_DC ............");
                switch (numberRangeConfiguration.printTemplateId){
                    case 1:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 2:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 3 :
                    this.deliveryChallanCLReportService.download(billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue);
                    break;

                    case 4 :
                    this.deliveryChallanReportLetterHeadService.download(billAddress, dataModel, company, printCopy, topMargin, printCopys,printHeaderText,shipAddressValue);
                    break;

                    default:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;
                }          
                break;

            case AppSettings.INCOMING_JOBWORK_OUT_DC:
                //console.log("INCOMING_JOBWORK_OUT_DC ............");
                switch (numberRangeConfiguration.printTemplateId){
                    case 1:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 2:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 3 :
                    this.deliveryChallanCLReportService.download(billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue);
                    break;

                    case 4 :
                    this.deliveryChallanReportLetterHeadService.download(billAddress, dataModel, company, printCopy, topMargin, printCopys,printHeaderText,shipAddressValue);
                    break;

                    default:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;
                }
                break;

            case AppSettings.OUTGOING_JOBWORK_IN_DC:
                switch (numberRangeConfiguration.printTemplateId){
                    case 1:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 2:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 3 :
                    this.deliveryChallanCLReportService.download(billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue);
                    break;
        
                    case 4 :
                    this.deliveryChallanReportLetterHeadService.download(billAddress, dataModel, company, printCopy, topMargin, printCopys,printHeaderText,shipAddressValue);
                    break;

                    default:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                }
                // this.deliveryChallanSecondReportService.download(billAddress, dataModel, company, printCopy, printCopys);
                break;

            case AppSettings.OUTGOING_JOBWORK_OUT_DC:
                switch (numberRangeConfiguration.printTemplateId){
                    case 1:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 2:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;

                    case 3 :
                    this.deliveryChallanCLReportService.download(billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue);
                    break;
                
                    case 4 :
                    this.deliveryChallanReportLetterHeadService.download(billAddress, dataModel, company, printCopy, topMargin, printCopys,printHeaderText,shipAddressValue);
                    break;
        
                    default:
                    this.deliveryChallanSecondReportService.download(transactionType.name,billAddress, dataModel, company, printCopy, printCopys,printHeaderText,shipAddressValue,signatureValue);
                    break;
                }
                // this.deliveryChallanSecondReportService.download(billAddress, dataModel, company, printCopy, printCopys);
                break;
            case AppSettings.OUTGOING_JOBWORK_INVOICE:
                //console.log('AppSettings.OUTGOING_JOBWORK_INVOICE: ', AppSettings.OUTGOING_JOBWORK_INVOICE);

                switch (numberRangeConfiguration.printTemplateId){
                    // case 1:
                    // this.invoiceReportService.download(dataModel, company);
                    // break;
                    case 2:
                        
                    this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                    break;
                    case 3:
                        
                    this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                    break;
                    case 4:
                    this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                    break;

                    default:
                    this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                    break;
                }
                break;

            case AppSettings.INCOMING_JOBWORK_INVOICE:
                switch (numberRangeConfiguration.printTemplateId){
                    case 1:
                    this.invoiceReportService.download(dataModel, company);
                    break;
                    case 2:
                        
                    this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                    break;
                    case 3:
                        
                    this.invoiceReportCLService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                    break;
                    case 4:
                    this.invoiceReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText,signatureValue);
                    break;

                    default:
                    this.invoiceReportSecondService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue,allowPartyCode);
                    break;
                }

                break;
            case AppSettings.INCOMING_JOBWORK_PO:
                //console.log('AppSettings.INCOMING_JOBWORK_PO: ', AppSettings.INCOMING_JOBWORK_PO);
                switch (numberRangeConfiguration.printTemplateId) {
                    case 1:
                        this.purchaseOrderReportService.download(dataModel, company);
                        //console.log("case 1 ............");

                        break;
                    case 2:
                        //console.log("case 2 ............");
                        this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 3:
                        //console.log("case 3 ............");
                        this.purchaseOrderCLReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                    case 4:
                        //console.log("case 4 ............");
                        this.purchaseOrderReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText);
                        break;

                    default:
                        //console.log("case default ............",printCopy);
                        this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                        break;
                }
                break;
                case AppSettings.OUTGOING_JOBWORK_PO:
                    //console.log('AppSettings.INCOMING_JOBWORK_PO: ', AppSettings.INCOMING_JOBWORK_PO);
                    switch (numberRangeConfiguration.printTemplateId) {
                        case 1:
                            this.purchaseOrderReportService.download(dataModel, company);
                            //console.log("case 1 ............");
    
                            break;
                        case 2:
                            this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                            //console.log("case 2 ............");
                            break;
                        case 3:
                            this.purchaseOrderCLReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                            break;
                        case 4:
                            this.purchaseOrderReportLetterHeadService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, topMargin, printCopys,printHeaderText);
                            break;
    
                        default:
                            this.purchaseOrderSecondReportService.download(transactionType.name, dataModel, company, printCopy, isItemLevelTax, printCopys,printHeaderText,signatureValue);
                            //console.log("default ............");
                            break;
                    }
                    break;
        }



    }

}