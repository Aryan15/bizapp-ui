import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
// import { ApiAiClient } from 'api-ai-javascript';
import { ApiAiClient } from 'api-ai-javascript/es6/ApiAiClient'
import { BehaviorSubject } from 'rxjs';
import { Message } from '../data-model/misc-model'
import { TextToSpeechService } from './text-to-speech.service';




@Injectable({
  providedIn: 'root'
})
export class ChatService {

  readonly token = environment.dialogflow.angularBot;
  readonly client = new ApiAiClient({ accessToken: this.token });

  projectId = environment.dialogflow.projectId;

  //sessionPath = this.sessionClient.sessionPath(this.projectId, this.token);


  //readonly client = new ApiAiClient({ accessToken: this.token });

  conversation = new BehaviorSubject<Message[]>([]);
  action = new BehaviorSubject<string>('');

  constructor(private textToSpeechService: TextToSpeechService) { }

  // Sends and receives messages via DialogFlow
  converse(msg: string) {
    const userMessage = new Message(msg, 'user');
    this.update(userMessage);

    return this.client.textRequest(msg)
      .then(res => {
        //console.log('response: ', res.result);
        const speech : string = res.result.fulfillment.speech;
        // if(speech.includes('...'))
        let speechlist:string[] = speech.split('...')
        speechlist.forEach((s,i) => {

          if(i === 0 ){
            this.textToSpeechService.speak(s);  
            const botMessage = new Message(s, 'bot');
            this.update(botMessage);
          }
          else{
            setTimeout(() => {
              this.textToSpeechService.speak(s);  
              const botMessage = new Message(s, 'bot');
              this.update(botMessage);
            }, 2000);
            
          }
          
        })
        
        


        let allParams: boolean = true;
        Object.keys(res.result.parameters).forEach(key => {
          if (!res.result.parameters[key]) {
            allParams = false;
          }
        });

        if (res.result.action && res.result.action === 'open_balance_report') {
          this.updateAction(res.result.action);
        }

      });
  }



  // Adds message to source
  update(msg: Message) {
    this.conversation.next([msg]);
  }

  updateAction(actionText: string) {
    this.action.next(actionText);
  }
}
