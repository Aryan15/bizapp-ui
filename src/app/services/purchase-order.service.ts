
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PurchaseOrderHeader, PurchaseOrderItem, RecentPurchaseOrder, PurchaseOrderReport, PurchaseOrderReportWrapper, PurchaseOrderWrapper, PoBalanceReport } from '../data-model/purchase-order-model';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import {PagedData} from "../reports/model/paged-data";
import {Page} from "../reports/model/page";
import { TransactionType } from '../data-model/transaction-type';
import { AppSettings } from '../app.settings';
import { DatePipe } from '@angular/common';
import { PayableReceivableReportRequest } from '../data-model/pr-model';
import { LongPressDirective } from '@swimlane/ngx-datatable';
@Injectable()
export class PurchaseOrderService {

    apiGetAllUrl: string;
    apiPostUrl: string;
    apiGetUrl: string;
    apiGetPurchaseOrderItemsForInvoiceUrl: string;
    //apiGetPurchaseOrderNumberUrl : string;
    apiDeletePoItemUrl: string;
    apiGetPurchaseOrderByType: string;
    apiGetPurchaseOrdersForParty: string;
    apiGetPurchaseOrderByPurchaseOrderNumber: string;
    apiDeleteUrl: string;
    recentPurchaseOrderApiUrl: string;
    poReportApiUrl: string;
    checkPONumberApiUrl: string;
    apiGetPurchaseOrderBalanceReport:string;
    apiPoCloseUrl: string;
    constructor(private http: HttpClient
        ,private datePipe: DatePipe) {
        this.apiGetAllUrl = environment.baseServiceUrl + environment.purchaseOrderApiGetAllUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.purchaseOrderApiPostUrl;
        this.apiGetUrl = environment.baseServiceUrl + environment.purchaseOrderApiGetUrl;
        //this.apiGetPurchaseOrderNumberUrl = environment.baseServiceUrl + environment.apiGetPurchaseOrderNumberUrl;
        this.apiGetPurchaseOrderByType = environment.baseServiceUrl + environment.purchaseOrderApiGetByType;
        this.apiGetPurchaseOrdersForParty = environment.baseServiceUrl + environment.purchaseOrderApiGetForParty;
        this.apiGetPurchaseOrderByPurchaseOrderNumber = environment.baseServiceUrl + environment.purchaseOrderApiGetByPurchaseOrderNumber;
        this.apiGetPurchaseOrderItemsForInvoiceUrl = environment.baseServiceUrl + environment.apiGetPurchaseOrderItemsForInvoiceUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.purchaseOrderApiGetUrl;
        this.apiDeletePoItemUrl = environment.baseServiceUrl + environment.purchaseOrderItemDeleteApiUrl;
        this.recentPurchaseOrderApiUrl = environment.baseServiceUrl + environment.recentPurchaseOrderApiUrl;
        this.poReportApiUrl = environment.baseServiceUrl + environment.poReportApiUrl;
        this.checkPONumberApiUrl = environment.baseServiceUrl + environment.checkPONumberApiUrl;
        this.apiGetPurchaseOrderBalanceReport = environment.baseServiceUrl + environment.getPoBalanceReportApiUrl;
        this.apiPoCloseUrl = environment.baseServiceUrl + environment.closePoApiUrl;
    }

    getPurchaseOrdersForParty(partyId: number, poType: string ,transactionTypeName: string): Observable<PurchaseOrderHeader[]> {
        return this.http.get<PurchaseOrderHeader[]>(this.apiGetPurchaseOrdersForParty + "/" + partyId + "/" + poType + "/" + transactionTypeName)
        // .map(response => response.json());
    }

    create(purchaseOrderWrapper: PurchaseOrderWrapper): Observable<PurchaseOrderHeader> {
        //return null;

        ////console.log("In create...", model.id);
        return this.http.post<PurchaseOrderHeader>(this.apiPostUrl, purchaseOrderWrapper)
        // .map(response => <PurchaseOrderHeader>response.json());
    }

    getPurchaseOrderItemsForInvoice(headerId: string): Observable<PurchaseOrderItem[]> {

        return this.http.get<PurchaseOrderItem[]>(this.apiGetPurchaseOrderItemsForInvoiceUrl + "/" + headerId)
        // .map(response => response.json());

    }
    getAllPurchaseOrders(): Observable<PurchaseOrderHeader[]> {
        return this.http.get<PurchaseOrderHeader[]>(this.apiGetAllUrl)
        // .map(response => response.json());
    }

    getPurchaseOrderByPurchaseOrderNumber(poNumber: string): Observable<PurchaseOrderHeader[]> {
        return this.http.get<PurchaseOrderHeader[]>(this.apiGetPurchaseOrderByPurchaseOrderNumber + "/" + poNumber)
        // .map(response => <PurchaseOrderHeader[]>response.json());
    }

    getPurchaseOrderById(id: string): Observable<PurchaseOrderHeader> {
        return this.http.get<PurchaseOrderHeader>(this.apiGetUrl + "/" + id)
        // .map(response => <PurchaseOrderHeader[]>response.json());
    }

    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }
    deleteItem(id: string): Observable<TransactionResponse> {

        return this.http.delete<TransactionResponse>(this.apiDeletePoItemUrl + "/" + id);
    }

    getRecentPurchaseOrders(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPurchaseOrder> {
        //console.log("in transactionTypeId",transactionTypeId)
        sortColumn = sortColumn ? sortColumn : "purchaseOrderDate";
        sortColumn = (sortColumn === "purchaseOrderNumber") ? "purchaseOrderId" : sortColumn;
        
        sortDirection = sortDirection ? sortDirection : "desc";
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : "null";

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        //console.log("filterText: ", filterText);

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        return this.http
            .get<RecentPurchaseOrder>(this.recentPurchaseOrderApiUrl + "/" + transactionTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }

    getPoReport(poTypeId: number, poReportRequest : TransactionReportRequest, page: Page) : Observable<PagedData<PurchaseOrderReport>>{
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
        pageSortColumn = (pageSortColumn === "purchaseOrderNumber") ? "purchaseOrderId" : pageSortColumn;
        
        // poReportRequest.transactionFromDate =  this.datePipe.transform(poReportRequest.transactionFromDate, AppSettings.DATE_FORMAT);
        // poReportRequest.transactionToDate =  this.datePipe.transform(poReportRequest.transactionToDate, AppSettings.DATE_FORMAT);
        return this.http.post<PurchaseOrderReportWrapper>(this.poReportApiUrl+ "/"
        + poTypeId + "/" 
        + pageNumber + "/" 
        + pageSize + "/" 
        + pageSortDirection + "/" 
        + pageSortColumn ,poReportRequest ).pipe(
            map(poData => this.getPagedData(page, poData)));
    }


    private getPagedData(page: Page, poData: PurchaseOrderReportWrapper): PagedData<PurchaseOrderReport> {
        
                //console.log("poItemData: ",poData);
                let pagedData = new PagedData<PurchaseOrderReport>();
        
                if(page){
                    page.totalElements = poData.totalCount ? poData.totalCount : 0 ;
                    page.totalPages = page.totalElements / page.size;
                }

                pagedData.page = page;
                pagedData.data = poData.purchaseOrderReports;
                //console.log('poData.purchaseOrderReports: ', poData.purchaseOrderReports);
                //console.log("page ", page);
                return pagedData;
            }

            
            checkPONumAvailability(poNumber: string, partyId: number, transactionTypeId: number): Observable<TransactionResponse> {
                //console.log("quatationNumber in serice", poNumber)

                let params = new HttpParams();
                // params = params.append("searchText", encodeURIComponent(filterText));
                params = params.append("poNumber", poNumber);

                return this.http.get<TransactionResponse>(this.checkPONumberApiUrl + "/" + partyId + "/" + transactionTypeId, {params: params})
                // .map(response => <TransactionResponse>response.json());
        
            }

    getPoBalanceReport(poTypeId: number, poBalanceReportRequest: PayableReceivableReportRequest, page: Page): Observable<PagedData<PoBalanceReport>> {
        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;
            if(page.sortColumn==='poNumber'){
                pageSortColumn ='purchaseOrderNumber'
            }
        return this.http.post<PurchaseOrderReportWrapper>(this.apiGetPurchaseOrderBalanceReport +
            "/" + poTypeId +
            "/" + pageNumber +
            "/" + pageSize +
            "/" + pageSortDirection + "/" + pageSortColumn, poBalanceReportRequest).pipe(
                map(poData => this.getPagedDatas(page, poData)));
    }

    private getPagedDatas(page: Page, poData: PurchaseOrderReportWrapper): PagedData<PoBalanceReport> {

        let pagedData = new PagedData<PoBalanceReport>();
        if (page) {
            page.totalElements = poData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = poData.poBalanceReport;

        return pagedData;
    }
    closePo(id: string ,statusID:number): Observable<TransactionResponse> {

        return this.http.get<TransactionResponse>(this.apiPoCloseUrl + "/" + id + "/" + statusID);
    }



}