import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Uom } from '../data-model/uom-model';
import { TokenService } from './token.service';
import { TransactionResponse } from '../data-model/transaction-response';

@Injectable()
export class UomService{


    uomsApiUrl : string;
    private headers = new Headers({'Content-Type': 'application/json'});


    constructor(private http: HttpClient, private tokenService : TokenService){
        this.uomsApiUrl = environment.baseServiceUrl + environment.uomsApiUrl
    }

    getAllUom() : Observable<Uom[]>{
                return this.http
                .get<Uom[]>(this.uomsApiUrl)
                // .map(response => <Uom[]>response.json());


    }

    delete(id: number) : Observable<TransactionResponse>{
        return this.http        
                .delete<TransactionResponse>(this.uomsApiUrl+"/"+id)
                // .map(response => <TransactionResponse>response.json());

    }
}