import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { MaterialType } from '../data-model/material-type-model';
import { TokenService } from './token.service';

@Injectable()
export class MaterialTypeService{
    materialTypeApiUrl : string;
    private headers = new Headers({'Content-Type': 'application/json'});


    constructor(private http: HttpClient, private tokenService : TokenService){
        this.materialTypeApiUrl = environment.baseServiceUrl + environment.materialTypeApiUrl;
    }

    getAllMaterialTypes() : Observable<MaterialType[]>{
        return this.http
        .get<MaterialType[]>(this.materialTypeApiUrl)
        // .map(response => <MaterialType[]>response.json());
}
} 