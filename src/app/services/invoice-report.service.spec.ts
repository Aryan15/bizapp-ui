import { TestBed, inject } from '@angular/core/testing';

import { InvoiceReportService } from './template1/invoice-report.service';

describe('InvoiceReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoiceReportService]
    });
  });

  it('should be created', inject([InvoiceReportService], (service: InvoiceReportService) => {
    expect(service).toBeTruthy();
  }));
});
