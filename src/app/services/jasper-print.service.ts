import { Injectable } from "@angular/core";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { TransactionReportRequest } from "../data-model/transaction-report-request-model";
import { Page } from "../reports/model/page";
import { InvoiceHeader } from "../data-model/invoice-model";

@Injectable({
    providedIn: 'root'
})
export class JasperPrintService {
    jasperPrintApiUrl: string;
    jasperReportApiUrl: string;
    jasperReportExcelApiUrl: string;
    jasperReportExcelFormateApiUrl: string;
    jasperReportWordFormateApiUrl: string;
    constructor(private http: HttpClient) {

        this.jasperPrintApiUrl = environment.baseServiceUrl + environment.jasperPrintApiUrl;
        this.jasperReportApiUrl = environment.baseServiceUrl + environment.jasperReportApiUrl;
        this.jasperReportExcelApiUrl = environment.baseServiceUrl + environment.jasperReportExcelApiUrl;
        this.jasperReportExcelFormateApiUrl = environment.baseServiceUrl + environment.jasperReportExcelFormateApiUrl;
        this.jasperReportWordFormateApiUrl = environment.baseServiceUrl + environment.jasperReportWordFormateApiUrl;
    }

    jasperPrint(id: string, reportName: string, copyText: string, amountInWords: string, printHeaderText: string,signature :number,allowShipAddress:number): Observable<any> {

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("id", id);
        params = params.append("reportName", reportName);
        params = params.append("copyText", copyText);
        params = params.append("amountInWords", amountInWords);
        params = params.append("printHeaderText", printHeaderText);
        
        const httpOption: Object = {
            params: params,
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.get<any>(this.jasperPrintApiUrl+ "/" + signature+ "/" + allowShipAddress, httpOption)
            // .pipe(
            //     map(
            //     (res) => {
            //         return new Blob([res.blob()], { type: 'application/pdf' })
            //     })
            // )
            .pipe(
                //tap(res => //console.log("res: ", res)),
                map(res => new Blob([res['body']], { type: 'application/pdf' }))
            )
    }


    // jasperExcel(id: string): Observable<any>{

    //     const httpOption: Object = {
    //         observe: 'response',
    //         headers: new HttpHeaders({
    //           'Content-Type': 'application/json'
    //         }),
    //         responseType: 'arraybuffer'
    //       };

    //     return this.http.get<any>(this.invoiceJasperExcelReportApiUrl + "/" + id, httpOption)

    //     .pipe(
    //         map(res => new Blob([res['body']],{ type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }))
    //     )
    // }


    jasperReport(tTypeId: number, counterTTypeId: number, jasperReportName: string, reportRequest: TransactionReportRequest, page: Page): Observable<any> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.post<any>(
            this.jasperReportApiUrl + "/"
            + tTypeId + "/"
            + counterTTypeId + "/"
            + jasperReportName + "/"
            + pageNumber + "/"
            + pageSize + "/"
            + pageSortDirection + "/"
            + pageSortColumn
            , reportRequest
            , httpOption)
            .pipe(
                map(res => new Blob([res['body']], { type: 'application/pdf' }))
            )
    }


    jasperReportExcel(tTypeId: number, counterTTypeId: number, jasperReportName: string, reportRequest: TransactionReportRequest, page: Page): Observable<any> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        const httpOption: Object = {
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.post<any>(
            this.jasperReportExcelApiUrl + "/"
            + tTypeId + "/"
            + counterTTypeId + "/"
            + jasperReportName + "/"
            + pageNumber + "/"
            + pageSize + "/"
            + pageSortDirection + "/"
            + pageSortColumn
            , reportRequest
            , httpOption)
            .pipe(
                map(res => new Blob([res['body']], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }))
            )
    }

    jasperReportExcelForm(id: string, reportName: string, copyText: string, amountInWords: string, printHeaderText: string): Observable<any> {

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("id", id);
        params = params.append("reportName", reportName);
        params = params.append("copyText", copyText);
        params = params.append("amountInWords", amountInWords);
        params = params.append("printHeaderText", printHeaderText)

        const httpOption: Object = {
            params: params,
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.get<any>(this.jasperReportExcelFormateApiUrl, httpOption)
            // .pipe(
            //     map(
            //     (res) => {
            //         return new Blob([res.blob()], { type: 'application/pdf' })
            //     })
            // )
            .pipe(
                //tap(res => //console.log("res: ", res)),
                map(res => new Blob([res['body']], { type: 'application/doc' }))
            )
    }

    jasperReportWordForm(id: string, reportName: string, copyText: string, amountInWords: string, printHeaderText: string): Observable<any> {
        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("id", id);
        params = params.append("reportName", reportName);
        params = params.append("copyText", copyText);
        params = params.append("amountInWords", amountInWords);
        params = params.append("printHeaderText", printHeaderText)

        const httpOption: Object = {
            params: params,
            observe: 'response',
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            responseType: 'arraybuffer' //ResponseContentType.Blob 
        };

        return this.http.get<any>(this.jasperReportWordFormateApiUrl, httpOption)
            // .pipe(
            //     map(
            //     (res) => {
            //         return new Blob([res.blob()], { type: 'application/pdf' })
            //     })
            // )
            .pipe(
                //tap(res => //console.log("res: ", res)),
                map(res => new Blob([res['body']], { type: 'application/docx' }))
            )

    }
}

