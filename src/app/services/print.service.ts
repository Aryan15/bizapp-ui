import { Injectable } from "@angular/core";
import { ImageMetadata } from "../data-model/misc-model";
import { TransactionItem } from "../data-model/transaction-item";
import { DatePipe } from "@angular/common";
import { Company } from "../data-model/company-model";
import { BehaviorSubject } from "rxjs";
import { AppSettings } from "../app.settings";
import { GlobalSetting } from "../data-model/settings-wrapper";
import { InvoiceHeader } from "../data-model/invoice-model";
import 'jspdf-autotable'


var totalPagesExp = "{total_pages_count_string}";

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  leftMargin: number = 15;
  rightMargin: number = 20;
  afterTop: number = 80;
  afterTopCL: number = 170;
  bottomMargin: number = 50;
   transactionName:string;
  transactionItem: TransactionItem;
  // leftMargin: number = 15;
  // rightMargin: number = 20;
  // bottomMargin: number = 50;
  datePipe = new DatePipe('en-US');
  printData: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  writeCompanyGST( tagLine: string
    , gstNumber: string
    , stateName: string
    , stateCode: string
    , doc: any
    , leftMargin: number = this.leftMargin
    , rightMargin: number = 400) {

    doc.autoTable(this.getGSTHeader(), this.getGST(tagLine, gstNumber, stateName, stateCode), {

      startY: doc.autoTable.previous.finalY + 10,
      margin: { left: leftMargin, right: rightMargin },
      theme: 'plain',
      tableWidth: '50',
      showHead: 'never',
      styles: {
        halign: 'left',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false,
        cellPadding: 0,
      },
      columnStyles: {
        columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'left' },
        columnContent: { cellWidth: 300, halign: 'left' }
      }

    });

  }

  writeCompanyGSTCL(tagLine: string
    , gstNumber: string
    , stateName: string
    , stateCode: string
    , doc: any
    , leftMargin: number = this.leftMargin
    , rightMargin: number = 400) {

    doc.autoTable(this.getGSTHeader(), this.getGSTCL(tagLine, gstNumber, stateName, stateCode), {

      startY: doc.autoTable.previous.finalY + 10,
      margin: { left: leftMargin, right: rightMargin },
      theme: 'plain',
      tableWidth: '50',
      showHead: 'never',
      styles: {
        halign: 'center',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false,
        cellPadding: 0,
      },
      columnStyles: {
        columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'right' },
        columnContent: { cellWidth: 300, halign: 'left' }
      }

    });

  }
  
  writeImage(imageMetadata: ImageMetadata
    , imgData: any
    , doc: any
    , x: number = this.leftMargin
    , y: number = 20
    , bound_width: number = 140
    , bound_height: number = 50 
    , isSignature: boolean = false) {

      if (!imageMetadata || !imgData){
        return;
      }
    let original_width: number = imageMetadata.width;
    let original_height: number = imageMetadata.height;
    //let bound_width: number = 140;
    //let bound_height: number = 50;
    let new_width = original_width;
    let new_height = original_height;

    // first check if we need to scale width
    if (original_width > bound_width) {
      //scale width to fit
      new_width = bound_width;
      //scale height to maintain aspect ratio
      new_height = (new_width * original_height) / original_width;
    }

    // then check if we need to scale even with the new height
    if (new_height > bound_height) {
      //scale height to fit instead
      new_height = bound_height;
      //scale width to maintain aspect ratio
      new_width = (new_height * original_width) / original_height;
    }

    //console.log('new width, new height ', new_width, new_height);

    // if (imgData) doc.addImage(imgData, 'JPEG', this.leftMargin, 20, new_width, new_height)
    if(isSignature){
      if (imgData) doc.addImage(imgData, 'JPEG', doc.internal.pageSize.width - new_width - 10, y, new_width, new_height)      
    }else{
      if (imgData) doc.addImage(imgData, 'JPEG', x, y, new_width, new_height)
    }
    
  }

  writeTitleAndCopyTypeCL(copyTypeText: string, titleText: string, doc: any) {



    var rectangleStartX: number = 400;
    var rectangleStartY: number = 10;
    var rectangleWidth: number = 180;
    var rectangleHeight: number = 20;
    doc.setDrawColor(150);
    doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

    doc.setFontSize(12);
    // doc.setFontType('light');
    doc.setTextColor(150);

    // var img = new Image();

    // // img.onload = function(){
    //     var dataURI = getBase64Image(img);
    //     return dataURI;

    // // }

    // img.src = "Horse.jpg";

    var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (doc.getStringUnitWidth(copyTypeText) * doc.internal.getFontSize() / 2));
    var copyTypeTextOffsetY: number = rectangleStartY + doc.internal.getFontSize() / 2; //14;
    //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
    //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
    doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);
    doc.setTextColor(40);
    doc.setFontSize(10);
    var xOffset: number = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(titleText) * doc.internal.getFontSize() / 2);



    var titleTextYOffset: number = 18;

    doc.text(titleText, xOffset, rectangleStartY + titleTextYOffset);


  }

  writeTitleAndCopyType(copyTypeText: string, titleText: string, doc: any) {



    var rectangleStartX: number = 400;
    var rectangleStartY: number = 10;
    var rectangleWidth: number = 180;
    var rectangleHeight: number = 20;
    doc.setDrawColor(150);
    doc.rect(rectangleStartX, rectangleStartY, rectangleWidth, rectangleHeight);

    doc.setFontSize(12);
    // doc.setFontType('light');
    doc.setTextColor(150);

    // var img = new Image();

    // // img.onload = function(){
    //     var dataURI = getBase64Image(img);
    //     return dataURI;

    // // }

    // img.src = "Horse.jpg";

    var copyTypeTextOffsetX: number = rectangleStartX + (rectangleWidth / 2 - (doc.getStringUnitWidth(copyTypeText) * doc.internal.getFontSize() / 2));
    var copyTypeTextOffsetY: number = rectangleStartY + doc.internal.getFontSize() / 2; //14;
    //console.log('copyTypeTextOffsetX: ', copyTypeTextOffsetX);
    //console.log('copyTypeTextOffsetY: ', copyTypeTextOffsetY);
    doc.text(copyTypeText, copyTypeTextOffsetX, rectangleStartY + copyTypeTextOffsetY);
    doc.setTextColor(40);
    doc.setFontSize(16);
    var xOffset: number = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(titleText) * doc.internal.getFontSize() / 2);



    var titleTextYOffset: number = 18;

    doc.text(titleText, xOffset, rectangleStartY + (copyTypeTextOffsetY / 2) + doc.internal.getFontSize() / 2 + titleTextYOffset);


  }


  getPhHeader(): any[] {

    var headerColumns = [
      // { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];


    return headerColumns;

  }

  getPhnNum(phoneNumber: string): any[] {

    return [
      {
        //  "columnHead": ":",
        "columnContent": "Phone: " + phoneNumber,
      },


    ];
  }

  getMobHeader(): any[] {

    var headerColumns = [
      // { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

    return headerColumns;

  }

  getMobNum(MobNumber: string): any[] {

    return [
      {
        //  "columnHead": ":",
        "columnContent": "Mobile: " + MobNumber,
      },
    ];
  }

  
  getCompanyHeader(): any[] {

    var headerColumns = [
      { title: "Company Name", dataKey: "companyName" },
    ];

    return headerColumns;

  }

  getCompany(companyName: string): any[] {

    return [
      {
        "companyName": companyName || "", //"Uthkrushta Technologies",
      }
    ];
  }

  getAddressHeader(): any[] {

    var headerColumns = [

      // { title: "Company Address", dataKey: "companyAddress" },
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

    return headerColumns;

  }

  getAddress(address: string
    , email: string
    , phone: string
    , mobile: string
    , website: string
     ,mobileSec:string
    , isCL: boolean = false): any[] {
      
    let mobileSecondary = mobileSec && mobile ? ","+mobileSec: "";
    
    let data: any[] = [
      // {

      //   "companyAddress": address || "", //"Nagendra Block, BSK 3rd Stage, 29203882038"
      // }
      {
        "columnHead": "Address",
        "columnContent": address,
      },
      {
        "columnHead": "Website",
        "columnContent": website,
      },
      {
        "columnHead": "Email",
        "columnContent": email,
      },
      {
        "columnHead": "Phone",
        "columnContent": isCL ? "Phone: "+phone : phone,
      },
      {
        "columnHead": "Mobile",
        "columnContent": isCL ? "Mobile: "+mobile : mobile+mobileSecondary,
      },
    ];

    if(isCL){
      return data;
    }else{
      return data.filter(row => row.columnContent);
    }
    
  }

  getAddressCL(
    tagLine: string
    , address: string
    , email: string
    , phone: string
    , mobile: string
    , website: string
    , gstNumber: string
    , stateName: string
    , stateCode: string): any[] {

    let data: any[] = [
      {
        "columnHead": "TagLine",
        "columnContent": tagLine ? tagLine : null,
      },
      {
        "columnHead": "GSTIN",
        "columnContent": gstNumber ? "GSTIN: "+gstNumber : null,
      },
      {
        "columnHead": "State",
        "columnContent": gstNumber ? stateName + "(" + stateCode.replace('.00','') + ")" : null,
      },
      {
        "columnHead": "Address",
        "columnContent": address,
      },
      {
        "columnHead": "Website",
        "columnContent": website,
      },
      {
        "columnHead": "Email",
        "columnContent": email,
      },
      {
        "columnHead": "Phone",
        "columnContent": phone ? "Phone: "+phone : null,
      },
      {
        "columnHead": "Mobile",
        "columnContent": mobile ? "Mobile: "+mobile : null,
      },
    ];

   
      return data;
   
    
  }


  getGSTHeader(): any[] {
    var headerColumns = [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

    return headerColumns;
  }

  getGST(tagLine: string, gstNumber: string, stateName: string, stateCode: string): any[] {
   
    let retValue = [
      {
        "columnHead": "TagLine",
        "columnContent": tagLine,
      },
      {
        "columnHead": "GSTIN",
        "columnContent": gstNumber,
      },
      {
        "columnHead": "State",
        "columnContent": stateName + "(" + stateCode.replace(".00",'') + ")",
      }
    ];
    return retValue.filter(r => r.columnContent);
  }

  getGSTCL(tagLine: string, gstNumber: string, stateName: string, stateCode: string): any[] {
    let retValue = [
      {
        "columnHead": "TagLine",
        "columnContent": tagLine,
      },
      {
        "columnHead": "GSTIN: ",
        "columnContent": gstNumber,
      },
      {
        "columnHead": "State: ",
        "columnContent": stateName + "(" + stateCode + ")",
      }
    ];

    // return gstNumber ? retValue : null;
    return retValue.filter(r => r.columnContent);
    
  }

  getCompanyEmail(): any[] {
    var headerColumns = [

      { title: "Column Content", dataKey: "columnContent" },
    ];

    return headerColumns;
  }

  getEmail(email: string): any[] {
    return [
      {
        //  "columnHead": ":",
        "columnContent": "Email: " + email,
      },
    ];
  }
  getCompanyWebsite(): any[] {
    var headerColumns = [

      { title: "Website", dataKey: "website" },
    ];

    return headerColumns;
  }

  getWebsite(website: string): any[] {
    return [
      {

        "website": website, //"emil.com"
      }
    ];
  }

  drawLine(doc: any, yAxis: number = null) {
    doc.setLineWidth(1.5);
    doc.setDrawColor(63, 81, 181); // draw red lines
    let lineX: number = this.leftMargin;
    let lineY: number = yAxis ? yAxis : doc.autoTable.previous.finalY + 5;
    let lineXEnd: number = doc.internal.pageSize.width - 15;
    doc.line(lineX, lineY, lineXEnd, lineY)
  }

  writePhoneIconImage(doc: any, posX: number, posY: number) {
    var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADySURBVEhL7dS9DgFBFIbhDb1OJVG5CIlStCJxAdyCzp0odQiXQKEXFRp/nUaHoPH3fslKZLISy9nOlzxZmeIcZ2Yy3j9hU8AKd1wxQgZmWUPFX7Vhlg3cBjOYZQi3wQ5macBtcEMcJqnCbTCAWRI441l8giRM04OKL2BeXMlB+75HSgtRpANNoWkiSRpHqElNC1GkCG3VBWUtvEnM/36VOjSFmriT6AL08TyvKVoI/XapiR4+NepCB6/iusJac+mBDJ0SDlABfZf+7yD6M19FB68t0JYEFX71U7LQ9T0hqLimM4melQqaGGOLOfL459N43gPrimFW4H6xDQAAAABJRU5ErkJggg==';
    //var doc = new jsPDF()
    doc.addImage(imgData, 'PNG', posX, posY, 10, 10)

  }

  writeMobileIconImage(doc: any, posX: number, posY: number) {

    var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjAgILJQv/ub7pAAAAY0lEQVQoz+3KsRGCQBgF4Q8NyMyIGCozhdKuDaUAuoAKHBK5YX6DM2BMTAzd6L3ZpTDIwmoyWYVNX0T1DpLO0+6Bi7Pa7OpA0h6vVirj5Av/4PfBIoTRKITlM7jLoNGA7FbECx94Gble1W29AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTAyLTAyVDEwOjM3OjExKzAxOjAwE1bO3AAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wMi0wMlQxMDozNzoxMSswMTowMGILdmAAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC'
    doc.addImage(imgData, 'PNG', posX, posY, 10, 10)
  }
  writeCompanyAndAddress(
    tagLine: string,
    companyName: string,
    address: string,
    email: string,
    website: string,
    primaryTelephone: string,
    primaryMobile: string,
    gstNumber: string,
    stateName: string,
    stateCode: string,
    secondaryMobile:string,
    doc: any) {
    ////console.log("this.company"+email+this.getCompanyEmail+ this.getEmail);
    var startCompanyY: number = 75;
    //var startAddressY: number = 80;
    var docl = doc;
    // var pageContent = function (data) {

    //   // FOOTER
    //   var str = "Page " + data.pageCount;
    //   // Total page number plugin only available in jspdf v1.0+
    //   if (typeof docl.putTotalPages === 'function') {
    //     str = str + " of " + totalPagesExp;
    //   }
    //   docl.setFontSize(10);
    //   docl.text(str, data.settings.margin.left, docl.internal.pageSize.height - 10);
    // };

    doc.setTextColor(63, 81, 181);
    doc.autoTable(this.getCompanyHeader(), this.getCompany(companyName), {
      // didDrawPage: pageContent,
      startY: startCompanyY,
      margin: { left: this.leftMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'bold',
        fontSize: 12,
        textColor: [63, 81, 181],
        cellPadding: 0,
      }

    });
    doc.setTextColor(40);
    if(gstNumber){
      this.writeCompanyGST(tagLine, gstNumber, stateName, stateCode, doc)
    }
    doc.autoTable(this.getAddressHeader(), this.getAddress(address, email, primaryTelephone, primaryMobile, website,secondaryMobile ), {

      startY: doc.autoTable.previous.finalY ,
      margin: { left: this.leftMargin, right: 300 },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
        cellPadding: 0,
      },
      columnStyles: {
        columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'left' },
        columnContent: { cellWidth: 300, halign: 'left' }
      }
    });



    //console.log("email" + email);
    

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }
  }


  writeCompanyAndAddressCL(
    tagLine: string,
    companyName: string,
    address: string,
    email: string,
    website: string,
    primaryTelephone: string,
    primaryMobile: string,
    gstNumber: string,
    stateName: string,
    stateCode: string,
    doc: any) {
    ////console.log("this.company"+email+this.getCompanyEmail+ this.getEmail);
    var startCompanyY: number = 40;
    //var startAddressY: number = 80;
    doc.setFontSize(12);
    var xOffset: number = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(companyName) * doc.internal.getFontSize() / 2);
    let companyStartX: number = 145 // Start after image
    //var docl = doc;
    // var pageContent = function (data) {

    //   // FOOTER
    //   var str = "Page " + data.pageCount;
    //   // Total page number plugin only available in jspdf v1.0+
    //   if (typeof docl.putTotalPages === 'function') {
    //     str = str + " of " + totalPagesExp;
    //   }
    //   docl.setFontSize(10);
    //   docl.text(str, data.settings.margin.left, docl.internal.pageSize.height - 10);
    // };

    doc.setTextColor(63, 81, 181);
    doc.autoTable(this.getCompanyHeader(), this.getCompany(companyName), {
      // didDrawPage: pageContent,
      startY: startCompanyY,
      margin: { left: companyStartX, right: 150 },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'bold',
        fontSize: 14,
        textColor: [63, 81, 181],
        cellPadding: 0,
      },
      columnStyles: {
        // columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'left' },
        companyName: { cellWidth: 300, halign: 'center' }
      }

    });
    doc.setTextColor(40);
    // if(gstNumber){
    //   this.writeCompanyGSTCL(gstNumber, stateName, stateCode, doc, companyStartX, 150)
    // }

    let dataHeader = [
      { title: "Column Content", dataKey: "columnContent" },
    ];
    let data: any[] = this.getAddressCL(tagLine, address, email, primaryTelephone, primaryMobile, website, gstNumber, stateName,  stateCode );
    ////console.log("data before", data);
    
    //data.concat(this.getAddress(address, email, primaryTelephone, primaryMobile, website, true ));
    ////console.log("data after", data);
    let dataJust: any[] = [];
    data.forEach(d => {
      if(d.columnContent){
        dataJust.push({
          "columnContent": d.columnContent
        })
      }      
    });

    data.forEach(d => {
      if(!d.columnContent){
        dataJust.push({
          "columnContent": d.columnContent
        })
      }      
    })

    doc.autoTable(dataHeader, dataJust, {

      startY: doc.autoTable.previous.finalY ,
      // margin: { left: this.leftMargin+50, right: this.rightMargin },
      margin: { left: companyStartX, right: 150 },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'left',
        overflow: 'linebreak',
        overflowColumns: false,
        fontStyle: 'normal',
        fontSize: 10,
        cellPadding: 0,
      },
      columnStyles: {
        // columnHead: { cellWidth: 50, fontStyle: 'bold', halign: 'left' },
        columnContent: { cellWidth: 300, halign: 'center' }
      }
    });



    //console.log("email" + email);
    

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }
  }

  getTxnNumberHeader() {
    var headerColumns = [
      { title: "Column Head", dataKey: "columnHead" },
      { title: "Column Content", dataKey: "columnContent" },
    ];

    return headerColumns;
  }

  getTxnNumber(transactionNumber: string
    , transactionDate: string
    , transactionNumberHeader: string 
    , transactionDateHeader: string): any[] {

      let tHeader = transactionNumberHeader ? transactionNumberHeader : "#";
      let tDateHeader = transactionDateHeader ? transactionDateHeader : "Date";
    return [
      {
        "columnHead": tHeader,
        "columnContent": transactionNumber,
      },
      {
        "columnHead": tDateHeader,
        "columnContent": this.datePipe.transform(transactionDate, 'dd/MM/yyyy')
      },
    ];
  }
  
  writeTransactionHeader(transactionNumber: string
    , transactionDate: string
    , doc: any
    , transactionNumberHeader: string 
    , transactionDateHeader: string) {

    let startY: number = 35;
    let leftMargin: number = doc.internal.pageSize.width - 240;
    doc.autoTable(this.getTxnNumberHeader(), this.getTxnNumber(transactionNumber, transactionDate, transactionNumberHeader, transactionDateHeader), {
      startY: startY,
      margin: { left: leftMargin, right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'right',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false,
        cellPadding: 1,
      },
      columnStyles: {
        columnHead: { fontStyle: 'bold', cellWidth: 100 },
        columnContent: { halign: 'left', },
      }
    });

    

  }
  writeTermsAndConditionAndDeliveryAndPaymentTerms(deliveryTerms: string, 
    paymentTerms: string, 
    termsAndCondition: string, 
    amountInwords: string, 
    doc: any, 
    tableHeight: number, 
    summaryTableStartY: number, 
    summaryTableLeftMargin: number, 
    companyName: string,
    company? : Company,
    printBank? : boolean,
    signatuerMetaData? :ImageMetadata,
    signatureData?:ImageMetadata,
    service?:any
  ) {
      
      
    
    doc.setFontSize(10);
    
    let amountHeader = [
      { title: "dummy", dataKey: "key" },
      { title: "dummy", dataKey: "data" },
    ];

    let summaryTableEnd: number = doc.autoTable.previous.finalY + 10 ;
    let tableStart: number = summaryTableStartY;//doc.autoTable.previous.finalY - 100 ;//150 ;

    //console.log("tableStart",tableStart,summaryTableEnd)

    if (amountInwords != null) {

      //console.log("condition true....!!");

      let amount = [
        {
          "key": "Amount in Words:",
          "data": amountInwords
        }
      ]
      

      //console.log("after del" + doc.autoTable.previous.finalY)
      doc.autoTable(amountHeader, amount, {
        startY: tableStart,
        // margin: { left: this.leftMargin, right: (doc.internal.pageSize.width - (summaryTableLeftMargin)) }, // +this.leftMargin+200 this.leftMargin + this.rightMargin +
        margin: { left: this.leftMargin, right: (doc.internal.pageSize.width - ( 250)) }, // this.leftMargin + this.rightMargin +this.leftMargin+200 this.leftMargin + this.rightMargin +
        showHead: 'never',
        theme: 'plain',
        styles: {
          halign: 'left',
          fontSize: 8,
          overflow: 'linebreak',
          overflowColumns: false,
          cellPadding: 0
        },
        columnStyles: {
          key: { fontStyle: 'bold', }
        },
      });

    }
    let bankTableStart = tableStart;
    tableStart = doc.autoTable.previous.finalY;
    //console.log("TnC tableStart: ", tableStart);
    if(printBank && company.bank){
      let amount = [
        {
          "key":"Bank:",
          "data": company.bank
        },
        {
          "key":"Branch:",
          "data": company.branch,
        }
        ,
        {
          "key":"A/C:",
          "data": company.account
        }
        ,
        {
          "key":"IFSC:",
          "data": company.ifsc
        }
      ]
      doc.autoTable(amountHeader, amount, {
        startY: bankTableStart,
        margin: { left: this.leftMargin+250, right: (doc.internal.pageSize.width - (summaryTableLeftMargin)-this.leftMargin - this.rightMargin) }, //this.leftMargin + this.rightMargin +
        showHead: 'never',
        theme: 'plain',
        styles: {
          halign: 'left',
          fontSize: 8,
          overflow: 'linebreak',
          overflowColumns: false,
          cellPadding: 0
        },
        columnStyles: {
          key: { fontStyle: 'bold', cellWidth: 30}
        },
      });
    }

    // var tableHeight = doc.internal.pageSize.height - (doc.autoTable.previous.finalY + 50);

    doc.setDrawColor(215, 235, 252);
    //doc.rect(this.leftMargin, doc.autoTable.previous.finalY + 10, (doc.internal.pageSize.width - (this.leftMargin + this.rightMargin)), tableHeight);

    let tncHeader = [
      { title: "Terms And Conditions", dataKey: "tnc" },
    ];

    let tncDetails = [
      {
        "tnc": termsAndCondition ? termsAndCondition : "",

      }
    ];

    

    doc.autoTable(tncHeader, tncDetails, {
      startY: tableStart + 5,
      margin: { left: this.leftMargin
              // , right: (doc.internal.pageSize.width - (this.leftMargin + this.rightMargin + summaryTableLeftMargin))               
              , right: (doc.internal.pageSize.width - ( 250))               
            },
      theme: 'plain',
      styles: {
        halign: 'left',
        fontSize: 7,
        fillStyle: 'DF',
        overflow: 'linebreak',
        overflowColumns: false,
        cellPadding: 0        
      },
      headStyles: {
        fontSize: 10,
      },
    });

    let signatureHeader = [
      { title: "key", dataKey: "data" },
    ];

    let signature = [
      {
        "data": "For " + companyName,
      }
    ];

   
    //tableStart += 20;
    // tableStart = doc.autoTable.previous.finalY + 5;
    
    // this.drawLine(doc, summaryTableEnd - 2);
    doc.autoTable(signatureHeader, signature, {
      startY: summaryTableEnd,
      margin: { left: (this.leftMargin + summaryTableLeftMargin), right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'right',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      didDrawCell: function (row, data) {
        if (row.index === 0) {
          //this.doc.setFontStyle('bold');
          //console.log(data);
          data.doc.setFontStyle('bold');
          data.settings.styles = { fontStyle: 'bold' };
        }
      }


    });

    let SignatureHeader = [
      { title: "key", dataKey: "data" },
    ];

    let Signature = [

      {
        "data": "",
      },

      {
        "data": "Authorised Signatory"
      }

    ];
    if(signatuerMetaData){
      service.writeImage(signatuerMetaData, signatureData, doc, doc.internal.pageSize.width - 200, summaryTableEnd + 27,190,40,true);    
    }
    doc.autoTable(SignatureHeader, Signature, {
      startY: summaryTableEnd + 45,
      margin: { left: (this.leftMargin + summaryTableLeftMargin), right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        halign: 'right',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      didDrawCell: function (row, data) {
        if (row.index === 0) {
          //console.log(data);
          data.doc.setFontStyle('bold');
          data.settings.styles = { fontStyle: 'bold' };
        }
      }

     
    });
    

    let recvSignatureHeader = [
      { title: "key", dataKey: "data" },
    ];

    let recvSignature = [
      {
        "data": "",
      },
      {
        "data": "Receiver's Signature"
      }
    ];


    doc.autoTable(recvSignatureHeader, recvSignature, {
      startY: summaryTableEnd + 40,
      //margin: { left: (this.leftMargin + this.summaryTableLeftMargin), right: this.rightMargin },
      theme: 'plain',
      showHead: 'never',
      styles: {
        valign: 'middle',
        fontSize: 10,
        overflow: 'linebreak',
        overflowColumns: false
      },
      didDrawCell: function (row, data) {
        if (row.index === 0) {
          //this.doc.setFontStyle('bold');
          //console.log(data);
          data.doc.setFontStyle('bold');
          data.settings.styles = { fontStyle: 'bold' };
        }
      }


    });

  }


  getItemColumnsIGST(transactionItem): any[] {

    //    var ItemArray: any[] = [];
    //   this.transactionItem.forEach(inv => {
    //     ItemArray.push(this.makeInvoiceItemIGST(inv));
    //         });
    return transactionItem;
  }




  getItemColumnHeadersIGST(): any[] {
    return [
      { title: "NO", dataKey: "slNo" },
      // { title: "Part Number", dataKey: "partNumber" },
      { title: "Material", dataKey: "materialName" },
      { title: "HSN/ SAC", dataKey: "hsnCode" },
      { title: "Qty", dataKey: "quantity" },
      { title: "Unit", dataKey: "uom" },
      { title: "Price", dataKey: "price" },
      { title: "Amount", dataKey: "amount" },
      { title: "IGST \n%", dataKey: "igstPercent" },
      { title: "IGST \nAmt", dataKey: "igstAmount" },
      // {title: "IGST%", dataKey: "igstPercent"},
      // {title: "IGST Amt", dataKey: "igstAmount"},
      { title: "Total", dataKey: "totalAmount" },
    ];
  }
  writeItemsIGST(doc: any, model: TransactionItem, totalPagesExp: string, custTableLastY: number, ) {

    var pageWidth: number = doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    var slNoWidth: number = pageWidth * 0.04; //5%
    // var partNumberWidth: number = pageWidth * 0.07; //10%
    var materialNameWidth: number = pageWidth * 0.34; //30% of total width
    var hsnWidth: number = pageWidth * 0.07; //6%
    var qtyWidth: number = pageWidth * 0.03; //6%
    var uomWidth: number = pageWidth * 0.05; //6%
    var priceWidth: number = pageWidth * 0.09; // 8%
    var amountWidth: number = pageWidth * 0.09; //8%
    var igstPercentWidth: number = pageWidth * 0.05; //5%
    var igstAmountWidth: number = pageWidth * 0.09; //8%
    //var sgstPercentWidth: number = pageWidth * 0.05; //5%
    //var sgstAmountWidth: number = pageWidth * 0.09; //8%
    var totalAmountWidth: number = pageWidth * 0.1; //10%

    //console.log('page width: ', doc.internal.pageSize.width);

    // var pageContent = function (data) {

    //   // FOOTER
    //   if (doc) {
    //     var str = "Page " + data.pageCount;
    //     if (typeof this.doc.putTotalPages === 'function') {
    //       str = str + " of " + this.totalPagesExp;
    //     }
    //     doc.setFontSize(10);
    //     doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

    //   }
    // };


    doc.autoTable(this.getItemColumnHeadersIGST(), this.getItemColumnsIGST(model), {
      //addPageContent: pageContent,
      startY: custTableLastY + 5,
      margin: { left: this.leftMargin, right: this.rightMargin },
      //theme:'grid',
      theme: 'grid',
      tableWidth: 'auto',
      //tableLineColor: [0,0,0],
      //tableLineWidth: 0.5,
      styles: {
        //cellPadding: 0.5,
        fontSize: 8,
        lineColor: [0, 0, 0],
        fillStyle: 'DF',
        //halign: 'right',
        overflow: 'linebreak',
        overflowColumns: false
      },
      headerStyles: {
        fillColor: [215, 235, 252],
        lineColor: [0, 0, 0],
        fontSize: 9,
        textColor: 0,
        fontStyle: 'normal',
        halign: 'center',
      },
      columnStyles: {
        slNo: { cellWidth: slNoWidth, halign: 'right' },
        // partNumber: { cellWidth: partNumberWidth },
        materialName: { cellWidth: materialNameWidth },
        hsnCode: { cellWidth: hsnWidth },
        quantity: { cellWidth: qtyWidth, halign: 'right' },
        uom: { cellWidth: uomWidth },
        price: { cellWidth: priceWidth, halign: 'right' },
        amount: { cellWidth: amountWidth, halign: 'right' },
        igstPercent: { cellWidth: igstPercentWidth, halign: 'right' },
        igstAmount: { cellWidth: igstAmountWidth, halign: 'right' },
        totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

      }
      // drawCell: function (cell, data) {
      //   //console.log('cell before:: ', cell);
      //   if (data.column.index % 2 === 1) {
      //     cell.styles.fillColor = "[215, 235, 252]";
      //   }
      //   //console.log('cell:: ', cell);
      // },
    });

    doc.setLineWidth(1);
    //this.doc.setDrawColor(215, 235, 252);
    //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

    //console.log('this.doc.autoTable.previous ', doc.autoTable.previous);
    //this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }


  }



  topSectionWithCertiImage(
    docl: any
    , companyl: Company
    , imageMetadatal: ImageMetadata
    , imgDatal: string
    , certiImageMetadatal: ImageMetadata
    , certiImgDatal: string
    , copyTypeTextl: string
    , titleTextl: string
    , transaction: any
    , service: any
    , transactionNumber: string
    , transactionDate: string
    , leftMargin: number = this.leftMargin
  ){
    service.writeImage(imageMetadatal, imgDatal, docl, leftMargin, 40);

    service.writeTitleAndCopyTypeCL(copyTypeTextl, titleTextl, docl);

    service.writeImage(certiImageMetadatal, certiImgDatal, docl, 470, 40);

    // service.writeCompanyAndAddressCL(companyl.tagLine, companyl.name, companyl.address, companyl.email, companyl.website, companyl.primaryTelephone, companyl.primaryMobile, companyl.gstNumber, companyl.stateName, companyl.stateCode, docl);
    service.writeCompanyAndAddressCL(transaction.companyTagLine, transaction.companyName, transaction.companyAddress, transaction.companyEmail, transaction.companyWebsite, transaction.companyPrimaryTelephone, transaction.companyPrimaryMobile, transaction.companyGstNumber, transaction.companyStateName, transaction.companyStateId, docl);

    // this.printService.writeCompanyPhoneAndMobileNumber(this.company.primaryTelephone, this.company.primaryMobile, this.doc);


    // this.writeTitleAndCopyType(this.copyTypeText, this.titleText);


    // //Company And address in the header 
    // this.writeCompanyAndAddress();
    //service.writeJustTransNo(docl, transactionNumber); 


    service.drawLine(docl);
  }

  topSection(docl: any
    , companyl: Company
    , imageMetadatal: ImageMetadata
    , imgDatal: string
    , copyTypeTextl: string
    , titleTextl: string
    , transaction: any
    , service: any
    , transactionNumber: string
    , transactionDate: string
    , transactionName: string
    ) {
      
      
      let transactionNames =transactionName;
    // service.writeCompanyGST(companyl.gstNumber, docl);
    service.writeImage(imageMetadatal, imgDatal, docl);

    //Set Title and copy type
    //this.doc.setDrawColor(215, 235, 252);
    service.writeTitleAndCopyType(copyTypeTextl, titleTextl, docl);

    let transactionNumberHeader: string = ""
    let transactionDateHeader: string =""

    if(transactionName === AppSettings.CUSTOMER_INVOICE||transactionName===AppSettings.PROFORMA_INVOICE||transactionName===AppSettings.INCOMING_JOBWORK_INVOICE){
      transactionNumberHeader = AppSettings.INV_NAME
      transactionDateHeader = AppSettings.INV_DATE
    }
    if(transactionName ===AppSettings.CREDIT_NOTE){
      transactionNumberHeader = AppSettings.CREDIT_NAME
      transactionDateHeader = AppSettings.CREDIT_DATE
    }
    if(transactionName ===AppSettings.DEBIT_NOTE){
      transactionNumberHeader = AppSettings.DEBIT_NAME
      transactionDateHeader = AppSettings.DEBIT_DATE
    }
  
    if(transactionName===AppSettings.CUSTOMER_PO||transactionName===AppSettings.SUPPLIER_PO||transactionName===AppSettings.OUTGOING_JOBWORK_PO||transactionName===AppSettings.INCOMING_JOBWORK_PO){
      transactionNumberHeader = AppSettings.PO_NAME
      transactionDateHeader = AppSettings.PO_DATE
     
     }
     if(transactionName===AppSettings.CUSTOMER_QUOTATION){
      transactionNumberHeader = AppSettings.QUOT_NAME
      transactionDateHeader = AppSettings.QUOT_DATE
     
     }
     if(transactionName===AppSettings.CUSTOMER_DC||transactionName===AppSettings.INCOMING_JOBWORK_OUT_DC||transactionName===AppSettings.OUTGOING_JOBWORK_OUT_DC){
      transactionNumberHeader =AppSettings.DC_NAME
      transactionDateHeader = AppSettings.DC_DATE
     }
     if(transactionName===AppSettings.GRN_TYPE_SUPPLIER){
      transactionNumberHeader = AppSettings.GRN_NAME
      transactionDateHeader = AppSettings.GRN_DATE
     }
     if(transactionName===AppSettings.SUPPLIER_PAYABLE){
      transactionNumberHeader = AppSettings.PAY_NAME
      transactionDateHeader = AppSettings.PAY_DATE
     }

     if(transactionName===AppSettings.CUSTOMER_RECEIVABLE){
      transactionNumberHeader = AppSettings.REC_NAME
      transactionDateHeader = AppSettings.REC_DATE
     }

    service.writeTransactionHeader(transactionNumber, transactionDate, docl, transactionNumberHeader, transactionDateHeader);

    //Company And address in the header 
    // service.writeCompanyAndAddress(companyl.tagLine, companyl.name, companyl.address, companyl.email, companyl.website, companyl.primaryTelephone, companyl.primaryMobile, companyl.gstNumber, companyl.stateName, companyl.stateCode, docl);
    service.writeCompanyAndAddress(transaction.companyTagLine, transaction.companyName, transaction.companyAddress, transaction.companyEmail, transaction.companyWebsite, transaction.companyPrimaryTelephone, transaction.companyPrimaryMobile, transaction.companyGstNumber, transaction.companyStateName, transaction.companyStateId,transaction.companySecondaryMobile,docl);

    // this.printService.writeCompanyPhoneAndMobileNumber(this.company.primaryTelephone, this.company.primaryMobile, this.doc);


    // this.writeTitleAndCopyType(this.copyTypeText, this.titleText);


    // //Company And address in the header 
    // this.writeCompanyAndAddress();

    service.drawLine(docl);
  }

  writeJustTransNo(doc:any, transactionNumber: string,ttype:string){
    //console.log("in writeJustTransNo "+transactionNumber);
    
    let y: number = doc.autoTable.previous.finalY - 50;
    //console.log("y "+y);
    doc.setFontSize(9);
    doc.setFontStyle('bold');
    doc.text(ttype+" "+transactionNumber, 442, y);
    

  }
  writeJustTransDate(doc:any, transactionDate:string,transactionDateLabel:string){
 let datePipe = new DatePipe('en-US');
    //console.log("in writeJustTransNo "+transactionNumber);
 
    let y: number = doc.autoTable.previous.finalY - 40;
    //console.log("y "+y);
    doc.setFontSize(9);
    doc.setFontStyle('bold');
    doc.text(transactionDateLabel+" "+datePipe.transform(transactionDate, 'dd/MM/yyyy'), 442, y);
  }
  topSectionWithoutCompany(docl: any
    , companyl: Company
    , imageMetadatal: ImageMetadata
    , imgDatal: string
    , copyTypeTextl: string
    , titleTextl: string
    , transaction: any
    , service: any
    , transactionNumber: string
    , transactionDate: string
    , transactionName: string) {
    // service.writeCompanyGST(companyl.gstNumber, docl);
    //console.log("service",service);
    service.writeImage(imageMetadatal, imgDatal, docl);
    //console.log("topSectionWithoutCompany 1");
    //Set Title and copy type
    //this.doc.setDrawColor(215, 235, 252);
    service.writeTitleAndCopyType(copyTypeTextl, titleTextl, docl);
    //console.log("topSectionWithoutCompany ")
    let transactionNames=transactionName;
    let transactionNumberHeader: string = ""
    let transactionDateHeader: string =""

    if(transactionName === AppSettings.CUSTOMER_INVOICE||transactionName===AppSettings.PROFORMA_INVOICE||transactionName===AppSettings.INCOMING_JOBWORK_INVOICE){
      transactionNumberHeader = AppSettings.INV_NAME
      transactionDateHeader = AppSettings.INV_DATE
    }
    if(transactionName ===AppSettings.CREDIT_NOTE){
      transactionNumberHeader = AppSettings.CREDIT_NAME
      transactionDateHeader = AppSettings.CREDIT_DATE
    }
    if(transactionName ===AppSettings.DEBIT_NOTE){
      transactionNumberHeader = AppSettings.DEBIT_NAME
      transactionDateHeader = AppSettings.DEBIT_DATE
    }
  
    if(transactionName===AppSettings.CUSTOMER_PO||transactionName===AppSettings.SUPPLIER_PO||transactionName===AppSettings.OUTGOING_JOBWORK_PO||transactionName===AppSettings.INCOMING_JOBWORK_PO){
      transactionNumberHeader = AppSettings.PO_NAME
      transactionDateHeader = AppSettings.PO_DATE
     
     }
     if(transactionName===AppSettings.CUSTOMER_QUOTATION){
      transactionNumberHeader = AppSettings.QUOT_NAME
      transactionDateHeader = AppSettings.QUOT_DATE
     
     }
     if(transactionName===AppSettings.CUSTOMER_DC||transactionName===AppSettings.INCOMING_JOBWORK_OUT_DC||transactionName===AppSettings.OUTGOING_JOBWORK_OUT_DC){
      transactionNumberHeader =AppSettings.DC_NAME
      transactionDateHeader = AppSettings.DC_DATE
     }
     if(transactionName===AppSettings.GRN_TYPE_SUPPLIER){
      transactionNumberHeader = AppSettings.GRN_NAME
      transactionDateHeader = AppSettings.GRN_DATE
     }
     if(transactionName===AppSettings.SUPPLIER_PAYABLE){
      transactionNumberHeader = AppSettings.PAY_NAME
      transactionDateHeader = AppSettings.PAY_DATE
     }

     if(transactionName===AppSettings.CUSTOMER_RECEIVABLE){
      transactionNumberHeader = AppSettings.REC_NAME
      transactionDateHeader = AppSettings.REC_DATE
     }


    service.writeTransactionHeader(transactionNumber, transactionDate, docl, transactionNumberHeader, transactionDateHeader);

    // service.writeTransactionHeader(transactionNumber, transactionDate, docl);    
    //console.log("topSectionWithoutCompany 3")
  }

  

  writeTransactionItems(doc: any
    , company: Company
    , imageMetadata: ImageMetadata
    , imgData: string
    , copyTypeText: string
    , titleText: string
    , transaction: any
    , columnHeaders: any
    , columnData: any
    , afterTop: number
    , lastTableY: number
    , transactionNumber: string
    , transactionDate: string
    , transactionName:string
    , isItemLevelTax: boolean = true
    , certMetadata: ImageMetadata = null
    , certData: string = null
    , isCertiLogo: boolean = false
    , isTemplate: boolean = false
    ) {
    var pageWidth: number = doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    var slNoWidth: number = pageWidth * 0.04; //5%
    // var partNumberWidth: number = pageWidth * 0.07; //10%
    var materialNameWidth: number = pageWidth * 0.19; //30% of total width
    var hsnWidth: number = pageWidth * 0.12; //6%
    var qtyWidth: number = pageWidth * 0.08; //6%
    //var uomWidth: number = pageWidth * 0.05; //6%
    var priceWidth: number = pageWidth * 0.09; // 8%
    var amountWidth: number = pageWidth * 0.1; //8%
    var discountWidth: number = pageWidth * 0.08; //5%
    var cgstAmountWidth: number = pageWidth * 0.1; //8%
    //var sgstPercentWidth: number = pageWidth * 0.05; //5%
    var sgstAmountWidth: number = pageWidth * 0.1; //8%
    var totalAmountWidth: number = pageWidth * 0.1; //10%

    if(!isItemLevelTax){
    

      slNoWidth = pageWidth * 0.10; //5%
      materialNameWidth = pageWidth * 0.45; //30% of total width
      hsnWidth = pageWidth * 0.13; //6%
      qtyWidth = pageWidth * 0.10; //6%
      priceWidth = pageWidth * 0.10; // 8%
      amountWidth = pageWidth * 0.10; //8%
      discountWidth = pageWidth * 0; //5%
      cgstAmountWidth = pageWidth * 0; //8%
      sgstAmountWidth = pageWidth * 0; //8%
      totalAmountWidth = pageWidth * 0; //10%
    }
    //console.log('page width: ', doc.internal.pageSize.width);

    // var pageContent = function (data) {

    //     // FOOTER
    //     if (this.doc) {
    //         var str = "Page " + data.pageCount;
    //         if (typeof this.doc.putTotalPages === 'function') {
    //             str = str + " of " + this.totalPagesExp;
    //         }
    //         this.doc.setFontSize(10);
    //         this.doc.text(str, data.settings.margin.left, this.doc.internal.pageSize.height - 10);

    //     }
    // };

    var docl = doc;
    var leftMarginl = this.leftMargin
    var topSectionl = this.topSectionWithoutCompany;
    var topSectionCL = this.topSectionWithCertiImage;
    var writeJustTransNo = this.writeJustTransNo;
    var writeJustTransDate =this.writeJustTransDate;
    //var printServicel = this.printService;

    var companyl = company;
    var imageMetadatal = imageMetadata;
    var imgDatal = imgData;
    var copyTypeTextl = copyTypeText;
    var titleTextl = titleText;
    var transactionl = transaction;
    var thisService = this;
    var transactionName =transactionName

    var pageContent = function (data) {
      // HEADER
      // docl.setFontSize(20);
      // docl.setTextColor(40);
      // docl.setFontStyle('normal');

      // docl.text("From Autotable", leftMarginl, 10);
      //console.log('data: ', data);
      if (data.pageNumber > 1 && !isTemplate) {
        if(!isCertiLogo){
          topSectionl(docl
            , companyl
            , imageMetadatal
            , imgDatal
            , copyTypeTextl
            , titleTextl
            , transactionl
            , thisService
            , transactionNumber
            , transactionDate
             ,transactionName);
        }
        else{
          topSectionCL(
            docl
            , companyl
            , imageMetadatal
            , imgDatal
            , certMetadata
            , certData
            , copyTypeTextl
            , titleTextl
            , transactionl
            , thisService
            , transactionNumber
            , transactionDate
            , leftMarginl
          )
        }
      }

      if(data.pageNumber > 1 &&  isCertiLogo){
       writeJustTransNo(docl, transactionNumber,"Trans Num :");
        writeJustTransDate(docl, transactionDate,"Trans Date :");
      }
     

      // FOOTER
      // var str = "Page " + data.pageCount;
      // // Total page number plugin only available in jspdf v1.0+
      // if (typeof docl.putTotalPages === 'function') {
      //   str = str + " of " + totalPagesExp;
      // }
      // docl.setFontSize(10);
      // docl.text(str, data.settings.margin.left, docl.internal.pageSize.height - 10);
    };

    console.log(lastTableY+"lastTableY)")
    docl.autoTable(columnHeaders, columnData, {
      //addPageContent: pageContent,
      // startY: this.custTableLastY + 95,
     
      didDrawPage: pageContent,
      startY: lastTableY,
      margin: { left: this.leftMargin, right: this.rightMargin + 5, top: afterTop, bottom: this.bottomMargin },
      //theme:'grid',
      theme: 'striped',
      tableWidth: 'auto',
      //tableLineColor: [0,0,0],
      //tableLineWidth: 0.5,
      styles: {
        // cellPadding: 0.5,
        fontSize: 8,
        lineColor: [0, 0, 0],
        fillStyle: 'DF',
        //halign: 'right',
        overflow: 'linebreak',
        overflowColumns: false
      },
      headStyles: {
        // fillColor: [241, 241, 241],
        fillColor: [63, 81, 181],
        lineColor: [0, 0, 0],
        fontSize: 9,
        // textColor: 0,
        textColor: [255, 255, 255],
        fontStyle: 'bold',
        halign: 'center',
      },
      columnStyles: {
        slNo: { cellWidth: slNoWidth, halign: 'left' },
        // partNumber: { cellWidth: partNumberWidth },
        materialName: { cellWidth: materialNameWidth },
        hsnCode: { cellWidth: hsnWidth },
        quantity: { cellWidth: qtyWidth, halign: 'right' },
        // uom: { cellWidth: uomWidth },
        price: { cellWidth: priceWidth, halign: 'right' },
        amount: { cellWidth: amountWidth, halign: 'right' },
        discountAmount: { cellWidth: discountWidth, halign: 'right' },
        cgstAmount: { cellWidth: cgstAmountWidth, halign: 'right' },
        // sgstPercent: { cellWidth: sgstPercentWidth, halign: 'right' },
        sgstAmount: { cellWidth: sgstAmountWidth, halign: 'right' },
        totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

      },

      // didDrawPage: function(data) {

      //     //console.log('in drawPage: ', data);
      //     // data.doc.addPage();
      //     super.topSection();

      // }
      // drawCell: function (cell, data) {
      //   //console.log('cell before:: ', cell);
      //   if (data.column.index % 2 === 1) {
      //     cell.styles.fillColor = "[215, 235, 252]";
      //   }
      //   //console.log('cell:: ', cell);
      // },
    });

    // this.doc.setLineWidth(1);
    // this.doc.setDrawColor(215, 235, 252);
    //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

    //console.log('this.doc.autoTable.previous ', docl.autoTable.previous);
    // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
    // Total page number plugin only available in jspdf v1.0+
    if (typeof docl.putTotalPages === 'function') {
      docl.putTotalPages(totalPagesExp);
    }



  }

  

  writeTransactionItemsIGST(
    doc: any
    , company: Company
    , imageMetadata: ImageMetadata
    , imgData: string
    , copyTypeText: string
    , titleText: string
    , transaction: any
    , columnHeaders: any
    , columnData: any
    , afterTop: number
    , lastTableY: number
    , transactionNumber: string
    , transactionDate: string
    , transactionName:string
    , isItemLevelTax: boolean = true
    , certMetadata: ImageMetadata = null
    , certData: string = null
    , isCertLogo: boolean = false
    , isTemplate: boolean = false
  ) {

    var pageWidth: number = doc.internal.pageSize.width - this.leftMargin - this.rightMargin;
    var slNoWidth: number = pageWidth * 0.05; //5%
    // var partNumberWidth: number = pageWidth * 0.08; //10%
    var materialNameWidth: number = pageWidth * 0.30; //30% of total width
    var hsnWidth: number = pageWidth * 0.13; //6%
    var qtyWidth: number = pageWidth * 0.07; //6%
    // var uomWidth: number = pageWidth * 0.05; //6%
    var priceWidth: number = pageWidth * 0.09; // 8%
    var amountWidth: number = pageWidth * 0.09; //8%
    var discountWidth: number = pageWidth * 0.08; //5%
    var igstAmountWidth: number = pageWidth * 0.09; //8%
    //var sgstPercentWidth: number = pageWidth * 0.05; //5%
    //var sgstAmountWidth: number = pageWidth * 0.09; //8%
    var totalAmountWidth: number = pageWidth * 0.1; //10%


    if(!isItemLevelTax){
      slNoWidth = pageWidth * 0.10; //5%
      materialNameWidth = pageWidth * 0.45; //30% of total width
      hsnWidth = pageWidth * 0.13; //6%
      qtyWidth = pageWidth * 0.10; //6%
      priceWidth = pageWidth * 0.10; // 8%
      amountWidth = pageWidth * 0.10; //8%
      discountWidth = 0; //5%
      igstAmountWidth = 0; //8%
      totalAmountWidth = 0; //10%
    }

    //console.log('page width: ', doc.internal.pageSize.width);



    var docl = doc;
    var leftMarginl = this.leftMargin
    var topSectionl = this.topSectionWithoutCompany;
    var topSectionCL = this.topSectionWithCertiImage;
    // var printServicel = this.printService;
    var writeJustTransNo = this.writeJustTransNo;
    var  writeJustTransDate =this.writeJustTransDate;
    var companyl = company;
    var imageMetadatal = imageMetadata;
    var imgDatal = imgData;
    var copyTypeTextl = copyTypeText;
    var titleTextl = titleText;
    var transactionl = transaction;
    var thisService = this;
    var transactionName =transactionName;
    var pageContent = function (data) {
      // HEADER
      // docl.setFontSize(20);
      // docl.setTextColor(40);
      // docl.setFontStyle('normal');

      // docl.text("From Autotable", leftMarginl, 10);
      //console.log('data: ', data);
      if (data.pageNumber > 1 && !isTemplate) {
        if(!isCertLogo){
          topSectionl(docl
            , companyl
            , imageMetadatal
            , imgDatal
            , copyTypeTextl
            , titleTextl
            , transactionl
            , thisService
            , transactionNumber
            , transactionDate
            , transactionName);
        }else{
          topSectionCL(
            docl
            , companyl
            , imageMetadatal
            , imgDatal
            , certMetadata
            , certData
            , copyTypeTextl
            , titleTextl
            , transactionl
            , thisService
            , transactionNumber
            , transactionDate
            
            , leftMarginl
          )
        }
      }
     
      if(data.pageNumber > 1 && isCertLogo){
        writeJustTransNo(docl, transactionNumber,"Trans:");
        writeJustTransDate(docl, transactionDate,"Trans Date:");
      }
      // FOOTER
      // var str = "Page " + data.pageCount;
      // // Total page number plugin only available in jspdf v1.0+
      // if (typeof docl.putTotalPages === 'function') {
      //     str = str + " of " + totalPagesExp;
      // }
      // docl.setFontSize(10);
      // docl.text(str, data.settings.margin.left, docl.internal.pageSize.height - 10);
    };


    doc.autoTable(columnHeaders, columnData, {
      //addPageContent: pageContent,
      didDrawPage: pageContent,
      startY: lastTableY,
      margin: { left: this.leftMargin, right: this.rightMargin + 5, top: afterTop, bottom: 100 },
      //theme:'grid',
      theme: 'striped',
      tableWidth: 'auto',
      //tableLineColor: [0,0,0],
      //tableLineWidth: 0.5,
      styles: {
        // cellPadding: 0.5,
        fontSize: 8,
        lineColor: [0, 0, 0],
        fillStyle: 'DF',
        //halign: 'right',
        overflow: 'linebreak',
        overflowColumns: false,
        // cellPadding: 0,
      },
      headStyles: {
        fillColor: [63, 81, 181],
        lineColor: [0, 0, 0],
        fontSize: 9,
        textColor: [255, 255, 255],
        fontStyle: 'normal',
        halign: 'center',
      },
      columnStyles: {
        slNo: { cellWidth: slNoWidth, halign: 'left' },
        // partNumber: { cellWidth: partNumberWidth },
        materialName: { cellWidth: materialNameWidth },
        hsnCode: { cellWidth: hsnWidth },
        quantity: { cellWidth: qtyWidth, halign: 'right' },
        // uom: { cellWidth: uomWidth },
        price: { cellWidth: priceWidth, halign: 'right' },
        amount: { cellWidth: amountWidth, halign: 'right' },
        discountAmount: { cellWidth: discountWidth, halign: 'right' },
        igstAmount: { cellWidth: igstAmountWidth, halign: 'right' },
        totalAmount: { cellWidth: totalAmountWidth, halign: 'right' }

      }
      // drawCell: function (cell, data) {
      //   //console.log('cell before:: ', cell);
      //   if (data.column.index % 2 === 1) {
      //     cell.styles.fillColor = "[215, 235, 252]";
      //   }
      //   //console.log('cell:: ', cell);
      // },
    });

    // this.doc.setLineWidth(1);
    // this.doc.setDrawColor(215, 235, 252);
    //this.doc.line(this.leftMargin, this.doc.autoTable.previous.finalY, (this.doc.internal.pageSize.width - this.rightMargin), this.doc.autoTable.previous.finalY);

    //console.log('this.doc.autoTable.previous ', doc.autoTable.previous);
    // this.doc.rect(this.leftMargin, this.doc.autoTable.previous.pageStartY, (this.doc.internal.pageSize.width - this.rightMargin - this.leftMargin), (this.doc.autoTable.previous.height + 20));
    // Total page number plugin only available in jspdf v1.0+
    // if (typeof doc.putTotalPages === 'function') {
    //     doc.putTotalPages(totalPagesExp);
    // }



  }


  writeFooter(pdfObject, copyText: string) {
    var number_of_pages = pdfObject.internal.getNumberOfPages()
    number_of_pages = (copyText === AppSettings.PRINT_ALL) ? number_of_pages/3 : number_of_pages;
    var pdf_pages = pdfObject.internal.pages
    // var myFooter = "Footer info"
    let j = 1;
    for (let i = 1; i < pdf_pages.length; i++) {
        // We are telling our pdfObject that we are now working on this page
        
        
        pdfObject.setPage(i)
        // The 10,200 value is only for A4 landscape. You need to define your own for other page sizes
        let footer = j + " of " + number_of_pages;
        pdfObject.text(footer, this.leftMargin, pdfObject.internal.pageSize.height - 10)

        if(copyText === AppSettings.PRINT_ALL ){
          if(j === number_of_pages ){
            j = 1;
          }else {
            j++;
          }
        }else{
          j++;
        }
    }
  }

  getIsItemLevelTax(isItemLevelTax: boolean, invoice: InvoiceHeader, transactionTypeName:string): boolean{

    let outIsItemLevelTax: boolean = false;

    if (isItemLevelTax) {
      outIsItemLevelTax =  true;
    } else {
      outIsItemLevelTax = false;
    }

    if(transactionTypeName === AppSettings.INCOMING_JOBWORK_INVOICE
      || transactionTypeName === AppSettings.OUTGOING_JOBWORK_INVOICE){

        let taxes = invoice.invoiceItems
        .map(item => item.igstTaxPercentage > 0 ? item.igstTaxPercentage : item.cgstTaxPercentage)
        
        // console.log(taxes);
        let distinctTaxes: number[] = Array.from(new Set(taxes))
        // console.log(distinctTaxes);

        let discounts = invoice.invoiceItems
        .map(item => item.discountPercentage)
        
        // console.log(discounts);
        let distinctDiscounts: number[] = Array.from(new Set(discounts))
        // console.log(distinctTaxes);

        if(distinctTaxes.length > 1 || distinctDiscounts.length > 1){
          outIsItemLevelTax = true;
        }else{
          outIsItemLevelTax = false;
        }

    }

    return outIsItemLevelTax;
  }

}