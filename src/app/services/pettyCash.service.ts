import { Injectable } from "@angular/core";

import { Observable } from "rxjs";

import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Category, Tag, PettyCashWrapper, PettyCashHeader, RecentPettyCash } from "../data-model/petty-cash-model";
import { TransactionResponse } from "../data-model/transaction-response";

@Injectable()
export class PettyCashService {
   categoryApiUrl:string;
   tagApiUrl:string;
   allCategoryApiUrl:string;
   allTagApiUrl:string;
   checkpettyCashNumberApiUrl:string;
   apiPostUrl: string;
   apiDeleteUrl:string;
   recentPettyCashApiUrl:string;
   apiApproveUrl: string;
   apiLatestUrl:string;
    private headers = new Headers({ 'Content-Type': 'application/json' });


    constructor(private http: HttpClient) {
        this.categoryApiUrl = environment.baseServiceUrl+ environment.categoryApiUrl;
        this.tagApiUrl = environment.baseServiceUrl+ environment.tagApiUrl;
        this.allCategoryApiUrl = environment.baseServiceUrl + environment.allCategoryApiUrl;
        this.allTagApiUrl = environment.baseServiceUrl + environment.allTagApiUrl;
        this.checkpettyCashNumberApiUrl = environment.baseServiceUrl + environment.checkpettyCashNumberApiUrl;
        this.apiPostUrl = environment.baseServiceUrl + environment.pettyCashPostApiUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.pettyCashApiGetUrl;
        this.recentPettyCashApiUrl = environment.baseServiceUrl + environment.recentPettyCashApiUrl;
        this.apiApproveUrl = environment.baseServiceUrl + environment.approveApiUrl;
        this.apiLatestUrl = environment.baseServiceUrl + environment.latestApiUrl;
    }
    saveCategory(categorys: Category[]): Observable<Category[]> {
        
        return this.http
            .post<Category[]>(this.categoryApiUrl, categorys)
       


    }

    saveTag(tags: Tag[]): Observable<Tag[]> {
        
        return this.http
            .post<Tag[]>(this.tagApiUrl, tags)
       


    }

    getAllCategory(): Observable<Category[]> {
        return this.http
            .get<Category[]>(this.allCategoryApiUrl)
    


    }

    getAllTags(): Observable<Tag[]>{
        return this.http
            .get<Tag[]>(this.allTagApiUrl)
    }

    checkPettyCashNumAvailability(pettyCashNumber, tTyp): Observable<TransactionResponse> {
    

        let params = new HttpParams();
       
        params = params.append("pettyCashNumber", pettyCashNumber);

        return this.http.get<TransactionResponse>(this.checkpettyCashNumberApiUrl + "/" +tTyp, {params: params})

    }

    create(pettyCashWrapper: PettyCashWrapper): Observable<PettyCashHeader> {

       
        return this.http.post<PettyCashHeader>(this.apiPostUrl, pettyCashWrapper)
       
    }
    delete(id: string): Observable<TransactionResponse> {
       
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        
    }


    getRecentPettyCash(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPettyCash> {
        sortColumn = sortColumn ? sortColumn : 'voucherDate';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

     

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        //console.log("filterText: ", filterText, transactionTypeId , this.recentVoucherApiUrl );

        return this.http
            .get<RecentPettyCash>(this.recentPettyCashApiUrl + "/" + transactionTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }



    approvePettyCash(pettyCashHeaderId: String): Observable<PettyCashHeader> {

       
        return this.http.post<PettyCashHeader>(this.apiApproveUrl , pettyCashHeaderId)
       
    }

    getLatestTransaction(): Observable<PettyCashHeader> {

      
        return this.http.get<PettyCashHeader>(this.apiLatestUrl)
       
    }


}