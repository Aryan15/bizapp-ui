import { Injectable } from '@angular/core';
import { InvoiceHeader } from '../data-model/invoice-model';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { QuotationHeader } from '../data-model/quotation-model';
import { PurchaseOrderHeader } from '../data-model/purchase-order-model';
import { DeliveryChallanHeader } from '../data-model/delivery-challan-model';
import { GrnHeader } from '../data-model/grn-model';

@Injectable({
  providedIn: 'root'
})
export class TransCommService {

 private invoiceHeader: BehaviorSubject<InvoiceHeader> = new BehaviorSubject<InvoiceHeader>(null);

  invoiceData: Observable<InvoiceHeader> = this.invoiceHeader.asObservable();

 private quotationHeader :BehaviorSubject<QuotationHeader> = new BehaviorSubject<QuotationHeader>(null);

  quotationData : Observable<QuotationHeader> = this.quotationHeader.asObservable();
  
 private poHeader:BehaviorSubject<PurchaseOrderHeader> = new BehaviorSubject<PurchaseOrderHeader>(null);
  
  poData : Observable<PurchaseOrderHeader> = this.poHeader.asObservable();

  // private dcHeader = new BehaviorSubject<DeliveryChallanHeader>(null);
  private dcHeader:BehaviorSubject<DeliveryChallanHeader> = new BehaviorSubject<DeliveryChallanHeader>(null);
  
  dcData: Observable<DeliveryChallanHeader> = this.dcHeader.asObservable();

  private grnHeader: BehaviorSubject<GrnHeader> = new BehaviorSubject<GrnHeader>(null);
  
  grnData : Observable<GrnHeader> = this.grnHeader.asObservable();

  replicatedInvoiceHeader = new BehaviorSubject<InvoiceHeader>(null);
  
  replicatedinvoiceData = this.replicatedInvoiceHeader.asObservable();
  // private replicatedInvoiceHeader: BehaviorSubject<InvoiceHeader>;
  
  // replicatedinvoiceData : Observable<InvoiceHeader>;

  constructor() { 
    
    // this.dcData = this.dcHeader.asObservable();
  }

  initDcData(){
    console.log("initDcData()");
    this.dcHeader = new BehaviorSubject<DeliveryChallanHeader>(null);
    this.dcData = this.dcHeader.asObservable();
  }

  updateDcData(inDcData: DeliveryChallanHeader){
    this.dcHeader.next(inDcData);
  }

  completeDcData(){
    this.dcHeader.complete();
  }


  initPoData(){
    console.log("initPoData()");
    this.poHeader = new BehaviorSubject<PurchaseOrderHeader>(null);
    this.poData = this.poHeader.asObservable();
  }

  updatePoData(inPoData: PurchaseOrderHeader){
    this.poHeader.next(inPoData);
  }

  completePoData(){
    this.poHeader.complete();
  }


  initGrnData(){
    console.log("initgrnData()");
    this.grnHeader = new BehaviorSubject<GrnHeader>(null);
    this.grnData = this.grnHeader.asObservable();
  }

  updateGrnData(inGrnData: GrnHeader){
    this.grnHeader.next(inGrnData);
  }

  completeGrnData(){
    this.grnHeader.complete();
  }

  initQuotationData(){ 
    console.log("initquotationData()");
    this.quotationHeader = new BehaviorSubject<QuotationHeader>(null);
    this.quotationData = this.quotationHeader.asObservable();
  }

  updateQuotationData(inQuotationData: QuotationHeader){
    this.quotationHeader.next(inQuotationData);
  }

  completeQuotationData(){
    this.quotationHeader.complete();
  }
  initInvoiceData(){ 
    console.log("initinvoiceData()");
    this.invoiceHeader = new BehaviorSubject<InvoiceHeader>(null);
    this.invoiceData = this.invoiceHeader.asObservable();
  }

  updateInvoiceData(inInvoiceData: InvoiceHeader){
    this.invoiceHeader.next(inInvoiceData);
  }

  completeInvoiceData(){
    this.invoiceHeader.complete();
  }


  initRInvoiceData(){ 
    console.log("initinvoiceData()");
    this.invoiceHeader = new BehaviorSubject<InvoiceHeader>(null);
    this.invoiceData = this.invoiceHeader.asObservable();
  }

  updateRInvoiceData(inInvoiceData: InvoiceHeader){
    this.invoiceHeader.next(inInvoiceData);
  }

  completeRInvoiceData(){
    this.invoiceHeader.complete();
  }








  initPJInvoiceData(){ 
    console.log("initinvoiceData()");
    this.invoiceHeader = new BehaviorSubject<InvoiceHeader>(null);
    this.invoiceData = this.invoiceHeader.asObservable();
  }

  updatePJInvoiceData(inInvoiceData: InvoiceHeader){
    this.invoiceHeader.next(inInvoiceData);
  }

  // pushInvoice(inData: InvoiceHeader)  {
  //   //console.log("inside push Invoice", inData)
  //   this.invoiceHeader.next(inData)    
  //   this.invoiceHeader.complete();
  // }

  // pushQuotation(inData: QuotationHeader)  {
  //   //console.log("inside push Invoice", inData)
  //   this.quotationHeader.next(inData)    
  // }
  
}
