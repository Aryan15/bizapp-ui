import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { DashboardTransactionSummary } from "../data-model/dashboard-transaction-summary-model";
import { TransactionResponse } from "../data-model/transaction-response";

@Injectable({
    providedIn: 'root'
  })
  export class DashboardReportService {
    
    getInvoiceWithPaymentsApiUrl: string;
    getPostDatedChequesApiUrl: string;
    getOpenQuotationApiUrl: string;
    getOpenPOApiUrl: string;
    getPayReceiveApiUrl: string;
    getTransactionType:string;
    localInvoices: DashboardTransactionSummary[] = [];
    localPayReceives: DashboardTransactionSummary[] = [];
    localQuotations: DashboardTransactionSummary[] = [];
    localPOs: DashboardTransactionSummary[] = [];

    constructor(private http: HttpClient) {
        this.getInvoiceWithPaymentsApiUrl = environment.baseServiceUrl + environment.getInvoiceWithPaymentsApiUrl;
        this.getPostDatedChequesApiUrl = environment.baseServiceUrl + environment.getPostDatedChequesApiUrl;
        this.getOpenQuotationApiUrl = environment.baseServiceUrl + environment.getOpenQuotationApiUrl;
        this.getOpenPOApiUrl = environment.baseServiceUrl + environment.getOpenPOApiUrl;
        this.getPayReceiveApiUrl = environment.baseServiceUrl + environment.getPayReceiveApiUrl;
        this.getTransactionType = environment.baseServiceUrl + environment.apiGetJobWorkTrnsactionApiUrl;
    }


    getInvoiceWithPayments(monthCount: number): Observable<DashboardTransactionSummary[]> {
        return this.http
            .get<DashboardTransactionSummary[]>(this.getInvoiceWithPaymentsApiUrl + "/" + monthCount)
            
    }

    getPayReceive(monthCount: number,sourceTransactionType:string): Observable<DashboardTransactionSummary[]> {
        return this.http
            .get<DashboardTransactionSummary[]>(this.getPayReceiveApiUrl + "/" + monthCount + "/" +sourceTransactionType)
            
    }

    getPostDatedCheques(): Observable<DashboardTransactionSummary[]> {
        return this.http
            .get<DashboardTransactionSummary[]>(this.getPostDatedChequesApiUrl)
            
    }

    getOpenQuotations(monthCount: number): Observable<DashboardTransactionSummary[]> {
        return this.http
            .get<DashboardTransactionSummary[]>(this.getOpenQuotationApiUrl + "/" + monthCount)
            
    }

    getOpenPurchaseOrders(monthCount: number): Observable<DashboardTransactionSummary[]> {
        return this.http
            .get<DashboardTransactionSummary[]>(this.getOpenPOApiUrl + "/" + monthCount)
            
    }

    getJwTransactionType(): Observable<TransactionResponse>{
        return this.http
        .get<TransactionResponse>(this.getTransactionType)

    }


  }