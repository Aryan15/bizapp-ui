import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { TransactionReportRequest } from '../data-model/transaction-report-request-model';
import { TransactionResponse } from '../data-model/transaction-response';
import { ExpenseHeader, RecentVoucher, VoucherHeader, VoucherReport, VoucherReportModel, VoucherWrapper } from '../data-model/voucher-model';
import { Page } from '../reports/model/page';
import { PagedData } from '../reports/model/paged-data';

@Injectable()
export class VoucherService {


    apiGetAllUrl: string;
    apiPostUrl: string;
    apiGetUrl: string;
    apiGetVouchersForParty: string;
    expensesApiUrl: string;
    apiGetForVoucherNumberUrl: string;
    apiDeleteUrl: string
    expenseApiUrl: string;
    recentVoucherApiUrl: string;
    checkvoucherNumberApiUrl: string;
    voucherReportApiUrl: string;
    expenseStatusApiUrl: string;
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: HttpClient) {
        this.apiGetAllUrl = environment.baseServiceUrl + environment.voucherApiGetAllUrl;//'http://localhost:8081/api/country';
        this.apiPostUrl = environment.baseServiceUrl + environment.voucherApiPostUrl;//'http://localhost:8081/api/country';
        this.apiGetUrl = environment.baseServiceUrl + environment.voucherApiGetUrl;
        this.expensesApiUrl = environment.baseServiceUrl + environment.expensesApiUrl;
        this.apiGetForVoucherNumberUrl = environment.baseServiceUrl + environment.voucherApiGetForVoucherNumberUrl;
        this.apiDeleteUrl = environment.baseServiceUrl + environment.voucherApiGetUrl;
        this.recentVoucherApiUrl = environment.baseServiceUrl + environment.recentVoucherApiUrl;
        this.expenseApiUrl = environment.baseServiceUrl+ environment.expenseApiUrl;
        this.checkvoucherNumberApiUrl = environment.baseServiceUrl + environment.checkvoucherNumberApiUrl;
        this.voucherReportApiUrl = environment.baseServiceUrl + environment.voucherReportApiUrl;
        this.expenseStatusApiUrl = environment.baseServiceUrl + environment.expenseStatusApiUrl
    }



    getVoucher(headerVoucherId: string): Observable<VoucherHeader> {

        return this.http.get<VoucherHeader>(this.apiGetUrl + "/" + headerVoucherId)
        // .map(response => <VoucherHeader>response.json());

    }


    delete(id: string): Observable<TransactionResponse> {
        //console.log("Deleting...", id);
        return this.http.delete<TransactionResponse>(this.apiDeleteUrl + "/" + id)
        // .map(response => <TransactionResponse>response.json());
    }


    create(voucherWrapper: VoucherWrapper): Observable<VoucherHeader> {

        //console.log("In create...");
        return this.http.post<VoucherHeader>(this.apiPostUrl, voucherWrapper)
        // .map(response => <VoucherHeader>response.json());
    }


    getAllExpense(): Observable<ExpenseHeader[]> {
        return this.http
            .get<ExpenseHeader[]>(this.expensesApiUrl)
        // .map(response => <Uom[]>response.json());


    }

    saveExpense(expenseHeader: ExpenseHeader[]): Observable<ExpenseHeader[]> {
        
                return this.http
                    .post<ExpenseHeader[]>(this.expenseApiUrl, expenseHeader)
                // .map(response => <Material>response.json());
        
        
            }

    getRecentVouchers(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentVoucher> {
        sortColumn = sortColumn ? sortColumn : 'voucherDate';
        sortDirection = sortDirection ? sortDirection : 'desc';
        page = page ? page + 1 : 1;
        pageSize = pageSize ? pageSize : 30;
        filterText = filterText ? filterText : 'null';

        // let re = /\//gi //replace back slash globally with %2F for date field
        // filterText = filterText.replace(re, "-");

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("searchText", filterText);

        //console.log("filterText: ", filterText, transactionTypeId , this.recentVoucherApiUrl );

        return this.http
            .get<RecentVoucher>(this.recentVoucherApiUrl + "/" + transactionTypeId + "/" + sortColumn + "/" + sortDirection + "/" + page + "/" + pageSize, {params: params});
    }

    deleteExpense(ids: number[]): Observable<TransactionResponse>{
        return this.http.delete<TransactionResponse>(this.expenseApiUrl+"/"+ids);
    }
    
    checkVoucherNumAvailability(voucherNumber, tTyp): Observable<TransactionResponse> {
        //console.log("voucherNumber in serice", voucherNumber)

        let params = new HttpParams();
        // params = params.append("searchText", encodeURIComponent(filterText));
        params = params.append("voucherNumber", voucherNumber);

        return this.http.get<TransactionResponse>(this.checkvoucherNumberApiUrl + "/" +tTyp, {params: params})

    }


    getVoucherReport(transactionTypeId: number, transactionReportRequest: TransactionReportRequest, page: Page): Observable<PagedData<VoucherReportModel>> {

        let pageNumber = page ? page.pageNumber : -99;
        let pageSize = page ? page.size : -99;
        let pageSortDirection = page ? page.sortDirection : null;
        let pageSortColumn = page ? page.sortColumn : null;

        return this.http.post<VoucherReport>(this.voucherReportApiUrl
            + "/" + transactionTypeId
            + "/" + pageNumber
            + "/" + pageSize +
            "/" + pageSortDirection + "/"
            + pageSortColumn, transactionReportRequest).pipe(
            map(voucherData => this.getPagedData(page, voucherData)));
    }


    private getPagedData(page: Page, voucherData: VoucherReport): PagedData<VoucherReportModel> {

        //console.log("voucherData: ", voucherData);
        let pagedData = new PagedData<VoucherReportModel>();
        if (page) {
            page.totalElements = voucherData.totalCount;
            page.totalPages = page.totalElements / page.size;
        }
        pagedData.page = page;
        pagedData.data = voucherData.voucherReportItems;
        //console.log('voucherData.voucherReportItems: ', voucherData.voucherReportItems);
        //console.log("page ", page);
        return pagedData;
    }

    checkExpenseStatus(ids: number[]): Observable<boolean>{
        return this.http.get<boolean>(this.expenseStatusApiUrl+"/"+ids);
    }

}