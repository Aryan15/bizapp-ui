
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { take } from 'rxjs/internal/operators/take';
import { startWith, takeUntil, map, filter } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { DeliveryChallanHeader, DeliveryChallanItem } from '../../data-model/delivery-challan-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { GrnHeader, GrnItem, GrnWrapper } from '../../data-model/grn-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { Party } from "../../data-model/party-model";
import { Tax } from '../../data-model/tax-model';
import { TermsAndCondition } from '../../data-model/terms-condition-model';
import { TransactionType } from '../../data-model/transaction-type';
import { UserModel } from '../../data-model/user-model';
import { ReferenceLinksComponent } from '../../reference-links/reference-links.component';
import { CompanyService } from '../../services/company.service';
import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GrnService } from '../../services/grn.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { TaxService } from '../../services/tax.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransCommService } from '../../services/trans-comm.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { PrintDialog } from '../../utils/print-dialog';
import { PrintDialogContainer } from '../../utils/print-dialog-container';
import { UtilityService } from '../../utils/utility-service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';

@Component({
    selector: 'grn-form',
    templateUrl: './grn.component.html',
    styleUrls: ['./grn.component.scss']
})
export class GrnComponent implements OnInit, OnDestroy {
    @ViewChild('grnNumber',{static: false}) grnNumberElement: ElementRef;
  
    public grnForm: FormGroup;
    public grnHeader: GrnHeader;

    // subheading : String = "this page is used to record Information about ";


    public parties: Party[] = [];
    public company: Company;
    public QualityCheckUsers: UserModel[] = [];
    public StockCheckUsers: UserModel[] = [];
    public deliveryChallans: DeliveryChallanHeader[] = [];
    public termsAndConditions: TermsAndCondition[] = [];
    statusBasedPermission: StatusBasedPermission;
    isTermsAndcondition: boolean = false;
    numberRangeConfiguration: NumberRangeConfiguration;
    transactionType: TransactionType;
    transactionTypeIncomingDC: TransactionType;
    transactionStatus: string = null;
    saveStatus: boolean = false;
    spinStart: boolean = false;
    isAutoNumber: boolean = false;
    fieldValidatorLabels = new Map<string, string>();
    recentGrnLoadCounter: number = 0;
    datePipe = new DatePipe('en-US');
    selectedParty: Party;
    financialYear: FinancialYear;
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;
    minDate: Date;
    maxDate: Date;
    itemToBeRemove: string[] = [];
    onDestroy : Subject<boolean> = new Subject();
    isCreateEnabled:boolean = false;
    filteredParties: Observable<Party[]>;
    printRef: MatDialogRef<PrintDialog>;
    defaultDate: string;
    isReadonly: boolean = false;
    activityId:number=0;
    displayPartyName:string="";
    showTheCreatedBy=false;
    applicableButtons: ApplicableButtons = {
        isApproveButton: false,
        isClearButton: true,
        isCloseButton: true,
        isCancelButton: false,
        isDeleteButton: true,
        isPrintButton: true,
        isSaveButton: true,
        isEditButton: true,
        isDraftButton: false,
    };
    taxes: Tax[] = [];
    headerTax: Tax;
    statusCheck:number=0;
    activityRoleBindings: ActivityRoleBinding[];
    tempTransactionNumber: string;
    //grnNumberVerified: boolean = false;

    constructor(private fb: FormBuilder,
        private dcService: DeliveryChallanService,
        private partyService: PartyService,
        private userService: UserService,
        private financialYearService: FinancialYearService,
        private termsAndConditionsService: TermsAndConditionsService,
        private transactionTypeService: TransactionTypeService,
        private grnService: GrnService,
        private transactionTypeService_in:TransactionTypeService,
        private numberRangeConfigService: NumberRangeConfigService,
        public utilityService: UtilityService,
        public dialog: MatDialog,
        private alertService: AlertService,
        private _router_in: Router,
        private transCommService:TransCommService,
        private _router: Router,
        public route: ActivatedRoute,
        private taxService: TaxService,        
        private companyService: CompanyService,
        private transactionItemService: TransactionItemService,
        private locationService: LocationService) {
        this.setTransactionType();
        this.setActivityRoleBinding();
    }

    ngOnDestroy() {
        //console.log('ngOnDestory');
        this.onDestroy.next(true);
        this.onDestroy.complete();
    }
    
    getDefaultDate() {
 
        this.utilityService.getCurrentDateObs().subscribe(response => {
            this.defaultDate = response;
            this.getTaxes();
            
           
        })
        
    }
    ngOnInit() {
        
        this.company = this.companyService.getCompany();
        this.getFinancialYear()
        this.getUsers();
        
        this.getallTerms();
        
        
        //  this.initForm();
        //this.createInvoice();
        //  this.handleChanges();
    }

    setActivityRoleBinding() {
        this.userService.setActivityRoleBinding(this.route).subscribe(response => {
            this.activityRoleBindings = response;
            this.activityRoleBindings.forEach(activityRoleBinding => {
                this.activityId=activityRoleBinding.activityId;
                });
        })
    }
    getFinancialYear() {
        this.financialYearService.getFinancialYear().subscribe(
            response => {
                this.financialYear = response;
                this.minDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                this.maxDate = new Date(this.financialYear.endDate);
                //console.log("this.minDate", this.minDate)
                //console.log("this.maxDate", this.maxDate)
            }
        )
    }
    getallTerms() {
        this.termsAndConditionsService.getAllTncs().subscribe(
            response => {
                this.termsAndConditions = response;

                //console.log("this.termsAndConditions", this.termsAndConditions);

                //activate recent GRN list
                this.recentGrnLoadCounter++;
            }
        )
    }
    private initForm() {
        let data: GrnHeader = {
            id: null,
            grnId: null,
            grnNumber: null,
            grnDate: this.defaultDate,//utilityService.getCurrentDate(),
            purchaseOrderHeaderId: null,
            comments: null,
            verbal: null,
            grnStatusId: null,
            companyId: null,
            qualityCheckedById: null,
            stockCheckedById: null,
            financialYearId: null,
            deliveryChallanDate:null,
            supplierId: null,
            deliveryChallanHeaderId: null,
            deliveryChallanNumber: null,
            termsAndConditionNote: null,
            termsAndConditionId: null,
            invoiceHeaderIds: null,
            grnTypeId: null,
            supplierName: null,
            grnItems: null,
            taxId: null,
            statusName: null,
            purchaseOrderNumber: null,
            purchaseOrderDate: null,
            supplierAddress: null,
            supplierGSTN: null,
            stockCheckedByName : null,
            qualityCheckedByName : null,
            inclusiveTax : null,

            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile : null,
            companySecondaryMobile : null,
            companyContactPersonNumber : null,
            companyContactPersonName : null,
            companyPrimaryTelephone : null,
            companySecondaryTelephone : null,
            companyWebsite : null,
            companyEmail : null,
            companyFaxNumber : null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber : null,
            companyPanNumber : null,
            companyPanDate : null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            //partyName: null,
            partyContactPersonNumber: null,
            partyPinCode: null,
            partyAreaId: null,
            partyCityId: null,
            partyStateId: null,
            partyCountryId: null,
            partyPrimaryTelephone: null,
            partySecondaryTelephone: null,
            partyPrimaryMobile: null,
            partySecondaryMobile: null,
            partyEmail: null,
            partyWebsite: null,
            partyContactPersonName: null,
            partyBillToAddress: null,
            partyShipAddress: null,
            partyDueDaysLimit: null,
            partyGstRegistrationTypeId: null,
            partyGstNumber: null,
            partyPanNumber: null,
            isIgst: null,
            partyCode:null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null,
        }

        this.grnForm = this.toFormGroup(data);
      
        this.handleChanges();
        this.createGRNFromDC();
        this.getAndPatchTerms();
        this.getFilteredParties();
    }

    setTransactionType() {

        this.route.params.
        pipe(takeUntil(this.onDestroy))
        .subscribe(params => {
            this.transactionTypeService.getTransactionType(+params['trantypeId'])
                .subscribe(response => {
                    this.transactionType = response;
                    //console.log("Transaction type in GRN ", this.transactionType.name);
                    //this.getParties();
                    this.getAutoNumber();
                    this.getDefaultDate();
                    this.getTempTransactionNumber();
                   // this.getParties();
                })

                if(params['id']){
                    //console.log('got id: ', params['id']);
                    this.grnService.getGrn(params['id']).subscribe(response => {
                        this.recentGrnGet(response);
                    })
                }
        });


    }

    showReference(){
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            tranTypeId: this.transactionType.id,
            tranId: this.grnForm.get('id').value

        };

        dialogConfig.height='600px';
        dialogConfig.width='1000px';

        
        const dialogRef = this.dialog.open(ReferenceLinksComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {
                //console.log('dialog closed: ', data)
                if (data) {
                    //console.log('navigating')
                    this._router.navigate([data.url, {id: data.id}]);
                    //this._router.navigate(['/']);                    
                }
            });
        
    }

    getAutoNumber() {
        this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id).subscribe(response => {
            this.numberRangeConfiguration = response;

            this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
            //this.grnNumberVerified = this.isAutoNumber ? true : false;
            //console.log("isAutoNumber" + this.isAutoNumber)
        }
        )
    }

    displayFnParty(party?: Party): string | undefined {
        return party ? party.partyCode+"-"+party.name + (party.areaName ? '[' + party.areaName + ']' : '') : undefined;
    }

    getAndPatchTerms() {

        this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id).subscribe(response => {
            this.numberRangeConfiguration = response;

            //  /   this.isTermsAndcondition = this.numberRangeConfiguration.termsAndConditionCheck === 1 ? true : false;
            this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
            //this.grnNumberVerified = this.isAutoNumber ? true : false;
            // //console.log("isTermsAndcondition" + this.isTermsAndcondition)
            // if (this.isTermsAndcondition === true) {


            this.termsAndConditionsService.getByTransactionTypeAndDefault(this.transactionType.id)
                .subscribe(response => {
                    //console.log("response in dc", response)
                    if (response != null) {
                        let deliveryTerms: string = response.deliveryTerms;
                        let paymentTerms: String = response.paymentTerms;
                        let termsAndConditions: string = response.termsAndCondition;

                        //console.log('paymentTerms: ', paymentTerms);
                        // this.grnForm.controls['paymentTerms'].patchValue(paymentTerms);
                        this.grnForm.controls['termsAndConditionId'].patchValue(response.id);
                        this.grnForm.controls['termsAndConditionNote'].patchValue(termsAndConditions);
                    }
                })

            // }

        })
    }

    private toFormGroup(data: GrnHeader): FormGroup {

        const itemArr = new FormArray([]);

        if (data.grnItems) {
            data.grnItems.forEach(item => {
                itemArr.push(this.makeItem(item));
            })
        }

        const formGroup = this.fb.group({
            id: [data.id],
            grnId: [data.grnId],
            grnNumber: [data.grnNumber, [Validators.required]],
            grnDate: [this.datePipe.transform(data.grnDate, AppSettings.DATE_FORMAT), [Validators.required]],
            purchaseOrderHeaderId: [data.purchaseOrderHeaderId],
            comments: [data.comments],
            verbal: [data.verbal],
            grnStatusId: [data.grnStatusId],
            companyId: [data.companyId],
            qualityCheckedById: [data.qualityCheckedById, [Validators.required]],
            stockCheckedById: [data.stockCheckedById, [Validators.required]],
            financialYearId: [data.financialYearId],
            supplierId: [data.supplierId, [Validators.required]],
            deliveryChallanHeaderId: [data.deliveryChallanHeaderId],
            deliveryChallanDate:[this.datePipe.transform(data.deliveryChallanDate, AppSettings.DATE_FORMAT)],
            deliveryChallanNumber: [data.deliveryChallanNumber, [Validators.required]],
            termsAndConditionNote: [data.termsAndConditionNote],
            termsAndConditionId: [data.termsAndConditionId],
            invoiceHeaderIds: [data.invoiceHeaderIds],
            purchaseOrderNumber: [data.purchaseOrderNumber],
            purchaseOrderDate: [data.purchaseOrderDate],
            supplierName: [data.supplierName, [Validators.required]],
            grnItems: itemArr,
            taxId: [data.taxId],
            statusName:[data.statusName],
            supplierAddress: [data.supplierAddress],
            supplierGSTN: [data.supplierGSTN],
            stockCheckedByName : [data.stockCheckedByName],
            qualityCheckedByName : [data.qualityCheckedByName],
            inclusiveTax : [data.inclusiveTax],
            // .......................
            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile : [data.companyPrimaryMobile],
            companySecondaryMobile : [data.companySecondaryMobile],
            companyContactPersonNumber : [data.companyContactPersonNumber],
            companyContactPersonName : [data.companyContactPersonName],
            companyPrimaryTelephone : [data.companyPrimaryTelephone],
            companySecondaryTelephone : [data.companySecondaryTelephone],
            companyWebsite : [data.companyWebsite],
            companyEmail : [data.companyEmail],
            companyFaxNumber : [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber : [data.companyGstNumber],
            companyPanNumber : [data.companyPanNumber],
            companyPanDate : [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
             //partyName: [data.partyName],
            partyContactPersonNumber: [data.partyContactPersonNumber],
            partyPinCode: [data.partyPinCode],
            partyAreaId: [data.partyAreaId],
            partyCityId: [data.partyCityId],
            partyStateId: [data.partyStateId],
            partyCountryId: [data.partyCountryId],
            partyPrimaryTelephone: [data.partyPrimaryTelephone],
            partySecondaryTelephone: [data.partySecondaryTelephone],
            partyPrimaryMobile: [data.partyPrimaryMobile],
            partySecondaryMobile: [data.partySecondaryMobile],
            partyEmail: [data.partyEmail],
            partyWebsite: [data.partyWebsite],
            partyContactPersonName: [data.partyContactPersonName],
            partyBillToAddress: [data.partyBillToAddress],
            partyShipAddress: [data.partyShipAddress],
            partyDueDaysLimit: [data.partyDueDaysLimit],
            partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
            partyGstNumber: [data.partyGstNumber],
            partyPanNumber: [data.partyPanNumber],
            isIgst: [data.isIgst],
            partyCode:[data.partyCode],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
            updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],

            
        });
        formGroup.get('grnItems').setValidators(Validators.required);
        this.fieldValidatorLabels.set("supplierId", "Party Name");
        this.fieldValidatorLabels.set("grnDate", "GRN Date");
        this.fieldValidatorLabels.set("grnNumber", "GRN Number");
        this.fieldValidatorLabels.set("grnItems", "GRN Items");
        this.fieldValidatorLabels.set("stockCheckedById", "Stock Checked By");
        this.fieldValidatorLabels.set("qualityCheckedById", "Quality Checked By");
        this.fieldValidatorLabels.set("deliveryChallanNumber", "DC Number");
        this.fieldValidatorLabels.set("materialId", "Material");
        this.fieldValidatorLabels.set("price", "Price");
        this.fieldValidatorLabels.set("rejectedQuantity", "Rejected Quantity");
        this.fieldValidatorLabels.set("acceptedQuantity", "Accepted Quantity");

        return formGroup;
    }


    makeItem(grnItem: GrnItem): FormGroup {
        return this.fb.group({
            id: grnItem.id,
            grnHeaderId: grnItem.grnHeaderId,
            materialId: grnItem.materialId,
            materialName: grnItem.materialName,
            deliveryChallanQuantity: grnItem.deliveryChallanQuantity,
            acceptedQuantity: [grnItem.acceptedQuantity, [Validators.required]],
            rejectedQuantity: [grnItem.rejectedQuantity, [Validators.required]],
            purchaseOrderHeaderId: grnItem.purchaseOrderHeaderId,
            invoiceBalanceQuantity: grnItem.invoiceBalanceQuantity,
            remarks: grnItem.remarks,
            amount: grnItem.amount,
            deliveryChallanHeaderId: grnItem.deliveryChallanHeaderId,
            deliveryChallanItemId: grnItem.deliveryChallanItemId,
            unitOfMeasurementId: grnItem.unitOfMeasurementId,
            unitPrice: grnItem.unitPrice,
            taxId:grnItem.taxId,
            materialSpecification: grnItem.materialSpecification,
            slNo: [grnItem.slNo],
        });
    }

    getFilteredParties() {
        //console.log('in getFilteredParties')
       let partyType: string =  AppSettings.PARTY_TYPE_SUPPLIER;
        this.filteredParties = this.grnForm.controls['supplierName'].valueChanges
            .pipe(
                startWith<string | Party>(''),
                debounceTime(300),
                map(value => typeof value === 'string' ? value : value.name),
                filter(value => value != (this.selectedParty ? this.selectedParty.name : '')),
                switchMap(value => this.partyService.getPartiesByNameLike(partyType, value))
            );

            this.transactionTypeService.getTransactionType(6).subscribe(response => {
                        this.transactionTypeIncomingDC = response;
                    });
            
    }

    // checkGrnNumAvailability(){
    //     //console.log("this.transactionType.name.....>>", this.transactionType.name);
    //         let grnNumber: string = this.grnForm.get('grnNumber').value
    //         //console.log("grnNumber...........>",grnNumber);
    //         if (grnNumber && grnNumber.length > 0 && this.transactionType.name === AppSettings.GRN_TYPE_SUPPLIER ) {
          
    //           this.grnService.checkGrnNumAvailability(grnNumber, this.selectedParty.id).subscribe(response => {
     
    //             //console.log("is grnNumber avialable? ", response);
    //             if (response.responseStatus === 1) {
    //               //console.log("Yes");
    //               //this.grnNumberVerified = true;
    //             } else {
    //               //console.log("No");
    //               //this.grnNumberVerified = false;
    //             //   this.alertService.danger( "grnNumber " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //               this.alertService.danger(response.responseString);
    //                 this.grnForm.controls['grnNumber'].patchValue('');
    //                this.grnNumberElement.nativeElement.focus();
     
    //             }
     
    //           });
    //         }
    //   }
    // getParties() {

    //     this.partyService.getSuppliers().subscribe(
    //         response => {
    //             //console.log(response);
    //             this.parties = response;
    //             this.getFilteredParties();
    //         }
    //     );

    //     //set incoming dc transaction type

    //     this.transactionTypeService.getTransactionType(6).subscribe(response => {
    //         this.transactionTypeIncomingDC = response;
    //     });

    // }
    // getFilteredParties() {
    //     //console.log('in getFilteredParties')
    //     this.filteredParties = this.grnForm.controls['supplierName'].valueChanges
    //         .pipe(
    //         startWith(''),
    //         map(val => this.filterParty(val))
    //         );
    // }

    // filterParty(val: string): Party[] {
    //     //console.log('in filter: ', val);
    //     //console.log('parties ', this.parties);
    //     if (val) {
    //         return this.parties.filter(party =>

    //             party.name.toLowerCase().includes(val.toLowerCase())
    //         );
    //     }
    //     return this.parties;
    // }

    partyChanged(_event: any, party: Party) {
        //console.log('party: ', party, _event);
        if (_event.isUserInput) {
            this.grnForm.controls['supplierId'].patchValue(party.id);
            this.selectedParty = party; //this.parties.find(p => p.id == party);
            this.grnForm.controls['deliveryChallanNumber'].patchValue("");
            // this.grnForm.controls['supplierId'].valueChanges.subscribe(partyId => {
            if (this.selectedParty.id !== undefined)
                this.getDCsForAParty(this.selectedParty.id);

                this.grnForm.controls['supplierGSTN'].patchValue(this.selectedParty.gstNumber);
                this.grnForm.controls['supplierAddress'].patchValue(this.selectedParty.address);
            // })
          let control =  <FormArray>this.grnForm.controls['grnItems']
            while (control.length !== 0) {
                control.removeAt(0);
            }
        }
    }
    handleChanges() {
      
       // this.createInvoice();
        // this.grnForm.controls['deliveryChallanNumber'].valueChanges.subscribe(dcId => {
        //     if (dcId !== undefined)
        //         this.populateItems(dcId);
        // })


    }
    onDCChange() {
        //console.log("this.deliveryChallans...........................>>>>>>>>>>>>>>",this.deliveryChallans);
        
        let dcNumber: string = this.grnForm.controls['deliveryChallanNumber'].value;
        if (dcNumber !== undefined) {
            //console.log(dcNumber);

            let dc: DeliveryChallanHeader = this.deliveryChallans.filter(dc => dc.deliveryChallanNumber === dcNumber)[0];

            this.grnForm.controls['deliveryChallanHeaderId'].patchValue(dc.id);
            this.grnForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(dc.deliveryChallanDate, AppSettings.DATE_FORMAT));
            if(dc.purchaseOrderNumber!= null)
            {
                this.grnForm.controls['purchaseOrderNumber'].patchValue(dc.purchaseOrderNumber);
                this.grnForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(dc.purchaseOrderDate, AppSettings.DATE_FORMAT));
            }

            this.populateItems(dc);
        }
    }

    getDCsForAParty(partyId) {
//console.log("partyId"+partyId,this.transactionTypeIncomingDC.name, this.transactionType.name)
        this.dcService.getDeliveryChallansForPartyAndTransactionType(partyId, this.transactionTypeIncomingDC.name, this.transactionType.name).subscribe(
            response => {
                this.deliveryChallans = response;
            }
        )

    }

    populateItems(dcs: DeliveryChallanHeader) {
        //console.log("dcID", dcs)
        // let dcHeader: DeliveryChallanHeader = this.deliveryChallans.filter(dc => dc.id === dcId)[0];

        if (dcs) {
            const control = <FormArray>this.grnForm.controls['grnItems'];

            //Clear already populated rows if any

            while (control.length !== 0) {
                control.removeAt(0);
            }

            this.headerTax = this.taxes.filter(tax => tax.id === dcs.taxId)[0];
            if(!this.headerTax){
                this.headerTax = this.taxes[0];
            }
            dcs.deliveryChallanItems.forEach(item => {

                let grnItem: GrnItem = this.mapItem(item);
                if (grnItem)
                    control.push(this.makeItem(grnItem));
            })

        }
    }


    mapItem(item: DeliveryChallanItem): GrnItem {

        let grnItem: GrnItem = null;
        if (item.grnBalanceQuantity > 0) {
            grnItem = {
                id: null,
                grnHeaderId: null,
                materialId: item.materialId,
                materialName: item.materialName,
                deliveryChallanQuantity: item.grnBalanceQuantity,
                acceptedQuantity: null,
                rejectedQuantity: null,
                purchaseOrderHeaderId: null,
                invoiceBalanceQuantity: null,
                remarks: null,
                amount: null,
                deliveryChallanHeaderId: item.headerId,
                deliveryChallanItemId: item.id,
                unitOfMeasurementId: item.unitOfMeasurementId,
                unitPrice: item.price,
                materialSpecification: item.specification,
                taxId: item.taxId,
                discountPercentage: null,
                discountAmount: null,
                amountAfterDiscount: null,
                sgstTaxPercentage: null,
                sgstTaxAmount: null,
                cgstTaxPercentage: null,
                cgstTaxAmount: null,
                igstTaxPercentage: null,
                igstTaxAmount: null,
                taxAmount: null,
                partNumber: null,
                hsnOrSac: null,
                partName: null,
                uom: null,
                inclusiveTax: null,
                slNo: item.slNo,
                status: null,
                grnItemStatus: null,
            }
        }
        return grnItem;
    }

    getUsers() {

        this.userService.getRecentUser(null, null, null,null,null).subscribe(
            response => {
                this.QualityCheckUsers = response.users;
                this.StockCheckUsers = response.users;
            }
        )

    }

    delete() {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE


        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.grnForm.controls['id'].value;//this.quotationHeader.id;
                //console.log("deleteing..." + id);
                this.grnService.delete(id)
                    .subscribe(response => {
                        //console.log(response);
                        if (response.responseStatus === 1) {
                            this.transactionStatus = response.responseString;
                            this.saveStatus = true;
                            this.recentGrnLoadCounter++;
                            this.spinStart = false;
                            this.isCreateEnabled =false;
                            this.clearFormNoStatusChange();
                            this.alertService.success(this.transactionStatus);
                        } else {
                            this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                            this.transactionStatus = response.responseString;
                            this.saveStatus = false;
                            this.spinStart = false;
                            this.alertService.danger( response.responseString);
                        }
                    },
                    error => {
                        //
                        
                        this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                        this.alertService.danger(this.transactionStatus);
                        this.saveStatus = false;
                        this.spinStart = false;
                    });
            }
            this.dialogRef = null;
        });
    }
    clearFormNoStatusChange() {
        this.initForm();
        this.handleChanges();
        this.selectedParty = null;
    }

    public findInvalidControls() {
        let invalid = [];
        let controls = this.grnForm.controls;
        ////console.log("controls "+controls.value)
        for (let name in controls) {
         // //console.log("name "+controls[name].value)
            if (controls[name].invalid) {
             // //console.log("name of cntr "+controls[name])
                invalid.push(name);
                ////console.log("invalid "+invalid);
            }
        }
        return invalid;
    }


    submitWrapper(model: GrnHeader){
        model.supplierName=this.displayPartyName;
        if(this.numberRangeConfiguration.id===null){
            
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
            return;
        }
        if(this.isAutoNumber && !model.grnNumber){
            this.grnForm.get('grnNumber').patchValue(this.tempTransactionNumber);
        }
        let grnNumber: string = this.grnForm.get('grnNumber').value
       //console.log("grnNumber...........>",grnNumber);
        if (grnNumber && grnNumber.length > 0 && this.transactionType.name === AppSettings.GRN_TYPE_SUPPLIER ) {
      
          this.grnService.checkGrnNumAvailability(grnNumber, this.selectedParty.id,model.id).subscribe(response => {
 
            //console.log("is grnNumber avialable? ", response);
            if (response.responseStatus === 1) {
              //console.log("Yes");
              this.onSubmit(model);
            } else {
              //console.log("No");
              //this.grnNumberVerified = false;
            //   this.alertService.danger( "grnNumber " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
              this.alertService.danger(response.responseString);
                this.grnForm.controls['grnNumber'].patchValue('');
               this.grnNumberElement.nativeElement.focus();
 
            }
 
          });
        }
        else{
            this.alertService.danger("Please enter GRN number");
            return false;
        }


    }

    onSubmit(model: GrnHeader) {     

       

        if(model.id===null){
            model = this.transactionItemService.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
           model.supplierGSTN=this.selectedParty.gstNumber;
           model.supplierAddress=this.selectedParty.address;
            //console.log("model :",model);   
            model.supplierName =this.selectedParty.name;
        }
       
        // if(!this.grnNumberVerified){
        //     this.alertService.danger("Enter Valid GRN Number");
        //     this.grnForm.get('grnNumber').markAsTouched();
        //     return;
        // }
        // else{
        //     this.grnForm.get('grnNumber').patchValue(this.tempTransactionNumber);
        // }
         if(!this.grnForm.valid){
            const controls = this.grnForm;
            let invalidFieldList: string[] =this.utilityService.findInvalidControlsRecursive(controls);
            //console.log("invalidFieldList ", invalidFieldList)
      
            let invalidFiledLabels: string[] = [];
            
            invalidFieldList.forEach(field => {
              if (this.fieldValidatorLabels.get(field))
                invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })
      
            //console.log("invalidFiledLabels ", invalidFiledLabels)
      
            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE+invalidFiledLabels);
            return false;
      }
        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id;
        model.grnTypeId = this.transactionType.id;
        model.taxId = this.headerTax ? this.headerTax.id : model.grnItems[0].taxId;
        
        this.spinStart = true;
        //Assign Serial numbers to the items
        model.grnItems.forEach((item, index) => {
            item.slNo = index+1;
        })        
        //console.log("before save: ", model);
        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
           
        }
        
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }
        
        let grnWrapper: GrnWrapper = {itemToBeRemove: this.itemToBeRemove, grnHeader: model};
        //console.log("grnWrapper.......",grnWrapper);
        this.grnService.create(grnWrapper)
            .subscribe(response => {
                //console.log("saved id: ", response);
                this.showTheCreatedBy=true;
                this.grnHeader = response;
                this.displayPartyName=this.grnHeader.supplierName;
               
                this.grnHeader.supplierName=this.grnHeader.partyCode+"-"+this.grnHeader.supplierName;
                if(!response.updateBy){
                    this.grnService.getGrn(response.id)
                    .subscribe(responses => {
                        this.grnForm.controls['updateBy'].patchValue(responses.updateBy);
                        this.grnForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));

                           

                    });
                }
                this.transactionStatus = "Grn " + response.grnNumber + message ;
                this.alertService.success(this.transactionStatus);
                this.saveStatus = true;
                this.spinStart = false;
                this.recentGrnLoadCounter++;
                this.isReadonly=false;
                // this.disableForm();
                //console.log("responsedeliveryChallanNumber", response.deliveryChallanNumber);
                this.grnForm.controls['deliveryChallanNumber'].patchValue(response.deliveryChallanNumber);
                this.grnForm = this.toFormGroup(this.grnHeader);                
                this.isCreateButtonenabled();
                this.disableForm();
            },
            error => {
                //console.log("Error ", error);
                this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
            });

    }

    clearForm() {
        this.saveStatus = false;
        this.transactionStatus = null;
        this.initForm();
        this.handleChanges();
        this.isCreateButtonenabled();
        this.getFilteredParties();
        this.grnForm.markAsPristine();
        this.getTempTransactionNumber();
        this.grnForm.controls['supplierName'].patchValue('');
        this.deliveryChallans = [];
        this.isReadonly = false;
    }

    print() {

        //console.log("Party....................>>>>>>>>>>>>>>>>>>>",this.selectedParty, this.grnHeader);

        let l_printCopy: number;
        let partyName: string = this.grnForm.get('supplierName').value
        this.grnForm.controls['supplierName'].patchValue(this.displayPartyName);
        this.printRef = this.dialog.open(PrintDialogContainer, {
            // width: '250px',
            data: { 
                transactionType: this.transactionType, 
                printDataModel: this.grnForm.value, 
                printCopy: l_printCopy, 
                globalSetting: null,
                numberRangeConfiguration: this.numberRangeConfiguration
             }
        });
        // this.printRef.afterClosed().subscribe(result => {
        //     //console.log("result: ", result);
        //     if (result) {
        //         l_printCopy = +result;
        //         let company: Company;
        //         this.companyService.get(1).subscribe(comp => {
        //             company = comp;
        //             this.grnReportService.download(this.selectedParty.billAddress, this.grnHeader, company, l_printCopy);
        //         });
        //     }

        // })
        this.grnForm.controls['supplierName'].patchValue(partyName);
    }


    closeForm() {
        // this._router.navigateByUrl('/dashboard')
        this._router.navigate(['/']);
    }

    setStatusBasedPermission(statusId: number) {
        
                if (statusId) {
                    //console.log('status id: ', statusId);
                    this.userService.getStatusBasedPermission(statusId).subscribe(response => {
                        this.statusBasedPermission = response;
                        //console.log("in this.statusBasedPermission", this.statusBasedPermission)
                    })
                } else {
                    //console.log("no status id: ", statusId);
                }

             
            }

    recentGrnGet(event) {
        //console.log('In recentGrnGet', event);
        this.showTheCreatedBy=false;
        this.grnHeader = event;
        this.grnForm = this.toFormGroup(this.grnHeader);
        let dcFound: boolean = false;
        this.partyService.getCustomer(this.grnHeader.supplierId).subscribe(party => {
            
                        this.selectedParty = party;
                      //  this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                      this.displayPartyName=this.grnHeader.supplierName;
                      this.grnHeader.supplierName=this.grnHeader.partyCode+"-"+this.grnHeader.supplierName;

                      this.grnForm = this.toFormGroup(this.grnHeader);
                        //console.log("this.grnForm.controls['grnStatusId'].value"+this.grnForm.controls['grnStatusId'].value);
                        this.setStatusBasedPermission(this.grnForm.controls['grnStatusId'].value);
                        this.isCreateButtonenabled();
                        this.disableForm();
                       
                    });
        this.deliveryChallans.forEach(dch => {
            if (dch.deliveryChallanNumber === this.grnHeader.deliveryChallanNumber) {
                dcFound = true;
            }
        });
        //console.log("this.grnHeader.deliveryChallanNumber", this.grnHeader.deliveryChallanNumber)
        if (!dcFound) {
            //console.log(" bef this.grnHeader.deliveryChallanNumber", this.grnHeader.deliveryChallanNumber)

            this.grnForm.controls['deliveryChallanNumber'].patchValue(this.grnHeader.deliveryChallanNumber);
            //console.log("aft this.grnHeader.deliveryChallanNumber", this.grnHeader.deliveryChallanNumber)

        }

        ////console.log(this.invoiceForm.controls('invoiceItems').)
        this.disableForm();
        
    }
    disableForm() {
        this.grnForm.disable({ onlySelf: true, emitEvent: false });
       
            this.isReadonly=true;
           
         
        // this.disableParty = true;
    }
   
    itemChanged(event) {
        this.handleChanges();
    }

    removeGrnItem(idx: number) { 

        // (<FormArray>this.grnForm.get('grnItems')).removeAt(idx);

        // return false;

        let itemId: string = (<FormArray>this.grnForm.get('grnItems')).at(idx).get("id").value;
        //console.log("Itemid " + itemId)
        if (itemId) {

            //console.log("before confirmationn dialog")
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

            this.dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.itemToBeRemove.push(itemId);
                    (<FormArray>this.grnForm.get('grnItems')).removeAt(idx);
                }
            //console.log("dialog open");
            });
        } else {
            //
            // else just delete in the array
            //
            (<FormArray>this.grnForm.get('grnItems')).removeAt(idx);
        }

        return false;

    }


    enableForm() {
        this.grnForm.enable({ onlySelf: true, emitEvent: false });
    }
    edit() {

        this.enableForm();

    }
    whenNoDataFound(noDataFound: boolean) {
        if (noDataFound) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE
        }
    }
    isCreateButtonenabled()
    {
        //console.log(this.grnForm.controls['grnStatusId'].value + "statys"+this.grnForm.controls['id'].value  ) //&& this.grnForm.controls['statusName'].value == AppSettings.STATUS_NEW && this.tranForm.controls['statusId'].value == 68
        if(this.grnForm.controls['id'].value != null && this.grnForm.controls['statusName'].value == AppSettings.STATUS_NEW)
        {
            this.isCreateEnabled = true;
        }
        else
        {
            this.isCreateEnabled = false;
        }
    }

    createGRNFromDC(){
        if( this.transCommService.dcData){
        this.transCommService.dcData
        .pipe(
            take(1)
            ,takeUntil(this.onDestroy)
        )
        .subscribe(response => {
            // console.log('response ', response);
            if(response){
                this.isReadonly=true;
                //console.log('response in', response.deliveryChallanNumber);
                this.grnForm.controls['deliveryChallanNumber'].patchValue(response.deliveryChallanNumber);
                this.grnForm.controls['deliveryChallanNumber'].patchValue(response.deliveryChallanNumber);
               this.grnForm.controls['supplierName'].patchValue(response.partyName);
               this.grnForm.controls['inclusiveTax'].patchValue(response.inclusiveTax);
                this.partyService.getCustomer(response.partyId).subscribe(party => {
                    this.grnForm.controls['supplierId'].patchValue(party.id);
                    this.selectedParty = party;
                    let isIgst: boolean = this.partyService.getIgstApplicable(this.selectedParty);
                                ////console.log(" this.isIgst " + this.isIgst);
                    this.grnForm.controls['isIgst'].patchValue(isIgst ? 1 : 0);
                });
               this.deliveryChallans.push(response);
               this.onDCChange();                   
            //    this.transCommService.dcHeader.next(null);
                this.transCommService.updateDcData(null);
            //    this.transCommService.dcHeader.complete();
                this.transCommService.completeDcData();
            }
        });
    }
 }

    createInvoice(){
        //console.log("this.grnHeader...............", this.grnHeader);
        
        let sourceTransactionType: TransactionType;
        this.transCommService.initDcData();
        this.transCommService.initGrnData();
        this.transCommService.updateGrnData(this.grnHeader);
            this.transactionTypeService_in.getAllTransactionTypes().subscribe(response => {
              //  this.transactionTypes=response;
                //console.log("this.transactionType.name"+this.transactionType.name);
                response.forEach(tt => {
                    if ( this.transactionType.name === AppSettings.GRN_TYPE_SUPPLIER) {
                      //console.log("tt"+tt)
                     
                     if (tt.name == AppSettings.PURCHASE_INVOICE)
                     {
                        //console.log("tt.name"+tt.name)
                        sourceTransactionType = tt;
                       
                        this._router_in.navigate(['transaction/app-new-invoice',tt.id]);
                        
                     }
                    }
                   
                   
                    }); 
            });
        }
          


           getTaxes() {
            this.taxService.getAllTaxes().subscribe(
                response => {
                    this.taxes = response;
                    this.initForm();
                }
            )
        }



        getTempTransactionNumber(){
            this.transactionTypeService.getTransactionNumber(this.transactionType.name)
            .pipe(
                takeUntil(this.onDestroy)
            )
            .subscribe(response => {
                this.tempTransactionNumber = response.responseString;
            })
        }


        helpVideos() {
        
            let status: string = "";
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.id =this.activityId.toString();
         
            
            const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(
                data => {
    
                });
    
    
    
            
        }
}