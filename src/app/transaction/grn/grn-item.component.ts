
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
@Component({
    selector: 'grn-item',
    templateUrl: 'grn-item.component.html',
    styleUrls: ['grn-item.component.scss']
})
export class GrnItemComponent implements OnInit {

    @Input() group: FormGroup;
    @Input() index: number;
    @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
    @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
    private changeCtr: number = 0;
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;

    constructor(
        public dialog: MatDialog, ) {

    }
    ngOnInit() {

    }

    resetRejectedQuantity() {

        //  //console.log('patchQuantity.', this.group.controls['deliveryChallanQuantity'].value, ' accpted qty ', this.group.controls['acceptedQuantity'].value);
        let lDcQuantity: number = this.group.controls['deliveryChallanQuantity'].value;

        let lAcceptedQuantity: number = this.group.controls['acceptedQuantity'].value;


        // //console.log('patchQuantity.', lDcQuantity, ' accpted qty ', lAcceptedQuantity);
        let lRejectedQuantity: number;
        lRejectedQuantity = null;
        if ((Number(lDcQuantity)) < (Number(lAcceptedQuantity))) {


            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "Accepted quantity must be less than or equal to DC Quantity"


            this.group.controls['rejectedQuantity'].patchValue("");

            this.group.controls['acceptedQuantity'].patchValue("");
        }
        else {
            // lRejectedQuantity = null;
            lRejectedQuantity = lDcQuantity - lAcceptedQuantity;

            // //console.log("lRejectedQuantity=" + lRejectedQuantity)
            lRejectedQuantity = +lRejectedQuantity.toFixed(2);

            this.group.controls['rejectedQuantity'].patchValue(lRejectedQuantity);
        }


    }
    delete(index: number) {

        //console.log("Deleting...", index);
        this.deleteIndex.emit(index);
    }

}