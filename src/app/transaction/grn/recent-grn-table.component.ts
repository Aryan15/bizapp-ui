import { AfterViewInit, Component, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { GrnHeader, RecentGrn } from '../../data-model/grn-model';
import { TransactionType } from '../../data-model/transaction-type';
import { GrnService } from '../../services/grn.service';

const PAGE_SIZE: number = 10;
@Component({
    selector: 'recent-grns-table',
    templateUrl: 'recent-grn-table.component.html',
    styleUrls: ['recent-grn-table.component.scss']
})
export class RecentGrnTableComponent implements AfterViewInit, OnChanges {

    @Input()
    recentGrnLoadCounter : number;

    @Input()
    transactionType: TransactionType;

    @Output()
    grnHeader : EventEmitter<GrnHeader> = new EventEmitter<GrnHeader>();
  
    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    displayedColumns = ['grnNumber', 'grnDate','partyName'];
    grnDatabase: GrnHttpDao | null;

    dataSource = new MatTableDataSource<GrnHeader>();

    filterForm = new FormGroup ({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
  
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    public recentGrns : GrnHeader[] = [];

    constructor(private grnService : GrnService){

    }

   
    ngAfterViewInit(){
        
                this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
                
                    this.grnDatabase = new GrnHttpDao(this.grnService);
            
                    //If the user changes sort order, reset back to first page
            
                    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
            
                    this.getData();
            
                }

                getData(){
                  merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
                    .pipe(
                      startWith({}),
                      switchMap(() => {
                        this.isLoadingResults = true;
                        return this.grnDatabase!.getGrns(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
                      }),
                      map(data => {
                        //Flip flag to show that loading has finished
                        this.isLoadingResults = false;
                        this.resultsLength = data.totalCount;
                        //console.log('data.materialList: ', data.grnHeaders);
                        //console.log('data.totalCount: ', data.totalCount);
                        if( this.filterForm.get('filterText').value != null)
                        {
                         if(data.totalCount === 0)
                         {
                           this.noDataFound.emit(true);
                         }
                        }
                        return data.grnHeaders;
                      }),
                      catchError(() => {
                        this.isLoadingResults = false;
                        //console.log('catchError error');
                        return observableOf([]);
                      })
                    ).subscribe(data => this.dataSource.data = data);
                }


                ngOnChanges(){
                
                  //console.log('in onchanage: ', this.recentGrnLoadCounter);
              
                  if(this.recentGrnLoadCounter > 0 ){
                    this.getData();
                    
                  }
                }
              
                onRowClick(row: any){
                  this.isLoadingResults = true;
                    //console.log('row clicked: ', row);
                    this.grnService.getGrn(row.id)
                    .subscribe(response => {
                      if(response){
                        this.grnHeader.emit(response);
                        
                      }
                      this.isLoadingResults = false;
                    }, error => {
                      this.isLoadingResults = false;
                    })
                    
                }
        
            }
        
            
        
            export class GrnHttpDao{
                
                  constructor(private grnService: GrnService) { }
                
                  getGrns(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentGrn>{
                
                    pageSize = pageSize ? pageSize : PAGE_SIZE;
                    //console.log('filterText: ', filterText);
                    //console.log('transactionType: ', transactionTypeId);
                
                
                    return this.grnService.getRecentGrns(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);
                  }
                }
        