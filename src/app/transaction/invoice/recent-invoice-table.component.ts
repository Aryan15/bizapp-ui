import { Component,Input, Output, EventEmitter, ViewChild, OnChanges, AfterViewInit } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable ,  merge ,  of as observableOf } from 'rxjs';
import { catchError ,  map ,  debounceTime ,  startWith ,  switchMap } from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';
import { TransactionType } from '../../data-model/transaction-type';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceHeader, RecentInvoice } from '../../data-model/invoice-model';
import { AppSettings } from '../../app.settings';

const PAGE_SIZE: number = AppSettings.REPORT_PAGE_SIZE;

@Component({
  selector: 'recent-invoice-table',
  templateUrl: './recent-invoice-table.component.html',
  styleUrls: ['./recent-invoice-table.component.scss']
})
export class RecentInvoiceTableComponent implements AfterViewInit, OnChanges {

  @Input()
  recentInvoiceLoadCounter : number;

  @Input()
  transactionType : TransactionType;

  @Output()
  invoiceHeader : EventEmitter<InvoiceHeader> = new EventEmitter<InvoiceHeader>();
  
  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();

  displayedColumns = ['invoiceNumber', 'invoiceDate', 'grandTotal', 'partyName'];
  invoiceDatabase: InvoiceHttpDao | null;

  dataSource = new MatTableDataSource<InvoiceHeader>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;
  //recentInvoiceLoadCounterObservable: Observable<number>;
  onChangeEvent: Observable<number>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  
  constructor(private invoiceService: InvoiceService) { }

  ngAfterViewInit() {
    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    //this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);

    this.invoiceDatabase = new InvoiceHttpDao(this.invoiceService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.getData();

    

  }

  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.invoiceDatabase!.getInvoices(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        //console.log('data.materialList: ', data.invoiceHeaders);
        //console.log('data.totalCount: ', data.totalCount);
        if( this.filterForm.get('filterText').value != null)
        {
         if(data.totalCount === 0)
         {
           this.noDataFound.emit(true);
         }
        }
        return data.invoiceHeaders;
      }),
      catchError((error) => {
        this.isLoadingResults = false;
        //console.log('catchError ',error);
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);
  }

  ngOnChanges(){
    
    //console.log('in onchanage: ', this.recentInvoiceLoadCounter);
    if(this.invoiceDatabase){
      this.getData();
    }
    
    
  }


  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.isLoadingResults = true;
    this.invoiceService.getInvoice(row.id)
    .subscribe(response => {
      this.invoiceHeader.emit(response);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
    //this.invoiceHeader.emit(row);
  }
}

export class InvoiceHttpDao {

  constructor(private invoiceService: InvoiceService) { }

  getInvoices(transactionType: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentInvoice> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);
    //console.log('transactionType: ', transactionType);

    return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);
  }
}
