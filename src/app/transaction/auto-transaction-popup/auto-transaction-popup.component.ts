import { Component, OnInit, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material/dialog";
import { AutoTransactionGeneration, AutoTransactionGenerationList } from '../../data-model/autotransaction-popup-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { DatePipe } from '@angular/common';
import { AlertService } from 'ngx-alerts';
import { InvoiceService } from '../../services/invoice.service';
import { UtilityService } from '../../utils/utility-service';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';

@Component({
  selector: 'app-auto-transaction-popup',
  templateUrl: './auto-transaction-popup.component.html',
  styleUrls: ['./auto-transaction-popup.component.scss']
})
export class AutoTransactionPopupComponent implements OnInit {

  public autoTransactionPopUpForm: FormGroup;
  public autoTransactionModel: AutoTransactionGeneration;
  currentUser: any;
  date: Date;
  themeClass: any;
  errorMessage: string = null;
  fieldValidatorLabels = new Map<string, string>();
  financialMinDate: Date;
  financialMaxDate: Date;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  datePipe = new DatePipe('en-US');
  weeks = true;
  dateToGenerate = true;
  transactionStatus: string = null;
  invoiceCount: number;
  defaultDate: string;
  disables: boolean = false;
  disableStop:boolean=false;
  alertRef: MatDialogRef<AlertDialog>;
  public periodToCreateTransactions: periodToCreateTransaction[] = [
    { id: 1, name: "Montly" },
    { id: 2, name: "Quarterly" },
    { id: 3, name: "Weekly" },
    { id: 4, name: "Day" },
  ]


  public daysOfWeeks: daysOfWeek[] = [
    { id: 1, name: "Sunday" },
    { id: 2, name: "Monday" },
    { id: 3, name: "Tuesday" },
    { id: 4, name: "Wednesday" },
    { id: 5, name: "Thursday" },
    { id: 6, name: "Friday" },
    { id: 7, name: "Saturday" },
  ]



  public dateToGenerateInvoices: dateToGenerateInvoice[] = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
    { id: 8 },
    { id: 9 },
    { id: 10 },
    { id: 11 },
    { id: 12 },
    { id: 13 },
    { id: 14 },
    { id: 15 },
    { id: 16 },
    { id: 17 },
    { id: 18 },
    { id: 19 },
    { id: 20 },
    { id: 21 },
    { id: 22 },
    { id: 23 },
    { id: 24 },
    { id: 25 },
    { id: 26 },
    { id: 27 },
    { id: 28 },
    { id: 29 },
    { id: 30 },
    { id: 31 },


  ]

  constructor(private fb: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private invoiceService_in: InvoiceService,
    private utilityService: UtilityService,
    protected dialog: MatDialog,
    private dialogRef: MatDialogRef<AutoTransactionPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }

  ngOnInit() {
    this.initForm();
    this.getDefaultDate();
    this.getAutoTransaction();

  }
  private initForm() {
    let data: AutoTransactionGeneration = {
      id: null,
      periodToCreateTransaction: null,
      countOfInvoice: 2,
      startDate: null,
      invoiceHeaderId: null,
      daysOfWeek: null,
      daysOfMonth: null,
      invoiceNumber: null,
      stopTranaction: null,
      email: null,
      endDate: null,
      totalCount: null,
      sheduledDates:null,
      partyName:null,
      partyId:null
    

    }
    this.autoTransactionPopUpForm = this.toFormGroup(data);
  }


  private toFormGroup(data: AutoTransactionGeneration): FormGroup {



    const formGroup = this.fb.group({
      id: [data.id],
      periodToCreateTransaction: [data.periodToCreateTransaction, Validators.required],
      countOfInvoice: [data.countOfInvoice, Validators.compose([Validators.required, Validators.min(2)])],
      startDate: [this.datePipe.transform(data.startDate, AppSettings.DATE_FORMAT), [Validators.required]],
      invoiceHeaderId: [data.invoiceHeaderId],
      daysOfWeek: [data.daysOfWeek],
      daysOfMonth: [data.daysOfMonth],
      invoiceNumber: [data.invoiceNumber],
      stopTranaction: [data.stopTranaction],
      email: [data.email],
      endDate: [this.datePipe.transform(data.endDate, AppSettings.DATE_FORMAT)],
      totalCount: [data.totalCount]

    });
    
    return formGroup;

  }


  onSelect(_event: any) {
    console.log(_event.value + "_event.value");

    if (_event.value === 4) {
      this.weeks = false;
      this.dateToGenerate = false;


    }

    else if (_event.value === 3) {

      this.dateToGenerate = false;
      this.weeks = true;
    }


    else if (_event.value === 1 || _event.value === 2) {

      this.weeks = false;
      this.dateToGenerate = true;

    }

  }



  getAutoTransaction() {
    this.dialogRef.id
    console.log(this.dialogRef.id)
    if (this.dialogRef.id) {
      this.invoiceService_in.getAutoTransaction(this.dialogRef.id).subscribe(
        response => {
          console.log(response)
          if (response.id) {
            this.autoTransactionModel = response;
            this.autoTransactionPopUpForm = this.toFormGroup(this.autoTransactionModel);
            if (response.periodToCreateTransaction === 1 || response.periodToCreateTransaction === 2) {
              this.weeks = false;
              this.dateToGenerate = true;
            }
            if (response.periodToCreateTransaction === 4) {
              this.weeks = false;
              this.dateToGenerate = false;
            }
            if (response.periodToCreateTransaction === 3) {

              this.dateToGenerate = false;
              this.weeks = true;
            }
            this.invoiceCount = response.totalCount;
            if (response.totalCount === response.countOfInvoice) {
              this.disables = true;
              this.autoTransactionPopUpForm.disable();
            }
            if (response.totalCount && response.totalCount != response.countOfInvoice) {
             
              this.disables = true;


              // this.autoTransactionPopUpForm.controls['countOfInvoice'].enable();


            }




          }
        }

      )
    }
  }


  save() {
   
    let dialogRef: MatDialogRef<ConfirmationDialog>;

    //   if(!this.invoiceCount){

    //       if(this.defaultDate<this.autoTransactionPopUpForm.get('startDate').value){


    //       dialogRef = this.dialog.open(ConfirmationDialog, {
    //         disableClose: false
    //       });

    //       dialogRef.componentInstance.confirmMessage = AppSettings.AUTO_TRANSACTION_STOP_CONFORMATION_MESSAGE;

    //       dialogRef.afterClosed().subscribe(result => {
    //         if (!result) {

    //           return;
    //         }
    //         if(result){
    //           this.dialogRef.close(this.autoTransactionPopUpForm.value);
    //         }
    //       });

    //     }
    //     else{
    //     this.dialogRef.close(this.autoTransactionPopUpForm.value);
    //     }
    //   }
    //  else{

    //  }


    this.dialogRef.close(this.autoTransactionPopUpForm.value);


  }

  getDefaultDate() {

    this.utilityService.getCurrentDateObs().subscribe(response => {
      this.defaultDate = this.datePipe.transform(response, AppSettings.DATE_FORMAT)
      this.financialMinDate = new Date(this.defaultDate);
    
    
    })

  }

  close(): void {
    //console.log('close clicked');
    this.dialogRef.close();
    // this.dialogRef.close();
  }


  stopTransaction() {
    let dialogRef: MatDialogRef<ConfirmationDialog>;

    dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    dialogRef.componentInstance.confirmMessage = AppSettings.AUTO_TRANSACTION_STOP_CONFORMATION_MESSAGE;

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {

        return
      }
      else {


        this.invoiceService_in.stopAutoTransaction(this.autoTransactionModel)
          .subscribe(response => {

            if (response.stopTranaction === 1) {
              this.disableStop=true;
              this.transactionStatus = "Transaction is Terminated";
              this.alertService.success(this.transactionStatus);




            }
          });
      }
    });

  }
       


}
export interface periodToCreateTransaction {
  id: number,
  name: string,
}

export interface limitToCreateTransaction {
  id: number,
  name: string,
}

export interface daysOfWeek {
  id: number,
  name: string,
}
export interface dateToGenerateInvoice {
  id: number,

}