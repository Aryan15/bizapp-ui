import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoTransactionPopupComponent } from './auto-transaction-popup.component';

describe('AutoTransactionPopupComponent', () => {
  let component: AutoTransactionPopupComponent;
  let fixture: ComponentFixture<AutoTransactionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoTransactionPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoTransactionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
