import { Component, Input, OnInit, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable ,  merge ,  of as observableOf } from 'rxjs';
import { catchError ,  map ,  debounceTime ,  startWith ,  switchMap } from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';
import { TransactionType } from '../../data-model/transaction-type';
import { QuotationService } from '../../services/quotation.service';
import { QuotationHeader, RecentQuotation } from '../../data-model/quotation-model';

const PAGE_SIZE: number = 10;

@Component({
  selector: 'recent-quotation-table',
  templateUrl: './recent-quotation-table.component.html',
  styleUrls: ['./recent-quotation-table.component.scss']
})
export class RecentQuotationTableComponent implements OnInit, OnChanges {

  @Input()
  recentQuotationLoadCounter: number;

  @Input()
  transactionType : TransactionType;

  @Output()
  quotationHeader: EventEmitter<QuotationHeader> = new EventEmitter<QuotationHeader>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();

  displayedColumns = ['quotationNumber', 'quotationDate', 'grandTotal', 'partyName'];
  quotationDatabase: QuotationHttpDao | null;

  dataSource = new MatTableDataSource<QuotationHeader>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private quotationService: QuotationService) { }


  ngOnInit() {


    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.quotationDatabase = new QuotationHttpDao(this.quotationService);

    //If the user changes sort order, reset back to first page
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.quotationDatabase!.getQuotations(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        //console.log('data.materialList: ', data.quotationHeaders);
        //console.log('data.totalCount: ', data.totalCount);
        if( this.filterForm.get('filterText').value != null)
        {
         if(data.totalCount === 0)
         {
           this.noDataFound.emit(true);
         }
        }
        return data.quotationHeaders;
      }),
      catchError(() => {
        this.isLoadingResults = false;
       
        //console.log('catchError error');
        return observableOf([]);
      })
     
      ).subscribe(data => this.dataSource.data = data);
   
  }
  ngOnChanges(){
    //this.onChangeEvent.
    // //console.log('this.recentInvoiceLoadCounterObservable: ',this.recentInvoiceLoadCounterObservable);
    // this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);
    //console.log('in onchanage: ', this.recentQuotationLoadCounter);

    if(this.recentQuotationLoadCounter > 0 ){
      this.quotationDatabase = new QuotationHttpDao(this.quotationService);

      this.quotationDatabase!.getQuotations(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize).pipe(
      map(data => {
        this.resultsLength = data.totalCount;
        //console.log('data.materialList: ', data.quotationHeaders);
        //console.log('data.totalCount: ', data.totalCount);
        if( this.filterForm.get('filterText').value != null)
        {
        if(data.totalCount === 0)
        {
          this.noDataFound.emit(true);
        }
        }
        return data.quotationHeaders;
      })).subscribe(data => this.dataSource.data = data);
    }
  }

  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.quotationHeader.emit(row);
  }

}


export class QuotationHttpDao {

  constructor(private quotationService: QuotationService) { }

  getQuotations(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentQuotation> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);


    // this.materialService.getAllMaterialsForReport(sortColumn, sortDirection, page, pageSize).subscribe(response => {
    //   ////console.log("response: ", response);
    // })

    return this.quotationService.getRecentQuotations(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);


  }

}