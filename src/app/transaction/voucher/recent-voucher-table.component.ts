import { Component,Input, OnInit,OnChanges,Output,EventEmitter, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Observable, merge, of as observableOf} from 'rxjs';
import {catchError,  map,  debounceTime , startWith, switchMap} from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';

import { TransactionType } from '../../data-model/transaction-type';
import { VoucherHeader, RecentVoucher } from '../../data-model/voucher-model';
import { VoucherService } from '../../services/voucher.service';

const PAGE_SIZE: number = 10;

@Component({
    selector: 'recent-voucher-table',
    templateUrl: './recent-voucher-table.component.html',
    styleUrls: ['./recent-voucher-table.component.scss']
  })
  export class RecentVoucherTableComponent implements OnInit, OnChanges {

    @Input()
    recentVoucherLoadCounter: number;

    @Input()
    transactionType : TransactionType;

    @Output()
    voucherHeader: EventEmitter<VoucherHeader> = new EventEmitter<VoucherHeader>();

    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    displayedColumns = ['voucherNumber', 'paidTo','voucherDate', 'amount'];
    voucherDatabase: VoucherHttpDao | null; 

    dataSource = new MatTableDataSource<VoucherHeader>();

    filterForm = new FormGroup ({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
  
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private voucherService: VoucherService) { }


    ngOnInit(){

      //console.log('in init');

        this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
    
        this.voucherDatabase = new VoucherHttpDao(this.voucherService);

        //If the user changes sort order, reset back to first page

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        

    }

    ngOnChanges(){
    
      //console.log('in onchanage: ', this.recentVoucherLoadCounter);
  
      if(this.recentVoucherLoadCounter > 0 ){
     
        merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
        .pipe(
          startWith({}),
          switchMap(() => {
            this.isLoadingResults = true;
            //console.log("this.filterForm.get('filterText').value",this.filterForm.get('filterText').value);
            return this.voucherDatabase!.getVouchers(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
          }),
          map(data => {
            //Flip flag to show that loading has finished
            this.isLoadingResults = false;
            this.resultsLength = data.totalCount;
            //console.log('data.materialList: ', data.voucherHeaders);
            //console.log('data.totalCount: ', data.totalCount);
            //console.log("this.filterForm.get('filterText').value:", this.filterForm.get('filterText').value)
           if( this.filterForm.get('filterText').value != null)
           {
            if(data.totalCount === 0)
            {
              this.noDataFound.emit(true);
            }
           }
            return data.voucherHeaders;
          }),
          catchError((error) => {
            this.isLoadingResults = false;
            //console.log('catchError', error);
            return observableOf([]);
          })
        ).subscribe(data => this.dataSource.data = data);

      }
    }
  
    onRowClick(row: any){
        //console.log('row clicked: ', row );
        this.voucherHeader.emit(row);
        //console.log('done' );
    }

  }


  export class VoucherHttpDao{
    
      constructor(private voucherService: VoucherService) { }
    
      getVouchers(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentVoucher>{
    
        pageSize = pageSize ? pageSize : PAGE_SIZE;
        //console.log('filterText: ', filterText);
    
    
        return this.voucherService.getRecentVouchers(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);
      }
    }