import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { TransactionType } from '../../data-model/transaction-type';
import { ExpenseHeader, VoucherHeader, VoucherItem, VoucherWrapper } from '../../data-model/voucher-model';
import { ExpensePopupComponent } from '../../master/expense-popup/expense-popup.component';
import { CompanyService } from '../../services/company.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { VoucherSecondReportService } from '../../services/template2/voucher-report.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { VoucherService } from '../../services/voucher.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { PrintDialog } from '../../utils/print-dialog';
import { PrintDialogContainer } from '../../utils/print-dialog-container';
import { UtilityService } from '../../utils/utility-service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';

@Component({
    selector: 'app-voucher',
    templateUrl: './voucher.component.html',
    styleUrls: ['./voucher.component.scss']
})
export class VoucherComponent implements OnInit, OnDestroy {
    @ViewChild('voucherNumber', {static: true}) voucherNumberElement: ElementRef;
    public voucherForm: FormGroup;

    // subheading : String = "this page is used to record Information about ";
    item_length : number = 200;

    public company: Company;
    
    saveStatus: boolean = false;
    recentVoucherLoadCounter: number = 0;
    spinStart: boolean = false;
    isChequeVoucher: boolean = false
    onDestroy: Subject<boolean> = new Subject();
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;
    defaultDate: string;
    financialYear: FinancialYear;
    fieldValidatorLabels = new Map<string, string>();
    isAutoNumber: boolean = false;
    disableAddButton: boolean = false;
    statusBasedPermission: StatusBasedPermission;
    itemToBeRemove: string[] = [];
    expenses: ExpenseHeader[] = [];
    minDate: Date;
    maxDate: Date;
    expense: ExpenseHeader;
    datePipe = new DatePipe('en-US');
    isTermsAndcondition: boolean = false;
    numberRangeConfiguration: NumberRangeConfiguration;
    transactionType: TransactionType;
    voucherHeader: VoucherHeader;
    transactionStatus: string = null;
    showTheCreatedBy=false;
    applicableButtons: ApplicableButtons = {
        isApproveButton: false,
        isClearButton: true,
        isCloseButton: true,
        isCancelButton: false,
        isDeleteButton: true,
        isPrintButton: true,
        isSaveButton: true,
        isEditButton: true,
        isDraftButton: false,
    };

    activityRoleBindings: ActivityRoleBinding[];
    enableSaveButton: boolean = false;
    amount: number;
    tempTransactionNumber: string;
    activityId:number=0;
    description: string;
    matExpenseHead: ExpenseHeader;
    printRef: MatDialogRef<PrintDialog>;
    constructor(private fb: FormBuilder,
        private _router: Router,
        private alertService: AlertService,
        public dialog: MatDialog,
        public userService: UserService,
        private voucherService: VoucherService,
        private companyService: CompanyService,
        private financialYearService: FinancialYearService,
        private utilityService: UtilityService,
        private voucherSecondReportService: VoucherSecondReportService,
        private termsAndConditionsService: TermsAndConditionsService,
        private numberRangeConfigService: NumberRangeConfigService,
        private transactionTypeService: TransactionTypeService,
        public route: ActivatedRoute,
        private transactionItemService: TransactionItemService,
        private locationService: LocationService) {
        this.setActivityRoleBinding();
        this.setTransactionType();

    }

    ngOnInit() {
        this.company = this.companyService.getCompany();

        this.initForm();
        this.getFinancialYear();
        this.getDefaultDate();


    }
    ngOnDestroy() {
        //console.log('ngOnDestory');
        this.onDestroy.next(true);
        this.onDestroy.complete();
    }
    setActivityRoleBinding() {
        this.userService.setActivityRoleBinding(this.route).subscribe(response => {
            this.activityRoleBindings = response;
            //console.log("activityRoleBindings: ", this.activityRoleBindings)
            this.activityRoleBindings.forEach(activityRoleBinding => {
                this.activityId=activityRoleBinding.activityId;
                this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
               });
        })
    }


    private initForm() {
        let data: VoucherHeader = {
            id: null,
            amount: null,
            voucherNumber: null,
            voucherId: null,
            voucherDate: this.defaultDate,
            voucherTypeId: null,
            comments: null,
            paidTo: null,
            chequeNumber: null,
            financialYearId: null,
            companyId: null,
            bankName: null,
            chequeDate: null,
            voucherItems: null,
            // .............
            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile : null,
            companySecondaryMobile : null,
            companyContactPersonNumber : null,
            companyContactPersonName : null,
            companyPrimaryTelephone : null,
            companySecondaryTelephone : null,
            companyWebsite : null,
            companyEmail : null,
            companyFaxNumber : null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber : null,
            companyPanNumber : null,
            companyPanDate : null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null

        };
        this.voucherForm = this.toFormGroup(data);
        this.handleChanges();

    }


    getDefaultDate() {

        this.utilityService.getCurrentDateObs().subscribe(response => {
            this.defaultDate = response;
            this.initForm();
        })
    }


    clearFormNoStatusChange() {
        this.initForm();

        this.handleChanges();
        
    }


    setTransactionType() {

        this.route.params.
            pipe(takeUntil(this.onDestroy))
            .subscribe(params => {
                this.transactionTypeService.getTransactionType(+params['trantypeId'])
                    .subscribe(response => {
                        this.transactionType = response;
                        //console.log("Transaction type in Voucher ", this.transactionType.name);
                        if (this.transactionType.name === AppSettings.CHEQUE_VOUCHER) {
                            this.isChequeVoucher = true;
                          }
 
                      
                        this.getExpense();
                       
                        this.getAutoNumber();

                        
                        setTimeout(() => {
                            this.recentVoucherLoadCounter++;
                        }, 500);

                        this.getTempTransactionNumber();
                    })

                // if (params['id']) {
                //     //console.log('got id: ', params['id']);
                //     this.voucherService.getVoucher(params['id']).subscribe(response => {
                //         this.recentVoucherGet(response);
                //     })
                // }
            });


    }



    getAutoNumber() {
        this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id).subscribe(response => {
            this.numberRangeConfiguration = response;

            this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
            //console.log("isAutoNumber" + this.isAutoNumber)
        }
        )
    }


    private toFormGroup(data: VoucherHeader): FormGroup {

        const itemArr = new FormArray([]);

        if (data.voucherItems) {
            data.voucherItems.forEach(item => {
                itemArr.push(this.makeItem(item));
            })
        }

        const formGroup = this.fb.group({
            id: [data.id],
            amount: [data.amount, [Validators.required]],
            voucherNumber: [data.voucherNumber],
            voucherId: [data.voucherId],
            voucherDate: [this.datePipe.transform(data.voucherDate, AppSettings.DATE_FORMAT), [Validators.required]],
            voucherTypeId: [data.voucherTypeId],
            comments: [data.comments],
            paidTo: [data.paidTo, [Validators.required]],
            chequeNumber: [data.chequeNumber],
            bankName: [data.bankName],
            chequeDate: [this.datePipe.transform(data.chequeDate, AppSettings.DATE_FORMAT)],
            financialYearId: [data.financialYearId],
            companyId: [data.companyId],
            voucherItems: itemArr,
            // ..........
            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile : [data.companyPrimaryMobile],
            companySecondaryMobile : [data.companySecondaryMobile],
            companyContactPersonNumber : [data.companyContactPersonNumber],
            companyContactPersonName : [data.companyContactPersonName],
            companyPrimaryTelephone : [data.companyPrimaryTelephone],
            companySecondaryTelephone : [data.companySecondaryTelephone],
            companyWebsite : [data.companyWebsite],
            companyEmail : [data.companyEmail],
            companyFaxNumber : [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber : [data.companyGstNumber],
            companyPanNumber : [data.companyPanNumber],
            companyPanDate : [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[data.createdDate],
            updatedDate:[data.updatedDate]
            // partyName: [data.companyName],

        });
        formGroup.get('voucherItems').setValidators(Validators.required);
        this.fieldValidatorLabels.set("voucherNumber", "Voucher Number");
        this.fieldValidatorLabels.set("voucherDate", "Voucher Date");
        this.fieldValidatorLabels.set("voucherItems", "Voucher Items");
        this.fieldValidatorLabels.set("paidTo", "Paid To");
        this.fieldValidatorLabels.set("chequeNumber", "Cheque Number");
        this.fieldValidatorLabels.set("chequeDate", "Cheque Date");
        this.fieldValidatorLabels.set("bankName", "Bank Name");
        this.fieldValidatorLabels.set("amount", "amount");
        return formGroup;
    }


    makeItem(voucherItem: VoucherItem): FormGroup {
        return this.fb.group({
            id: voucherItem.id,
            voucherHeaderId: voucherItem.voucherHeaderId,
            description: voucherItem.description,
            expenseHeaderId: voucherItem.expenseHeaderId,
            expenseName: voucherItem.expenseName,
            amount: voucherItem.amount
        });
    }

    itemChanged(event) {
        //console.log("sdsdsd")
        this.handleChanges();

    }

    getFinancialYear() {
        this.financialYearService.getFinancialYear().subscribe(
            response => {
                this.financialYear = response;
                this.minDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                this.maxDate = new Date(this.financialYear.endDate);
                //console.log("this.minDate", this.minDate)
                //console.log("this.maxDate", this.maxDate)
            }
        )
    }

    handleChanges() {
        let items: VoucherItem[] = this.voucherForm.controls['voucherItems'].value;
        this.updateValidationForCheqVouchr();
        
        //console.log("items", items)
        this.handleItems(items);
    }
    delete() {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE


        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.voucherForm.controls['id'].value;//this.quotationHeader.id;
                //console.log("deleteing..." + id);
                this.voucherService.delete(id)
                    .subscribe(response => {
                        //console.log(response);
                        if (response.responseStatus === 1) {
                            this.transactionStatus = response.responseString;
                            this.saveStatus = true;
                            this.recentVoucherLoadCounter++;
                            this.spinStart = false;
                            this.clearFormNoStatusChange();
                            this.alertService.success(this.transactionStatus);
                        } else {
                            this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                            this.transactionStatus = response.responseString;
                            this.saveStatus = false;
                            this.spinStart = false;
                            this.alertService.danger(response.responseString);
                        }
                    },
                    error => {
                        //console.log("Error ", error);
                        this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                        this.alertService.danger(this.transactionStatus);
                        this.saveStatus = false;
                        this.spinStart = false;
                    });
            }
            this.dialogRef = null;
        });
    }


    public findInvalidControls() {
        let invalid = [];
        let controls = this.voucherForm.controls;
        ////console.log("controls "+controls.value)
        for (let name in controls) {
            // //console.log("name "+controls[name].value)
            if (controls[name].invalid) {
                // //console.log("name of cntr "+controls[name])
                invalid.push(name);
                ////console.log("invalid "+invalid);
            }
        }
        return invalid;
    }

    findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
        //console.log("formToInvestigate" + formToInvestigate);
        var invalidControls: string[] = [];
        let recursiveFunc = (form: FormGroup | FormArray) => {
            Object.keys(form.controls).forEach(field => {
                const control = form.get(field);
                // //console.log("control"+control +"field"+field);
                if (control.invalid) {

                    invalidControls.push(field);
                }
                if (control instanceof FormGroup) {
                    recursiveFunc(control);
                } else if (control instanceof FormArray) {
                    recursiveFunc(control);
                }
            });
        }
        recursiveFunc(formToInvestigate);
        return invalidControls;
    }

    onSubmit(model: VoucherHeader) {
        if(this.numberRangeConfiguration.id===null){
            
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
            return;
        }

        //console.log("model :",model);
        // let voucherNumber: string = this.voucherForm.get('invoiceNumber').value
        // //console.log("invoiceNumber :",voucherNumber);
        
        if (model.voucherNumber == null && !this.isAutoNumber || model.voucherNumber == "") {
            this.alertService.danger("Please Enter Voucher Number");
            return;
        }

        let checkNumAvail: boolean = false;
        //check number avail      
        if(this.isAutoNumber){ //in case of auto number do not check
            checkNumAvail = false;
        }
        else{
            if(model.id != null){ //if we are editing invoice which is not auto number
                if(model.voucherNumber != this.voucherHeader.voucherNumber){ // if invoice number is changed
                    checkNumAvail = true;
                }else{
                    checkNumAvail = false;
                }
            }else{ // if not auto number and creating new invoice, always check
                checkNumAvail = true;
            }
        }
        
        if(checkNumAvail){
            this.voucherService.checkVoucherNumAvailability(model.voucherNumber, this.transactionType.id)
            .subscribe(response => {
                if (response.responseStatus === 1) {
                    this.save(model);
                } else {
                    //console.log("No");
                    this.alertService.danger("Invoice Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                    this.voucherForm.controls['voucherNumber'].patchValue('');
                    this.voucherNumberElement.nativeElement.focus();
    
                }
    
            });
        }
        else{
            this.save(model);
        }


    }
    save(model: VoucherHeader){
        //console.log("model : ",model);

        if(model.id===null){
            model = this.transactionItemService.patchValueFromCompanyAndParty(model, null, this.company, false)
            //console.log("model : ",model);
        }

        //console.log('this.voucherForm.valid: ', this.voucherForm.valid);
        const controls = this.voucherForm;
        if (!this.voucherForm.valid) {
            const controls = this.voucherForm;
            let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
            //console.log("invalidFieldList ", invalidFieldList)

            let invalidFiledLabels: string[] = [];

            invalidFieldList.forEach(field => {
                if (this.fieldValidatorLabels.get(field))
                    invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })

            //console.log("invalidFiledLabels ", invalidFiledLabels)

            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
            return false;
        }

        // if (model.voucherNumber == null && !this.isAutoNumber || model.voucherNumber == "") {
        //     this.alertService.danger("Please Enter Transaction Number");
        //     return;
        // }

        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id;
        model.voucherTypeId = this.transactionType.id;
        this.spinStart = true;
        //console.log("before save: ", model);
        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        }
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }

        let voucherWrapper: VoucherWrapper = { itemToBeRemove: this.itemToBeRemove, voucherHeader: model };

        //console.log("voucherWrapper :", voucherWrapper);
        
        this.voucherService.create(voucherWrapper)
            .subscribe(response => {
                //console.log("saved id: ", response);
                this.showTheCreatedBy=true;
                this.voucherHeader = response;
                this.transactionStatus = "Voucher " + response.voucherNumber + message;
                this.alertService.success(this.transactionStatus);
                this.saveStatus = true;
                this.spinStart = false;
                this.recentVoucherLoadCounter++;

                // this.disableForm();
                this.voucherForm = this.toFormGroup(this.voucherHeader);
                if(!this.voucherHeader.updateBy){
                    this.voucherService.getVoucher(this.voucherHeader.id)
                    .subscribe(responses => {
                        this.voucherForm.controls['updateBy'].patchValue(responses.updateBy);
                        this.voucherForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));

                           

                    });
                }
                this.disableForm();
            },
            error => {
                //console.log("Error ", error);
                this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
            });
    }

    clearForm() {
       // formDirective.resetForm();
        this.saveStatus = false;
        this.transactionStatus = null;
        this.initForm();
        this.handleChanges();
        //this.voucherForm.markAsPristine();
        this.expense = undefined;
        this.matExpenseHead = undefined;
        this.description = undefined;
        this.amount = undefined;
        this.getTempTransactionNumber();
        //  this.voucherForm.controls['supplierName'].patchValue('');

    }

    closeForm() {
        // this._router.navigateByUrl('/dashboard')
        this._router.navigate(['/']);
    }

    setStatusBasedPermission(statusId: number) {

        if (statusId) {
            //console.log('status id: ', statusId);
            this.userService.getStatusBasedPermission(statusId).subscribe(response => {
                this.statusBasedPermission = response;
            })
        } else {
            //console.log("no status id: ", statusId);
        }
    }

    recentVoucherGet(event) {
        //console.log('In recentVoucherGet', event);
        this.showTheCreatedBy=false;
        this.voucherHeader = event;
        this.voucherForm = this.toFormGroup(this.voucherHeader);
        let dcFound: boolean = false;

        ////console.log(this.invoiceForm.controls('invoiceItems').)
        this.disableForm();
    }
    disableForm() {
        this.voucherForm.disable({ onlySelf: true, emitEvent: false });
        // this.disableParty = true;
    }

    removeVoucherItem(idx: number) {

        // (<FormArray>this.voucherForm.get('voucherItems')).removeAt(idx);

        // return false;

        let itemId: string = (<FormArray>this.voucherForm.get('voucherItems')).at(idx).get("id").value;
        //console.log("Itemid " + itemId)
        if (itemId) {

            //console.log("before confirmationn dialog")
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

            this.dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.itemToBeRemove.push(itemId);
                    (<FormArray>this.voucherForm.get('voucherItems')).removeAt(idx);
                }
                else
                {
                    this.disableAddButton = true;
                }
                //console.log("dialog open");
                this.handleChanges();
            });
        } else {
            //
            // else just delete in the array
            //
            (<FormArray>this.voucherForm.get('voucherItems')).removeAt(idx);
            this.handleChanges();
        }
        this.disableAddButton = false;
        return false;

    }


    enableForm() {
        this.voucherForm.enable({ onlySelf: true, emitEvent: false });
    }
    edit() {
        this.disableAddButton = true;
        this.enableForm();

    }
    whenNoDataFound(noDataFound: boolean) {
        if (noDataFound) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE
        }
    }
    getExpense() {
        this.voucherService.getAllExpense().subscribe(
            response => {

                this.expenses = response;
                //console.log("this.expenses" + this.expenses);
            }
        )
    }

    addItem() {

        let control = <FormArray>this.voucherForm.controls['voucherItems'];


        //console.log('control: ', control + "amount " + this.amount + " expenseHeaderId " + this.expense + " description " + this.description + " matExpenseHead " + this.matExpenseHead);

        control.push(this.initTransactionItem(control.length + 1, this.matExpenseHead, this.amount, this.description));

        this.expense = undefined;
        this.matExpenseHead = undefined;
        this.description = undefined;
        this.amount = undefined;
        this.handleChanges();

        if (control.length > 0) {
            this.disableAddButton = true;
        }
        return false;
    }

    handleItems(items: VoucherItem[]) {

        let totalAmount: number = 0;
        items.forEach(item => {
            totalAmount += +item.amount;
        })
        //console.log("items.length",items.length);
        if(items.length >= 1){
            this.disableAddButton = true;
        }
        else
        {
            this.disableAddButton = false;
        }
        //console.log('tottalAmount: ', totalAmount);
        this.voucherForm.controls['amount'].patchValue(totalAmount);

    }

    initTransactionItem(slNoIn: number
        , expense: ExpenseHeader
        , amount: number
        , description: string
    ): FormGroup {

        let voucherItem: VoucherItem = {
            id: null,
            voucherHeaderId: null,
            expenseHeaderId: expense.id,
            expenseName: expense.name,
            amount: amount,
            description: description
        }

        return this.makeItem(voucherItem);


    }

    print() {

        // let company: Company;
        // this.companyService.get(1).subscribe(comp => {
        //     company = comp;
        //     // this.deliveryChallanReportService.download(this.dcHeader, company);
        //     this.deliveryChallanSecondReportService.download(this.dcHeader, company);
        // });

        let l_printCopy: number;
        this.printRef = this.dialog.open(PrintDialogContainer, {
            // width: '250px',
            data: {
                transactionType: this.transactionType, 
                printDataModel: this.voucherForm.value, 
                printCopy: l_printCopy, 
                globalSetting: null,
                numberRangeConfiguration: this.numberRangeConfiguration
             }
        });
        // this.printRef.afterClosed().subscribe(result => {
        //     //console.log("result: ", result);
        //     if (result) {
        //         l_printCopy = +result;
        //         let company: Company;
        //         this.companyService.get(1).subscribe(comp => {
        //             company = comp;
        //             this.voucherSecondReportService.download(this.voucherHeader, company, l_printCopy, this.transactionType);
        //         });
        //     }

        // })

    }

    updateValidationForCheqVouchr() {
        //console.log(' afetr isChequeVoucher:');
        if (this.isChequeVoucher) {
            //console.log(' afetr isChequeVoucher: false case ', this.isChequeVoucher);
             this.voucherForm.controls['chequeNumber'].setValidators(Validators.required);
            this.voucherForm.controls['chequeDate'].setValidators(Validators.required);
        }
        else {
            this.voucherForm.controls['chequeNumber'].patchValue("");
            this.voucherForm.controls['chequeNumber'].setValidators(null);
            this.voucherForm.controls['chequeDate'].patchValue("");
            this.voucherForm.controls['chequeDate'].setValidators(null);
           
        }
        this.voucherForm.controls['chequeDate'].updateValueAndValidity();
        this.voucherForm.controls['chequeNumber'].updateValueAndValidity();
      
    }
    getTempTransactionNumber() {
        //console.log("this.transactionType.name" + this.transactionType.name);
        this.transactionTypeService.getTransactionNumber(this.transactionType.name)
            .pipe(
            takeUntil(this.onDestroy)
            )
            .subscribe(response => {
                this.tempTransactionNumber = response.responseString;
            })
    }


    // checkVoucherNumAvailability(){
    //     //console.log("this.transactionType....", this.transactionType.id);
        
    //     let voucherNumber: string = this.voucherForm.get('voucherNumber').value
    //     if (voucherNumber && voucherNumber.length > 0 ) {
      
    //       this.voucherService.checkVoucherNumAvailability(voucherNumber, this.transactionType.id).subscribe(response => {
 
    //         //console.log("is voucherNumber avialable? ", response);
    //         if (response.responseStatus === 1) {
    //           //console.log("Yes");
    //         } else {
    //           //console.log("No");
    //           this.alertService.danger( "voucherNumber Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //             this.voucherForm.controls['voucherNumber'].patchValue('');
    //            this.voucherNumberElement.nativeElement.focus();
 
    //         }
 
    //       });
    //     }
    // }



    addNewExpense() {
        //console.log('addNewExpense() clicked');
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            // id: 1,
            // title: 'Angular For Beginners'
        };

        const dialogRef = this.dialog.open(ExpensePopupComponent, dialogConfig);
        // //console.log("dialogRef......!!!!!!!!!!!!!!!!!!!!!!!", dialogRef['result']);

        dialogRef.afterClosed().subscribe(
            data => {
                if (data) {
                    data.companyId = 1; //TO DO
                    data.supplyTypeId = 1;
                    //console.log("Dialog output:", data)
                    this.voucherService.saveExpense([data]).subscribe(response => {
                        //console.log("Dialog response:", response)

                        let status = response[0].name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                        this.alertService.success(status);
                        this.expenses.push(response[0]);
                        this.matExpenseHead = response[0];

                    },
                    error => {
                        //console.log("Error ", error, error.exception);
                        
                        if(error instanceof HttpErrorResponse){
                            var re = /DataIntegrityViolationException/gi;
                            if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
                            this.transactionStatus = "Duplicate Expense Name!! Cannot save";
                        }
                        this.alertService.danger(this.transactionStatus);
                        this.saveStatus = false;
                        this.spinStart = false;
                    });
                }
            });

    }

    helpVideos() {
        
        let status: string = "";
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.id =this.activityId.toString();
     
        
        const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {

            });



        
    }




}
