
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { QuantityChangeEvent } from '../../data-model/misc-model';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
@Component({
    selector: 'voucher-item',
    templateUrl: 'voucher-item.component.html',
    styleUrls: ['voucher-item.component.scss']
})
export class VoucherItemComponent implements OnInit {
     @Input() group: FormGroup; 
    @Input() index: number;
     @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
     @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
     @Output() quantityChange: EventEmitter<QuantityChangeEvent>= new EventEmitter<QuantityChangeEvent>();
     private changeCtr: number = 0;
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;
    constructor(
        public dialog: MatDialog, ) {

    }
    ngOnInit() {

    }

   

    onQuantityChange() {
        //console.log('onQuantityChange()');
        
        let lquantity: number = this.group.controls['amount'].value;
      //  let materialId: number = this.group.controls['materialId'].value;
       
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);
    }

    delete(index: number) {
        
                //console.log("Deleting...", index);
                this.deleteIndex.emit(index);
            }
}
