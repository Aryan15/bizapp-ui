import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EwayBillTracking, TransactionMode, VehicleType, ReasonCode, cancelCode, InvoiceHeader } from '../../data-model/invoice-model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UtilityService } from '../../utils/utility-service';
import { AlertService } from 'ngx-alerts';
import { AppSettings } from '../../app.settings';
import { InvoiceService } from '../../services/invoice.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-ewaybill-popup',
  templateUrl: './ewaybill-popup.component.html',
  styleUrls: ['./ewaybill-popup.component.scss']
})
export class EwaybillPopupComponent implements OnInit {

  type: number;
  heading: string;
  errorMessage: string = "";
  fieldValidatorLabels = new Map<string, string>();
  themeClass: any;
  currentUser: any;
  datePipe = new DatePipe('en-US');
  maxDate = new Date();
  invDate= new Date();
  transDocDate= new Date();
  invoiceHeader: InvoiceHeader;
  public vehiclePopUpForm: FormGroup;
  public ewayBillTracking: EwayBillTracking;
  transactionModes: TransactionMode[] = [
    { id: 1, name: 'Road' },
    { id: 2, name: 'Rail' },
    { id: 3, name: 'Air' },
    { id: 4, name: 'Ship' },
  ];

  vehicleTypes: VehicleType[] = [
    { id: 1, name: 'Regular', code: 'R' },
    { id: 2, name: 'ODC(Over Dimentional Cargo)', code: 'O' },
  ];

  reasonCodes: ReasonCode[] = [
    { id: 1, name: 'Due to Break Down' },
    { id: 2, name: 'Due to Transshipment' },
    { id: 3, name: 'Others (Pls. Specify)' },
    { id: 4, name: 'SFirst Timehip' },
  ];

  cancelCodes: cancelCode[] = [
    { id: 1, name: 'Duplicate' },
    { id: 2, name: 'Order Cancelled' },
    { id: 3, name: 'Data Entry mistake' },
    { id: 4, name: 'Others' },
  ];

  minDate = new Date();

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<EwaybillPopupComponent>,
    private utilityService: UtilityService,
    private alertService: AlertService,
    private invoiceService: InvoiceService,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }

  ngOnInit() {
    this.initForm();
    this.type = Number(this.dialogRef.id);

    if (this.type == 1) {
      this.heading = "Vehicle Information"
    } else if (this.type == 2) {
      this.heading = "Transporter ID Information"
    }
    else if (this.type == 3) {
      this.heading = "Cancel E-Way bill"

    }
    console.log
    this.vehiclePopUpForm.get("vehicleType").patchValue('R');
    this.vehiclePopUpForm.get("transactionMode").patchValue(1);
    this.vehiclePopUpForm.get("cancelledReason").patchValue(1);

  }

  private initForm() {
    let data: EwayBillTracking = {
      id: null,
      eWayBillNo: null,
      eWayBillDate: null,
      validUpto: null,
      docDate: null,
      docNo: null,
      pdfUrl: null,
      detailedpdfUrl: null,
      jsonData: null,
      invoiceHeaderId: null,
      status: null,
      error: null,
      transporterId: null,
      transporterName: null,
      transDocNo: null,
      vehicleNo: null,
      portName: null,
      portStateId:null,
      portPin: null,
      cancelledDate: null,
      response: null,
      vehicleType: null,
      reasonCode: null,
      reasonRemarks: null,
      transactionMode: null,
      remainingDistance: null,
      extensionReasonCode: null,
      extensionReasonRemarks: null,
      fromPlace: null,
      fromState: null,
      fromPincode: null,
      transDocDate: null,
      addressLine1: null,
      addressLine2: null,
      addressLine3: null,
      transDistance: null,
      noOfItems: null,
      hsnCode: null,
      isCurrent: null,
      cancelledReason: null,
      cancelledRemarks: null,
    };

    this.vehiclePopUpForm = this.toFormGroup(data);

  }

  private toFormGroup(data: EwayBillTracking): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      eWayBillNo: [data.eWayBillNo],
      eWayBillDate: [data.eWayBillDate],
      validUpto: [data.validUpto],
      docDate: [data.docDate],
      docNo: [data.docNo],
      pdfUrl: [data.pdfUrl],
      detailedpdfUrl: [data.detailedpdfUrl],
      jsonData: [data.jsonData],
      invoiceHeaderId: [data.invoiceHeaderId],
      status: [data.status],
      error: [data.error],
      transporterId: [data.transporterId, Validators.compose([Validators.minLength(1), Validators.maxLength(15)])],
      transporterName: [data.transporterName],
      transDocNo: [data.transDocNo, Validators.compose([Validators.maxLength(15)])],
      vehicleNo: [data.vehicleNo, Validators.compose([Validators.minLength(7), Validators.maxLength(15)])],
      portName: [data.portName],
      portStateId:[data.portStateId],
      portPin: [data.portPin],
      cancelledDate: [data.cancelledDate],
      response: [data.response],
      vehicleType: [data.vehicleType],
      reasonCode: [data.reasonCode, Validators.required],
      reasonRemarks: [data.reasonRemarks, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(50)])],
      transactionMode: [data.transactionMode, Validators.required],
      remainingDistance: [data.remainingDistance],
      extensionReasonCode: [data.extensionReasonCode],
      extensionReasonRemarks: [data.extensionReasonRemarks],
      fromPlace: [data.fromPlace],
      fromState: [data.fromState],
      fromPincode: [data.fromPincode],
      transDocDate: [data.transDocDate],
      addressLine1: [data.addressLine1],
      addressLine2: [data.addressLine2],
      addressLine3: [data.addressLine3],
      transDistance: [data.transDistance],
      noOfItems: [data.noOfItems],
      hsnCode: [data.hsnCode],
      isCurrent: [data.isCurrent],
      cancelledReason: [data.cancelledReason],
      cancelledRemarks: [data.cancelledRemarks, Validators.compose([Validators.minLength(1), Validators.maxLength(250)])],
    });

    return formGroup;

  }

  update(model: EwayBillTracking) {

    this.invoiceService.getInvoice(model.invoiceHeaderId).subscribe( response =>{
      this.invoiceHeader = response;
      if(this.invoiceHeader){
        this.invDate =  new Date(this.invoiceHeader.invoiceDate);
      }
    })

    if (this.dialogRef.id == "1") {

      this.errorMessage = "";
      //console.log("invalidFiledLabels ", invalidFiledLabels)
      if (model.transactionMode == 1) {
        if (model.vehicleNo == null || model.vehicleNo == "") {
          this.errorMessage = "If mode of transportation is ‘Road’, then the Vehicle number should be passed.";
          this.alertService.danger(this.errorMessage);
          return;
        }

      } else  {
        if ((model.transDocNo == null || model.transDocNo == "")) {
          this.errorMessage = "If mode of transportation is Ship, Air, Rail, then the transport document number and date should be passed.";
          this.alertService.danger(this.errorMessage);
          return;
        }

        if ((model.transDocDate == null || model.transDocDate == "")) {
          this.errorMessage = "If mode of transportation is Ship, Air, Rail, then the transport document date should be passed.";
          this.alertService.danger(this.errorMessage);
          return;
        }else{
          this.transDocDate = new Date(model.transDocDate);
           if(this.datePipe.transform(this.invDate, AppSettings.DATE_FORMAT) > this.datePipe.transform(this.transDocDate, AppSettings.DATE_FORMAT) ){
            this.alertService.danger("Transporter document date cannot be earlier than the Invoice Date");
            return;
          }
           
        }

      }

    } else if (this.dialogRef.id == "2") {
      console.log("model.transporterId ", model.transporterId)

      if (model.transporterId == null || model.transporterId == "") {
        this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + "Trnsporter ID");
        return;
      } else if (model.transporterId.length > 15) {
        this.alertService.danger("Transporter ID must be 1-15 characters");
        return;
      }
    } else if (this.dialogRef.id == "3") {

      if (model.cancelledRemarks == null || model.cancelledRemarks == "") {
        this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + " Cancel Reason");
        return;
      }else if (model.cancelledRemarks.length > 250) {
        this.alertService.danger("Remarks must be 1-250 characters");
        return;
      }  
    } 
     this.dialogRef.close(this.vehiclePopUpForm.value);
  }


  close(): void {
    this.dialogRef.close();
  }

}