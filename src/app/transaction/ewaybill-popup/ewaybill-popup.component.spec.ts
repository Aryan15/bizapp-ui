import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EwaybillPopupComponent } from './ewaybill-popup.component';

describe('EwaybillPopupComponent', () => {
  let component: EwaybillPopupComponent;
  let fixture: ComponentFixture<EwaybillPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EwaybillPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EwaybillPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
