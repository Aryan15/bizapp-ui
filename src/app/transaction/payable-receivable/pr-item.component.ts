import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';

@Component({
    selector: 'pay-receive-item',
    templateUrl: 'pr-item.component.html',
    styleUrls: ['pr-item.component.scss']
})
export class PayableReceivableItemComponent implements OnInit {

    @Input() group: FormGroup;
    @Input() index: number;
    @Input() isClearingModeAuto: boolean;
    @Input() statusName: string;
    @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
    @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;

    originalDueAmount: boolean = false;
    constructor( public dialog: MatDialog,)
    {}
    // group = new FormGroup({
    //     fromDate: new FormControl()
    //   });
    private changeCtr: number = 0;
   //  currentDueAmount = 0;
   lpaidAmount
    ngOnInit() {
        // this.group.controls['payingAmount'].valueChanges.subscribe(value => {
        //    this.calculateGridAmount();
        //   this.onPayingAmountChange();
        // })
        // // this.group.controls['payingAmount'].valueChanges.subscribe(value => {
        //     this.changeCtr++;
        //     this.onPayingAmountChange();
        //     //this.changeCounter.emit(this.changeCtr);
        // })
    };

    delete(index: number) {
        //console.log('deleteing... ', index);
        this.deleteIndex.emit(index);
    }

    calculateAmountAfterTds(lamount: number, ltdsAmount: number): number {
        return lamount - ltdsAmount;
    }

    // calculateGridAmount()
    // {
    //    let originalDueAmount: number;
    //     let dueAmount = this.group.controls['dueAmount'].value;
    //     let payingAmount = this.group.controls['payingAmount'].value;
    //     let paidAmount =  this.group.controls['paidAmount'].value;
    //     let lTdsAmount: number = this.group.controls['tdsAmount'].value;
    //     //console.log("isClearingModeAuto",this.isClearingModeAuto)
    //     if(this.isClearingModeAuto === true)
    //     {
    //     this.currentDueAmount = dueAmount - paidAmount - payingAmount;
    //     //console.log("this.currentDueAmount", this.currentDueAmount )
    //     originalDueAmount = this.group.controls['dueAmount'].value;
    //     //console.log("originalDueAmount",originalDueAmount)
    //     this.group.controls['debitAmount'].patchValue(originalDueAmount);
    //     this.group.controls['dueAmount'].patchValue(this.currentDueAmount);
        
    //     }
    // }

    resetTdsPercentage() {
        let amount: number = this.group.controls['taxableAmount'].value;
        let tdsAmount: number = this.group.controls['tdsAmount'].value;
        let tdsPercentage: number = (100 * tdsAmount) / amount;
        tdsPercentage = +tdsPercentage.toFixed(2)
        this.group.controls['tdsPercentage'].patchValue(tdsPercentage, { emitEvent: false });
    }

    resetTdsAmount() {
        let amount: number = this.group.controls['taxableAmount'].value;
        let tdsPercentage: number = this.group.controls['tdsPercentage'].value;
        let tdsAmount: number = amount * tdsPercentage / 100;
        tdsAmount = +tdsAmount.toFixed(2);
        tdsAmount = tdsAmount >= amount ? 0 : tdsAmount;
        this.group.controls['tdsAmount'].patchValue(tdsAmount, { emitEvent: false });


    }

    patchAmountAfterTds() {
        let ltotalDueAmount: number;
        let ldueAmountAfterTds: number;
        let lafterDebit: number;

        ////console.log('patchAmountAfterTds.', this.group.controls['amount'].value, this.group.controls['TdsAmount'].value);
        let lamount: number = this.group.controls['taxableAmount'].value;
        let lTdsAmount: number = this.group.controls['tdsAmount'].value;
        let lpaidAmount: number = this.group.controls['paidAmount'].value;
        let lamountAfterTdsAmount: number = this.group.controls['amountAfterTds'].value;
        let lpayingAmount: number = this.group.controls['payingAmount'].value;
        let ldeductAmount: number = this.group.controls['debitAmount'].value;
        let ldueAmount: number = this.group.controls['dueAmount'].value;
        let taxAmount: number = this.group.controls['taxAmount'].value;
        let invoiceAmount: number = this.group.controls['invoiceAmount'].value;
        let preTds: number = this.group.controls['preTdsAmount'].value;
        if (ldeductAmount) {
            if (this.statusName === AppSettings.SAVE_AS_DRAFT_STATUS) {
                // ldueAmountAfterTds=ldueAmount-lTdsAmount;
                ldueAmountAfterTds = invoiceAmount - ldeductAmount
                ldueAmountAfterTds = ldueAmountAfterTds - lTdsAmount
            }

            else {
                ltotalDueAmount = lamount - lpaidAmount;
                lafterDebit = ltotalDueAmount - ldeductAmount;
                ldueAmountAfterTds = lafterDebit - lTdsAmount;
                ldueAmountAfterTds =ldueAmountAfterTds+taxAmount;
            }
        }

       else{
            ltotalDueAmount = lamount - lpaidAmount;
          
          ldueAmountAfterTds = ltotalDueAmount - lTdsAmount;
       
        }

        if (!ldeductAmount||ldeductAmount === 0) {
            ldueAmountAfterTds = ldueAmountAfterTds + taxAmount;
        }
        if ((lpaidAmount || lpaidAmount != 0) && this.statusName != AppSettings.SAVE_AS_DRAFT_STATUS) {
            ldueAmountAfterTds = ldueAmountAfterTds - lTdsAmount;
        }
        if ((lpaidAmount || lpaidAmount != 0) && this.statusName != AppSettings.SAVE_AS_DRAFT_STATUS && (!preTds||preTds===0)) {
           let td:number= ldueAmountAfterTds - (-lTdsAmount)
            ldueAmountAfterTds= td
         

        
        }
        

        if ((lpaidAmount || lpaidAmount != 0) && this.statusName === AppSettings.SAVE_AS_DRAFT_STATUS) {
            ldueAmountAfterTds = (invoiceAmount - preTds) - lpaidAmount
            ldueAmountAfterTds = ldueAmountAfterTds - lTdsAmount
        }
        if (this.statusName === AppSettings.SAVE_AS_DRAFT_STATUS && (!lpaidAmount || lpaidAmount == 0)&& (!ldeductAmount)) {
            ldueAmountAfterTds = invoiceAmount - lTdsAmount
        }

        let lamountAfterTds: number = this.calculateAmountAfterTds(lamount, lTdsAmount);
        lamountAfterTds = +lamountAfterTds.toFixed(2);
        lamountAfterTds = lamountAfterTds > lamount ? 0 : lamountAfterTds;

        this.group.controls['amountAfterTds'].patchValue(lamountAfterTds);
        this.group.controls['dueAmount'].patchValue(this.isClearingModeAuto ? 0 : ldueAmountAfterTds);
        this.group.controls['payingAmount'].patchValue(this.isClearingModeAuto ? ldueAmountAfterTds : 0);
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);
    }

    onTdsAmountChange() {
        this.resetTdsPercentage();
        // this.changeCtr++;
        // this.changeCounter.emit(this.changeCtr);

        this.patchAmountAfterTds();

        // this.changeCtr++;
        // this.changeCounter.emit(this.changeCtr);

    }

    onTdsPercentageChange() {
        //console.log('in Tds per change')
        this.resetTdsAmount();
        //this.resetAmounts();
        this.patchAmountAfterTds();

        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);



    }

    onPayingAmountChange() {
        if (this.statusName === AppSettings.SAVE_AS_DRAFT_STATUS) {
            //let lamountAfterTds: number = this.group.controls['amountAfterTds'].value;
            // let amount: number = this.group.get('originalDebitAmount').value;

            // let dueAmountToPay = this.group.get('amountAfterTds').value - this.group.get('originalDebitAmount').value;
            let lpayingAmount: number = this.group.get('payingAmount').value
            let amountAfterTds = this.group.get('amountAfterTds').value;
            let taxAmount = this.group.get('taxAmount').value;
            let lpaidAmount: number = this.group.controls['paidAmount'].value;
            let tdsAmount: number = this.group.controls['tdsAmount'].value;
            let invoiceAmount: number = this.group.controls['invoiceAmount'].value;
            let preTds: number = this.group.controls['preTdsAmount'].value;
            let debitAmount: number = this.group.controls['debitAmount'].value;
            let roundOffAmount: number = this.group.controls['roundOff'].value;
            let taxableAmount: number = this.group.controls['taxableAmount'].value;

            let totalAmount = amountAfterTds + +taxAmount;
            if(preTds){
                totalAmount=totalAmount+preTds;  
            }
    
    let total= taxableAmount+ +taxAmount;
     roundOffAmount = total-invoiceAmount;
   //  lpaidAmount =lpaidAmount - roundOffAmount;
         
            let dueAmountToPay = totalAmount - lpayingAmount;
            if (lpaidAmount || lpaidAmount != 0) {
                dueAmountToPay = (totalAmount - lpaidAmount) - preTds;
                dueAmountToPay = dueAmountToPay - lpayingAmount;
                //dueAmountToPay=dueAmountToPay-tdsAmount
            }
            if (debitAmount) {
                dueAmountToPay = invoiceAmount - tdsAmount
                dueAmountToPay = dueAmountToPay - debitAmount
                dueAmountToPay = dueAmountToPay - lpayingAmount
            }
            dueAmountToPay=dueAmountToPay - roundOffAmount
            dueAmountToPay=+ dueAmountToPay.toFixed(2)
            this.group.controls['dueAmount'].patchValue(dueAmountToPay);


        }
        else {
            // let lpayingAmount: number = this.group.controls['payingAmount'].value;
            let lpayingAmount: number = this.group.get('payingAmount').value

            let lamountAfterTds: number = this.group.controls['amountAfterTds'].value;
            let ldueAmount: number = this.group.controls['dueAmount'].value;
            let lpaidAmountO: number = this.group.controls['paidAmount'].value;
            let ldeductedAmount: number = this.group.controls['debitAmount'].value;
            let ltaxAmount: number = this.group.controls['taxAmount'].value;
            let tdsAmount: number = this.group.controls['tdsAmount'].value;
            let preTds: number = this.group.controls['preTdsAmount'].value;
            let invoiceAmount: number = this.group.controls['invoiceAmount'].value;
            let roundOffAmount: number = this.group.controls['roundOff'].value;
            let taxableAmount: number = this.group.controls['taxableAmount'].value;

            


            // if(preTds){
            //     lamountAfterTds=lamountAfterTds+preTds;  
            // }
        let lpaidAmount: number = this.calculateDueAmount(lpayingAmount, lamountAfterTds, lpaidAmountO, ldeductedAmount,ltaxAmount,preTds);
          if(roundOffAmount<0){
           let total= taxableAmount+ +ltaxAmount;
           roundOffAmount = total-invoiceAmount;
            lpaidAmount =lpaidAmount - roundOffAmount;
        }
      

       
          if(lpaidAmount!=0 && lpaidAmount>0){
            lpaidAmount =lpaidAmount + +roundOffAmount;
           }
     

           lpaidAmount = +lpaidAmount.toFixed(2);
           
            let finalDueAmount:number=invoiceAmount - lpaidAmountO - ldeductedAmount - preTds - tdsAmount;
               
            if (lpayingAmount > finalDueAmount) {
                this.group.controls['payingAmount'].patchValue(finalDueAmount);
                this.group.controls['dueAmount'].patchValue(0);
            }
            else {
                this.group.controls['dueAmount'].patchValue(lpaidAmount);
            }

            this.changeCtr++;
            this.changeCounter.emit(this.changeCtr);
        }
    }
    
    calculateDueAmount(lpayingAmount: number, lamountAfterTds: number, lpaidAmount: number,ldeductedAmount:number,taxAmount:number,preTdsAmount:number): number {
       
        let totalAmount:number =lamountAfterTds + +taxAmount;
        let lafterDebit: number=totalAmount-ldeductedAmount;
        let lafterPaid: number=lafterDebit-lpaidAmount;
        let lafterPaying:number=lafterPaid-lpayingAmount;
        if(lafterPaying<0){
            lafterPaying=0;  
        }
        return lafterPaying;
        }
   
}