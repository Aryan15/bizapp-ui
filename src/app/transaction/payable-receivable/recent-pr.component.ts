import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, debounceTime, map, startWith, switchMap, tap } from 'rxjs/operators';
import { PayableReceivableHeader, RecentPayableReceivable } from '../../data-model/pr-model';
import { TransactionType } from '../../data-model/transaction-type';
import { PayableReceivableService } from '../../services/payable-receivable.service';


const PAGE_SIZE: number = 10;

@Component({
    selector: 'recent-prs',
    templateUrl: 'recent-pr.component.html',
    styleUrls: ['recent-pr.component.scss']
})
export class RecentPayableReceivableComponent implements OnInit, OnChanges{
    public recentPrs : PayableReceivableHeader[] = [];
    @Input()
    recentPrLoadCounter : number;
    
    @Input()
    transactionType : TransactionType;

    @Output()
    prHeader : EventEmitter<PayableReceivableHeader> = new EventEmitter<PayableReceivableHeader>();
    
    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    
    
    displayedColumns = ['payReferenceNumber', 'partyName', 'payReferenceDate', 'grandTotal'];
    prDatabase: PayableReceivableHttpDao | null;
    
    dataSource = new MatTableDataSource<PayableReceivableHeader>();
    
    filterForm = new FormGroup({
      filterText: new FormControl()
    });
    
    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
    
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;
    onChangeEvent: Observable<number>;
    
    
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    
    
    constructor(private prService: PayableReceivableService) { }
    
    ngOnInit() {
      this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
    
      this.prDatabase = new PayableReceivableHttpDao(this.prService);
    
      //If the user changes sort order, reset back to first page
    
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    
      merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
        .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.prDatabase!.getPrs(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.payableReceivableHeaders);
          //console.log('data.totalCount: ', data.totalCount);
          if( this.filterForm.get('filterText').value != null)
          {
           if(data.totalCount === 0)
           {
             this.noDataFound.emit(true);
           }
          }
          return data.payableReceivableHeaders;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          //console.log('catchError error');
          return observableOf([]);
        })
        ).subscribe(data => this.dataSource.data = data);
    
    }
    
    ngOnChanges(){
    
      //console.log('in onchanage: ', this.recentPrLoadCounter);
  
      if(this.recentPrLoadCounter > 0 ){
        this.prDatabase = new PayableReceivableHttpDao(this.prService);
  
        this.prDatabase!.getPrs(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize).pipe(
          //tap(response => console.log("response from getPrs: ", response))
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.payableReceivableHeaders);
          //console.log('data.totalCount: ', data.totalCount);
          if( this.filterForm.get('filterText').value != null)
          {
          if(data.totalCount === 0)
          {
            this.noDataFound.emit(true);
          }
          }
          return data.payableReceivableHeaders;
        })).subscribe(data => this.dataSource.data = data);
      }
    }
  
    onRowClick(row: any) {
      //console.log('row clicked: ', row);
      this.prHeader.emit(row);
    }
    }
    
    export class PayableReceivableHttpDao {
    
    constructor(private prService: PayableReceivableService) { }
    
    getPrs(transactionType: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPayableReceivable> {
    
      pageSize = pageSize ? pageSize : PAGE_SIZE;
      //console.log('filterText: ', filterText);
      //console.log('transactionType: ', transactionType);
    
      return this.prService.getRecentPayableReceivables(transactionType, filterText, sortColumn, sortDirection, page, pageSize);
    }
    }
    
