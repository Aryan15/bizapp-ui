import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Observable, of as observableOf, Subject } from 'rxjs';
import { take } from 'rxjs/internal/operators/take';
import { debounceTime, filter, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { DeliveryChallanHeader, DeliveryChallanHeaderWrapper, DeliveryChallanItem } from '../../data-model/delivery-challan-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { Material, Process } from '../../data-model/material-model';
import { ApplicableButtons, QuantityChangeEvent } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { Party, PartyWithPriceList } from "../../data-model/party-model";
import { PurchaseOrderHeader } from '../../data-model/purchase-order-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { Tax } from '../../data-model/tax-model';
import { TransactionItem } from '../../data-model/transaction-item';
import { TransactionStatus, TransactionType } from '../../data-model/transaction-type';
import { Uom } from '../../data-model/uom-model';
import { MaterialPopupComponent } from '../../master/material-popup/material-popup.component';
import { PartyPopupComponent } from '../../master/party-popup/party-popup.component';
import { ReferenceLinksComponent } from '../../reference-links/reference-links.component';
import { CompanyService } from '../../services/company.service';
import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { MaterialService } from '../../services/material.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { TaxService } from '../../services/tax.service';
// import { DeliveryChallanReportService } from '../../services/delivery-challan-report.service';
import { DeliveryChallanSecondReportService } from '../../services/template2/delivery-challan-report.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TextToSpeechService } from '../../services/text-to-speech.service';
import { TransCommService } from '../../services/trans-comm.service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { PrintDialog } from '../../utils/print-dialog';
import { PrintDialogContainer } from '../../utils/print-dialog-container';
import { UtilityService } from '../../utils/utility-service';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';
import { ConnectionService } from '../../services/connection.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';


@Component({
    selector: 'app-job-work-delivery-challan',
    templateUrl: './job-work-delivery-challan.component.html',
    styleUrls: ['./job-work-delivery-challan.component.scss']
})
export class JobWorkDeliveryChallanComponent implements OnInit, OnDestroy {
    @ViewChild('deliveryChallanNumber', { static: true }) deliveryChallanNumberElement: ElementRef;
    public dcForm: FormGroup;
    public dcHeader: DeliveryChallanHeader;
    transactionId: string;
    loadFromId: boolean = false;
    selectedParty: Party;
    selectedPurchaseOrder: PurchaseOrderHeader;
    selectedPartyGstNumber: string;
    selectedPartyState: number;
    fieldValidatorLabels = new Map<string, string>();
    isTermsAndcondition: boolean = false;
    isPartyChangeable: boolean = true;
    recentDcLoadCounter: number = 0;
    transactionType: TransactionType;
    partyPlaceHolder: string;
    isAutoNumber: boolean = false;
    readonlyIndc:boolean =false;
    dcStatuses: TransactionStatus[] = [];
    //isItemLevelTax: boolean = false;
    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;
    isCreateEnabled: boolean = false;
    toolTipBtn: string;
    printRef: MatDialogRef<PrintDialog>;
    filteredPO: Observable<PurchaseOrderHeader[]>;
    financialMinDate: Date;
    financialMaxDate: Date;
    maxDate: Date;
    itemToBeRemove: string[] = [];
    public materials: Material[] = [];
    material: Material;
    uom: Uom;
    tax: Tax;
    headerTax: Tax;
    financialYear: FinancialYear;
    showTheCreatedBy=false;
    transactionStatus: string = null;

    saveStatus: boolean = false;
    isItemLevelTax: boolean = false;
    isItemLevelDiscount: boolean = false;
    spinStart: boolean = false;
    isInclusiveTax: boolean = false;
    isInclusiveTaxApplicable: boolean = false;
    isItemGrouping: boolean = false;
    activityId:number=0;
    public deliveryChallans: DeliveryChallanHeader[] = [];

    numberRangeConfiguration: NumberRangeConfiguration;
    globalSetting: GlobalSetting;

    isStockCheckRequired: boolean = false;
    isIgst: boolean = false;
    isJWINDC: boolean = false;
    isJWOUTDC: boolean = false;
    isOUTJWOUTDC: boolean = false;
    materialQuantity: number = 1;
    disableParty: boolean = false;
    itemOfDC: string;
    statusCheck:number=0;
    isShowOnlyCustomerMaterialInSales: boolean = false;
    isContainer: boolean = false;
    displayPartyName:string="";
    public company: Company;

    filteredParties: Observable<Party[]>;
    defaultDate: string;
    public parties: Party[] = [];
    public uoms: Uom[] = [];
    public taxes: Tax[] = [];
    public purchaseOrders: PurchaseOrderHeader[] = [];
    initFormItem: string
    mapPoItem: string = AppSettings.TXN_ITEM_OF_PO_IN_JW_DC; //"PO Item";
    //   recentDcItem: string = AppSettings.TXN_ITEM_OF_RECENT_DC;;
    datePipe = new DatePipe('en-US');
    materialControl: FormControl = new FormControl();
    filteredMaterials: Observable<Material[]>;
    deliveryTerms: string;
    paymentTerms: string;
    termsAndConditions: string;
    allowInternalRefNo:boolean =false;
    // supplyTypeControl: FormControl = new FormControl();

    applicableButtons: ApplicableButtons = {
        isApproveButton: false,
        isClearButton: true,
        isCloseButton: true,
        isCancelButton: false,
        isDeleteButton: true,
        isPrintButton: true,
        isSaveButton: true,
        isEditButton: true,
        isDraftButton: false,
    };


    statusBasedPermission: StatusBasedPermission;

    activityRoleBindings: ActivityRoleBinding[];
    tempTransactionNumber: string;
    onDestroy: Subject<boolean> = new Subject();
    isDCList: boolean = false;
    prevDCNumbers: string[] = [];
    selectedDeliveryChallans: DeliveryChallanHeader[] = [];
    processes: Process[] = [];
    process: Process[] = [];
    item_length: number = AppSettings.length_limit;
    constructor(private fb: FormBuilder,
        private dcService: DeliveryChallanService,
        private partyService: PartyService,
        private materialService: MaterialService,
        private uomService: UomService,
        private taxService: TaxService,
        public dialog: MatDialog,
        private _router: Router,
        private _router_in: Router,
        public utilityService: UtilityService,
        private userService: UserService,
        private financialYearService: FinancialYearService,
        private termsAndConditionsService: TermsAndConditionsService,
        private numberRangeConfigService: NumberRangeConfigService,
        private purchaseOrderService: PurchaseOrderService,
        private transactionItemService: TransactionItemService,
        private transactionTypeService: TransactionTypeService,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private transCommService: TransCommService,
        private globalSettingService: GlobalSettingService,
        // private deliveryChallanReportService: DeliveryChallanReportService,
        private deliveryChallanSecondReportService: DeliveryChallanSecondReportService,
        private companyService: CompanyService,
        private textToSpeechService: TextToSpeechService,
        private locationService: LocationService,
        protected connectionService: ConnectionService) {

        this.setTransactionType();
        this.setActivityRoleBinding();
        this.getGlobalSetting();
    }

    getDefaultDate() {

        this.utilityService.getCurrentDateObs().subscribe(response => {
            this.defaultDate = response;
            this.initForm();

            if (this.loadFromId && this.transactionId) {
                this.dcService.getDeliveryChallan(this.transactionId).subscribe(response => {
                    if (response) {
                        this.recentDeliveryChallanGet(response, AppSettings.TXN_ITEM_OF_RECENT_DC);
                    }
                })
            }
        })
    }

    ngOnInit() {
        //console.log("material.....................................", this.material);

        this.company = this.companyService.getCompany();
        
        //this.getParties();
        this.getAllMaterials();
        this.getUoms();
        this.getTaxes();
        this.getFinancialYear();
        this.getDefaultDate();
        // this.initForm();
        this.maxDate = new Date();
        // this.handleChanges();
    }
    setActivityRoleBinding() {
        this.userService.setActivityRoleBinding(this.route).subscribe(response => {
            this.activityRoleBindings = response;

            this.activityRoleBindings.forEach(activityRoleBinding => {
                this.activityId=activityRoleBinding.activityId;

            });


        })
    }

    setTransactionType() {

        this.route.params.pipe(takeUntil(this.onDestroy))
            .subscribe(params => {
                this.transactionTypeService.getTransactionType(+params['trantypeId'])
                    .subscribe(response => {
                        this.transactionType = response;
                        if(this.transactionType.name===AppSettings.INCOMING_JOBWORK_OUT_DC){
                            this.applicableButtons.isCancelButton=true;
                        }
                        //this.recentDcLoadCounter = 0;
                        //console.log("Transaction type in DC ", this.transactionType.name);
                        //   this.getParties();


                        this.getAndPatchTerms();
                        this.getTempTransactionNumber();

                        this.dcStatuses.push({
                            id: 4,
                            name: 'Cancelled',
                            transactionTypeId: this.transactionType.id,
                            transactionTypeName: this.transactionType.name,
                            deleted: 'N'
                        });
                        //this.recentDcLoadCounter++;
                        if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                            this.isDCList = true;
                            this.isJWOUTDC = true;
                        } else if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC) {
                            this.isJWINDC = true;
                            this.applicableButtons.isPrintButton = false;
                        } else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC) {
                            this.isOUTJWOUTDC = true;
                        }

                        if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                            this.applicableButtons.isPrintButton = false;
                        }
                        setTimeout(() => {
                            this.recentDcLoadCounter++;
                        }, 1000);
                    });

                if (params['id']) {
                    //console.log('got id: ', params['id']);
                    this.transactionId = params['id'];
                    this.loadFromId = true;

                }

                this.materialService.getAllProcesses()
                    .subscribe(response => {
                        this.processes = response;
                    })
            });


    }
    displayFnParty(party?: Party): string | undefined {
        return party ?party.partyCode+"-"+party.name + (party.areaName ? '[' + party.areaName + ']' : '') : undefined;
    }
    setStatusBasedPermission(statusId: number) {

        if (statusId) {
            this.spinStart = true;
            console.log('status id: ', statusId);
            this.userService.getStatusBasedPermission(statusId).subscribe(response => {

                if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC
                    || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                    this.checkHigherCount(response);
                } else {
                    this.statusBasedPermission = response;
                    this.spinStart = false;
                }


                //console.log("this.statusBasedPermission: ", this.statusBasedPermission);
            })
        } else {
            //console.log("no status id: ", statusId);
        }
    }

    checkHigherCount(sbp: StatusBasedPermission) {

        if (this.dcHeader) {
            let tranNumbers: string[] = [];
            tranNumbers.push(this.dcHeader.id);
            // console.log("tranNumbers: ", tranNumbers);
            this.transactionTypeService.checkHigherCount(tranNumbers)
                .subscribe(response => {
                    // console.log("response: ", response);
                    if (response > 0) {
                        sbp.permissionToUpdate = 0;
                        this.statusBasedPermission = sbp;
                        // console.log("this.statusBasedPermission: ", this.statusBasedPermission);
                    } else {
                        this.statusBasedPermission = sbp;
                    }
                    this.spinStart = false;
                },
                    error => this.spinStart = false);
        } else {
            this.statusBasedPermission = sbp;
            this.spinStart = false;
        }
    }

    getGlobalSetting() {

        //console.log("in getGlobalSetting()");
        this.globalSettingService.getGlobalSetting().subscribe(response => {
            this.globalSetting = response;
            //console.log("this.globalSetting: ", this.globalSetting);
            //   this.isRoundOffAmount = this.globalSetting.roundOffTaxTransaction === 1 ? true : false;
            //FOR JOBWORK/SUBCONTRACT make it always item level tax
            this.isItemLevelTax = true; //this.globalSetting.itemLevelTax === 1 ? true : false;       
            this.isInclusiveTaxApplicable = this.globalSetting.inclusiveTax === 1 ? true : false;
            this.isInclusiveTax = this.globalSetting.inclusiveTax === 1 ? true : false;
            this.isItemLevelDiscount = this.isItemLevelTax; //this.globalSetting.itemLevelDiscount === 1 ? true : false;
            this.isShowOnlyCustomerMaterialInSales = this.globalSetting.showOnlyCustomerMaterialInSales === 1 ? true : false;
            this.isItemGrouping = this.globalSetting.disableItemGroup === 1 ? true : false;
            this.isStockCheckRequired = this.globalSetting.stockCheckRequired === 1 ? true : false;

        });


    }



    //   getParties() {
    //       //console.log("this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC: ",this.transactionType.name, AppSettings.OUTGOING_JOBWORK_OUT_DC);

    //       if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC || this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC) {
    //           this.partyService.getCustomers().subscribe(
    //               response => {
    //                   this.parties = response;
    //                   this.getFilteredParties();
    //                   //Activate recent list
    //                   this.recentDcLoadCounter++;
    //               }
    //           );
    //           this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER;
    //       } else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC ) {
    //           this.partyService.getSuppliers().subscribe(
    //               response => {
    //                   this.parties = response;
    //                   this.getFilteredParties();
    //                   //Activate recent list
    //                   this.recentDcLoadCounter++;
    //               }
    //           );
    //           this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER;
    //       }
    //   }
    getFilteredParties() {
        //console.log('in getFilteredParties')
        let partyType: string = AppSettings.PARTY_TYPE_ALL;
        this.partyPlaceHolder = AppSettings.PARTY_TYPE_VENDOR;
        //   if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC || this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC ) {
        //     this.partyPlaceHolder = AppSettings.PARTY_TYPE_VENDOR;
        //   }
        this.filteredParties = this.dcForm.controls['partyName'].valueChanges
            .pipe(
                startWith<string | Party>(''),
                debounceTime(300),
                map(value => typeof value === 'string' ? value : value.name),
                filter(value => value != (this.selectedParty ? this.selectedParty.name : '')),
                switchMap(value => this.partyService.getPartiesByNameLike(partyType, value))
            );
    }

    //   filterParty(val: string): Party[] {
    //       //console.log('in filter: ', val);
    //       //console.log('parties ', this.parties);
    //       if (val) {
    //           return this.parties.filter(party =>

    //               party.name.toLowerCase().includes(val.toLowerCase())
    //           );
    //       }
    //       return this.parties;
    //   }
    getAllMaterials() {
        //   this.materialService.getJobworkMaterials().subscribe(
        //       response => {
        //           this.materials = response;
        //           //console.log('got materials: ', this.materials);
        //           // this.supplyTypeControl.patchValue(1);
        //           // //console.log('this.supplyTypeControl.value: ', this.supplyTypeControl.value);

        this.filteredMaterials = this.materialControl.valueChanges
            .pipe(
                startWith<string | Material>(''),
                map(value => typeof value === 'string' ? value : value.name),
                filter(value => value != (this.material ? this.material.name : '')),
                switchMap(value => this.materialService.getJobworkMaterials(value))
            );


        //       }
        //   )
        // this.supplyTypeControl.valueChanges.subscribe(res => {
        //     ////console.log('this.supplyTypeControl.value: ', this.supplyTypeControl.value);
        //     this.materialControl.patchValue('');

        //     ////console.log(this.materials, this.materials.filter(mat => mat.supplyTypeId === +this.supplyTypeControl.value));
        // })
    }

    //   filterMaterial(val: string): Material[] {

    //       return this.materials
    //           // .filter(mat => mat.supplyTypeId === +this.supplyTypeControl.value)
    //           .filter(mat => mat.name.toLowerCase().includes(val.toLowerCase()));


    //   }

    displayFnMaterial(material?: Material): string | undefined {
        return material ? material.name + (material.partNumber ? '[' + material.partNumber + ']' : '') : undefined;
    }
    getUoms() {
        this.uomService.getAllUom().subscribe(
            response => {
                this.uoms = response;
            }
        )
    }

    getTaxes() {
        this.taxService.getAllTaxes().subscribe(
            response => {
                this.taxes = response;
            }
        )
    }

    getFinancialYear() {
        this.financialYearService.getFinancialYear().subscribe(
            response => {
                this.financialYear = response;
                this.financialMinDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                this.financialMaxDate = new Date(this.financialYear.endDate);
                //console.log("this.minDate", this.financialMinDate)
                //console.log("this.maxDate", this.financialMaxDate)

                if (this.financialMaxDate < new Date()) {
                    // this.alertRef = this.dialog.open(AlertDialog, {
                    //     disableClose: false
                    // });
                    let msg = `Please note that the financial year(FY) has changed.  To create transactions pertaining to current FY, you need to make the current FY as "Active" in settings.
                    <br/> Before switching to new FY, please make sure that you have entered all transactions pertaining to previous FY.
                    <br/> <br/>For clarifications, please get in touch with our support team.`;
                    // this.alertRef.componentInstance.alertMessage = msg;
                    this.connectionService.warningInfo.next(msg);


                } else {
                    this.connectionService.warningInfo.next(null);
                }
            }
        )
    }

    private initForm() {
        let data: DeliveryChallanHeader = {
            id: null,
            deliveryChallanNumber: null,
            deliveryChallanId: null,
            deliveryChallanDate: this.defaultDate,//utilityService.getCurrentDate(),
            internalReferenceNumber: null,
            internalReferenceDate: null,
            purchaseOrderNumber: null,
            purchaseOrderDate: null,
            deliveryChallanTypeId: null,
            partyId: null,
            officeAddress: null,
            deliveryAddress: null,
            dispatchDate: null,
            inspectedBy: null,
            inspectionDate: null,
            modeOfDispatch: null,
            vehicleNumber: null,
            deliveryNote: null,
            totalQuantity: null,
            statusId: null,
            statusName: null,
            financialYearId: null,
            companyId: null,
            numberOfPackages: null,
            deliveryTerms: null,
            termsAndConditions: null,
            paymentTerms: null,
            dispatchTime: null,
            taxId: null,
            labourChargesOnly: null,
            inDeliveryChallanNumber: null,
            inDeliveryChallanDate: null,
            returnableDeliveryChallan: null,
            nonReturnableDeliveryChallan: null,
            notForSale: null,
            forSale: null,
            returnForJobWork: null,
            modeOfDispatchStatus: null,
            vehicleNumberStatus: null,
            numberOfPackagesStatus: null,
            personName: null,
            sgstTaxRate: null,
            sgstTaxAmount: null,
            cgstTaxRate: null,
            cgstTaxAmount: null,
            igstTaxRate: null,
            igstTaxAmount: null,
            taxAmount: null,
            price: null,
            amount: null,
            eWayBill: null,
            balanceQuantity: null,
            partyName: null,
            address: null,
            shipToAddress: null,
            gstNumber: null,
            discountPercentage: null,
            discountAmount: null,
            amountAfterDiscount: null,
            deliveryChallanItems: null,
            remarks: null,
            inclusiveTax: null,
            billToAddress: null,
            // ....................
            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile: null,
            companySecondaryMobile: null,
            companyContactPersonNumber: null,
            companyContactPersonName: null,
            companyPrimaryTelephone: null,
            companySecondaryTelephone: null,
            companyWebsite: null,
            companyEmail: null,
            companyFaxNumber: null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber: null,
            companyPanNumber: null,
            companyPanDate: null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            // partyName: null,
            partyContactPersonNumber: null,
            partyPinCode: null,
            partyAreaId: null,
            partyCityId: null,
            partyStateId: null,
            partyCurrencyId:null,
            partyCountryId: null,
            partyPrimaryTelephone: null,
            partySecondaryTelephone: null,
            partyPrimaryMobile: null,
            partySecondaryMobile: null,
            partyEmail: null,
            partyWebsite: null,
            partyContactPersonName: null,
            partyBillToAddress: null,
            partyShipAddress: null,
            partyDueDaysLimit: null,
            partyGstRegistrationTypeId: null,
            partyGstNumber: null,
            partyPanNumber: null,
            isIgst: null,
            partyCode:null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null,
            jwNoteId:null,

        };
        this.dcForm = this.toFormGroup(data, this.initFormItem);
        this.handleChanges();
        this.createDCFromPO();
        this.createOutDCFromIncomingDC();
        this.getFilteredParties();
    }

    private toFormGroup(data: DeliveryChallanHeader, mapItemCategory: string): FormGroup {
        const itemArr = new FormArray([]);
        if (data.deliveryChallanItems) {
            data.deliveryChallanItems.forEach(item => {
               let isOutMaterial: boolean = this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC ? true : false;
                let tItem: TransactionItem = this.transactionItemService.mapItem(item, mapItemCategory, isOutMaterial);
                itemArr.push(this.transactionItemService.makeItem(tItem));
            });
        }
       
        const formGroup = this.fb.group({
            id: [data.id],
            deliveryChallanNumber: [data.deliveryChallanNumber],
            deliveryChallanId: [data.deliveryChallanId],
            deliveryChallanDate: [this.datePipe.transform(data.deliveryChallanDate, AppSettings.DATE_FORMAT), [Validators.required]],
            internalReferenceNumber: [data.internalReferenceNumber],
            internalReferenceDate: [this.datePipe.transform(data.internalReferenceDate, AppSettings.DATE_FORMAT)],
            purchaseOrderNumber: [data.purchaseOrderNumber],
            purchaseOrderDate: [this.datePipe.transform(data.purchaseOrderDate, AppSettings.DATE_FORMAT)],
            deliveryChallanTypeId: [data.deliveryChallanTypeId],
            partyId: [data.partyId],
            // partyId: [{ value: data.partyId, }, [Validators.required]],
            partyName: [data.partyName, [Validators.required]],
            officeAddress: [data.officeAddress],
            deliveryAddress: [data.deliveryAddress],
            dispatchDate: [this.datePipe.transform(data.dispatchDate, AppSettings.DATE_FORMAT)],
            inspectedBy: [data.inspectedBy],
            inspectionDate: [this.datePipe.transform(data.inspectionDate, AppSettings.DATE_FORMAT)],
            modeOfDispatch: [data.modeOfDispatch],
            vehicleNumber: [data.vehicleNumber],
            deliveryNote: [data.deliveryNote],
            totalQuantity: [data.totalQuantity],
            statusId: [data.statusId],
            statusName: [data.statusName],
            financialYearId: [data.financialYearId],
            companyId: [data.companyId],
            numberOfPackages: [data.numberOfPackages],
            deliveryTerms: [data.deliveryTerms],
            termsAndConditions: [data.termsAndConditions],
            paymentTerms: [data.paymentTerms],
            dispatchTime: [data.dispatchTime],
            taxId: [data.taxId],
            labourChargesOnly: [data.labourChargesOnly],
            //inDeliveryChallanNumber: [data.inDeliveryChallanNumber],
            inDeliveryChallanNumber: [data.inDeliveryChallanNumber ? data.inDeliveryChallanNumber.split(',') : null],
            inDeliveryChallanDate: [this.datePipe.transform(data.inDeliveryChallanDate, AppSettings.DATE_FORMAT)],
            returnableDeliveryChallan: [data.returnableDeliveryChallan],
            nonReturnableDeliveryChallan: [data.nonReturnableDeliveryChallan],
            notForSale: [data.notForSale],
            forSale: [data.forSale],
            returnForJobWork: [data.returnForJobWork],
            modeOfDispatchStatus: [data.modeOfDispatchStatus],
            vehicleNumberStatus: [data.vehicleNumberStatus],
            numberOfPackagesStatus: [data.numberOfPackagesStatus],
            personName: [data.personName],
            sgstTaxRate: [data.sgstTaxRate],
            sgstTaxAmount: [data.sgstTaxAmount],
            cgstTaxRate: [data.cgstTaxRate],
            cgstTaxAmount: [data.cgstTaxAmount],
            igstTaxRate: [data.igstTaxRate],
            igstTaxAmount: [data.igstTaxAmount],
            taxAmount: [data.taxAmount],
            price: [data.price],
            amount: [data.amount],
            eWayBill: [data.eWayBill],
            discountPercentage: [data.discountPercentage],
            discountAmount: [data.discountAmount],
            amountAfterDiscount: [data.amountAfterDiscount],
            deliveryChallanItems: itemArr,
            billToAddress: [data.billToAddress],
            gstNumber: [data.gstNumber],
            shipToAddress: [data.shipToAddress],
            inclusiveTax: [data.inclusiveTax],
            // ............
            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile: [data.companyPrimaryMobile],
            companySecondaryMobile: [data.companySecondaryMobile],
            companyContactPersonNumber: [data.companyContactPersonNumber],
            companyContactPersonName: [data.companyContactPersonName],
            companyPrimaryTelephone: [data.companyPrimaryTelephone],
            companySecondaryTelephone: [data.companySecondaryTelephone],
            companyWebsite: [data.companyWebsite],
            companyEmail: [data.companyEmail],
            companyFaxNumber: [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber: [data.companyGstNumber],
            companyPanNumber: [data.companyPanNumber],
            companyPanDate: [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
            // partyName: [data.companyName],
            partyContactPersonNumber: [data.partyContactPersonNumber],
            partyPinCode: [data.partyPinCode],
            partyAreaId: [data.partyAreaId],
            partyCityId: [data.partyCityId],
            partyStateId: [data.partyStateId],
            partyCountryId: [data.partyCountryId],
            partyPrimaryTelephone: [data.partyPrimaryTelephone],
            partySecondaryTelephone: [data.partySecondaryTelephone],
            partyPrimaryMobile: [data.partyPrimaryMobile],
            partySecondaryMobile: [data.partySecondaryMobile],
            partyEmail: [data.partyEmail],
            partyWebsite: [data.partyWebsite],
            partyContactPersonName: [data.partyContactPersonName],
            partyBillToAddress: [data.partyBillToAddress],
            partyShipAddress: [data.partyShipAddress],
            partyDueDaysLimit: [data.partyDueDaysLimit],
            partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
            partyGstNumber: [data.partyGstNumber],
            partyPanNumber: [data.partyPanNumber],
            isIgst: [data.isIgst],
            partyCode:[data.partyCode],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
            updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],
            jwNoteId:[data.jwNoteId]
        });

        formGroup.get('deliveryChallanItems').setValidators(Validators.required);
        this.fieldValidatorLabels.set("partyName", "Party Name");
        this.fieldValidatorLabels.set("deliveryChallanDate", "DeliveryChallan Date");
        this.fieldValidatorLabels.set("deliveryChallanItems", "DeliveryChallan Items");
        this.fieldValidatorLabels.set("materialId", "Material");
        this.fieldValidatorLabels.set("price", "Price");
        this.fieldValidatorLabels.set("quantity", "Quantity");
        this.fieldValidatorLabels.set("amount", "Amount");


        return formGroup;
    }




    handleChanges() {
        // this.dcForm.controls['deliveryChallanItems'].valueChanges.subscribe(items => {
        //     this.handleItems(items);
        // })
        //console.log("in handlechange");

        let items: DeliveryChallanItem[] = this.dcForm.controls['deliveryChallanItems'].value;
        //console.log("items", items)
        this.handleItems(items);
        this.setStatusBasedPermission(this.dcForm.controls['statusId'].value);

        // this.dcForm.controls['purchaseOrderNumber'].valueChanges.subscribe(poNumber => {
        //     if (poNumber !== undefined) {
        //         //console.log(poNumber);

        //         let po: PurchaseOrderHeader = this.purchaseOrders.filter(po => po.purchaseOrderNumber === poNumber)[0];

        //         this.populateItemsWithPo(po);
        //         this.selectedPurchaseOrder = po;
        //         this.dcForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(po.purchaseOrderDate, 'yyyy-MM-dd'));
        //     }
        // })

        // this.dcForm.controls['partyId'].valueChanges.subscribe(partyId => {
        //     this.selectedParty = this.parties.find(p => p.id == partyId);
        //     if (partyId !== undefined) {
        //         this.getPurchaseOrdersForAParty(partyId);
        //         this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
        //         //console.log('isIgst: ', this.isIgst);
        //     }
        // })

        // this.dcForm.controls['purchaseOrderHeaders'].valueChanges.subscribe(po => {
        //     if (po !== undefined){
        //         //console.log(po);
        //         this.populateItemsWithPo(po);
        //     }
        // })        
    }


    getAndPatchTerms() {

        this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id).subscribe(response => {
            this.numberRangeConfiguration = response;

            //  /   this.isTermsAndcondition = this.numberRangeConfiguration.termsAndConditionCheck === 1 ? true : false;
            this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
            // //console.log("isTermsAndcondition" + this.isTermsAndcondition)
            // if (this.isTermsAndcondition === true) {


            this.termsAndConditionsService.getByTransactionTypeAndDefault(this.transactionType.id)
                .subscribe(response => {
                    //console.log("response in dc", response)
                    if (response != null) {
                        this.deliveryTerms = response.deliveryTerms;
                        this.paymentTerms = response.paymentTerms;
                        this.termsAndConditions = response.termsAndCondition;

                        //console.log('paymentTerms: ', this.paymentTerms);
                        this.dcForm.controls['paymentTerms'].patchValue(this.paymentTerms);
                        this.dcForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
                        this.dcForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
                    }
                })

            // }

        })
    }

    handleItems(items: DeliveryChallanItem[]) {

        let totalQuantity: number = 0;
        items.forEach(item => {
            totalQuantity += +item.quantity;
        })
        //console.log('tottalQuantity: ', totalQuantity);

        this.dcForm.controls['totalQuantity'].patchValue(totalQuantity);

    }

    onPurchaseOrderNumberChange(_event: any, po: PurchaseOrderHeader, isInternal: boolean = false) {
        if (isInternal || _event.isUserInput) {
            if (po) {
       // let poNumber: string = this.dcForm.controls['purchaseOrderNumber'].value;
       // if (poNumber) {
            //console.log(poNumber);

           // let po: PurchaseOrderHeader = this.purchaseOrders.filter(po => po.purchaseOrderNumber === poNumber)[0];

            this.populateItemsWithPo(po);
            this.selectedPurchaseOrder = po;
            this.dcForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(po.purchaseOrderDate, AppSettings.DATE_FORMAT));
            this.dcForm.controls['inclusiveTax'].patchValue(po.inclusiveTax ? true : false);
            this.dcForm.controls['internalReferenceDate'].patchValue(this.datePipe.transform(po.internalReferenceDate, AppSettings.DATE_FORMAT));
            this.dcForm.controls['internalReferenceNumber'].patchValue(po.internalReferenceNumber);
            this.isInclusiveTax = po.inclusiveTax ? true : false;
            this.dcForm.controls['purchaseOrderDate'].disable();
            if(po.internalReferenceNumber){
                this.allowInternalRefNo=true;
            }
            this.handleChanges();
        }
        else{
            console.log("manual PO");
            this.dcForm.controls['purchaseOrderDate'].enable();
            this.allowInternalRefNo=false;
        }
    
    }

    }

    onPartyChange(_event: any, party: Party) {
        //console.log('party: ', party, _event);
        if (_event.isUserInput) {
            this.dcForm.controls['partyId'].patchValue(party.id);
            this.selectedParty = party;
            // let partyId: number = this.dcForm.controls['partyId'].value;

            // this.selectedParty = this.parties.find(p => p.id === partyId);

            if (this.selectedParty !== undefined) {
                this.dcForm.controls['billToAddress'].patchValue(this.selectedParty.address);
                this.dcForm.controls['shipToAddress'].patchValue(this.selectedParty.billAddress);
                this.dcForm.controls['gstNumber'].patchValue(this.selectedParty.gstNumber);

                if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC) {
                    this.getPurchaseOrdersForAParty(this.selectedParty.id);
                } else if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {

                    this.getDeliveryChallansForAParty(this.selectedParty.id)
                }


                //console.log('this.parties: ', this.parties);
                //console.log('party id: ', this.selectedParty);
                this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);

                //console.log('isIgst: ', this.isIgst);


            }

            const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }

            if (this.isShowOnlyCustomerMaterialInSales === true) {
                //console.log('this.partyId in partyId: ', this.selectedParty)
                let newMaterials: Material[] = [];
                newMaterials = this.materials.filter(material => material.partyId === this.selectedParty.id);
                //console.log("newMaterials", newMaterials)
                this.materials = newMaterials;
                
                this.materialControl.patchValue('');
            }

        }   // this.dcForm.controls['address'].patchValue(this.selectedParty.address);
    }

    onMaterialSelectChange(_event: any, materialOption: Material) {

        //console.log('materialOption: ', materialOption, _event);
        if (materialOption.isContainer === 1) {
            this.isContainer = true
        }
        else {
            this.isContainer = false;
        }
        if (_event.isUserInput) {
            this.material = materialOption;
            this.onMaterialSelect();
        }

    }
    onMaterialSelect() {

        this.uom = this.uoms.filter(uom => uom.id === this.material.unitOfMeasurementId)[0];
        this.tax = this.taxes.filter(tax => tax.id === this.material.taxId)[0];
        
        //console.log("tax id:", this.tax)
        //  this.headerTax = this.taxes.filter(tax => tax.id === dc.taxId)[0];
        if (this.material.isContainer) {
            let dummyTax: Tax = {
                deleted: "N",
                id: null,
                name: "0%_TAX",
                rate: 0,
                taxTypeId: null
            };

            this.taxes.push(dummyTax);
            this.tax = dummyTax;
            this.material.price = 0;
        }
        //console.log("tax :", this.tax)

        if (!this.isItemLevelTax) {

            this.headerTax = this.tax;
        }

    }

    getPurchaseOrdersForAParty(party: number) {
        //console.log('getPurchaseOrdersForAParty: ', party);
        if (party) {
            if (party !== undefined) {

                let poType: string;

                if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC) {
                    poType = AppSettings.INCOMING_JOBWORK_PO;
                } else {
                    poType = AppSettings.OUTGOING_JOBWORK_PO
                }

                //console.log("poType: ",poType);
                this.purchaseOrderService.getPurchaseOrdersForParty(party, poType, this.transactionType.name).subscribe(
                    response => {
                        this.purchaseOrders = response;
                        this.getFillteredPO()
                        //console.log("PO: ", response);
                    }
                )
            }
        }
    }

    getDeliveryChallansForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let dcType: string;

                if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                    dcType = AppSettings.OUTGOING_JOBWORK_OUT_DC;
                } else {
                    dcType = AppSettings.INCOMING_JOBWORK_IN_DC;
                }

                // if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name == AppSettings.PROFORMA_INVOICE) {
                //     dcType = AppSettings.CUSTOMER_DC;
                // } 
                // else 
                // if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE){
                //     dcType = AppSettings.INCOMING_JOBWORK_OUT_DC
                // }
                // else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE){
                //     dcType = AppSettings.OUTGOING_JOBWORK_OUT_DC
                // }
                // else {
                //     dcType = AppSettings.INCOMING_DC;
                // }


                this.dcService.getDeliveryChallansForPartyAndTransactionType(partyId, dcType, this.transactionType.name).subscribe(
                    response => {
                        this.deliveryChallans = response;
                        //console.log("DC: ", response);
                    }
                )





            }
        }
    }


    populateItemsWithPo(pos: PurchaseOrderHeader) {
        if (pos) {
            const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }

            // const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];

            //console.log("inside populateItemsWithPo : ", pos);
            // this.dcForm.controls['deliveryChallanNumber'].reset(null, {emitEvent: false});
            // this.dcForm.controls['deliveryChallanDate'].reset(null, {emitEvent: false});
            this.headerTax = this.taxes.filter(tax => tax.id === pos.taxId)[0];
            //console.log("this.headerTax: "+this.headerTax.id);
            //console.log("this.headerTax: ", this.headerTax.id);
            pos.purchaseOrderItems.forEach(poItem => {
                if (poItem.igstTaxPercentage != undefined && poItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                if (poItem.dcBalanceQuantity > 0 || pos.isOpenEnded) {
                    //console.log("calling map item")
                    let isOutMaterial: boolean = this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC ? true : false;
                    let tItem: TransactionItem = this.transactionItemService.mapItem(poItem, this.mapPoItem, isOutMaterial);
                    //console.log('tItem: ', tItem);
                    control.push(this.transactionItemService.makeItem(tItem));
                } else {
                    //console.log("NOT calling map item")
                }

            });

        }
    }




    onDCChange() {


        let dcNumber: string[] = this.dcForm.controls['inDeliveryChallanNumber'].value;
        //console.log('dcNumber: ', dcNumber.length ,this.prevDCNumbers.length);



        //Check for new addition
        if (dcNumber.length > this.prevDCNumbers.length) {
            dcNumber.forEach(dcn => {
                if (!(this.prevDCNumbers.length > 0 && this.prevDCNumbers.includes(dcn))) {
                    //console.log('Added: ', dcn);
                    this.deliveryChallans.forEach(dc => {

                        if (dcn === dc.deliveryChallanNumber) {
                            if (!this.selectedDeliveryChallans.find(selDc => selDc.deliveryChallanNumber === dcn)) {
                                //this.selectedDeliveryChallans.push(dc);
                                //console.log('populateItemsWithDc: ', dc);
                                this.populateItemsWithDc(dc);
                                this.selectedDeliveryChallans.push(dc);
                                this.dcForm.controls['inDeliveryChallanDate'].patchValue(this.datePipe.transform(dc.deliveryChallanDate, AppSettings.DATE_FORMAT));
                                this.dcForm.controls['purchaseOrderNumber'].patchValue(dc.purchaseOrderNumber);
                                this.dcForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(dc.purchaseOrderDate, AppSettings.DATE_FORMAT));
                                // this.dcForm.controls['eWayBillNumber'].patchValue(dc.eWayBill);
                                // this.dcForm.controls['modeOfDispatch'].patchValue(dc.modeOfDispatch);
                                // this.dcForm.controls['vehicleNumber'].patchValue(dc.vehicleNumber);
                                // this.dcForm.controls['numOfPackages'].patchValue(dc.numberOfPackages);
                                // this.dcForm.controls['internalReferenceNumber'].patchValue(dc.internalReferenceNumber);
                                // this.dcForm.controls['referenceDate'].patchValue(this.datePipe.transform(dc.internalReferenceDate, AppSettings.DATE_FORMAT));

                            }
                        }


                    })
                }
            })
        }

        //Check for removals
        if (this.prevDCNumbers.length > dcNumber.length) {
            this.prevDCNumbers.forEach(preDcn => {
                if (!(dcNumber.length > 0 && dcNumber.includes(preDcn))) {
                    //console.log('Removed: ', preDcn);

                    let control = <FormArray>this.dcForm.controls['deliveryChallanItems'];
                    //console.log('control.length: ', control.length);
                    let loopIndex: number = 0;

                    let uncheckedDc: DeliveryChallanHeader;

                    uncheckedDc = this.selectedDeliveryChallans.find(sDc => sDc.deliveryChallanNumber === preDcn);

                    //console.log('uncheckedDc: ', uncheckedDc);

                    while (loopIndex < control.length) {
                        //console.log('dc in item: ', control.at(loopIndex));
                        //console.log('loopindex, dc in item: ', loopIndex, control.at(loopIndex).get('sourceDeliveryChallanHeaderId') ? control.at(loopIndex).get('sourceDeliveryChallanHeaderId').value : null);
                        //let itemId: string = (<FormArray>this.invoiceForm.get('invoiceItems')).at(idx).get("id").value;
                        if (control.at(loopIndex).get('sourceDeliveryChallanHeaderId').value === uncheckedDc.id) {
                            this.removeDcItem(loopIndex);
                            loopIndex = 0;
                            ////console.log('need to remove this: ',control.at(loopIndex).get("id"));
                            ////console.log('control.length inside loop: ', control
                        } else {
                            loopIndex++
                        }


                    }

                    let idx: number = this.selectedDeliveryChallans.indexOf(uncheckedDc);

                    this.selectedDeliveryChallans.splice(idx, 1);
                }
            })
        }

        //console.log('at the end, this.selectedDeliveryChallans: ', this.selectedDeliveryChallans);
        this.prevDCNumbers = dcNumber;
        this.handleChanges();

        // let dcNumber: string = this.dcForm.controls['inDeliveryChallanNumber'].value;
        // if (dcNumber) {
        //     //console.log(dcNumber);

        //     let dc: DeliveryChallanHeader = this.deliveryChallans.filter(dc => dc.deliveryChallanNumber === dcNumber)[0];

        //     this.populateItemsWithDc(dc);
        //     //this.selected = po;
        //     this.dcForm.controls['inDeliveryChallanDate'].patchValue(this.datePipe.transform(dc.deliveryChallanDate, AppSettings.DATE_FORMAT));
        // }

    }

    populateItemsWithDc(dc: DeliveryChallanHeader) {
        const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];

        let i: number = control.length;
        //No need to Clear any pre populated items in DC because multiple DC selection is a valid scenario
        // while (i !== 0) {
        //     if (!control.at(0).get('deliveryChallanHeaderId').value)
        //         control.removeAt(0);
        //     i--;
        // }




        this.headerTax = this.taxes.filter(tax => tax.id === dc.taxId)[0];
        dc.deliveryChallanItems.forEach(dcItem => {
            dcItem.sourceDeliveryChallanNumber=dc.deliveryChallanNumber
            dcItem.sourceDeliveryChallanDate=dc.deliveryChallanDate
            
            //see if it is IGST DC
            //console.log("dcItem.igstTaxRate: ", dcItem.igstTaxPercentage);
            //console.log("dcItem.igstTaxRate: ", dcItem.igstTaxPercentage);
            if (this.isItemLevelTax) {
                if (dcItem.igstTaxPercentage != undefined && dcItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
            }

            let isOutMaterial: boolean = this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC ? true : false;
            let tItem: TransactionItem = this.transactionItemService.mapItem(dcItem, AppSettings.TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC, isOutMaterial);
            control.push(this.transactionItemService.makeItem(tItem));
        })


    }


    addDcItem(materialquantity: number) {

        //console.log('this.material.id: ', this.material.id);
        const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];
        //console.log('control: ', control);
        let tempMaterial = this.material;
        let tempTax = this.tax;
        let tempUom = this.uom;
        let tempIsIgst = this.isIgst;

        //console.log("this.isItemGrouping", this.isItemGrouping)
        const controlWithSameMaterial = <FormGroup>control.controls.filter(item => item.get('materialId').value === this.material.id)[0];
        this.checkStockQuantity(materialquantity, tempMaterial).subscribe(val => {
            //console.log("val", val)
            if (val) {
                //console.log("in", val)
                if (controlWithSameMaterial && this.isItemGrouping === false) {
                    //console.log("qty"+controlWithSameMaterial.get('quantity').value);
                    let updatedQuantity: number = +controlWithSameMaterial.get('quantity').value + +materialquantity;
                    //   let trIncomingQuantity: number = controlWithSameMaterial.get('incomingQuantity').value() ;
                    let trOriginalDCBalanceQuantity: number = controlWithSameMaterial.get('dcBalanceQuantity').value;
                    let trQuantity: number = controlWithSameMaterial.get('quantity').value;
                    let deliveredQuantity: number = controlWithSameMaterial.get('id').value ? updatedQuantity - trOriginalDCBalanceQuantity - trQuantity : updatedQuantity - trOriginalDCBalanceQuantity;
                    controlWithSameMaterial.get('quantity').patchValue(updatedQuantity);
                    controlWithSameMaterial.get('incomingQuantity').patchValue(updatedQuantity);
                    controlWithSameMaterial.get('deliveredQuantity').patchValue(deliveredQuantity);
                    controlWithSameMaterial.get('dcBalanceQuantity').patchValue(updatedQuantity);


                    console.log("trOriginalDCBalanceQuantity: ", trOriginalDCBalanceQuantity);
                    console.log("trQuantity: ", trQuantity);
                    console.log("deliveredQuantity: ", deliveredQuantity);

                    //incomingQuantity: [transactionItem.incomingQuantity],
                    //deliveredQuantity: [transactionItem.id ? (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity - transactionItem.quantity) : (transactionItem.incomingQuantity - transactionItem.originalDCBalanceQuantity)],

                }
                else {
                    //console.log("materialquantity"+materialquantity);
                    control.push(this.transactionItemService.initTransactionItem(control.length + 1, tempMaterial, tempUom, materialquantity, tempTax, tempIsIgst, null, null, false, this.process));
                }
                this.handleChanges();
            }

            //    checkStockQuantity= val;
        })
        // control.push(this.transactionItemService.initTransactionItem(control.length + 1, this.material, this.uom, materialquantity, this.tax, this.isIgst, null, null, false));

        this.material = undefined;
        this.materialQuantity = 1;
        this.uom = undefined;
        this.tax = undefined;
        this.materialControl.patchValue('');
        this.dcForm.get('partyName').disable();
        //this.materialControl.patchValue(null,{emitEvent: false});
        return false
    }
    removeDcItem(idx: number) {
        // (<FormArray>this.dcForm.get('deliveryChallanItems')).removeAt(idx);
        // this.handleChanges();
        //console.log("id",(<FormArray>this.dcForm.get('deliveryChallanItems')))

        let itemId: string = (<FormArray>this.dcForm.get('deliveryChallanItems')).at(idx).get("id").value;

        //console.log("Itemid " + itemId)
        if (itemId) {



            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE;

            this.dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.itemToBeRemove.push(itemId);
                    (<FormArray>this.dcForm.get('deliveryChallanItems')).removeAt(idx);
                    this.handleChanges();
                }

            });
            return false;
        } else {
            //
            // else just delete in the array
            //
            (<FormArray>this.dcForm.get('deliveryChallanItems')).removeAt(idx);
            this.handleChanges();
            return false;
        }


    }

    clearForm(formDirective: FormGroupDirective) {

        formDirective.resetForm();
        this.initForm();
        this.enableForm();

        this.handleChanges();
        // this.disableParty = false;
        this.saveStatus = false;
        this.transactionStatus = null;
        this.selectedParty = null;
        this.selectedPurchaseOrder = null;
        this.isPartyChangeable = true;
        this.material = null;
        this.prevDCNumbers = [];
        this.selectedDeliveryChallans = [];
        // this.getAndPatchTerms();
        //this.materialControl.patchValue(null,{emitEvent: false});
        this.uom = null;
        this.tax = null;
        this.getTempTransactionNumber();
        this.isCreateButtonenabled();
        this.getFilteredParties();
        this.dcForm.controls['partyName'].patchValue('');
        this.materialControl.patchValue('');

        this.dcForm.controls['paymentTerms'].patchValue(this.paymentTerms);
        this.dcForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
        this.dcForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
        this.dcForm.markAsPristine();
        this.allowInternalRefNo=false;
    }


    disableForm() {
        this.dcForm.disable({ onlySelf: true, emitEvent: false });
        this.disableParty = true;
        this.materialControl.disable({ onlySelf: true, emitEvent: false });
        this.dcForm.markAsPristine();
    }

    enableForm() {
        this.dcForm.enable({ onlySelf: true, emitEvent: false });
        this.disableParty = false;
        this.materialControl.enable({ onlySelf: true, emitEvent: false });
    }

    cancel() {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = AppSettings.INVOICE_CANCEL_CONFIRMATION_MESSAGE;
        this.dialogRef.afterClosed().subscribe(result => {

            if (result) {
                let model: DeliveryChallanHeader = this.dcForm.value;
                
                model.statusId = this.dcStatuses.find(is => (is.name === AppSettings.INVOICE_CANCEL_STATUS && is.transactionTypeId === this.transactionType.id)).id;
                model.statusName = AppSettings.INVOICE_CANCEL_STATUS;
                this.onSubmit(model);
            }

        });
    }

    validateTnCNewLines(termsAndConditions: string): boolean {
        if (termsAndConditions != null) {
            let tncArray = termsAndConditions.split("\n");
            //console.log("tncArray.length: ", tncArray.length);

            if (tncArray.length >= AppSettings.TNC_POINTS_LIMIT) {
                this.alertService.danger(AppSettings.TNC_POINTS_ERROR_MESSAGE);
                return false;
            }
        }
        return true;
    }
    onSubmitWrap(model: DeliveryChallanHeader) {
        model.partyName=this.displayPartyName;
        if (this.numberRangeConfiguration.id === null) {

            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage = "please add Transaction Type in Transaction Numbering";
            return;
        }

        if (model.deliveryChallanNumber == null && !this.isAutoNumber || model.deliveryChallanNumber == "") {
            this.alertService.danger("Please Enter DC Number");
            return
        }

        if (!this.validateTnCNewLines(model.termsAndConditions)) {
            return;
        }

        let checkNumAvail: boolean = false;
        //check number avail      
        if (this.isAutoNumber) { //in case of auto number do not check
            checkNumAvail = false;
        }
        else {
            if (model.id != null) { //if we are editing invoice which is not auto number
                if (model.deliveryChallanNumber != this.dcHeader.deliveryChallanNumber) { // if invoice number is changed
                    checkNumAvail = true;
                } else {
                    checkNumAvail = false;
                }
            } else { // if not auto number and creating new invoice, always check
                checkNumAvail = true;
            }
        }

        if (checkNumAvail) {
            this.dcService.checkDcNumAvailability(model.deliveryChallanNumber, this.transactionType.id, this.selectedParty.id)
                .subscribe(response => {
                    //console.log("respone.....................>>>>>>",response);
                    if (response.responseStatus === 1) {
                        this.onSubmit(model);
                    }
                    else {
                        this.alertService.danger(model.deliveryChallanNumber + " " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                        this.dcForm.controls['deliveryChallanNumber'].patchValue('');
                        this.deliveryChallanNumberElement.nativeElement.focus();
                    }
                });
        }
        else {
            this.onSubmit(model);
        }

    }

    onSubmit(model: DeliveryChallanHeader) {
        //console.log("model : ",model);
        model.partyName =this.displayPartyName;

        if (model.id === null) {
            model = this.transactionItemService.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
        }

        //   if(this.globalSetting.inclusiveTax===1){
        //     model.inclusiveTax = this.globalSetting.inclusiveTax;
        //   }
        model.inclusiveTax = model.inclusiveTax ? 1 : 0;
        if (model.purchaseOrderNumber === null) {
            model.inclusiveTax = this.globalSetting.inclusiveTax;
        }
        //console.log("model.........................",model);



        const control = <FormArray>this.dcForm.controls['deliveryChallanItems']
        
        let i: number = control.length - 1;
      // console.log("i ==== "+i+"======"+control.length);
        while (i !== -1) {

           
            control.at(i).get('price').clearValidators();
            
            control.at(i).get('price').setErrors(null);
            this.dcForm.updateValueAndValidity();

            i--;
        }
       console.log("this.dcForm.valid = "+this.dcForm.valid);
       
        if (!this.dcForm.valid) {

            const controls = this.dcForm;
            let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
            //console.log("invalidFieldList ", invalidFieldList)

            let invalidFiledLabels: string[] = [];

            invalidFieldList.forEach(field => {
                if (this.fieldValidatorLabels.get(field))
                    invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })

          //  console.log("invalidFiledLabels ", invalidFiledLabels)

            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
            return false;
        }


        this.spinStart = true;

        //console.log('In onSubmit: ', model);
        //console.log('Grand total: ', model.totalQuantity);
        
        model.partyId = this.selectedParty.id; //override id to replace whole cusotmer object
        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id;
        //model.partyId = this.selectedCustomer.id;      
        model.taxId = this.headerTax ? this.headerTax.id : model.deliveryChallanItems[0].taxId; // hack
        model.deliveryChallanTypeId = this.transactionType.id;//'Customer DC';
        model.taxAmount = model.cgstTaxAmount + model.sgstTaxAmount + model.igstTaxAmount;
        //console.log('Before save: ', model.deliveryChallanItems);
        //console.log("itemToBeRemove " + this.itemToBeRemove);

        let dcNumber: string[] = this.dcForm.controls['inDeliveryChallanNumber'].value;

        //console.log('in save: dcNumber: ', dcNumber);
        try {
            model.inDeliveryChallanNumber = dcNumber.join(",");
        } catch (e) {
            model.inDeliveryChallanNumber = this.dcForm.controls['inDeliveryChallanNumber'].value;
        }
        model.deliveryChallanItems.forEach((item, index) => {
            item.slNo = index + 1;
            item.inDeliveryChallanNumber=item.inDeliveryChallanNumber?item.inDeliveryChallanNumber:item.sourceDeliveryChallanNumber
            item.inDeliveryChallanDate=item.inDeliveryChallanDate?item.inDeliveryChallanDate:item.sourceDeliveryChallanDate

          console.log(item.purchaseOrderItemId+"-------")
           
        })


        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        }
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }

        let deliveryChallanHeaderWrapper: DeliveryChallanHeaderWrapper = { itemToBeRemove: this.itemToBeRemove, deliveryChallanHeader: model };
        //console.log("deliveryChallanHeaderWrapper..................",deliveryChallanHeaderWrapper);
        this.dcService.create(deliveryChallanHeaderWrapper)
            .subscribe(response => {
                //console.log("respnonse dcHeader..................",response);

                if (response) {
                    //console.log("respnonse dcHeader..................",response);
                    this.showTheCreatedBy=true;
                    this.dcHeader = response;
                    this.displayPartyName=this.dcHeader.partyName;
                    
                    this.dcHeader.partyName=this.dcHeader.partyCode+"-"+this.dcHeader.partyName;
                    
                    //console.log("saved id: ", response.id);
                    if(!response.updateBy){
                        this.dcService.getDeliveryChallan(response.id)
                        .subscribe(responses => {
                            this.dcForm.controls['updateBy'].patchValue(responses.updateBy);
                            this.dcForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));
              
                               
              
                        });
                    }
                    
                    this.transactionStatus = "DC " + response.deliveryChallanNumber + message
                    this.saveStatus = true;
                    if (response.statusName === "Cancelled") {
                        ////console.log("model.statusName in cncel", model.statusName)
                        this.alertService.success(this.transactionType.name + " Cancelled Successfully");
                    }
                    else{
                    this.alertService.success(this.transactionStatus);
                    }
                    this.spinStart = false;

                    this.isPartyChangeable = false;
                    this.recentDcLoadCounter++;
                    this.isCreateButtonenabled();
                    this.dcForm = this.toFormGroup(this.dcHeader, AppSettings.TXN_ITEM_OF_RECENT_DC);
                    this.isCreateButtonenabled();
                    this.setStatusBasedPermission(response.statusId);
                    this.disableForm();
                } else {
                    this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
                    this.alertService.danger(this.transactionStatus);
                    this.saveStatus = false;
                    this.spinStart = false;
                }

                this.itemToBeRemove = [];

            },
                error => {
                    //console.log("Error ", error);
                    this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
                    this.alertService.danger(this.transactionStatus);
                    this.saveStatus = false;
                    this.spinStart = false;
                    this.itemToBeRemove = [];
                }


            );


    }


    recentDeliveryChallanGet(event, formSource, isOutDC?: boolean) {
        //   console.log('In recentDeliveryChallanGet', event);
        this.showTheCreatedBy=false;
        this.dcHeader = event;

        this.partyService.getCustomer(this.dcHeader.partyId).subscribe(party => {

            this.selectedParty = party;
            this.displayPartyName=this.dcHeader.partyName;
           if(formSource===AppSettings.TXN_ITEM_OF_RECENT_DC) {
          this.dcHeader.partyName=this.dcHeader.partyCode+"-"+this.dcHeader.partyName;
           
        }
        this.dcHeader.deliveryChallanItems.forEach(item => {
         item.sourceDeliveryChallanNumber=this.dcHeader.inDeliveryChallanNumber?this.dcHeader.inDeliveryChallanNumber:this.dcHeader.deliveryChallanNumber
         item.sourceDeliveryChallanDate=this.dcHeader.inDeliveryChallanDate?this.dcHeader.inDeliveryChallanDate:this.dcHeader.deliveryChallanDate
        })
           if(this.dcHeader.deliveryChallanTypeId==13 || this.dcHeader.deliveryChallanTypeId==16 && this.dcHeader.jwNoteId!=null){
                
            this.dcHeader.statusName="New-Generated By Note";
           }
           

            console.log(this.dcHeader.inDeliveryChallanNumber)
           this.dcForm = this.toFormGroup(this.dcHeader, formSource);
        
            this.setStatusBasedPermission(this.dcForm.controls['statusId'].value);
            this.isIgst = this.dcHeader.isIgst > 0 ? true : false;
            if (isOutDC) {
                this.readonlyIndc=true;
                this.deliveryChallans.push(event);
                this.dcForm.controls['inDeliveryChallanNumber'].patchValue([event.deliveryChallanNumber]);
                this.dcForm.controls['id'].patchValue(null);
                this.dcForm.controls['partyName'].patchValue(event.partyName);
                // this.dcForm.controls['partyId'].patchValue(event.partyId);
                this.dcForm.controls['deliveryChallanNumber'].patchValue(null);
                this.dcForm.controls['deliveryChallanDate'].patchValue(this.defaultDate, AppSettings.DATE_FORMAT);
                //this.dcForm.controls['inDeliveryChallanNumber'].patchValue(event.inDeliveryChallanNumber);
                this.dcForm.controls['inDeliveryChallanDate'].patchValue(this.datePipe.transform(event.deliveryChallanDate, AppSettings.DATE_FORMAT));
                this.prevDCNumbers = [];

                //this.onDCChange();
                this.patchItemID();
                this.handleChanges();
            } else {
                this.disableForm();
            }
            this.isCreateButtonenabled();
        });
        //console.log('list of dc: ', this.purchaseOrders);

        let poFound: boolean = false;

        this.purchaseOrders.forEach(poh => {
            if (poh.purchaseOrderNumber === this.dcHeader.purchaseOrderNumber) {
                poFound = true;
            }
        });

        if (!poFound) {
            this.dcForm.controls['purchaseOrderNumber'].patchValue(this.dcHeader.purchaseOrderNumber);
        }
        //console.log('selectedParty: ', this.selectedParty);

        this.disableForm();
        // this. = this.taxes.filter(tax => tax.id === this.invoiceHeader.taxId)[0];
        // this.handleChanges();
        this.isPartyChangeable = false;
        this.headerTax = this.taxes.filter(tax => tax.id === this.dcHeader.taxId)[0];
    }
    // disableEnableForm() {

    //     // //console.log("this.dcForm.disabled: ", this.dcForm.disabled);
    //     // if (this.dcForm.disabled) {
    //     //     //console.log('going to enable');
    //     //     //console.log('dcForm before enable: ', this.dcForm);
    //     //     this.dcForm.enable();
    //     //     //console.log('dcForm after enable: ', this.dcForm);
    //     //     this.disableParty = false;
    //     //     this.dcForm.get('partyId').enable();
    //     // this.disableMaterialSelection = false;
    //     // this.disableQuantity = false;
    //     // this.disableUom = false;
    //     // this.disableTax = false;
    //     // this.disableQuotationDate = false;
    //     // } else {
    //     //     //console.log('going to disable ');
    //     //     //console.log('dcForm before disable: ', this.dcForm);
    //     //     this.dcForm.get('partyId').disable();
    //     //     this.dcForm.disable();
    //     //     //console.log('dcForm after disable: ', this.dcForm);
    //     //     this.disableParty = true;

    //     // this.disableMaterialSelection = true;
    //     // this.disableQuantity = true;
    //     // this.disableUom = true;
    //     // this.disableTax = true;
    //     // this.disableQuotationDate = true;

    //     // }
    //     if (this.dcForm.disabled) {

    //         this.dcForm.enable({ onlySelf: true, emitEvent: false }); // emitEvent is not working. Bug in angular


    //     } else {


    //         this.dcForm.disable({ onlySelf: true, emitEvent: false });
    //         this.disableParty = true;
    //jwNoteId

    //     }

    // }
   
    edit() {

        this.enableForm();
       // this.readonlyIndc=false;
        this.disableParty = true;

    }
    itemChanged(event) {
        //console.log("itemChanged: ", event);
        this.handleChanges();
    }

    whenNoDataFound(noDataFound: boolean) {
        if (noDataFound) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE
        }
    }

    delete() {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.dcForm.controls['id'].value;//this.quotationHeader.id;
                //console.log("deleteing..." + id);
                this.dcService.delete(id)
                    .subscribe(response => {
                        //console.log(response);
                        if (response.responseStatus === 1) {
                            this.transactionStatus = response.responseString;
                            this.saveStatus = true;
                            this.recentDcLoadCounter++;
                            this.spinStart = false;
                            this.clearFormNoStatusChange();
                            this.alertService.success(this.transactionStatus);
                        } else {
                            this.transactionStatus = response.responseString ? response.responseString : AppSettings.DELETE_FAILED_MESSAGE;

                            this.saveStatus = false;
                            this.spinStart = false;
                            this.alertService.danger(this.transactionStatus);
                        }
                    },
                        error => {
                            //console.log("Error ", error);
                            this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE

                            this.saveStatus = false;
                            this.spinStart = false;
                        });
            }
            //this.handleChanges()
            this.dialogRef = null;
        });

    }

    clearFormNoStatusChange() {
        this.initForm();
        this.handleChanges();
        this.selectedParty = null;
        this.isPartyChangeable = true;
        this.getFilteredParties();
        this.dcForm.markAsPristine();
    }

    print() {

        //console.log("print method printRef...........", this.printRef );
        //console.log("print method...........", this.dcForm.value);
        //console.log("print method transactionType...........", this.transactionType);
        //console.log("print method globalSetting...........", this.globalSetting);


        let l_printCopy: number = 1;
        let partyName: string = this.dcForm.get('partyName').value
        this.dcForm.controls['partyName'].patchValue(this.displayPartyName);
        this.printRef = this.dialog.open(PrintDialogContainer, {
            // width: '250px',
            data: {
                transactionType: this.transactionType,
                printDataModel: this.dcForm.value,
                printCopy: l_printCopy,
                globalSetting: this.globalSetting,
                billAddress: this.selectedParty.billAddress,
                numberRangeConfiguration: this.numberRangeConfiguration
            }
        });
        this.dcForm.controls['partyName'].patchValue(partyName);
    }

    closeForm() {
        // this._router.navigateByUrl('/dashboard')
        this._router.navigate(['/']);
    }
    addNewParty() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        let partyType: number = 2;

        dialogConfig.data = {
            partyTypeId: partyType

        };


        // dialogConfig.data = {
        //     partyTypeId: 1
        // };

        //  this.dialog.open(PartyPopupComponent, dialogConfig);

        const dialogRef = this.dialog.open(PartyPopupComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {

                //console.log("Dialog output:", data)
                if (data) {
                    let partyWithPriceList: PartyWithPriceList = {
                        partyDTO: data,
                        materialPriceListDTOList: [],
                        materialPriceListDeletedIds: [],
                        partyBankMapDTOList: [],
                        partyBankMapDeletedIds: [],
                    };

                    //partyWithPriceList.partyDTO = data;
                    this.partyService.savePartyWithPriceList(partyWithPriceList).subscribe(response => {
                        //console.log("Dialog response:", response)
                        //
                        let status = response.partyDTO.name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                        this.alertService.success(status);
                        this.parties.push(response.partyDTO);
                        this.dcForm.controls['partyName'].patchValue(response.partyDTO);

                        this.selectedParty = this.parties.find(p => p.id === response.partyDTO.id);
                        //console.log("this.selectedParty", this.selectedParty)
                        this.dcForm.controls['address'].setValue(this.selectedParty.address);
                        this.dcForm.controls['stateName'].setValue(this.selectedParty.stateName);
                        this.dcForm.controls['billToAddress'].patchValue(response.partyDTO.address);
                        this.dcForm.controls['shipToAddress'].patchValue(response.partyDTO.billAddress);
                        //   this.isIgst = this.getIsIgst(this.selectedParty);
                        this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                        //this.getFiltredMaterial();

                    });
                }
            });

    }

    addNewMaterial() {
        let selectedMaterial: number;
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            supplyTypeId: 1,
            materialTypeId: 4
        };

        //  this.dialog.open(MaterialPopupComponent, dialogConfig);

        const dialogRef = this.dialog.open(MaterialPopupComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {
                if (data) {
                    data.companyId = 1; //TO DO
                    data.supplyTypeId = 1;
                    //console.log("Dialog output:", data)
                    this.materialService.save(data).subscribe(response => {
                        //console.log("Dialog response:", response)
                        //
                        let status = response.name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                        this.alertService.success(status);
                        this.materials.push(response);
                        this.material = response;
                        this.materialControl.patchValue(this.material, { emitEvent: false });
                        this.onMaterialSelect();
                        // this.material = this.materials.find(p => p.id === response.id);
                        // this.uom = this.materials.find(u => u.id === response.unitOfMeasurementId)
                        // this.uom = this.uoms.filter(uom => uom.id === this.material.unitOfMeasurementId)[0];
                        // //console.log("this.uom",this.uom)

                        // this.dcForm.controls['material'].patchValue(response.id);
                        // this.dcForm.controls['uom'].patchValue(response.unitOfMeasurementId);

                    });
                }
            });
    }

    public findInvalidControls() {
        const invalid = [];
        const controls = this.dcForm.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                invalid.push(name);
            }
        }
        //console.log(invalid);
    }

    quantityChange(event: QuantityChangeEvent) {
        //console.log("in chnage", event);
        const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];

        const rowControl = control.at(event.index);
        //console.log("in chnage rowControl", rowControl);
        let material: Material = this.materials.find(mat => mat.id === event.materialId);
        this.checkStockQuantity(event.quantity, material).subscribe(val => {
            //console.log("val", val)
            let originlQty = event.quantity
            //console.log("originlQty", originlQty)
            if (!val) {
                rowControl.get('quantity').patchValue(0);
            }
        });


    }

    checkStockQuantity(materialquantity: number, material: Material): Observable<boolean> {

        if (this.transactionType.name == AppSettings.CUSTOMER_DC && this.isStockCheckRequired) {
            if (material.stock < materialquantity) {
                this.dialogRef = this.dialog.open(ConfirmationDialog, {
                    disableClose: false
                });
                //console.log("outside result", materialquantity, this.material)
                this.dialogRef.componentInstance.confirmMessage = AppSettings.STOCK_CONFIRMATION_MESSAGE
                return this.dialogRef.afterClosed();

            }
            //console.log(" 3")
            return observableOf(true);
        }
        //console.log(" 4")
        return observableOf(true);
    }


    //   checkDcNumAvailability() {
    //       let deliveryChallanNumber: string = this.dcForm.get('deliveryChallanNumber').value
    //       //console.log("deliveryChallanNumber: ", deliveryChallanNumber);
    //       if (deliveryChallanNumber && deliveryChallanNumber.length > 0) {
    //           this.dcService.checkDcNumAvailability(deliveryChallanNumber, this.transactionType.id, this.selectedParty.id).subscribe(response => {

    //               //console.log("is deliveryChallanNumber avialable? ", response);
    //               if (response.responseStatus === 1) {
    //                   //console.log("Yes");
    //               } else {
    //                   //console.log("No");
    //                   this.alertService.danger("Delivery Challan Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //                   this.dcForm.controls['deliveryChallanNumber'].patchValue('');
    //                   this.deliveryChallanNumberElement.nativeElement.focus();

    //               }

    //           });
    //       }

    //   }


    showReference() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            tranTypeId: this.transactionType.id,
            tranId: this.dcForm.get('id').value

        };

        dialogConfig.height = '600px';
        dialogConfig.width = '1000px';


        const dialogRef = this.dialog.open(ReferenceLinksComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {
                //console.log('dialog closed: ', data)
                if (data) {
                    //console.log('navigating')
                    this._router.navigate([data.url, { id: data.id }]);
                    //this._router.navigate(['/']);                    
                }
            });

    }

    ngOnDestroy() {
        //console.log('ngOnDestory');
        this.onDestroy.next(true);
        this.onDestroy.complete();
    }
    createDCFromPO() {
        if(this.transCommService.poData){
        this.transCommService.poData
            .pipe(
                take(1)
                , takeUntil(this.onDestroy)
            )
            .subscribe(response => {
                //console.log('response frm po ', response);
                if (response) {
                    this.isPartyChangeable=false;
                    //console.log('response in', response.purchaseOrderNumber);
                    this.dcForm.controls['purchaseOrderNumber'].patchValue(response.purchaseOrderNumber);
                    this.dcForm.controls['billToAddress'].patchValue(response.address);
                    this.dcForm.controls['shipToAddress'].patchValue(response.shipToAddress);
                    this.dcForm.controls['gstNumber'].patchValue(response.gstNumber);
                    this.dcForm.controls['internalReferenceDate'].patchValue(this.datePipe.transform(response.internalReferenceDate, AppSettings.DATE_FORMAT));
                    this.dcForm.controls['internalReferenceNumber'].patchValue(response.internalReferenceNumber);
                    this.dcForm.controls['inclusiveTax'].patchValue(response.inclusiveTax);

                    // this.dcForm.controls['purchaseOrderNumber'].patchValue(response.purchaseOrderNumber);
                    this.dcForm.controls['partyName'].patchValue(response.partyName);
                    this.partyService.getCustomer(response.partyId).subscribe(party => {
                        this.dcForm.controls['partyId'].patchValue(party.id);
                        this.selectedParty = party;
                    });
                    this.purchaseOrders.push(response);
                    this.onPurchaseOrderNumberChange(null, response, true);
                    // this.transCommService.initDcData();
                    // this.transCommService.updateDcData(this.dcHeader);
                    this.transCommService.updatePoData(null)
                    this.transCommService.completePoData();
                }
            });
        }
    }

    createOutgoingDC() {
        //console.log("in createOutgoingDC")
        let sourceTransactionType: TransactionType;
        if (this.dcHeader && this.dcHeader.id) {
            this.dcHeader.deliveryChallanItems.forEach(item =>{
                console.log("this.dcHeader............................>>>>>>",item.purchaseOrderItemId);

            })
            //console.log("this.dcHeader............................>>>>>>",this.dcHeader.deliveryChallanItems);

            // this.transCommService.dcHeader.next(this.dcHeader);
            this.transCommService.initDcData();
            this.transCommService.updateDcData(this.dcHeader);
            this.transactionTypeService.getAllTransactionTypes().subscribe(response => {
                //  this.transactionTypes=response;
                //console.log("this.transactionType.name"+this.transactionType.name);
                response.forEach(tt => {
                    if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC) {
                        //console.log("tt"+tt)

                        if (tt.name == AppSettings.INCOMING_JOBWORK_OUT_DC) {
                            //console.log("tt.name"+tt.id)
                            sourceTransactionType = tt;

                            //this._router_in.navigate(['transaction/grn-form',tt.id]);
                            this._router_in.navigate(['transaction/app-job-work-delivery-challan', tt.id]);

                        }
                    }
                    else if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC) {
                        if (tt.name == AppSettings.INCOMING_JOBWORK_INVOICE) {
                            //console.log("tt.name"+tt.id)
                            sourceTransactionType = tt;

                            //this._router_in.navigate(['transaction/grn-form',tt.id]);
                            this._router_in.navigate(['transaction/app-new-invoice', tt.id]);

                        }
                    }
                    else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC) {
                        if (tt.name == AppSettings.OUTGOING_JOBWORK_IN_DC) {
                            //console.log("tt.name"+tt.id)
                            sourceTransactionType = tt;

                            //this._router_in.navigate(['transaction/grn-form',tt.id]);
                            this._router_in.navigate(['transaction/app-job-work-delivery-challan', tt.id]);

                        }
                    }
                    else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                        if (tt.name == AppSettings.OUTGOING_JOBWORK_INVOICE) {
                            //console.log("tt.name"+tt.id)
                            sourceTransactionType = tt;

                            //this._router_in.navigate(['transaction/grn-form',tt.id]);
                            this._router_in.navigate(['transaction/app-new-invoice', tt.id]);

                        }
                    }
                });
            });
        }
    }

    createOutDCFromIncomingDC() {
        //console.log("in createdc");
        if( this.transCommService.dcData){
        this.transCommService.dcData
            .pipe(
                take(1)
                , takeUntil(this.onDestroy)
            )
            .subscribe(response => {

                if (response) {
                    this.isPartyChangeable = true;
                    // if(response.invoiceTypeId)
                    //console.log('response ', this.transactionType.name , "this.transactionId",response);
                    //    this.recentDcItem = AppSettings.TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC;
                    if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC) {
                        this.recentDeliveryChallanGet(response, AppSettings.TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC, true);
                    }
                    else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC) {
                        this.recentDeliveryChallanGet(response, AppSettings.TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC, true);
                    }
                    // this.transCommService.dcHeader.next(null);
                    this.transCommService.updateDcData(null);
                    this.transCommService.completeDcData();
                    //    this.recentDcItem = AppSettings.TXN_ITEM_OF_RECENT_DC;
                    this.isCreateEnabled = false;

                }
            });
        }
    }
    isCreateButtonenabled() {
        //console.log(this.dcForm.controls['statusId'].value + "statys"+this.dcForm.controls['id'].value  ) // && this.tranForm.controls['statusId'].value == 68

        switch (this.transactionType.name) {
            case AppSettings.INCOMING_JOBWORK_IN_DC:
                this.toolTipBtn = "Create Outgoing Delivery Challan";
                break;
            case AppSettings.INCOMING_JOBWORK_OUT_DC:
                this.toolTipBtn = "Create Outgoing Jobwork Invoice";
                break;
            case AppSettings.OUTGOING_JOBWORK_OUT_DC:
                this.toolTipBtn = "Create Subcontract IN DC";
                break;
            case AppSettings.OUTGOING_JOBWORK_IN_DC:
                this.toolTipBtn = "Create Subcontract Invoice";
                break;
        }

        //this.toolTipBtn= this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC? "Create Outgoing Delivery Challan":"Create Outgoing Jobwork Invoice";
        if (this.dcForm.controls['id'].value != null && this.dcForm.controls['statusName'].value == AppSettings.STATUS_NEW) {
            this.isCreateEnabled = true;
        }
        else {
            this.isCreateEnabled = false;
        }
    }

    patchItemID() {
        let control = <FormArray>this.dcForm.controls['deliveryChallanItems'];
        //console.log('control.length: '+control.length);
        let loopIndex: number = 0;

        while (loopIndex < control.length) {
            //console.log('dc in item: ', control.at(loopIndex));
            //console.log('loopindex, dc in item: '+ control.at(loopIndex).get('id').value);
            control.at(loopIndex).get('id').patchValue(null);
            loopIndex++

        }

    }

    protected getTempTransactionNumber() {
        //console.log("transactionType.name..........",this.transactionType);

        this.transactionTypeService.getTransactionNumber(this.transactionType.name)
            .pipe(
                takeUntil(this.onDestroy)
            )
            .subscribe(response => {
                //console.log("tempTransactionNumber.....................", response.responseString);

                this.tempTransactionNumber = response.responseString;
            })
    }

    recentDeliveryChallanGetMain(event) {
       
        this.recentDeliveryChallanGet(event, AppSettings.TXN_ITEM_OF_RECENT_DC);
    }

    isInJWINDC(): boolean {
        return this.transactionType && this.transactionType.name === AppSettings.INCOMING_JOBWORK_IN_DC ? true : false;
    }

    isSCOUTDC(): boolean {
        return this.transactionType && this.transactionType.name === AppSettings.OUTGOING_JOBWORK_OUT_DC ? true : false;
    }

    isInJWOUTDC(): boolean {
        return this.transactionType && this.transactionType.name === AppSettings.INCOMING_JOBWORK_OUT_DC ? true : false;
    }

    isSCINDC(): boolean {
        return this.transactionType && this.transactionType.name === AppSettings.OUTGOING_JOBWORK_IN_DC ? true : false;
    }



    helpVideos() {
        
        let status: string = "";
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.id =this.activityId.toString();
     
        
        const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {

            });



        
    }

    getFillteredPO() {
       
        this.filteredPO = this.dcForm.get('purchaseOrderNumber').valueChanges
            .pipe(
                startWith(''),
                map(value => this._poFilter(value))
            );
    }

    _poFilter(value: string): PurchaseOrderHeader[] {
        // const filterValue = value.toLowerCase();
        const filterValue = value ? value.toLowerCase() : '';
        if (!value) {
            const control = <FormArray>this.dcForm.controls['deliveryChallanItems'];
            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }
            this.dcForm.get("purchaseOrderDate").setValue(null);
            this.dcForm.controls['purchaseOrderDate'].enable();
        }
        return this.purchaseOrders.filter(po => po.purchaseOrderNumber.toLowerCase().includes(filterValue))
    }

   
}
