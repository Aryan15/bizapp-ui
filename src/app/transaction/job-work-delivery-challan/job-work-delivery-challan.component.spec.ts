import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobWorkDeliveryChallanComponent } from './job-work-delivery-challan.component';

describe('JobWorkDeliveryChallanComponent', () => {
  let component: JobWorkDeliveryChallanComponent;
  let fixture: ComponentFixture<JobWorkDeliveryChallanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobWorkDeliveryChallanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobWorkDeliveryChallanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
