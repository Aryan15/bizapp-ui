import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { take, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Process } from '../../data-model/material-model';
import { QuantityChangeEvent } from '../../data-model/misc-model';
import { Party } from '../../data-model/party-model';
import { PurchaseOrderHeader, PurchaseOrderWrapper } from '../../data-model/purchase-order-model';
import { QuotationHeader } from '../../data-model/quotation-model';
import { TransactionItem } from '../../data-model/transaction-item';
import { TransactionType } from '../../data-model/transaction-type';
import { CompanyService } from '../../services/company.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { InvoiceService } from '../../services/invoice.service';
import { MaterialService } from '../../services/material.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PrintWrapperService } from '../../services/print-warpper.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { QuotationService } from '../../services/quotation.service';
import { TaxService } from '../../services/tax.service';
import { PurchaseOrderSecondReportService } from '../../services/template2/purchase-order-report.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransCommService } from '../../services/trans-comm.service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { TransactionSummaryService } from '../../services/transaction-summary.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { UtilityService } from '../../utils/utility-service';
import { TransactionParentComponent } from '../transaction-parent/transaction-parent.component';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';
import { MatRadioButton } from '@angular/material/radio';
import { ConnectionService } from '../../services/connection.service';
import { AppComponent } from '../../app.component';

@Component({
    selector: 'app-new-po-component',
    templateUrl: './new-po-component.component.html',
    styleUrls: ['./new-po-component.component.scss']
})
export class NewPoComponentComponent extends TransactionParentComponent implements OnInit {
    @ViewChild('purchaseOrderNumber', {static: true}) purchaseOrderNumberElement: ElementRef;

    //poForm: FormGroup;s
    public poHeader: PurchaseOrderHeader;

    // subheading : String = "this page is used to record Information about ";

    itemOfQuotation: string = AppSettings.TXN_ITEM_OF_QUOTATION_IN_PO; //"Quotation Item";
    item_length : number = AppSettings.length_limit;
    itemOfNewPO: string;
    itemOfRecentPO: string = AppSettings.TXN_ITEM_OF_RECENT_PO; //"Recent PO Item";
    displayableGrandTotal: number;
    selectedQuotation: QuotationHeader;
    public quotationHeaders: QuotationHeader[] = [];
    jobworkPO: boolean = false;
    isChecked: boolean =false;
    processes: Process[] = [];
    isCreateDCenabled : boolean = false;
    fieldValidatorLabels = new Map<string, string>();
    isPoClosed:boolean = false;
    // public company: Company;
    // applicableButtons: ApplicableButtons = {
    //     isApproveButton: false,
    //     isClearButton: true,
    //     isCloseButton: true,
    //     isCancelButton: false,
    //     isDeleteButton: true,
        // isPrintButton: false,
    //     isSaveButton: true,
    //     isEditButton: true,
    //     isDraftButton: false,
    // };
    recentPoLoadCounter: number = 0;
    displayPartyName:string="";
    showRecent: boolean = false;
    statusCheck:number=0;
    showTheCreatedBy=false;
    isheaderTaxDisable:boolean=false;
    taxNumberText="GST Number";
  
    constructor(
        private fb: FormBuilder,
        public route_po: ActivatedRoute,
        private _router_in: Router,
        private transactionTypeService_po: TransactionTypeService,
        private numberRangeConfigService_po: NumberRangeConfigService,
        private termsAndConditionsService_po: TermsAndConditionsService,
        private utilityService_po: UtilityService,
        private materialService_po: MaterialService,
        private partyService_po: PartyService,
        private userService_po: UserService,
        private globalSettingService_po: GlobalSettingService,
        private uomService_po: UomService,
        private taxService_po: TaxService,
        private alertService_po: AlertService,
        private transactionTypeService_in: TransactionTypeService,
        private quotationService_po: QuotationService,
        private financialYearService_po: FinancialYearService,
        private transactionItemService_po: TransactionItemService,
        private transactionSummaryService_po: TransactionSummaryService,
        private poService: PurchaseOrderService,
        private purchaseOrderSecondReportService: PurchaseOrderSecondReportService,
        private companyService_in: CompanyService,
        private dialog_in: MatDialog,
        private printWrapperService_in: PrintWrapperService,
        private transCommService: TransCommService,
        private invoiceService_in : InvoiceService,
        private locationService_po: LocationService,
        protected connectionService: ConnectionService

        
       
        
        
    ) {
        super(
            route_po,
            _router_in,
            transactionTypeService_po,
            numberRangeConfigService_po,
            termsAndConditionsService_po,
            utilityService_po,
            materialService_po,
            partyService_po,
            userService_po,
            globalSettingService_po,
            uomService_po,
            taxService_po,
            financialYearService_po,
            quotationService_po,
            alertService_po,
            transactionItemService_po,
            transactionSummaryService_po,
            dialog_in,
            printWrapperService_in,
            companyService_in,
            locationService_po,
            connectionService
        );
    }

    ngOnInit() {
        this.initParent();
        //setTimeout(() => {//console.log('in child: ', this.transactionType)}, 1000);
        //console.log("before init complete...")
        this.initComplete.subscribe(response => {
            //console.log("in init complete...", response);
            if (response) {

                this.initForm();          

                this.itemsControl = this.tranForm.controls['purchaseOrderItems'];
                this.partyFormControl = this.tranForm.controls['partyName'];
                this.getFilteredParties();
                this.handleChangesParent(this.itemOfNewPO, this.poHeader ? this.poHeader.taxId : null);

                this.tranForm.controls['paymentTerms'].patchValue(this.paymentTerms);
                this.tranForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
                this.tranForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
              
                // if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_PO){
                //     this.applicableButtons.isPrintButton = false;
                // }
                if(this.loadFromId && this.transactionId){
                    this.poService.getPurchaseOrderById(this.transactionId).subscribe(response => {
                        if(response){
                            this.recentPurchaseOrderGet(response);
                        }
                    })
                }
                else{
                  
                    this.transCommService.quotationData
                    .pipe(
                        take(1)
                        ,takeUntil(this.onDestroy)
                    )
                    .subscribe(response => {
                      
                        //console.log('response............................>>>>>> ', response);
                        //console.log('this.tranForm ', this.tranForm);
                      
                        if(response){
                            this.isPartyChangeable = false;
                            this.tranForm.controls['quotationNumber'].patchValue(response.quotationNumber);
                           this.tranForm.controls['partyName'].patchValue(response.partyName);
                           this.tranForm.controls['gstNumber'].patchValue(response.gstNumber);
                           this.tranForm.controls['billToAddress'].patchValue(response.address);
                          
                           console.log("id"+response.partyCountryId +"grandTotal---"+response.grandTotal+"totalTaxableAmount--"+response.totalTaxableAmount+"--"+response.taxAmount);
                           if(response.partyCountryId!=1)
                           {
                               console.log(response.partyCurrencyId+"id"+response.currencyName)
                            this.tranForm.controls['partyCurrencyId'].patchValue(response.partyCurrencyId);
                            this.tranForm.controls['currencyName'].patchValue(response.currencyName);
                            //helps to get id value in the parent transction component
                            this.tranForm.controls['partyCountryId'].patchValue(response.partyCountryId);
                            this.taxNumberText="CIN Number "

                           }
                           else{
                            this.tranForm.controls['partyCurrencyId'].patchValue(0);
                            this.taxNumberText="GST Number"

                           }

                           this.isInclusiveTax = response.inclusiveTax ? true : false;
                           this.inclusiveTaxDisable = true;
                          

                         
                  if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
                      {
                         this.isheaderTaxDisable=true;//headerlevel
                          
                      }
                 if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==0)
                      {  
                        this.isheaderTaxDisable=false;
                      }
     
                 if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
                     {
                      
                         this.isItemLevelTax=false;
                       this.isheaderTaxDisable=true
           
                     }
                if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==1)
                  {  
                   this.isItemLevelTax=true;
                  }

                           this.partyService.getCustomer(response.partyId).subscribe(party => {
                                this.tranForm.controls['partyId'].patchValue(party.id);
                                            this.selectedParty = party;
                                            this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                                            ////console.log(" this.isIgst " + this.isIgst);
                                            this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                                           /*  if(this.selectedParty.partyCurrencyId!=null)
                                            {

                                            
                                            this.partyService.getCurrency(this.selectedParty.partyCurrencyId).subscribe(data => {
                                                this.tranForm.controls['currencyName'].patchValue(data.currencyName);

                                            
                                            });
                                        }
 */
                            });
                           this.quotationHeaders.push(response);
                           this.onQuotationNumberChange()    
                           this.transCommService.updateQuotationData(null);
                           this.transCommService.completeQuotationData();                   
                         
                        }
                    });
              
            }

                if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_PO || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_PO){
                    this.jobworkPO = true;
                     
                }

                this.materialService_po.getAllProcesses()
                .subscribe(response => {
                    this.processes = response;
                })

            }
        })


    }


    private initForm() {
        let data: PurchaseOrderHeader = {
            id: null,
            purchaseOrderNumber: null,
            purchaseOrderId: null,
            purchaseOrderDate: this.defaultDate,//this.utilityService.getCurrentDate(),
            quotationNumber: null,
            quotationDate: null,
            internalReferenceNumber: null,
            internalReferenceDate: null,
            partyId: null,
            subTotalAmount: null,
            totalDiscount: null,
            discountPercent: null,
            totalTaxableAmount: 0,
            taxId: null,
            roundOffAmount: null,
            grandTotal: null,
            statusId: null,
            financialYearId: null,
            companyId: null,
            purchaseOrderTypeId: null,
            paymentTerms: null,
            deliveryTerms: null,
            termsAndConditions: null,
            taxAmount: null,
            advanceAmount: null,
            sgstTaxRate: null,
            sgstTaxAmount: null,
            cgstTaxRate: null,
            cgstTaxAmount: null,
            igstTaxRate: null,
            igstTaxAmount: null,
            isReverseCharge: null,
            transportationCharges: null,
            purchaseOrderItems: null,
            statusName: null,
            partyName: null,
            shipToAddress: null,
            address: '',
            gstNumber: null,
            purchaseOrderEndDate: null,
            isOpenEnded: null,
            remarks : null,
            inclusiveTax: null,
            // address: String
            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile : null,
            companySecondaryMobile : null,
            companyContactPersonNumber : null,
            companyContactPersonName : null,
            companyPrimaryTelephone : null,
            companySecondaryTelephone : null,
            companyWebsite : null,
            companyEmail : null,
            companyFaxNumber : null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber : null,
            companyPanNumber : null,
            companyPanDate : null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            // partyName: null,
            partyContactPersonNumber: null,
            partyPinCode: null,
            partyAreaId: null,
            partyCityId: null,
            partyStateId: null,
            partyCountryId: null,
            partyPrimaryTelephone: null,
            partySecondaryTelephone: null,
            partyPrimaryMobile: null,
            partySecondaryMobile: null,
            partyEmail: null,
            partyWebsite: null,
            partyContactPersonName: null,
            partyBillToAddress: null,
            partyShipAddress: null,
            partyDueDaysLimit: null,
            partyGstRegistrationTypeId: null,
            partyGstNumber: null,
            partyPanNumber: null,
            isIgst: null,
            partyCode:null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null,
            closePo:null,
            currencyName:null,
            partyCurrencyId:null,
        };

        this.tranForm = this.toFormGroup(data, this.itemOfNewPO);

    }


    private toFormGroup(data: PurchaseOrderHeader, mapItemCategory: string): FormGroup {
        const itemArr = new FormArray([]);
        if (data.purchaseOrderItems) {

            data.purchaseOrderItems.forEach(item => {


                let tItem: TransactionItem = this.transactionItemService_po.mapItem(item, mapItemCategory);

                itemArr.push(this.transactionItemService_po.makeItem(tItem));

            });
        }
        const formGroup = this.fb.group({
            id: [data.id],
            purchaseOrderNumber: [data.purchaseOrderNumber],
            purchaseOrderId: [data.purchaseOrderId],
            purchaseOrderDate: [this.datePipe.transform(data.purchaseOrderDate, AppSettings.DATE_FORMAT), [Validators.required]],
            quotationNumber: [data.quotationNumber],
            quotationDate: [this.datePipe.transform(data.quotationDate, AppSettings.DATE_FORMAT)],
            internalReferenceNumber: [data.internalReferenceNumber],
            internalReferenceDate: [this.datePipe.transform(data.internalReferenceDate, AppSettings.DATE_FORMAT)],
            partyId: [data.partyId],
            partyName: [data.partyName, [Validators.required]],
            subTotalAmount: [data.subTotalAmount],
            totalDiscount: [data.totalDiscount ? data.totalDiscount.toFixed(2) : data.totalDiscount],
            discountPercent: [data.discountPercent, [Validators.max(100)]],
            totalTaxableAmount: [data.totalTaxableAmount ? data.totalTaxableAmount.toFixed(2) : data.totalTaxableAmount],
            taxId: [data.taxId],
            roundOffAmount: [data.roundOffAmount],
            grandTotal: [data.grandTotal], //, [Validators.required]
            statusId: [data.statusId],
            financialYearId: [data.financialYearId],
            companyId: [data.companyId],
            purchaseOrderType: [data.purchaseOrderTypeId],
            paymentTerms: [data.paymentTerms],
            deliveryTerms: [data.deliveryTerms],
            termsAndConditions: [data.termsAndConditions],
            taxAmount: [data.taxAmount],
            advanceAmount: [data.advanceAmount],
            sgstTaxRate: [data.sgstTaxRate],
            sgstTaxAmount: [data.sgstTaxAmount],
            cgstTaxRate: [data.cgstTaxRate],
            cgstTaxAmount: [data.cgstTaxAmount],
            igstTaxRate: [data.igstTaxRate],
            igstTaxAmount: [data.igstTaxAmount],
            isReverseCharge: [data.isReverseCharge],
            transportationCharges: [data.transportationCharges],
            purchaseOrderItems: itemArr,
            statusName: [data.statusName],
            gstNumber: [data.gstNumber],
            address:[data.address],
            purchaseOrderEndDate: [this.datePipe.transform(data.purchaseOrderEndDate, AppSettings.DATE_FORMAT)],
            isOpenEnded: [data.isOpenEnded],
            remarks: [data.remarks],
            inclusiveTax: [data.inclusiveTax],
            billToAddress: [data.address],
            shipToAddress: [data.address],

            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile : [data.companyPrimaryMobile],
            companySecondaryMobile : [data.companySecondaryMobile],
            companyContactPersonNumber : [data.companyContactPersonNumber],
            companyContactPersonName : [data.companyContactPersonName],
            companyPrimaryTelephone : [data.companyPrimaryTelephone],
            companySecondaryTelephone : [data.companySecondaryTelephone],
            companyWebsite : [data.companyWebsite],
            companyEmail : [data.companyEmail],
            companyFaxNumber : [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber : [data.companyGstNumber],
            companyPanNumber : [data.companyPanNumber],
            companyPanDate : [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
            // partyName: [data.companyName],
            partyContactPersonNumber: [data.partyContactPersonNumber],
            partyPinCode: [data.partyPinCode],
            partyAreaId: [data.partyAreaId],
            partyCityId: [data.partyCityId],
            partyStateId: [data.partyStateId],
            partyCountryId: [data.partyCountryId],
            partyPrimaryTelephone: [data.partyPrimaryTelephone],
            partySecondaryTelephone: [data.partySecondaryTelephone],
            partyPrimaryMobile: [data.partyPrimaryMobile],
            partySecondaryMobile: [data.partySecondaryMobile],
            partyEmail: [data.partyEmail],
            partyWebsite: [data.partyWebsite],
            partyContactPersonName: [data.partyContactPersonName],
            partyBillToAddress: [data.partyBillToAddress],
            partyShipAddress: [data.partyShipAddress],
            partyDueDaysLimit: [data.partyDueDaysLimit],
            partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
            partyGstNumber: [data.partyGstNumber],
            partyPanNumber: [data.partyPanNumber],
            isIgst: [data.isIgst],
            partyCode:[data.partyCode],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
            updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],
            closePo:[data.closePo],
            currencyName:[data.currencyName],
            partyCurrencyId:[data.partyCurrencyId]
              
        });

        formGroup.get('purchaseOrderItems').setValidators(Validators.required);

        
        this.fieldValidatorLabels.set("partyName", "Party Name");
        this.fieldValidatorLabels.set("purchaseOrderDate", "Po Date");
        this.fieldValidatorLabels.set("purchaseOrderEndDate", "Po End Date");
        this.fieldValidatorLabels.set("purchaseOrderItems", "Purchase order Items");
        this.fieldValidatorLabels.set("materialId", "Material");
        this.fieldValidatorLabels.set("price", "Price");
        this.fieldValidatorLabels.set("quantity", "Quantity");
        this.fieldValidatorLabels.set("amount", "Amount");





        return formGroup;

    }

    // checkInvNumAvailability() {
    //     //console.log("this.transactionType.name.....>>", this.transactionType.name);
    //     // if(this.transactionType.name=="Supplier PO")
    //     // {
    //         let purchaseOrderNumber: string = this.tranForm.get('purchaseOrderNumber').value
    //         //console.log("purchaseOrderNumber.....>",purchaseOrderNumber);
    //         if (purchaseOrderNumber && purchaseOrderNumber.length > 0 && this.transactionType.name === AppSettings.SUPPLIER_PO || this.transactionType.name === AppSettings.CUSTOMER_PO ) {
          
    //           this.poService.checkPONumAvailability(purchaseOrderNumber, this.transactionType.id).subscribe(response => {
     
    //             //console.log("is purchaseOrderNumber avialable? ", response);
    //             if (response.responseStatus === 1) {
    //               //console.log("Yes");
    //             } else {
    //               //console.log("No");
    //               this.alertService.danger( "purchaseOrderNumber " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //                 this.tranForm.controls['purchaseOrderNumber'].patchValue('');
    //                this.purchaseOrderNumberElement.nativeElement.focus();
     
    //             }
     
    //           });
    //         }
    //     // }
    //     // else{
            
    //     // }
    //   }

    onPartyChange(_event: any, party: Party) {
        //console.log("party..........................>>", party);
        
        if (_event.isUserInput) {
            super.onPartyChange(_event, party, this.tranForm.controls['partyId'], <FormArray>this.tranForm.controls['purchaseOrderItems']);

            if (this.selectedParty.id !== undefined) {
                if(this.transactionType.name !== AppSettings.OUTGOING_JOBWORK_PO && this.transactionType.name !== AppSettings.INCOMING_JOBWORK_PO ){
                    this.getQuotationsForAParty(this.selectedParty.id);
                }                
                this.tranForm.controls['billToAddress'].patchValue(this.selectedParty.address);
                this.tranForm.controls['shipToAddress'].patchValue(this.selectedParty.billAddress);
                this.tranForm.controls['gstNumber'].patchValue(this.selectedParty.gstNumber);
                if(this.selectedParty.countryId!=1)

                {       
                    this.tranForm.controls['partyCurrencyId'].patchValue(this.selectedParty.partyCurrencyId);

                    this.tranForm.controls['currencyName'].patchValue(this.selectedParty.partyCurrencyName);
                    this.taxNumberText="CIN Number";

                }else{
                    this.tranForm.controls['partyCurrencyId'].patchValue(0);
                    this.taxNumberText="GST Number";
  
                }
             

            }
        }
        this.taxDisablecChanges()
    }
    taxDisablecChanges(){
          
        
        if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
            {
                this.isheaderTaxDisable=true;//headerlevel
            }
            if(this.selectedParty.countryId==1&& this.globalSetting.itemLevelTax==0)
            { 
               this.isheaderTaxDisable=false;
            }
         
            if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
             {
              
               this.isItemLevelTax=false;
               this.isheaderTaxDisable=true
               
             }
             if(this.selectedParty.countryId==1 && this.globalSetting.itemLevelTax==1)
             { 
                this.isItemLevelTax=true;
             }
    }
    handleChanges() {
        this.handleChangesParent(this.itemOfNewPO, this.poHeader ? this.poHeader.taxId : null);
    }

    addPoItem(materialquantity: number) {

        //this.addTranItem(materialquantity, <FormArray>this.tranForm.controls['purchaseOrderItems'], this.tranForm.get('partyId'));
        this.addItemWrapper(materialquantity, <FormArray>this.tranForm.controls['purchaseOrderItems']);

    }


    closePo(){
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.CLOSE_PO_CONFIRM;

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) { 
                let poId: string = this.tranForm.get('id').value
                console.log(poId+"-----");
               this.poService.closePo(poId,1).subscribe(
                  response => {

                   if(response.responseStatus==1)
{
    console.log(this.transactionStatus+"----")
    this.alertService.success("Po Closed Succesfully");
    this.isPoClosed =false;
    this.applicableButtons.isEditButton=false;
    this.applicableButtons.isSaveButton=false;
}
else{
    this.alertService.danger("Po Closed Failed");
    this.isPoClosed =true;

}
                  }

               )

            }
        });

    }


    getQuotationsForAParty(party: number) {
        //console.log('getQuotationsForAParty: ', party);
        if (party) {
            if (party !== undefined) {
                this.quotationService_po.getQuotationsForParty(party).subscribe(
                    response => {
                        this.quotationHeaders = response;
                        //console.log("PO: ", response);
                    }
                )
            }
        }
    }
    onQuotationNumberChange() {

        let qoNumber: string = this.tranForm.controls['quotationNumber'].value;
        if (qoNumber) {
            //console.log(qoNumber);

            let qo: QuotationHeader = this.quotationHeaders.filter(qo => qo.quotationNumber === qoNumber)[0];
            //console.log("Sss"+qo.quotationNumber);
            //console.log("qo "+qo);
            this.populateItemsWithQuotation(qo);
            this.selectedQuotation = qo;
           
            this.tranForm.controls['quotationDate'].patchValue(this.datePipe.transform(qo.quotationDate, AppSettings.DATE_FORMAT));
            this.tranForm.controls['roundOffAmount'].patchValue(qo.roundOffAmount);
            this.isInclusiveTax = qo.inclusiveTax ? true : false;
            
            this.inclusiveTaxDisable = true; //qo.inclusiveTax  ? true : false;
        }

    }

    populateItemsWithQuotation(quotation: QuotationHeader) {
        
        if (quotation) {
            const control = <FormArray>this.tranForm.controls['purchaseOrderItems'];
            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }
            this.headerTax = this.taxes.filter(tax => tax.id === quotation.taxId)[0];
            quotation.quotationItems.forEach(quotationItem => {
                //see if it is IGST PO
                //console.log("quotationItem", quotationItem)
                if (quotationItem.igstTaxPercentage != undefined && quotationItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                //console.log(" this.isIgst in populate", this.isIgst)
                let tItem: TransactionItem = this.transactionItemService_po.mapItem(quotationItem, this.itemOfQuotation);
                //console.log('tItem: ', tItem);
                control.push(this.transactionItemService_po.makeItem(tItem));
            });
           if (!this.isItemLevelTax) {
                this.tranForm.controls["discountPercent"].patchValue(quotation.discountPercent);
              this.tranForm.controls["totalDiscount"].patchValue(quotation.totalDiscount);
            }
        }
     
       
    }



    onSubmit(model: PurchaseOrderHeader) {
        model.partyName=this.displayPartyName;
        //console.log("model :",model);
        if(this.numberRangeConfiguration.id===null){
            
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
            return;
        }
        
        if(model.purchaseOrderNumber==null && !this.isAutoNumber ||model.purchaseOrderNumber==""){
            if(this.isCust)
            {
                this.alertService.danger( "Please Enter Customer PO Number");
            }
            else{
                this.alertService.danger( "Please Enter Supplier PO Number");
            }
            return
        }

        if(!this.validateTnCNewLines(model.termsAndConditions)){
            return;
        }

        let checkNumAvail: boolean = false;
        //check number avail      
        if(this.isAutoNumber){ //in case of auto number do not check
            checkNumAvail = false;
        }
        else{
            if(model.id != null){ //if we are editing invoice which is not auto number
                if(model.purchaseOrderNumber != this.poHeader.purchaseOrderNumber){ // if invoice number is changed
                    checkNumAvail = true;
                }else{
                    checkNumAvail = false;
                }
            }else{ // if not auto number and creating new invoice, always check
                checkNumAvail = true;
            }
        }

        //console.log("checkNumAvail :", checkNumAvail);

        if(checkNumAvail){
        this.poService.checkPONumAvailability(model.purchaseOrderNumber, this.selectedParty.id,  this.transactionType.id)
        .subscribe(response => {
            if (response.responseStatus === 1) {
                //console.log('In onSubmit: ', model);
                //console.log('Grand total: ', model.grandTotal);
                
        
                //let textToBeAppend: string = this.priceCheck(model);
                let textToBeAppend: string = this.priceCheck(model.purchaseOrderItems);
                if (textToBeAppend.length > 0) {
        
                    this.dialogRef = this.dialog.open(ConfirmationDialog, {
                        disableClose: false
                    });
                    this.dialogRef.componentInstance.confirmMessage = textToBeAppend + AppSettings.PRICE_CONFIRMATION_MESSAGE
                    this.dialogRef.afterClosed().subscribe(result => {
        
                        if (result) {
                            this.save(model);
                        }
        
                    });
                }
                else {
                    this.save(model);
              
                }
            } 
            else{
                this.alertService.danger( "purchaseOrderNumber " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                  this.tranForm.controls['purchaseOrderNumber'].patchValue('');
                 this.purchaseOrderNumberElement.nativeElement.focus();
            }
          });
        }
        else{
            this.save(model);
        }


 

    }

    removePOItem(idx: number) {
        this.removeItem(idx, 'purchaseOrderItems',this.itemOfNewPO, this.poHeader ? this.poHeader.taxId : null);
       
        //this.handleChangesParent(this.itemOfNewPO, this.poHeader ? this.poHeader.taxId : null);
    }
    quantityChange(event: QuantityChangeEvent) {
        this.onQuantityChange(event, 'purchaseOrderItems');
    }

    save(model: PurchaseOrderHeader) {
        console.log("model :------------- ",model.isIgst);
        
        if(model.id===null){
            model = this.transactionItemService_po.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
            //console.log("model ....:",model);   
        }

        
            console.log("this.tranForm.valid: ",this.tranForm.valid);
         if(!this.tranForm.valid){
            const controls = this.tranForm;
            let invalidFieldList: string[] =this.utilityService_po.findInvalidControlsRecursive(controls);
            //console.log("invalidFieldList ", invalidFieldList)
      
            let invalidFiledLabels: string[] = [];
            
            invalidFieldList.forEach(field => {
              if (this.fieldValidatorLabels.get(field))
                invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })
      
            //console.log("invalidFiledLabels ", invalidFiledLabels)
      
            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE+invalidFiledLabels);
            return false;
      }
        //console.log('in save: ', model);
        model.partyId = this.selectedParty.id; //override id to replace whole cusotmer object
        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id; 
         model.isReverseCharge = model.isReverseCharge ? 1: 0;
         model.inclusiveTax = this.isInclusiveTax ? 1 : 0;
         model.isOpenEnded = model.isOpenEnded ? 1 : 0;

        //model.partyId = this.selectedCustomer.id;
        // model.taxId = model.purchaseOrderItems[0].taxId; // hack
        model.taxId = this.headerTax ? this.headerTax.id : model.purchaseOrderItems[0].taxId; // hack
        if (this.headerTax === null && !this.isItemLevelTax) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "Please select tax";
            return;
        }
   



        this.spinStart = true;

        model.purchaseOrderTypeId = this.transactionType.id;//'Customer PO';
        model.taxAmount = model.cgstTaxAmount + model.sgstTaxAmount + model.igstTaxAmount;


        //Assign Serial numbers to the items
        model.purchaseOrderItems.forEach((item, index) => {
            item.slNo = index+1;
        })
        
        //console.log('Before save: ', model);
        //console.log("itemToBeRemove " + this.itemToBeRemove)

        // if (this.itemToBeRemove.length > 0) {
        //     //console.log(" in itemToBeRemove " + this.itemToBeRemove)
        //     this.itemToBeRemove.forEach(itemId =>
        //         this.poService.deleteItem(itemId).subscribe(response => {
        //             //console.log("item id before save on deltion " + itemId);
        //             if (response.responseStatus) {
        //                 //console.log("Item deleted in DB: " + itemId);
        //                 // (<FormArray>this.quotationForm.get('quotationItems')).removeAt(idx);
        //             } else {
        //                 console.error("Cannot delete: ", response.responseString);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }
        //             return false;

        //         },
        //             error => {
        //                 console.error("Cannot delete: ", error);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }))
        // }
        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        }
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }

        let purchaseOrderWrapper: PurchaseOrderWrapper = {itemToBeRemove: this.itemToBeRemove, purchaseOrderHeader: model};

        //caliing post api.....
        this.poService.create(purchaseOrderWrapper)
            .subscribe(response => {
                this.showTheCreatedBy=true;
                this.poHeader = response;
                this.displayPartyName=this.poHeader.partyName;
                this.displayParty=this.poHeader.partyName
                this.poHeader.partyName=this.poHeader.partyCode+"-"+this.poHeader.partyName;
              
                //console.log("saved id: ", response.id);
                //console.log('after save: ', response);
                // this.tranForm.controls['purchaseOrderNumber'].patchValue(response.purchaseOrderNumber);
                // this.tranForm.controls['id'].patchValue(response.id);
                this.isPartyChangeable = false;
                this.tranForm = this.toFormGroup(response, this.itemOfRecentPO)
                if(!response.updateBy){
                    this.poService.getPurchaseOrderById(response.id)
                    .subscribe(responses => {
                        this.tranForm.controls['updateBy'].patchValue(responses.updateBy);
                        this.tranForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));

                           

                    });
                }
                
                
                this.transactionStatus = "PO " + response.purchaseOrderNumber + message;

                this.saveStatus = true;
                this.alertService.success(this.transactionStatus);
                this.spinStart = false;
                this.itemToBeRemove = [];
                this.recentPoLoadCounter++;
                this.isCreateDCButtonenabled()
                this.disableForm();
            },
            error => {
                //console.log("Error ", error);
                this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
                this.alertService.danger(this.transactionStatus);
                this.spinStart = false;
                this.saveStatus = false;
            }
            );
    }


    recentPurchaseOrderGet(event) {

        this.showTheCreatedBy=false;
        //console.log('In recentPurchaseOrderGet', event);
        this.poHeader = event;
       this.isInclusiveTax = this.poHeader.purchaseOrderItems[0].inclusiveTax === 0 ? false : true;
        //console.log("this.isInclusiveTax: ",this.isInclusiveTax,this.poHeader.purchaseOrderItems[0].inclusiveTax);
         this.inclusiveTaxDisable =true;
        this.partyService.getCustomer(this.poHeader.partyId).subscribe(party => {

            this.selectedParty = party;
            this.isIgst = this.poHeader.isIgst > 0 ? true : false; //this.partyService.getIgstApplicable(this.selectedParty);
            this.displayPartyName=this.poHeader.partyName;
            this.displayParty=this.poHeader.partyName;
            this.poHeader.partyName=this.poHeader.partyCode+"-"+this.poHeader.partyName;
            this.tranForm = this.toFormGroup(this.poHeader, this.itemOfRecentPO);
            this.setStatusBasedPermission(this.tranForm.controls['statusId'].value);
            this.itemsControl = this.tranForm.controls['purchaseOrderItems'];
            this.partyFormControl = this.tranForm.controls['partyName'];

            if(this.poHeader.partyCountryId!=1)
            {
                this.taxNumberText="CIN Number"
            }
            else{
                this.taxNumberText="GST Number"
            }
            this.isCreateDCButtonenabled();
            this.disableForm();
        });
        let qoFound: boolean = false;

        this.quotationHeaders.forEach(qoh => {
            if (qoh.quotationNumber === this.poHeader.quotationNumber) {
                qoFound = true;
            }
        });

        if (!qoFound) {
            this.tranForm.controls['quotationNumber'].patchValue(this.poHeader.quotationNumber);
        }
        //console.log('selectedParty: ', this.selectedParty);
        this.headerTax = this.taxes.filter(tax => tax.id === this.poHeader.taxId)[0];
        this.isPartyChangeable = false;
        this.getFiltredMaterial();

    }

    clearForm(formDirective: FormGroupDirective) {

        formDirective.resetForm();
        this.initForm();
        this.isCreateDCButtonenabled();
        super.clearFormParent();
        this.quotationHeaders.length =0;
        this.itemsControl = this.tranForm.controls['purchaseOrderItems'];
        this.handleChanges();
        this.headerTax = null;
        this.getTempTransactionNumber();
        this.tranForm.markAsPristine();
    }

    delete(formDirective: FormGroupDirective) {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE
        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.tranForm.controls['id'].value;//this.poHeader.id;
                //console.log("deleteing..." + id);
                this.poService.delete(id)
                    .subscribe(response => {
                        //console.log(response);
                        if (response.responseStatus === 1) {
                            this.transactionStatus = response.responseString;
                            this.saveStatus = true;
                            this.recentPoLoadCounter++;
                            this.spinStart = false;
                            this.isCreateDCenabled =false;
                            this.alertService.success(this.transactionStatus);
                            this.clearForm(formDirective);
                        } else {
                            this.transactionStatus = response.responseString;

                            this.saveStatus = false;
                            this.spinStart = false;
                            this.alertService.danger(this.transactionStatus);
                        }
                    },
                    error => {
                        //console.log("Error ", error);
                        this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE

                        this.saveStatus = false;
                        this.spinStart = false;
                        this.alertService.danger(this.transactionStatus);
                    });
            }
            this.dialogRef = null;
        });

    }

    isCreateDCButtonenabled()
    {
        //console.log(this.tranForm.controls['statusId'].value + "statys"+this.tranForm.controls['id'].value  ) // && this.tranForm.controls['statusId'].value == 68

        let purchaseOrderEndDate : Date = new Date(this.tranForm.controls['purchaseOrderEndDate'].value)
        let currentDate: Date = new Date();
        purchaseOrderEndDate.setHours(0,0,0,0);
        currentDate.setHours(0,0,0,0);

        // console.log("purchaseOrderEndDate ",purchaseOrderEndDate);
        let isOpenEnded = this.tranForm.controls['isOpenEnded'].value;
        // console.log("isOpenEnded :",isOpenEnded);
        // console.log("purchaseOrderEndDate.getTime() ", purchaseOrderEndDate.getTime())
        // console.log("currentDate.getTime() ", currentDate.getTime())
        // console.log("purchaseOrderEndDate.getTime() === currentDate.getTime(): ",purchaseOrderEndDate.getTime() === currentDate.getTime());
        let validOpen = isOpenEnded && purchaseOrderEndDate ? (purchaseOrderEndDate > currentDate || purchaseOrderEndDate.getTime() === currentDate.getTime() ) : true;
        // console.log("validOpen: ",validOpen);

        if(this.tranForm.controls['id'].value != null 
        && (this.tranForm.controls['statusName'].value == AppSettings.STATUS_NEW || this.tranForm.controls['statusName'].value == AppSettings.STATUS_OPEN ) 
        && (validOpen)
        )
        {
            this.isPoClosed =true;
            this.isCreateDCenabled = true;
        }
        else
        {
            this.isPoClosed =false;
            this.isCreateDCenabled = false;
        }
    }

    createInvoice(){
        let id=0;
        let sourceTransactionType: TransactionType;
        if(this.poHeader && this.poHeader.id)
        this.transCommService.initQuotationData();
        this.transCommService.initPoData();
            this.transCommService.updatePoData(this.poHeader);
            //this.transCommService.poHeader.next(this.poHeader);
            this.transactionTypeService_in.getAllTransactionTypes().subscribe(response => {
              //  this.transactionTypes=response;
                //console.log("this.transactionType.name"+this.transactionType.name);
                response.forEach(tt => {
                    if ( this.transactionType.name === AppSettings.CUSTOMER_PO) {
                      //console.log("tt"+tt)
                     if (tt.name == AppSettings.CUSTOMER_INVOICE)
                     {
                        //console.log("tt.name"+tt.name)
                        sourceTransactionType = tt;
                        this._router_in.navigate(['transaction/app-new-invoice',tt.id]);
                        
                     }
                    }
                    else if( this.transactionType.name === AppSettings.SUPPLIER_PO){
                        if (tt.name == AppSettings.PURCHASE_INVOICE)
                        {
                         console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/app-new-invoice',tt.id]);
                           
                        }
                    }
                    else if( this.transactionType.name === AppSettings.INCOMING_JOBWORK_PO){
                        if (tt.name == AppSettings.INCOMING_JOBWORK_INVOICE)
                        {
                          console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/app-new-invoice',tt.id]);
                           
                        }
                    }
                    else if( this.transactionType.name === AppSettings.OUTGOING_JOBWORK_PO){
                        if (tt.name == AppSettings.OUTGOING_JOBWORK_INVOICE)
                        {
                           //console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/app-new-invoice',tt.id]);
                           
                        }
                    }
                   
                    }); 
            });
    }

    createDeliveryChallan(){
        let id=0;
        let sourceTransactionType: TransactionType;
        if(this.poHeader && this.poHeader.id)
        this.transCommService.initPoData();
        this.transCommService.updatePoData(this.poHeader);
            this.transactionTypeService_in.getAllTransactionTypes().subscribe(response => {
              //  this.transactionTypes=response;
                //console.log("this.transactionType.name"+this.transactionType.name);
                response.forEach(tt => {
                    if ( this.transactionType.name === AppSettings.CUSTOMER_PO) {
                      //console.log("tt"+tt)
                     if (tt.name == AppSettings.CUSTOMER_DC)
                     {
                        //console.log("tt.name"+tt.name)
                        sourceTransactionType = tt;
                        this._router_in.navigate(['transaction/dc-form',tt.id]);
                        
                     }
                    }
                    else if( this.transactionType.name === AppSettings.SUPPLIER_PO){
                        if (tt.name == AppSettings.INCOMING_DC)
                        {
                           //console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/dc-form',tt.id]);
                           
                        }
                    }
                    else if( this.transactionType.name === AppSettings.INCOMING_JOBWORK_PO){
                        if (tt.name == AppSettings.INCOMING_JOBWORK_IN_DC)
                        {
                           //console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/app-job-work-delivery-challan',tt.id]);
                           
                        }
                    }
                    else if( this.transactionType.name === AppSettings.OUTGOING_JOBWORK_PO){
                        if (tt.name == AppSettings.OUTGOING_JOBWORK_OUT_DC)
                        {
                           //console.log("tt.name"+tt.name)
                           sourceTransactionType = tt;
                           this._router_in.navigate(['transaction/app-job-work-delivery-challan',tt.id]);
                           
                        }
                    }
                   
                    }); 
            });
           }

    // print() { 

    //     let l_printCopy: number;
    //     this.printRef = this.dialog.open(PrintDialog, {
    //         width: '250px',
    //         data: { printCopy: l_printCopy }
    //     });
    //     this.printRef.afterClosed().subscribe(result => {
    //         //console.log("result: ", result);
    //         if (result) {
    //             l_printCopy = +result;
    //             let company: Company;
    //             this.companyService.get(1).subscribe(comp => {
    //                 company = comp;
    //                 this.purchaseOrderSecondReportService.download(this.poHeader, company, l_printCopy);
    //             });
    //         }

    //     })

    // }


    edit() {
        super.edit();
        this.itemsControl = this.tranForm.controls['purchaseOrderItems'];
        let statusName: string = this.tranForm.get('statusName').value
        if(statusName !== AppSettings.STATUS_NEW){
            this.tranForm.get('isOpenEnded').disable();
        }else{
            this.tranForm.get('isOpenEnded').enable();
        }
        this.handleChanges();
        // if (this.tranForm.controls['deliveryChallanNumber'].value) {
        //     this.tranForm.controls['invoiceItems'].disable();
        //     this.itemDisableMessage = "Items cannot be changed for DC tracked Invoice"
        // }
    }
    openEndedConformationChange(event: any) {
        if (event.checked) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });
            this.dialogRef.componentInstance.confirmMessage = AppSettings.PO_OPEN_ENDED_CONFORMATION_MSG
            this.dialogRef.afterClosed().subscribe(result => {

                if (result) {
                    this.isChecked = true;
                    this.openEndedPOChange();

                }
                else {
                    this.isChecked = false;
                    return;
                }
            });
        } else {
            this.openEndedPOChange();
        }

    }
    openEndedPOChange(){
      
              
                     let isOpenEnded: boolean = this.tranForm.get('isOpenEnded').value; 
                            if(isOpenEnded){
                                this.tranForm.get('purchaseOrderEndDate').setValidators(Validators.required);
                                this.tranForm.get('purchaseOrderEndDate').updateValueAndValidity();
                            }else{
                                this.tranForm.get('purchaseOrderEndDate').clearValidators();
                                this.tranForm.get('purchaseOrderEndDate').setErrors(null);
                            }
                    
                            //reset values of item quantity to 1 if changed from non open ended to open ended
                            if(isOpenEnded){
                                const control = <FormArray>this.tranForm.controls['purchaseOrderItems'];
                                //Clear any pre populated items exist
                                for(let i=0; i < control.length; i++) { 
                                    control.at(i).get('quantity').patchValue(1);
                                  
                                }
                                this.quantityChangeCounter++;
                            }
                        }
        
                    
         
                
               
    }


