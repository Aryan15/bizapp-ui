import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPoComponentComponent } from './new-po-component.component';

describe('NewPoComponentComponent', () => {
  let component: NewPoComponentComponent;
  let fixture: ComponentFixture<NewPoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
