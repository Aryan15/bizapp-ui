import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Observable, of as observableOf, Subject } from 'rxjs';
import { debounceTime, filter, flatMap, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { Material, Process } from '../../data-model/material-model';
import { ApplicableButtons, QuantityChangeEvent } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { MaterialPriceList, Party, PartyWithPriceList } from '../../data-model/party-model';
import { QuotationHeader } from '../../data-model/quotation-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { Tax } from '../../data-model/tax-model';
import { TermsAndCondition } from '../../data-model/terms-condition-model';
import { TransactionItem } from '../../data-model/transaction-item';
import { TransactionType } from '../../data-model/transaction-type';
import { Uom } from '../../data-model/uom-model';
import { MaterialPopupComponent } from '../../master/material-popup/material-popup.component';
import { PartyPopupComponent } from '../../master/party-popup/party-popup.component';
import { ReferenceLinksComponent } from '../../reference-links/reference-links.component';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { MaterialService } from '../../services/material.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PrintWrapperService } from '../../services/print-warpper.service';
import { QuotationService } from '../../services/quotation.service';
import { TaxService } from '../../services/tax.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { TransactionSummaryService } from '../../services/transaction-summary.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
//import { PrintDialog } from '../../utils/print-dialog';
import { PrintDialogContainer } from '../../utils/print-dialog-container';
import { UtilityService } from '../../utils/utility-service';
import { CompanyService } from '../../services/company.service';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';
import { ConnectionService } from '../../services/connection.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';
import { Currency } from '../../data-model/currency-model';
import {CurrencyPipe} from '@angular/common';

///import {provideTranslator, TranslatorContainer, Translator} from 'angular-translator';

@Component({
    selector: 'app-transaction-parent',
    templateUrl: './transaction-parent.component.html',
    styleUrls: ['./transaction-parent.component.scss']
})
export class TransactionParentComponent implements OnInit, OnDestroy {

    tranForm: FormGroup;
    itemsControl: AbstractControl;
    itemsIn: any[];
    transactionType: TransactionType;
    transactionId: string;
    loadFromId: boolean = false;
    numberRangeConfiguration: NumberRangeConfiguration;
    isAutoNumber: boolean = false;
    date: Date;
    deliveryTerms: string;
    paymentTerms: String;
    termsAndConditions: string;

    isContainer:boolean = false;
    isTcsAmount:boolean=false;

    termsAndConditionObjects: TermsAndCondition[] = [];

    dialogRef: MatDialogRef<ConfirmationDialog>;
    alertRef: MatDialogRef<AlertDialog>;
    printRef: MatDialogRef<PrintDialogContainer>;

    defaultDate: string;
    materials: Material[] = [];
    currencys: Currency[]=[];
    currency:Currency;
    partyPlaceHolder: string;
    isCust: boolean;
    isJW: boolean
    isIgst: boolean = false;
    isImport:number;
    isStockCheckRequired: boolean = false;
    isNote: boolean = false;
    displayParty:string="";
    tcsAmount:number;
    tcsPercentage:number;

    applicableButtons: ApplicableButtons = {
        isApproveButton: false,
        isClearButton: true,
        isCloseButton: true,
        isCancelButton: false,
        isDeleteButton: true,
        isPrintButton: true,
        isSaveButton: true,
        isEditButton: true,
        isDraftButton: false,
    };
    buyOrSell: string;
    //parties: Party[] = [];
    activityRoleBindings: ActivityRoleBinding[];
    enableSaveButton: boolean = false;
    globalSetting: GlobalSetting;
    isRoundOffAmount: boolean;
    isItemLevelTax: boolean;
    //isInclusiveTaxApplicable: boolean;
    inclusiveTaxDisable: boolean;
    isInclusiveTax: boolean;
    isItemLevelDiscount: boolean;
    isShowOnlyCustomerMaterialInSales: boolean;
    isItemGrouping: boolean;
    quantityChangeCounter: number = 0;
    uoms: Uom[] = [];
    taxes: Tax[] = [];
    financialYear: FinancialYear;
    financialMinDate: Date;
    financialMaxDate: Date;
    maxDate: Date;
    statusBasedPermission: StatusBasedPermission;
    datePipe = new DatePipe('en-US');
     currencyPipe = new CurrencyPipe('en-IN');

    headerTax: Tax;
    totalTaxAmount: number;
    grandTotalOriginal: number;
    totalTaxableAmountOriginal: number;
    originalGrandTotal: number;
    filteredParties: Observable<Party[]>;
    materialControl: FormControl = new FormControl();
    isPartyChangeable: boolean = true;
    partyFormControl: AbstractControl;
    materialPriceLists: MaterialPriceList[] = [];
    isReverseChargeApplicable: boolean;
    quotationHeaders: QuotationHeader[] = [];
    filteredMaterials: Observable<Material[]>;
    material: Material;
    process: Process[] = [];
    tnc: TermsAndCondition;    
    uom: Uom;
    tax: Tax;
    itemToBeRemove: string[] = [];
    //public dialog: MatDialog;
    transactionStatus: string = null;
    saveStatus: boolean = false;
    spinStart: boolean = false;
    disableParty: boolean = false;
    selectedParty: Party;
    displayableGrandTotal: number = 0;
    supplyTypeControl: FormControl = new FormControl();
    initComplete: Subject<boolean> = new Subject();
    isRoundOffDisabled: boolean = false;
    materialQuantity: number = 1;
    grandTotalWithoutReverse: number;
    onDestroy : Subject<boolean> = new Subject();
    tempTransactionNumber: string;
    public company: Company;
    
    gstrate: number = 0;
    taxCount: number = 0;
    activityId:number=0;
    finalGrandTotalAmountforExport: number;
    
    constructor(private route: ActivatedRoute,
        private _router: Router,
        private transactionTypeService: TransactionTypeService,
        private numberRangeConfigService: NumberRangeConfigService,
        private termsAndConditionsService: TermsAndConditionsService,
        private utilityService: UtilityService,
        private materialService: MaterialService,
        protected partyService: PartyService,
        private userService: UserService,
        private globalSettingService: GlobalSettingService,
        private uomService: UomService,
        private taxService: TaxService,
        private financialYearService: FinancialYearService,
        protected quotationService: QuotationService,
        protected alertService: AlertService,
        private transactionItemService: TransactionItemService,
        private transactionSummaryService: TransactionSummaryService,
        protected dialog: MatDialog,
        private printWrapperService: PrintWrapperService,
        private companyService: CompanyService,
        private locationService: LocationService,
        protected connectionService: ConnectionService
    ) { }

    ngOnInit() {

    } 

    ngOnDestroy() {
        ////console.log('ngOnDestory');
        this.onDestroy.next(true);
        this.onDestroy.complete();
        //this.tranCommSub.unsubscribe();
        
    }

    initParent() {

        this.company = this.companyService.getCompany();
        ////console.log("this.company :", this.company);   
        
        this.spinStart = true;
        
        this.route.params.pipe(takeUntil(this.onDestroy)
            , flatMap(result => {
                ////console.log('result1: ', result);
                ////console.log('got id: ', result['id']);
                if(result['id']){
                    this.transactionId = result['id'];
                    this.loadFromId = true;
                }
                else{
                    this.loadFromId = false;
                }
                
                return this.transactionTypeService.getTransactionType(+result['trantypeId'])
            })
            //set transaction type
            //get number range config
            , flatMap(result => {
                ////console.log('result2: ', result);
                this.transactionType = result;
                this.deriveBuyOrSell();
                this.getTempTransactionNumber();
                return this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id);
            })
            //get terms and cond
            , flatMap(result => {
                this.numberRangeConfiguration = result;
                this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;

                console.log('this.numberRangeConfiguration: ', this.numberRangeConfiguration, this.isAutoNumber, this.transactionType.id);
                return this.termsAndConditionsService.getByTransactionTypeAndDefault(this.transactionType.id);
            })
            , flatMap(result => {
                if (result) {
                    this.deliveryTerms = result.deliveryTerms;
                    this.paymentTerms = result.paymentTerms;
                    this.termsAndConditions = result.termsAndCondition;

                }
                //get default date
                return this.termsAndConditionsService.getByTransactionType(this.transactionType.id);
            })
            , flatMap(result => {
                if (result) {
                    this.termsAndConditionObjects = result;
                }
                //get default date
                return this.utilityService.getCurrentDateObs();
            })
            , flatMap(result => {
                this.defaultDate = result;
                return this.globalSettingService.getGlobalSetting();
            })
                      
            
        )
        .subscribe(response => {
            
            this.globalSetting = response;
           
          
            ////console.log("this.globalSetting: ", this.globalSetting);
            this.isRoundOffAmount = this.globalSetting.roundOffTaxTransaction === 1 ? true : false;
            //this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
            if(this.buyOrSell === AppSettings.TXN_SELL){
                this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
            }else{
                this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
            }
            //FOR JOBWORK/SUBCONTRACT keep always item level tax
            if(this.isJW){
                this.isItemLevelTax = true;
            }
            ////console.log("this.isItemLevelTax: ", this.isItemLevelTax);
            // this.isInclusiveTaxApplicable = this.globalSetting.inclusiveTax === 1 ? true : false;
            this.isInclusiveTax = this.globalSetting.inclusiveTax === 1 ? true : false;
            this.isItemLevelDiscount = this.isItemLevelTax ;//this.globalSetting.itemLevelDiscount === 1 ? true : false;
            this.isShowOnlyCustomerMaterialInSales = this.globalSetting.showOnlyCustomerMaterialInSales === 1 ? true : false;
            this.isItemGrouping = this.globalSetting.disableItemGroup === 1 ? true : false;
            this.isStockCheckRequired = this.globalSetting.stockCheckRequired === 1 ? true : false;

        
            
            //get all materials
            ////console.log('this.defaultDate: ',this.defaultDate);
            this.supplyTypeControl.patchValue(1);
            // return this.materialService.getAllMaterials();

            //this.materials = result;
            //get all parties
            

            this.supplyTypeControl.valueChanges.subscribe(res => {
                this.materialControl.patchValue('');
            });

            // this.parties = result;
            
            ////console.log("Befire initComplete in parent...", this.activityRoleBindings);            
            this.initComplete.next(true);
            //this.initComplete.complete();
            this.spinStart = false;
            
            this.getFilteredParties();
            this.getFiltredMaterial();
            //get global settings
            //this.getGlobalSetting();

        })

        
        this.userService.setActivityRoleBinding(this.route)
        .subscribe(result => {
            this.activityRoleBindings = result;

            this.activityRoleBindings.forEach(activityRoleBinding => {
                this.activityId=activityRoleBinding.activityId;
                
                this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
               
            });
        })



        //get uoms
        this.getUoms();

        //get taxes
        this.getTaxes();

        //get financialyear
        this.getFinancialYear();

        //get terms and conditions
        // this.getAllTermsAndConditions();

        //init form -- keep it last in the child form

        //subscribe to material autocomplete control -- make it seperate fn call from child once form is initialized

        //subscribe to party autocomplete control -- make it seperate fn call from child once form is initialized
        this.maxDate = new Date();

    }

    // getGlobalSetting() {

    //     this.globalSettingService.getGlobalSetting().subscribe(response => {
    //         this.globalSetting = response;
    ////         //console.log("this.globalSetting: ", this.globalSetting);
    //         this.isRoundOffAmount = this.globalSetting.roundOffTaxTransaction === 1 ? true : false;
    //         //this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
    //         if(this.buyOrSell === AppSettings.TXN_SELL){
    //             this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
    //         }else{
    //             this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
    //         }
    ////         //console.log("this.isItemLevelTax: ", this.isItemLevelTax);
    //        // this.isInclusiveTaxApplicable = this.globalSetting.inclusiveTax === 1 ? true : false;
    //         this.isInclusiveTax = this.globalSetting.inclusiveTax === 1 ? true : false;
    //         this.isItemLevelDiscount = this.isItemLevelTax ;//this.globalSetting.itemLevelDiscount === 1 ? true : false;
    //         this.isShowOnlyCustomerMaterialInSales = this.globalSetting.showOnlyCustomerMaterialInSales === 1 ? true : false;
    //         this.isItemGrouping = this.globalSetting.disableItemGroup === 1 ? true : false;
    //         this.isStockCheckRequired = this.globalSetting.stockCheckRequired === 1 ? true : false;

    //     })

    // }
    getUoms() {
        this.uomService.getAllUom().subscribe(
            response => {
                this.uoms = response;
            }
        )
    }

    getTaxes() {
        this.taxService.getAllTaxes().subscribe(
            response => {
                this.taxes = response;
            }
        )
    }

    getFinancialYear() {
        this.financialYearService.getFinancialYear().subscribe(
            response => { 
                this.financialYear = response;
                ////console.log("response", response)
                this.financialMinDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                this.financialMaxDate = new Date(this.financialYear.endDate);
                this.date = new Date();
                if (this.financialMaxDate < this.date) {
                    // this.alertRef = this.dialog.open(AlertDialog, {
                    //     disableClose: false
                    // });
                    let msg = `Please note that the financial year(FY) has changed.  To create transactions pertaining to current FY, you need to make the current FY as "Active" in settings.
                    <br/>  Before switching to new FY, please make sure that you have entered all transactions pertaining to previous FY.
                    <br/> <br/> For clarifications, please get in touch with our support team.`;
                    // this.alertRef.componentInstance.alertMessage = msg
                    this.connectionService.warningInfo.next(msg); 
                    
                }else{
                    this.connectionService.warningInfo.next(null); 
                }
                ////console.log("this.minDate", this.financialMinDate)
                ////console.log("this.maxDate", this.financialMaxDate)
            }
        )
    }
    // getAllTermsAndConditions(){
    //     this.termsAndConditionsService.getAllTncs().subscribe(
    //         response=>{
    //             this.ter
    //         }
    //     )
    // }

    deriveBuyOrSell() {
        let sellTranTypes: string[] = [
            AppSettings.CUSTOMER_PO, 
            AppSettings.CUSTOMER_INVOICE, 
            AppSettings.CUSTOMER_DC, 
            AppSettings.CUSTOMER_QUOTATION, 
            AppSettings.PROFORMA_INVOICE, 
            AppSettings.CREDIT_NOTE, 
            AppSettings.INCOMING_JOBWORK_INVOICE, 
            AppSettings.INCOMING_JOBWORK_PO,
            AppSettings.INCOMING_JOBWORK_INVOICE
        ];
        let buyTranTypes: string[] = [
            AppSettings.SUPPLIER_PO, 
            AppSettings.PURCHASE_INVOICE, 
            AppSettings.INCOMING_DC, 
            AppSettings.DEBIT_NOTE, 
            AppSettings.OUTGOING_JOBWORK_PO, 
            AppSettings.OUTGOING_JOBWORK_INVOICE
        ] ;
        if (sellTranTypes.includes(this.transactionType.name)) {
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
            this.isCust = true;
            this.applicableButtons.isPrintButton = true;
            ////console.log("getCustomers this.applicableButtons.isPrintButton ", this.applicableButtons.isPrintButton)
            this.buyOrSell = AppSettings.TXN_SELL;
        } else if (buyTranTypes.includes(this.transactionType.name)) {
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
            this.isCust = false;
            //this.applicableButtons.isPrintButton = false;
            ////console.log("getSuppliers this.applicableButtons.isPrintButton ", this.applicableButtons.isPrintButton)
            this.buyOrSell = AppSettings.TXN_PURCHASE;
        }
        if(this.transactionType.name === AppSettings.DEBIT_NOTE || this.transactionType.name === AppSettings.CREDIT_NOTE){
            this.isNote = true;
            this.applicableButtons.isPrintButton = true;
        }else{
            this.isNote = false;
        }
        let JW_TXN_LIST: string[]=[
              AppSettings.INCOMING_JOBWORK_INVOICE
            , AppSettings.INCOMING_JOBWORK_IN_DC
            , AppSettings.INCOMING_JOBWORK_OUT_DC
            , AppSettings.INCOMING_JOBWORK_PO
            , AppSettings.OUTGOING_JOBWORK_INVOICE
            , AppSettings.OUTGOING_JOBWORK_IN_DC
            , AppSettings.OUTGOING_JOBWORK_OUT_DC
            , AppSettings.OUTGOING_JOBWORK_PO
        ]
        // if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE){
        if(JW_TXN_LIST.includes(this.transactionType.name)){
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_VENDOR
            this.isJW = true;
        }else{
            this.isJW = false;
        }
    }z


    setStatusBasedPermission(statusId: number) {

        if (statusId) {
            ////console.log('status id: ', statusId);
            this.userService.getStatusBasedPermission(statusId).subscribe(response => {
                this.statusBasedPermission = response;
            })
        } else {
            ////console.log("no status id: ", statusId);
        }
    }

    getFilteredParties() {

        let partyType: string = this.isCust ? AppSettings.PARTY_TYPE_CUSTOMER : AppSettings.PARTY_TYPE_SUPPLIER;
        if(this.isJW){
            partyType = AppSettings.PARTY_TYPE_ALL;
        }
        this.filteredParties = this.partyFormControl.valueChanges
            .pipe(
            startWith<string | Party>(''),
            debounceTime(300),
            map(value => typeof value === 'string' ? value : value.name),
            filter(value => value != (this.selectedParty ? this.selectedParty.name : '')),
            //// tap(val => //console.log('party search: ', val)),
            //map(val => this.filterParty(val))
            switchMap(value => this.partyService.getPartiesByNameLike(partyType, value) )
            );
    }
    displayFnParty(party?:Party): string | undefined {
        return party ? party.partyCode+"-"+party.name + (party.areaName ? '['+party.areaName+']' : ''): undefined;
    }
    // filterParty(val: string): Party[] {
    ////     ////console.log('in filter: ', val);
    ////     ////console.log('parties ', this.parties);
    //     if (val) {
    //         // return this.parties.filter(party =>

    //         //     party.name.toLowerCase().includes(val.toLowerCase())
    //         // );
    //         if(this.isCust)
    //              this.partyService.getPartiesByNameLike(AppSettings.PARTY_TYPE_CUSTOMER, val)
    //             .subscribe(result => {
    //                 return result;
    //             })
    //     }
    //     return this.parties;
    // }

    getFiltredMaterial() {
        this.filteredMaterials = this.materialControl.valueChanges
            .pipe(
            startWith<string | Material>(''),
            debounceTime(300),            
            map(value => typeof value === 'string' ? value : value.name),
            //// tap(value => //console.log("value and material name"+value+":"+(this.material ? this.material.name: ''))),
            filter(value => value != (this.material ? this.material.name : '')),
            // map(val => this.filterMaterial(val))
            switchMap(value => this.getMaterial(value) )
            );
    }

    getMaterial(value): Observable<Material[]>{
        //////console.log("in getMaterial: "+value);
        let JW_TT: string[] = [AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_INVOICE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE];
        if(JW_TT.includes(this.transactionType.name)){
            ////console.log("1")
            return this.materialService.getJobworkMaterials(value);
        }else{
            ////console.log("2")
            return this.materialService.getMaterialByNameAndSupplTypeAndParty(value, this.supplyTypeControl.value, this.selectedParty? this.selectedParty.id: null, this.buyOrSell)
        }
    }
    // filterMaterial(val: string): Material[] {
    ////     //console.log('in filterMaterial: ', this.materials);
    //     return this.materials.filter(mat => mat.supplyTypeId === +this.supplyTypeControl.value)
    //         .filter(mat => mat.name.toLowerCase().includes(val.toLowerCase()));


    // }

    displayFnMaterial(material?: Material): string | undefined {
        return material ? material.name + (material.partNumber ? '['+material.partNumber+']' : ''): undefined;
    }

    getIsIgst(party: Party): boolean {
        return this.partyService.getIgstApplicable(party);
    }


    onMaterialSelectChange(_event: any, materialOption: Material) {

     //console.log('materialOption: ', materialOption, _event);
        if(materialOption.isContainer===1){
            this.isContainer=true
        }
        else{
            this.isContainer = false;
        }
        
        if (_event.isUserInput) {

            //let materialOptions: Material[] = [];
            //materialOptions.push(materialOption);

            let foundObj = materialOption; //this.materials.find(mat => mat.name === 'One');
            if (foundObj) {
                foundObj = JSON.parse(JSON.stringify(foundObj));
            }
            this.material = foundObj;

           // this.material = [...materialOptions].pop();
            //this.material = materialOption;
            this.onMaterialSelect();
        }

    }
    onMaterialSelect() {

        ////console.log('this.material: ', this.material);
        this.materials.push(this.material);
        this.uom = this.uoms.filter(uom => uom.id === this.material.unitOfMeasurementId)[0];
        let defaultTax: Tax = {
            deleted: "N",
            id: 1,
            name: "5%_TAX",
            rate: 5,
            taxTypeId: 1
        };
       this.tax = defaultTax; //this.taxes.filter(tax => tax.id === ;//hardcode to 5per

        if(this.material.isContainer){
            let dummyTax: Tax = {
                deleted: "N",
                id: null,
                name: "0%_TAX",
                rate: 0,
                taxTypeId: null
            };

            this.taxes.push(dummyTax);
            this.tax = dummyTax;
            this.material.price = 0;
        }
        if (!this.isItemLevelTax) {
            this.headerTax = this.tax;
        }
    }


    removeItem(idx: number, itemArrayName: string,itemType: string, taxIdIn: number) {

        // (<FormArray>this.poForm.get('purchaseOrderItems')).removeAt(idx);
        let itemId: string = (<FormArray>this.tranForm.get(itemArrayName)).at(idx).get("id").value;
        ////console.log("Itemid " + itemId)
        if (itemId) {

            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

            this.dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.itemToBeRemove.push(itemId);                    
                    this.updateHeaderTax(itemArrayName);
                    (<FormArray>this.tranForm.get(itemArrayName)).removeAt(idx);
                    this.handleChangesParent(itemType, taxIdIn);
                }

            });
        } else {
            //
            // else just delete in the array
            //
            this.updateHeaderTax(itemArrayName);
            (<FormArray>this.tranForm.get(itemArrayName)).removeAt(idx);
            this.handleChangesParent(itemType, taxIdIn); 
        }
       
        return false;
    }


    updateHeaderTax(itemArrayName: string) {
        if(!this.isItemLevelTax){

            let itemArray: any[] = this.tranForm.get(itemArrayName).value;
            ////console.log("itemArray: ", itemArray);
            if (itemArray.length > 1 && this.materials.length > 0 ) {
                let materialId: number = itemArray[itemArray.length - 2].materialId;
                ////console.log("materialId: ", materialId)
                let mat: Material = this.materials.find(m => m.id === materialId);
                if(mat){
                    let taxId: number = mat.taxId;
                    ////console.log("taxId: ", taxId);
                    let tax: Tax = this.taxes.find(t => t.id === taxId);
                    ////console.log("tax: ", tax);
                    this.headerTax = tax;
                    this.gstrate = tax.rate;
                }
            }
        }
    }
        
    disableForm() {
        this.tranForm.disable({ onlySelf: true, emitEvent: false });
        this.disableParty = true;
        this.materialControl.disable({ onlySelf: true, emitEvent: false });
        this.tranForm.markAsPristine();
    }


    onPartyChange(_event: any, party: Party, partyControl: AbstractControl, tranItems: FormArray) {

        ////console.log('party: ', party, _event);
        if (_event.isUserInput) {
            partyControl.patchValue(party.id);
         
            this.selectedParty = party;
            this.isImport=this.selectedParty.countryId;
            console.log(" on party chnage in parent = "+this.selectedParty.countryId)
            // this.selectedParty = this.parties.find(p => p.id === partyId);
            this.tranForm.controls['address'].patchValue(this.selectedParty.address)
            if (this.selectedParty.id !== undefined) {
                //// //console.log('this.parties: ', this.parties);
                ////console.log('party id: ', this.selectedParty.id);
                this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);

                ////console.log('isIgst: ', this.isIgst);
                ////console.log('this.tranForm.get(isIgst).value(): ', this.tranForm.controls['isIgst'].value);

                //Get customer/supplier specific price list
                this.partyService.getMaterialPriceListForParty(this.selectedParty.id).subscribe(response => {
                    this.materialPriceLists = response;
                    ////console.log('this.materialPriceLists in party change: ', this.materialPriceLists);
                });
            }

            if (this.selectedParty.gstRegistrationTypeId === 3) {
                this.isReverseChargeApplicable = true
            } else {
                this.isReverseChargeApplicable = false;
            }

            ////console.log("this.isShowOnlyCustomerMaterialInSales: ",this.isShowOnlyCustomerMaterialInSales);

            // if (this.isShowOnlyCustomerMaterialInSales && this.buyOrSell === AppSettings.TXN_SELL) {

            //     this.materialService.getMaterilsForParty(this.selectedParty.id).subscribe(mats => {
            ////         //console.log('this.partyId in partyId: ', this.selectedParty.id)
            //         //let newMaterials: Material[] = [];
            ////         ////console.log("this.materials", this.materials)
            //         // newMaterials = mats.filter(material => material.partyId === this.selectedParty.id);
            ////         //console.log("newMaterials", mats)
            //         this.materials = mats;
            //         this.materialControl.patchValue('');
            //     })


            // }
            this.getFiltredMaterial();


            //Clear any pre populated items exist
            while (tranItems.length !== 0) {
                tranItems.removeAt(0);
            }
        }
        ////console.log('materials after party change: ', this.materials);

    }



    addTranItem(materialquantity: number, control: FormArray, partyControl: AbstractControl) {

        ////console.log('this.material.id: ', this.material.id+materialquantity);
        //const control = <FormArray>this.quotationForm.controls['quotationItems'];
        ////console.log('control: ', control);


        ////console.log('this.isInclusiveTax : ', this.isInclusiveTax );

        //Update material with party specific prices and discounts
        ////console.log('material price list: ', this.materialPriceLists);

        let materialPriceList: MaterialPriceList;
        if (this.materialPriceLists.length > 0) {
            materialPriceList = this.materialPriceLists.find(mpl => mpl.materialId === this.material.id)
            ////console.log('materialPriceList: ', materialPriceList)
        }

        if (materialPriceList) {
            if (this.buyOrSell === AppSettings.TXN_PURCHASE)
                this.material.buyingPrice = materialPriceList.sellingPrice;
            else if (this.buyOrSell === AppSettings.TXN_SELL)
                this.material.price = materialPriceList.sellingPrice;
            this.material.discountPercentage = materialPriceList.discountPercentage;
        }

        ////console.log('material after updated with price: ', this.material);

        //Disable is inclusive check box if enabled
        this.inclusiveTaxDisable = true;
        let tempMaterial = this.material;
        let tempTax = this.tax;
        let tempUom = this.uom;
        let tempIsIgst = this.isIgst;
        let tempbuyOrSell = this.buyOrSell;
        let tempglobalSetting = this.globalSetting;
        let tempisInclusiveTaxApplicable = this.isInclusiveTax;

        ////console.log("this.isItemGrouping", this.isItemGrouping)
        const controlWithSameMaterial = <FormGroup>control.controls.filter(item => item.get('materialId').value === this.material.id)[0];
        this.checkStockQuantity(materialquantity, tempMaterial).subscribe(val => {
            ////console.log("val", val)
            if (val) {
                if (controlWithSameMaterial && this.isItemGrouping === false) {
                    controlWithSameMaterial.get('quantity').patchValue(+controlWithSameMaterial.get('quantity').value + +materialquantity);
                    this.quantityChangeCounter++;
                }
                else {
                    control.push(this.transactionItemService.initTransactionItem(control.length + 1, tempMaterial, tempUom, materialquantity, tempTax, tempIsIgst, tempbuyOrSell, tempglobalSetting, tempisInclusiveTaxApplicable, this.process));
                }
            }
            //    checkStockQuantity= val;
        })
        this.material = undefined;
        this.materialQuantity = 1;
        this.uom = undefined;
        this.tax = undefined;
        this.process = undefined;
        this.materialControl.patchValue('');
        //Disabling party selection after item addition

        partyControl.disable();

        return false;
    }

    handleChangesParent(
        itemType: string
        , taxIdIn: number) {
        ////console.log('in handleChanges');
        ////console.log('is form enabled? ', this.tranForm.enabled);


        //let items: InvoiceItem[] = this.invoiceForm.controls['invoiceItems'].value;

        //this.handleItems(itemType, taxIdIn);

        this.itemsControl.valueChanges.subscribe(items => {
            ////console.log("item control changed");
            if (this.tranForm.enabled){
                this.handleItems(itemType, taxIdIn);

                if(this.tranForm.get('discountPercent').value > 0 && !this.isItemLevelTax){
                    this.headerDiscountPerChange();
                }
            }

        })

        this.setStatusBasedPermission(this.tranForm.controls['statusId'].value);
      
    }


    handleItems(itemType: string, taxIdIn: number) {

        ////console.log('in handleItems: ', this.itemsIn);
        //Map invoice item to TransactionItem
        let tItems: TransactionItem[] = [];

        this.itemsIn = this.itemsControl.value;
        this.itemsIn.forEach(item => {
            tItems.push(this.transactionItemService.mapItem(item, itemType));
        })

        ////console.log("control.length",this.itemsIn.length);
        if (this.itemsIn.length === 0) {
           this.inclusiveTaxDisable = false;
        }
        if (!this.headerTax) {
            this.headerTax = this.taxes.filter(tax => tax.id === taxIdIn)[0];
        }

        ////console.log('this.headerTax: ', this.headerTax,tItems);
        this.transactionSummaryService.patchAmounts(tItems, this.tranForm, this.isItemLevelTax, this.isIgst, this.headerTax, this.isInclusiveTax);


        this.totalTaxAmount = this.tranForm.controls['sgstTaxAmount'].value
            +
            this.tranForm.controls['cgstTaxAmount'].value
            +
            this.tranForm.controls['igstTaxAmount'].value;
        ////console.log(" this.totalTaxAmount"+ this.totalTaxAmount);
        this.grandTotalOriginal = this.tranForm.controls['grandTotal'].value;
        this.totalTaxableAmountOriginal = this.tranForm.controls['totalTaxableAmount'].value;

        this.originalGrandTotal = this.tranForm.controls['grandTotal'].value;
       if(this.transactionType.name===AppSettings.CUSTOMER_INVOICE||this.transactionType.name===AppSettings.PROFORMA_INVOICE||this.transactionType.name===AppSettings.PURCHASE_INVOICE||this.transactionType.name===AppSettings.CREDIT_NOTE||this.transactionType.name===AppSettings.DEBIT_NOTE||this.transactionType.name===AppSettings.INCOMING_JOBWORK_INVOICE||this.transactionType.name===AppSettings.OUTGOING_JOBWORK_INVOICE){
            this.tranForm.controls['tcsPercentage'].patchValue(0);
           this.tranForm.controls['tcsAmount'].patchValue(0);
       }
       let partyCountryId :number =this.tranForm.controls['partyCountryId'].value;
      // console.log("partyCountryId = "+partyCountryId);
       if(partyCountryId!=1 && this.transactionType.name===AppSettings.CUSTOMER_INVOICE || this.transactionType.name===AppSettings.PROFORMA_INVOICE || this.transactionType.name===AppSettings.CREDIT_NOTE)
       {
        this.tranForm.controls['tcsPercentage'].patchValue(0);
        this.tranForm.controls['tcsAmount'].patchValue(0);

        let currencyRate: number = this.tranForm.controls['currencyRate'].value;
        let currencyId  :any  = this.tranForm.controls['currencyId'].value;
        //console.log("displayAmt = "+this.displayableGrandTotal+"----"+currencyRate)
        //this.tranForm.controls['totalAmountCurrency'].patchValue(this.tranForm.controls['totalTaxableAmount'].value);//taxonvoice works correctly
         if(currencyRate!=null)
        {
           //console.log("--------")
          this.onCurrencyRateChange()
       
        } 
       }
       this.calculateGrandtotal();
       
      
           
    }


    calculateGrandtotal() {

        let reverseAmt: number = 0;
        let roundOffAmt: number = 0;
        let advanceAmount: number = 0;
        ////console.log("  this.originalGrandTotal", this.originalGrandTotal);
        this.displayableGrandTotal = this.originalGrandTotal;

        if (this.tranForm.controls['advanceAmount']) {


            advanceAmount = this.tranForm.controls['advanceAmount'].value;
            if (this.checkAdvanceAmt(advanceAmount, this.displayableGrandTotal)) {
                this.tranForm.controls['advanceAmount'].patchValue("");
                ////console.log("  this.advanceAmount", advanceAmount)
                return;
            }
        }

        this.displayableGrandTotal = this.originalGrandTotal - advanceAmount;

        if (this.tranForm.controls['isReverseCharge']) {

            ////console.log("isReverseCharge"+this.tranForm.controls['isReverseCharge'].value);
            let isReverseCharge: boolean = this.tranForm.controls['isReverseCharge'].value;

            ////console.log("  this.advanceAmount displayableGrandTotal", this.displayableGrandTotal,isReverseCharge )
            if (isReverseCharge) {

                this.displayableGrandTotal = this.displayableGrandTotal - this.totalTaxAmount;
                if (this.checkAdvanceAmt(advanceAmount, this.displayableGrandTotal)) {
                    this.tranForm.controls['isReverseCharge'].patchValue(false);
                    return;
                }
                ////console.log("after  this.isReverseChargeApplicable displayableGrandTotal", this.displayableGrandTotal)
            }
        }

        // if (this.isRoundOffAmount) {
        let roundOFf: number = 0;
        
        if (this.isRoundOffAmount === true) {
            roundOFf = +(Math.round(this.displayableGrandTotal) - this.displayableGrandTotal).toFixed(2);
            this.displayableGrandTotal = Math.round(this.displayableGrandTotal);
            ////console.log(" in rd grandtotal " + this.displayableGrandTotal)
            this.tranForm.controls['roundOffAmount'].patchValue(roundOFf);
            ////console.log("after  this.isRoundOffAmount displayableGrandTotal", this.displayableGrandTotal)
          
                this.tranForm.controls['grandTotal'].patchValue(this.displayableGrandTotal);
               
              // let value = this.tranForm.controls['totalDiscount'].value;
            // console.log("this.currencyPipe.transform(this.displayableGrandTotal)"+this.currencyPipe.transform(this.displayableGrandTotal))
           
        }
        else {
            roundOFf = 0.00;
            this.tranForm.controls['roundOffAmount'].patchValue(roundOFf);
            this.tranForm.controls['grandTotal'].patchValue(this.displayableGrandTotal);
        }
        // }
       
    }
    onCurrencyRateChange(){
      console.log("onCurrencyRateChange")   
        let currencyRate: number = this.tranForm.get('currencyRate').value;
        console.log("currencyRate = "+currencyRate)
           //let totalTaxableAmount = this.tranForm.get('totalTaxableAmount').value;
        
        let currencyAmount: number = this.tranForm.get('totalAmountCurrency').value;
        console.log("currencyAmount = "+currencyAmount)
        let totalAmount=currencyAmount*currencyRate;
        console.log("totalAmount = "+totalAmount)
            this.displayableGrandTotal = totalAmount;
            console.log("displayableGrandTotal = "+  this.displayableGrandTotal)
            let finalGrandTotalAmount: number = +totalAmount;
            console.log("finalGrandTotalAmount = "+finalGrandTotalAmount)
            this.tranForm.controls['grandTotal'].patchValue(finalGrandTotalAmount);
            this.originalGrandTotal = this.tranForm.controls['grandTotal'].value;
          
        // this.tranForm.controls['totalTaxableAmount'].patchValue(totalAmount.toFixed(2));
        // this.tranForm.controls['grandTotal'].patchValue(totalAmount.toFixed(2));
        // this.originalGrandTotal = this.tranForm.controls['grandTotal'].value;
        this.calculateGrandtotal();
      // this.numberInWords( this.originalGrandTotal)



    }
    checkAdvanceAmt(advanceAmount: number, displayableGrandTotal: number) {
        if (advanceAmount > displayableGrandTotal) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "You cant pay more than grand total"
            // this.invoiceForm.controls['advanceAmount'].patchValue("");
            return true;
        }
        return false;
    }

    priceCheck(tranItems: any[]): string {

        let textToBeAppend: string = "";
        let returnResult: boolean = false;

        tranItems.forEach(item => {

            ////console.log(item.materialId);
            ////console.log(item.price);

            let material: Material = this.materials.find(materialElement => materialElement.id === item.materialId);

            if(material)
            {
                ////console.log('material got: ', material);
                ////console.log('item.price: ', item.price);
                ////console.log('material.buyingPrice: ', material.buyingPrice);
                if (item.price < material.buyingPrice) {
                    textToBeAppend += material.name +" with buying price: "+material.buyingPrice;
                    ////console.log("strCount value " + textToBeAppend)
                }
            }

        });
        return textToBeAppend;
    }

    enableForm() {
        this.tranForm.enable({ onlySelf: true, emitEvent: false });
        this.disableParty = false;
        this.materialControl.enable({ onlySelf: true, emitEvent: false });
    }

    resetVariables(){
        this.saveStatus = false;
        this.transactionStatus = null;
        this.selectedParty = null;

        this.isRoundOffDisabled = false;
        this.isPartyChangeable = true;
        this.material = null;
        //   this.materialControl.patchValue(null,{emitEvent: false});
        this.uom = null;
        this.tax = null;
        this.headerTax = null;
        this.materialQuantity = 1;
        this.taxCount = 0;
        //this.isNote = false;
    }
    clearFormParent() {

        ////console.log('in clear form')
        

        //// //console.log('got parties: ', this.parties.length);

        //this.getAndPatchTerms();

        this.resetVariables();
        this.getTempTransactionNumber();
        
        this.partyFormControl = this.tranForm.controls['partyName'];
        this.getFilteredParties();
        this.tranForm.controls['partyName'].patchValue('',{emitEvent: false});
        this.materialControl.patchValue('',{emitEvent: false});
        this.isInclusiveTax = this.globalSetting.inclusiveTax === 1 ? true : false;
        this.inclusiveTaxDisable = false;
        this.tranForm.controls['paymentTerms'].patchValue(this.paymentTerms);
        this.tranForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
        this.tranForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
        this.enableForm();

    }


    closeForm() {
        // this._router.navigateByUrl('/')
        this._router.navigate(['/']);
    }


    edit() {
        this.enableForm();
        this.disableParty = true;

    }


   

    headerTaxChange(isDiscountAmountChanged = false) {

        if(this.isInclusiveTax){
            const control = <FormArray>this.itemsControl;//<FormArray>this.tranForm.controls[formArrayName];
            let lprice: number;
            let lquantity: number;
            let lamount: number;
            let idx: number = 0;

            


            //Re clculate price of each item
            while (control.length !== idx) {
                lprice = (control.at(idx).get('price').value / (100.00 + this.headerTax.rate)) * 100;
                lprice = +lprice.toFixed(2);
                lquantity = control.at(idx).get('quantity').value;
                lamount = lprice * lquantity;
                ////console.log("lamount: "+lamount);

                if(!this.isItemLevelDiscount){
                    if(isDiscountAmountChanged){
                        let discountAmount: number = this.tranForm.controls['totalDiscount'].value;
                        console.log("discountAmount: ",discountAmount);
                        if(discountAmount > 0){
                            let lamountx = control.at(idx).get('price').value * control.at(idx).get('quantity').value;
                            ////console.log("lamountx: ", lamountx);
                            let discountPercent: number = (discountAmount * 100)/lamountx;
                            ////console.log("discountPercent: ",discountPercent);
                            discountPercent = +discountPercent.toFixed(2);
                            discountPercent = discountPercent >= 100 ? 0 : discountPercent;
                            control.at(idx).get('discountPercentage').patchValue(discountPercent, { emitEvent: false });
                            lamount = lamount - discountAmount;            
                        }
                    }else{
                        let discountPercentage: number = this.tranForm.controls['discountPercent'].value;
                        ////console.log("discountPercentage: ",discountPercentage);
                        if(discountPercentage > 0){
                            let discountAmount: number = lamount * discountPercentage / 100;
                            console.log("discountAmount: "+discountAmount);
                            discountAmount = +discountAmount.toFixed(2);
                            discountAmount = discountAmount >= lamount ? 0 : discountAmount;
                            control.at(idx).get('discountAmount').patchValue(discountAmount, { emitEvent: false });
                            lamount = lamount - discountAmount;            
                        }
                    }
                    
                }
          

                ////console.log("price, quantity, amont: "+lprice+":"+lquantity+":"+lamount);
                ////console.log("price, quantity, amont: ", lprice, lquantity, lamount);
                // control.at(idx).get('price').patchValue(lprice);
                control.at(idx).get('quantity').patchValue(lquantity, { emitEvent: false });
                control.at(idx).get('amount').patchValue(lamount, { emitEvent: false });
                control.at(idx).get('amountAfterDiscount').patchValue(lamount, { emitEvent: false });

                idx++;
            }

            let tItems: TransactionItem[] = [];
            
            this.itemsIn = this.itemsControl.value;
            this.itemsIn.forEach(item => {
                tItems.push(this.transactionItemService.mapItem(item, null));
            })

            this.transactionSummaryService.patchAmounts(tItems, this.tranForm, this.isItemLevelTax, this.isIgst, this.headerTax, this.isInclusiveTax);
            
        }
        
        let totalTaxableAmount: number = this.tranForm.controls['totalTaxableAmount'].value;
        ////console.log('headerTaxChange: totalTaxableAmount ', totalTaxableAmount);
        ////console.log("this.headerTax.rate ", this.headerTax)
        if (this.headerTax != null) {
            let totalSgstTax: number = !this.isIgst ? (totalTaxableAmount * (this.headerTax.rate / 100)) / 2 : 0;
            let totalCgstTax: number = !this.isIgst ? (totalTaxableAmount * (this.headerTax.rate / 100)) / 2 : 0;
            let totalIgstTax: number = this.isIgst ? totalTaxableAmount * (this.headerTax.rate / 100) : 0;

            ////console.log(totalSgstTax, totalCgstTax, totalIgstTax)
            totalSgstTax = +totalSgstTax.toFixed(2);
            totalCgstTax = +totalCgstTax.toFixed(2);
            totalIgstTax = +totalIgstTax.toFixed(2);
            let grandTotal: number = +totalTaxableAmount + +totalSgstTax + +totalCgstTax + +totalIgstTax;
            ////console.log("grandTotal: "+grandTotal);
            ////console.log("grandTotal: ",grandTotal);
            grandTotal = +grandTotal.toFixed(2);

            this.tranForm.controls['sgstTaxAmount'].patchValue(totalSgstTax);
            this.tranForm.controls['cgstTaxAmount'].patchValue(totalCgstTax);
            this.tranForm.controls['igstTaxAmount'].patchValue(totalIgstTax);
            this.tranForm.controls['grandTotal'].patchValue(grandTotal);
            this.tranForm.controls['sgstTaxRate'].patchValue(!this.isIgst ? this.headerTax.rate/2 : 0);
            this.tranForm.controls['cgstTaxRate'].patchValue(!this.isIgst ? this.headerTax.rate/2 : 0);
            this.tranForm.controls['igstTaxRate'].patchValue(this.isIgst?this.headerTax.rate :0);
           
            this.grandTotalOriginal = grandTotal;
            this.originalGrandTotal = grandTotal;
            this.calculateGrandtotal();
        }
        let partyCountryId :number =this.tranForm.controls['partyCountryId'].value;
      //  let currencyId :number = this.tranForm.controls['partyCurrencyId'].value;
      //  console.log("partyCountryId = "+partyCountryId);
        if (this.isImport!=1 && this.transactionType.name!=AppSettings.CREDIT_NOTE) {
         
            console.log("import")
            let grandTotal: number = +totalTaxableAmount ;
            grandTotal = +grandTotal.toFixed(2);
            this.tranForm.controls['grandTotal'].patchValue(grandTotal);
            this.grandTotalOriginal = grandTotal;
            this.originalGrandTotal = grandTotal;
            this.calculateGrandtotal();
         }
        if(this.transactionType.name===AppSettings.CUSTOMER_INVOICE||this.transactionType.name===AppSettings.PROFORMA_INVOICE||this.transactionType.name===AppSettings.PURCHASE_INVOICE||this.transactionType.name===AppSettings.CREDIT_NOTE||this.transactionType.name===AppSettings.DEBIT_NOTE||this.transactionType.name===AppSettings.INCOMING_JOBWORK_INVOICE||this.transactionType.name===AppSettings.OUTGOING_JOBWORK_INVOICE){
            this.tranForm.controls['tcsPercentage'].patchValue(0);
           this.tranForm.controls['tcsAmount'].patchValue(0);
           }
    }

    headerDiscountAmtChange() {
        let discountAmount: number = this.tranForm.controls['totalDiscount'].value;

        let totalAmount: number = this.isInclusiveTax ? this.tranForm.controls['subTotalAmount'].value :  this.tranForm.controls['subTotalAmount'].value;

        console.log("discountAmount, totalAmount ", discountAmount, totalAmount);

        let discountedPer: number = (100 * discountAmount) / totalAmount;
        discountedPer = +discountedPer.toFixed(2);
        this.tranForm.controls['discountPercent'].patchValue(discountedPer, {emitEvent:false});

        //let totalTaxableAmount: number = this.invoiceForm.controls['totalTaxableAmount'].value;
        this.tranForm.controls['totalTaxableAmount'].patchValue((this.totalTaxableAmountOriginal ? this.totalTaxableAmountOriginal : totalAmount) - discountAmount, {emitEvent:false});
        this.headerTaxChange(true);
    }

    headerDiscountPerChange() {
        let discountPercentage: number = this.tranForm.controls['discountPercent'].value;
        let totalAmount: number = this.tranForm.controls['subTotalAmount'].value;

        let discountedAmount: number = Math.round(totalAmount * discountPercentage / 100);
    
        console.log("discountedAmount: ", discountedAmount);
        this.tranForm.controls['totalDiscount'].patchValue(discountedAmount.toFixed(2), {emitEvent:false});

        ////console.log("this.totalTaxableAmountOriginal: ", this.totalTaxableAmountOriginal);
        ////console.log("totalAmount: ", totalAmount);
        let totalTaxableAmount = (this.totalTaxableAmountOriginal ? this.totalTaxableAmountOriginal : totalAmount) - discountedAmount;
        ////console.log("totalTaxableAmount: ",totalTaxableAmount);
        this.tranForm.controls['totalTaxableAmount'].patchValue(totalTaxableAmount, {emitEvent:false});
        if(this.isImport!=1 && this.transactionType.name===AppSettings.CUSTOMER_INVOICE||this.transactionType.name===AppSettings.PROFORMA_INVOICE ||this.transactionType.name===AppSettings.CREDIT_NOTE)
        {  // console.log("totalTaxableAmount isImport = "+totalTaxableAmount)
            this.tranForm.controls['totalAmountCurrency'].patchValue(totalTaxableAmount, {emitEvent:false});
            this.onCurrencyRateChange();
        }

        if(this.transactionType.name===AppSettings.CUSTOMER_INVOICE||this.transactionType.name===AppSettings.PROFORMA_INVOICE||this.transactionType.name===AppSettings.PURCHASE_INVOICE||this.transactionType.name===AppSettings.CREDIT_NOTE||this.transactionType.name===AppSettings.DEBIT_NOTE||this.transactionType.name===AppSettings.INCOMING_JOBWORK_INVOICE||this.transactionType.name===AppSettings.OUTGOING_JOBWORK_INVOICE){
            this.tranForm.controls['tcsPercentage'].patchValue(0);
           this.tranForm.controls['tcsAmount'].patchValue(0);
           }
        this.headerTaxChange();
    }


    addNewParty() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        let partyType: number;
        if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name == AppSettings.PROFORMA_INVOICE || this.transactionType.name === AppSettings.CUSTOMER_PO || this.transactionType.name === AppSettings.CUSTOMER_QUOTATION || this.transactionType.name === AppSettings.CUSTOMER_PO || this.transactionType.name === AppSettings.CUSTOMER_QUOTATION || this.transactionType.name === AppSettings.CREDIT_NOTE) {
            partyType = 1;//'Customer PO';
        }
        else {
            partyType = 2; //'Supplier PO';
        }
        dialogConfig.data = {
            partyTypeId: partyType

        };


        const dialogRef = this.dialog.open(PartyPopupComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {
                if (data) {
                    ////console.log("Dialog output:", data)
                    let partyWithPriceList: PartyWithPriceList = {
                        partyDTO: data,
                        materialPriceListDTOList: [],
                        materialPriceListDeletedIds: [],
                        partyBankMapDTOList: [],
                        partyBankMapDeletedIds: [],
                    };
                    this.partyService.savePartyWithPriceList(partyWithPriceList).subscribe(response => {
                        ////console.log("Dialog response:", response)
                        //
                        let status = response.partyDTO.name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                        this.alertService.success(status);
                        // this.parties.push(response.partyDTO);
                        this.tranForm.controls['partyName'].patchValue(response.partyDTO);
                        this.tranForm.controls['billToAddress'].patchValue(response.partyDTO.address);
                        this.tranForm.controls['shipToAddress'].patchValue(response.partyDTO.billAddress);
                        this.tranForm.controls['address'].patchValue(response.partyDTO.address)
                        this.tranForm.controls['partyId'].patchValue(response.partyDTO.id)
                        this.tranForm.controls['gstNumber'].patchValue(response.partyDTO.gstNumber)
                        this.selectedParty = response.partyDTO; //this.parties.find(p => p.id === response.partyDTO.id);
                        ////console.log("this.selectedParty", this.selectedParty)
                        this.isIgst = this.getIsIgst(this.selectedParty);
                        this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                        this.getFiltredMaterial();

                    },
                    error => {
                        ////console.log("Error ", error, error.exception);
                        
                        if(error instanceof HttpErrorResponse){
                            var re = /DataIntegrityViolationException/gi;
                            if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
                            this.transactionStatus = "Duplicate Entry!! Cannot save";
                        }
                        this.alertService.danger(this.transactionStatus);
                        this.saveStatus = false;
                        this.spinStart = false;
                    });
                }
            });

    }

    addNewMaterial() {
        ////console.log('addNewMaterial() clicked');
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        let materialTypeId: number = this.isJW ? 4 : null;

        dialogConfig.data = {
            supplyTypeId: this.supplyTypeControl.value,
            materialTypeId: materialTypeId
        };

       // const dialogRef = this.dialog.open(MaterialPopupComponent, dialogConfig);
       ////console.log("this.supplyTypeControl",this.supplyTypeControl)
       const dialogRef = this.dialog.open(MaterialPopupComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {
                if (data) {
                    data.companyId = 1; //TO DO
                   // data.supplyTypeId = 1;
                  // data.supplyTypeId =  this.supplyTypeControl.value;
                    ////console.log("Dialog output:", data)
                    
                    if(data.openingStock===null && data.stock===null && data.id===null){
                        data.openingStock=0;
                        data.stock=0;
                      }
                    this.materialService.save(data).subscribe(response => {
                        ////console.log("Dialog response:", response)

                        let status = response.name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                        this.alertService.success(status);
                        this.materials.push(response);
                        this.material = response;
                       this.materialControl.patchValue(this.material, { emitEvent: false });
                        this.onMaterialSelect();

                    },
                    error => {
                        ////console.log("Error ", error, error.exception);
                        
                        if(error instanceof HttpErrorResponse){
                            
                            // var re = /DataIntegrityViolationException/gi;
                            // if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
                            //    this.transactionStatus = "Duplicate Entry!! Cannot save";

                            var re = "ConstraintViolationException";
                            if(error.error && error.error.message.includes(re)){
                                this.transactionStatus = "Duplicate Entry!! Cannot save";
                            }
                        }
                        this.alertService.danger(this.transactionStatus);
                        this.saveStatus = false;
                        this.spinStart = false;
                    });
                }
            });

    }

    
    checkStockQuantity(materialquantity: number, material: Material): Observable<boolean> {
        
        ////console.log("this.supplyTypeControl.value", this.supplyTypeControl.value)
        ////console.log("this.isStockCheckRequired ", this.isStockCheckRequired )
        ////console.log("this.isCust  ", this.isCust )
        let newQuantity: number;

            if ((this.isCust ||this.transactionType.name ===AppSettings.DEBIT_NOTE)&& this.isStockCheckRequired && this.supplyTypeControl.value === 1 && !this.isJW && this.transactionType.name!=AppSettings.CREDIT_NOTE) {
               
                  newQuantity = material.stock - materialquantity
                if (material.stock < materialquantity) {
                    this.dialogRef = this.dialog.open(ConfirmationDialog, {
                        disableClose: false
                    });
                    ////console.log("outside result", materialquantity, this.material, this.buyOrSell)
                    this.dialogRef.componentInstance.confirmMessage = AppSettings.STOCK_CONFIRMATION_MESSAGE
                    return this.dialogRef.afterClosed();
    
                }

                if(material.minimumStock > material.stock){                    
                    this.dialogRef = this.dialog.open(ConfirmationDialog, {
                        disableClose: false
                    });
                    this.dialogRef.componentInstance.confirmMessage = AppSettings.CURRENT_STOCK_BELOW_MINIMUM_STOCK_CONFIRMATION_MESSAGE
                    return this.dialogRef.afterClosed();
                }
    
                ////console.log("materialTypeName :",material.materialTypeName);
    
                    if(material.minimumStock > newQuantity){                    
                        this.dialogRef = this.dialog.open(ConfirmationDialog, {
                            disableClose: false
                        });
                        this.dialogRef.componentInstance.confirmMessage = AppSettings.MINIMUM_STOCK_CONFIRMATION_MESSAGE
                        return this.dialogRef.afterClosed();
                    }
                
    
                ////console.log("3")
                return observableOf(true);
            }
        

        ////console.log(" 4")
        return observableOf(true);
    }
     

    onQuantityChange(event: QuantityChangeEvent, itemArrayName: string) {
        ////console.log("in chnage", event);
        const control = <FormArray>this.tranForm.controls[itemArrayName];

        const rowControl = control.at(event.index);
        ////console.log("in chnage rowControl", rowControl);
        let material: Material = this.materials.find(mat => mat.id === event.materialId);
        this.checkStockQuantity(event.quantity, material).subscribe(val => {
            ////console.log("val", val)
            let originlQty = event.quantity
            ////console.log("originlQty", originlQty)
            if (!val) {
                rowControl.get('quantity').patchValue(0);
            }
        });


    }

    whenNoDataFound(noDataFound: boolean) {
        if (noDataFound) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE
        }
    }

    onchangeRoundoff() {
        ////console.log('grandTotal.', this.tranForm.controls['grandTotal'].value);
        let grandTotal = 0;
        grandTotal = this.tranForm.controls['grandTotal'].value;


        ////console.log("roundoff", this.isRoundOffAmount);
        let roundOffAmount: number = 0;
        let roundOffgrandTotal: number;

        if (this.isRoundOffAmount === true) {
            roundOffgrandTotal = +(Math.round(grandTotal) - grandTotal).toFixed(2);//Math.round(Math.round(grandTotal) - grandTotal);
            ////console.log("grandtotal " + roundOffgrandTotal)
            grandTotal = Math.round(grandTotal);
            ////console.log("grandtotal " + grandTotal)
            this.tranForm.controls['roundOffAmount'].patchValue(roundOffgrandTotal);
            this.tranForm.controls['grandTotal'].patchValue(grandTotal);
        }
        else {

            roundOffAmount = 0.00;
            this.tranForm.controls['roundOffAmount'].patchValue(roundOffAmount);
            this.tranForm.controls['grandTotal'].patchValue(this.grandTotalOriginal);
        }
        this.grandTotalWithoutReverse = this.tranForm.controls['grandTotal'].value;
    }

    showReference(){
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            tranTypeId: this.transactionType.id,
            tranId: this.tranForm.get('id').value

        };

        dialogConfig.height='500px';
        dialogConfig.width='600px';

        
        const dialogRef = this.dialog.open(ReferenceLinksComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => {
                ////console.log('dialog closed: ', data)
                if (data) {
                    ////console.log('navigating')
                    this._router.navigate([data.url, {id: data.id}]);
                    //this._router.navigate(['/']);                    
                }
            });
        
    }

    protected getTempTransactionNumber(){
        this.transactionTypeService.getTransactionNumber(this.transactionType.name)
        .pipe(
            takeUntil(this.onDestroy)
        )
        .subscribe(response => {
            this.tempTransactionNumber = response.responseString;
        })
    }

    print() {

        // let l_printCopy: number = 1;
        // this.printRef = this.dialog.open(PrintDialog, {
        //     // width: '250px',
        //     data: { printCopy: l_printCopy }
        // });

        //// //console.log("print method printRef...........", this.printRef );
        //// //console.log("print method...........", this.tranForm.value);
        //// //console.log("print method transactionType...........", this.transactionType);
        //// //console.log("print method globalSetting...........", this.globalSetting);

        
        // let printDataModel = this.tranForm.value;

        // this.printRef.afterClosed().subscribe(result => {
        ////     //console.log("result: ", result);
        //     if (result) {
        //         l_printCopy = +result;
        //         this.printWrapperService.print(this.transactionType, printDataModel, l_printCopy, this.globalSetting);
        //     }
        // })

        
        ////console.log("print method printRef...........", this.printRef );
        ////console.log("print method...........", this.tranForm.value);
        ////console.log("print method transactionType...........", this.transactionType);
        ////console.log("print method globalSetting...........", this.globalSetting);


        let l_printCopy: number = 1;
        let partyName: string = this.tranForm.get('partyName').value
        this.tranForm.controls['partyName'].patchValue(this.displayParty);
        this.printRef = this.dialog.open(PrintDialogContainer, {
            // width: '250px',
            data: { 
                transactionType: this.transactionType, 
                printDataModel: this.tranForm.value, 
                printCopy: l_printCopy, 
                globalSetting: this.globalSetting,
                numberRangeConfiguration: this.numberRangeConfiguration,
                isItemLevelTax: this.isItemLevelTax,
                //printHeaderText: this.numberRangeConfiguration.printHeaderText
            }
        });

this.tranForm.controls['partyName'].patchValue(partyName);
    }

    onTncChange(){
        this.termsAndConditions = this.tnc.termsAndCondition;
        this.paymentTerms = this.tnc.paymentTerms;
        this.deliveryTerms = this.tnc.deliveryTerms;

        this.tranForm.controls['paymentTerms'].patchValue(this.paymentTerms);
        this.tranForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
        this.tranForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
    }

    inclusiveTaxChange(){
        ////console.log("this.isInclusiveTax ",this.isInclusiveTax );
    }

    validateTnCNewLines(termsAndConditions: string): boolean{
        ////console.log("termsAndConditions..........................", termsAndConditions);
        if(termsAndConditions!=null){
            let tncArray = termsAndConditions.split("\n");
            ////console.log("tncArray.length: ", tncArray.length);
    
            if(tncArray.length >= AppSettings.TNC_POINTS_LIMIT){
                this.alertService.danger(AppSettings.TNC_POINTS_ERROR_MESSAGE); 
                return false;
            }
        }

        return true;
    }

    addItemWrapper(materialquantity: number, inArray: FormArray) {

        
        // console.log("tax..................", this.tax);
        // console.log("tax..................", this.taxCount);
        //// //console.log("tax..................", this.gstrate);
        //// //console.log("tax..................", this.globalSetting.itemLevelTax);

        
        if (this.taxCount > 0 && this.tax && this.gstrate !== this.tax.rate && (!this.isItemLevelTax)) {
            // if (confirm("You are trying to add materials with different GST rates. Previously added material/s GST rate will be over-ridden. Are you sure you want to go ahead? ")) {
            ////     //console.log("material added");
            // }
            // else {
            //     return false;
            // }
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });
            this.dialogRef.componentInstance.confirmMessage = "You are trying to add materials with different GST rates. Previously added material/s GST rate will be over-ridden. Are you sure you want to go ahead? ";
            this.dialogRef.afterClosed().subscribe(result => {

                if (!result) {
                    return;
                }else{
                    this.gstrate = this.tax ? this.tax.rate : 0;
                    this.addTranItem(materialquantity, inArray, this.tranForm.get('partyId'));
                    
                    this.taxCount++;
                }

            });
        }else{
            this.gstrate = this.tax ? this.tax.rate : 0;
            this.addTranItem(materialquantity, inArray, this.tranForm.get('partyId'));
            
            this.taxCount++;
        }
        
        
    }
    
    helpVideos() {
        
        let status: string = "";
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.id =this.activityId.toString();
     
        
        const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {

            });



        
    }
}
