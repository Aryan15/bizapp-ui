import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { take, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { QuantityChangeEvent } from '../../data-model/misc-model';
import { Party } from '../../data-model/party-model';
import { QuotationHeader, QuotationWrapper } from '../../data-model/quotation-model';
import { TransactionItem } from '../../data-model/transaction-item';
import { TransactionType } from '../../data-model/transaction-type';
import { CompanyService } from '../../services/company.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { MaterialService } from '../../services/material.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PrintWrapperService } from '../../services/print-warpper.service';
import { QuotationService } from '../../services/quotation.service';
import { TaxService } from '../../services/tax.service';
import { QuotationSecondReportService } from '../../services/template2/quotation-report.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransCommService } from '../../services/trans-comm.service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { TransactionSummaryService } from '../../services/transaction-summary.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { UtilityService } from '../../utils/utility-service';
import { TransactionParentComponent } from '../transaction-parent/transaction-parent.component';
import { LocationService } from '../../services/location.service';
import { ConnectionService } from '../../services/connection.service';
import { Currency } from '../../data-model/currency-model';
import { CurrencyService } from '../../services/currency.service';
import {CurrencyPipe} from '@angular/common';
@Component({
    selector: 'app-new-quotation',
    templateUrl: './new-quotation.component.html',
    styleUrls: ['./new-quotation.component.scss'],
})
export class NewQuotationComponent extends TransactionParentComponent implements OnInit {
    @ViewChild('quotationNumber', {static: true}) quotationNumberElement: ElementRef;

    item_length : number = AppSettings.length_limit;
    //quotationForm: FormGroup;
    quotationHeader: QuotationHeader;



    // subheading : String = "this page is used to record Information about ";

    itemOfNewQuotation: string;
    recentQuotationLoadCounter: number = 0;
    recentQuotationItem: string = AppSettings.TXN_ITEM_OF_RECENT_QUOTATION; //"Recent Quotation Item";
    tempQuotationNumber: string;
    transactionTypes :TransactionType[]=[];
    
    fieldValidatorLabels = new Map<string, string>();
    isCreatePOenabled : boolean = false;
    //whileCreatePofrmQt: boolean = false;
    showRecent: boolean = false;
    isPartyChangeable: boolean = true;
    isheaderTaxDisable:boolean=false;
    currencySymbol:any;
    currencyName:any;
    currencyRate:any;
    
    public currencys: Currency[] = [];
    // currencyName:any;
       currencyFraction:string;
      currencyDecimal:string;
    
    showTheCreatedBy=false;
   displayPartyName:string="";
 taxNumberText:string ="GST Number";
    constructor(
        private fb: FormBuilder,
        public route_qo: ActivatedRoute,
        private _router_in: Router,
        
        // title : String = "",
        private transactionTypeService_qo: TransactionTypeService,
        private numberRangeConfigService_qo: NumberRangeConfigService,
        private termsAndConditionsService_qo: TermsAndConditionsService,
        private utilityService_qo: UtilityService,
        private materialService_qo: MaterialService,
        private partyService_qo: PartyService,
        private userService_qo: UserService,
        private transactionTypeService_in: TransactionTypeService,
        private globalSettingService_qo: GlobalSettingService,
        private uomService_qo: UomService,
        private taxService_qo: TaxService,
        private alertService_qo: AlertService,
        private quotationService_qo: QuotationService,
        private financialYearService_qo: FinancialYearService,
        private transactionItemService_qo: TransactionItemService,
        private transactionSummaryService_qo: TransactionSummaryService,
        private dialog_in: MatDialog,
        private companyService_in: CompanyService,
        private quotationSecondReportService: QuotationSecondReportService,
        private printWrapperService_in: PrintWrapperService,
        private transCommService: TransCommService,
        private locationService_qot: LocationService,
       // private currencyService: CurrencyService,
        protected connectionService: ConnectionService
        
        
        //private quotationService: QuotationService,
    ) {

        super(
            route_qo,
            _router_in,
            transactionTypeService_qo,
            numberRangeConfigService_qo,
            termsAndConditionsService_qo,
            utilityService_qo,
            materialService_qo,
            partyService_qo,
            userService_qo,
            globalSettingService_qo,
            uomService_qo,
            taxService_qo,
            financialYearService_qo,
            quotationService_qo,
            alertService_qo,
            transactionItemService_qo,
            transactionSummaryService_qo,
            dialog_in,
            printWrapperService_in,
            companyService_in,
            locationService_qot,
            
            connectionService 
        );
    }

    ngOnInit() {

        // //console.log("transactionType..........!!!!!!!!!",this.transactionType);
        

        this.initParent();

        //setTimeout(() => {//console.log('in child: ', this.transactionType)}, 1000);
        //console.log("before init complete...")
        this.initComplete.subscribe(response => {
            //console.log("in init complete...", response);
            if (response) {

                this.initForm();
                this.itemsControl = this.tranForm.controls['quotationItems'];
                this.partyFormControl = this.tranForm.controls['partyName'];
                this.getFilteredParties();
                this.handleChangesParent(this.itemOfNewQuotation, this.quotationHeader ? this.quotationHeader.taxId : null);
                //this.getFilteredParties(this.quotationForm.controls['partyName']);

                this.tranForm.controls['paymentTerms'].patchValue(this.paymentTerms);
                this.tranForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
                this.tranForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
              
                if (this.loadFromId && this.transactionId) {
                    this.quotationService_qo.getQuotation(this.transactionId).subscribe(response => {
                        if (response) {
                            this.recentQuotationGet(response);
                        }
                    })
                }


            }
            else{
                this.transCommService.quotationData
                .pipe(
                    take(1)
                    ,takeUntil(this.onDestroy)
                )
                .subscribe(response => {
                    //console.log('response ', response);
                    if(response){
                        this.recentQuotationGet(response);                            
                        this.transCommService.updateQuotationData(null);
                        this.transCommService.completeQuotationData();  
                    }
                });

                
            }
        })
        //this.calculateGrandtotal();  

    }



    private initForm() {

        let data: QuotationHeader = {
            id: null,
            amount: null,
            quotationNumber: null,
            quotationId: null,
            quotationDate: this.defaultDate,
            enquiryNumber: null,
            enquiryDate: null,
            partyId: null,
            subTotalAmount: null,
            totalDiscount: null,
            discountPercent: null,
            totalTaxableAmount: null,
            taxId: null,
            roundOffAmount: null,
            grandTotal: null,
            statusId: null,
            financialYearId: null,
            companyId: null,
            paymentTerms: null,
            deliveryTerms: null,
            termsAndConditions: null,
            taxAmount: null,
            kindAttention: null,
            quotationSubject: null,
            quotationStatus: null,
            isPaymentChecked: null,
            sgstTaxRate: null,
            sgstTaxAmount: null,
            cgstTaxRate: null,
            cgstTaxAmount: null,
            igstTaxRate: null,
            igstTaxAmount: null,
            quotationTypeId: null,
            quotationItems: null,
            statusName: null,
            partyName: null,
            shipToAddress: null,
            address: '',
            gstNumber: null,
            remarks :null,
            inclusiveTax: null,
            // ................
            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile : null,
            companySecondaryMobile : null,
            companyContactPersonNumber : null,
            companyContactPersonName : null,
            companyPrimaryTelephone : null,
            companySecondaryTelephone : null,
            companyWebsite : null,
            companyEmail : null,
            companyFaxNumber : null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber : null,
            companyPanNumber : null,
            companyPanDate : null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            // partyName: null,
            partyContactPersonNumber: null,
            partyPinCode: null,
            partyAreaId: null,
            partyCityId: null,
            partyStateId: null,
            partyCountryId: null,
            partyCurrencyId:null,
            partyPrimaryTelephone: null,
            partySecondaryTelephone: null,
            partyPrimaryMobile: null,
            partySecondaryMobile: null,
            partyEmail: null,
            partyWebsite: null,
            partyContactPersonName: null,
            partyBillToAddress: null,
            partyShipAddress: null,
            partyDueDaysLimit: null,
            partyGstRegistrationTypeId: null,
            partyGstNumber: null,
           
            partyPanNumber: null,
            isIgst: null,
            partyCode:null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null,
            currencyName:null,

        }

        this.tranForm = this.toFormGroup(data, this.itemOfNewQuotation);

    }


    private toFormGroup(data: QuotationHeader, mapItemCategory: string): FormGroup {

        const itemArr = new FormArray([]);
        if (data.quotationItems) {
            data.quotationItems.forEach(item => {
                let tItem: TransactionItem = this.transactionItemService_qo.mapItem(item, mapItemCategory);

                itemArr.push(this.transactionItemService_qo.makeItem(tItem));
            });

        }

        const formGroup = this.fb.group({
            id: [data.id],
            amount: [data.amount],
            quotationNumber: [data.quotationNumber],
            quotationId: [data.quotationId],
            //quotationDate: [this.dateAdapter., [Validators.required]],
            quotationDate: [this.datePipe.transform(data.quotationDate, AppSettings.DATE_FORMAT), [Validators.required]],
            enquiryNumber: [data.enquiryNumber],
            enquiryDate: [this.datePipe.transform(data.enquiryDate, AppSettings.DATE_FORMAT)],
            // partyId: [data.partyId, [Validators.required]],
            partyId: [data.partyId, [Validators.required]],
            subTotalAmount: [data.subTotalAmount],
            totalDiscount: [data.totalDiscount ? data.totalDiscount.toFixed(2) : data.totalDiscount],
            discountPercent: [data.discountPercent, [Validators.max(100)]],
            totalTaxableAmount: [data.totalTaxableAmount ? data.totalTaxableAmount.toFixed(2) : data.totalTaxableAmount],
            taxId: [data.taxId],
            roundOffAmount: [data.roundOffAmount],
            grandTotal: [data.grandTotal, [Validators.required, Validators.min(0.1)]],
            statusId: [data.statusId],
            financialYearId: [data.financialYearId],
            companyId: [data.companyId],
            paymentTerms: [data.paymentTerms],
            deliveryTerms: [data.deliveryTerms],
            termsAndConditions: [data.termsAndConditions],
            taxAmount: [data.taxAmount],
            kindAttention: [data.kindAttention],
            quotationSubject: [data.quotationSubject],
            quotationStatus: [data.quotationStatus],
            isPaymentChecked: [data.isPaymentChecked],
            sgstTaxRate: [data.sgstTaxRate],
            sgstTaxAmount: [data.sgstTaxAmount],
            cgstTaxRate: [data.cgstTaxRate],
            cgstTaxAmount: [data.cgstTaxAmount],
            igstTaxRate: [data.igstTaxRate],
            igstTaxAmount: [data.igstTaxAmount],
            quotationItems: itemArr,
            statusName: [data.statusName],
            partyName: [data.partyName, [Validators.required]],
            address: [data.address],
            gstNumber: [data.gstNumber],
            remarks :[data.remarks],
            inclusiveTax: [data.inclusiveTax],
            billToAddress: [data.address],
            shipToAddress: [data.address],
            // .............................
            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile : [data.companyPrimaryMobile],
            companySecondaryMobile : [data.companySecondaryMobile],
            companyContactPersonNumber : [data.companyContactPersonNumber],
            companyContactPersonName : [data.companyContactPersonName],
            companyPrimaryTelephone : [data.companyPrimaryTelephone],
            companySecondaryTelephone : [data.companySecondaryTelephone],
            companyWebsite : [data.companyWebsite],
            companyEmail : [data.companyEmail],
            companyFaxNumber : [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber : [data.companyGstNumber],
            companyPanNumber : [data.companyPanNumber],
            companyPanDate : [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
            // partyName: [data.companyName],
            partyContactPersonNumber: [data.partyContactPersonNumber],
            partyPinCode: [data.partyPinCode],
            partyAreaId: [data.partyAreaId],
            partyCityId: [data.partyCityId],
            partyStateId: [data.partyStateId],
            partyCountryId: [data.partyCountryId],
            partyPrimaryTelephone: [data.partyPrimaryTelephone],
            partySecondaryTelephone: [data.partySecondaryTelephone],
            partyPrimaryMobile: [data.partyPrimaryMobile],
            partySecondaryMobile: [data.partySecondaryMobile],
            partyEmail: [data.partyEmail],
            partyWebsite: [data.partyWebsite],
            partyContactPersonName: [data.partyContactPersonName],
            partyBillToAddress: [data.partyBillToAddress],
            partyShipAddress: [data.partyShipAddress],
            partyDueDaysLimit: [data.partyDueDaysLimit],
            partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
            partyGstNumber: [data.partyGstNumber],
            partyPanNumber: [data.partyPanNumber],
            isIgst: [data.isIgst],
            partyCode:[data.partyCode],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
            updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],
            currencyName:[data.currencyName],
            partyCurrencyId:[data.partyCurrencyId],
        });

        formGroup.get('quotationItems').setValidators(Validators.required);
        
        this.fieldValidatorLabels.set("partyName", "Party Name");
        this.fieldValidatorLabels.set("quotationDate", "Quotation Date");
        this.fieldValidatorLabels.set("quotationItems", "Quotation Items");
        this.fieldValidatorLabels.set("materialId", "Material");
        this.fieldValidatorLabels.set("price", "Price");
        this.fieldValidatorLabels.set("quantity", "Quantity");
        this.fieldValidatorLabels.set("amount", "Amount");




        return formGroup;
    }

    // checkQtnNumAvailability() {
    //     let quotationNumber: string = this.tranForm.get('quotationNumber').value
    //     if (quotationNumber && quotationNumber.length > 0 && this.transactionType.name === AppSettings.CUSTOMER_QUOTATION) {
      
    //       this.quotationService_qo.checkQtnNumAvailability(quotationNumber, this.transactionType.id).subscribe(response => {
 
    //         //console.log("is invoiceNumber avialable? ", response);
    //         if (response.responseStatus === 1) {
    //           //console.log("Yes");
    //         } else {
    //           //console.log("No");
    //           this.alertService.danger( "Invoice Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //             this.tranForm.controls['quotationNumber'].patchValue('');
    //            this.quotationNumberElement.nativeElement.focus();
 
    //         }
 
    //       });
    //     }
    //   }


    onPartyChange(_event: any, party: Party) {
        console.log("---------------------------")
        if (_event.isUserInput) {
            super.onPartyChange(_event, party, this.tranForm.controls['partyId'], <FormArray>this.tranForm.controls['quotationItems']);
            if (this.selectedParty.id !== undefined) {
                this.tranForm.controls['address'].patchValue(this.selectedParty.address);
                // this.tranForm.controls['billToAddress'].patchValue(this.selectedParty.address);
                this.tranForm.controls['gstNumber'].patchValue(this.selectedParty.gstNumber);
                if(this.selectedParty.countryId!=1)
                {
                    this.tranForm.controls['partyCurrencyId'].patchValue(this.selectedParty.partyCurrencyId)
                    this.tranForm.controls['currencyName'].patchValue(this.selectedParty.partyCurrencyName);
                    this.tranForm.controls['partyCountryId'].patchValue(this.selectedParty.countryId);
                    this.taxNumberText="CIN Number";


                }else{
                    this.tranForm.controls['partyCurrencyId'].patchValue(0);
                    this.taxNumberText="GST Number";

                }
              /*   if(this.selectedParty.partyCurrencyId!=null)
                {

                
                this.partyService.getCurrency(this.selectedParty.partyCurrencyId).subscribe(currency => {
                       console.log("on party funtion and setting currency Name"+currency.currencyName)                   
                    this.tranForm.controls['currencyName'].patchValue(currency.currencyName);
                   // this.tranForm.controls['currencyName'].patchValue(this.selectedParty.partyCurrencyName);


                });
            } */
                
              
            }
        }
       this.taxDisablecChanges()
    }
    taxDisablecChanges(){
          
      
        console.log("globalsetting = "+this.globalSetting.itemLevelTax)
            
       
      
        if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
            {
                this.isheaderTaxDisable=true;//headerlevel
                
            }
            if(this.selectedParty.countryId==1&& this.globalSetting.itemLevelTax==0)
            { 
               this.isheaderTaxDisable=false;
            }
         
            if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
             {
               
               this.isItemLevelTax=false;
               this.isheaderTaxDisable=true
               
             }
             if(this.selectedParty.countryId==1 && this.globalSetting.itemLevelTax==1)
             {  
                this.isItemLevelTax=true;
             }
           

    }
    addQuotationItem(materialquantity: number) {
        //this.addTranItem(materialquantity, <FormArray>this.tranForm.controls['quotationItems'], this.tranForm.get('partyId'));
        this.addItemWrapper(materialquantity, <FormArray>this.tranForm.controls['quotationItems']);
    }

    handleChanges() {
        //this.handleChanges();
        this.handleChangesParent(this.itemOfNewQuotation, this.quotationHeader ? this.quotationHeader.taxId : null);
    }


    onSubmit(model: QuotationHeader) {
        model.partyName=this.displayPartyName;
       
        if(this.numberRangeConfiguration.id===null){
           
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
            return;
        }
        let quotationNumber: string = this.tranForm.get('quotationNumber').value
        //console.log("Invoce Number : ",model.termsAndConditions);
        
        if(model.quotationNumber==null && !this.isAutoNumber || model.quotationNumber==""){
            this.alertService.danger( "Please Enter Customer Quotation Number");
            return
        }

        if(!this.validateTnCNewLines(model.termsAndConditions)){
            return;
        }

        //console.log('In onSubmit: ', model);
        //console.log('Grand total: ', model.grandTotal);

        let checkNumAvail: boolean = false;
        //check number avail      
        if(this.isAutoNumber){ //in case of auto number do not check
            checkNumAvail = false;
        }
        else{
            if(model.id != null){ //if we are editing invoice which is not auto number
                if(model.quotationNumber != this.quotationHeader.quotationNumber){ // if invoice number is changed
                    checkNumAvail = true;
                }else{
                    checkNumAvail = false;
                }
            }else{ // if not auto number and creating new invoice, always check
                checkNumAvail = true;
            }
        }

        if(checkNumAvail){
        this.quotationService_qo.checkQtnNumAvailability(quotationNumber, this.transactionType.id).subscribe(response => {
 
            //console.log("is invoiceNumber avialable? ", response);
            if (response.responseStatus === 1) {
                let textToBeAppend: string = this.priceCheck(model.quotationItems);
                if (textToBeAppend.length > 0) {
        
                    this.dialogRef = this.dialog.open(ConfirmationDialog, {
                        disableClose: false
                    });
                    this.dialogRef.componentInstance.confirmMessage = textToBeAppend + AppSettings.PRICE_CONFIRMATION_MESSAGE
                    this.dialogRef.afterClosed().subscribe(result => {
        
                        if (result) {
                            this.save(model);
                        }
        
                    });
                }
                else {
                    this.save(model);
                }
            } 
            else {
              this.alertService.danger( "quotation Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                this.tranForm.controls['quotationNumber'].patchValue('');
               this.quotationNumberElement.nativeElement.focus();
 
            }
 
          });
        }
        else{
            this.save(model);
        }

    }
   

    save(model: QuotationHeader) {
        //console.log("model : ",model);
       
        if(model.id===null){
            model = this.transactionItemService_qo.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
            //console.log("model :",model);   
        }

         if(!this.tranForm.valid){
            const controls = this.tranForm;
            let invalidFieldList: string[] =this.utilityService_qo.findInvalidControlsRecursive(controls);
            //console.log("invalidFieldList ", invalidFieldList)
      
            let invalidFiledLabels: string[] = [];
            
            invalidFieldList.forEach(field => {
              if (this.fieldValidatorLabels.get(field))
                invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })
      
            //console.log("invalidFiledLabels ", invalidFiledLabels)
      
            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE+invalidFiledLabels);
            return false;
      }

        model.partyId = this.selectedParty.id; //override id to replace whole cusotmer object
        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id;
        //model.partyId = this.selectedCustomer.id;
        
        model.taxId = this.headerTax ? this.headerTax.id : model.quotationItems[0].taxId; // hack


        if (this.headerTax == null && !this.isItemLevelTax && model.partyCountryId==1) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "Please select tax";
            return;
        }
        this.spinStart = true;


        model.taxAmount = model.cgstTaxAmount + model.sgstTaxAmount + model.igstTaxAmount;
       
        model.quotationTypeId = this.transactionType.id;
        model.inclusiveTax = this.isInclusiveTax ? 1 : 0;
        //Assign Serial numbers to the items
        model.quotationItems.forEach((item, index) => {
            item.slNo = index+1;
        })

        //console.log('Before save: ', model);

        //console.log("itemToBeRemove " + this.itemToBeRemove)

        // if (this.itemToBeRemove.length > 0) {
        //     //console.log(" in itemToBeRemove " + this.itemToBeRemove)
        //     this.itemToBeRemove.forEach(itemId =>
        //         this.quotationService.deleteItem(itemId).subscribe(response => {
        //             //console.log("item id before save on deltion " + itemId);
        //             if (response.responseStatus) {
        //                 //console.log("Item deleted in DB: " + itemId);
        //                 // (<FormArray>this.tranForm.get('quotationItems')).removeAt(idx);
        //             } else {
        //                 console.error("Cannot delete: ", response.responseString);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }
        //             return false;

        //         },
        //             error => {
        //                 console.error("Cannot delete: ", error);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }))
        // }
        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        }
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }
            
        let quotationWrapper: QuotationWrapper = { itemToBeRemove: this.itemToBeRemove, quotationHeader: model };
        this.quotationService.create(quotationWrapper)
            .subscribe(response => {
            this.showTheCreatedBy=true;
                this.quotationHeader = response;
                this.displayPartyName=this.quotationHeader.partyName;
                this.displayParty=this.quotationHeader.partyName
                 this.quotationHeader.partyName=this.quotationHeader.partyCode+"-"+this.quotationHeader.partyName;
              
               
                this.isPartyChangeable = false;
                // this.tranForm.controls['quotationNumber'].patchValue(response.quotationNumber);
                // this.tranForm.controls['id'].patchValue(response.id);
                // this.tranForm.controls['statusId'].patchValue(response.statusId);
                // this.tranForm.controls['statusName'].patchValue(response.statusName);
          
                this.tranForm = this.toFormGroup(this.quotationHeader, this.recentQuotationItem)
                if(!response.updateBy){
                    this.quotationService.getQuotation(response.id)
                    .subscribe(responses => {
                        this.tranForm.controls['updateBy'].patchValue(responses.updateBy);
                        this.tranForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));

                           

                    });
                }
                this.transactionStatus = "Quotation " + response.quotationNumber + message;
                //this.quotationHeader.id = response.id;
                this.saveStatus = true;
                this.alertService.success(this.transactionStatus);
                this.itemToBeRemove = [];
                this.isCreatePOButtonenabled()
                this.disableForm();
                this.recentQuotationLoadCounter++;
                this.spinStart = false;
            },
                error => {
                    //console.log("Error ", error);
                    this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
                    this.alertService.danger(this.transactionStatus);
                    this.saveStatus = false;
                    this.spinStart = false;
                }
            );
    }

    recentQuotationGet(event) {

        //console.log('In recentQuotationGet', event);
        this.quotationHeader = event;
        this.showTheCreatedBy=false;
        this.isInclusiveTax = this.quotationHeader.quotationItems[0].inclusiveTax === 0 ? false : true;
        this.inclusiveTaxDisable =true;
        //console.log("this.quotationHeader.partyId", this.quotationHeader.partyId)
        this.partyService.getCustomer(this.quotationHeader.partyId).subscribe(party => {
            this.selectedParty = party;
            this.isIgst = this.quotationHeader.isIgst > 0 ? true : false; //this.partyService.getIgstApplicable(this.selectedParty);
            console.log(this.quotationHeader.partyName+"befor")
            this.displayPartyName=this.quotationHeader.partyName;
            this.displayParty=this.quotationHeader.partyName
          this.quotationHeader.partyName=this.quotationHeader.partyCode+"-"+this.quotationHeader.partyName;
          console.log(this.quotationHeader.partyName+"after")
          this.tranForm = this.toFormGroup(this.quotationHeader, this.recentQuotationItem);
            this.setStatusBasedPermission(this.tranForm.controls['statusId'].value);
            this.itemsControl = this.tranForm.controls['quotationItems'];
            this.partyFormControl = this.tranForm.controls['partyName'];
            this.tranForm.get('billToAddress').patchValue(this.selectedParty.address);
            console.log("country"+this.selectedParty.countryId+"-----"+  this.quotationHeader.partyCountryId)
            if(this.quotationHeader.partyCountryId!=1)
            {
                this.taxNumberText="CIN Number"
            }
            else{
                this.taxNumberText="GST Number"
            }
           
           
            this.isCreatePOButtonenabled();
            this.disableForm();
            
           
        });
        //console.log('selectedParty: ', this.selectedParty);

        //On load from recent quotation, disable the form so that no changes can be done
        //unless Edit button is clicked
        this.headerTax = this.taxes.filter(tax => tax.id === this.quotationHeader.taxId)[0];
        
        this.isPartyChangeable = false;
        this.getFiltredMaterial();
    }

    clearForm(formDirective: FormGroupDirective) {

        formDirective.resetForm();
        this.initForm();
        this.isCreatePOButtonenabled();
        super.clearFormParent();
        this.itemsControl = this.tranForm.controls['quotationItems'];
        this.handleChanges();
        this.headerTax = null;
        this.tranForm.markAsPristine();
    }
    delete(formDirective: FormGroupDirective) {

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.tranForm.controls['id'].value;//this.quotationHeader.id;
                //console.log("deleteing..." + id);
                this.quotationService.delete(id)
                    .subscribe(response => {
                        //console.log(response);
                        this.transactionStatus = response.responseString;
                        this.alertService.success(this.transactionStatus);
                        this.saveStatus = true;
                        this.recentQuotationLoadCounter++;
                        this.spinStart = false;
                        this.isCreatePOenabled = false;
                        this.clearForm(formDirective);

                    },
                        error => {
                            //console.log("Error ", error);
                            this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                            this.alertService.danger(this.transactionStatus);
                            this.saveStatus = false;
                            this.spinStart = false;
                        });
            }
            this.dialogRef = null;
        });

    }
    removeQuotationItem(idx: number) {
        this.removeItem(idx, 'quotationItems', this.itemOfNewQuotation, this.quotationHeader ? this.quotationHeader.taxId : null);
        //this.handleChangesParent(this.itemOfNewQuotation, this.quotationHeader ? this.quotationHeader.taxId : null);
    }
    quantityChange(event: QuantityChangeEvent) {
        this.onQuantityChange(event, 'quotationItems');
    }
    isCreatePOButtonenabled()
    {
        if(this.tranForm.controls['id'].value != null && this.tranForm.controls['statusName'].value == AppSettings.STATUS_NEW)
        {
            this.isCreatePOenabled = true;
        }
        else
        {
            this.isCreatePOenabled = false;
        }
       
    }

    createPurchaseOrder(){
        console.log("createPurchaseOrder "+this.quotationHeader.totalTaxableAmount)
        let id=0;
        let sourceTransactionType: TransactionType;
        if(this.quotationHeader && this.quotationHeader.id)
        this.transCommService.initQuotationData();
        this.transCommService.updateQuotationData(this.quotationHeader);
            this.transactionTypeService_in.getAllTransactionTypes().subscribe(response => {
                this.transactionTypes=response;
                //console.log("this.transactionType.name"+this.transactionType.name);
                
                response.forEach(tt => {
                    if ( this.transactionType.name === AppSettings.CUSTOMER_QUOTATION) {
                      //console.log("tt"+tt)
                     if (tt.name == AppSettings.CUSTOMER_PO)
                     {
                        //console.log("tt.name"+tt.name)
                       
                        sourceTransactionType = tt;
                        this._router_in.navigate(['transaction/app-new-po-component',tt.id]);
                        
                     }
                    }
                   
                    }); 
            });
           

           
           
           }

    // print() {

    //     // let company: Company;
    //     // this.companyService.get(1).subscribe(comp => {
    //     //     company = comp;
    //     //     this.quotationSecondReportService.download(this.quotationHeader, company);
    //     //     // this.quotationReportService.download(this.quotationHeader, company);

    //     // });


    //     let l_printCopy: number;
    //     this.printRef = this.dialog.open(PrintDialog, {
    //         width: '250px',
    //         data: { printCopy: l_printCopy }
    //     });
    //     this.printRef.afterClosed().subscribe(result => {
    //         //console.log("result: ", result);
    //         if (result) {
    //             l_printCopy = +result;
    //             this.printWrapperService.print(this.transactionType, this.quotationHeader, l_printCopy, this.globalSetting);
    //             // let company: Company;
    //             // this.companyService.get(1).subscribe(comp => {
    //             //     company = comp;
    //             //     this.quotationSecondReportService.download(this.quotationHeader, company, l_printCopy);
    //             // });
    //         }

    //     })
    // }


    edit() {
        super.edit();
        this.itemsControl = this.tranForm.controls['quotationItems'];
        this.handleChanges();
        // if (this.tranForm.controls['deliveryChallanNumber'].value) {
        //     this.tranForm.controls['invoiceItems'].disable();
        //     this.itemDisableMessage = "Items cannot be changed for DC tracked Invoice"
        // }
        this.taxDisablecChanges()
    }

/*      getCurrencys() {
    console.log("getCurrencys");
    this.currencyService.getAllCurrency().subscribe(
        response => {
            this.currencys = response;
            this.currencys.forEach(currencys => {
              this.currencyName=currencys.currencyName;
              this.currencyFraction=currencys.currencyFraction;
              this.currencyDecimal=currencys.currencyDecimal;
              this.currencySymbol=currencys.currencySymbol;
             // console.log("="+this.currencyFraction)
            
        });
        
      
  
})
  } */
 /* getCurrencyData(currId:number){
  
 
  
  if(+currId!=0)
  {
  this.currency= this.currencys.filter(data => data.id===+currId)[0];
  
  this.currencyFraction=this.currency.currencyFraction;
  this.currencyDecimal=this.currency.currencyDecimal;
  this.currencySymbol = this.currency.currencySymbol;
  this.currencyName=this.currency.currencyName;
  console.log("currencyFraction = "+this.currencyFraction +"currencyDecimal = "+this.currencyDecimal+"currencySymbol"+this.currencySymbol);
  }
 } */
}
