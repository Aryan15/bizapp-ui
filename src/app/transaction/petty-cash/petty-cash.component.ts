import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { TransactionType, TransactionStatus } from '../../data-model/transaction-type';
import { CompanyService } from '../../services/company.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { UtilityService } from '../../utils/utility-service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { Company } from '../../data-model/company-model';
import { LocationService } from '../../services/location.service';

import { PettyCashHeader, PettyCashItem, Tag, Category, PettyCashWrapper } from '../../data-model/petty-cash-model';

import { PettyCashService } from '../../services/pettyCash.service';

import { PaymentMethod } from '../../data-model/payment-method';
import { PaymentMethodService } from '../../services/payment-method.service';
import { TagPopupComponent } from '../tag-popup/tag-popup.component';
import { CategoryPopupComponent } from '../category-popup/category-popup.component';
@Component({
  selector: 'app-petty-cash',
  templateUrl: './petty-cash.component.html',
  styleUrls: ['./petty-cash.component.scss']
})
export class PettyCashComponent implements OnInit, OnDestroy {
  @ViewChild('pettyCashNumber', {static: true}) pettyCashNumberElement: ElementRef;
  public pettyCashForm: FormGroup;

  item_length : number = 200;

  public company: Company;
  
  saveStatus: boolean = false;
  disableAdd: boolean =false;
  enableDatePicker = true;
  recentPettyCashLoadCounter: number = 0;
  spinStart: boolean = false;
  isChequeVoucher: boolean = false
  onDestroy: Subject<boolean> = new Subject();
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  defaultDate: string;
  financialYear: FinancialYear;
 
  fieldValidatorLabels = new Map<string, string>();
  isAutoNumber: boolean = false;
  disableAddButton: boolean = false;
  statusBasedPermission: StatusBasedPermission;
  itemToBeRemove: string[] = [];
  categorys: Category[] = [];
  tags:Tag[] = [];
 paymentMethods: PaymentMethod[]=[];
  minDate: Date;
  maxDate: Date;
  category: Category;
  tag:Tag;
  paymentMethod:PaymentMethod;
  datePipe = new DatePipe('en-US');
  isTermsAndcondition: boolean = false;
  numberRangeConfiguration: NumberRangeConfiguration;
  transactionType: TransactionType;
  pettyCashHeader: PettyCashHeader;
  transactionStatus: string = null;
  isPettyCash:boolean;
  pettyCashStatuses: TransactionStatus[] = [];
  applicableButtons: ApplicableButtons = {
      isApproveButton: false,
      isClearButton: true,
      isCloseButton: true,
      isCancelButton: false,
      isDeleteButton: true,
      isPrintButton: false,
      isSaveButton: true,
      isEditButton: true,
      isDraftButton: false,
  };

  activityRoleBindings: ActivityRoleBinding[];
  enableSaveButton: boolean = false;
  amount: number;
  tempTransactionNumber: string;
   enableApproveButton =false;
  description: string;
  matCategory: Category;
  matTag: Tag;
  matPayment:PaymentMethod;
  remarks:string
  incomingAmount:number
  outgoingAmount:number
  originalTotalAmount: number = 0;

  constructor(private fb: FormBuilder,
    private _router: Router,
    private alertService: AlertService,
    public dialog: MatDialog,
    public userService: UserService,
    private companyService: CompanyService,
    private financialYearService: FinancialYearService,
    private utilityService: UtilityService,
    private paymentMethodService: PaymentMethodService,
    private termsAndConditionsService: TermsAndConditionsService,
    private numberRangeConfigService: NumberRangeConfigService,
    private transactionTypeService: TransactionTypeService,
    private pettyCashService:PettyCashService,
    private transactionTypeService_in: TransactionTypeService,
    public route: ActivatedRoute,
   
    private transactionItemService: TransactionItemService,
    private locationService: LocationService) {
    this.setActivityRoleBinding();
    this.setTransactionType();

}

  ngOnInit() {
    this.company = this.companyService.getCompany();
   
    this.initForm();
    this.getFinancialYear();
    this.getDefaultDate();
 this.getPettyCashStatues();
  
  }
  ngOnDestroy() {
   
    this.onDestroy.next(true);
    this.onDestroy.complete();
}
  private initForm() {
    let data: PettyCashHeader = {
      id: null,
      peetCashId :null,
      pettyCashNumber:null,
      pettyCashDate:this.defaultDate,
      totalIncomingAmount:null,
      totalOutgoingAmount:null,
      isDayClosed:null,
      comments:null,
      approvedBy:null,
      approvedDate:null,
      financialYearId:null,
      companyId:null,
      statusId:null,
      statusName:null,
      totalAmount:null,
      bankAmount:null,
      cashAmount:null,
      pettyCashItems:null,
      pettyCashTypeId:null,
      isLatest:null,
      };
    this.pettyCashForm = this.toFormGroup(data);
    this.handleChanges();
     
    this.getLatestTransacion();   


     

  
  }



  private toFormGroup(data: PettyCashHeader): FormGroup {

    const itemArr = new FormArray([]);

    if (data.pettyCashItems) {
        data.pettyCashItems.forEach(item => {
            itemArr.push(this.makeItem(item));
        })
    }

    const formGroup = this.fb.group({
    id:[data.id],
    peetCashId:[data.peetCashId],
    pettyCashNumber: [data.pettyCashNumber],
    pettyCashDate:[this.datePipe.transform(data.pettyCashDate, AppSettings.DATE_FORMAT), [Validators.required]],
    totalIncomingAmount :[data.totalIncomingAmount],
    isDayClosed :[data.isDayClosed],
    totalOutgoingAmount :[data.totalOutgoingAmount],
    comments : [data.comments],
    approvedBy :[data.approvedBy],
    approvedDate :[data.approvedDate],
    financialYearId :[data.financialYearId],
    companyId :[data.companyId],
    statusId :[data.statusId],
    statusName:[data.statusName],
    totalAmount :[data.totalAmount],
    bankAmount :[data.bankAmount],
    cashAmount :[data.cashAmount],
    pettyCashTypeId:[data.pettyCashTypeId],
    isLatest:[data.isLatest],
    pettyCashItems: itemArr
    });


formGroup.get('pettyCashItems').setValidators(Validators.required);
     this.fieldValidatorLabels.set("pettyCashNumber", "PettyCash Number");
     this.fieldValidatorLabels.set("pettyCashDate", "PettyCash Date");
     this.fieldValidatorLabels.set("pettyCashItems", "PettyCash Items");
     this.fieldValidatorLabels.set("totalAmount", "PettyCash Items");
    return formGroup;
  }
  initTransactionItem(slNoIn: number
    , category: Category
    , tag: Tag
    ,paymentMethod:PaymentMethod
     ,remarks:string
    
    
):FormGroup {

    let pettyCashItem: PettyCashItem = {
        id: null,
        headerId: null,
        categoryId:category.id,
        categoryName:category.name,
        tagId:tag.id,
        tagName:tag.name,
        paymentMethodId: paymentMethod.id,
        paymentMethodName:paymentMethod.name,
        remarks:remarks,
        incomingAmount:null,
        outgoingAmount:null,
    }

    return this.makeItem(pettyCashItem);


}







  makeItem(pettyCashItem: PettyCashItem): FormGroup {
    return this.fb.group({
        id: pettyCashItem.id,
        pettyCashHeaderId:pettyCashItem.headerId,
        categoryId:pettyCashItem.categoryId,
        categoryName:pettyCashItem.categoryName,
        paymentMethodId:pettyCashItem.paymentMethodId,
        paymentMethodName:pettyCashItem.paymentMethodName,
        remarks:pettyCashItem.remarks,
        tagId:pettyCashItem.tagId,
        tagName:pettyCashItem.tagName,
        incomingAmount:pettyCashItem.incomingAmount,
        outgoingAmount:pettyCashItem.outgoingAmount,
    });
}

itemChanged(event) {
    
    this.handleChanges();

}


  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
        this.activityRoleBindings = response;
         this.activityRoleBindings.forEach(activityRoleBinding => {
            this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
           });
    })
}



handleItems(items: PettyCashItem[]) {
  
 let totalAmount: number= this.originalTotalAmount;//advTotalAmount;
 let totalIncomingAmount :number=0;
 let totalOutgoingAmount :number =0;
 let bankAmount :number =0;
 let cashAmount :number =0;
 items.forEach(item => {
 
   
   
      totalAmount += +item.incomingAmount;
    
      totalAmount +=-item.outgoingAmount;
    
      totalIncomingAmount += +item.incomingAmount;
      totalOutgoingAmount += +item.outgoingAmount;
      if(item.paymentMethodId === 2){
        cashAmount += +item.incomingAmount;
        cashAmount += +item.outgoingAmount;
      }
      else{
        bankAmount += +item.incomingAmount;
        bankAmount += +item.outgoingAmount; 
      }

 })


 this.pettyCashForm.controls['totalAmount'].patchValue(totalAmount);
   this.pettyCashForm.controls['totalIncomingAmount'].patchValue(totalIncomingAmount);
   this.pettyCashForm.controls['totalOutgoingAmount'].patchValue(totalOutgoingAmount);
   this.pettyCashForm.controls['cashAmount'].patchValue(cashAmount);
   this.pettyCashForm.controls['bankAmount'].patchValue(bankAmount);
 }
 quantityChange(items: PettyCashItem[]) {

   this.handleChanges();  
 }






 clearFormNoStatusChange() {
    this.initForm();

    this.handleChanges();
   
}

getPettyCashStatues() {
    this.transactionTypeService_in.getAllTransactionStatuses()
        .subscribe(response => {
            this.pettyCashStatuses = response;
        })
}

removePettyCashItem(idx: number) {

   

    let itemId: string = (<FormArray>this.pettyCashForm.get('pettyCashItems')).at(idx).get("id").value;
   
    if (itemId) {

       
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.itemToBeRemove.push(itemId);
                (<FormArray>this.pettyCashForm.get('pettyCashItems')).removeAt(idx);
            }
            else
            {
                this.disableAddButton = true;
            }
          
            this.handleChanges();
        });
    } else {
       
        (<FormArray>this.pettyCashForm.get('pettyCashItems')).removeAt(idx);
        this.handleChanges();
    }
    this.disableAddButton = false;
    return false;

}



setTransactionType() {

  this.route.params.
      pipe(takeUntil(this.onDestroy))
      .subscribe(params => {
          this.transactionTypeService.getTransactionType(+params['trantypeId'])
              .subscribe(response => {
                
                  this.transactionType = response;
                  ;
                  if (this.transactionType.name === AppSettings.PETTY_CASH) {
                      this.isPettyCash = true;
                    }

                    this.getCategory();
                    this.getTags();
                    this.getAllPaymentsMethod();
                  
                    this.getAutoNumber();
                  
                  setTimeout(() => {
                      this.recentPettyCashLoadCounter++;
                  }, 500);

                  this.getTempTransactionNumber();
              })

          
      });


}

getTempTransactionNumber() {

  this.transactionTypeService.getTransactionNumber(this.transactionType.name)
      .pipe(
      takeUntil(this.onDestroy)
      )
      .subscribe(response => {
          this.tempTransactionNumber = response.responseString;
         
      })
}
getAutoNumber() {
    
  this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id).subscribe(response => {
      this.numberRangeConfiguration = response;

      this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
     
  }
  )
}



  getFinancialYear() {
    this.financialYearService.getFinancialYear().subscribe(
        response => {
            this.financialYear = response;
            this.minDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
            this.maxDate = new Date(this.financialYear.endDate);
            
        }
    )
}


addItem() {
 
    let control = <FormArray>this.pettyCashForm.controls['pettyCashItems'];


    

    control.push(this.initTransactionItem(control.length + 1, this.matCategory, this.matTag,this.matPayment,this.remarks));
   

    this.category = undefined;
    this.matCategory = undefined;
    this.tag = undefined;
    this.matTag = undefined; 
    this.remarks = undefined;
    this.matPayment = undefined;
    
   // this.handleChanges();

  
    return false;
}


handleChanges() {
   let items: PettyCashItem[] = this.pettyCashForm.controls['pettyCashItems'].value;
    let header :number = this.pettyCashForm.controls['totalAmount'].value;
    
   
    this.handleItems(items);
}


  getDefaultDate() {

    this.utilityService.getCurrentDateObs().subscribe(response => {
        this.defaultDate = response;
        this.initForm();
    })
}

recentPettyCashGet(event) {
   
    this.pettyCashHeader = event;
    this.enableApproveButton =true;
    
    this.pettyCashForm = this.toFormGroup(this.pettyCashHeader);
    let dcFound: boolean = false;
    
    if(this.pettyCashHeader.isDayClosed===1){
      
        this.applicableButtons.isEditButton=false;
        this.applicableButtons.isDeleteButton = false;
        this.disableAdd = true;
       
    }
   
    this.disableForm();
}

onSubmit(model: PettyCashHeader) {


   
    if(this.numberRangeConfiguration.id===null){
        
        this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false
        });
        this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
        return;
    }

  
    
    if (model.pettyCashNumber == null && !this.isAutoNumber || model.pettyCashNumber == "") {
        this.alertService.danger("Please Enter PettyCash Number");
        return;
    }

    let checkNumAvail: boolean = false;
  
    if(this.isAutoNumber){ //in case of auto number do not check
        checkNumAvail = false;
    }
    else{
        if(model.id != null){ //if we are editing invoice which is not auto number
            if(model.pettyCashNumber != this.pettyCashHeader.pettyCashNumber){ // if invoice number is changed
                checkNumAvail = true;
            }else{
                checkNumAvail = false;
            }
        }else{ // if not auto number and creating new invoice, always check
            checkNumAvail = true;
        }
    }
    
    if(checkNumAvail){
        this.pettyCashService.checkPettyCashNumAvailability(model.pettyCashNumber, this.transactionType.id)
        .subscribe(response => {
            if (response.responseStatus === 1) {
                this.save(model);
               
            } else {
                
                this.alertService.danger("PettyCash Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                this.pettyCashForm.controls['pettyCashNumber'].patchValue('');
                this.pettyCashNumberElement.nativeElement.focus();

            }

        });
    }
    else{
        this.save(model);
    }


}

save(model: PettyCashHeader){
   

    if(model.id===null){
        model = this.transactionItemService.patchValueFromCompanyAndParty(model, null, this.company, false)
      
    }

   
    const controls = this.pettyCashForm;
    if (!this.pettyCashForm.valid) {
        const controls = this.pettyCashForm;
        let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
        //console.log("invalidFieldList ", invalidFieldList)

        let invalidFiledLabels: string[] = [];

        invalidFieldList.forEach(field => {
            if (this.fieldValidatorLabels.get(field))
                invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
        })

      

        this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
        return false;
    }


    model.companyId = 1; //hack....change it later
    model.financialYearId = this.financialYear.id;
    model.pettyCashTypeId = this.transactionType.id;
    this.spinStart = true;
    //console.log("before save: ", model);
    let message: string;
    if (model.id === null) {
        message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
    }
    else {
        message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
    }

    let PettyCashWrapper: PettyCashWrapper = { itemToBeRemove: this.itemToBeRemove, pettyCashHeader: model };

  
    
    this.pettyCashService.create(PettyCashWrapper)
        .subscribe(response => {
           
            this.pettyCashHeader = response;
            this.transactionStatus = "PettyCash " + response.pettyCashNumber + message;
            this.alertService.success(this.transactionStatus);
            this.saveStatus = true;
            this.enableApproveButton =true;
            this.spinStart = false;
            this.recentPettyCashLoadCounter++;
        
            // this.disableForm();
            this.pettyCashForm = this.toFormGroup(this.pettyCashHeader);
            this.disableForm();
        },
        error => {
            //console.log("Error ", error);
            this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
            this.alertService.danger(this.transactionStatus);
            this.saveStatus = false;
            this.spinStart = false;
        });
}


delete() {

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE


    this.dialogRef.afterClosed().subscribe(result => {
        if (result) {

            this.spinStart = true;
            let id: string = this.pettyCashForm.controls['id'].value;//this.quotationHeader.id;
            //console.log("deleteing..." + id);
            this.pettyCashService.delete(id)
                .subscribe(response => {
                    //console.log(response);
                    if (response.responseStatus === 1) {
                        this.transactionStatus = response.responseString;
                        this.saveStatus = true;
                        this.recentPettyCashLoadCounter++;
                        this.spinStart = false;
                        this.clearFormNoStatusChange();
                        this.alertService.success(this.transactionStatus);
                    } else {
                        this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                        this.transactionStatus = response.responseString;
                        this.saveStatus = false;
                        this.spinStart = false;
                        this.alertService.danger(response.responseString);
                    }
                },
                error => {
                    //console.log("Error ", error);
                    this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                    this.alertService.danger(this.transactionStatus);
                    this.saveStatus = false;
                    this.spinStart = false;
                });
        }
        this.dialogRef = null;
    });
}
 
closeForm() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
}

clearForm() {
   
     this.saveStatus = false;
     this.transactionStatus = null;
     this.initForm();
     this.handleChanges();
     this.category = undefined;
     this.matCategory = undefined;
     this.remarks = undefined;
     this.incomingAmount = undefined;
     this.outgoingAmount = undefined;
     this.tag = undefined;
     this.matTag = undefined;
     this.paymentMethod = undefined;
     this.matPayment = undefined;
     this.applicableButtons.isDeleteButton=true;
     this.applicableButtons.isEditButton =true;
     this.disableAdd =false;
     this.getTempTransactionNumber();
     this.enableApproveButton =false;
 }
 enableForm() {
    this.pettyCashForm.enable({ onlySelf: true, emitEvent: false });
}
 edit() {
    this.disableAddButton = true;
    this.enableForm();

}

setStatusBasedPermission(statusId: number) {

    if (statusId) {
        //console.log('status id: ', statusId);
        this.userService.getStatusBasedPermission(statusId).subscribe(response => {
            this.statusBasedPermission = response;
        })
    } else {
        //console.log("no status id: ", statusId);
    }
}

disableForm() {
    this.pettyCashForm.disable({ onlySelf: true, emitEvent: false });
    // this.disableParty = true;
}


getCategory() {
    this.pettyCashService.getAllCategory().subscribe(
        response => {

            this.categorys = response;
           
        }
    )
}


getTags(){
this.pettyCashService.getAllTags().subscribe(
    response =>{
        this.tags = response;
    }
)

}

getAllPaymentsMethod(){

    this.paymentMethodService.getAllPaymentMethods().subscribe(
        response =>{
           
            this.paymentMethods =response;

        }
    )
}

getLatestTransacion(){
    
this.pettyCashService.getLatestTransaction()
.subscribe(response => {
  


this.originalTotalAmount = response.totalAmount;

if(this.defaultDate != response.pettyCashDate){
   
    this.pettyCashForm.controls['totalAmount'].patchValue(response.totalAmount);
}

 else if(response.isDayClosed===1){
         this.applicableButtons.isDeleteButton =false;
        this.applicableButtons.isEditButton =false;
        this.disableAdd =true;
         
         this.pettyCashForm = this.toFormGroup(response);
         this.originalTotalAmount = response.totalAmount;
         this.disableForm();
        

}
else{
    this.pettyCashForm = this.toFormGroup(response);
    this.originalTotalAmount = response.totalAmount;
} 

});
}

//code for the popup

addNewCategory(){
 
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
      // id: 1,
      // title: 'Angular For Beginners'
  };
  const dialogRef = this.dialog.open(CategoryPopupComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(
    data => {
        if (data) {
            data.companyId = 1; //TO DO
            data.supplyTypeId = 1;
        
            this.pettyCashService.saveCategory([data]).subscribe(response => {
              

                let status = response[0].name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                this.alertService.success(status);
                this.categorys.push(response[0]);
                this.matCategory = response[0];

            },
            error => {
                //console.log("Error ", error, error.exception);
                
                if(error instanceof HttpErrorResponse){
                    var re = /DataIntegrityViolationException/gi;
                    if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
                    this.transactionStatus = "Duplicate Expense Name!! Cannot save";
                }
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
            });
        }
    });



}
addNewTag(){
   
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
      // id: 1,
      // title: 'Angular For Beginners'
  };
  const dialogRef = this.dialog.open(TagPopupComponent, dialogConfig);
  dialogRef.afterClosed().subscribe(
    data => {
        if (data) {
            data.companyId = 1; 
            data.supplyTypeId = 1;
            
            this.pettyCashService.saveTag([data]).subscribe(response => {
                

                let status = response[0].name + AppSettings.SAVE_SUCESSFULL_MESSAGE
                this.alertService.success(status);
                this.tags.push(response[0]);
                this.matTag = response[0];

            },
            error => {
                //console.log("Error ", error, error.exception);
                
                if(error instanceof HttpErrorResponse){
                    var re = /DataIntegrityViolationException/gi;
                    if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
                    this.transactionStatus = "Duplicate Tag Name!! Cannot save";
                }
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
            });
        }
    });
}

approvePettyCash(){

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.APPROVE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
        if (result) {

    this.pettyCashService.approvePettyCash(this.pettyCashHeader.id)
    .subscribe(response => {
        if(response.isDayClosed===1){
           
        let message ="Approved";
        this.transactionStatus = "PettyCash " + response.pettyCashNumber +message ;
        this.alertService.success(this.transactionStatus);
        this.applicableButtons.isDeleteButton =false;
        this.applicableButtons.isEditButton =false;
        this.disableAdd =true;
         this.disableForm();
        }

        else{
            this.alertService.success("Approve Failed");
        }
});

}

else{
    return;
}

    });
}
}