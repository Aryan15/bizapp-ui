import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentPettyCashComponent } from './recent-petty-cash.component';

describe('RecentPettyCashComponent', () => {
  let component: RecentPettyCashComponent;
  let fixture: ComponentFixture<RecentPettyCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentPettyCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentPettyCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
