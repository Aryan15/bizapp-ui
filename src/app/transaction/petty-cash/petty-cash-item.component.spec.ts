import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PettyCashItemComponent } from './petty-cash-item.component';

describe('PettyCashItemComponent', () => {
  let component: PettyCashItemComponent;
  let fixture: ComponentFixture<PettyCashItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PettyCashItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PettyCashItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
