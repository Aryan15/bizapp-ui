import { Component, OnInit, OnChanges, Input, Output, ViewChild,EventEmitter } from '@angular/core';
import { PettyCashService } from '../../services/pettyCash.service';

import { RecentPettyCash, PettyCashHeader } from '../../data-model/petty-cash-model';
import { TransactionType } from '../../data-model/transaction-type';
import {Observable, merge, of as observableOf} from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, startWith, switchMap, catchError, map } from 'rxjs/operators';
const PAGE_SIZE: number = 10;
@Component({
  selector: 'app-recent-petty-cash',
  templateUrl: './recent-petty-cash.component.html',
  styleUrls: ['./recent-petty-cash.component.scss']
})
export class RecentPettyCashComponent  implements OnInit, OnChanges {

  @Input()
  recentPettyCashLoadCounter: number;

    @Input()
    transactionType : TransactionType;

    @Output()
    pettyCashHeader: EventEmitter<PettyCashHeader> = new EventEmitter<PettyCashHeader>();

    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    displayedColumns = ['pettyCashNumber','pettyCashDate', 'totalAmount'];
   pettyCashDatabase: PettyCashHttpDao | null; 

    dataSource = new MatTableDataSource<PettyCashHeader>();

    filterForm = new FormGroup ({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
  
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private pettyCashService: PettyCashService) { }


  ngOnInit() {
    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
    
    this.pettyCashDatabase = new PettyCashHttpDao(this.pettyCashService);

 

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);



  }
  ngOnChanges(){
    
    console.log('in onchanage: ', this.recentPettyCashLoadCounter);

    if(this.recentPettyCashLoadCounter > 0 ){
   
      merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          
          return this.pettyCashDatabase!.getPettyCash(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
      
          this.isLoadingResults = false;
          this.resultsLength = data.totalCount;
         
         if( this.filterForm.get('filterText').value != null)
         {
          if(data.totalCount === 0)
          {
            this.noDataFound.emit(true);
          }
         }
          return data.pettyCashHeaders;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError', error);
          return observableOf([]);
        })
      ).subscribe(data => this.dataSource.data = data);

    }
  }

  onRowClick(row: any){
   
    this.pettyCashHeader.emit(row);
   
}
}
export class PettyCashHttpDao{
    
  constructor(private pettyCashService: PettyCashService) { }

  getPettyCash(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPettyCash>{

    pageSize = pageSize ? pageSize : PAGE_SIZE;
  


    return this.pettyCashService.getRecentPettyCash(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);
  }
}