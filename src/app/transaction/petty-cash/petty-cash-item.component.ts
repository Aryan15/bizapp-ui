import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
//import { EventEmitter } from 'events';

import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { QuantityChangeEvent } from '../../data-model/misc-model';

@Component({
  selector: 'app-petty-cash-item',
  templateUrl: './petty-cash-item.component.html',
  styleUrls: ['./petty-cash-item.component.scss']
})
export class PettyCashItemComponent implements OnInit {
  @Input() group: FormGroup; 
  @Input() index: number;
  @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
 @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
   @Output() quantityChange: EventEmitter<QuantityChangeEvent>= new EventEmitter<QuantityChangeEvent>();
   private changeCtr: number = 0;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  constructor(
      public dialog: MatDialog, ) {

  }
  ngOnInit() {

  }






  onQuantityChange() {
    
    
   let Iquantity: number = this.group.controls['incomingAmount'].value;
   let Oquantity: number = this.group.controls['outgoingAmount'].value;

   
    this.changeCtr++;
    this.changeCounter.emit(this.changeCtr);
}

delete(index: number) {
    
           
            this.deleteIndex.emit(index);
        }
 

}
