import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreventUnsavedChangesGuardInvoice } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-invoice';
import { PreventUnsavedChangesGuardPo } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-po';
import { DeliveryChallanComponent } from './delivery-challan/dc.component';
import { PreventUnsavedChangesGuardDC } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-dc';
import { GrnComponent } from './grn/grn.component';
import { PreventUnsavedChangesGuardGrn } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-grn';
import { PreventUnsavedChangesGuardPr } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-pr';
import { NewPoComponentComponent } from './new-po-component/new-po-component.component';
import { NewQuotationComponent } from './new-quotation/new-quotation.component';
import { NewInvoiceComponent } from './new-invoice/new-invoice.component';
import { NewPayableReceivableComponent } from './new-payable-receivable/new-payable-receivable.component';
import { JobWorkDeliveryChallanComponent } from './job-work-delivery-challan/job-work-delivery-challan.component';
import { VoucherComponent } from './voucher/voucher.component';
import { PreventUnsavedChangesGuardQuotation } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-quotation';
import { PreventUnsavedChangesGuardJobWorkDc } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-jwdc';
import { PreventUnsavedChangesGuardVoucher } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-voucher';
import { AutoTransactionPopupComponent } from './auto-transaction-popup/auto-transaction-popup.component';
import { PettyCashComponent } from './petty-cash/petty-cash.component';
import { TagPopupComponent } from './tag-popup/tag-popup.component';
import { CategoryPopupComponent } from './category-popup/category-popup.component';
import { EwayBillComponent } from './eway-bill/eway-bill.component';
import { AutoTransactionMasterComponent } from '../master/auto-transaction-master/auto-transaction-master.component';

const routes: Routes = [  
    {
      path: 'dc-form/:trantypeId',
      component: DeliveryChallanComponent,
      data: { state: 'dc-form' },
      canDeactivate: [PreventUnsavedChangesGuardDC]
    },
    {
      path: 'grn-form/:trantypeId',
      component: GrnComponent,
      data: { state: 'grn-form' },
      canDeactivate: [PreventUnsavedChangesGuardGrn]
    },    

    {
      path: 'app-new-po-component/:trantypeId',
      component: NewPoComponentComponent,
      data: { state: 'app-new-po-component'},
      canDeactivate: [PreventUnsavedChangesGuardPo]
    },
    {
      path: 'app-new-quotation/:trantypeId',
      component: NewQuotationComponent,
      data: { state: 'app-new-quotation'},
     canDeactivate: [PreventUnsavedChangesGuardQuotation]
    },
    {
      path: 'app-new-invoice/:trantypeId',
      component: NewInvoiceComponent,
      data: { state: 'app-new-invoice'},
      canDeactivate: [PreventUnsavedChangesGuardInvoice]
    },
    {
      path: 'app-new-payable-receivable/:trantypeId',
      component: NewPayableReceivableComponent,
      data: { state: 'app-new-payable-receivable' },
      canDeactivate: [PreventUnsavedChangesGuardPr]
    },
    {
      path: 'app-eway-bill',
      component: EwayBillComponent,
      data: { state: 'app-eway-bill' },
     },
    {
      path: 'app-job-work-delivery-challan/:trantypeId',
      component: JobWorkDeliveryChallanComponent,
      data: { state: 'JobWorkDeliveryChallanComponent' },
      canDeactivate: [PreventUnsavedChangesGuardJobWorkDc]
    },
    {
      path: 'app-voucher/:trantypeId',
      component: VoucherComponent,
      data: { state: 'VoucherComponent' },
      canDeactivate: [PreventUnsavedChangesGuardVoucher]
    },

    {
      path: 'app-auto-transaction-popup',
      component: AutoTransactionPopupComponent,
      data: { state: 'AutoTransactionPopupComponent' },
     
    },

    {
      path: 'app-petty-cash/:trantypeId',
      component: PettyCashComponent,
      data: { state: 'PettyCashComponent' },
     
    },

    {
      path: 'app-category-popup',
      component: CategoryPopupComponent,
      data: { state: 'CategoryPopupComponent' },
     
    },

    {
      path: 'app-tag-popup',
      component: TagPopupComponent,
      data: { state: 'TagPopupComponent' },
     
    },

    {
      path: 'app-auto-transaction-master',
      component: AutoTransactionMasterComponent,
      data: { state: 'app-auto-transaction-master'}
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionRoutingModule { }
