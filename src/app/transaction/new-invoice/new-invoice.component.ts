import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators, NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { AlertService } from 'ngx-alerts';
import { take, takeUntil, startWith, map, ignoreElements } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { DeliveryChallanHeader } from '../../data-model/delivery-challan-model';
import { GrnHeader } from '../../data-model/grn-model';
import { InvoiceHeader, InvoiceWrapper } from '../../data-model/invoice-model';
import { ApplicableButtons, QuantityChangeEvent } from '../../data-model/misc-model';
import { Party } from '../../data-model/party-model';
import { PurchaseOrderHeader } from '../../data-model/purchase-order-model';
import { TransactionItem } from '../../data-model/transaction-item';
import { TransactionStatus, TransactionType } from '../../data-model/transaction-type';
import { CompanyService } from '../../services/company.service';
import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { GrnService } from '../../services/grn.service';
import { InvoiceService } from '../../services/invoice.service';
import { LanguageService } from '../../services/language.service';
import { MaterialService } from '../../services/material.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PrintWrapperService } from '../../services/print-warpper.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { QuotationService } from '../../services/quotation.service';
import { TaxService } from '../../services/tax.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransCommService } from '../../services/trans-comm.service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { TransactionSummaryService } from '../../services/transaction-summary.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { UtilityService } from '../../utils/utility-service';
import { TransactionParentComponent } from '../transaction-parent/transaction-parent.component';
import { LocationService } from '../../services/location.service';
import { Material, MaterialStockCheckDTOWrapper, MaterialStockCheckDTO, MaterialTypeForCreadit } from '../../data-model/material-model';
import { JasperPrintService } from '../../services/jasper-print.service';
import { ConnectionService } from '../../services/connection.service';
import { AutoTransactionPopupComponent } from '../auto-transaction-popup/auto-transaction-popup.component';
import { AutoTransactionGenerationList } from '../../data-model/autotransaction-popup-model';
import { Observable } from 'rxjs';
import { AutoTransactionMasterComponent } from '../../master/auto-transaction-master/auto-transaction-master.component';
import { MaterialType } from '../../data-model/material-type-model';
import { NumberToWordsService } from '../../services/number-to-words.service';
import {CurrencyPipe} from '@angular/common';
import { formatCurrency,getCurrencySymbol } from '@angular/common';
import {registerLocaleData} from '@angular/common';

// import { ReportParentComponent } from '../../reports/report-parent/report-parent.component';

//import {provideTranslator, TranslatorContainer, Translator} from 'angular-translator';

@Component({
    selector: 'app-new-invoice',
    templateUrl: './new-invoice.component.html',
    styleUrls: ['./new-invoice.component.scss']
})
export class NewInvoiceComponent extends TransactionParentComponent implements OnInit {
    @ViewChild('invoiceNumber', { static: true }) invoiceNumberElement: ElementRef;
    @ViewChild('formDirective', { static: false }) formDirective: NgForm;

    item_length: number = AppSettings.length_limit;
    // public invoiceForm: FormGroup;
    public invoiceHeader: InvoiceHeader;

    // subheading : String = "this page is used to record Information about ";
    // disableSelect: boolean = false;
    initFormItem: string;
    recentInvoiceLoadCounter: number = 0;
    itemDisableMessage: string;
    invTaxDisable :boolean=false;
    public purchaseOrders: PurchaseOrderHeader[] = [];
    public deliveryChallans: DeliveryChallanHeader[] = [];
    public deliveryChallansSelect: DeliveryChallansSelect[] = [];
    public selectedDeliveryChallansSelect: DeliveryChallansSelect[] = [];
    public proformaInvoices: InvoiceHeader[] = [];
    public sourceInvoices: InvoiceHeader[] = [];
    public grns: GrnHeader[] = [];
    selectedPurchaseOrder: PurchaseOrderHeader;
    selectedProformaInvoice: InvoiceHeader;
    selectedGrn: GrnHeader;
    isCreateEnabled: boolean = false;
    isCloneEnabled: boolean = false;
    selectedSourceInvoice: InvoiceHeader;
    selectedDeliveryChallans: DeliveryChallanHeader[] = [];
    fieldValidatorLabels = new Map<string, string>();
    itemOfRecentInvoice: string = AppSettings.TXN_ITEM_OF_RECENT_INVOICE; //"Recent Invoice Item";
    itemOfPO: string = AppSettings.TXN_ITEM_OF_PO_IN_INVOICE; //"PO Item";
    itemOfDC: string = AppSettings.TXN_ITEM_OF_DC_IN_INVOICE; //"DC Item";
    itemOfProformaInvoice: string = AppSettings.TXN_ITEM_OF_PI_IN_INVOICE;
    itemOfGRN: string = AppSettings.TXN_ITEM_OF_GRN_IN_INVOICE;
    itemOfSourceInvoice: string = AppSettings.TXN_ITEM_OF_SOURCE_INVOICE;
    itemOfNewInvoice: string; // = "New Invoice Item";
    prevDCNumbers: string[] = [];
    invoiceStatuses: TransactionStatus[] = [];
    isProformaInvoice: boolean = false;
    isPurchaseInvoice: boolean = false;
    allowAutoTransaction: boolean = false;
    isJobWorkNote:boolean =false;
    isRecuringInvoice: number = 0;
    invoiceNumber: string;
    invoiceId: number = 0;
    invoiceHeaderId: string;
    isReused: number = 0;
    allowReuse: boolean = false;
    statusCheck: number = 0;
    preTcsAmount:number =0;
    preTcsPercentage:number=0;
    showTheCreatedBy=false;
    isheaderTaxDisable:boolean=false;
    isEwayBillEnabled: boolean = false;
    disableSelect: boolean = false;
    currencyDecimal:string;
    currencyFraction :string;
    amountInWords:string;
    currencyRate:number;
    taxNumberText="GST Number";
   // ExportAmountInWords:string;
    public materialTypes: MaterialTypeForCreadit[] = [
        { id: 1, name: "Inventry/Rework" },
        { id: 2, name: "Scrap/Reject" }
      
      ]

    applicableButtons: ApplicableButtons = {
        isApproveButton: false,
        isClearButton: true,
        isCloseButton: true,
        isCancelButton: true,
        isDeleteButton: true,
        isPrintButton: false,
        isSaveButton: true,
        isEditButton: true,
        isDraftButton: false,

    };

    showRecent: boolean = false;
    displayPartyName: string = "";
    filteredPO: Observable<PurchaseOrderHeader[]>;
    filteredDC: Observable<DeliveryChallansSelect[]>;

    noteType: string = "rejectNote";
  

    constructor(
        private fb: FormBuilder,
        public route_in: ActivatedRoute,
        private _router_in: Router,
        private transactionTypeService_in: TransactionTypeService,
        private numberRangeConfigService_in: NumberRangeConfigService,
        private termsAndConditionsService_in: TermsAndConditionsService,
        private utilityService_in: UtilityService,
        private materialService_in: MaterialService,
        private partyService_in: PartyService,
        private userService_in: UserService,
        private globalSettingService_in: GlobalSettingService,
        private uomService_in: UomService,
        private taxService_in: TaxService,
        //private translator: Translator,
        private alertService_in: AlertService,
        private quotationService_in: QuotationService,
        private financialYearService_in: FinancialYearService,
        private transactionItemService_in: TransactionItemService,
        private transactionSummaryService_in: TransactionSummaryService,
        private purchaseOrderService_in: PurchaseOrderService,
        private grnService_in: GrnService,
        private invoiceService_in: InvoiceService,
        private deliveryChallanService_in: DeliveryChallanService,
        private companyService_in: CompanyService,
        private translate: TranslateService,
        private numberToWordsService: NumberToWordsService,
        //private invoiceReportSecondService: InvoiceReportSecondService,
        private dialog_in: MatDialog,
        private printWrapperService_in: PrintWrapperService,
        private languageService: LanguageService,
        private transCommService: TransCommService,
        private locationService_in: LocationService,
        private jasperPrintService: JasperPrintService,
        protected connectionService: ConnectionService,
        currencyPipe:CurrencyPipe

    ) {

        super(
            route_in,
            _router_in,
            transactionTypeService_in,
            numberRangeConfigService_in,
            termsAndConditionsService_in,
            utilityService_in,
            materialService_in,
            partyService_in,
            userService_in,
            globalSettingService_in,
            uomService_in,
            taxService_in,
            financialYearService_in,
            quotationService_in,
            alertService_in,
            transactionItemService_in,
            transactionSummaryService_in,
            dialog_in,
            printWrapperService_in,
            companyService_in,
            locationService_in,
            connectionService,
         
          
        );
        // translator.translate("HELLO_WORLD").then(
        ////     (translation) => //console.log(translation)
        //   );
        translate.setDefaultLang('en');
     //  this.grandTotal =+this.currencyPipe.transform(this.grandTotal);
    }


    ngOnInit() {
      
        this.initParent();
        this.globalSettingService_in.getGlobalSetting().subscribe(response => {
            this.globalSetting = response;
        
              this.isEwayBillEnabled = this.globalSetting.allowEwayBill === 1 ? true : false;
        
           })
        ////setTimeout(() => {//console.log('in child: ', this.transactionType)}, 1000);
        //console.log("before init complete...")
        this.initComplete.subscribe(response => {
            ////console.log("in init complete...", this.transactionType.name);
            this.recentInvoiceLoadCounter++;
            if (response) {


                this.initForm();
                this.resetVariables();

                this.getInvoiceStatues();
                // this.invoiceStatuses.push({
                //     id: 4,
                //     name: 'Cancelled',
                //     transactionTypeId: this.transactionType.id,
                //     deleted: 'N',
                //     transactionTypeName: ''
                // });
                if (this.transactionType.name === AppSettings.PROFORMA_INVOICE || this.transactionType.name === AppSettings.INCOMING_JOBWORK_PROFORMA_INVOICE) {
                    this.isProformaInvoice = true;
                } else {
                    this.isProformaInvoice = false;
                }
                if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
                    this.isPurchaseInvoice = true;
                } else {
                    this.isPurchaseInvoice = false;
                }
                // let isCancelButton: ApplicableButtons;
                if (this.transactionType.name === AppSettings.PROFORMA_INVOICE || this.transactionType.name === AppSettings.CREDIT_NOTE || this.transactionType.name === AppSettings.DEBIT_NOTE) {
                    this.applicableButtons.isCancelButton = false;

                }

                if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE) {
                    this.applicableButtons.isPrintButton = false;

                }
           if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_CREDIT_NOTE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_DEBIT_NOTE){
               this.isJobWorkNote=true;

            }




                this.itemsControl = this.tranForm.controls['invoiceItems'];
                this.partyFormControl = this.tranForm.controls['partyName'];
                this.getFilteredParties();
                this.handleChangesParent(this.itemOfNewInvoice, this.invoiceHeader ? this.invoiceHeader.taxId : null);

                this.tranForm.controls['paymentTerms'].patchValue(this.paymentTerms);
                this.tranForm.controls['deliveryTerms'].patchValue(this.deliveryTerms);
                this.tranForm.controls['termsAndConditions'].patchValue(this.termsAndConditions);
               
                if (this.loadFromId && this.transactionId) {
                    this.invoiceService_in.getInvoice(this.transactionId).subscribe(response => {
                        if (response) {

                            this.recentInvoiceGet(response);
                        }
                    })
   
                } else {
                    this.transCommService.invoiceData
                        .pipe(
                            take(1)
                            , takeUntil(this.onDestroy)
                        )
                        .subscribe(response => {
                            ////console.log('response ', this.transactionType.name, "this.transactionId", response);
                            if (response) {
                                // console.log("response invoiceData.....", response);

                                // if(response.invoiceTypeId)
                                this.tcsAmount=response.tcsAmount;
                                this.tcsPercentage=response.tcsPercentage;
                                if (response.statusName === AppSettings.STATUS_NEW) {
                                    this.recentInvoiceGet(response, true);
                                  
                                }
                                this.transCommService.updateInvoiceData(null);
                                this.transCommService.completeInvoiceData();
                            }

                            else {

                                this.transCommService.dcData
                                    .pipe(
                                        take(1)
                                        , takeUntil(this.onDestroy)
                                    )
                                    .subscribe(response => {

                                        
                                        if (response) {
                                            this.isPartyChangeable = false;
                                            this.tranForm.controls['deliveryChallanNumber'].patchValue([response.deliveryChallanNumber]);
                                            this.tranForm.controls['partyName'].patchValue(response.partyName);
                                            this.tranForm.controls['partyId'].patchValue(response.partyId);
                                            this.tranForm.controls['billToAddress'].patchValue(response.billToAddress);
                                            this.tranForm.controls['shipToAddress'].patchValue(response.shipToAddress);
                                            this.tranForm.controls['gstNumber'].patchValue(response.gstNumber);
                                            this.tranForm.controls['inclusiveTax'].patchValue(response.inclusiveTax);
                                         /*    
                                            if(response.partyCountryId!=1){
                                                this.tranForm.controls['currencyId'].patchValue(response.partyCurrencyId);
                                                console.log(" response.partyCurrencyId "+response.partyCurrencyId)
      
                                            }else{ this.tranForm.controls['currencyId'].patchValue(0);
                                        } */

                                            //this.tranForm.controls['currencyName'].patchValue(response.partyCurrencyId)
                                            //console.log(response.partyCurrencyId +"= response.currencyId");
                                            // this.getAllCurrencys();
                                            
                                                   
                                   if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
                                     {
                                       this.isheaderTaxDisable=true;//headerlevel
                                    }
                                     if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==0)
                                      { 
                                    this.isheaderTaxDisable=false;
                                      }
 
                                    if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
                                    {
                                      this.isItemLevelTax=false;
                                     this.isheaderTaxDisable=true
       
                                     }
                                    if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==1)
                                        { 
                                           this.isItemLevelTax=true;
                                        }
                                           

                                            this.isInclusiveTax = response.inclusiveTax ? true : false;
                                            this.inclusiveTaxDisable = true;//this.isInclusiveTax ? true : false;
                                            this.prevDCNumbers = [];
                                            this.partyService.getCustomer(response.partyId).subscribe(party => {

                                               

                                                this.selectedParty = party;
                                                
                                                this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                                                ////console.log(" this.isIgst " + this.isIgst);
                                                this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                                                this.tranForm.controls['shipToAddress'].patchValue(party.billAddress);
                                                
                                                if( this.selectedParty.countryId!=1 && this.selectedParty.partyCurrencyId!=null)
                                                {
                                                    this.tranForm.controls['currencyId'].patchValue(this.selectedParty.partyCurrencyId);
                                                    this.tranForm.controls['currencyName'].patchValue(this.selectedParty.partyCurrencyName);
                                                    this.tranForm.controls['partyCountryId'].patchValue(response.partyCountryId);

                                               // console.log(" inside api call "+this.selectedParty.partyCurrencyId +this.selectedParty.partyCurrencyName)
                                               /*  this.partyService.getCurrency(this.selectedParty.partyCurrencyId).subscribe(data => {
                                                    this.tranForm.controls['currencyName'].patchValue(data.currencyName);
    
                                                }); */
                                               }
                                              
                                                this.onDCChange(response);
                                                // this.tranForm.controls['address'].patchValue(party.address);
                                                // this.tranForm.controls['shipToAddress'].patchValue(party.billAddress);
                                            });


                                            this.deliveryChallans.push(response);

                                            // this.transCommService.dcHeader.next(null);
                                            this.transCommService.updateDcData(null);
                                            // this.transCommService.dcHeader.complete();
                                            this.transCommService.completeDcData();
                                           

                                        }

                                        else {

                                            this.transCommService.grnData
                                                .pipe(
                                                    take(1)
                                                    , takeUntil(this.onDestroy)
                                                )
                                                .subscribe(response => {
                                                    ////console.log("response : ", response);

                                                    if (response) {
                                                        ////console.log("response grnData..................", response);
                                                        this.isPartyChangeable = false;
                                                        this.grns.push(response);
                                                        this.tranForm.controls['grnNumber'].patchValue(response.grnNumber);
                                                        this.tranForm.controls['partyName'].patchValue(response.supplierName);
                                                        this.tranForm.controls['partyId'].patchValue(response.supplierId);
                                                        this.tranForm.controls['grnDate'].patchValue(response.grnDate);
                                                        this.tranForm.controls['billToAddress'].patchValue(response.supplierAddress);
                                                        this.tranForm.controls['address'].patchValue(response.supplierAddress);
                                                        this.tranForm.controls['deliveryChallanDateString'].patchValue(this.datePipe.transform(response.deliveryChallanDate, AppSettings.DATE_FORMAT_DISPLAY));

                                                        if(this.selectedParty.countryId!=null){
                                                            this.tranForm.controls['currencyId'].patchValue(this.selectedParty.partyCurrencyId);
                                                        }else{  this.tranForm.controls['currencyId'].patchValue(0);
                                                    }
                                                       

                                                       
                                                        this.isInclusiveTax = response.inclusiveTax === 1 ? true : false;
                                                        this.inclusiveTaxDisable = true;//response.inclusiveTax === 1 ? true : false;

                                                        ////console.log("this.isInclusiveTax :", this.isInclusiveTax);
                                                        ////console.log("this.inclusiveTaxDisable :", this.inclusiveTaxDisable);
                                                        ////console.log("id :", response.supplierId);

                                                        this.partyService.getCustomer(response.supplierId).subscribe(party => {
                                                            ////console.log("party : ", party);
                                                            this.selectedParty = party;
                                                            this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                                                            this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                                                            ////console.log(" this.isIgst " + this.isIgst);
                                                            this.tranForm.controls['shipToAddress'].patchValue(party.billAddress);
                                                        });
                                                        this.onGrnChange(response);
                                                        this.transCommService.updateDcData(null);
                                                        this.transCommService.completeDcData();
                                                        this.transCommService.updateGrnData(null);

                                                        this.transCommService.completeGrnData();

                                                    }
                                                    else {
                                                        this.transCommService.replicatedinvoiceData
                                                            .pipe(
                                                                take(1)
                                                                , takeUntil(this.onDestroy)
                                                            )
                                                            .subscribe(response => {
                                                                ////console.log('response at replication', this.transactionType.name, "this.transactionId", response);
                                                                if (response) {

                                                                    this.recentInvoiceGet(response, false, true);
                                                                    this.isPartyChangeable = false;
                                                                    this.transCommService.updateInvoiceData(null);

                                                                    this.transCommService.completeInvoiceData();
                                                                    //this.transCommService.invoiceHeader.next(null);
                                                                    //this.transCommService.invoiceHeader.complete();
                                                                } else {
                                                                    this.transCommService.poData
                                                                        .pipe(
                                                                            take(1)
                                                                            , takeUntil(this.onDestroy)
                                                                        )
                                                                        .subscribe(response => {
                                                                            if (response) {
                                                                                this.isPartyChangeable = false;
                                                                                this.tranForm.controls['purchaseOrderNumber'].patchValue(response.purchaseOrderNumber);
                                                                                this.tranForm.controls['partyName'].patchValue(response.partyName);
                                                                                this.tranForm.controls['partyId'].patchValue(response.partyId);
                                                                                this.tranForm.controls['billToAddress'].patchValue(response.address);
                                                                                this.tranForm.controls['shipToAddress'].patchValue(response.shipToAddress);
                                                                                this.tranForm.controls['gstNumber'].patchValue(response.gstNumber);
                                                                                this.tranForm.controls['inclusiveTax'].patchValue(response.inclusiveTax);
                                                                                console.log("neam"+response.currencyName)
                                                                                this.tranForm.controls['currencyName'].patchValue(response.currencyName);

                                                                                this.isInclusiveTax = response.inclusiveTax ? true : false;
                                                                                this.inclusiveTaxDisable = true; //this.isInclusiveTax ? true : false;

                                                                                  //this.numberInWords(response.grandTotal)     
                  if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
                  {
                     this.isheaderTaxDisable=true;//headerlevel
                  }
             if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==0)
                  {  
                    this.isheaderTaxDisable=false;
                  }
 
             if(response.partyCountryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
                 {
                     this.isItemLevelTax=false;
                   this.isheaderTaxDisable=true
       
                 }
            if(response.partyCountryId==1 && this.globalSetting.itemLevelTax==1)
              { 
               this.isItemLevelTax=true;
              }
                                                                              
                                                                                this.partyService.getCustomer(response.partyId).subscribe(party => {
                                                                                    this.selectedParty = party;
                                                                                    this.onPurchaseOrderNumberChange(null, response, true);
                                                                                    
                                                                                    if(this.selectedParty.countryId!=1){
                                                                                        this.tranForm.controls['currencyId'].patchValue(this.selectedParty.partyCurrencyId);
                                                                                        this.taxNumberText="CIN Number";
                                                                                    } else{
                                                                                        this.taxNumberText="GST Number";
                                                                                        this.tranForm.controls['currencyId'].patchValue(0);}

                                                                                    

                                                                                  /*   if(this.selectedParty.partyCurrencyId!=null)
                                                                                    {
                                                                                        this.partyService.getCurrency(this.selectedParty.partyCurrencyId).subscribe(data => {
                                                                                            this.tranForm.controls['currencyName'].patchValue(data.currencyName);
                                                                                               this.currencyDecimal = data.currencyDecimal;
                                                                                               this.currencyFraction= data.currencyFraction;
                                                                                                 
                                                                                        });   } */
                                                                                    
                                                                                  
                                                                            
                                                                                    this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                                                                                    ////console.log(" this.isIgst " + this.isIgst);
                                                                                    this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                                                                                });


                                                                                this.purchaseOrders.push(response);
                                                                                this.transCommService.updateQuotationData(null);
                                                                                this.transCommService.completeQuotationData();
                                                                                this.transCommService.updatePoData(null);

                                                                                this.transCommService.completePoData();

                                                                            }
                                                                        })
                                                                }

                                                            });
                                                    }
                                                });
                                        }

                                    });
                            }


                        });


                }
                //this.getFilteredParties(this.quotationForm.controls['partyName']);
            }
            
        })

        this.languageService.getLanguage()
            .subscribe(resp => {
                if (resp === null) {
                    this.translate.use("en");

                }
                else {
                    this.translate.use(resp);
                }
            })

    }

    getInvoiceStatues() {
        this.transactionTypeService_in.getAllTransactionStatuses()
            .subscribe(response => {
                this.invoiceStatuses = response;
            })
    }
    handleChanges() {

        this.handleChangesParent(this.itemOfNewInvoice, this.invoiceHeader ? this.invoiceHeader.taxId : null);

    }

    addInvoiceItem(materialquantity: number) {

        this.addItemWrapper(materialquantity, <FormArray>this.tranForm.controls['invoiceItems']);

    }


    private initForm() {
        let data: InvoiceHeader = {
            id: null,
            invoiceDate: this.defaultDate,//this.utilityService.getCurrentDate(),
            gstNumber: null,
            invoiceNumber: null,
            purchaseOrderDate: null,
            purchaseOrderNumber: null,
            referenceDate: null,
            address: null,
            internalReferenceNumber: null,
            deliveryChallanDate: null,
            deliveryChallanNumber: null,
            proformaInvoiceNumber: null,
            proformaInvoiceDate: null,
            sourceInvoiceNumber: null,
            sourceInvoiceDate: null,
            grnNumber: null,
            grnDate: null,
            stateId: null,
            stateName: null,
          
            paymentDueDate: null,
            eWayBillNumber: null,
            totalTaxableAmount: null,
            totalIgstTax: null,
            totalCgstTax: null,
            grandTotal: null,
            billToAddress: null,
            labourChargesOnly: null,
            subTotalAmount: null,
            paymentTerms: null,
            deliveryTerms: null,
            termsAndConditions: null,
            modeOfDispatch: null,
            vehicleNumber: null,
            numOfPackages: null,
            documentThrough: null,
            shipToAddress: null,
            totalDiscount: null,
            netAmount: null,
            transportationCharges: null,
            advanceAmount: null,
            isRoudedOff: null,
            roundOffAmount: null,
            invId: null,
            cessAmount: null,
            cessPercent: null,
            sgstTaxRate: null,
            sgstTaxAmount: null,
            cgstTaxRate: null,
            cgstTaxAmount: null,
            igstTaxRate: null,
            igstTaxAmount: null,
            companyId: null,
            discountAmount: null,
            discountPercent: null,
            dueAmount: null,
            financialYearId: null,
            internalReferenceDate: null,
            invoiceTypeId: null,
            isReverseCharge: null,
            partyId: null,
            partyName: null,
            invoiceItems: null,
            taxId: null,
            taxAmount: null,
            timeOfInvoice: null,
            vendorCode: null,
            statusId: null,
            statusName: null,
            roundOffDecimal: null,
            remarks: null,
            inclusiveTax: null,
            deliveryChallanDateString: null,
            // ................
            companyName: null,
            companyGstRegistrationTypeId: null,
            companyPinCode: null,
            companyStateId: null,
            companyStateName: null,
            companyCountryId: null,
            companyPrimaryMobile: null,
            companySecondaryMobile: null,
            companyContactPersonNumber: null,
            companyContactPersonName: null,
            companyPrimaryTelephone: null,
            companySecondaryTelephone: null,
            companyWebsite: null,
            companyEmail: null,
            companyFaxNumber: null,
            companyAddress: null,
            companyTagLine: null,
            companyGstNumber: null,
            companyPanNumber: null,
            companyPanDate: null,
            companyCeritificateImagePath: null,
            companyLogoPath: null,
            // partyName: null,
            partyContactPersonNumber: null,
            partyPinCode: null,
            partyAreaId: null,
            partyCityId: null,
            partyStateId: null,
            partyCountryId: null,
            partyPrimaryTelephone: null,
            partySecondaryTelephone: null,
            partyPrimaryMobile: null,
            partySecondaryMobile: null,
            partyEmail: null,
            partyWebsite: null,
            partyContactPersonName: null,
            partyBillToAddress: null,
            partyShipAddress: null,
            partyDueDaysLimit: null,
            partyGstRegistrationTypeId: null,
            partyGstNumber: null,
            partyPanNumber: null,
            isIgst: null,
            isReused: null,
            reusedInvoiceId: null,
            partyCode: null,
            transactionStatus: null,
            tcsPercentage: null,
            tcsAmount: null,
            createdBy:null,
            updateBy:null,
            createdDate:null,
            updatedDate:null,
            materialNoteType:null,
            currencyId:null,
            currencyName:null,//currencyName
            currencyRate:null,
          //  exportAmtInWords:null,
            totalAmountCurrency:null,
            finalDestination:null,
            vesselNumber:null,
            shipper:null,
            cityOfLoading:null,
            cityOfDischarge:null,
            countryOfOriginOfGoods:null,
            countryOfDestination:null,
            
        };
        if (this.isReused === 1) {

            data.invoiceNumber = this.invoiceNumber;
            data.invId = this.invoiceId;
            data.reusedInvoiceId = this.invoiceHeaderId;
            data.isReused = 1;
        }
        this.tranForm = this.toFormGroup(data, this.itemOfNewInvoice);

    }

    private toFormGroup(data: InvoiceHeader, formSource: string): FormGroup {

        const itemArr = new FormArray([]);//<FormArray>this.tranForm.controls['invoiceItems'];
        if (data.invoiceItems) {
            data.invoiceItems.forEach(item => {
                let isOutMaterial: boolean = this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE ? true : false;
                let tItem: TransactionItem = this.transactionItemService_in.mapItem(item, formSource, isOutMaterial);
                itemArr.push(this.transactionItemService_in.makeItem(tItem));
                //this.makeItem(tItem)
            });
        }
        const formGroup = this.fb.group({
            id: [data.id],
            invoiceDate: [this.datePipe.transform(data.invoiceDate, AppSettings.DATE_FORMAT), [Validators.required]],
            gstNumber: [data.gstNumber],
            invoiceNumber: [data.invoiceNumber],
            purchaseOrderDate: [this.datePipe.transform(data.purchaseOrderDate, AppSettings.DATE_FORMAT)],
            partyId: [data.partyId, [Validators.required]],
            partyName: [data.partyName, [Validators.required]],
            purchaseOrderNumber: [data.purchaseOrderNumber],
            referenceDate: [this.datePipe.transform(data.referenceDate, AppSettings.DATE_FORMAT)],
            // address: [data.address, [Validators.required]],
            address: [data.address],
            internalReferenceNumber: [data.internalReferenceNumber],
            deliveryChallanDate: [this.datePipe.transform(data.deliveryChallanDate, AppSettings.DATE_FORMAT)],
            deliveryChallanNumber: [data.deliveryChallanNumber ? data.deliveryChallanNumber.split(',') : null],
            proformaInvoiceNumber: [data.proformaInvoiceNumber],
            proformaInvoiceDate: [this.datePipe.transform(data.proformaInvoiceDate, AppSettings.DATE_FORMAT)],
            sourceInvoiceNumber: [data.sourceInvoiceNumber],
            sourceInvoiceDate: [this.datePipe.transform(data.sourceInvoiceDate, AppSettings.DATE_FORMAT)],
            grnNumber: [data.grnNumber],
            grnDate: [this.datePipe.transform(data.grnDate, AppSettings.DATE_FORMAT)],
            stateId: [data.stateId],
            stateName: [data.stateName],
            //currencyName:[data.currencyName],
            paymentDueDate: [this.datePipe.transform(data.paymentDueDate, AppSettings.DATE_FORMAT)],
            eWayBillNumber: [data.eWayBillNumber],
            totalIgstTax: [data.totalIgstTax],
            totalCgstTax: [data.totalCgstTax],
            grandTotal: [data.grandTotal ,[Validators.min(0.01)]],
            billToAddress: [data.billToAddress],
            labourChargesOnly: [data.labourChargesOnly],
            subTotalAmount: [data.subTotalAmount],
            paymentTerms: [data.paymentTerms],
            deliveryTerms: [data.deliveryTerms],
            termsAndConditions: [data.termsAndConditions],
            modeOfDispatch: [data.modeOfDispatch],
            vehicleNumber: [data.vehicleNumber],
            numOfPackages: [data.numOfPackages],
            documentThrough: [data.documentThrough],
            shipToAddress: [data.shipToAddress],
            totalDiscount: [data.totalDiscount ? data.totalDiscount.toFixed(2) : data.totalDiscount],
            discountPercent: [data.discountPercent, [Validators.max(100)]],
            totalTaxableAmount: [data.totalTaxableAmount ? data.totalTaxableAmount.toFixed(2) : data.totalTaxableAmount],
            netAmount: [data.netAmount],
            transportationCharges: [data.transportationCharges],
            advanceAmount: [data.advanceAmount],
            isRoudedOff: [data.isRoudedOff],
            roundOffAmount: [data.roundOffAmount],
            cessAmount: [data.cessAmount],
            cessPercent: [data.cessPercent],
            sgstTaxRate: [data.sgstTaxRate],
            sgstTaxAmount: [data.sgstTaxAmount],
            cgstTaxRate: [data.cgstTaxRate],
            cgstTaxAmount: [data.cgstTaxAmount],
            igstTaxRate: [data.igstTaxRate],
            igstTaxAmount: [data.igstTaxAmount],
            companyId: [data.companyId],
            discountAmount: [data.discountAmount],
            dueAmount: [data.dueAmount],
            financialYearId: [data.financialYearId],
            internalReferenceDate: [this.datePipe.transform(data.internalReferenceDate, AppSettings.DATE_FORMAT)],
            invoiceTypeId: [data.invoiceTypeId],
            isReverseCharge: [data.isReverseCharge],
            //partyId: [data.partyId],
            taxId: [data.taxId],
            taxAmount: [data.taxAmount],
            timeOfInvoice: [data.timeOfInvoice],
            vendorCode: [data.vendorCode],
            statusId: [data.statusId],
            statusName: [data.statusName],
            invId: [data.invId],

            roundOffDecimal: [data.roundOffDecimal],
            remarks: [data.remarks],
            inclusiveTax: [data.inclusiveTax],
            deliveryChallanDateString: [data.deliveryChallanDateString],
            //invoiceItems : this.putItems(data),//itemArr,//this.fb.array(data.invoiceItems.forEach(item => this.makeItem(item)), Validators.required),
            invoiceItems: itemArr,//this.putItems(data),//this.fb.array(data.invoiceItems.forEach(item => this.putItemIntoFormGroup(item)), Validators.required),

            // .............................
            companyName: [data.companyName],
            companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
            companyPinCode: [data.companyPinCode],
            companyStateId: [data.companyStateId],
            companyStateName: [data.companyStateName],
            companyCountryId: [data.companyCountryId],
            companyPrimaryMobile: [data.companyPrimaryMobile],
            companySecondaryMobile: [data.companySecondaryMobile],
            companyContactPersonNumber: [data.companyContactPersonNumber],
            companyContactPersonName: [data.companyContactPersonName],
            companyPrimaryTelephone: [data.companyPrimaryTelephone],
            companySecondaryTelephone: [data.companySecondaryTelephone],
            companyWebsite: [data.companyWebsite],
            companyEmail: [data.companyEmail],
            companyFaxNumber: [data.companyFaxNumber],
            companyAddress: [data.companyAddress],
            companyTagLine: [data.companyTagLine],
            companyGstNumber: [data.companyGstNumber],
            companyPanNumber: [data.companyPanNumber],
            companyPanDate: [data.companyPanDate],
            companyCeritificateImagePath: [data.companyCeritificateImagePath],
            companyLogoPath: [data.companyLogoPath],
            // partyName: [data.companyName],
            partyContactPersonNumber: [data.partyContactPersonNumber],
            partyPinCode: [data.partyPinCode],
            partyAreaId: [data.partyAreaId],
            partyCityId: [data.partyCityId],
            partyStateId: [data.partyStateId],
            
            partyCountryId: [data.partyCountryId],
            partyPrimaryTelephone: [data.partyPrimaryTelephone],
            partySecondaryTelephone: [data.partySecondaryTelephone],
            partyPrimaryMobile: [data.partyPrimaryMobile],
            partySecondaryMobile: [data.partySecondaryMobile],
            partyEmail: [data.partyEmail],
            partyWebsite: [data.partyWebsite],
            partyContactPersonName: [data.partyContactPersonName],
            partyBillToAddress: [data.partyBillToAddress],
            partyShipAddress: [data.partyShipAddress],
            partyDueDaysLimit: [data.partyDueDaysLimit],
            partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
            partyGstNumber: [data.partyGstNumber],
            partyPanNumber: [data.partyPanNumber],
            isIgst: [data.isIgst],
            isReused: [data.isReused],
            replacedInvoiceId: [data.reusedInvoiceId],
            partyCode: [data.partyCode],
            tcsPercentage: [data.tcsPercentage],
            tcsAmount: [data.tcsAmount],
            createdBy:[data.createdBy],
            updateBy:[data.updateBy],
            createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
            updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],
            materialNoteType:[data.materialNoteType],
            currencyId:[data.currencyId],
          //  exportAmtInWords :[data.exportAmtInWords],
            currencyRate :[data.currencyRate],
            totalAmountCurrency:[data.totalAmountCurrency],
            finalDestination:[data.finalDestination],
            vesselNumber:[data.vesselNumber],
            shipper:[data.shipper],
            cityOfLoading:[data.cityOfLoading],
            cityOfDischarge:[data.cityOfDischarge],
            countryOfOriginOfGoods:[data.countryOfOriginOfGoods],
            countryOfDestination:[data.countryOfDestination],
            currencyName:[data.currencyName]//currencyName
        });

        formGroup.get('invoiceItems').setValidators(Validators.required);

        // this.fieldValidatorLabels.set("invoiceNumber", "Invoice Number");
        this.fieldValidatorLabels.set("partyName", "Party Name");
        this.fieldValidatorLabels.set("invoiceDate", "Invoice Date");
        this.fieldValidatorLabels.set("invoiceItems", "Invoice Items");
        // this.fieldValidatorLabels.set("address", "Address");
        this.fieldValidatorLabels.set("materialId", "Material");
       this.fieldValidatorLabels.set("price", "Price");
        this.fieldValidatorLabels.set("quantity", "Quantity");
        this.fieldValidatorLabels.set("amount", "Amount");



        return formGroup;
    }


    // checkInvNumAvailability() {
    //     let invoiceNumber: string = this.tranForm.get('invoiceNumber').value
    //     if (invoiceNumber && invoiceNumber.length > 0 && (this.transactionType.name === AppSettings.PURCHASE_INVOICE || this.transactionType.name === AppSettings.PROFORMA_INVOICE || this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name === AppSettings.CREDIT_NOTE)) {

    //         this.invoiceService_in.checkInvNumAvailability(invoiceNumber, this.transactionType.id).subscribe(response => {

    ////             //console.log("is invoiceNumber avialable? ", response);
    //             if (response.responseStatus === 1) {
    ////                 //console.log("Yes");
    //             } else {
    ////                 //console.log("No");
    //                 this.alertService.danger("Invoice Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
    //                 this.tranForm.controls['invoiceNumber'].patchValue('');
    //                 this.invoiceNumberElement.nativeElement.focus();

    //             }

    //         });
    //     }
    // }




    onPartyChange(_event: any, party: Party) {
        if (_event.isUserInput) {
          
            if (party) {
                super.onPartyChange(_event, party, this.tranForm.controls['partyId'], <FormArray>this.tranForm.controls['invoiceItems']);

                this.tranForm.controls['deliveryChallanNumber'].patchValue("");
                this.tranForm.controls['purchaseOrderNumber'].patchValue("");
                this.tranForm.controls['grnNumber'].patchValue("");
                this.tranForm.controls['proformaInvoiceNumber'].patchValue("");
                this.tranForm.controls['grnDate'].patchValue("");
                this.tranForm.controls['purchaseOrderDate'].patchValue("");
                this.tranForm.controls['deliveryChallanDate'].patchValue("");
                this.tranForm.controls['proformaInvoiceDate'].patchValue("");
                
                if (this.selectedParty.id !== undefined) {
                    
                    this.tranForm.controls['billToAddress'].patchValue(this.selectedParty.address);
                    this.tranForm.controls['shipToAddress'].patchValue(this.selectedParty.billAddress);

                    this.tranForm.controls['gstNumber'].patchValue(this.selectedParty.gstNumber);
                    this.tranForm.controls['stateName'].patchValue(this.selectedParty.stateName);
                    if(this.selectedParty.countryId!=1)
                    {
                        this.tranForm.controls['currencyId'].patchValue(this.selectedParty.partyCurrencyId);
                        this.tranForm.controls['currencyName'].patchValue(this.selectedParty.partyCurrencyName);
                     
                       this.taxNumberText="CIN Number"; 

                    }else{
                        this.tranForm.controls['currencyId'].patchValue(0);
                      
                        this.taxNumberText="CIN Number";   
                    }
                   /*  if(this.selectedParty.partyCurrencyId!=null)
                    {

                    
                    this.partyService.getCurrency(this.selectedParty.partyCurrencyId).subscribe(currency => {
                        this.tranForm.controls['currencyName'].patchValue(currency.currencyName);
                        this.currencyDecimal=currency.currencyDecimal;
                        this.currencyFraction=currency.currencyFraction;

                    });
                } */
                    
                    

                  
                    // ................
                    if (!this.isNote) {
                        this.getPurchaseOrdersForAParty(this.selectedParty.id);
                        this.getProformaInvoicesForAParty(this.selectedParty.id);
                        this.getDeliveryChallansForAParty(this.selectedParty.id);
                        this.getGrnsForAParty(this.selectedParty.id);
                       
                    }
                    else {
                        this.getInvoicesForAParty(this.selectedParty.id)
                       
                    }
 
                }
                
                this.getPaymentDueDate(party);
                if(this.transactionType.name===AppSettings.CUSTOMER_INVOICE ||this.transactionType.name===AppSettings.PROFORMA_INVOICE ||this.transactionType.name===AppSettings.CREDIT_NOTE && this.selectedParty.partyCurrencyId!=null){
                    this.currency = this.currencys.filter(cur => cur.id === this.selectedParty.partyCurrencyId)[0];
                     
                }
            }
        }
       this.taxDisablecChanges();

    }
    taxDisablecChanges(){
          
        if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==0)//headerlevel
        {
            this.isheaderTaxDisable=true;//headerlevel
        }
        if(this.selectedParty.countryId==1&& this.globalSetting.itemLevelTax==0)
        { 
           this.isheaderTaxDisable=false;
        }
     
        if(this.selectedParty.countryId!=1 && this.globalSetting.itemLevelTax==1)//itemlevel
         {
         
           this.isItemLevelTax=false;
           this.isheaderTaxDisable=true
           
         }
         if(this.selectedParty.countryId==1 && this.globalSetting.itemLevelTax==1)
         {
            this.isItemLevelTax=true;
         }

    }
    getPaymentDueDate(party: Party) {

        if (this.tranForm.controls['invoiceDate'].value) {

            let dp = new DatePipe(navigator.language);
            //let dtr = dp.transform(new Date(), 'yyyy-MM-dd');
            ////console.log("party date limit", party.dueDaysLimit)
            let invoiceDate = new Date(this.tranForm.controls['invoiceDate'].value);
            ////console.log("invoiceDate: ", invoiceDate);
            let paymentDueDate = invoiceDate.setDate(invoiceDate.getDate() + party.dueDaysLimit);
            ////console.log("paymentDueDate: ", paymentDueDate);
            let dtr = dp.transform(paymentDueDate, AppSettings.DATE_FORMAT);

            ////console.log(dtr);
            this.tranForm.controls['paymentDueDate'].patchValue(dtr);
            //let paymentDueDate
        }
    }

    getInvoicesForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let invType: string;
                if (this.isNote) {
                    //invType = AppSettings.TXN_NOTE; //'Customer PO';
                    this.spinStart = true;
                    ////console.log("invType" + invType)
                    let sourceTransactionType: TransactionType;
                    this.transactionTypeService_in.getAllTransactionTypes().subscribe(response => {
                        response.forEach(tt => {

                            if (this.transactionType.name === AppSettings.CREDIT_NOTE || this.transactionType.name === AppSettings.DEBIT_NOTE || this.transactionType.name === AppSettings.INCOMING_JOBWORK_CREDIT_NOTE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_DEBIT_NOTE) {
                                if (tt.name == AppSettings.CUSTOMER_INVOICE && this.transactionType.name === AppSettings.CREDIT_NOTE) {
                                    sourceTransactionType = tt;
                                } else if (tt.name == AppSettings.PURCHASE_INVOICE && this.transactionType.name === AppSettings.DEBIT_NOTE) {
                                    sourceTransactionType = tt;
                                }
                                else if(tt.name == AppSettings.INCOMING_JOBWORK_INVOICE && this.transactionType.name === AppSettings.INCOMING_JOBWORK_CREDIT_NOTE){
                                    sourceTransactionType = tt;
                                }
                                else if(tt.name == AppSettings.OUTGOING_JOBWORK_INVOICE && this.transactionType.name === AppSettings.OUTGOING_JOBWORK_DEBIT_NOTE){
                                    sourceTransactionType = tt;
                                }


                            }

                        });

                        this.invoiceService_in.getInvoicesByPartyAndTransactionTypeForNote(partyId, sourceTransactionType.name).subscribe(
                            response => {
                                ////console.log("inside");
                                this.sourceInvoices = response;
                                this.spinStart = false;
                            }
                        )
                       // this.calculateGrandtotal();  
                    })

                }
            }
        }
    }

    onSourceInvoiceNumberChange() {
        let srcInvNumber: string = this.tranForm.controls['sourceInvoiceNumber'].value;
        
        if (srcInvNumber) {
            ////console.log(srcInvNumber);

            let si: InvoiceHeader = this.sourceInvoices.filter(si => si.invoiceNumber === srcInvNumber)[0];
            ////console.log("Source I", si)
            this.populateItemsWithSourceInvoice(si);
            //let dcNumber: string[] = (<string>pi.deliveryChallanNumber).split(',');

            //////console.log('onchange: ', dcNumber);

            this.selectedSourceInvoice = si;
            this.tranForm.controls['sourceInvoiceDate'].patchValue(this.datePipe.transform(si.invoiceDate, AppSettings.DATE_FORMAT));
            //this.tranForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(pi.deliveryChallanDate, AppSettings.DATE_FORMAT));
            this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(si.purchaseOrderDate, AppSettings.DATE_FORMAT));
            this.tranForm.controls['purchaseOrderNumber'].patchValue(si.purchaseOrderNumber);
            //this.tranForm.controls['deliveryChallanNumber'].patchValue(dcNumber);
            this.tranForm.controls['eWayBillNumber'].patchValue(si.eWayBillNumber);
            this.tranForm.controls['vehicleNumber'].patchValue(si.vehicleNumber);
            this.tranForm.controls['modeOfDispatch'].patchValue(si.modeOfDispatch);
            this.tranForm.controls['numOfPackages'].patchValue(si.numOfPackages);
            // this.tranForm.controls['billToAddress'].patchValue(si.billToAddress);
            this.tranForm.controls['documentThrough'].patchValue(si.documentThrough);
            this.tranForm.controls['shipToAddress'].patchValue(si.shipToAddress);
            this.tranForm.controls['tcsPercentage'].patchValue(si.tcsPercentage);
            this.tranForm.controls['tcsAmount'].patchValue(si.tcsAmount);
            if(si.partyCountryId!=1)
            {   this.tranForm.controls['currencyId'].patchValue(si.currencyId);
             this.tranForm.controls['currencyName'].patchValue(si.currencyName);

                this.tranForm.controls['currencyRate'].patchValue(si.currencyRate);
              
               

            }
            else{ 
                this.tranForm.controls['currencyId'].patchValue(0);
                this.tranForm.controls['currencyRate'].patchValue(0); 
            }

           
            this.isInclusiveTax = si.inclusiveTax === 1 ? true : false;
            this.inclusiveTaxDisable = true;
            
        }
       
    }

    populateItemsWithSourceInvoice(sourceInvoice: InvoiceHeader) {
        if (sourceInvoice) {
            const control = <FormArray>this.tranForm.controls['invoiceItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }

            //Reset DC number if any
            //this.invoiceForm.controls['deliveryChallanNumber'].patchValue(null);
            //this.tranForm.controls['deliveryChallanNumber'].reset(null, { emitEvent: false });
            //this.tranForm.controls['deliveryChallanDate'].reset(null, { emitEvent: false });

            ////console.log("inside populateItemsWithSourceInvoice : ", sourceInvoice);
            if (sourceInvoice.purchaseOrderNumber) {

                this.tranForm.controls['purchaseOrderNumber'].patchValue(sourceInvoice.purchaseOrderNumber, { emitEvent: false });

            }
            if (sourceInvoice.purchaseOrderDate) {
                this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(sourceInvoice.purchaseOrderDate, AppSettings.DATE_FORMAT), { emitEvent: false });
            }
            // if (sourceInvoice.deliveryChallanNumber) {

            //     // this.invoiceForm.controls['deliveryChallanNumber'].patchValue(sourceInvoice.deliveryChallanNumber, { emitEvent: false });

            // }
            // if (sourceInvoice.deliveryChallanDate) {
            //     this.tranForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(sourceInvoice.deliveryChallanDate, AppSettings.DATE_FORMAT), { emitEvent: false });
            // }
            this.headerTax = this.taxes.filter(tax => tax.id === sourceInvoice.taxId)[0];
            sourceInvoice.invoiceItems.forEach(invItem => {
                //see if it is IGST PO
                if (invItem.igstTaxPercentage != undefined && invItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                // if (invItem.invoiceBalanceQty != 0) {
                let tItem: TransactionItem = this.transactionItemService_in.mapItem(invItem, this.itemOfSourceInvoice);
                control.push(this.transactionItemService_in.makeItem(tItem));
                // }


            });

            if (!this.isItemLevelTax) {
                this.tranForm.controls["discountPercent"].patchValue(sourceInvoice.discountPercent);
                this.tranForm.controls["totalDiscount"].patchValue(sourceInvoice.totalDiscount);

                setTimeout(() => {
                    this.headerDiscountPerChange();
                }, 500);
            }
        }
    }

    getGrnsForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let gnrType: string;
                if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
                    gnrType = AppSettings.GRN_TYPE_SUPPLIER; //'Customer PO';
                    this.spinStart = true;

                    this.grnService_in.getGrnsForParty(partyId, gnrType, this.transactionType.name).subscribe(
                        response => {
                            this.grns = response;
                            this.spinStart = false;
                            ////console.log("grns: ", response);
                        },
                        error => {
                            this.spinStart = false;
                        }
                    )
                }
            }
        }
    }

    getPurchaseOrdersForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let poType: string;
                if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name === AppSettings.PROFORMA_INVOICE) {
                    poType = AppSettings.CUSTOMER_PO; //'Customer PO';
                } else if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.INCOMING_JOBWORK_PROFORMA_INVOICE) {
                    poType = AppSettings.INCOMING_JOBWORK_PO;
                }
                else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE) {
                    poType = AppSettings.OUTGOING_JOBWORK_PO;
                }
                else {
                    poType = AppSettings.SUPPLIER_PO; //'Supplier PO';
                }
                this.spinStart = true;
                this.purchaseOrderService_in.getPurchaseOrdersForParty(partyId, poType, this.transactionType.name).subscribe(
                    response => {
                        this.purchaseOrders = response;
                        this.spinStart = false;
                        ////console.log("PO: ", response);
                        this.getFillteredPO();
                    },
                    error => {
                        this.spinStart = false;
                    }
                )
            }
        }
    }


    getFillteredPO() {
        this.filteredPO = this.tranForm.get('purchaseOrderNumber').valueChanges
            .pipe(
                startWith(''),
                map(value => this._poFilter(value))
            );
    }


    _poFilter(value: string): PurchaseOrderHeader[] {
        // const filterValue = value.toLowerCase();
        const filterValue = value ? value.toLowerCase() : '';
        if (!value) {
            const control = <FormArray>this.tranForm.controls['invoiceItems'];
            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }
            this.tranForm.get("purchaseOrderDate").setValue(null);
            this.tranForm.controls['purchaseOrderDate'].enable();
        }
        return this.purchaseOrders.filter(po => po.purchaseOrderNumber.toLowerCase().includes(filterValue))
    }


    getFillteredDC() {
        this.filteredDC = this.tranForm.get('deliveryChallanNumber').valueChanges
            .pipe(
                startWith(''),
                map(value => this._dcFilter(value))
            );
    }

    _dcFilter(value: string): DeliveryChallansSelect[] {
        const filterValue = value ? value.toLowerCase() : '';
        //const filterValue = value.toLowerCase();

        return this.deliveryChallansSelect.filter(dc => dc.deliveryChallanNumber.toLowerCase().includes(filterValue))
    }








    getProformaInvoicesForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let invType: string;
                if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name == AppSettings.PROFORMA_INVOICE) {
                    invType = AppSettings.PROFORMA_INVOICE; //'Customer PO';
                    this.spinStart = true;
                    ////console.log("invType" + invType)
                    this.invoiceService_in.getProformaInvoicesForParty(partyId, invType).subscribe(
                        response => {
                            this.proformaInvoices = response;
                            this.spinStart = false;
                            ////console.log("proformaInvoices: ", response);
                        },
                        error => {
                            this.spinStart = false;
                        }
                    )
                }
            }
        }
    }

    getDeliveryChallansForAParty(partyId: number) {
        if (partyId) {
            if (partyId !== undefined) {

                let dcType: string;
                if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name == AppSettings.PROFORMA_INVOICE) {
                    dcType = AppSettings.CUSTOMER_DC;
                }
                else if (this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.INCOMING_JOBWORK_PROFORMA_INVOICE) {
                    dcType = AppSettings.INCOMING_JOBWORK_OUT_DC
                }
                else if (this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE) {
                    dcType = AppSettings.OUTGOING_JOBWORK_IN_DC
                }
                else {
                    dcType = AppSettings.INCOMING_DC;
                }
                this.spinStart = true;
                this.deliveryChallanService_in.getDeliveryChallansForPartyAndTransactionType(partyId, dcType, this.transactionType.name).subscribe(
                    response => {
                        this.deliveryChallans = response;
                        this.deliveryChallansSelect = response.map(dc => { return new DeliveryChallansSelect(dc.deliveryChallanNumber, dc.id, false) });
                        this.getFillteredDC();
                        this.spinStart = false;
                        ////console.log("DC: ", response);
                        this.getFillteredDC();
                    },
                    error => {
                        this.spinStart = false;
                    }
                )





            }
        }
    }

    dcOptionClicked(_event: any, dc: DeliveryChallansSelect) {
        _event.stopPropagation();
        this.toggleDcSelection(dc);
    }

    toggleDcSelection(dc: DeliveryChallansSelect) {
        dc.selected = !dc.selected;
        if (dc.selected) {
            this.selectedDeliveryChallansSelect.push(dc);
            //console.log(dc.id.)
        } else {
            const i = this.selectedDeliveryChallansSelect.findIndex(value => value.deliveryChallanNumber === dc.deliveryChallanNumber);
            this.selectedDeliveryChallansSelect.splice(i, 1);
        }

        let selectedDcs: string[] = [];
        this.selectedDeliveryChallansSelect.forEach(sd => {
            selectedDcs.push(sd.deliveryChallanNumber);
        })

        this.tranForm.controls['deliveryChallanNumber'].patchValue(selectedDcs, { emitEvent: false });
        this.onDCChange(null);
    }

    onPurchaseOrderNumberChange(_event: any, po: PurchaseOrderHeader, isInternal: boolean = false) {
        if (isInternal || _event.isUserInput) {
            if (po) {
                // let poNumber: string = this.tranForm.controls['purchaseOrderNumber'].value;
                // if (poNumber) {
                ////console.log(poNumber);

                // let po: PurchaseOrderHeader = this.purchaseOrders.filter(po => po.purchaseOrderNumber === poNumber)[0];

                this.populateItemsWithPo(po);
                this.selectedPurchaseOrder = po;
                this.tranForm.controls['purchaseOrderDate'].disable();
                this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(po.purchaseOrderDate, AppSettings.DATE_FORMAT));
                this.tranForm.controls['internalReferenceNumber'].patchValue(po.internalReferenceNumber);
                this.tranForm.controls['internalReferenceDate'].patchValue(this.datePipe.transform(po.internalReferenceDate, AppSettings.DATE_FORMAT));
                this.tranForm.controls['advanceAmount'].patchValue(po.advanceAmount);
                this.tranForm.controls['isReverseCharge'].patchValue(po.isReverseCharge);
                this.tranForm.controls['roundOffAmount'].patchValue(po.roundOffAmount);
                this.isInclusiveTax = po.inclusiveTax === 1 ? true : false;
                this.inclusiveTaxDisable = true; //po.inclusiveTax === 1 ? true : false;

                this.isIgst = this.partyService.getIgstApplicable(this.selectedParty);
                ////console.log(" this.isIgst " + this.isIgst);
                // this.tranForm.controls['isIgst'].patchValue(this.isIgst ? 1 : 0);
                // }
            }
            else {
                this.tranForm.controls['purchaseOrderDate'].enable();

            }
        }
    }

    onGrnChange(event: any) {

        if (this.tranForm.controls['grnNumber']) {
            let grnNumber: string = this.tranForm.controls['grnNumber'].value;
            if (grnNumber) {
                ////console.log("grnNumber" + grnNumber);

                let grn: GrnHeader = this.grns.filter(grn => grn.grnNumber === grnNumber)[0];
                this.selectedGrn = grn;
                /// let dc: DeliveryChallanHeader = this.deliveryChallans.filter(dc => dc.deliveryChallanNumber === grn.deliveryChallanNumber)[0];
                // let po: PurchaseOrderHeader = this.purchaseOrders.filter(po => po.id === grn.purchaseOrderHeaderId)[0];


                let dcNumber: string[];

                if (grn.deliveryChallanNumber != null) {
                    dcNumber = (<string>grn.deliveryChallanNumber).split(',');
                }
                ////console.log('onchange: ', dcNumber);

                // this.selectedDeliveryChallans.push(dc);
                this.purchaseOrders = [];
                this.deliveryChallans = [];
                this.tranForm.controls['grnDate'].patchValue(this.datePipe.transform(grn.grnDate, AppSettings.DATE_FORMAT));
                this.tranForm.controls['deliveryChallanNumber'].patchValue(dcNumber);
                this.tranForm.controls['deliveryChallanDateString'].patchValue(this.datePipe.transform(grn.deliveryChallanDate, AppSettings.DATE_FORMAT));
                this.tranForm.controls['purchaseOrderNumber'].patchValue(grn.purchaseOrderNumber);
                this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(grn.purchaseOrderDate, AppSettings.DATE_FORMAT));

                //   if (grn.purchaseOrderNumber!=null) {
                //     this.purchaseOrders = [];
                //      this.tranForm.controls['purchaseOrderNumber'].patchValue(grn.purchaseOrderNumber, { emitEvent: false });
                //     this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(grn.purchaseOrderDate, AppSettings.DATE_FORMAT), { emitEvent: false });

                // }
                this.populateItemsWithGrn(grn);

            }
        }

    }


    populateItemsWithPo(pos: PurchaseOrderHeader) {

        if (pos) {
            const control = <FormArray>this.tranForm.controls['invoiceItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }

            //Reset DC number if any
            //this.tranForm.controls['deliveryChallanNumber'].patchValue(null);
            this.tranForm.controls['deliveryChallanNumber'].reset(null, { emitEvent: false });
            this.tranForm.controls['deliveryChallanDate'].reset(null, { emitEvent: false });

            ////console.log("inside populateItemsWithPo : ", pos);

            this.headerTax = this.taxes.filter(tax => tax.id === pos.taxId)[0];
            pos.purchaseOrderItems.forEach(poItem => {
                //see if it is IGST PO
                if (poItem.igstTaxPercentage != undefined && poItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                if (poItem.invoiceBalanceQuantity != 0) {
                    let tItem: TransactionItem = this.transactionItemService_in.mapItem(poItem, this.itemOfPO);
                    control.push(this.transactionItemService_in.makeItem(tItem));
                }


            });
            if (!this.isItemLevelTax) {
                this.tranForm.controls["discountPercent"].patchValue(pos.discountPercent);
                this.tranForm.controls["totalDiscount"].patchValue(pos.totalDiscount);

                setTimeout(() => {
                    this.headerDiscountPerChange();
                }, 500);
            }
        }

    }

    populateItemsWithGrn(grns: GrnHeader) {

        if (grns) {
            const control = <FormArray>this.tranForm.controls['invoiceItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }


            ////console.log("inside populateItemsWithgn : ", grns, grns.grnItems[0].purchaseOrderHeaderId);

            this.headerTax = this.taxes.filter(tax => tax.id === grns.taxId)[0];
            //     if(grns.grnItems[0].purchaseOrderHeaderId!= null){
            //         let po: PurchaseOrderHeader = this.purchaseOrders.filter(po => po.id === grns.grnItems[0].purchaseOrderHeaderId)[0];
            ////   //console.log("po in grn",po);
            //     }

            grns.grnItems.forEach(grnItem => {
                //see if it is IGST PO
                if (grnItem.igstTaxPercentage != undefined && grnItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                ////console.log(" populateItemsWithgn this.isIgst", this.isIgst)
                if (grnItem.invoiceBalanceQuantity != 0) {
                    let tItem: TransactionItem = this.transactionItemService_in.mapItem(grnItem, this.itemOfGRN);
                    control.push(this.transactionItemService_in.makeItem(tItem));
                }


            });
            // if (!this.isItemLevelTax) {
            //     this.tranForm.controls["discountPercent"].patchValue(pos.discountPercent);
            //     this.tranForm.controls["totalDiscount"].patchValue(pos.totalDiscount);
            // }
        }

    }




    validateAndSubmit(model: InvoiceHeader) {
     // console.log("this.exportAmtInWords"+this.tranForm.get('exportAmtInWords').value);      

  
       if((this.transactionType.name === AppSettings.INCOMING_JOBWORK_CREDIT_NOTE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_DEBIT_NOTE) && model.materialNoteType==null ){
        this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false
        });
        this.alertRef.componentInstance.alertMessage = "Please Select Material Note Type";
        return;
    } 

     


        if (this.numberRangeConfiguration.id === null) {

            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });
            this.alertRef.componentInstance.alertMessage = "please add Transaction Type in Transaction Numbering";
            return;
        }

        if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
            let poNumber: string = this.tranForm.get('purchaseOrderNumber').value
            let poDate: string = this.tranForm.get('purchaseOrderDate').value

            let dcNumber: string = this.tranForm.get('deliveryChallanNumber').value
            let dcDate: string = this.tranForm.get('deliveryChallanDateString').value
            

            if (poDate && !poNumber) {
                this.alertService.danger("Please Enter PO Number");
                return;
            }
            

            if (dcDate && !dcNumber) {
                this.alertService.danger("Please Enter DC Number");
                return;

            }

        }
          if(this.transactionType.name===AppSettings.CUSTOMER_INVOICE||this.transactionType.name===AppSettings.PROFORMA_INVOICE||this.transactionType.name===AppSettings.CREDIT_NOTE)
          {   // let currencyRate = this.tranForm.get('currencyRate').value;
           
              if(this.selectedParty.countryId!=1 && model.currencyRate==null)
              {
                 this.alertService.danger("Please Enter Exchange Rate");
                 return;
              
              }
          }
        let materialStockCheckDTOWrapper: MaterialStockCheckDTOWrapper = {
            materialStockCheckDTOs: []
        };


        if ((this.isCust || this.transactionType.name === AppSettings.DEBIT_NOTE) && this.isStockCheckRequired && this.supplyTypeControl.value === 1 && !this.isJW && this.transactionType.name != AppSettings.CREDIT_NOTE) {
            model.invoiceItems.forEach(item => {
                let materialStockCheckDTO: MaterialStockCheckDTO = {
                    id: item.materialId,
                    itemQuantity: item.quantity,
                    deleted: 'N',
                    name: null,
                    partNumber: null,
                    minimumStock: null,

                };

                materialStockCheckDTOWrapper.materialStockCheckDTOs.push(materialStockCheckDTO)

            })

            this.materialService_in.minStockValidate(materialStockCheckDTOWrapper)
                .subscribe(response => {
                    ////console.log("is  stock alert? ", response);

                    if (response.materialStockCheckDTOs.length > 0) {
                        let materialsName: string[] = response.materialStockCheckDTOs.map(m => m.name);
                        this.dialogRef = this.dialog.open(ConfirmationDialog, {
                            disableClose: false
                        });

                        this.dialogRef.componentInstance.confirmMessage = materialsName + "," + AppSettings.MINIMUM_STOCK_CONFIRMATION_MESSAGE
                        return this.dialogRef.afterClosed().subscribe(result => {
                            if (!result) {
                                return;
                            }
                            else {
                                this.checkTcs(model);
                            }
                        })

                    }
                    else {
                        this.checkTcs(model);
                    }
                })
        }
        else {
            this.checkTcs(model);
        }
    // this.disableSelect==true;
    }

    checkTcs(model: InvoiceHeader) {
        if (!model.tcsAmount && !model.tcsPercentage && this.preTcsAmount && this.preTcsPercentage) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.TCS_CONFIRM;
            return this.dialogRef.afterClosed().subscribe(result => {
                if (!result) {
                    return;
                }
                else {
                    this.onSubmit(model);
                }
            })

        }
        else {
            this.onSubmit(model);
        }




    }


    onSubmit(model: InvoiceHeader) {
        // if(model.id){
        // this.getForRecuring();
        // }
        //console.log("model :", model);
        let invoiceNumber: string = this.tranForm.get('invoiceNumber').value
        ////console.log("invoiceNumber :", invoiceNumber);

        if (model.invoiceNumber == null && !this.isAutoNumber || model.invoiceNumber == "") {
            this.alertService.danger("Please Enter Transaction Number");
            return;
        }
        if (!this.validateTnCNewLines(model.termsAndConditions)) {
            return;
        }
        if (model.tcsPercentage && !model.tcsAmount) {
            this.alertService.danger("Please Enter Tcs Amount");
            return;
        }

        if (!model.tcsPercentage && model.tcsAmount) {
            this.alertService.danger("Please Enter Tcs Percentage");
            return;
        }


        ////console.log('In onSubmit: ', model);
        ////console.log('Grand total: ', model.grandTotal);

        ////console.log("model.id :", model.id);
        ////console.log("invoiceNumber :", invoiceNumber);
        ////console.log("this.invoiceHeader.invoiceNumber :", this.invoiceHeader);
        ////console.log("this.isAutoNumber :", this.isAutoNumber);
        // if(this.isRecuringInvoice===1){
        let checkNumAvail: boolean = false;
        //check number avail      
        if (this.isAutoNumber) {
            //in case of auto number do not check
            checkNumAvail = false;
        }
        else {
            if (model.id != null) { //if we are editing invoice which is not auto number
                if (model.invoiceNumber != this.invoiceHeader.invoiceNumber) { // if invoice number is changed
                    checkNumAvail = true;
                } else {
                    checkNumAvail = false;
                }
            }
            else { // if not auto number and creating new invoice, always check

                checkNumAvail = true;
            }
        }

        if (checkNumAvail && this.isReused!=1) {
            this.invoiceService_in.checkInvNumAvailability(invoiceNumber, this.transactionType.id, this.selectedParty.id).subscribe(response => {

                if (response.responseStatus === 1) {
                    let textToBeAppend: string = this.priceCheck(model.invoiceItems)
                    if (textToBeAppend.length > 0) {

                        this.dialogRef = this.dialog.open(ConfirmationDialog, {
                            disableClose: false
                        });
                        this.dialogRef.componentInstance.confirmMessage = textToBeAppend + AppSettings.PRICE_CONFIRMATION_MESSAGE;
                        this.dialogRef.afterClosed().subscribe(result => {

                            if (result) {
                                this.getForRecuring(model);
                            }

                        });


                    }
                    else {
                        this.getForRecuring(model);
                    }
                } else {
                    ////console.log("No");
                    this.alertService.danger("Invoice Number " + AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE);
                    this.tranForm.controls['invoiceNumber'].patchValue('');
                    this.invoiceNumberElement.nativeElement.focus();

                }

            });
        }
        else {
            this.getForRecuring(model);
        }



        // }

    }

    patchDcHeaderNumber(model: InvoiceHeader) {

        let dcIDs: string[] = model.invoiceItems.map(item => item.deliveryChallanHeaderId);
        let grnId: string[] = model.invoiceItems.map(item => item.grnHeaderId);
        let poNumber: string = "";
        let dcNumber: string[] = [];
        ////console.log("dcIDs: " + dcIDs, this.selectedDeliveryChallans.length);
        ////console.log("grnId: " + grnId);
        dcIDs = Array.from(new Set(dcIDs));

        ////console.log("distinct dcIDs: " + dcIDs);

        if (dcIDs != null && this.selectedDeliveryChallans.length != 0) {


            dcNumber = this.selectedDeliveryChallans
                .filter(selDC => dcIDs.includes(selDC.id))
                .map(selDC => selDC.deliveryChallanNumber);

            ////console.log("dcNumber: " + dcNumber);

            let poNumbers: string[] = [];

            poNumbers = this.selectedDeliveryChallans
                .filter(selDC => dcIDs.includes(selDC.id))
                .map(selDC => selDC.purchaseOrderNumber);

            ////console.log("poNumber: " + poNumbers);


            poNumbers.forEach(po => {
                poNumber += po;
                ////console.log("poNumber");
            })


            try {
                model.deliveryChallanNumber = dcNumber.join(",");
            } catch (e) {
                model.deliveryChallanNumber = this.tranForm.controls['deliveryChallanNumber'].value;
                model.purchaseOrderNumber = this.tranForm.controls['purchaseOrderNumber'].value;
            }
            finally {
                ////console.log("model.deliveryChallanNumber", model.deliveryChallanNumber, "model.po", model.purchaseOrderNumber + poNumber)
                this.tranForm.controls['deliveryChallanNumber'].patchValue(dcNumber, { emitEvent: false });
                this.tranForm.controls['purchaseOrderNumber'].patchValue(poNumber, { emitEvent: false });
            }
        }
    }

    // patchDcHeaderNumber(model:InvoiceHeader)
    // {
    ////     //console.log("in patch"+model);
    //     let dcIDs: string[] =[];
    //     let dc : DeliveryChallanHeader;
    //     model.invoiceItems.forEach(item =>{
    ////         //console.log("dcid "+item+dcIDs)
    //         dcIDs.push(item.deliveryChallanHeaderId);

    //     })


    //   //  let dcNumber: string[] = this.tranForm.controls['deliveryChallanNumber'].value;
    //   let dcNumber: string[] = [];
    ////     //console.log('in save: dcNumber: ', dcNumber);

    //     dcIDs.forEach(
    //         id=>{
    ////             //console.log("dcIDs"+id);
    //             dc= this.selectedDeliveryChallans.find(selDc => selDc.id === id)
    //             if(dc!=undefined && dc!= null)
    //             {
    ////             //console.log("ddddc"+dc)
    //             dcNumber.push(dc.deliveryChallanNumber);
    //             }
    //     })
    //     try {
    //         model.deliveryChallanNumber = dcNumber.join(",");
    //     } catch (e) {
    //         model.deliveryChallanNumber = this.tranForm.controls['deliveryChallanNumber'].value;
    //     }
    //     finally{
    ////         //console.log("model.deliveryChallanNumber"+model.deliveryChallanNumber)
    //         this.tranForm.controls['deliveryChallanNumber'].patchValue(dcNumber);
    //     }


    // }

    save(model: InvoiceHeader) {
        model.partyName = this.displayPartyName;
        if (this.invoiceHeaderId) {
            model.reusedInvoiceId = this.invoiceHeaderId;
        }


        //console.log('igst: '+this.tranForm.controls['isIgst'].value)
        ////console.log("model : ", model);

        if (model.id === null) {
            model = this.transactionItemService_in.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
            ////console.log("model :", model);
        }


        if (!this.tranForm.valid) {

            const controls = this.tranForm;
            let invalidFieldList: string[] = this.utilityService_in.findInvalidControlsRecursive(controls);
            ////console.log("invalidFieldList ", invalidFieldList)

            let invalidFiledLabels: string[] = [];

            invalidFieldList.forEach(field => {
                if (this.fieldValidatorLabels.get(field))
                    invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
            })

            ////console.log("invalidFiledLabels ", invalidFiledLabels)

            this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
            return false;
        }

        // this.spinStart = true;

        model.labourChargesOnly = model.labourChargesOnly ? 1 : 0;
        model.isReverseCharge = model.isReverseCharge ? 1 : 0;
        model.partyId = model.partyId ? model.partyId : this.selectedParty.id; //override id to replace whole cusotmer object
        model.companyId = 1; //hack....change it later
        model.financialYearId = this.financialYear.id;

        //model.partyId = this.selectedCustomer.id;
        //  model.taxId = model.invoiceItems[0].taxId; // hack
        model.taxId = this.headerTax ? this.headerTax.id : model.invoiceItems[0].taxId; // hack
        if (this.headerTax == null && !this.isItemLevelTax && model.partyCountryId==1 ) {
            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "Please select tax";
            return;
        }
        this.spinStart = true;
        model.invoiceTypeId = this.transactionType.id;
        model.inclusiveTax = this.isInclusiveTax ? 1 : 0;
        //model.purchaseOrderNumber = this.selectedPurchaseOrder?.purchaseOrderNumber;
        // this.patchDcHeaderNumber(model);
        let dcNumber: string[] = this.tranForm.controls['deliveryChallanNumber'].value;

        ////console.log('in save: dcNumber: ', dcNumber);
        try {
            model.deliveryChallanNumber = dcNumber.join(",");
        } catch (e) {
            model.deliveryChallanNumber = this.tranForm.controls['deliveryChallanNumber'].value;
        }

       // console.log('Before save: ', model);
        model.invoiceDate = this.datePipe.transform(model.invoiceDate, 'yyyy-MM-dd HH:mm:ss');
        // update due amount as grand total value from model
        model.dueAmount = model.grandTotal;

        //Assign Serial numbers to the items
        model.invoiceItems.forEach((item, index) => {
            item.slNo = index + 1;
        })
        model.taxAmount = model.cgstTaxAmount + model.sgstTaxAmount + model.igstTaxAmount;
        ////console.log('Before save2 : ', model);
        ////console.log("itemToBeRemove " + this.itemToBeRemove)

        // if (this.itemToBeRemove.length > 0) {
        ////     //console.log(" in itemToBeRemove " + this.itemToBeRemove)
        //     this.itemToBeRemove.forEach(itemId =>
        //         this.invoiceService_in.deleteItem(itemId).subscribe(response => {
        ////             //console.log("item id before save on deltion " + itemId);
        //             if (response.responseStatus) {
        ////                 //console.log("Item deleted in DB: " + itemId);
        //                 // (<FormArray>this.quotationForm.get('quotationItems')).removeAt(idx);
        //             } else {
        //                 console.error("Cannot delete: ", response.responseString);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }
        //             return false;

        //         },
        //             error => {
        //                 console.error("Cannot delete: ", error);
        //                 this.alertRef = this.dialog.open(AlertDialog, {
        //                     disableClose: false
        //                 });

        //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
        //             }))
        // }
        let message: string;
        if (model.id === null) {
            message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        }
        else {
            message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
        }
        ////console.log("insave")
        let invoiceWrapper: InvoiceWrapper = { itemToBeRemove: this.itemToBeRemove, invoiceHeader: model };
        invoiceWrapper.invoiceHeader.shipToAddress = this.tranForm.get('shipToAddress').value


        this.invoiceService_in.create(invoiceWrapper)
            .subscribe(response => {
                this.showTheCreatedBy=true;
                ////console.log('after save: ', response);
                ////console.log("saved id: ", response.id);
                this.invoiceHeader = response;
                this.displayPartyName = this.invoiceHeader.partyName;
                this.displayParty = this.invoiceHeader.partyName;

                this.invoiceHeader.partyName = this.invoiceHeader.partyCode + "-" + this.invoiceHeader.partyName;
                 this.preTcsAmount=this.invoiceHeader.tcsAmount
                 this.preTcsPercentage=this.invoiceHeader.tcsPercentage
                // this.tranForm.controls['invoiceNumber'].patchValue(response.invoiceNumber);
                // this.tranForm.controls['id'].patchValue(response.id);
                // this.tranForm.controls['statusId'].patchValue(response.statusId);
                // this.tranForm.controls['statusName'].patchValue(response.statusName);
                this.isPartyChangeable = false;
                this.tranForm = this.toFormGroup(this.invoiceHeader, this.itemOfRecentInvoice);
                ////console.log("response.statusName", response.statusName, response.isReverseCharge);
                if(!response.updateBy){
                    this.invoiceService_in.getInvoice(response.id)
                    .subscribe(responses => {
                        this.tranForm.controls['updateBy'].patchValue(responses.updateBy);
                        this.tranForm.controls['updatedDate'].patchValue(this.datePipe.transform(responses.updatedDate,AppSettings.DATE_FORMAT));
          
                           
          
                    });
                }
                this.transactionStatus = this.transactionType.name + " " + response.invoiceNumber + message;
                this.itemToBeRemove = [];
                this.saveStatus = true;
                this.sourceInvoices = [];
                ////console.log("model.statusName ", model.statusName)
                if (response.statusName === "Cancelled") {
                    ////console.log("model.statusName in cncel", model.statusName)
                    this.alertService.success(this.transactionType.name + " Cancelled Successfully");
                }
                else {
                    this.alertService.success(this.transactionStatus);
                    // this.alertService.success("Invoice Cancelled Successfully");
                }
                this.recentInvoiceLoadCounter++;
                this.isCreateButtonenabled();

                this.spinStart = false;
                this.isReused = 0;

                ////console.log('save() : ', this.tranForm.disabled);
                this.setStatusBasedPermission(response.statusId);
                this.disableForm();
                if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE && this.invoiceHeader != null && this.invoiceHeader.id != null) {
                    this.allowAutoTransaction = true;


                }

                if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE && this.invoiceHeader != null && this.invoiceHeader.id != null && this.globalSetting.allowReuse === 1 && this.invoiceHeader.statusName === AppSettings.STATUS_NEW) {
                    this.allowReuse = true;


                }
            },
                error => {
                    ////console.log("Error ", error);
                    this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
                    this.alertService.danger(this.transactionStatus);
                    this.saveStatus = false;
                    this.spinStart = false;
                }
            );


    }

    recentInvoiceGet(event, isProforma?: boolean, isReplicate?: boolean) {
        this.showTheCreatedBy=false;
        if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE && this.isAutoNumber == true) {

            this.allowAutoTransaction = true;

        }
        if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE && event.statusName === AppSettings.STATUS_NEW && this.globalSetting.allowReuse === 1) {
            this.allowReuse = true;
        }
        else {
            this.allowReuse = false;
        }
        this.disableSelect=true;
        this.invoiceHeader = event;
        this.preTcsAmount=this.invoiceHeader.tcsAmount;
        this.preTcsPercentage =this.invoiceHeader.tcsPercentage
        //this.tranForm = this.toFormGroup(this.invoiceHeader, this.itemOfRecentInvoice);
        // this.isInclusiveTax = this.invoiceHeader.invoiceItems[0].inclusiveTax === 0 ? false : true;
        this.isInclusiveTax = this.invoiceHeader.inclusiveTax === 1 ? true : false;
        this.inclusiveTaxDisable = this.isInclusiveTax ? true : false;
        this.tranForm.controls['isReverseCharge'].patchValue(this.invoiceHeader.isReverseCharge === 0 ? false : true);
        ////console.log("this.isInclusiveTax: ", this.isInclusiveTax);
        ////console.log('list of pos: ', this.purchaseOrders);
        this.inclusiveTaxDisable = true;
        console.log("inclusiveTaxDisable: " + this.inclusiveTaxDisable);
        this.partyService.getCustomer(this.invoiceHeader.partyId).subscribe(party => {

            this.selectedParty = party;
            this.isIgst = this.invoiceHeader.isIgst > 0 ? true : false;//this.partyService.getIgstApplicable(this.selectedParty);
            ////console.log("recent this.isIgst "+this.isIgst);
            ////console.log("recent this.isIgst ", this.isIgst);
            this.displayPartyName = this.invoiceHeader.partyName;
            this.displayParty = this.invoiceHeader.partyName
            if (!isProforma) {
                this.invoiceHeader.partyName = this.invoiceHeader.partyCode + "-" + this.invoiceHeader.partyName;
            }
            if(this.invoiceHeader.partyCountryId!=1)
            {
                this.taxNumberText="CIN Number"
            }
            else{
                this.taxNumberText="GST Number"
            }
            this.tranForm = this.toFormGroup(this.invoiceHeader, this.itemOfRecentInvoice);
            this.isCreateButtonenabled();
            this.setStatusBasedPermission(this.tranForm.controls['statusId'].value);
            this.itemsControl = this.tranForm.controls['invoiceItems'];
            this.partyFormControl = this.tranForm.controls['partyName'];
            console.log("inclusiveTaxDisable 1.5: " + this.inclusiveTaxDisable);
            if (isProforma) {
                this.edit();
              this.tranForm.controls['proformaInvoiceNumber'].patchValue(event.invoiceNumber);
               this.tranForm.controls['id'].patchValue(null);
                this.tranForm.controls['invoiceNumber'].patchValue(null);
                this.tranForm.controls['invId'].patchValue(null);
                this.patchFromProformaInvoice(event);
                console.log("inclusiveTaxDisable 2: " + this.inclusiveTaxDisable);
                this.inclusiveTaxDisable = true;
            } else if (isReplicate) {
                this.edit();
            }
            else {
                this.disableForm();
            }

        });
        let poFound: boolean = false;
        console.log("inclusiveTaxDisable 3: " + this.inclusiveTaxDisable);
        this.purchaseOrders.forEach(poh => {
            ''
            if (poh.purchaseOrderNumber === this.invoiceHeader.purchaseOrderNumber) {
                poFound = true;
            }
        });

        if (!poFound) {
            this.tranForm.controls['purchaseOrderNumber'].patchValue(this.invoiceHeader.purchaseOrderNumber);

        }




        this.headerTax = this.taxes.filter(tax => tax.id === this.invoiceHeader.taxId)[0];
        this.isPartyChangeable = false;
        this.getFiltredMaterial();


    }




    clearForm() {

        this.formDirective.resetForm();

        //this.initForm();
        //formDirective.resetForm();
        //this.tranForm.reset();
        this.initForm();
        super.clearFormParent();
        this.isCreateButtonenabled();
        this.selectedDeliveryChallans = [];
        this.selectedDeliveryChallansSelect = [];
        this.sourceInvoices = [];
        this.deliveryChallans = [];
        this.proformaInvoices = [];
        this.grns = [];
        this.purchaseOrders = [];
        this.selectedPurchaseOrder = null;
        this.selectedSourceInvoice = null;
        this.itemsControl = this.tranForm.controls['invoiceItems'];
        this.handleChanges();
        this.headerTax = null;
        this.tranForm.markAsPristine();
        this.itemDisableMessage = "";
        this.prevDCNumbers = [];
        this.allowAutoTransaction = false;
        this.allowReuse = false;
        this.preTcsAmount=null;
        this.preTcsPercentage=null;
        this.disableSelect=false;
        //this.invoiceHeader.billToAddress = null;
    }


    getAddress() {
        if (this.invoiceHeader && this.invoiceHeader.id) {
            return this.invoiceHeader.billToAddress;
        } else {
            return this.selectedParty ? this.selectedParty.address : ''
        }
    }
    // ...................................................
    onDCChange(event: any) {
        //// //console.log("event: ", event);
        // if (event.isUserInput) {


        let dcNumber: string[] = this.tranForm.controls['deliveryChallanNumber'].value;
        ////console.log('dcNumber: ', dcNumber);
        let dcDate: string = '';
        //Check for new addition
        if (dcNumber.length > this.prevDCNumbers.length) {
            dcNumber.forEach(dcn => {
                if (!(this.prevDCNumbers.length > 0 && this.prevDCNumbers.includes(dcn))) {
                    ////console.log('Added: ', dcn);
                    this.deliveryChallans.forEach(dc => {

                        if (dcn === dc.deliveryChallanNumber) {
                            if (!this.selectedDeliveryChallans.find(selDc => selDc.deliveryChallanNumber === dcn)) {
                                //this.selectedDeliveryChallans.push(dc);
                                ////console.log('populateItemsWithDc: ', dc);

                                ////console.log('onchange: ', dcNumber);
                                this.populateItemsWithDc(dc);
                                this.selectedDeliveryChallans.push(dc);
                                this.patchEWayAndVehicleNumber(dc);
                                this.patchNumOfPackagesAndModeOfDispatch(dc);
                                // this.tranForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(dc.deliveryChallanDate, AppSettings.DATE_FORMAT));
                                // this.tranForm.controls['numOfPackages'].patchValue(dc.numberOfPackages);
                                // this.tranForm.controls['internalReferenceNumber'].patchValue(dc.internalReferenceNumber);
                                // this.tranForm.controls['referenceDate'].patchValue(this.datePipe.transform(dc.internalReferenceDate, AppSettings.DATE_FORMAT));
                                // this.tranForm.controls['modeOfDispatch'].patchValue(dc.modeOfDispatch);
                                this.tranForm.controls['internalReferenceNumber'].patchValue(dc.internalReferenceNumber);
                                this.tranForm.controls['internalReferenceDate'].patchValue(this.datePipe.transform(dc.internalReferenceDate, AppSettings.DATE_FORMAT));
                                console.log('invpoice2.0: ', dc.inclusiveTax);
                                this.isInclusiveTax = dc.inclusiveTax === 1 ? true : false;
                                this.inclusiveTaxDisable = true;
                            }
                        }


                    })
                }
            })
        }

        //Check for removals
        if (this.prevDCNumbers.length > dcNumber.length) {
            this.prevDCNumbers.forEach(preDcn => {
                if (!(dcNumber.length > 0 && dcNumber.includes(preDcn))) {
                    ////console.log('Removed: ', preDcn);

                    let control = <FormArray>this.tranForm.controls['invoiceItems'];
                    ////console.log('control.length: ', control.length);
                    let loopIndex: number = 0;

                    let uncheckedDc: DeliveryChallanHeader;

                    uncheckedDc = this.selectedDeliveryChallans.find(sDc => sDc.deliveryChallanNumber === preDcn);

                    ////console.log('uncheckedDc: ', uncheckedDc);
                    this.patchEWayAndVehicleNumber(uncheckedDc, true);
                    while (loopIndex < control.length) {
                        ////console.log('dc in item: ', control.at(loopIndex));
                        ////console.log('loopindex, dc in item: ', loopIndex, control.at(loopIndex).get('deliveryChallanHeaderId') ? control.at(loopIndex).get('deliveryChallanHeaderId').value : null);
                        //let itemId: string = (<FormArray>this.invoiceForm.get('invoiceItems')).at(idx).get("id").value;
                        if (control.at(loopIndex).get('deliveryChallanHeaderId').value === uncheckedDc.id) {
                            this.removeInvoiceItem(loopIndex);
                            loopIndex = 0;
                            //////console.log('need to remove this: ',control.at(loopIndex).get("id"));
                            //////console.log('control.length inside loop: ', control
                        } else {
                            loopIndex++
                        }


                    }

                    let idx: number = this.selectedDeliveryChallans.indexOf(uncheckedDc);

                    this.selectedDeliveryChallans.splice(idx, 1);
                }
            })
        }

        ////console.log('at the end, this.selectedDeliveryChallans: ', this.selectedDeliveryChallans);
        this.prevDCNumbers = dcNumber;
        this.selectedDeliveryChallans.forEach(dc => {
            dcDate += this.datePipe.transform(dc.deliveryChallanDate, AppSettings.DATE_FORMAT_DISPLAY) + ","
        })
        dcDate = dcDate.slice(0, -1);
        this.tranForm.controls['deliveryChallanDateString'].patchValue(dcDate);
        // //Disable Items if populated from DC
        // this.tranForm.controls['invoiceItems'].disable();
        // this.itemDisableMessage = "Items cannot be changed for DC tracked Invoice"

        //}

    }

    patchNumOfPackagesAndModeOfDispatch(dc: DeliveryChallanHeader, isdeleted?: boolean) {
        let numOfPackages: string[] = [];
        let modeOfDispatch: string[] = [];

        numOfPackages = this.tranForm.controls['numOfPackages'].value ? [this.tranForm.controls['numOfPackages'].value] : [];
        modeOfDispatch = this.tranForm.controls['modeOfDispatch'].value ? [this.tranForm.controls['modeOfDispatch'].value] : [];

        if (isdeleted) {
            let idx: number = numOfPackages.indexOf(dc.numberOfPackages);
            let index: number = modeOfDispatch.indexOf(dc.modeOfDispatch);
            numOfPackages.splice(idx, 1)
            modeOfDispatch.splice(index, 1)
        }
        else {

            if (dc.numberOfPackages != null) {
                numOfPackages.push(dc.numberOfPackages);

            }
            if (dc.modeOfDispatch != null) {
                modeOfDispatch.push(dc.modeOfDispatch);

            }
        }


        let numOfPack = numOfPackages.toString();
        let modOfDisp = modeOfDispatch.toString();

        this.tranForm.controls['numOfPackages'].patchValue(numOfPack);

        this.tranForm.controls['modeOfDispatch'].patchValue(modOfDisp);


    }

    patchEWayAndVehicleNumber(dc: DeliveryChallanHeader, isdeleted?: boolean) {
        let eWayBillNumbers: string[] = [];
        let vehicleNumbers: string[] = [];

        eWayBillNumbers = this.tranForm.controls['eWayBillNumber'].value ? [this.tranForm.controls['eWayBillNumber'].value] : [];
        vehicleNumbers = this.tranForm.controls['vehicleNumber'].value ? [this.tranForm.controls['vehicleNumber'].value] : [];
        ;

        if (isdeleted) {
            let idx: number = eWayBillNumbers.indexOf(dc.eWayBill);
            let index: number = vehicleNumbers.indexOf(dc.vehicleNumber);

            eWayBillNumbers.splice(idx, 1);
            vehicleNumbers.splice(index, 1);

        }
        else {
            if (dc.eWayBill != null) {
                eWayBillNumbers.push(dc.eWayBill);

            }
            if (dc.vehicleNumber != null) {
                vehicleNumbers.push(dc.vehicleNumber);

            }

        }
        ////console.log("eWayBillNumber", eWayBillNumbers);
        // if (eWayBillNumber != null) {
        //     eWayBillNumber+=",";
        // }
        ////console.log("after , ", eWayBillNumbers);
        // eWayBillNumber =eWayBillNumber.substr(0, eWayBillNumber.length - 1)
        ////console.log("aftereWayBillNumber", eWayBillNumbers.toString());
        let eway = eWayBillNumbers.toString();
        let vehicle = vehicleNumbers.toString();


        ////console.log("eway", eway);
        this.tranForm.controls['eWayBillNumber'].patchValue(eway);

        this.tranForm.controls['vehicleNumber'].patchValue(vehicle);

    }



    populateItemsWithDc(dc: DeliveryChallanHeader) {
        const control = <FormArray>this.tranForm.controls['invoiceItems'];

        let i: number = control.length;
        //No need to Clear any pre populated items in DC because multiple DC selection is a valid scenario
        while (i !== 0) {
            if (!control.at(0).get('deliveryChallanHeaderId').value)
                control.removeAt(0);
            i--;
        }


        //Reset PO number if any

        let prevPoNumber: string = this.tranForm.controls['purchaseOrderNumber'].value;
        ////console.log("prevPoNumber: ", prevPoNumber);

        this.tranForm.controls['purchaseOrderNumber'].reset(null, { emitEvent: false });
        this.tranForm.controls['purchaseOrderDate'].reset(null, { emitEvent: false });

        //Populate PO number and date if DC has them
        ////console.log("dc.purchaseOrderNumber", dc.purchaseOrderNumber)
        if (dc.purchaseOrderNumber) {
            this.purchaseOrders = [];

            if (prevPoNumber && prevPoNumber != dc.purchaseOrderNumber) {
                this.tranForm.controls['purchaseOrderNumber'].patchValue(prevPoNumber + "," + dc.purchaseOrderNumber, { emitEvent: false });
            } else {
                this.tranForm.controls['purchaseOrderNumber'].patchValue(dc.purchaseOrderNumber, { emitEvent: false });
            }


        }
        if (dc.purchaseOrderDate) {
            this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(dc.purchaseOrderDate, AppSettings.DATE_FORMAT), { emitEvent: false });
        }

        this.headerTax = this.taxes.filter(tax => tax.id === dc.taxId)[0];
        dc.deliveryChallanItems
            .filter(dcItem => dcItem.isContainer != 1)
            .forEach(dcItem => {
                //see if it is IGST DC
                ////console.log("dcItem.igstTaxRate: ", dcItem.igstTaxPercentage);
                if (this.isItemLevelTax) {
                    if (dcItem.igstTaxPercentage != undefined && dcItem.igstTaxPercentage != 0) {
                        this.isIgst = true;
                    } else {
                        this.isIgst = false;
                    }
                }
                let isOutMaterial: boolean = this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE ? true : false;
                let tItem: TransactionItem = this.transactionItemService_in.mapItem(dcItem, this.itemOfDC, isOutMaterial);
                control.push(this.transactionItemService_in.makeItem(tItem));
            })
        //Recreate serial numbers in case multiple DC added

        let j: number = 0;
        while (j < control.length) {
            control.at(j).get('slNo').patchValue(j + 1)
            j++;
        }


    }

    removeInvoiceItem(idx: number) {
        this.removeItem(idx, 'invoiceItems', this.initFormItem, this.invoiceHeader ? this.invoiceHeader.taxId : null);
        this.patchDcHeaderNumber(this.tranForm.value);
        this.tranForm.controls['tcsPercentage'].patchValue(0);
        this.tranForm.controls['tcsAmount'].patchValue(0);
        //this.handleChangesParent(this.initFormItem, this.invoiceHeader ? this.invoiceHeader.taxId : null);
    }

    quantityChange(event: QuantityChangeEvent) {
        console.log("quantityChange ")
        this.onQuantityChange(event, 'invoiceItems');
        this.tranForm.controls['tcsPercentage'].patchValue(0);
        this.tranForm.controls['tcsAmount'].patchValue(0);
    }

    delete(formDirective: FormGroupDirective) {


      


        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.spinStart = true;
                let id: string = this.tranForm.controls['id'].value;//this.poHeader.id;
                //console.log("deleteing...----24-12-2021" + id);
                this.invoiceService_in.delete(id)
                    .subscribe(response => {
                 //   console.log(response+"24-12-2021");
                        this.transactionStatus = response.responseString;
                        this.saveStatus = true;
                        this.recentInvoiceLoadCounter++;
                        this.spinStart = false;
                        if (response.responseStatus === 1) {
                            this.alertService.success(this.transactionStatus);
                            this.clearForm();
                           
                        }
                        else
                            this.alertService.danger(this.transactionStatus);
                          


                    },
                        error => {
                            ////console.log("Error ", error);
                            this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
                            this.saveStatus = false;
                            this.spinStart = false;
                            this.alertService.danger(this.transactionStatus);
                        });
            }
            this.dialogRef = null;
        });
    }

    cancel() {
        let id: string = this.tranForm.controls['id'].value;
             this.invoiceService_in.getAutoTransactionInvoiceForCancel(id) .subscribe(response => {
              if(response.responseStatus===1){
                this.alertService.danger(response.responseString);
                return;
              }
else{
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = AppSettings.INVOICE_CANCEL_CONFIRMATION_MESSAGE;
    this.dialogRef.afterClosed().subscribe(result => {

        if (result) {
            let model: InvoiceHeader = this.tranForm.value;
            ////console.log('cancel()2 : ', this.tranForm.disabled);
            ////console.log('trans type: ', this.transactionType);
            ////console.log('invoiceStatuses: ', this.invoiceStatuses);
            //Set status as cancel
            model.statusId = this.invoiceStatuses.find(is => (is.name === AppSettings.INVOICE_CANCEL_STATUS && is.transactionTypeId === this.transactionType.id)).id;
            ////console.log('model.statusId: ', model.statusId);
            model.transactionStatus = AppSettings.INVOICE_CANCEL_STATUS;
            this.save(model);
        }

    });
}



             });
        ////console.log('cancel()1 : ', this.tranForm.disabled);
       
    }

    onProformaInvoiceNumberChange() {
        let proInvNumber: string = this.tranForm.controls['proformaInvoiceNumber'].value;
        if (proInvNumber) {
            ////console.log(proInvNumber);

            let pi: InvoiceHeader = this.proformaInvoices.filter(pi => pi.invoiceNumber === proInvNumber)[0];
            this.patchFromProformaInvoice(pi);

        }
    }

    patchFromProformaInvoice(pi: InvoiceHeader) {
        let dcNumber: string[];
        this.populateItemsWithProformaInvoice(pi);
        if (pi.deliveryChallanNumber != null) {
            dcNumber = (<string>pi.deliveryChallanNumber).split(',');
        }
        ////console.log('onchange: ', dcNumber);

        this.selectedProformaInvoice = pi;
        this.tranForm.controls['proformaInvoiceDate'].patchValue(this.datePipe.transform(pi.invoiceDate, AppSettings.DATE_FORMAT));
        this.tranForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(pi.deliveryChallanDate, AppSettings.DATE_FORMAT));
        this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(pi.purchaseOrderDate, AppSettings.DATE_FORMAT));
        this.tranForm.controls['purchaseOrderNumber'].patchValue(pi.purchaseOrderNumber, { emitEvent: false });
        this.tranForm.controls['deliveryChallanNumber'].patchValue(dcNumber, { emitEvent: false });
        this.tranForm.controls['eWayBillNumber'].patchValue(pi.eWayBillNumber);
        this.tranForm.controls['vehicleNumber'].patchValue(pi.vehicleNumber);
        this.tranForm.controls['modeOfDispatch'].patchValue(pi.modeOfDispatch);
        this.tranForm.controls['numOfPackages'].patchValue(pi.numOfPackages);
        this.tranForm.controls['billToAddress'].patchValue(pi.billToAddress);
        this.tranForm.controls['documentThrough'].patchValue(pi.documentThrough);
        this.tranForm.controls['shipToAddress'].patchValue(pi.shipToAddress);
        if(pi.partyCountryId!=1)
        {   
            this.tranForm.controls['currencyRate'].patchValue(pi.currencyRate);
            this.tranForm.controls['currencyId'].patchValue(pi.currencyId);
           // this.tranForm.controls['exportAmtInWords'].patchValue(pi.exportAmtInWords);

        }
        else{ this.tranForm.controls['currencyRate'].patchValue(0);
                 this.tranForm.controls['currencyId'].patchValue(0);  }
        this.spinStart = true;
        setTimeout(() => {            
            this.tranForm.controls['tcsPercentage'].patchValue(pi.tcsPercentage);
            this.tranForm.controls['tcsAmount'].patchValue(pi.tcsAmount); 
            this.tranForm.controls['grandTotal'].patchValue(pi.grandTotal);       
            this.spinStart = false;
        }, 1000);
        this.isInclusiveTax = pi.inclusiveTax === 1 ? true : false;
        this.inclusiveTaxDisable = true;
    }

    populateItemsWithProformaInvoice(pri: InvoiceHeader) {

        if (pri) {
            const control = <FormArray>this.tranForm.controls['invoiceItems'];

            //Clear any pre populated items exist
            while (control.length !== 0) {
                control.removeAt(0);
            }

            //Reset DC number if any
            //this.invoiceForm.controls['deliveryChallanNumber'].patchValue(null);
            this.tranForm.controls['deliveryChallanNumber'].reset(null, { emitEvent: false });
            this.tranForm.controls['deliveryChallanDate'].reset(null, { emitEvent: false });
            this.tranForm.controls['invoiceDate'].patchValue(this.defaultDate, AppSettings.DATE_FORMAT)

            ////console.log("inside populateItemsWithProformaInvoice : ", pri);
            if (pri.purchaseOrderNumber) {

                this.tranForm.controls['purchaseOrderNumber'].patchValue(pri.purchaseOrderNumber, { emitEvent: false });

            }
            if (pri.purchaseOrderDate) {
                this.tranForm.controls['purchaseOrderDate'].patchValue(this.datePipe.transform(pri.purchaseOrderDate, AppSettings.DATE_FORMAT), { emitEvent: false });
            }
            if (pri.deliveryChallanNumber) {

                // this.invoiceForm.controls['deliveryChallanNumber'].patchValue(pri.deliveryChallanNumber, { emitEvent: false });

            }
            if (pri.deliveryChallanDate) {
                this.tranForm.controls['deliveryChallanDate'].patchValue(this.datePipe.transform(pri.deliveryChallanDate, AppSettings.DATE_FORMAT), { emitEvent: false });
            }
            this.headerTax = this.taxes.filter(tax => tax.id === pri.taxId)[0];
            pri.invoiceItems.forEach(invItem => {
                //see if it is IGST PO
                if (invItem.igstTaxPercentage != undefined && invItem.igstTaxPercentage != 0) {
                    this.isIgst = true;
                } else {
                    this.isIgst = false;
                }
                // if (invItem.invoiceBalanceQty != 0) {isRoundOffDisabled
                let tItem: TransactionItem = this.transactionItemService_in.mapItem(invItem, this.itemOfProformaInvoice);
                control.push(this.transactionItemService_in.makeItem(tItem));
                // }


            });
          
        }

    }

    edit() {
        super.edit();
        this.itemsControl = this.tranForm.controls['invoiceItems'];
     //  this.currencyRate = +this.tranForm.controls['currencyRate'].value;
      //  console.log("======"+ this.currencyRate+"======"+this.invoiceHeader.currencyRate);
       //
      //  this.tranForm.controls['currencyrate'].get
   //  let currencyvalue = this.tranForm.get('currencyrate').value();
   //  console.log("currencyvalue in edit "+currencyvalue);
      // if(this.selectedParty.countryId!=1 && this.currencyRate==null)
      // {
      //    this.alertService.danger("Please Enter Currency Rate");
       //   return;
       
      // }
        this.handleChanges();
        this.disableSelect=true;
        this.taxDisablecChanges();
        
        ////console.log('dc number: ', this.tranForm.controls['deliveryChallanNumber'].value)
        // if (this.tranForm.controls['deliveryChallanNumber'].value) {
        //     this.tranForm.controls['invoiceItems'].disable();
        //     this.itemDisableMessage = "Items cannot be changed for DC tracked Invoice"
        // }
    }

    // public setLanguage(lang: string) {
    // this.translatorContainer.language = lang;
    // }

    isCreateButtonenabled() {
        ////  //console.log("this.transactionType.name " +this.transactionType.name + " statys " +this.tranForm.controls['statusId'].value+" ID " +this.tranForm.controls['id'].value+" Statusname "+this.tranForm.get('statusName').value + this.tranForm.controls['statusName'].value ) //&& this.grnForm.controls['statusName'].value == AppSettings.STATUS_NEW && this.tranForm.controls['statusId'].value == 68
        if (this.tranForm.controls['id'].value != null && (this.transactionType.name == AppSettings.PROFORMA_INVOICE||this.transactionType.name == AppSettings.INCOMING_JOBWORK_PROFORMA_INVOICE ) && this.tranForm.controls['statusName'].value == AppSettings.STATUS_NEW ) {
            this.isCreateEnabled = true;
        }
        else {
            this.isCreateEnabled = false;
        }
        if (this.tranForm.controls['id'].value != null && this.transactionType.name == AppSettings.CUSTOMER_INVOICE) {
            this.isCloneEnabled = true;
        }
        else {
            this.isCloneEnabled = false;
        }


    }
    // saveAsExcel() {

    //     // this.setPage({ offset: 0 });
    //     this.jasperPrintService.jasperInvoiceReportExcel(
    //      this.transactionType.id,this.invoiceHeader)
    //     .subscribe(response => {
    //             this.exportExcel(response);
    //         }, error => {
    //             //console.log("error: ", error)
    //     })
    //   }

    //   exportExcel(data){       
    //     let blob = data;
    //     let a = document.createElement("a");
    //     a.setAttribute('style', 'display: none');
    //     a.href = URL.createObjectURL(blob);
    //     a.download = this.transactionType.name+'.xlsx';
    //     document.body.appendChild(a);
    //     a.click();        
    //     a.remove();
    // }



    createProformaInvoice() {
        if (this.invoiceHeader && this.invoiceHeader.id) {
            this.transCommService.initInvoiceData();
            this.transCommService.updateInvoiceData(this.invoiceHeader);
        }

        this._router_in.navigate(['transaction/app-new-invoice', 1]);


    }

    createJobWorkProformaInvoice() {
        if (this.invoiceHeader && this.invoiceHeader.id) {
            this.transCommService.initInvoiceData();
            this.transCommService.updateInvoiceData(this.invoiceHeader);
        }

        this._router_in.navigate(['transaction/app-new-invoice', 28]);


    }


    replicateInvoice() {
        ////console.log("in replicate", this.invoiceHeader);
        if (this.invoiceHeader && this.invoiceHeader.id)
            this.invoiceHeader = this.replicateInvoiceObject(this.invoiceHeader)
        ////console.log("this.invoiceHeader", this.invoiceHeader)
        // this.transCommService.replicatedInvoiceHeader.next(this.invoiceHeader);
        this.transCommService.initRInvoiceData();
        this.transCommService.updateRInvoiceData(this.invoiceHeader);

        this._router_in.navigate(['transaction/app-new-invoice', 1]);
    }

    replicateInvoiceObject(invoiceHeader: InvoiceHeader) {
        invoiceHeader.invoiceDate = this.defaultDate;
        invoiceHeader.id = null;
        invoiceHeader.grnNumber = null;
        invoiceHeader.purchaseOrderNumber = null;
        invoiceHeader.deliveryChallanNumber = null;
        invoiceHeader.proformaInvoiceNumber = null;
        invoiceHeader.grnDate = null;
        invoiceHeader.purchaseOrderDate = null;
        invoiceHeader.deliveryChallanDate = null;
        invoiceHeader.proformaInvoiceDate = null;
        invoiceHeader.invoiceNumber = null;
        invoiceHeader.invId = null;
        invoiceHeader.statusId = null;
        invoiceHeader.stateName = null;
       
        invoiceHeader.currencyId=null;
        invoiceHeader.invoiceItems.forEach(item => {
            item.id = null;
            item.headerId = null;
            item.grnHeaderId = null;
            item.purchaseOrderHeaderId = null;
            item.deliveryChallanHeaderId = null;
            item.proformaInvoiceHeaderId = null;
            item.grnItemId = null;
            item.purchaseOrderItemId = null;
            item.deliveryChallanItemId = null;
            item.proformaInvoiceItemId = null;
        }
        )
        return invoiceHeader;
    }

    reuseInvoice() {
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.REUSE_INVOICE;

        return this.dialogRef.afterClosed().subscribe(result => {
            if (!result) {
                return;
            }
            else {
                this.isReused = 1;
                this.invoiceNumber = this.invoiceHeader.invoiceNumber
                this.invoiceId = this.invoiceHeader.invId;
                this.invoiceHeaderId = this.invoiceHeader.id
                this.clearForm();
            }
        })




    }
    // jasperPrint(){
    //     this.invoiceService_in.jasperPrint(this.invoiceHeader.id, null)
    //     .subscribe(response => {
    //         this.exportData(response);
    //     }, error => {
    ////         //console.log("error: ", error)
    //     })
    // }

    // jasperExcel(){
    //     this.invoiceService_in.jasperExcel(this.invoiceHeader.id)
    //     .subscribe(response => {
    //         this.exportExcel(response);
    //     }, error => {
    ////         //console.log("error: ", error)
    //     })
    // }

    // exportExcel(data){       
    //     let blob = data;
    //     let a = document.createElement("a");
    //     a.setAttribute('style', 'display: none');
    //     a.href = URL.createObjectURL(blob);
    //     a.download = 'fileName.xlsx';
    //     document.body.appendChild(a);
    //     a.click();        
    //     a.remove();
    // }

    // exportData(data){       
    //     let blob = data;
    //     let a = document.createElement("a");
    //     a.setAttribute('style', 'display: none');
    //     a.href = URL.createObjectURL(blob);
    //     a.download = 'fileName.pdf';
    //     document.body.appendChild(a);
    //     a.click();        
    //     a.remove();
    // }

    autoTransactionGeneration() {
        let status: string = "";
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.id = this.invoiceHeader.id;
        const dialogRef = this.dialog.open(AutoTransactionMasterComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {
                if (data) {
                    if (data.id === null) {
                        status = AppSettings.SAVE_SUCESSFULL_MESSAGE;
                    }
                    else {
                        status = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
                    }

                    let AutoTransactionGenerationList: AutoTransactionGenerationList = {
                        autoTransactionGenerationDTO: data

                    };
                    AutoTransactionGenerationList.autoTransactionGenerationDTO.startDate = this.datePipe.transform(AutoTransactionGenerationList.autoTransactionGenerationDTO.startDate, 'yyyy-MM-dd HH:mm:ss');

                    this.invoiceService_in.saveAutoTransactionDetail(AutoTransactionGenerationList).subscribe(response => {

                        if (response.autoTransactionGenerationDTO.id === 0) {

                            this.alertService.danger("Invoice IS alerdy EXISTS");
                        }
                        else {
                            console.log(response.autoTransactionGenerationDTO);

                            this.alertService.success(status);
                        }
                    });

                }
            });
    }


    validationForEditInvoice() {
        this.invoiceService_in.getAutoTransactionInvoice(this.invoiceHeader.id).subscribe(response => {
            response.responseStatus


        });


    }

    getForRecuring(model: InvoiceHeader) {

        this.invoiceService_in.getAutoTransactionInvoice(model.id).subscribe(response => {

            if (response && response.responseStatus === 1) {
                this.dialogRef = this.dialog.open(ConfirmationDialog, {
                    disableClose: false
                });

                this.dialogRef.componentInstance.confirmMessage = "changes will get affected only to newly generating recurring invoices. Old invoices will remain same";
                this.dialogRef.afterClosed().subscribe(result => {

                    if (result) {
                        this.save(model)
                    }
                    else {
                        return;
                    }

                });


            }
            else {
                this.save(model)
            }


        });

    }


    onTcsAmountChage() {
        let finalGrandTotal: number = 0;
        let dblTcsPerc:number=0;
        let tcsPercentage: number = this.tranForm.get('tcsPercentage').value;
        let tcsAmount: number = this.tranForm.get('tcsAmount').value;
        let taxableAmount: number = this.tranForm.get('totalTaxableAmount').value;
        let sgstTaxAmount: number = this.tranForm.get('sgstTaxAmount').value;
        let cgstTaxAmount: number = this.tranForm.get('cgstTaxAmount').value;
        let igstTaxAmount: number = this.tranForm.get('igstTaxAmount').value;
        if (igstTaxAmount) {
            finalGrandTotal = +taxableAmount + +igstTaxAmount;
        }
        else {
            finalGrandTotal = +taxableAmount + +sgstTaxAmount + +cgstTaxAmount;
        }
                if(tcsPercentage){
                      tcsAmount = ((taxableAmount) * tcsPercentage)/100;
                       this.tranForm.controls['tcsAmount'].patchValue(tcsAmount.toFixed(2));
          
                   }
           else{
                   dblTcsPerc = (tcsAmount *100)/taxableAmount;
                   this.tranForm.controls['tcsPercentage'].patchValue(dblTcsPerc.toFixed(2));
            
                 }
            





        let finalGrandTotalAmount: number = +finalGrandTotal + +tcsAmount;
        this.tranForm.controls['grandTotal'].patchValue(finalGrandTotalAmount);
        this.originalGrandTotal = this.tranForm.controls['grandTotal'].value;

        this.calculateGrandtotal();
    }
   
  //  numberInWords(value :number){
  //   this.amountInWords   = this.numberToWordsService.number2text(value);
      
  //   if(this.amountInWords.includes('RUPEES'))
   //  {
   //     this.ExportAmountInWords = this.amountInWords.replace('RUPEES',this.currencyFraction)
//
   //  }
   //  if(this.amountInWords.includes('PAISE'))
  //   {
  //      this.ExportAmountInWords = this.amountInWords.replace('PAISE',this.currencyDecimal)

   //  }
//
  //   this.tranForm.controls['exportAmtInWords'].patchValue(this.ExportAmountInWords);
  //  console.log(" amountInWords ="+this.amountInWords+"----"+this.currencyDecimal+"-----"+this.currencyFraction+"--------"+ this.ExportAmountInWords);
   // }
    onCurrencyChange(){
        this.tranForm.controls['currencyRate'].patchValue(0.0);
        this.tranForm.controls['totalTaxableAmount'].patchValue(0.0);
        this.tranForm.controls['grandTotal'].patchValue(0.0);

    }

    mainCreditNoteTypeChange(){





    }


}


export class DeliveryChallansSelect {
    constructor(public deliveryChallanNumber: string,
        public id: string,
        public selected?: boolean) {
        if (selected === undefined) selected = false;
    }
}

