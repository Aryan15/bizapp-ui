import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobworkDcItemComponent } from './jobwork-dc-item.component';

describe('JobworkDcItemComponent', () => {
  let component: JobworkDcItemComponent;
  let fixture: ComponentFixture<JobworkDcItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobworkDcItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobworkDcItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
