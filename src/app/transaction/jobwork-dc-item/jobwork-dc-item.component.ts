import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';
import { QuantityChangeEvent } from '../../data-model/misc-model';
import { AlertDialog } from '../../utils/alert-dialog';

@Component({
  selector: 'jobwork-dc-item',
  templateUrl: './jobwork-dc-item.component.html',
  styleUrls: ['./jobwork-dc-item.component.scss']
})
export class JobworkDcItemComponent implements OnInit {

    //@Input() isItemChangeNeeded: boolean; //A work around for an existing angular bug #12366
    @Input() group: FormGroup;
    @Input() index: number;

    // @Input() isJWINDC: boolean;
    // @Input() isJWOUTDC: boolean;
    // @Input() isOUTJWOUTDC: boolean;
    @Input() transactionType;
    @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
    @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
    @Output() quantityChange: EventEmitter<QuantityChangeEvent>= new EventEmitter<QuantityChangeEvent>();
    private changeCtr: number = 0;

    itemRowConfig: any = [];
    itemClass: string;
    appearance: string;
    alertRef: MatDialogRef<AlertDialog>;
    constructor(
      public dialog: MatDialog, ) {

  }

  isContainer: string = "";

    ngOnInit() {

        //console.log('transactionType: ', this.transactionType);
        if(this.group.value.isContainer===1){
            this.isContainer="readonly"
        }
        else{
            this.isContainer=null
        }

        this.initConfig();
        ////console.log('filtered items: ', this.filteredItems());
        //console.log("in tr items before:", this.group.controls['quantity'].value)
        //this.resetAmounts();
        //console.log("in tr items after:", this.group.controls['quantity'].value)
    }

    initConfig() {
        if(this.transactionType === AppSettings.INCOMING_JOBWORK_IN_DC){ //this.isJWINDC
            this.itemRowConfig = [
                { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-sm  read-only" },
                { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Part Number', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'spec/desc', fxFlex:"15", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200 },
                { text: 'Process', fxFlex: "20", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },                
                { text: 'HSN', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },                
                // { text: 'Price', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },                
                { text: 'Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                { text: 'UoM', fxFlex: "7", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Amount', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only input-right" },
                // { text: 'Accepted Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Rejected Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'rejectedQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                { text: 'Del', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
            ];
        }else if(this.transactionType === AppSettings.INCOMING_JOBWORK_OUT_DC){ //this.isJWOUTDC
          this.itemRowConfig = [
            { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-sm  read-only" },
            { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'Part Number', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'spec/desc', fxFlex:"5", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200 },
            { text: 'Process', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },                
            { text: 'UoM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'Received Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'incomingQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'Sending Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },            
            { text: 'Prev Sent Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'deliveredQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'Bal Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'dcBalanceQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
            { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
            { text: 'Del', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
        ];
        }else if(this.transactionType === AppSettings.OUTGOING_JOBWORK_OUT_DC ) {
            //this.isOUTJWOUTDC
            this.itemRowConfig = [
                // { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-sm  read-only" },
                // { text: 'Material', fxFlex: "30", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Part Number', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Process', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },                
                // { text: 'Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'incomingQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                // { text: 'UoM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },                
                // { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                // { text: 'Del', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-sm  read-only" },
                { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Part Number', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'spec/desc', fxFlex:"15", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200 },
                { text: 'Process', fxFlex: "20", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },                
                { text: 'HSN', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },                
                // { text: 'Price', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },                
                { text: 'Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                { text: 'UoM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Amount', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only input-right" },
                // { text: 'Accepted Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm read-only" },
                // { text: 'Rejected Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'rejectedQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                { text: 'Del', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
            ];
        }else if(this.transactionType === AppSettings.OUTGOING_JOBWORK_IN_DC ) {
            //this.isOUTJWOUTDC
            this.itemRowConfig = [
                { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-sm  read-only" },
                { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Part Number', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'spec/desc', fxFlex:"5", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200 },
                { text: 'Process', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },                                
                { text: 'UoM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Sent Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'incomingQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Receiving Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                { text: 'Prev Recd Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'deliveredQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Bal Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'dcBalanceQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                { text: 'Del', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
            ];
        }
    }

    filteredItems() {
        return this.itemRowConfig.filter(item => item.isDisplay)
    }

    getRowClass(): string {

            return null; //"fixed-item-header";

    }
    getItemClass(idx: number, formFieldName: string){
    //     let testA: any[] = [];
       
       
    //    if(this.filteredItems()[idx].readonly === null)
    //    {
    //        this.appearance ="outline"
          
    //    }
    //    else
    //    {
    //     this.appearance ="fill"
    //    }
      
    //   return this.appearance;
    }

    getToolTip(formControlName: string): string{
        return this.group.get(formControlName).value;
    }

    getFieldValid(formControlName: string) : boolean{

        if(this.group.get(formControlName).dirty){

            if(this.group.get(formControlName).valid){
                return true;
            }else{
                return false;
            }

        }else{
            return true;
        }
        //return this.group.get(formControlName).dirty && !this.group.get(formControlName).valid ? false : true; 
    }
    genericKeyUp(idx: number,formControlName: string) {

        // this.itemClass = this.group.get(formControlName).valid ? this.getItemClass(idx, formControlName) : 'input-sm-invalid';
        
        //console.log('in genericKeyUp', formControlName);
        if (formControlName === "quantity") {
            this.onQuantityChange();
        }
        if (formControlName === "price") {
            this.onPriceChange();
        }
        
        
    }
    onQuantityChange() {
        //console.log('onQuantityChange()');
        if(this.transactionType === AppSettings.INCOMING_JOBWORK_IN_DC) { //this.isJWINDC
            this.onIncomingQuantityChange();
        }
        this.resetAmounts();
        let lquantity: number = this.group.controls['quantity'].value;
        let lreceivedQuantity: number = this.group.controls['incomingQuantity'].value;
        let materialId: number = this.group.controls['materialId'].value;

        if(this.transactionType === AppSettings.INCOMING_JOBWORK_OUT_DC){ //this.isJWOUTDC
          if ((Number(lquantity)) > (Number(lreceivedQuantity))) {


            this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
            });

            this.alertRef.componentInstance.alertMessage = "Sending quantity must be less than or equal to received Quantity"


            this.group.controls['quantity'].patchValue("");

        }
        }

        if(this.transactionType === AppSettings.INCOMING_JOBWORK_OUT_DC || this.transactionType === AppSettings.OUTGOING_JOBWORK_IN_DC){
            this.calculateBalanceQuantity();
        }
        
        if(this.transactionType === AppSettings.INCOMING_JOBWORK_IN_DC || this.transactionType === AppSettings.OUTGOING_JOBWORK_OUT_DC){
            this.resetIncomingQuantity();
        }

        this.quantityChange.emit({
            index: this.index,
            materialId: materialId,
            quantity: lquantity
        });
        
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);
    }

    onPriceChange() {

        this.resetAmounts();
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);

    }


    resetAmounts() {
        
        
        this.patchAmount();
        
        // this.changeCtr++;
        // this.changeCounter.emit(this.changeCtr);
    }

    patchAmount() {
        //console.log('patchAmount.', this.group.controls['quantity'].value, this.group.controls['price'].value);
        let lquantity: number = this.group.controls['quantity'].value;

        let lprice: number;
        
        lprice = this.group.controls['price'].value;
        

        let lamount: number = this.calculateAmount(lquantity, lprice);

        lamount = +lamount.toFixed(2);

        this.group.controls['amount'].patchValue(lamount);
        //this.group.controls['discountPercentage'].patchValue(0);
    }

    calculateBalanceQuantity() {
        let incomingQuantity: number = +this.group.controls['incomingQuantity'].value;
        let deliveredQuantity: number = +this.group.controls['deliveredQuantity'].value;
        let quantity: number = +this.group.controls['quantity'].value;


        let dcBalanceQuantity: number  = incomingQuantity - (deliveredQuantity + quantity);

        //console.log("incomingQuantity,deliveredQuantity,quantity,dcBalanceQuantity ",incomingQuantity,deliveredQuantity,quantity,dcBalanceQuantity);
        //console.log("dcBalanceQuantity "+dcBalanceQuantity);

        this.group.controls['dcBalanceQuantity'].patchValue(dcBalanceQuantity,  { emitEvent: false });
    }

    calculateAmount(lquantity: number, lprice: number): number {
        return lquantity * lprice;
    }

    onIncomingQuantityChange(){
        let lquantity: number = this.group.controls['quantity'].value;
        let lincomingQuantity: number = this.group.controls['incomingQuantity'].value;

        let rejectedQuantity: number = lincomingQuantity - lquantity;

        this.group.controls['rejectedQuantity'].patchValue(rejectedQuantity);
        
    }

    resetIncomingQuantity(){
        let lquantity: number = this.group.controls['quantity'].value;
        this.group.controls['incomingQuantity'].patchValue(lquantity);
        this.group.controls['dcBalanceQuantity'].patchValue(lquantity,  { emitEvent: false });
    }

    delete(index: number) {

        ////console.log("Deleting...", index);
        this.deleteIndex.emit(index);
    }

    

}
