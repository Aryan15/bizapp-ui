import { Component, Input, OnInit, Output, EventEmitter,OnChanges, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable ,  merge ,  of as observableOf } from 'rxjs';
import { catchError ,  map ,  debounceTime ,  startWith ,  switchMap } from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';

import { PurchaseOrderService } from '../../services/purchase-order.service';
import { PurchaseOrderHeader, RecentPurchaseOrder } from '../../data-model/purchase-order-model';
import { TransactionType } from '../../data-model/transaction-type';

const PAGE_SIZE: number = 10;

@Component({
    selector: 'app-recent-po-table',
    templateUrl: './recent-po-table.component.html',
    styleUrls: ['./recent-po-table.component.scss']
})
export class RecentPurchaseOrderTableComponent implements OnInit, OnChanges {

    @Input()
    recentPoLoadCounter: number;

    @Input()
    transactionType : TransactionType;

    @Output()
    poHeader: EventEmitter<PurchaseOrderHeader> = new EventEmitter<PurchaseOrderHeader>();

    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();

    displayedColumns = ['purchaseOrderNumber',   'purchaseOrderDate',  'grandTotal', 'partyName'];
    purchaseOrderDatabase: PurchaseOrderHttpDao | null;

    dataSource = new MatTableDataSource<PurchaseOrderHeader>();

    filterForm = new FormGroup({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;

    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private purchaseOrderService: PurchaseOrderService) { }


    ngOnInit() {

        this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

        this.purchaseOrderDatabase = new PurchaseOrderHttpDao(this.purchaseOrderService);

        //If the user changes sort order, reset back to first page

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        this.getData();

    }

    getData(){
        merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
        .pipe(
        startWith({}),
        switchMap(() => {
            this.isLoadingResults = true;
            // tslint:disable-next-line:max-line-length
            return this.purchaseOrderDatabase!.getPurchaseOrders(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        }),
        map(data => {
            //Flip flag to show that loading has finished
            this.isLoadingResults = false;
            this.resultsLength = data.totalCount;
            //console.log('data.materialList: ', data.purchaseOrderHeaders);
            //console.log('data.totalCount: ', data.totalCount);
            if (this.filterForm.get('filterText').value != null) {
                if (data.totalCount === 0) {
                    this.noDataFound.emit(true);
                }
            }
            return data.purchaseOrderHeaders;
        }),
        catchError(() => {
            this.isLoadingResults = false;
            //console.log('catchError error');
            return observableOf([]);
        })
        ).subscribe(data => this.dataSource.data = data);
    }


    ngOnChanges(){
        //this.onChangeEvent.
        // //console.log('this.recentInvoiceLoadCounterObservable: ',this.recentInvoiceLoadCounterObservable);
        // this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);
        //console.log('in onchanage: ', this.recentPoLoadCounter);

        // if(this.purchaseOrderDatabase){
        //   this.getData();
        // }
        
    
        if(this.recentPoLoadCounter > 0 ){
            this.getData();
        //   this.purchaseOrderDatabase = new PurchaseOrderHttpDao(this.purchaseOrderService);
    
        //   this.purchaseOrderDatabase!.getPurchaseOrders(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize).pipe(
        //   map(data => {
        //     this.resultsLength = data.totalCount;
        //     //console.log('data.materialList: ', data.purchaseOrderHeaders);
        //     //console.log('data.totalCount: ', data.totalCount);
        //     if( this.filterForm.get('filterText').value != null)
        //     {
        //     if(data.totalCount === 0)
        //     {
        //       this.noDataFound.emit(true);
        //     }
        //     }
        //     return data.purchaseOrderHeaders;
        //   })).subscribe(data => this.dataSource.data = data);
        }
      }
    
    onRowClick(row: any) {
        //console.log('row clicked: ', row);
        this.isLoadingResults = true;
        this.purchaseOrderService.getPurchaseOrderById(row.id)
        .subscribe(response => {
            this.poHeader.emit(response);
            this.isLoadingResults = false;
        }, error => {
          this.isLoadingResults = false;
        })
        
    }

}



export class PurchaseOrderHttpDao {

    constructor(private purchaseOrderService: PurchaseOrderService) { }

    // tslint:disable-next-line:max-line-length
    getPurchaseOrders(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentPurchaseOrder> {

        pageSize = pageSize ? pageSize : PAGE_SIZE;
        //console.log('filterText: ', filterText);


        // this.materialService.getAllMaterialsForReport(sortColumn, sortDirection, page, pageSize).subscribe(response => {
        //   ////console.log("response: ", response);
        // })

        return this.purchaseOrderService.getRecentPurchaseOrders(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);
    }
}
