
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuantityChangeEvent } from '../../data-model/misc-model';
@Component({
    selector: 'transaction-item',
    templateUrl: 'transaction.item.component.html',
    styleUrls: ['transaction.item.component.scss'],
})
export class TransactionItem implements OnInit, OnChanges {
    //@Input() isItemChangeNeeded: boolean; //A work around for an existing angular bug #12366
    @Input() group: FormGroup;
    @Input() index: number;
    @Input() isIgst: boolean;
    @Input() isInclusiveTax: boolean;
    @Input() isItemLevelTax: boolean;
    @Input() isItemLevelDiscount: boolean;
    @Input() isDC: boolean;
    @Input() isJWDC: boolean;
    @Input() isJWPO: boolean;
     @Input() isOpen: boolean;
    @Input() quantityChangeCounter: number;
    @Output() changeCounter: EventEmitter<number> = new EventEmitter<number>();
    @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
    @Output() quantityChange: EventEmitter<QuantityChangeEvent>= new EventEmitter<QuantityChangeEvent>();
   // @Input()isImport:number;
    private changeCtr: number = 0;

    itemRowConfig: any = [];
    itemClass: string;
    appearance: string;

    isContainer: string = "";
    isOpenCheck: string = "";
    

    ngOnInit() {
        if(this.group.value.isContainer===1){
            this.isContainer="readonly"
        }
        else{
            this.isContainer=null
        }
        this.isOpen ? this.isOpenCheck="readonly" : this.isOpenCheck=null
        //console.log('isOpen.............................', this.isOpen);

        ////console.log('in transaction item component, isDC?', this.isDC);

        this.initConfig();
        ////console.log('filtered items: ', this.filteredItems());
        //console.log("in tr items before:", this.group.controls['discountPercentage'].value)
        if(!this.group.disabled)
        {
            this.resetAmounts();
        }
        //console.log("in tr items after:", this.group.controls['discountPercentage'].value)

        
    }

    ngOnChanges(){
        if(this.quantityChangeCounter > 0){
            this.resetAmounts();
        }
    }

    initConfig() {
        if (!(this.isDC || this.isJWDC)) {
            //console.log("!(this.isDC this.isJWDC)...................................66");

            if (this.isItemLevelTax) {                
                if (!this.isIgst) {//for CGST/SGST with item level tax
                    if (this.isItemLevelDiscount) {//Show discount columns
                        this.itemRowConfig = [
                            { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center overflow-visible" },
                            { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'Part Number', fxFlex: "8", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000, isTextArea: true },
                            { text: 'HSN/ SAC', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly:null, inputType: "text", itemClass: "input-sm " },
                            { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'Qty', fxFlex: "3", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right"}, //{'input-sm-invalid' : group.controls.quantity?.invalid }" },
                            { text: 'Price', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amount', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'Disc%', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountPercentage', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Disc Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountAmount', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amt After Disc', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterDiscount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'SGST %', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'SGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'CGST %', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'CGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'Amt After Tax', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Remarks', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                            { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                        ];
                    } else {//don't show discount columns
                        this.itemRowConfig = [
                            { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                            { text: 'Material', fxFlex: "19", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'Part Number', fxFlex: "7", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'spec/desc', fxFlex:"6", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                            { text: 'HSN/ SAC', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm " },
                            { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                            { text: 'Qty', fxFlex: "6", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Price', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amount', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'SGST %', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'SGST Amt', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                            { text: 'CGST %', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'CGST Amt', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Amt After Tax', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Remarks', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                            { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                        ];
                    }
                }
                else {//Just show IGST
                    if (this.isItemLevelDiscount) {//show discount columns
                        this.itemRowConfig = [
                            { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  " input-center input-sm  read-only" },
                            { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                            { text: 'Part Number', fxFlex: "11", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                            { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                            { text: 'HSN/ SAC', fxFlex: "9", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm " },
                            { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                            { text: 'Qty', fxFlex: "3", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Price', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amount', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Disc%', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountPercentage', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Disc Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountAmount', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amt After Disc', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterDiscount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'IGST %', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'IGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Amt After Tax', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                            { text: 'Remarks', fxFlex: "4", headerClass: 'item-header', color: 'primary', readonly: null, formControlName: 'remarks', isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                            { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                        ];
                    } else {//Don't show discount columns
                        this.itemRowConfig = [
                            { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                            { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },                            
                            { text: 'Part Number', fxFlex: "8", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },
                            { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                            { text: 'HSN/ SAC', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                            { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },
                            { text: 'Qty', fxFlex: "3", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Price', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amount', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'IGST %', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'IGST Amt', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Amt After Tax', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                            { text: 'Remarks', fxFlex: "4", headerClass: 'item-header', color: 'primary', readonly: null, formControlName: 'remarks', isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                            { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                        ];
                    }
                }
            }
            else {//Don't show tax columns
            //console.log("false............................");

                if (this.isItemLevelDiscount) {
                    this.itemRowConfig = [
                        { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  " input-center input-sm  read-only" },
                        { text: 'Material', fxFlex: "25", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Part Number', fxFlex: "8", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                        { text: 'HSN/ SAC', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                        { text: 'UOM', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Qty', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Price', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amount', fxFlex: " 5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Disc%', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountPercentage', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Disc Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountAmount', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amt After Disc', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterDiscount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', readonly: null, formControlName: 'remarks', isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                        { text: 'Del', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                    ];
                } else {//Don't show discount columns
                    this.itemRowConfig = [
                        { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                        { text: 'Material', fxFlex: "25", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Part Number', fxFlex: "12", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                        { text: 'HSN/ SAC', fxFlex: "13", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm " },
                        { text: 'UOM', fxFlex: "7", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Qty', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Price', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amount', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                        { text: 'Del', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                    ];
                }
            }
        } else if(this.isDC) {//for DC only
            //console.log("this.isDC...................................187");
            
            this.itemRowConfig = [
                { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                { text: 'Material', fxFlex: "37", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Part Number', fxFlex: "8", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                { text: 'HSN/ SAC', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm" },
                { text: 'UOM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
            ];
        }
        else if(this.isJWDC) { // Jobwork DC
            //console.log("this.isJWDC...................................203");

            this.itemRowConfig = [
                { text: 'SL', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                { text: 'Material', fxFlex: "27", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Part Number', fxFlex: "8", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                { text: 'Process', fxFlex: "10", headerClass: 'item-header',  color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm" },                
                { text: 'Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'incomingQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right" },
                { text: 'UOM', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Accepted Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Rejected Qty', fxFlex: "10", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'rejectedQuantity', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
            ];
        }

        if(this.isJWPO){
            //console.log("this.isJWPO...........................",this.isJWPO);
            
            if (this.isItemLevelTax) {
                if (!this.isIgst) {
                    this.itemRowConfig = [
                        { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center overflow-visible" },
                        { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                        { text: 'Part Number', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                        { text: 'Process', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                        { text: 'spec/desc', fxFlex:"5", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                        { text: 'HSN/ SAC', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly:null, inputType: "text", itemClass: "input-sm " },
                        { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm  read-only" },
                        { text: 'Qty', fxFlex: "3", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: this.isOpenCheck, inputType: "text", itemClass: "input-sm input-right"}, //{'input-sm-invalid' : group.controls.quantity?.invalid }" },
                        { text: 'Price', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amount', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'Disc%', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountPercentage', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Disc Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountAmount', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amt After Disc', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterDiscount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'SGST %', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'SGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'sgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'CGST %', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'CGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'cgstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right  read-only" },
                        { text: 'Amt After Tax', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Remarks', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                        { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                    ];
                }else{
                    this.itemRowConfig = [
                        { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  " input-center input-sm  read-only" },
                        { text: 'Material', fxFlex: "20", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Part Number', fxFlex: "7", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Process', fxFlex: "5", headerClass: 'item-header', color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'spec/desc', fxFlex:"6", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                        { text: 'HSN/ SAC', fxFlex: "9", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm " },
                        { text: 'UOM', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                        { text: 'Qty', fxFlex: "3", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: this.isOpenCheck, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Price', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amount', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Disc%', fxFlex: "4", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountPercentage', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Disc Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'discountAmount', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm input-right" },
                        { text: 'Amt After Disc', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterDiscount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'IGST %', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxPercentage', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'IGST Amt', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'igstTaxAmount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Amt After Tax', fxFlex: "5", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amountAfterTax', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                        { text: 'Remarks', fxFlex: "4", headerClass: 'item-header', color: 'primary', readonly: null, formControlName: 'remarks', isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                        { text: 'Del', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                    ];
                }
            }
            else{
                
                this.itemRowConfig = [
                    { text: 'SL', fxFlex: "2", headerClass: 'item-header', color: 'primary', formControlName: "slNo", isDisplay: true, readonly: "readonly", inputType: null, itemClass:  "input-center input-sm  read-only" },
                    { text: 'Material', fxFlex: "15", headerClass: 'item-header', color: 'primary', formControlName: 'partName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                    { text: 'Part Number', fxFlex: "12", headerClass: 'item-header', color: 'primary', formControlName: 'partNumber', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                    { text: 'Process', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'processName', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                    { text: 'spec/desc', fxFlex:"7", headerClass: 'item-header', color: 'primary', formControlName: 'specification', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 2000,isTextArea: true },
                    { text: 'HSN/ SAC', fxFlex: "13", headerClass: 'item-header', color: 'primary', formControlName: 'hsnOrSac', isDisplay: true, readonly: null, inputType: "text", itemClass: "input-sm " },
                    { text: 'UOM', fxFlex: "7", headerClass: 'item-header', color: 'primary', formControlName: 'uom', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm read-only" },
                    { text: 'Qty', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'quantity', isDisplay: true, readonly: this.isOpenCheck, inputType: "text", itemClass: "input-sm input-right" },
                    { text: 'Price', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'price', isDisplay: true, readonly: this.isContainer, inputType: "text", itemClass: "input-sm input-right" },
                    { text: 'Amount', fxFlex: "7", headerClass: 'item-header', OnlyNumber: true, color: 'primary', formControlName: 'amount', isDisplay: true, readonly: "readonly", inputType: "text", itemClass: "input-sm input-right read-only" },
                    { text: 'Remarks', fxFlex: "10", headerClass: 'item-header', color: 'primary', formControlName: 'remarks', readonly: null, isDisplay: true, inputType: "text", itemClass: "input-sm", maxLength: 200,isTextArea: true },
                    { text: 'Del', fxFlex: "3", headerClass: 'item-header', color: 'primary', formControlName: null, isDisplay: true, readonly: null, inputType: null, itemClass: "delete-class" },
                ];
            }
        }
    }

    filteredItems() {
        return this.itemRowConfig.filter(item => item.isDisplay)
    }

    getRowClass(): string {
        if (!(this.isDC || this.isJWDC)) {
            return "large-row";
        } else {
            return null; //"fixed-item-header";
        }
    }
    getItemClass(idx: number, formFieldName: string){
    //     let testA: any[] = [];
       
       
    //    if(this.filteredItems()[idx].readonly === null)
    //    {
    //        this.appearance ="outline"
          
    //    }
    //    else
    //    {
    //     this.appearance ="fill"
    //    }
      
    //   return this.appearance;
    }

    getToolTip(formControlName: string): string{
        return this.group.get(formControlName).value;
    }

    getFieldValid(formControlName: string) : boolean{

        if(this.group.get(formControlName).dirty){

            if(this.group.get(formControlName).valid){
                return true;
            }else{
                return false;
            }

        }else{
            return true;
        }
        //return this.group.get(formControlName).dirty && !this.group.get(formControlName).valid ? false : true; 
    }
    genericKeyUp(idx: number,formControlName: string) {

        // this.itemClass = this.group.get(formControlName).valid ? this.getItemClass(idx, formControlName) : 'input-sm-invalid';
        
        //console.log('in genericKeyUp', formControlName);
        if(!this.group.disabled){
            if (formControlName === "quantity") {
                this.onQuantityChange();
            }
            if (formControlName === "price") {
                this.onPriceChange();
            }
            if (formControlName === "discountPercentage") {
                this.onDiscountPercentageChange();
            }
            if (formControlName === "discountAmount") {
                this.onDiscountAmountChange();
            }
        }
        
    }
    onQuantityChange() {
        //console.log('onQuantityChange()');
        if(this.isJWDC){
            this.onIncomingQuantityChange();
        }
        let lquantity: number = this.group.controls['quantity'].value;
        let materialId: number = this.group.controls['materialId'].value;
        this.quantityChange.emit({
            index: this.index,
            materialId: materialId,
            quantity: lquantity
        });
        this.resetAmounts();
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);
    }

    onIncomingQuantityChange(){
        let lquantity: number = this.group.controls['quantity'].value;
        let lincomingQuantity: number = this.group.controls['incomingQuantity'].value;

        let rejectedQuantity: number = lincomingQuantity - lquantity;

        this.group.controls['rejectedQuantity'].patchValue(rejectedQuantity);
    }

    onPriceChange() {

        this.resetAmounts();
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);

    }

    onDiscountPercentageChange() {
        //console.log('in discount per change')

        this.resetDiscountAmount();
        //this.resetAmounts();
        this.patchAmountAfterDiscount();
        if(this.isInclusiveTax){
            this.patchAmount();
        }
        if (this.isItemLevelTax) {
            if (this.isIgst) {
                this.patchIgstAmount();
            } else {
                this.patchSgstAmount();
                this.patchCgstAmount();
            }
            this.patchAmountAfterTax();
        }
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);

    }

    onDiscountAmountChange() {

        this.resetDiscountPercentage();
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);

        this.patchAmountAfterDiscount();
        if(this.isInclusiveTax){
            this.patchAmount();
        }
        if (this.isItemLevelTax) {
            if (this.isIgst) {
                this.patchIgstAmount();
            } else {
                this.patchSgstAmount();
                this.patchCgstAmount();
            }
            this.patchAmountAfterTax();
        }
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);

    }

    resetAmounts() {
        
        if(!this.isInclusiveTax)
        {
            this.patchAmount();
        }
        if (this.isItemLevelDiscount) {
            this.resetDiscountAmount();
            this.patchAmountAfterDiscount();
        } else {
            this.patchDiscountFieldsFromAmount();
        }

        if(this.isInclusiveTax)
        {
            this.patchAmount();
            this.patchAmountAfterDiscount();
        }
            

        
        if (this.isItemLevelTax) {
            if (this.isIgst) {
                this.patchIgstAmount();
            } else {
                this.patchSgstAmount();
                this.patchCgstAmount();
            }
            this.patchAmountAfterTax();
        }
        this.changeCtr++;
        this.changeCounter.emit(this.changeCtr);
    }

    patchDiscountFieldsFromAmount() {
        this.group.controls['discountPercentage'].patchValue(0);
        this.group.controls['amountAfterDiscount'].patchValue(this.group.controls['amount'].value);
    }
    resetDiscountAmount() {
        let amount: number;
        if(!this.isInclusiveTax)
        {
            amount = this.group.controls['amount'].value;
        }
        else
        {
            amount = this.group.controls['price'].value * this.group.controls['quantity'].value;
        }
        //console.log('amount: ', amount);
        let discountPercentage: number = this.group.controls['discountPercentage'].value;
        let discountAmount: number = amount * discountPercentage / 100;
        discountAmount = +discountAmount.toFixed(2);
        discountAmount = discountAmount >= amount ? 0 : discountAmount;
       // console.log("discountAmount transaction item "+discountAmount );
        this.group.controls['discountAmount'].patchValue(discountAmount, { emitEvent: false });
      

    }
    resetDiscountPercentage() {
        // let amount: number = this.group.controls['amount'].value;
        let amount: number;
        if(!this.isInclusiveTax)
        {
            amount = this.group.controls['amount'].value;
        }
        else
        {
            amount = this.group.controls['price'].value * this.group.controls['quantity'].value;
        }
        //console.log('amount: ', amount);
        let discountAmount: number = this.group.controls['discountAmount'].value;
        let discountPercentage: number = (100 * discountAmount) / amount;
        discountPercentage = +discountPercentage.toFixed(2)
        this.group.controls['discountPercentage'].patchValue(discountPercentage, { emitEvent: false });
    }

    delete(index: number) {

        ////console.log("Deleting...", index);
        this.deleteIndex.emit(index);
    }

    patchAmount() {
        //console.log('patchAmount.', this.group.controls['quantity'].value, this.group.controls['price'].value);
        let lquantity: number = this.group.controls['quantity'].value;

        let lprice: number;
        let ltax: number = this.isIgst ? this.group.controls['igstTaxPercentage'].value : (this.group.controls['sgstTaxPercentage'].value + this.group.controls['cgstTaxPercentage'].value);
        //console.log("ltax: "+ltax);
        // For inclusive tax, formula is:
        // Amount = (price/(100+total tax percent))*100
        if (this.isInclusiveTax && this.isItemLevelDiscount) {
            lprice = ((this.group.controls['amountAfterDiscount'].value/lquantity) / (100.00 + ltax)) * 100;
            lprice = +lprice.toFixed(2);
        } else if(this.isInclusiveTax && !this.isItemLevelDiscount) {
            lprice = ((this.group.controls['price'].value) / (100.00 + ltax)) * 100;
            lprice = +lprice.toFixed(2);
        }
        else {
            lprice = this.group.controls['price'].value;
        }

        let lamount: number = this.calculateAmount(lquantity, lprice);

        lamount = +lamount.toFixed(2);

        this.group.controls['amount'].patchValue(lamount);
        //this.group.controls['discountPercentage'].patchValue(0);
    }

    patchIgstAmount() {
        ////console.log('patchIgstAmount');
        let lamountAfterDiscount: number;
        if(this.isInclusiveTax){
            lamountAfterDiscount = this.group.controls['amount'].value;
        }else{
            lamountAfterDiscount = this.group.controls['amountAfterDiscount'].value;
        }
        
        let igstTaxPercentage: number = this.group.controls['igstTaxPercentage'].value;
        let ligstTaxAmount: number = this.calculateIgstTaxAmount(lamountAfterDiscount, igstTaxPercentage);
        ligstTaxAmount = +ligstTaxAmount.toFixed(2);
        this.group.controls['igstTaxAmount'].patchValue(ligstTaxAmount);
    }

    patchSgstAmount() {
        ////console.log('patchSgstAmount.', this.group.controls['amountAfterDiscount'].value, this.group.controls['sgstTaxPercentage'].value);
        // let lamountAfterDiscount: number = this.group.controls['amountAfterDiscount'].value;
        let lamountAfterDiscount: number;
        if(this.isInclusiveTax){
            lamountAfterDiscount = this.group.controls['amount'].value;
        }else{
            lamountAfterDiscount = this.group.controls['amountAfterDiscount'].value;
        }
        let sgstTaxPercentage: number = this.group.controls['sgstTaxPercentage'].value;
        let lsgstTaxAmount: number = this.calculateSgstTaxAmount(lamountAfterDiscount, sgstTaxPercentage);
        lsgstTaxAmount = +lsgstTaxAmount.toFixed(2);
        this.group.controls['sgstTaxAmount'].patchValue(lsgstTaxAmount);
    }

    patchCgstAmount() {
        ////console.log('patchCgstAmount.', this.group.controls['amountAfterDiscount'].value, this.group.controls['cgstTaxPercentage'].value);
        // let lamountAfterDiscount: number = this.group.controls['amountAfterDiscount'].value;
        let lamountAfterDiscount: number;
        if(this.isInclusiveTax){
            lamountAfterDiscount = this.group.controls['amount'].value;
        }else{
            lamountAfterDiscount = this.group.controls['amountAfterDiscount'].value;
        }
        let cgstTaxPercentage: number = this.group.controls['cgstTaxPercentage'].value;
        let lcgstTaxAmount: number = this.calculateCgstTaxAmount(lamountAfterDiscount, cgstTaxPercentage);
        lcgstTaxAmount = +lcgstTaxAmount.toFixed(2);
        this.group.controls['cgstTaxAmount'].patchValue(lcgstTaxAmount);
    }

    patchAmountAfterDiscount() {
        ////console.log('patchAmountAfterDiscount.', this.group.controls['amount'].value, this.group.controls['discountAmount'].value);
        // let lamount: number = this.group.controls['amount'].value;
        let lamount: number;
        if(this.isInclusiveTax){
            lamount = this.group.controls['price'].value * this.group.controls['quantity'].value;
        }else{
            lamount = this.group.controls['amount'].value;
        }
        //console.log("lamount"+lamount);
        let ldiscountAmount: number = this.group.controls['discountAmount'].value;

        let lamountAfterDiscount: number = this.calculateAmountAfterDiscount(lamount, ldiscountAmount);
        ////console.log('lamountAfterDiscount:', lamountAfterDiscount);
        lamountAfterDiscount = +lamountAfterDiscount.toFixed(2);
        lamountAfterDiscount = lamountAfterDiscount > lamount ? 0 : lamountAfterDiscount;
        ////console.log('lamountAfterDiscount:', lamountAfterDiscount);
        this.group.controls['amountAfterDiscount'].patchValue(lamountAfterDiscount);
    }

    patchAmountAfterTax() {
        ////console.log('patchAmountAfterTax. ', this.group.controls['amountAfterDiscount'].value, this.group.controls['cgstTaxPercentage'].value, this.group.controls['sgstTaxPercentage'].value);
        // let lamountAfterDiscount: number = this.group.controls['amountAfterDiscount'].value;
        let lamountAfterDiscount: number;
        if(this.isInclusiveTax){
            lamountAfterDiscount = this.group.controls['amount'].value;
        }else{
            lamountAfterDiscount = this.group.controls['amountAfterDiscount'].value;
        }
        let cgstTaxPercentage: number = this.group.controls['cgstTaxPercentage'].value;
        let sgstTaxPercentage: number = this.group.controls['sgstTaxPercentage'].value;
        let igstTaxPercentage: number = this.group.controls['igstTaxPercentage'].value;
        let lamountAfterTax: number = this.calculateAmountAfterTax(lamountAfterDiscount, cgstTaxPercentage, sgstTaxPercentage, igstTaxPercentage);
        lamountAfterTax = +lamountAfterTax.toFixed(2);
        this.group.controls['amountAfterTax'].patchValue(lamountAfterTax);

    }
    // quantityChange(){
    //     this.resetAmounts();
    // }

    calculateAmount(lquantity: number, lprice: number): number {
        return lquantity * lprice;
    }

    calculateAmountAfterDiscount(lamount: number, ldiscountAmount: number): number {
        return lamount - ldiscountAmount;
    }
    calculateIgstTaxAmount(lamountAfterDiscount: number, igstTaxPercentage: number) {
        return lamountAfterDiscount * (igstTaxPercentage / 100);
    }
    calculateSgstTaxAmount(lamountAfterDiscount: number, sgstTaxPercentage: number) {
        return lamountAfterDiscount * (sgstTaxPercentage / 100);
    }
    calculateCgstTaxAmount(lamountAfterDiscount: number, cgstTaxPercentage: number) {
        return lamountAfterDiscount * (cgstTaxPercentage / 100);
    }
    calculateAmountAfterTax(lamountAfterDiscount: number, cgstTaxPercentage: number, sgstTaxPercentage: number, igstTaxPercentage: number) {
        return lamountAfterDiscount + (lamountAfterDiscount * (cgstTaxPercentage + sgstTaxPercentage + igstTaxPercentage) / 100);

    }

}
