import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionRoutingModule } from './transaction-routing.module';
import { RecentPurchaseOrderTableComponent } from './purchase-order/recent-po-table.component';
import { RecentQuotationTableComponent } from './quotation/recent-quotation-table.component';
import { DeliveryChallanComponent } from './delivery-challan/dc.component';
import { GrnComponent } from './grn/grn.component';
import { GrnItemComponent } from './grn/grn-item.component';
import { PayableReceivableItemComponent } from './payable-receivable/pr-item.component';
import { RecentPayableReceivableComponent } from './payable-receivable/recent-pr.component';
import { TransactionItem } from './transaction-item/transaction.item.component';
import { NewPoComponentComponent } from './new-po-component/new-po-component.component';
import { TransactionParentComponent } from './transaction-parent/transaction-parent.component';
import { NewQuotationComponent } from './new-quotation/new-quotation.component';
import { NewInvoiceComponent } from './new-invoice/new-invoice.component';
import { NewPayableReceivableComponent } from './new-payable-receivable/new-payable-receivable.component';
import { PreventUnsavedChangesGuardPr } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-pr';
import { PreventUnsavedChangesGuardPo } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-po';
import { PreventUnsavedChangesGuardDC } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-dc';
import { PreventUnsavedChangesGuardGrn } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-grn';
import { PreventUnsavedChangesGuardInvoice } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-invoice';
import { SharedModule } from '../shared/shared.module';
import { RecentInvoiceTableComponent } from './invoice/recent-invoice-table.component';
import { RecentDeliveryChallanTableComponent } from './delivery-challan/recent-dc-table.component';
import { RecentGrnTableComponent } from './grn/recent-grn-table.component';
import { ReferenceLinksComponent } from '../reference-links/reference-links.component';
import { PartyPopupComponent } from '../master/party-popup/party-popup.component';
import { MaterialPopupComponent } from '../master/material-popup/material-popup.component';
import { JobWorkDeliveryChallanComponent } from './job-work-delivery-challan/job-work-delivery-challan.component';
import { JobworkDcItemComponent } from './jobwork-dc-item/jobwork-dc-item.component';
import { VoucherComponent } from './voucher/voucher.component';
import { VoucherItemComponent } from './voucher/voucher-item.component';
import { RecentVoucherTableComponent } from './voucher/recent-voucher-table.component';
import { ExpensePopupComponent } from '../master/expense-popup/expense-popup.component';
import { PreventUnsavedChangesGuardQuotation } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-quotation';
import { PreventUnsavedChangesGuardJobWorkDc } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-jwdc';
import { PreventUnsavedChangesGuardVoucher } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-voucher';
import { AutoTransactionPopupComponent } from './auto-transaction-popup/auto-transaction-popup.component';
import { PettyCashComponent } from './petty-cash/petty-cash.component';

import { PettyCashItemComponent } from './petty-cash/petty-cash-item.component';
import { RecentPettyCashComponent } from './petty-cash/recent-petty-cash.component';
import { CategoryPopupComponent } from './category-popup/category-popup.component';
import { TagPopupComponent } from './tag-popup/tag-popup.component';
import { EwayBillComponent } from './eway-bill/eway-bill.component';
import { EwaybillPopupComponent } from './ewaybill-popup/ewaybill-popup.component';
import { EwayBillListComponent } from './eway-bill/eway-bill-list.component';
import { EwayBillExtendValidityPopupComponent } from './eway-bill-extend-validity-popup/eway-bill-extend-validity-popup.component';
import { AutoTransactionMasterComponent } from '../master/auto-transaction-master/auto-transaction-master.component';
import { RecentAutoTransactionComponent } from '../master/auto-transaction-master/recent-auto-transaction.component';
  


@NgModule({
  imports: [
    CommonModule,
    TransactionRoutingModule,
    SharedModule,
    
  ],
  declarations: [
    RecentPurchaseOrderTableComponent,
    //QuotationComponent,
    RecentQuotationTableComponent,
    RecentVoucherTableComponent,
    DeliveryChallanComponent,
    GrnComponent,
    GrnItemComponent,
    VoucherItemComponent,
    PayableReceivableItemComponent,
    //PayableReceivableComponent,
    RecentPayableReceivableComponent,
    // NoteComponent,
    // NoteItemComponent,
    // RecentNoteTableComponent,
    TransactionItem,
    NewPoComponentComponent,
    TransactionParentComponent,
    // MasterSubHeadingComponent,
    NewQuotationComponent,
    NewInvoiceComponent,
    NewPayableReceivableComponent,
    RecentInvoiceTableComponent,
    RecentDeliveryChallanTableComponent,
    RecentGrnTableComponent,
    ReferenceLinksComponent,
    PartyPopupComponent,
    MaterialPopupComponent,
    JobWorkDeliveryChallanComponent,
    ExpensePopupComponent,
    JobworkDcItemComponent,
    VoucherComponent,
    AutoTransactionPopupComponent,
    PettyCashComponent,
    PettyCashItemComponent,
    CategoryPopupComponent,
    TagPopupComponent,
    RecentPettyCashComponent,
    EwayBillComponent,
    EwaybillPopupComponent,
    EwayBillListComponent,
    EwayBillExtendValidityPopupComponent,
    AutoTransactionMasterComponent,
    RecentAutoTransactionComponent
   
  ],
  providers: [
    PreventUnsavedChangesGuardPr,
    PreventUnsavedChangesGuardPo,
    //PreventUnsavedChangesGuardQuotation,
    PreventUnsavedChangesGuardDC,
    PreventUnsavedChangesGuardGrn,
    PreventUnsavedChangesGuardInvoice,
    //PreventUnsavedChangesGuardNote,
    PreventUnsavedChangesGuardQuotation,
    PreventUnsavedChangesGuardJobWorkDc,
    PreventUnsavedChangesGuardVoucher,
  ],
  schemas: [NO_ERRORS_SCHEMA],
  //entryComponents: [ReferenceLinksComponent,]
  entryComponents: [ ReferenceLinksComponent, PartyPopupComponent, MaterialPopupComponent, ExpensePopupComponent, EwaybillPopupComponent , EwayBillExtendValidityPopupComponent]
})
export class TransactionModule { }
