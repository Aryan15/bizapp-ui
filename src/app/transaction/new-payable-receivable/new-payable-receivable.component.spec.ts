import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPayableReceivableComponent } from './new-payable-receivable.component';

describe('NewPayableReceivableComponent', () => {
  let component: NewPayableReceivableComponent;
  let fixture: ComponentFixture<NewPayableReceivableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPayableReceivableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPayableReceivableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
