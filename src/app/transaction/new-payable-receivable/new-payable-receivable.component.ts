import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { flatMap, startWith, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ActivityRoleBinding, StatusBasedPermission } from '../../data-model/activity-model';
import { CardType } from '../../data-model/card-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { InvoiceHeader } from '../../data-model/invoice-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { Party } from '../../data-model/party-model';
import { PaymentMethod } from '../../data-model/payment-method';
import { PayableReceivableHeader, PayableReceivableItem, PayableReceivableWrapper } from '../../data-model/pr-model';
import { TransactionType } from '../../data-model/transaction-type';
import { FinancialYearService } from '../../services/financial-year.service';
import { InvoiceService } from '../../services/invoice.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { PartyService } from '../../services/party.service';
import { PayableReceivableItemService } from '../../services/payable-receivable-item.service';
import { PayableReceivableService } from '../../services/payable-receivable.service';
import { PaymentMethodService } from '../../services/payment-method.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { AlertDialog } from '../../utils/alert-dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { PrintDialog } from '../../utils/print-dialog';
import { PrintDialogContainer } from '../../utils/print-dialog-container';
import { UtilityService } from '../../utils/utility-service';
import { TransactionItemService } from '../../services/transaction-item.service';
import { Company } from '../../data-model/company-model';
import { CompanyService } from '../../services/company.service';
import { LocationService } from '../../services/location.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';

@Component({
  selector: 'app-new-payable-receivable',
  templateUrl: './new-payable-receivable.component.html',
  styleUrls: ['./new-payable-receivable.component.scss']
})
export class NewPayableReceivableComponent implements OnInit, OnDestroy {

  public prForm: FormGroup;
  public prHeader: PayableReceivableHeader;
  public prHeaderUpdate: PayableReceivableHeader;

  public company: Company;

  // subheading : String = "this page is used to record Information about ";

  public parties: Party[] = [];
  public invoices: InvoiceHeader[] = [];
  public creditDebitNotes: InvoiceHeader[] = [];
  public paymentMethods: PaymentMethod[] = [];
  public cardTypes: CardType[] = [];
  financialYear: FinancialYear;
  fieldValidatorLabels = new Map<string, string>();
  isPartyChangeable: boolean = true;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  datePipe = new DatePipe('en-US');
  transactionType: TransactionType;
  transactionTypeInvoice: number;
  transactionTypeNote: number;
  partyPlaceHolder: string;
  jwPartyPlaceHolder: string;
  notePlaceHolder: string;
  numberRangeConfiguration: NumberRangeConfiguration;
  isAutoNumber: boolean;
  selectedParty: Party;
  itemToBeRemove: string[] = [];
  transactionStatus: string = null;
  recentPrLoadCounter: number = 0;
  isCust: boolean;

  paymentMethodAuto: number = AppSettings.PAYMENT_METHOD.auto;
  paymentMethodManual: number = AppSettings.PAYMENT_METHOD.manual;
  isClearingModeAuto: boolean = true;
  activityId:number=0;
  statusBasedPermission: StatusBasedPermission;
  minDate: Date;
  maxDate: Date;
  filteredParties: Observable<Party[]>;
  disableParty: boolean = false;
  displayPartyName:string="";
  payingAmountChange: string = "PayingAmountChange";
  noteAmountApplied: string = "NoteAmountApplied";
  paymentDocumentPlaceHolder: string = "Cheque/DD/RTGS/NEFT";
  //makeItemFromRecentPr: string;
  showCardType: boolean = true;
  showNonCashType: boolean = true;
  bankNamePlaceHolder: string = "Bank Name";
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  onDestroy: Subject<boolean> = new Subject();
  statusCheck:number=0;
  showTheCreatedBy=false;
  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: true,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: true,
    isSaveButton: true,
    isEditButton: true,
    isDraftButton: true,
  };

  activityRoleBindings: ActivityRoleBinding[];
  defaultDate: string;
  todaysDate = new Date();
  printRef: MatDialogRef<PrintDialog>;
  mainPartyType: string = "customer";
  
  constructor(private fb: FormBuilder,
    public route: ActivatedRoute,
    private transactionTypeService: TransactionTypeService,
    private partyService: PartyService,
    private numberRangeConfigService: NumberRangeConfigService,
    private userService: UserService,
    private financialYearService: FinancialYearService,
    private utilityService: UtilityService,
    private paymentMethodService: PaymentMethodService,
    private payableReceivableItemService: PayableReceivableItemService,
    private invoiceService: InvoiceService,
    private dialog: MatDialog,
    private alertService: AlertService,
    private payableReceivableService: PayableReceivableService,
    private _router: Router,
    private transactionItemService: TransactionItemService,
    private companyService: CompanyService,
    private locationService: LocationService) { }

  ngOnInit() {

    this.company = this.companyService.getCompany();

    //console.log("todaysDate: ", this.todaysDate);
    this.route.params.pipe(
      takeUntil(this.onDestroy),
      flatMap(result => {
        //console.log('result1: ', result);
        return this.transactionTypeService.getTransactionType(+result['trantypeId'])
      }),
      flatMap(result => {
        //console.log("0", result);
        this.transactionType = result;
        if (this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
          this.mainPartyTypeChange();
          this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER;
          this.jwPartyPlaceHolder = "Jobwork Customer"
          this.notePlaceHolder = AppSettings.CREDIT_NOTE;
          this.isCust = true;
          //console.log("1");
          return this.partyService.getCustomers();

        } else if (this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {

          this.mainPartyTypeChange();
          this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER;
          this.jwPartyPlaceHolder = "Jobwork Vendor"
          this.notePlaceHolder = AppSettings.DEBIT_NOTE;
          this.isCust = false;
          //console.log("2");
          return this.partyService.getSuppliers();
        } else {
          //let p : Party[] = []
          return new Observable<Party[]>();
        }
      }),
      flatMap(result => {
        this.parties = result;
        //console.log("3");
        return this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(this.transactionType.id);
      })
      , flatMap(result => {
        this.numberRangeConfiguration = result;

        this.isAutoNumber = this.numberRangeConfiguration.autoNumber === 1 ? true : false;
        //console.log("isAutoNumber" + this.isAutoNumber);
        return this.userService.setActivityRoleBinding(this.route);
      })
      , flatMap(response => {
        this.activityRoleBindings = response;
        this.activityRoleBindings.forEach(activityRoleBinding => {
          this.activityId=activityRoleBinding.activityId;
          });

        return this.financialYearService.getFinancialYear();
      })
      , flatMap(response => {
        this.financialYear = response;
        this.minDate = new Date(this.financialYear.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
        this.maxDate = new Date(this.financialYear.endDate);
        //console.log("this.minDate", this.minDate)
        //console.log("this.maxDate", this.maxDate);
        return this.utilityService.getCurrentDateObs();

      })
      , flatMap(response => {
        this.defaultDate = response;
        return this.paymentMethodService.getAllPaymentMethods();
      })
      , flatMap(response => {
        this.paymentMethods = response;
        return this.paymentMethodService.getCardTypes();
      })
    ).subscribe(response => {
      this.cardTypes = response;
      this.initForm();
      this.getFilteredParties();
      this.prForm.get('clearingMode').patchValue(this.paymentMethodAuto);
    })

  }


  mainPartyTypeChange() {
    if (this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      //console.log("this.mainPartyType: " + this.mainPartyType)
      if (this.mainPartyType === "customer") {
        this.transactionTypeInvoice = 1 //Customer Invoice
      } else {
        this.transactionTypeInvoice = 15; //Jobwork Invoice
      }
      this.transactionTypeNote = 9; //Credit note Id
    }
    else {
      if (this.mainPartyType === "customer") {
        this.transactionTypeInvoice = 5 //Supplier Invoice
      } else {
        this.transactionTypeInvoice = 18; //Subcontracting Invoice
      }
      this.transactionTypeNote = 10; //Debit note Id
    }
  }

  ngOnDestroy() {
    //console.log('ngOnDestory');
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  getFilteredParties() {
    //console.log('in getFilteredParties')
    let partyType: string = this.isCust ? AppSettings.PARTY_TYPE_CUSTOMER : AppSettings.PARTY_TYPE_SUPPLIER;
    this.filteredParties = this.prForm.get('partyName').valueChanges
        .pipe(
        startWith(''),
        debounceTime(300),
        // tap(val => //console.log('party search: ', val)),
        //map(val => this.filterParty(val))
        switchMap(value => this.partyService.getPartiesByNameLike((this.mainPartyType === "jobworker" ? AppSettings.PARTY_TYPE_ALL : partyType), value) )
        );

      
}

  // getFilteredParties() {
  //   //console.log('in getFilteredParties')
  //   this.filteredParties = this.prForm.get('partyName').valueChanges
  //     .pipe(
  //       startWith(''),
  //       map(val => this.filterParty(val))
  //     );
  // }

  // filterParty(val: string): Party[] {
  //   //console.log('in filter: ', val);
  //   //console.log('parties ', this.parties);
  //   if (val) {
  //     return this.parties.filter(party =>

  //       party.name.toLowerCase().includes(val.toLowerCase())
  //     );
  //   }
  //   return this.parties;
  // }

  // onNoteChange(){
  //   this.getInvoicesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeInvoice, this.noteAmountApplied);
  // }

  private initForm() {
    let data: PayableReceivableHeader = {
      id: null,
      payReferenceNumber: null,
      payReferenceId: null,
      payReferenceDate: this.defaultDate,//this.utilityService.getCurrentDate(),
      partyId: null,
      paymentMethodId: null,
      cardTypeId: null,
      paymentDocumentNumber: null,
      bankName: null,
      paymentDocumentDate: null,
      paymentNote: null,
      clearingMode: null,
      paymentStatus: null,
      statusId: null,
      financialYearId: null,
      companyId: null,
      transactionTypeId: null,
      paymentAmount: null,
      isPost: null,
      partyName: null,
      payableReceivableItems: null,
      statusName: null,
      creditDebitNumber: [],
      creditDebitId: [],
      paymentMode: null,
      // ..............
      companyName: null,
      companyGstRegistrationTypeId: null,
      companyPinCode: null,
      companyStateId: null,
      companyStateName: null,
      companyCountryId: null,
      companyPrimaryMobile : null,
      companySecondaryMobile : null,
      companyContactPersonNumber : null,
      companyContactPersonName : null,
      companyPrimaryTelephone : null,
      companySecondaryTelephone : null,
      companyWebsite : null,
      companyEmail : null,
      companyFaxNumber : null,
      companyAddress: null,
      companyTagLine: null,
      companyGstNumber : null,
      companyPanNumber : null,
      companyPanDate : null,
      companyCeritificateImagePath: null,
      companyLogoPath: null,
      // partyName: null,
      partyContactPersonNumber: null,
      partyPinCode: null,
      partyAreaId: null,
      partyCityId: null,
      partyStateId: null,
      partyCountryId: null,
      partyPrimaryTelephone: null,
      partySecondaryTelephone: null,
      partyPrimaryMobile: null,
      partySecondaryMobile: null,
      partyEmail: null,
      partyWebsite: null,
      partyContactPersonName: null,
      partyBillToAddress: null,
      partyShipAddress: null,
      partyDueDaysLimit: null,
      partyGstRegistrationTypeId: null,
      partyGstNumber: null,
      partyPanNumber: null,
      isIgst: null,
      partyCode:null,
      createdBy:null,
      updateBy:null,
      createdDate:null,
      updatedDate:null,
    }

    this.prForm = this.toFormGroup(data, null);
    //console.log("this.form", this.prForm)

  }

  private toFormGroup(data: PayableReceivableHeader, mapItemCategory: string): FormGroup {

    const itemArr = new FormArray([]);
    if (data.payableReceivableItems) {
      data.payableReceivableItems.forEach(item => {
        let frmPaymentMethod: number = this.prForm.get('clearingMode').value;
       
        let tItem: PayableReceivableItem = this.payableReceivableItemService.mapItem(item.slNo,item, mapItemCategory, frmPaymentMethod, null);
        itemArr.push(this.payableReceivableItemService.makeItem(tItem));
        //this.makeItem(tItem)
      });
    }
    const formGroup = this.fb.group({
      id: [data.id],
      payReferenceNumber: [data.payReferenceNumber],
      payReferenceId: [data.payReferenceId],
      payReferenceDate: [this.datePipe.transform(data.payReferenceDate, AppSettings.DATE_FORMAT), Validators.required],
      partyId: [data.partyId, Validators.required],
      paymentMethodId: [data.paymentMethodId, Validators.required],
      cardTypeId: [data.cardTypeId],
      paymentDocumentNumber: [data.paymentDocumentNumber],
      bankName: [data.bankName],
      paymentDocumentDate: [this.datePipe.transform(data.paymentDocumentDate, AppSettings.DATE_FORMAT)],
      paymentNote: [data.paymentNote],
      clearingMode: [data.clearingMode],
      paymentStatus: [data.paymentStatus],
      statusId: [data.statusId],
      financialYearId: [data.financialYearId],
      companyId: [data.companyId],
      transactionTypeId: [data.transactionTypeId],
      paymentAmount: [data.paymentAmount, Validators.compose([Validators.required, Validators.min(0.0)])],
      isPost: [data.isPost],
      partyName: [data.partyName, Validators.required],
      statusName: [data.statusName],
      payableReceivableItems: itemArr,
      creditDebitNumber: [data.creditDebitNumber],
      creditDebitId: [data.creditDebitId],
      paymentMode:[data.paymentMode],
      // .............
      companyName: [data.companyName],
      companyGstRegistrationTypeId: [data.companyGstRegistrationTypeId],
      companyPinCode: [data.companyPinCode],
      companyStateId: [data.companyStateId],
      companyStateName: [data.companyStateName],
      companyCountryId: [data.companyCountryId],
      companyPrimaryMobile : [data.companyPrimaryMobile],
      companySecondaryMobile : [data.companySecondaryMobile],
      companyContactPersonNumber : [data.companyContactPersonNumber],
      companyContactPersonName : [data.companyContactPersonName],
      companyPrimaryTelephone : [data.companyPrimaryTelephone],
      companySecondaryTelephone : [data.companySecondaryTelephone],
      companyWebsite : [data.companyWebsite],
      companyEmail : [data.companyEmail],
      companyFaxNumber : [data.companyFaxNumber],
      companyAddress: [data.companyAddress],
      companyTagLine: [data.companyTagLine],
      companyGstNumber : [data.companyGstNumber],
      companyPanNumber : [data.companyPanNumber],
      companyPanDate : [data.companyPanDate],
      companyCeritificateImagePath: [data.companyCeritificateImagePath],
      companyLogoPath: [data.companyLogoPath],
      // partyName: [data.companyName],
      partyContactPersonNumber: [data.partyContactPersonNumber],
      partyPinCode: [data.partyPinCode],
      partyAreaId: [data.partyAreaId],
      partyCityId: [data.partyCityId],
      partyStateId: [data.partyStateId],
      partyCountryId: [data.partyCountryId],
      partyPrimaryTelephone: [data.partyPrimaryTelephone],
      partySecondaryTelephone: [data.partySecondaryTelephone],
      partyPrimaryMobile: [data.partyPrimaryMobile],
      partySecondaryMobile: [data.partySecondaryMobile],
      partyEmail: [data.partyEmail],
      partyWebsite: [data.partyWebsite],
      partyContactPersonName: [data.partyContactPersonName],
      partyBillToAddress: [data.partyBillToAddress],
      partyShipAddress: [data.partyShipAddress],
      partyDueDaysLimit: [data.partyDueDaysLimit],
      partyGstRegistrationTypeId: [data.partyGstRegistrationTypeId],
      partyGstNumber: [data.partyGstNumber],
      partyPanNumber: [data.partyPanNumber],
      isIgst: [data.isIgst],
      partyCode:[data.partyCode],
      createdBy:[data.createdBy],
      updateBy:[data.updateBy],
      createdDate:[this.datePipe.transform(data.createdDate,AppSettings.DATE_FORMAT)],
      updatedDate:[this.datePipe.transform(data.updatedDate,AppSettings.DATE_FORMAT)],
    });
    this.fieldValidatorLabels.set("partyName", "Party Name");
    this.fieldValidatorLabels.set("payReferenceDate", "Reference Date");
    //this.fieldValidatorLabels.set("paymentAmount", "Payment Amount");
    this.fieldValidatorLabels.set("paymentMethodId", "Payment Method");
    this.fieldValidatorLabels.set("paymentDocumentDate", "Payment Document Date");
    this.fieldValidatorLabels.set("paymentDocumentNumber", "Payment Document Number");
    this.fieldValidatorLabels.set("bankName", "Bank Name");
    this.fieldValidatorLabels.set("cardTypeId", "Card Type");


    formGroup.get('payableReceivableItems').setValidators(Validators.required);
    return formGroup;
  }



  partyChanged(_event: any, party: Party) {
  


    if (_event.isUserInput) {
      this.prForm.get('partyId').patchValue(party.id);
      this.prForm.get('creditDebitNumber').patchValue([]);
      this.selectedParty = party; //this.parties.find(p => p.id == party);
    
      this.payableReceivableService.checkDraftForParty(party.id, this.transactionType.id)
      .subscribe(response => {
        if(response.responseStatus !== 1 ){
          //console.log("Draft party exists");
          //console.log(response);
          this.alertService.danger(response.responseString);
          this.prForm.get('partyId').patchValue(null);
          this.prForm.get('partyName').patchValue(null);
          this.selectedParty = null;
        }else{
          if (this.selectedParty.id !== undefined) {
            this.invoices = [];
            this.getInvoicesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeInvoice, null);
            this.getCreditDebitNotesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeNote, null);
          }
        }
      })

    }
  }

  getCreditDebitNotesForAPartyAndTransactionType(partyId: number, transactionTypeId: number, eventSource: string) {
   
    if (partyId) {
      if(transactionTypeId===9 && this.mainPartyType === "jobworker"){
        transactionTypeId=28
      
      }
      else if(transactionTypeId===10 && this.mainPartyType === "jobworker"){
        transactionTypeId=29
        
      }
      
      this.invoiceService.getOpenNotesByPartyAndTransactionType(partyId, transactionTypeId).subscribe(response => {
        this.creditDebitNotes = response;
      
      })
    }
  }

  getInvoicesForAPartyAndTransactionType(partyId: number, transactionTypeId: number, eventSource: string) {
    //console.log("sd............");
   
    if (partyId != undefined) {
      

      if(this.invoices.length > 0){
        this.doWithInvoice(this.invoices, eventSource);
      

      }else{
      this.spinStart = true;
      this.invoiceService.getInvoicesByPartyAndTransactionType(partyId, transactionTypeId).subscribe(
        response => {
          this.doWithInvoice(response, eventSource);
          this.spinStart = false;
        }
      )
      
      }
    }
  }

  doWithInvoice(response: InvoiceHeader[], eventSource: string){

    const control = <FormArray>this.prForm.get('payableReceivableItems');

      //Clear any pre populated items exist
      while (control.length !== 0) {
        control.removeAt(0);
      }

    this.invoices = response;
    //console.log(response);
    let hdrPaymentAmount: number = 0;
    let finalPaymentAmount: number = 0;
    this.invoices.forEach((invoice, index) => {
      let frmPaymentMethod: number = this.prForm.get('clearingMode').value;
      let tItem: PayableReceivableItem = this.payableReceivableItemService.mapItem(index+1, invoice, AppSettings.PR_ITEM_INVOICE_ITEM, frmPaymentMethod, eventSource);
      control.push(this.payableReceivableItemService.makeItem(tItem));
      hdrPaymentAmount += invoice.dueAmount;
      finalPaymentAmount = +hdrPaymentAmount.toFixed(2);
    });


    if (!eventSource) {
      if (+this.prForm.get('clearingMode').value === this.paymentMethodAuto) {

        this.isClearingModeAuto = true;
        this.prForm.get('paymentAmount').patchValue(finalPaymentAmount);
        //console.log("this.isClearingModeAuto 1", this.isClearingModeAuto);
      }
      else {

        this.isClearingModeAuto = false;
        this.prForm.get('paymentAmount').patchValue(0);
        //console.log("this.isClearingModeAuto 2", this.isClearingModeAuto);
      }
    }
    else if (eventSource === this.payingAmountChange) {
      //console.log('calling patchItemPayingAmount()');
      this.patchItemPayingAmount();
    }
    else if (eventSource === this.noteAmountApplied) 
    {
      //console.log('calling patchItemNoteAmount()');
      this.patchItemNoteAmount();
    }
  }

  getClearingMethod(_event: any) {

    //console.log("event: ", _event);
    if(this.selectedParty && this.prForm.get('statusName').value !== AppSettings.SAVE_AS_DRAFT_STATUS){
      this.getInvoicesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeInvoice, null);
    }

    if(this.prForm.get('statusName').value === AppSettings.SAVE_AS_DRAFT_STATUS){

      if (+this.prForm.get('clearingMode').value === this.paymentMethodAuto) {
        this.isClearingModeAuto = true;        
      }
      else {
        this.isClearingModeAuto = false;
        const control = <FormArray>this.prForm.get('payableReceivableItems');

        let loopIdx: number = 0;
        
        while ((loopIdx < control.length)) {
          
          control.at(loopIdx).get('payingAmount').patchValue(0);
          let dueAmount = control.at(loopIdx).get('amountAfterTds').value - control.at(loopIdx).get('originalDebitAmount').value;
          control.at(loopIdx).get('dueAmount').patchValue(dueAmount);
          
          loopIdx++;


        }
        this.prForm.get('paymentAmount').patchValue(0);
      }
    }
    


  }

  patchItemPayingAmountMainWrapper(eventSource: string) {
    if(this.prForm.get('statusName').value !== AppSettings.SAVE_AS_DRAFT_STATUS){
      this.patchItemPayingAmountMain(eventSource);
    }else{
      this.patchItemPayingAmount();
    }
  }

  patchItemPayingAmountMain(eventSource: string) {

    let items: PayableReceivableItem[] = this.prForm.get('payableReceivableItems').value;
   
    let amt: number = 0;
    let deductAmount: number = 0;
    //let sumAmt: number = 0;
    items.forEach(item => {
      if ((item.tdsAmount != undefined && item.tdsAmount != 0) || (item.debitAmount != undefined && item.debitAmount != 0)) {
        amt += Number(item.tdsAmount);
        deductAmount += Number(item.debitAmount);
        //sumAmt += Number(item.debitAmount);
      }
    });

    console.log("amt, deductAmount", amt, deductAmount)
    if (amt > 0 || deductAmount >0 ) {
      this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
      });

      this.dialogRef.componentInstance.confirmMessage = AppSettings.PAYMENT_AMOUNT_RESET_MESSAGE

      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.prForm.get('creditDebitId').patchValue([]);
          this.getInvoicesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeInvoice, eventSource);
        }

      });
    } else {
      this.getInvoicesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeInvoice, eventSource);
    }
   //this.prForm.controls['paymentAmount'].setValidators([Validators.required, Validators.min(0)]);
  }

  patchItemNoteAmount() {
    let noteIds: string[] = this.prForm.get('creditDebitId').value;
    let noteAmount: number = 0;
    //console.log('noteIds: ', noteIds);
    ////console.log('this.creditDebitNotes: ',this.creditDebitNotes);

    this.creditDebitNotes.filter(n => noteIds.includes(n.id))
      .forEach(n => {
        ////console.log(n.grandTotal);
        noteAmount += n.grandTotal;
      })

    //console.log('noteAmount: ', noteAmount);

    const control = <FormArray>this.prForm.get('payableReceivableItems');
      //let payingAmount = this.prForm.get('paymentAmount').value;

      ////console.log(control.value);
    let loopIdx: number = 0;

    if (noteAmount > 0) {

      
      //let actualDueAmount: number = 0;
      while ((loopIdx < control.length) && noteAmount > 0) {

        let dueAmountToPay: number = control.at(loopIdx).get('originalDueAmount').value;
        //let payingAmount: number = control.at(loopIdx).get('payingAmount').value;
        //console.log("dueAmountToPay: ", dueAmountToPay);
        ////console.log("payingAmount: ", payingAmount);
        //actualDueAmount += dueAmountToPay;

        let debitAmount: number = noteAmount > dueAmountToPay ? dueAmountToPay : noteAmount;
        let updatedDueAmount: number = dueAmountToPay - debitAmount
        // let payingAmount: number = updatedDueAmount > 0 ? ((updatedDueAmount - debitAmount) > 0 ? (updatedDueAmount - debitAmount) : debitAmount) : 0;
        // //console.log("payingAmount: ",payingAmount);
        //console.log(" updatedDueAmount: ",updatedDueAmount);
        control.at(loopIdx).get('debitAmount').patchValue(debitAmount);
        control.at(loopIdx).get('dueAmount').patchValue(updatedDueAmount);
        control.at(loopIdx).get('creditDebitHeaderId').patchValue(noteIds);
        control.at(loopIdx).get('payingAmount').patchValue(updatedDueAmount );
        noteAmount = noteAmount - debitAmount;
        //console.log("noteAmount: ", noteAmount);
        loopIdx++;


      }

      loopIdx = 0;
      let paymentAmount: number = 0;
      while (loopIdx < control.length) {
        let l_paymentAmount: number = control.at(loopIdx).get('payingAmount').value;
        paymentAmount += l_paymentAmount;
        loopIdx++;
      }

      // if (actualDueAmount < this.prForm.get('paymentAmount').value) {

      //   this.prForm.get('paymentAmount').patchValue(actualDueAmount);
      // }

      if (paymentAmount != this.prForm.get('paymentAmount').value) {

        this.prForm.get('paymentAmount').patchValue(paymentAmount);
      }

    }else{
      loopIdx = 0;
      let paymentAmount: number = 0;
      while (loopIdx < control.length) {
        let l_paymentAmount: number = control.at(loopIdx).get('payingAmount').value;
        paymentAmount += l_paymentAmount;
        loopIdx++;
      }

      // if (actualDueAmount < this.prForm.get('paymentAmount').value) {

      //   this.prForm.get('paymentAmount').patchValue(actualDueAmount);
      // }

      if (paymentAmount != this.prForm.get('paymentAmount').value) {

        this.prForm.get('paymentAmount').patchValue(paymentAmount);
      }
    }


  }

  patchItemPayingAmount() {
    const control = <FormArray>this.prForm.get('payableReceivableItems');
    let payingAmount = this.prForm.get('paymentAmount').value;

    ////console.log(control.value);
    let loopIdx: number = 0;
    let actualDueAmount: number = 0;
    while ((loopIdx < control.length)) {

      let dueAmountToPay: number = control.at(loopIdx).get('originalDueAmount').value;

      if(dueAmountToPay === 0 && this.prForm.get('statusName').value === AppSettings.SAVE_AS_DRAFT_STATUS){
        dueAmountToPay = control.at(loopIdx).get('amountAfterTds').value - control.at(loopIdx).get('originalDebitAmount').value;
      }

      //console.log("dueAmountToPay: ", dueAmountToPay)
      actualDueAmount += dueAmountToPay;

      control.at(loopIdx).get('payingAmount').patchValue(payingAmount >= dueAmountToPay ? dueAmountToPay : (payingAmount > 0 ? payingAmount : 0));
      control.at(loopIdx).get('dueAmount').patchValue(dueAmountToPay - +control.at(loopIdx).get('payingAmount').value);
      payingAmount = payingAmount - dueAmountToPay;
      loopIdx++;


    }

    if (actualDueAmount < this.prForm.get('paymentAmount').value) {

      this.prForm.get('paymentAmount').patchValue(actualDueAmount);
    }
    //this.prForm.controls['paymentAmount'].setValidators([Validators.required, Validators.min(0)]);


  }

  patchItemDebitAmount() {
    const control = <FormArray>this.prForm.get('payableReceivableItems');


    ////console.log(control.value);
    let loopIdx: number = 0;
    let actualDueAmount: number = 0;
    let totalDebitAmount: number = 0;

    let noteIds: string[] = this.prForm.get('creditDebitNumber').value;

    while ((loopIdx < control.length)) {

      let dueAmountToPay: number = control.at(loopIdx).get('originalDueAmount').value;
      //console.log("dueAmountToPay: ", dueAmountToPay)
      actualDueAmount += dueAmountToPay;

      // control.at(loopIdx).get('payingAmount').patchValue(payingAmount >= dueAmountToPay ? dueAmountToPay : (payingAmount > 0 ? payingAmount : 0));
      // control.at(loopIdx).get('dueAmount').patchValue(dueAmountToPay - +control.at(loopIdx).get('payingAmount').value);
      // payingAmount = payingAmount - dueAmountToPay;
      loopIdx++;


    }

    if (actualDueAmount < this.prForm.get('paymentAmount').value) {

      this.prForm.get('paymentAmount').patchValue(actualDueAmount);
    }


  }

  onPaymentMethodChange() {

    let paymentMethodId = this.prForm.get('paymentMethodId').value;

    let selectedPaymentMethod: string = this.paymentMethods.find(pm => pm.id === paymentMethodId).name;

    //console.log("selectedPaymentMethod1: "+selectedPaymentMethod);
    if (selectedPaymentMethod === "Cash") {
      //console.log("fdsaff")
      this.paymentDocumentPlaceHolder = "Cheque/DD/RTGS/NEFT";
      // this.showCardType = false;
      // this.showNonCashType = false;
      this.bankNamePlaceHolder = "Bank Name";
      this.prForm.get('paymentDocumentNumber').disable();
      this.prForm.get('cardTypeId').disable();
      this.prForm.get('bankName').disable();
      this.prForm.get('paymentDocumentDate').disable();
    } else if (selectedPaymentMethod === "Money Order") {
      this.paymentDocumentPlaceHolder = "Money Order No.";
      this.prForm.get('paymentDocumentNumber').enable();
      this.prForm.get('paymentDocumentNumber').setValidators(Validators.required);
      this.prForm.get('paymentDocumentNumber').updateValueAndValidity();
      // this.showCardType = false;
      // this.showNonCashType = true;
      this.bankNamePlaceHolder = "Post Office Name";
      this.prForm.get('bankName').enable();
      this.prForm.get('bankName').setValidators(Validators.required);
      this.prForm.get('bankName').updateValueAndValidity();
    }
    else if (selectedPaymentMethod === "Cheque/DD") {
      //console.log("Cheque/DD")
      this.paymentDocumentPlaceHolder = "Cheque/DD/RTGS/NEFT";
      this.bankNamePlaceHolder = "Bank Name";
      // this.showCardType = false;
      // this.showNonCashType = true;
      this.prForm.get('paymentDocumentNumber').enable();
      this.prForm.get('paymentDocumentNumber').setValidators(Validators.required);
      this.prForm.get('paymentDocumentNumber').updateValueAndValidity();
      this.prForm.get('bankName').enable();
      this.prForm.get('bankName').setValidators(Validators.required);
      this.prForm.get('bankName').updateValueAndValidity();
      this.prForm.get('paymentDocumentDate').enable();
      this.prForm.get('paymentDocumentDate').setValidators(Validators.required);
      this.prForm.get('paymentDocumentDate').updateValueAndValidity();
    }
    else if (selectedPaymentMethod === "Bank Transfer") {
      //console.log("Bank Transfer")
      this.paymentDocumentPlaceHolder = "Cheque/DD/RTGS/NEFT";
      this.bankNamePlaceHolder = "Bank Name";
      // this.showCardType = false;
      // this.showNonCashType = true;
      this.prForm.get('paymentDocumentNumber').enable();
      this.prForm.get('paymentDocumentNumber').setValidators(Validators.required);
      this.prForm.get('paymentDocumentNumber').updateValueAndValidity();
      this.prForm.get('bankName').enable();
      this.prForm.get('bankName').setValidators(Validators.required);
      this.prForm.get('bankName').updateValueAndValidity();
      this.prForm.get('paymentDocumentDate').enable();
      this.prForm.get('paymentDocumentDate').setValidators(Validators.required);
      this.prForm.get('paymentDocumentDate').updateValueAndValidity();
    }
    else if(selectedPaymentMethod=="Debit/Credit"){
      
      console.log("selectedPaymentMethod2: "+selectedPaymentMethod);
      this.prForm.get('paymentDocumentNumber').disable();
      this.prForm.get('cardTypeId').disable();
      this.prForm.get('bankName').disable();
      this.prForm.get('paymentDocumentDate').disable();
    }
    else {
      //console.log("selectedPaymentMethod2: "+selectedPaymentMethod);
      // this.showCardType = true;
      // this.showNonCashType = true;
      this.bankNamePlaceHolder = "Bank Name";
      this.prForm.get('paymentDocumentNumber').enable();
      this.prForm.get('paymentDocumentNumber').setValidators(Validators.required);
      this.prForm.get('paymentDocumentNumber').updateValueAndValidity();
      this.prForm.get('cardTypeId').enable();
      this.prForm.get('cardTypeId').setValidators(Validators.required);
      this.prForm.get('cardTypeId').updateValueAndValidity();
      this.prForm.get('bankName').enable();
      this.prForm.get('bankName').setValidators(Validators.required);
      this.prForm.get('bankName').updateValueAndValidity();
      this.prForm.get('paymentDocumentDate').enable();
      this.prForm.get('paymentDocumentDate').setValidators(Validators.required);
      this.prForm.get('paymentDocumentDate').updateValueAndValidity();

      
      
      
    }

    this.prForm.updateValueAndValidity();
    //console.log('form valid: '+this.prForm.valid);

  }

  itemChanged($event) {

    //console.log('in itemchanged')
    let items: PayableReceivableItem[] = this.prForm.get('payableReceivableItems').value;

    let amt: number = 0;
    //let sumAmt: number = 0;
    items.forEach(item => {
      if (item.payingAmount != undefined && item.payingAmount != 0) {
        amt += Number(item.payingAmount);
        //sumAmt += Number(item.debitAmount);
      }
    });

    //console.log('amt: ', amt);
    this.prForm.get('paymentAmount').patchValue(amt);

  }

  removePRItem(idx: number) {
    //console.log("id", (<FormArray>this.prForm.get('payableReceivableItems')))
    let itemId: string = (<FormArray>this.prForm.get('payableReceivableItems')).at(idx).get("id").value;
    //console.log("Itemid " + itemId)
    if (itemId) {

      this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
      });

      this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

      this.dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.itemToBeRemove.push(itemId);
          (<FormArray>this.prForm.get('payableReceivableItems')).removeAt(idx);
          this.itemChanged(null);
        }

      });
    } else {
      (<FormArray>this.prForm.get('payableReceivableItems')).removeAt(idx);
      this.itemChanged(null);
    }



  }

  submitWrap(model: PayableReceivableHeader){
    model.partyName=this.displayPartyName;
    //check any invoice in cancelled or deleted
    let invoiceIds: string[] = [];
    let invoiceNumbers: string[] = [];
    model.payableReceivableItems.forEach(item => {
      invoiceIds.push(item.invoiceHeaderId);
    })
    this.payableReceivableService.checkCancelledInvoices(invoiceIds)
    .subscribe(response => {
      if(response && response.length > 0){
        model.payableReceivableItems
        .filter(pi => response.includes(pi.invoiceHeaderId))
        .map(pi => pi.invoiceNumber)
        .forEach(inn => {
          invoiceNumbers.push(inn);
        })
        this.alertService.danger("Invoices are cancelled or deleted. please remove them and save again: "+invoiceNumbers);
      }else{
        this.onSubmit(model);
      }
    })

  }
  saveAsDraft() {
    //console.log(' saveAsDraft ');
    let model: PayableReceivableHeader = this.prForm.value;
    model.statusName = AppSettings.SAVE_AS_DRAFT_STATUS;
    //console.log(' model.statusName1: ', model.statusName);
    this.submitWrap(model);
  }
  displayFnParty(party?:Party): string | undefined {
    return party ? party.partyCode+"-"+party.name + (party.areaName ? '['+party.areaName+']' : ''): undefined;
}
  save() {

    //console.log(' save ');
    if(this.numberRangeConfiguration.id===null){
      this.alertRef = this.dialog.open(AlertDialog, {
          disableClose: false
      });
      this.alertRef.componentInstance.alertMessage="please add Transaction Type in Transaction Numbering";
      return;
    }

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = "Data cannot be modified once it is saved. Are you sure you want to save?";

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let model: PayableReceivableHeader = this.prForm.value;
        model.statusName = AppSettings.SAVE_AS_COMPLTED_STATUS;
        //console.log(' model.statusName1: ', model.statusName);
        this.submitWrap(model);
      }
    });

    
  }

  onSubmit(model: PayableReceivableHeader) {
    //console.log("model : ",model);
        //console.log("model : ",model);
    model.payableReceivableItems.forEach(pi => {

      if (pi.paidAmount < 0) {
        this.alertService.danger(AppSettings.PAYMENT_ERROR_MESSAGE);
        return false;
      }

    })
    let dupInvoices = model.payableReceivableItems.map(item => item.invoiceHeaderId).filter((value, index, self) => self.indexOf(value) !== index);
    if (dupInvoices.length > 0) {
      this.alertService.danger(AppSettings.DUPLICATE_INVOICE_MESSAGE);
      return false;
    }
    if(model.id===null){
        model = this.transactionItemService.patchValueFromCompanyAndParty(model, this.selectedParty, this.company)
        //console.log("model :",model);   
    }
    console.log(model.creditDebitId.length+"=====")
    if(model.creditDebitId.length==0 && model.paymentAmount<=0){
    this.alertService.success(AppSettings.PAYMENT_AMMOUNT_MESSAGE);
return;

    }

    //console.log('Form valid: '+this.prForm.valid);
     if(!this.prForm.valid){
      const controls = this.prForm;
      let invalidFieldList: string[] =this.utilityService.findInvalidControlsRecursive(controls);
      //console.log("invalidFieldList ", invalidFieldList)

      let invalidFiledLabels: string[] = [];
      
      invalidFieldList.forEach(field => {
        if (this.fieldValidatorLabels.get(field))
          invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
      })

      //console.log("invalidFiledLabels ", invalidFiledLabels)

      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE+invalidFiledLabels);
      return false;
    }
    //console.log('In onSubmit: ', model);
    //console.log('Grand total: ', model.paymentAmount);
    //console.log(' model.statusNames: ', model.statusName);
    //console.log("itemToBeRemove " + this.itemToBeRemove)

    // if (this.itemToBeRemove.length > 0) {
    //     //console.log(" in itemToBeRemove " + this.itemToBeRemove)
    //     this.itemToBeRemove.forEach(itemId =>
    //         this.payableReceivableService.deleteItem(itemId).subscribe(response => {
    //             //console.log("item id before save on deltion " + itemId);
    //             if (response.responseStatus) {
    //                 //console.log("Item deleted in DB: " + itemId);
    //                 // (<FormArray>this.quotationForm.get('quotationItems')).removeAt(idx);
    //             } else {
    //                 console.error("Cannot delete: ", response.responseString);
    //                 this.alertRef = this.dialog.open(AlertDialog, {
    //                     disableClose: false
    //                 });

    //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
    //             }
    //             return false;

    //         },
    //             error => {
    //                 console.error("Cannot delete: ", error);
    //                 this.alertRef = this.dialog.open(AlertDialog, {
    //                     disableClose: false
    //                 });

    //                 this.alertRef.componentInstance.alertMessage = AppSettings.DELETE_FAILED_MESSAGE
    //             }))
    // }
    this.spinStart = true;
    model.companyId = 1;
    model.financialYearId = this.financialYear.id;
    model.transactionTypeId = this.transactionType.id;
    let message: string;
    if (model.id === null) {
      if(model.statusName === AppSettings.SAVE_AS_DRAFT_STATUS){
        message = AppSettings.DRAFT_SUCESSFULL_MESSAGE;
      }else{
        message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
      }
      
    }
    else {
      message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
    }

    model.creditDebitId.forEach(cid => {
      //console.log("cid: ", cid);
      if(cid){
        let crNumber: string = this.creditDebitNotes.find(cdn => cdn.id === cid).invoiceNumber;
        model.creditDebitNumber.push(crNumber);
      }
      
    })

    //Remove items with 0 paying amount
    console.log(model.creditDebitId.length+"-------");
    if(model.creditDebitId.length==0){
    model.payableReceivableItems = model.payableReceivableItems.filter(item => item.payingAmount > 0);
    }
    //console.log('items after removing 0 paying amt: ', model.payableReceivableItems);

    let payableReceivableWrapper: PayableReceivableWrapper = { itemToBeRemove: this.itemToBeRemove, payableReceivableHeader: model };
    this.payableReceivableService.create(payableReceivableWrapper)
      .subscribe(response => {
        //console.log('after save: ', response);
        //console.log("saved id: ", response.id);
        this.showTheCreatedBy=true;
        this.prHeader = response;
        this.displayPartyName=this.prHeader.partyName;
        
        this.prHeader.partyName=this.prHeader.partyCode+"-"+this.prHeader.partyName;
        
        this.prForm.get('payReferenceNumber').patchValue(response.payReferenceNumber);
        this.prForm.get('id').patchValue(response.id);
        this.prForm.get('statusId').patchValue(response.statusId);
        this.isPartyChangeable=false;
        this.prForm = this.toFormGroup(this.prHeader, AppSettings.PR_ITEM_RECENT_ITEM);
        if(!response.updateBy){
          this.payableReceivableService.getPayableRecivbleById(response.id)
          .subscribe(response => {
            this.prHeaderUpdate=response;
              this.prForm.controls['updateBy'].patchValue(response.updateBy);
              this.prForm.controls['updatedDate'].patchValue(this.datePipe.transform(response.updatedDate,AppSettings.DATE_FORMAT));

                 

          });
      }
        this.prHeader.clearingMode === "1" ? this.prForm.get('clearingMode').patchValue(1) : this.prForm.get('clearingMode').patchValue(2)
        this.transactionStatus = "Pay Recceive " + response.payReferenceNumber + message;
        this.alertService.success(this.transactionStatus);
        this.saveStatus = true;
        this.spinStart = false;
        this.recentPrLoadCounter++;
        this.itemToBeRemove = [];
        this.setStatusBasedPermission(response.statusId);
        this.disableForm();
      },
        error => {
          console.error("Error", error);
          this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
          this.alertService.danger(this.transactionStatus);
          this.saveStatus = false;
          this.spinStart = false;
        });

  }

  setStatusBasedPermission(statusId: number) {

    if (statusId) {
      //console.log('status id: ', statusId);
      this.userService.getStatusBasedPermission(statusId).subscribe(response => {
        this.statusBasedPermission = response;
      })
    } else {
      //console.log("no status id: ", statusId);
    }
  }

  disableForm() {
    this.prForm.disable({ onlySelf: true, emitEvent: false });
    this.disableParty = true;
    this.prForm.markAsPristine();
  }

  delete() {

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.spinStart = true;
        let id: string = this.prForm.get('id').value;//this.quotationHeader.id;
        //console.log("deleteing..." + id);
        this.payableReceivableService.delete(id)
          .subscribe(response => {
            //console.log(response);
            this.transactionStatus = response.responseString;
            this.saveStatus = true;
            this.recentPrLoadCounter++;
            this.spinStart = false;
            if (response.responseStatus === 1) {
              this.alertService.success(this.transactionStatus);
              this.clearFormNoStatusChange();
            }
            else
              this.alertService.danger(this.transactionStatus);


          },
            error => {
              //console.log("Error ", error);
              this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
              this.saveStatus = false;
              this.spinStart = false;
              this.alertService.danger(this.transactionStatus);
            });
      }
      this.dialogRef = null;
    });
  }

  clearFormNoStatusChange() {
    this.initForm();
    this.selectedParty = null;
  }

  recentPayableAndReceivableGet(event) {
    //console.log('In recentPayableAndReceivableGet', event);
    this.prHeader = event;
    this.showTheCreatedBy=false;
    //this.prHeader.clearingMode === "1"
    this.displayPartyName=this.prHeader.partyName;
    this.prHeader.partyName=this.prHeader.partyCode+"-"+this.prHeader.partyName;
    this.prForm = this.toFormGroup(this.prHeader, AppSettings.PR_ITEM_RECENT_ITEM);
    this.prHeader.clearingMode === "1" ? this.prForm.get('clearingMode').patchValue(AppSettings.PAYMENT_METHOD.auto) : this.prForm.get('clearingMode').patchValue(AppSettings.PAYMENT_METHOD.manual)
    this.partyService.getCustomer(this.prHeader.partyId).subscribe(party => {

      this.selectedParty = party;

      this.getCreditDebitNotesForAPartyAndTransactionType(this.selectedParty.id, this.transactionTypeNote, null);

    });

    //console.log('selectedParty: ', this.selectedParty);
    this.isPartyChangeable = false;
    this.prForm.get('creditDebitNumber').disable();
    this.prForm.get('cardTypeId').disable();
    this.prForm.get('paymentMethodId').disable();
    this.disableForm();
    
    this.setStatusBasedPermission(this.prHeader.statusId);
    // this.prHeader.statusName === AppSettings.SAVE_AS_DRAFT_STATUS ? this.statusBasedPermission
  }


  print() {
    //console.log("print..................................");

    let l_printCopy: number;
    let partyName: string = this.prForm.get('partyName').value
    this.prForm.controls['partyName'].patchValue(this.displayPartyName);
    this.printRef = this.dialog.open(PrintDialogContainer, {
        // width: '250px',
        data: {
            transactionType: this.transactionType, 
            printDataModel: this.prForm.value, 
            printCopy: l_printCopy, 
            globalSetting: null,
            numberRangeConfiguration: this.numberRangeConfiguration
         }
    });
    this.prForm.controls['partyName'].patchValue(partyName);
}


  clearForm(formDirective: FormGroupDirective) {

    formDirective.resetForm();
    //console.log('in clear form')
    this.enableForm();
    this.initForm();
    //this.totalAmount = null;
    this.getFilteredParties();
    this.showCardType = true;
    this.showNonCashType = true;
    this.isPartyChangeable = true;
    this.creditDebitNotes = [];
    this.prForm.get('partyName').patchValue('');
    this.prForm.get('clearingMode').patchValue(this.paymentMethodAuto);

    setTimeout(() => {
      this.prForm.markAsPristine();
    }, 500)


  }

  closeForm() {
    // this._router.navigateByUrl('/')
    this._router.navigate(['/']);
  }
  enableForm() {
    this.prForm.enable({ onlySelf: true, emitEvent: false });
  }
  edit() {
    this.enableForm();
    if (+this.prForm.get('clearingMode').value === this.paymentMethodAuto) {
      this.isClearingModeAuto = true;     
    }
    else {
      this.isClearingModeAuto = false;      
    }
    this.disableParty = true;
  }

  helpVideos() {
        
    let status: string = "";
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id =this.activityId.toString();
 
    
    const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
        data => {

        });



    
}
}
