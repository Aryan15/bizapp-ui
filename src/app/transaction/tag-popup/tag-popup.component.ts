import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AppSettings } from '../../app.settings';
import { Tag } from '../../data-model/petty-cash-model';


@Component({
  selector: 'app-tag-popup',
  templateUrl: './tag-popup.component.html',
  styleUrls: ['./tag-popup.component.scss']
})
export class TagPopupComponent implements OnInit {

  public tagpopupForm: FormGroup;
  public tagModel:Tag;
  currentUser: any;
  themeClass: any;
  errorMessage: string = null;
 
  saveStatus: boolean = false;
  spinStart: boolean = false;

 constructor(private fb: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    
    private dialogRef: MatDialogRef<TagPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
     
   this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
   this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }


  ngOnInit() {

    this.initForm();
    
  }


  private initForm() {
    let data:  Tag= {
      id: null,
      name:null,
      deleted:null,
     
    
    }
    this.tagpopupForm = this.toFormGroup(data);
}

private toFormGroup(data: Tag): FormGroup {
  const formGroup = this.fb.group({
      id: [data.id],
     name:[data.name],
     deleted: [data.deleted],
    });
   return formGroup;
  }



  save() {
    
 
    this.dialogRef.close(this.tagpopupForm.value);
  }
  
  close(): void {
    //console.log('close clicked');
    this.dialogRef.close();
    // this.dialogRef.close();
  }
}
