import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagPopupComponent } from './tag-popup.component';

describe('TagPopupComponent', () => {
  let component: TagPopupComponent;
  let fixture: ComponentFixture<TagPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
