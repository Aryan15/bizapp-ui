import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';
import { Category } from '../../data-model/petty-cash-model';



@Component({
  selector: 'app-category-popup',
  templateUrl: './category-popup.component.html',
  styleUrls: ['./category-popup.component.scss']
})
export class CategoryPopupComponent implements OnInit {
  public categoryPopUpForm: FormGroup;
  public autoTransactionModel:Category;
  currentUser: any;
  themeClass: any;
  errorMessage: string = null;
 
  saveStatus: boolean = false;
  spinStart: boolean = false;

 constructor(private fb: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    
    private dialogRef: MatDialogRef<CategoryPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
     
   this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
   this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }

  ngOnInit() {
    
      this.initForm();
    console.log("entring here3444");
  
    
  }

  private initForm() {
    let data:  Category= {
      id: null,
      name:null,
      deleted:null,
     
    
    }
    this.categoryPopUpForm = this.toFormGroup(data);
}
private toFormGroup(data: Category): FormGroup {
const formGroup = this.fb.group({
    id: [data.id],
   name:[data.name],
   deleted: [data.deleted],
  });
 return formGroup;
}

save() {
    
 
  this.dialogRef.close(this.categoryPopUpForm.value);
}

close(): void {
  //console.log('close clicked');
  this.dialogRef.close();
  // this.dialogRef.close();
}


}