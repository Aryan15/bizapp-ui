import { Component, Input, OnInit, OnChanges, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, merge, of as observableOf } from 'rxjs';
import { catchError, map, debounceTime, startWith, switchMap } from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';

import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { DeliveryChallanHeader, RecentDeliveryChallan } from '../../data-model/delivery-challan-model';
import { TransactionType } from '../../data-model/transaction-type';

const PAGE_SIZE: number = 10;

@Component({
  selector: 'recent-dc-table',
  templateUrl: './recent-dc-table.component.html',
  styleUrls: ['./recent-dc-table.component.scss']
})
export class RecentDeliveryChallanTableComponent implements OnInit, OnChanges, AfterViewInit {

  @Input()
  recentDcLoadCounter: number;

  @Input()
  transactionType: TransactionType;

  @Output()
  dcHeader: EventEmitter<DeliveryChallanHeader> = new EventEmitter<DeliveryChallanHeader>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
  displayedColumns = ['deliveryChallanNumber', 'deliveryChallanDate', 'partyName'];
  deliveryChallanDatabase: DeliveryChallanHttpDao | null;

  dataSource = new MatTableDataSource<DeliveryChallanHeader>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private deliveryChallanService: DeliveryChallanService) { }


  ngOnInit() {

    //console.log('in init');




  }

  ngAfterViewInit() {
    //console.log("in after view init");


    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.deliveryChallanDatabase = new DeliveryChallanHttpDao(this.deliveryChallanService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.getData();


  }
  getData(){
    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        //console.log("this.isLoadingResults: " + this.isLoadingResults);
        return this.deliveryChallanDatabase!.getDcs(this.transactionType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished
        this.isLoadingResults = false;

        this.resultsLength = data.totalCount;
        //console.log('data.materialList: ', data.deliveryChallanHeaders);
        //console.log('data.totalCount: ', data.totalCount);
        //console.log("this.filterForm.get('filterText').value:", this.filterForm.get('filterText').value)
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.deliveryChallanHeaders;
      }),
      catchError((error) => {
        this.isLoadingResults = false;
        //console.log('catchError', error);
        return observableOf([]);
      })
    ).subscribe(data => this.dataSource.data = data);
  }

  ngOnChanges() {

    //console.log('in onchanage: ', this.recentDcLoadCounter);

    if (this.recentDcLoadCounter > 0) {
      this.getData();
    }
  }

  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.dcHeader.emit(row);
    //console.log('done');
  }

}


export class DeliveryChallanHttpDao {

  constructor(private deliveryChallanService: DeliveryChallanService) { }

  getDcs(transactionTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentDeliveryChallan> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);


    // this.materialService.getAllMaterialsForReport(sortColumn, sortDirection, page, pageSize).subscribe(response => {
    //   ////console.log("response: ", response);
    // })

    return this.deliveryChallanService.getRecentDeliveryChallans(transactionTypeId, filterText, sortColumn, sortDirection, page, pageSize);
  }
}