import { Component, OnInit, Input, Output, OnChanges, ViewChild ,EventEmitter,} from '@angular/core';
import { EWayBillRecentReport, EWayBillRecentDTO } from '../../data-model/invoice-model';
 import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, merge, of as observableOf } from 'rxjs';
import { EWayBillService } from '../../services/e-way-bill.service';
import { debounceTime, startWith, switchMap, catchError, map } from 'rxjs/operators';
const PAGE_SIZE: number = 10;

@Component({
  selector: 'app-eway-bill-list',
  templateUrl: './eway-bill-list.component.html',
  styleUrls: ['./eway-bill-list.component.scss']
})
export class EwayBillListComponent implements OnInit, OnChanges {

  @Input()
  recentEWayBillLoadCounter: number;
  
  @Output()
  eWayBillReport: EventEmitter<EWayBillRecentReport> = new EventEmitter<EWayBillRecentReport>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();

  displayedColumns: string[] =['partyName', 'invoiceNumber', 'eWayBillNo'];

  eWayBillDatabase: EWayBillHttpDao | null;

  dataSource = new MatTableDataSource<EWayBillRecentReport>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private ewayBillservice: EWayBillService) { }

  ngOnInit() {

    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.eWayBillDatabase = new EWayBillHttpDao(this.ewayBillservice);

    //If the Material changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.eWayBillDatabase!.getEWayBills( this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished 
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
       
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.eWayBillRecentReports;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        //console.log('catchError error');
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);

  }

  ngOnChanges() {
    
    if (this.recentEWayBillLoadCounter > 0) {
      this.eWayBillDatabase = new EWayBillHttpDao(this.ewayBillservice);

      this.eWayBillDatabase!.getEWayBills(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.materials);
          //console.log('data.totalCount: ', data.totalCount);
          if (this.filterForm.get('filterText').value != null) {
            if (data.totalCount === 0) {
              this.noDataFound.emit(true);
            }
          }
          return data.eWayBillRecentReports;
        })).subscribe(data => this.dataSource.data = data);
    }
  }

  
  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.eWayBillReport.emit(row);
    // console.log('done',row);
  }

}


export class EWayBillHttpDao {

  constructor(private eWayBillService: EWayBillService) { }

  getEWayBills(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<EWayBillRecentDTO> {
    sortColumn =  'eWayBillNo';

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);

    return this.eWayBillService.getRecentEWayBills(filterText, sortColumn, sortDirection, page, pageSize);
  }

}
