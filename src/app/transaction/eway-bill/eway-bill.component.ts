import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { InvoiceHeader, EwayBillTracking, EwayBillInvoiceResponseDTO, EWayBillRecentReport, TransactionMode, VehicleType } from '../../data-model/invoice-model';
import { EWayBillService } from '../../services/e-way-bill.service';
import { AlertService } from 'ngx-alerts';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { EwaybillPopupComponent } from '../ewaybill-popup/ewaybill-popup.component';
import { HttpErrorResponse } from '@angular/common/http';
import { AppSettings } from '../../app.settings';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { DatePipe } from '@angular/common';
import { State } from '../../data-model/state-model';
import { ActivatedRoute } from '@angular/router';
import { UtilityService } from '../../utils/utility-service';
import { DomSanitizer } from '@angular/platform-browser';
import { EwayBillExtendValidityPopupComponent } from '../eway-bill-extend-validity-popup/eway-bill-extend-validity-popup.component';
import { InvoiceService } from '../../services/invoice.service';
import { LocationService } from '../../services/location.service';
@Component({
  selector: 'app-eway-bill',
  templateUrl: './eway-bill.component.html',
  styleUrls: ['./eway-bill.component.scss']
})
export class EwayBillComponent implements OnInit, OnChanges {
  @Input() id: FormControl;
  @Input() isInvoiceTab: boolean;
  @Input() isDisabledButtons: boolean;
  @ViewChild('eWayBillNo', { static: true }) EwayBillNumElement: ElementRef;

  // @Input() invId: FormControl;

  eWayBillForm: FormGroup;
  ewayBills: EwayBillTracking[] = [];
  states: State[] = [];
  allStates: State[] = [];
  ewayBillTracking: EwayBillTracking;
   invoiceHeader: InvoiceHeader;
  ewayBillInvoiceResponseDTO: EwayBillInvoiceResponseDTO[];
  saveStatus: boolean = false;
  spinStart: boolean = false;
  disableGenerateButton: boolean = true;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  recentEWayBillLoadCounter: number = 0;
  eWayBillReport: EWayBillRecentReport;
  datePipe = new DatePipe('en-US');
  isLoadingResults = true;
  isEwayBillCancelled: boolean = false;

  fieldValidatorLabels = new Map<string, string>();
  transactionMode: TransactionMode[] = [
    { id: 1, name: 'Road' },
    { id: 2, name: 'Rail' },
    { id: 3, name: 'Air' },
    { id: 4, name: 'Ship' },

  ];

  vehicleType: VehicleType[] = [
    { id: 1, name: 'Regular', code: 'R' },
    { id: 2, name: 'ODC(Over Dimentional Cargo)', code: 'O' },
  ];
  minDate = new Date();
  maxDate = new Date();
  errorMessage: string = "";
  currentDate: Date = new Date();
  isValidDateExpired: boolean = true;

  invDate= new Date();
  transDocDate= new Date();
  constructor(private fb: FormBuilder,
    private eWayBillService: EWayBillService,
    protected alertService: AlertService,
    protected dialog: MatDialog,
    public route: ActivatedRoute,
    private utilityService: UtilityService,
    private sanitizer: DomSanitizer,
    private invoiceService: InvoiceService,
    private locationService: LocationService,
     
  ) { }


  ngOnInit() {

    this.initForm();
    this.getEWayBills();
    this.getAllStates();//port state
   
    if (this.isDisabledButtons == true) {
      this.disableForm();
    } else {
      this.enableForm();
    }

  }

  ngOnChanges(changes: SimpleChanges) {

    if (this.isDisabledButtons == true) {
      this.disableForm();

    } else {
      this.enableForm();
    }
       this.getEWayBills();
       this.getEWayBill();
    // console.log(changes['id'].previousValue);
    if (changes['id'].currentValue != changes['id'].previousValue) {
      this.initForm();
      this.getEWayBills();
    }
   
  }


  getEWayBill() {
 
    if (this.id) {
      this.eWayBillService.getEWayBill(this.id.value)
        .subscribe(response => {
          console.log("response "+response.status)
          if(!this.isDisabledButtons && response.status == 3){
            this.disableGenerateButton = false;
          }else{
            this.disableGenerateButton = true;
          }     
          console.log("-disableGenerateButtonid---   "+this.disableGenerateButton)
 
         })
    }
   }

   checkHsn(){
  var allow=0;
  console.log(this.id.value)
   this.invoiceService.getInvoice(this.id.value).subscribe( response =>{
    this.invoiceHeader =response;

    this.invoiceHeader.invoiceItems.forEach(item =>{
           console.log(item.hsnOrSac)
      if(item.hsnOrSac==null|| item.hsnOrSac.length>8){
        this.disableGenerateButton = true;
       allow=1
       this.errorMessage = "Please Enter Valid HSN CODE for Items , It Should contain 8 Character";
       this.alertService.danger(this.errorMessage);
         return;
      }

    })
 if(allow!=1){
  this.disableGenerateButton = false;


 }

    });
  }

  private getEWayBills() {
    console.log("item :", this.id);

    if (this.id) {
      this.eWayBillService.getEWayBills(this.id.value)
        .subscribe(response => {
          this.ewayBills = response;
          if (this.ewayBills.length == 0 ) {
            
            if(this.isDisabledButtons){
              this.disableGenerateButton = true;
            }else{
              this.disableGenerateButton = false;
            }
           
          }
          else {
             this.ewayBills.forEach(eway => {
              let validUptoDate: Date = new Date(eway.validUpto);
              this.currentDate = new Date();
              //console.log(validUptoDate.getTime()/1000 +"   "+ this.currentDate.getTime()/1000)
                if(eway.validUpto != null && validUptoDate < this.currentDate){
                this.isValidDateExpired = true;
                }else{
                this.isValidDateExpired = false;
               }
              
            })
            this.getEWayBill();
          
          }
        })
    }

  }

  private getEWayBillsById(invoiceHeaderId: string) {
    this.eWayBillService.getEWayBills(invoiceHeaderId)
      .subscribe(response => {
        this.ewayBills = response;
        if (this.ewayBills.length == 0 ) {
            
          if(this.isDisabledButtons){
            this.disableGenerateButton = true;
          }else{
            this.disableGenerateButton = false;
          }
         
        }
          else {  
          this.disableGenerateButton = true;
         
          this.ewayBills.forEach(eway => {
          let validUptoDate: Date = new Date(eway.validUpto);
          if(eway.validUpto != null && validUptoDate < this.currentDate){
            this.isValidDateExpired = true;
           }else{
            this.isValidDateExpired = false;
           }
            
        })
        this.getEWayBill();
        
      }
      })
     
  }

  private initForm() {
    let data: EwayBillTracking = {
      id: null,
      eWayBillNo: null,
      eWayBillDate: null,
      validUpto: null,
      docDate: null,
      docNo: null,
      pdfUrl: null,
      jsonData: null,
      detailedpdfUrl: null,
      invoiceHeaderId: null,
      status: null,
      error: null,
      transporterId: null,
      transporterName: null,
      transDocNo: null,
      transDocDate: null,
      vehicleNo: null,
      portName: null,
      portPin: null,
      portStateId:null,
      cancelledDate: null,
      response: null,
      vehicleType: null,
      reasonCode: null,
      reasonRemarks: null,
      transactionMode: null,
      remainingDistance: null,
      extensionReasonCode: null,
      extensionReasonRemarks: null,
      fromPlace: null,
      fromState: null,
      fromPincode: null,
      addressLine1: null,
      addressLine2: null,
      addressLine3: null,
      transDistance: null,
      noOfItems: null,
      hsnCode: null,
      isCurrent: null,
      cancelledReason: null,
      cancelledRemarks: null,
    };

    this.eWayBillForm = this.toFormGroup(data);

  }

  private toFormGroup(data: EwayBillTracking): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      eWayBillNo: [data.eWayBillNo],
      eWayBillDate: [this.datePipe.transform(data.eWayBillDate, AppSettings.DATE_FORMAT)],
      validUpto: [this.datePipe.transform(data.validUpto, AppSettings.DATE_FORMAT)],
      docDate: [data.docDate],
      docNo: [data.docNo],
      pdfUrl: [data.pdfUrl],
      detailedpdfUrl: [data.detailedpdfUrl],
      jsonData: [data.jsonData],
      invoiceHeaderId: [data.invoiceHeaderId],
      status: [data.status],
      error: [data.error],
      transporterId: [data.transporterId, Validators.compose([Validators.minLength(15), Validators.maxLength(15)])],
      transporterName: [data.transporterName, Validators.compose([Validators.maxLength(100)])],
      transDocNo: [data.transDocNo, Validators.compose([Validators.maxLength(15)])],
      vehicleNo: [data.vehicleNo, Validators.compose([Validators.maxLength(15)])],
      portName: [data.portName, Validators.compose([Validators.maxLength(100)])],
      portPin: [data.portPin, Validators.compose([Validators.minLength(6), Validators.maxLength(6)])],
      portStateId :[data.portStateId],
      cancelledDate: [data.cancelledDate],
      response: [data.response],
      vehicleType: [data.vehicleType],
      reasonCode: [data.reasonCode],
      reasonRemarks: [data.reasonRemarks],
      transactionMode: [data.transactionMode],
      remainingDistance: [data.remainingDistance],
      extensionReasonCode: [data.extensionReasonCode],
      extensionReasonRemarks: [data.extensionReasonRemarks],
      fromPlace: [data.fromPlace],
      fromState: [data.fromState],
      fromPincode: [data.fromPincode],
      transDocDate: [data.transDocDate],
      addressLine1: [data.addressLine1],
      addressLine2: [data.addressLine2],
      addressLine3: [data.addressLine3],
      transDistance: [data.transDistance, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(5)])],
      noOfItems: [data.noOfItems],
      hsnCode: [data.hsnCode],
      isCurrent: [data.isCurrent],
      cancelledReason: [data.cancelledReason],
      cancelledRemarks: [data.cancelledRemarks],
    });
    // this.fieldValidatorLabels.set("vehicleNo", "valid vehicle No");
    // this.fieldValidatorLabels.set("portPin", "valid 6 digit port Pin");
    // this.fieldValidatorLabels.set("transporterId", "valid transporter ID");
    // this.fieldValidatorLabels.set("transDistance", "Distance of transportation");
    // this.fieldValidatorLabels.set("transporterName", "Transporter Name less than 100 characters");
    // this.fieldValidatorLabels.set("portName", "Port Name less than 100 characters");
    // this.fieldValidatorLabels.set("transDocNo", "valid Transport Document Number");

    return formGroup;

  }

  eWayBillGeneration(ewayBillTracking: EwayBillTracking) {

    console.log(ewayBillTracking.portStateId)
    var allowEway :boolean;
    ewayBillTracking.invoiceHeaderId = this.id.value;
    this.invoiceService.getInvoice(this.id.value).subscribe( response =>{
      this.invoiceHeader = response;
      if(this.invoiceHeader){
        console.log(this.invoiceHeader.gstNumber+"----ar");
        if(this.invoiceHeader.gstNumber==null){
          this.errorMessage = "Please Enter Customer GST Number";
          this.alertService.danger(this.errorMessage);
            return;
        }
      //  this.invoiceHeader.invoiceItems.forEach(itemsv =>{
      //     if(itemsv.hsnOrSac==null ||itemsv.hsnOrSac.length<8 || itemsv.hsnOrSac.length>8){
      //     this.errorMessage = "HSN CODE Should contain only Eight Numbers";
      //     this.alertService.danger(this.errorMessage);
      //       return;
      //    }

      //  });


        this.invDate =  new Date(this.invoiceHeader.invoiceDate);
      }
      if(this.invoiceHeader.partyCountryId!=1)
      {
         if(ewayBillTracking.portStateId==null)
         {
          this.errorMessage = "Please select Port State";
          this.alertService.danger(this.errorMessage);
            return; 
         }
      }
    
    let status = "";
    this.errorMessage = "";
 
    if (ewayBillTracking.transactionMode) {
        if(ewayBillTracking.vehicleNo == null || ewayBillTracking.vehicleNo == ""){
        this.errorMessage = "Vehicle Number";
        this.alertService.danger("If mode of transportation is selected, then the " + this.errorMessage + " should be passed.");
          return;
      }
      if (ewayBillTracking.transactionMode == 1) {
        if ((ewayBillTracking.vehicleNo == null || ewayBillTracking.vehicleNo == "") && ewayBillTracking.vehicleType == null) {
          this.errorMessage = "Vehicle number and Type";
        } else if (ewayBillTracking.vehicleType == null) {
          this.errorMessage = "Vehicle Type";
        }
        if(this.errorMessage != ""){
          this.alertService.danger("If mode of transportation is ‘Road’, then the " + this.errorMessage + " should be passed.");
          return;
        }

      } else if (ewayBillTracking.transactionMode != 5) {
        if ((ewayBillTracking.transDocNo == null || ewayBillTracking.transDocNo == "") && ewayBillTracking.transDocDate == null) {
          this.errorMessage = "transport document number and date";
        } else if (ewayBillTracking.transDocDate == null) {
          this.errorMessage = "document date";
        } else if(ewayBillTracking.transDocNo == null || ewayBillTracking.transDocNo == ""){
          this.errorMessage = "transport document number";
        }
        if(this.errorMessage != ""){
          this.alertService.danger("If mode of transportation is Ship, Air, Rail, then the " + this.errorMessage + " should be passed.");
        return;
        }

     
      }
    }
    if (ewayBillTracking.transDocDate != null) {
      this.transDocDate = new Date(ewayBillTracking.transDocDate);
      //console.log(this.invoiceDate + " "+this.datePipe.transform(this.transDocDate, AppSettings.DATE_FORMAT))
      if(this.datePipe.transform(this.invDate, AppSettings.DATE_FORMAT) > this.datePipe.transform(this.transDocDate, AppSettings.DATE_FORMAT) ){
        this.alertService.danger("Transporter document date cannot be earlier than the Invoice Date");
        return;
      }
       
     }
    this.spinStart = true;

    this.eWayBillService.generateEWayBill(ewayBillTracking)
      .subscribe(response => {
        this.ewayBillInvoiceResponseDTO = response;

        this.ewayBillInvoiceResponseDTO.forEach(ewayBillResponse => {
          status = ewayBillResponse.message;
          if (ewayBillResponse.flag === true) {
            setTimeout(() => {
              this.alertService.success(status);
            }, 1500);
            this.getEWayBillsById(this.id.value);
           // this.disableGenerateButton = true;
          } else {
            setTimeout(() => {
              this.alertService.danger(status);
            }, 1500);
            this.disableGenerateButton = false;
          }
        })
        
         this.spinStart = false;
      }, error => {
        console.log("error: ", error);
        this.spinStart = false;
      })

    })
  }


  eWayBillCancellation(ewaybillTracking: EwayBillTracking) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    this.ewayBillTracking = ewaybillTracking;
    dialogConfig.id = "3";
     let status = "";
 
    const dialogRef = this.dialog.open(EwaybillPopupComponent, dialogConfig);
    this.getEWayBillsById(this.id.value);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.spinStart = true;
          
            ewaybillTracking.cancelledReason = data.cancelledReason;
            ewaybillTracking.cancelledRemarks = data.cancelledRemarks;

            this.eWayBillService.cancelEWayBill(ewaybillTracking)
            .subscribe(response => {
              this.ewayBillInvoiceResponseDTO = response;
              this.ewayBillInvoiceResponseDTO.forEach(ewayBillResponse => {
                status = ewayBillResponse.message
                if (ewayBillResponse.flag === true) {
                  setTimeout(() => {
                    this.alertService.danger(status);
                  }, 1500);
                  this.getEWayBillsById(this.id.value);
      
                } else {
                  setTimeout(() => {
                    this.alertService.danger(status);
                  }, 1500);
      
                }
              })
              this.spinStart = false;
      
            }, 
            error => {
              if (error instanceof HttpErrorResponse) {
                var re = "ConstraintViolationException";
                if (error.error && error.error.message.includes(re)) {
                }
              }
              this.alertService.danger("");
              this.spinStart = false;
            });
        }
      });
 
  }

  updateVehicleInfo(ewaybillTracking: EwayBillTracking, type: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    this.ewayBillTracking = ewaybillTracking;
    dialogConfig.id = type;
    const dialogRef = this.dialog.open(EwaybillPopupComponent, dialogConfig);
    this.getEWayBillsById(this.id.value);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.spinStart = true;
          if (type == "1") {
            ewaybillTracking.vehicleNo = data.vehicleNo;
            ewaybillTracking.vehicleType = data.vehicleType;
            ewaybillTracking.transDocNo = data.transDocNo;
            ewaybillTracking.transactionMode = data.transactionMode;
            ewaybillTracking.transDocDate = data.transDocDate;
            ewaybillTracking.reasonCode = data.reasonCode;
            ewaybillTracking.reasonRemarks = data.reasonRemarks;
          } else if (type == "2") {
            ewaybillTracking.transporterId = data.transporterId;
          }


          this.eWayBillService.eWayBillVehicleUpdate(ewaybillTracking, type).subscribe(response => {
            let status = "";
            this.ewayBillInvoiceResponseDTO = response;
            this.ewayBillInvoiceResponseDTO.forEach(ewayBillResponse => {
              status = ewayBillResponse.message
              if (ewayBillResponse.flag === true) {

                setTimeout(() => {
                  this.alertService.success(status);
                }, 1500);
                this.getEWayBillsById(this.id.value);

              } else {
                setTimeout(() => {
                  this.alertService.danger(status);
                }, 1500);
              }
            })
            // this.alertService.success(status); 
            this.spinStart = false;
          },
            error => {
              if (error instanceof HttpErrorResponse) {
                var re = "ConstraintViolationException";
                if (error.error && error.error.message.includes(re)) {
                }
              }
              this.alertService.danger("");
              this.spinStart = false;
            });
        }
      });
  }

  extendValidityEWayBill(ewaybillTracking: EwayBillTracking) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    this.ewayBillTracking = ewaybillTracking;
    dialogConfig.id = ewaybillTracking.transDistance+"";
    const dialogRef = this.dialog.open(EwayBillExtendValidityPopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.spinStart = true;
          ewaybillTracking.fromPlace = data.fromPlace;
          ewaybillTracking.fromState = data.fromState;
          ewaybillTracking.fromPincode = data.fromPincode;
          ewaybillTracking.remainingDistance = data.remainingDistance;
          ewaybillTracking.extensionReasonCode = data.extensionReasonCode;
          ewaybillTracking.extensionReasonRemarks = data.extensionReasonRemarks;
          ewaybillTracking.addressLine1 = data.addressLine1;
          ewaybillTracking.addressLine2 = data.addressLine2;
          ewaybillTracking.addressLine3 = data.addressLine3;

          this.eWayBillService.eWayBillExtendValidity(ewaybillTracking).subscribe(response => {
            let status = "";
            this.ewayBillInvoiceResponseDTO = response;
            this.ewayBillInvoiceResponseDTO.forEach(ewayBillResponse => {
              status = ewayBillResponse.message
              if (ewayBillResponse.flag === true) {
                setTimeout(() => {
                  this.alertService.success(status);
                }, 1500);
                this.getEWayBillsById(this.id.value);

              } else {
                setTimeout(() => {
                  this.alertService.danger(status);
                }, 1500);
              }
            })

            // this.alertService.success(status); 
            this.spinStart = false;
          },
            error => {
              if (error instanceof HttpErrorResponse) {
                var re = "ConstraintViolationException";
                if (error.error && error.error.message.includes(re)) {
                }
              }
              this.alertService.danger("");
              this.spinStart = false;
            });
        }
      });
  }

  getEWayBillByNumber(ewaybill: string) {

    let status = "";
    this.spinStart = true;
    this.eWayBillService.getEWayBillByNumber(ewaybill)
      .subscribe(response => {
        this.ewayBillInvoiceResponseDTO = response;
        this.ewayBillInvoiceResponseDTO.forEach(ewayBillResponse => {
          status = ewayBillResponse.message
          if (ewayBillResponse.flag === true) {
            this.alertService.danger(status);
          } else {
            this.alertService.danger(status);
          }
        })
        this.spinStart = false;
        this.getEWayBillsById(this.id.value);

      }, error => {
        console.log("error: ", error);
        this.spinStart = false;
      })
  }


  recentEWayBillGet(event) {
    this.eWayBillService.getEWayBillByEwayNum(event.eWayBillNo)
      .subscribe(response => {
        this.ewayBillTracking = response;
        //this.eWayBillForm = this.toFormGroup(this.ewayBillTracking);
        this.getEWayBillsById(this.ewayBillTracking.invoiceHeaderId);
      })
  }


  whenNoDataFound(noDataFound: boolean) {

    if (noDataFound) {
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });

      this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE;
    }
  }
  disableForm() {

    this.eWayBillForm.disable({ onlySelf: true, emitEvent: false });

  }
  enableForm() {

    this.eWayBillForm.enable({ onlySelf: true, emitEvent: false });

  }

  goToLink(url) {
    console.log(url);
    var win = window.open(url, '_blank');
    win.focus();
  }

  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
        this.allStates = response;
      })
  }
}
