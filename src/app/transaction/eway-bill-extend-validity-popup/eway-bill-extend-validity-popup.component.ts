import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { City } from '../../data-model/city-model';
import { EwayBillTracking, InvoiceHeader, ReasonCode, TransactionMode, VehicleType } from '../../data-model/invoice-model';
import { State } from '../../data-model/state-model';
import { LocationService } from '../../services/location.service';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { InvoiceService } from '../../services/invoice.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-eway-bill-extend-validity-popup',
  templateUrl: './eway-bill-extend-validity-popup.component.html',
  styleUrls: ['./eway-bill-extend-validity-popup.component.scss']
})
export class EwayBillExtendValidityPopupComponent implements OnInit {

  
  
  public extendValidityPopUpForm: FormGroup;
  public ewayBillTracking: EwayBillTracking;
  cities: City[] = [];
  states: State[] = [];
  themeClass: any;
  currentUser: any;
  reasonCodes: ReasonCode[] =[
    {id: 1, name: 'Natural Calamity'},
    {id: 2, name: 'Law and Order Situation'},
    {id: 3, name: 'Transshipment'},
    {id: 4, name: 'Accident'},
    {id: 99, name: 'Others'},

   ];
   transactionModes: TransactionMode[] =[
    {id: 1, name: 'Road'},
    {id: 2, name: 'Rail'},
    {id: 3, name: 'Air'},
    {id: 4, name: 'Ship'},
     
  ];

  vehicleTypes: VehicleType[] =[
    {id: 1, name: 'Regular', code: 'R'},
    {id: 2, name: 'ODC(Over Dimentional Cargo)', code: 'O'}, 
  ];
  errorMessage: string = "";
  minDate = new Date();  
  maxDate = new Date();
  invDate= new Date();
  transDocDate= new Date();
  invoiceHeader: InvoiceHeader;
  transDistance : number = 0;
  datePipe = new DatePipe('en-US');

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<EwayBillExtendValidityPopupComponent>,
    private locationService: LocationService,
    private alertService: AlertService,
    private invoiceService: InvoiceService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;
   }

  ngOnInit() {
    this.initForm();
    this.getAllStates()
    this.getAllCity()
    this.extendValidityPopUpForm.get("vehicleType").patchValue('R');
    this.extendValidityPopUpForm.get("transactionMode").patchValue(1);
    this.transDistance = Number(this.dialogRef.id);
    console.log("tra ",this.transDistance)
  }
  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
       })
  }

  private getAllCity() {

    this.locationService.getCities()
      .subscribe(response => {
        this.cities = response;
       })
  }
  private initForm() {
      let data: EwayBillTracking = {
        id: null,
        eWayBillNo: null,
        eWayBillDate: null,
        validUpto:null,
        docDate:null,
        docNo:null,
        pdfUrl:null,
        detailedpdfUrl: null,
        jsonData:null,
        invoiceHeaderId:null,
        status:null,
        error: null,
        transporterId: null,
        transporterName: null,
        transDocNo: null,
        vehicleNo: null,
        portName: null,
        portStateId:null,
        portPin: null,
        cancelledDate: null,
        response: null,
        vehicleType:  null,
        reasonCode: null,
        reasonRemarks:  null,
        transactionMode: null,
        remainingDistance:  null,
        extensionReasonCode: null,
        extensionReasonRemarks: null,
        fromPlace:  null,
        fromState: null,
        fromPincode: null,
        transDocDate: null,
        addressLine1: null,
        addressLine2: null,
        addressLine3: null,
        transDistance: null,
        noOfItems: null,
        hsnCode: null,
        isCurrent: null,
        cancelledReason: null,
        cancelledRemarks: null,
       };

      this.extendValidityPopUpForm = this.toFormGroup(data);

    }

private toFormGroup(data: EwayBillTracking): FormGroup {
  
     const formGroup = this.fb.group({
      id: [data.id],
      eWayBillNo: [data.eWayBillNo],
      eWayBillDate: [data.eWayBillDate],
      validUpto:[data.validUpto],
      docDate:[data.docDate],
      docNo:[data.docNo],
      pdfUrl:[data.pdfUrl],
      detailedpdfUrl:[data.detailedpdfUrl],
      jsonData:[data.jsonData],
      invoiceHeaderId:[data.invoiceHeaderId],
      status:[data.status],
      error: [data.error],
      transporterId: [data.transporterId ,Validators.compose([Validators.maxLength(15)])],
      transporterName:[data.transporterName],
      transDocNo: [data.transDocNo ,Validators.compose([Validators.maxLength(15)])],
      vehicleNo: [data.vehicleNo, Validators.compose([Validators.minLength(7),Validators.maxLength(15)])],
      portName: [data.portName],
      portPin: [data.portPin],
      portStateId:[data.portStateId],
      cancelledDate: [data.cancelledDate],
      response: [data.response],
      vehicleType: [data.vehicleType],
      reasonCode: [data.reasonCode],
      reasonRemarks:  [data.reasonRemarks],
      transactionMode: [data.transactionMode],
      remainingDistance:  [data.remainingDistance, Validators.required],
      extensionReasonCode: [data.extensionReasonCode, Validators.required],
      extensionReasonRemarks: [data.extensionReasonRemarks, Validators.required],
      fromPlace:  [data.fromPlace, Validators.required],
      fromState: [data.fromState, Validators.required],
      fromPincode: [data.fromPincode, Validators.compose([Validators.required,Validators.minLength(6), Validators.maxLength(6)])],
      transDocDate: [data.transDocDate],
      addressLine1:  [data.addressLine1, Validators.compose([Validators.maxLength(100)])],
      addressLine2:  [data.addressLine2, Validators.compose([Validators.maxLength(100)])],
      addressLine3:  [data.addressLine3, Validators.compose([Validators.maxLength(100)])],
      transDistance: [data.transDistance],
      noOfItems: [data.noOfItems],
      hsnCode: [data.hsnCode],
      isCurrent: [data.isCurrent],
      cancelledReason: [data.cancelledReason],
      cancelledRemarks: [data.cancelledRemarks],
    });
   
     return formGroup;
    
   } 

   save(model: EwayBillTracking) {
    this.errorMessage = "";
    this.invoiceService.getInvoice(model.invoiceHeaderId).subscribe( response =>{
      this.invoiceHeader = response;
      if(this.invoiceHeader){
        this.invDate =  new Date(this.invoiceHeader.invoiceDate);
      }
    })
     if(model.transactionMode == 1){
          if( model.vehicleNo==null || model.vehicleNo == ""){
            this.errorMessage ="If mode of transportation is ‘Road’, then the Vehicle number should be passed.";
            this.alertService.danger(this.errorMessage);
            return  ;
          } 
        
        }else if(model.transactionMode != 5){
          if( model.transDocNo==null|| model.transDocNo == ""){
            this.errorMessage = "If mode of transportation is Ship, Air, Rail, then the transport document number and date should be passed.";
            this.alertService.danger(this.errorMessage);
          return  ;
          } 
          if ((model.transDocDate == null || model.transDocDate == "")) {
            this.errorMessage = "If mode of transportation is Ship, Air, Rail, then the transport document date should be passed.";
            this.alertService.danger(this.errorMessage);
            return;
          }else{
            this.transDocDate = new Date(model.transDocDate);
            if(this.datePipe.transform(this.invDate, AppSettings.DATE_FORMAT) > this.datePipe.transform(this.transDocDate, AppSettings.DATE_FORMAT) ){
             this.alertService.danger("Transporter document date cannot be earlier than the Invoice Date");
             return;
           }
          }
        }    
     if(model.remainingDistance >= this.transDistance){
      this.alertService.danger(" Remaining distance should not be more than the actual distance mentioned during the generation of E-way bill. ");
      return  ;
     }
     this.dialogRef.close(this.extendValidityPopUpForm.value);
   }


  close(): void {
     this.dialogRef.close();
   }

}
