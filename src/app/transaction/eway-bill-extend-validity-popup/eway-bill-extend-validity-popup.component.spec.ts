import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EwayBillExtendValidityPopupComponent } from './eway-bill-extend-validity-popup.component';

describe('EwayBillExtendValidityPopupComponent', () => {
  let component: EwayBillExtendValidityPopupComponent;
  let fixture: ComponentFixture<EwayBillExtendValidityPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EwayBillExtendValidityPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EwayBillExtendValidityPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
