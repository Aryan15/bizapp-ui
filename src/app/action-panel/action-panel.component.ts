import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApplicableButtons } from '../data-model/misc-model';
import { ActivityRoleBinding, StatusBasedPermission } from '../data-model/activity-model';
import { RoutingUtilityService } from '../utils/routing-utility.service';

import { LanguageService } from '../services/language.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { TranslateService } from '@ngx-translate/core';
// import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'app-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.scss']
})
export class ActionPanelComponent implements OnInit, OnChanges {

  @Input() inForm: FormGroup;

  @Input() primaryKey: any;

  @Input() statusId: number;

  @Input() applicableButtons: ApplicableButtons;

  @Input() activityRoleBindings: ActivityRoleBinding[];

  @Input() statusBasedPermission: StatusBasedPermission;

  // @Output() closeEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() deleteEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() cancelEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() printEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() clearEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() editEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() saveAsDraftEvent: EventEmitter<number> = new EventEmitter<number>();

  @Output() saveEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() addEvent: EventEmitter<number> = new EventEmitter<number>();
  

  enableCloseButton: boolean = false;
  enableDeleteButton: boolean = false;
  enableCancelButton: boolean = false;
  enablePrintButton: boolean = false;
  enableClearButton: boolean = false;
  enableSaveButton: boolean = false;
  enableApproveButton: boolean = false;
  enableEditButton: boolean = false;
  enableDraftButton: boolean = false;
  progress: number;
  cancelProgress: number;

  constructor(private routingUtilityService: RoutingUtilityService,
    private translate: TranslateService,
    private languageService: LanguageService,
    private bottomSheet: MatBottomSheet,
    ) { translate.setDefaultLang('en'); }

  ngOnInit() {

    this.setButtonStates();
    this.languageService.getLanguage()
    .subscribe(resp => {
      if(resp===null){

        this.translate.use("en");
      }
      else{
        this.translate.use(resp);
      }
    })

  }

  ngOnChanges() {
    this.setButtonStates();
  }

  setButtonStates() {

    //Role based button enable/disable

    this.enableCloseButton = true;

    //console.log('any role binding? ', this.activityRoleBindings);

    if (this.activityRoleBindings) {
      this.activityRoleBindings.forEach(activityRoleBinding => {

        ////console.log('in action panel: ', activityRoleBinding.permissionToUpdate);
        ////console.log('in action panel: ', activityRoleBinding.permissionToCreate);
        ////console.log('in action panel activityRoleBinding.permissionToCancel: ', activityRoleBinding.permissionToCancel);

        this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
        this.enableEditButton = ((activityRoleBinding.permissionToUpdate === 1 && this.primaryKey) || this.enableEditButton) ? true : false;
        this.enableDeleteButton = (activityRoleBinding.permissionToDelete === 1 || this.enableDeleteButton) ? true : false;
        this.enableClearButton = this.enableEditButton ? true : false;
        this.enableCancelButton = (activityRoleBinding.permissionToCancel === 1 || this.enableCancelButton) ? true : false;
        this.enablePrintButton = (activityRoleBinding.permissionToPrint === 1 || this.enablePrintButton) ? true : false;
        this.enableApproveButton = (activityRoleBinding.permissionToApprove === 1 || this.enableApproveButton) ? true : false;
        this.enableDraftButton = (activityRoleBinding.permissionToDraft===1 || this.enableDraftButton)? true : false;
        // if (activityRoleBinding.permissionToUpdate === 1 && !this.enableSaveButton) {
        //   //console.log('1');
        //   this.enableSaveButton = true;
        // } else {
        //   //console.log('2');
        //   this.enableSaveButton = false;
        // }

        // if (activityRoleBinding.permissionToCreate === 1 && !this.enableSaveButton) {
        //   //console.log('3');
        //   this.enableSaveButton = true;
        // } else {
        //   //console.log('4');
        //   this.enableSaveButton = false;
        // }

        // if (activityRoleBinding.permissionToDelete === 1 && !this.enableDeleteButton) {
        //   this.enableDeleteButton = true;
        // } else {
        //   this.enableDeleteButton = false;
        // }

        // if (activityRoleBinding.permissionToCancel === 1 && !this.enableCancelButton) {
        //   this.enableCancelButton = true;
        // } else {
        //   this.enableCancelButton = false;
        // }

        // if (activityRoleBinding.permissionToPrint === 1 && !this.enablePrintButton) {
        //   this.enablePrintButton = true;
        // } else {
        //   this.enablePrintButton = false;
        // }

        // if (activityRoleBinding.permissionToApprove === 1 && !this.enableApproveButton) {
        //   this.enableApproveButton = true;
        // } else {
        //   this.enableApproveButton = false;
        // }
      })
    }

    //console.log('this.enableSaveButton: ', this.enableSaveButton);
    //console.log('this.statusId && this.statusBasedPermission: ',this.statusId , this.statusBasedPermission);

    //Another round of button enabling based on status
    //this overrides previously enabled buttons
    //Example: If the transaction is configured not to edit in 'Completed' status, then edit/save/delete/cancel will be disabled

    if (this.statusId && this.statusBasedPermission) {

      //console.log('this.statusId: ',this.statusId, this.statusBasedPermission, this.inForm.disabled);
      this.enableSaveButton = (this.statusBasedPermission.permissionToCreate === 1 || this.statusBasedPermission.permissionToUpdate === 1)  && this.enableSaveButton ? true : false;
      this.enableEditButton = (this.statusBasedPermission.permissionToUpdate === 1) && this.enableEditButton ? true : false;
      this.enableDeleteButton = (this.statusBasedPermission.permissionToDelete === 1) && this.enableDeleteButton ? true : false;
      this.enableClearButton = this.enableEditButton ? true : false;
      this.enableCancelButton = (this.statusBasedPermission.permissionToCancel === 1) && this.enableCancelButton ? true : false;
      this.enablePrintButton = (this.statusBasedPermission.permissionToPrint === 1) && this.enablePrintButton ? true : false;
      this.enableApproveButton = (this.statusBasedPermission.permissionToApprove === 1) && this.enableApproveButton ? true : false;
      this.enableDraftButton = (this.statusBasedPermission.permissionToDraft===1 || this.enableDraftButton)? true : false;
      
      //console.log('this.enableSaveButton after permissions: ', this.enableSaveButton);
    }

    // if(!this.statusBasedPermission){
    //   //For master forms, always enable edit
    //   this.enableEditButton = true;
    // }

    //console.log("this.enableEditButton: ", this.enableEditButton);

  }

  close() {
    //this.closeEvent.emit(1);
    this.routingUtilityService.close();
  }

  // delete() {
  //   this.deleteEvent.emit(1);

  // }

  delete(e) {
    if(!(this.enableDeleteButton && this.primaryKey)){
      return false;
    }
    this.progress = e / 10;
    // //console.log('progress: ', this.progress);
    if (this.progress === 100) {
      this.deleteEvent.emit(1);
    }
  }

  cancel(e) {

    if(!(this.enableCancelButton && this.primaryKey) || this.inForm.disabled){
      return false;
    }

    this.cancelProgress = e / 10;
    // //console.log('progress: ', this.cancelProgress);
    if (this.cancelProgress === 100) {
      this.cancelEvent.emit(1);
    }

    
  }

  print() {

    if (!(this.enablePrintButton && this.primaryKey)){
      return false;
    }
    this.printEvent.emit(1);
  }

  clear() {

    this.clearEvent.emit(1);
  }

  edit() {
    if(!this.enableEditButton){
      return false;
    }
    this.editEvent.emit(1);
  }

  saveAsDraft(){
    if(!this.enableSaveButton || this.inForm.disabled){
      return false;
    }
    this.saveAsDraftEvent.emit(1);
  }
  save(){
    if(!this.enableSaveButton || this.inForm.disabled){
      return false;
    }
    this.saveEvent.emit(1);
  }
  addNew(){
    if(!this.enableApproveButton){
      return false;
    }
    this.addEvent.emit(1);
  }
  // openDeleteBottomSheet(){
  //   this.bottomSheet.open(DeleteBottomSheet);
  // }

  onRightClick(){
    return false;
  }

  openDisclaimer(){
    this.bottomSheet.open(DisclaimerBottomSheet);
  }
}



@Component({
  selector: 'disclaimer',
  templateUrl: 'disclaimer.html',
})
export class DisclaimerBottomSheet {
  constructor(private bottomSheetRef: MatBottomSheetRef<DisclaimerBottomSheet>) {}

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  clickOK(){
    this.bottomSheetRef.dismiss();
  }
}

// @Component({
//   selector: 'delete-bottom-sheet',
//   templateUrl: 'delete-bottom-sheet.html',
// })
// export class DeleteBottomSheet {
//   constructor(private bottomSheetRef: MatBottomSheetRef<DeleteBottomSheet>) {}

//   openLink(event: MouseEvent): void {
//     this.bottomSheetRef.dismiss();
//     event.preventDefault();
//   }
// }