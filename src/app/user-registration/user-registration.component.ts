import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { Subscription } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Company } from '../data-model/company-model';
import { Country } from '../data-model/country-model';
import { State } from '../data-model/state-model';
import { RegistrationModel, UserModel } from '../data-model/user-model';
import { UserService } from '../services/user.service';
import { AlertDialog } from '../utils/alert-dialog';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
import { UtilityService } from '../utils/utility-service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {


  @ViewChild('username', {static: true}) usernameElement: ElementRef;
  @ViewChild('email', {static: true}) emailElement: ElementRef;

  public registrationForm: FormGroup;
  public registrationModel: RegistrationModel;

  countries: Country[] = [];
  states: State[] = [];
  allStates: State[] = [];
  hide: boolean = true;

  busy: Subscription;
  transactionStatus: string = null;
  errorMessage: string = null;
  confirmPassword : FormControl = new FormControl('', Validators.required);
  saveStatus: boolean = false;
  spinStart: boolean = false;

  showFirst: boolean = true;
  showSecond: boolean = false;

  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;

  public gstRegistrationTypes: GSTRegistrationType[] = [
    { id: 1, name: "Registered" },
    { id: 2, name: "Composition" },
    { id: 3, name: "Unregistered" }
  ]

  //   applicableButtons: ApplicableButtons ={
  //     isApproveButton: false,
  //     isClearButton: true,
  //     isCloseButton: true,
  //     isCancelButton: false,
  //     isDeleteButton: false,
  //     isPrintButton: false,
  //     isSaveButton: true,
  //     isEditButton: false,
  // };

  // activityRoleBindings: ActivityRoleBinding[];

  usernameValidated: boolean = false;
  emailValidated: boolean = false;

  constructor(private fb: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private userService: UserService,
    private alertService: AlertService,
    private utilityService: UtilityService,

    //private locationService: LocationService, 
  ) {
    // this.setActivityRoleBinding();

  }
  // setActivityRoleBinding(){
  //   this.userService.setActivityRoleBinding(this.route).subscribe(response => {
  //     this.activityRoleBindings = response;
  // })
  // }

  ngOnInit() {
    this.initForm();

    // this.getCountries();
    //this.getAllStates();

    // this.registrationForm.controls['companyDTO'].get('countryId').valueChanges.subscribe(countryId => {

      this.getStates(1);
    // });
  }

  // private getCountries() {

  //   this.userService.getCountries()
  //     .subscribe(response => {
  //       this.countries = response;
  //     })
  // }

  // private getAllStates() {

  //   this.userService.getStates()
  //     .subscribe(response => {
  //       this.states = response;
  //       this.allStates = response;
  //     })
  // }

  private getStates(countryId) {

    //this.states = this.allStates.filter(state => state.countryId === countryId);

    this.userService.getStatesForCountry(countryId)
      .subscribe(response => {
        this.states = response;
      })

  }


  closeForm() {
    this._router.navigate(['/'])

  }
  clearForm() {
    // this.transactionStatus = null;
    this.initForm();
    this.saveStatus = false;
    this.spinStart = false;
    this.transactionStatus = null;
    // this.employeeManagementForm.reset();
  }

  private initForm() {

    const data: RegistrationModel = {
      userDTO: this.initUserDTO(),
      companyDTO: this.initCompanyDTO(),
      enableJobwork: null
    }
    this.registrationForm = this.toFormGroup(data);
    this.registrationForm.get('companyDTO.stateId').markAsTouched();
  }

  submit(model: RegistrationModel) {

    // this.spinStart = true;

    if(!this.usernameValidated || !this.emailValidated){
      return;
    }


    if (!this.registrationForm.valid) {
      //console.log("invlid")
      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE);
      return false;
    }

    // this.spinStart = true;
    model.userDTO.isLogInRequired = 1;
    model.enableJobwork = model.enableJobwork ? 1 : 0;
    //console.log('Before save: ', model);

    //console.log("model......................", model);
    var passwordpattern = /^[A-Za-z\d$@$!%*?&].{4,11}$/;
    if (passwordpattern.test(model.userDTO.password) == false ||
      passwordpattern.test(this.confirmPassword.value) == false) {
      this.alertService.danger("password minimum lenght should be 5 and maximum 12");
      return
    }
    else {
      if (model.userDTO.password != null || this.confirmPassword.value != "") {
        if (model.userDTO.password === this.confirmPassword.value) {
          if (model.userDTO.username === model.userDTO.password) {
            this.alertService.danger(AppSettings.USER_NAME_AND_PASSWORD_DISTINGUISH_LINE);
            return;
          }
          else {
            this.spinStart = true;
            this.userService.saveRegisterdUser(model)
              .subscribe(response => {

                if (response.responseStatus === 1) {
                  this.userService.createCompanyAndUser(model)
                    .subscribe(response => {
                      this.saveStatus = true;
                      this.spinStart = false;
                      this.transactionStatus = AppSettings.REGISTERED_SUCESSFULL_MESSAGE;
                      this.alertService.success(this.transactionStatus);
                      setTimeout(() => {
                        this._router.navigate(['/']);
                      }, 3000);
                    },
                      error => {
                        //console.log("Error ", error);
                        this.alertService.danger(this.transactionStatus);
                        this.transactionStatus = AppSettings.REGISTER_FAILED_MESSAGE;
                        this.saveStatus = false;
                        this.spinStart = false;
                      })
                }
                else {
                  //console.log("Error ", response.responseString);
                  this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
                  this.alertService.danger(this.transactionStatus);
                  this.saveStatus = false;
                  this.spinStart = false;
                }
              }
                // error => {
                //   //console.log("Error ", error);
                //   this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
                //   this.alertService.danger(this.transactionStatus);
                //   this.saveStatus = false;
                //   this.spinStart = false;
                // }

              )
          }
        }
        else {
          this.alertService.danger("Password mismatch");
        }
      }
    }

  }
  checkUsernameAvailability() {
    let username: string = this.registrationForm.get('userDTO.username').value
    //console.log("username: ", username);
    if (username && username.length > 0) {
      this.userService.checkUsernameAvailabilityOnRegistration(username).subscribe(response => {

        //console.log("is user avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
          this.usernameValidated = true;
        } else {
          //console.log("No");
          this.usernameValidated = false;
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false
          });

          this.alertRef.componentInstance.alertMessage = response.responseString;

          this.alertRef.afterClosed().subscribe(result => {
            this.registrationForm.controls['userDTO'].get('username').patchValue('');
            this.usernameElement.nativeElement.focus();
          })


        }

      });
    }

  }

  validateEmail(){
    let email: string = this.registrationForm.get('userDTO.email').value
    //console.log("email: ", email);
    if (email && email.length > 0) {
      this.userService.checkEmailAvailabilityOnRegistration(email).subscribe(response => {

        //console.log("is email avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
          this.emailValidated = true;
        } else {
          //console.log("No");
          this.emailValidated = false;
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false
          });

          this.alertRef.componentInstance.alertMessage = response.responseString;

          this.alertRef.afterClosed().subscribe(result => {
            this.registrationForm.controls['userDTO'].get('email').patchValue('');
            this.emailElement.nativeElement.focus();
          })


        }

      });
    }
  }
  private initUserDTO(): UserModel {
    const data: UserModel = {
      id: null,
      username: null,
      email: null,
      password: null,
      name: null,
      lastName: null,
      active: null,
      roleIds: null,//[{ id: 1, role: 'Admin', roleType: { id: 1, name: 'ADMIN', deleted: 'N'} }],
      currentAddress: null,
      permanentAddress: null,
      prefixId: null,
      countryId: null,
      stateId: null,
      cityId: null,
      areaId: null,
      genderId: null,
      pinCode: null,
      employeeNumber: null,
      telephoneNumber: null,
      mobileNumber: null,
      dateOfBirth: null,
      dateOfJoin: null,
      dateOfLeaving: null,
      comments: null,
      employeeStatusId: null,
      panNumber: null,
      displayName: null,
      companyId: null,
      employeeTypeId: null,
      employeeId: null,
      isLogInRequired: null,
      deleted: null,
      userThemeName: null,
      roles: null,
      userLanguage: null,
   
    };
    return data;
  }
  private initCompanyDTO(): Company {
    const data: Company = {
      id: null,
      name: null,
      address: null,
      cityId: null,
      stateId: 29, //Default to Karnataka
      countryId: null,
      gstNumber: null,
      materialIds: null,
      deleted: null,
      pinCode: null,
      primaryTelephone: null,
      secondaryTelephone: null,
      tinNumber: null,
      tinDate: null,
      faxNumber: null,
      contactPersonName: null,
      primaryMobile: null,
      secondaryMobile: null,
      email: null,
      website: null,
      contactPersonNumber: null,
      statusId: null,
      panNumber: null,
      panDate: null,
      companyLogoPath: null,
      tagLine: null,
      gstRegistrationTypeId: null,
      addressesListDTO: null,
      stateCode: null,
      stateName: null,
      bank: null,
      branch: null,
      account: null,
      ifsc: null,
      ceritificateImagePath: null,
      signatureImagePath: null,
      msmeNumber:null,
      cinNumber:null,
      subject:null,
      iecCode:null,
      companyCurrencyId:null,
      creditLimit:null,
      insuranceType:null,
      insuranceRef :null,
      financePerson:null,
      financeEmail:null,
      
    };
    return data;
  }

  private toFormGroup(data: RegistrationModel): FormGroup {



    const formGroup = this.fb.group({
      userDTO: this.toUserFormGroup(data.userDTO),
      companyDTO: this.toCompanyFormGroup(data.companyDTO),
      enableJobwork: data.enableJobwork
    });

    return formGroup;
  }

  private toUserFormGroup(data: UserModel): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      username: [data.username, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.noWhitespaceValidator])],
      email: [data.email, Validators.compose([Validators.required, Validators.email])],
      // password: [data.password, Validators.required, Validators.pattern("?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20}")],
      // password: [data.password, Validators.required],
      password: [data.password, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.noWhitespaceValidator])],

      name: [data.name, Validators.required],
      currentAddress: [data.currentAddress],
      isLogInRequired: [data.isLogInRequired],
      employeeNumber: [data.employeeNumber],
      mobileNumber: [data.mobileNumber],
    });

    return formGroup;

  }

  public noWhitespaceValidator(control: FormControl) {
    // const isWhitespace = (control.value || '').indexOf(' ') > 0
    const isWhitespace = /\s/g.test(control.value || '');
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  private toCompanyFormGroup(data: Company): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      name: [data.name, Validators.required],
      address: [data.address, Validators.required],
      stateId: [data.stateId, Validators.required],
      countryId: [data.countryId],
      gstNumber: [data.gstNumber, Validators.compose([Validators.minLength(15),Validators.maxLength(15)])],
      primaryMobile: [data.primaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      secondaryMobile: [data.secondaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      email: [data.email, Validators.email],
      gstRegistrationTypeId: [data.gstRegistrationTypeId],
    });

    return formGroup;

  }

  getIsFirstDisabled(): boolean {

    if (this.registrationForm) {
      if (this.registrationForm.get('companyDTO.name').touched && this.registrationForm.get('companyDTO.name').valid && this.registrationForm.get('companyDTO.stateId').touched && this.registrationForm.get('companyDTO.stateId').valid && this.registrationForm.get('companyDTO.address').touched && this.registrationForm.get('companyDTO.address').valid) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  firstSection() {
    this.showFirst = false;
    this.showSecond = true;
  }

  goBack(){
    this.showFirst = true;
    this.showSecond = false;
  }

  validateGSTType(){
    let gstRegTypeId = this.registrationForm.get('companyDTO.gstRegistrationTypeId').value;
    if(gstRegTypeId === 3){
      this.registrationForm.get('companyDTO.gstNumber').disable();
    }else{
      this.registrationForm.get('companyDTO.gstNumber').enable();
    }
  }

  validatePassword() {
    //console.log("confirmPassword" + this.confirmPassword.value)

    if (this.confirmPassword.value !== "") {
      if (this.registrationForm.get('userDTO.password').value !== this.confirmPassword.value) {
        //console.log('Not matching: ', this.registrationForm.get('userDTO.password').value, this.confirmPassword);
        this.registrationForm.get('userDTO.password').setErrors({ 'validateEqual': true });
        this.registrationForm.get('userDTO.password').setErrors({ 'incorrect': true });
        this.confirmPassword.setErrors({ 'incorrect': true });
        this.registrationForm.get('userDTO.password').markAsTouched();

      }
      else {
        //console.log('Matching');
      
        this.registrationForm.get('userDTO.password').setErrors(null);
        this.registrationForm.get('userDTO.password').setErrors(null);
      }
    }
    
    if(this.registrationForm.get('userDTO.password').value === this.registrationForm.get('userDTO.username').value){
      this.registrationForm.get('userDTO.password').setErrors({ 'sameAsUsername': true });
    }else{
      this.registrationForm.get('userDTO.password').setErrors(null);
    }


    const controls = this.registrationForm;
    let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
    //console.log("invalidFieldList ", invalidFieldList)
  }

  // validatePassword() {
  //   //console.log("confirmPassword" + this.confirmPassword)



  //   if (this.confirmPassword !== "") {
  //     if (this.registrationForm.controls['userDTO'].get('password').value !== this.confirmPassword) {
  //       //console.log('Not matching: ', this.registrationForm.controls['userDTO'].get('password').value, this.confirmPassword);
  //       this.registrationForm.controls['userDTO'].get('password').setErrors({ "validateEqual": true });
  //       this.registrationForm.controls['userDTO'].get('password').setErrors({ 'incorrect': true });

  //     }
  //     else {
  //       //console.log('Matching');
  //       this.registrationForm.controls['userDTO'].get('password').setErrors(null);
  //       this.registrationForm.controls['userDTO'].get('password').setErrors(null);
  //     }
  //   }

  //   if (this.registrationForm.controls['userDTO'].get('password').invalid) {
  //     this.errorMessage = "Please enter Password ";
  //   }
  //   if (this.registrationForm.controls['userDTO'].get('password').hasError) {
  //     this.errorMessage = "Invalid Password ";
  //   }

  // }

  // (ngModelChange)="validatePassword()"
}

export interface GSTRegistrationType {
  id: number,
  name: string,
}
