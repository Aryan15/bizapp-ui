import { Component, ViewChild, OnDestroy, AfterViewInit, Inject, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { HelpVideo, HelpVideoWrapper } from '../data-model/help-video-model';
import { AppSettings } from '../app.settings';
import { MatDialogModule, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatInput } from '@angular/material';

import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';


import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-help-videos',
  templateUrl: './help-videos.component.html',
  styleUrls: ['./help-videos.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class HelpVideosComponent implements AfterViewInit {

  public helpVideo: HelpVideo;
  public safeURL: SafeResourceUrl;
  








  title: String;
  subheading: String;


  helpVideos: HelpVideo[] = [];
  temphelpVideos: HelpVideo[] = [];
  helpVideosShow: boolean = false;






  subHeadingAllow: boolean = true;
  cancelButton: boolean = true;
  activityId: string = "null";
  noData:string="";
  noDataCondition:boolean=false;

  filterForm = new FormGroup({
    videoHeader: new FormControl()
  });

  constructor(
    public route_in: ActivatedRoute,
    private _router: Router,
    private userService: UserService,
    private sanitizer: DomSanitizer,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected dialog: MatDialog,
    private dialogRef: MatDialogRef<HelpVideosComponent>

  ) {
    this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
    this.title = this.userService.activityName
    
  }


  ngAfterViewInit() {
    if (this.dialogRef.id) {
      this.subHeadingAllow = false;
      this.cancelButton = true;

    }
    else {
      this.subHeadingAllow = true;
      this.cancelButton = false;
    }




    this.getAllHelpVideos();








  }


  private getAllHelpVideos() {
    if (this.dialogRef.id) {
      this.activityId = this.dialogRef.id;
    }
    this.userService.getAllHelpVideos(this.activityId).subscribe(response => {
      console.log(response)
       if(response.length===0){
   
      this.noDataCondition=true;
    }
    else{
      this.helpVideos = response;
      this.temphelpVideos = response;
      this.helpVideos.forEach(hv => {
        hv.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(hv.videoLink);
      })
    }
    })
   
  }










  updateFilter(event) {

    const val = event.target.value.toLowerCase();
         
    this.helpVideos = this.helpVideos.filter(d => d.header.toLowerCase().indexOf(val) != -1 || !val)





  }




  clear() {
    this.helpVideos = this.temphelpVideos;
    this.filterForm.get('videoHeader').patchValue('');
    
  
  }



  closePopUp(): void {

    this.dialogRef.close();

  }

  close() {
    this._router.navigate(['/'])
  }

}

