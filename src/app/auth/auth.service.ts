import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";

import { environment } from '../../environments/environment';
import { AppSettings } from '../app.settings';
import { Menu } from '../data-model/activity-model';

@Injectable()
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable());

  loginUrl : string = environment.baseServiceUrl + environment.loginUrl;

  isLoggedIn() : Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  private tokenAvailable(): boolean {
    var currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
    if(currentUser && currentUser.token) {
      return true;
    }
    return false;
  }

  setLoggedIn(value: boolean){
    this.loggedIn.next(value);
  }
  constructor(private router: Router,
              private http: HttpClient,
              private zone: NgZone) { 

               
  }

  login(loginDetails : any): Observable<any>{
    return this
            .http
            .post<any>(this.loginUrl,loginDetails)
            // .post(this.loginUrl, JSON.stringify(loginDetails) , {headers: this.headers})
            ;

    
    //this.router.navigate(['/']);
  }

  logOut(){
        localStorage.removeItem(AppSettings.CURRENT_USER);
        localStorage.removeItem('menuForUser');
        localStorage.removeItem('transactionTypes');
        localStorage.removeItem('globalSetting');
        this.setLoggedIn(false);
        

        setTimeout(() => {          
          this.reload();
          this.router.navigate(['/login']); 
        }, 50);

  }

  public reload(): any {
    return this.zone.runOutsideAngular(() => {
      location.reload()
    });
  }

  isDashboardFound(): boolean {
    let activityPath: string = 'dashboard';
    let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));

    //console.log("storedMenu: ", storedMenu);

    if (!storedMenu) return true;

    if (storedMenu.find(m => m.activities.find(a => a.pageUrl && a.pageUrl.includes(activityPath)) ? true : false))
      return true;
    else {
      return false;
    }
  }
}
