
import { map, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Menu, Activity } from '../data-model/activity-model';
import { AppSettings } from '../app.settings';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isLoggedIn().pipe(
      take(1),
      map((isLoggedIn: boolean) => {
        if (!isLoggedIn) {
          this.router.navigate(['/login']);
          return false;
        }
        else {
          let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));

          // let activities: Activity[] = [];
          // storedMenu.forEach(element => {
          //   activities.concat(element.activities)
          // });

          //console.log("storedMenu: ", storedMenu);



          //if(activities.find(act => act.pageUrl.includes(next.routeConfig.path)))
          if (!storedMenu) return true;

          if (storedMenu.find(m => m.activities.find(a => a.pageUrl && a.pageUrl.includes(next.routeConfig.path)) ? true : false) || next.routeConfig.path.includes("app-about-company"))
            return true;
          else {
            this.router.navigate(['/404']);
            return false;
          }
        }
      }));
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isLoggedIn().pipe(
      take(1),
      map((isLoggedIn: boolean) => {
        if (!isLoggedIn) {
          this.router.navigate(['/login']);
          return false;
        }
        else {
          // let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));

          // if(!storedMenu) return true;

          // let activities: Activity[] = [];



          // storedMenu.forEach(element => {
          //   activities = activities.concat(...element.activities);

          //   element.activities.forEach(act => {
          //     if (act.subMenu && act.subMenu.activities){
          //       activities = activities.concat(...act.subMenu.activities);
          //     }
          //   });


          // });


          // //console.log("activities 3: ",activities);
          // //console.log("next.routeConfig.path: ",next.routeConfig.path);
          // let path: string = next.routeConfig.path.substring(0, next.routeConfig.path.indexOf('/'))

          // if(activities.find(act => act.pageUrl && act.pageUrl.includes(path)))
          //   return true;
          // else
          // {
          //   this.router.navigate(['/404']);
          //   return false;
          // }

          let storedMenu: Menu[] = JSON.parse(localStorage.getItem(AppSettings.LOCAL_STORAGE_MENU_FOR_USER));



          //console.log("storedMenu: ", storedMenu);




          if (!storedMenu) return true;

          //console.log("next.routeConfig.path: ", next.routeConfig.path)
          let searchString: string
          if (next.routeConfig.path.indexOf('/') > 0)
            searchString = next.routeConfig.path.substring(0, next.routeConfig.path.indexOf('/'));
          else
            searchString = next.routeConfig.path;

          //console.log("searchString: ", searchString);
          if (storedMenu.find(m => m.activities.find(a => a.pageUrl && a.pageUrl.includes(searchString)) ? true : false) || next.routeConfig.path.includes("app-about-company"))
            return true;
          else {
            this.router.navigate(['/404']);
            return false;
          }
        }
      }));
  }
}
