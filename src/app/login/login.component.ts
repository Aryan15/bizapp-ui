import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AppSettings } from '../app.settings';
import { CompanyService } from '../services/company.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    localUser = {
        username: '',
        password: ''
    }

    public token: string;
    isUnauthorized: boolean = false;
    hide: boolean = true;
    spinStart: boolean = false;

    constructor(private authService: AuthService,
        private router: Router,
        public dialog: MatDialog, 
        public route: ActivatedRoute,
        private companyService: CompanyService) { }

    ngOnInit() {
        this.LoginByReference();
    }

    // login(){
    //   this.authService.login();
    // }

    private LoginByReference() {
        this.route.params.subscribe(params => {
         
           if(params['user']){
             this.localUser.username = params['user'];
            this.localUser.password = "system@suktha";
            this.login();
           }
        })
      }

    login() {
        // this._service.loginfn(this.localUser).then((res) => {
        //     if(res)
        //         this._router.navigate(['dashboard']);
        //     else
        //         //console.log(res);
        // })
        this.spinStart = true;
        //console.log('User name: ', this.localUser.username);
        this
            .authService
            .login(this.localUser)
            .subscribe(next => {
                //let token = next.json() && next.json().token;
                // //console.log('next: ', next)
                let token = next.token;
                let user = next.user;
                // console.log('token :', token);
                // //console.log('user :', user);
                if (token) {
                    this.token = token;
                    localStorage.setItem(AppSettings.CURRENT_USER, JSON.stringify({ user: next.user, token: token }));
                }
                this.isUnauthorized = false;
                //console.log('login success');
                this.authService.setLoggedIn(true);
                this.companyService.getWithBankMap(1)
                .subscribe(resp => {
                    localStorage.setItem(AppSettings.CURRENT_COMPANY, JSON.stringify(resp));
                });

                this.companyService.getCompanyLogoAsBase64(1)
                .subscribe(response => {
                    this.companyService.companyLogoBase64 = response;
                })
                this.spinStart = false;
                this.router.navigate(['/']);
            }, error => {
                //this.error = 'Bad Credentials';
                //console.log("in error: ",error);
                if (error.status === 401) {
                    //console.log("Unauthorized");
                    this.isUnauthorized = true;
                }
                if( error.status === 403){
                    //console.log("Forced reset of password");
                    this.router.navigate(['/app-forget-password']);
                }
                this.spinStart = false;
            });

    }
}
