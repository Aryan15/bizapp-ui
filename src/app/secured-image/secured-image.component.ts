
import {map, switchMap} from 'rxjs/operators';
import { Component, OnChanges, Input } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'secured-image',
  template: `
    <img [src]="dataUrl$|async" />
  `,
  styles:[
  `
  img {
    max-width:100%;
    height:auto;
  }
  `
]
})


export class SecuredImageComponent implements OnChanges {


  @Input() private src: string;
  private src$ = new BehaviorSubject(this.src);
  ngOnChanges(): void {
    this.src$.next(this.src);
  }


  //dataUrl$ = this.src$.switchMap(url => this.loadImage(url))

  dataUrl$ : Observable<SafeUrl> = this.src$.pipe(switchMap(url => this.loadImage(url)));
  

  constructor(private httpClient: HttpClient, private domSanitizer: DomSanitizer) {
  }

  private loadImage(url: string): Observable<SafeUrl> {
    return this.httpClient
      // load the image as a blob
      .get(url, {responseType: 'blob'}).pipe(
      // create an object url of that blob that we can use in the src attribute
      //.map(e => URL.createObjectURL(e))
      map(e => this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(e))));
  }
}
