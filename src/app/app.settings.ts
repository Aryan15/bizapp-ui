export class AppSettings {


    public static length_limit :number = 600;
    public static address_limit :number = 150;
    public static CURRENT_USER: string = "myBOSCoreERPCURRENTUSER";
    public static CURRENT_COMPANY: string = "myBOSCoreERPCURRENTCOMPANY";
    public static ADMIN_ROLE_TYPE: string = "ADMIN";
    public static STAFF_ROLE_TYPE: string = "STAFF";
    public static LOCAL_STORAGE_NRC_FOR_USER: string = "myBOSCoreERPNUMRANGECONFIG";
    public static LOCAL_STORAGE_ALL_MATERIALS_FOR_USER: string = "myBOSCoreERPAllMaterials";
    public static LOCAL_STORAGE_ALL_CUSTOMERS_FOR_USER: string = "myBOSCoreERPAllCustomers";
    public static LOCAL_STORAGE_ALL_SUPPLIERS_FOR_USER: string = "myBOSCoreERPAllSuppliers";
    public static LOCAL_STORAGE_MENU_FOR_USER: string = "menuForUser";
    
    public static MATERIAL_PRICE_TYPE : MaterialPriceType = {
        buyingPrice : 1,
        sellingPrice : 2,
        MRP: 3        
    }

    public static SPECIAL_PRICE_CALCULATION : SpecialPriceCalculation = {
        yes: 1,
        no: 0
    }
    public static STOCK_CHECK_REQUIRED : StockCheckRequired = {
        yes: 1,
        no: 0
    }
    

    public static PAYMENT_METHOD : PaymentMethod = {
        auto: 1,
        manual: 2
    }
    public static TXN_PURCHASE: string = "Purchase";
    public static TXN_SELL: string = "Sell";
    public static CUSTOMER_PO: string = "Customer PO";
    public static SUPPLIER_PO: string = "Supplier PO";
    public static INCOMING_JOBWORK_PO: string = "Jobwork PO";
    public static OUTGOING_JOBWORK_PO: string = "Subcontracting PO";
    public static CUSTOMER_QUOTATION: string = "Customer Quotation";
    public static CUSTOMER_INVOICE: string = "Customer Invoice";
    public static CUSTOMER_INVOICE_TYPE_ID: string = "1.00";
    public static PURCHASE_INVOICE: string = "Purchase Invoice";
    public static PROFORMA_INVOICE: string = "Proforma Invoice";
    public static INCOMING_JOBWORK_INVOICE: string = "Jobwork Invoice";
    public static OUTGOING_JOBWORK_INVOICE: string = "Subcontracting Invoice";
    public static CUSTOMER_JOBWORK_INVOICE: string = "Customer and JobWork Invoice";
    public static CUSTOMER_TOTAL_RECEIVED: string = "Total_Received";
    public static SUPPLIER_SUBCONTRACT_INVOICE: string = "Supplier and Subcontract Invoice";
    public static SUPPLIER_TOTAL_PAID: string = "Total Paid";
    public static CUSTOMER_RECEIVABLE : string = "Customer Receipts";
    public static SUPPLIER_PAYABLE: string = "Supplier Payments";
    public static CUSTOMER_DC: string = "Customer DC";
    public static INCOMING_DC: string = "Incoming DC";
    public static CREDIT_NOTE: string = "Credit Note"
    public static DEBIT_NOTE: string = "Debit Note"
    public static TXN_ITEM_OF_PO: string =  "PO Item";
    public static TXN_NOTE: string = "Note";
    public static INCOMING_JOBWORK_IN_DC: string = "Jobwork In DC";
    public static INCOMING_JOBWORK_OUT_DC: string = "Jobwork Out DC";
    public static OUTGOING_JOBWORK_OUT_DC: string = "Subcontracting Out DC";
    public static OUTGOING_JOBWORK_IN_DC: string = "Subcontracting In DC";
    public static GRN_TYPE_SUPPLIER: string = "GRN";
    public static INCOMING_JOBWORK_PROFORMA_INVOICE: string = "Jobwork Proforma Invoice";
    public static JOBWORK_QUOTATION: string = "Jobwork Quotation";
    public static PARTY_TYPE_CUSTOMER: string = "Customer"
    public static PARTY_TYPE_SUPPLIER: string = "Supplier"
    public static PARTY_TYPE_CUSTOMER_CODE: string = "Customer Code"
    public static PARTY_TYPE_SUPPLIER_CODE: string = "Supplier Code"
    public static PARTY_CTYPE: number = 26;
    public static PARTY_STYPE: number = 27;
    public static INCOMING_JOBWORK_CREDIT_NOTE: string = "Jobwork Credit Note";
    public static OUTGOING_JOBWORK_DEBIT_NOTE: string = "Subcontract Debit Note";


    public static PARTY_TYPE_JOBWORK: string = "Jobwork"
    public static PARTY_TYPE_BOTH: string = "Both"
    public static PARTY_TYPE_VENDOR: string = "Vendor"
    public static PARTY_TYPE_ALL: string = "ALL"

    public static TXN_ITEM_OF_QUOTATION_IN_PO: string = "Item Of Quotation In PO";
    public static TXN_ITEM_OF_PO_IN_DC: string = "Item Of PO In DC";    
    public static TXN_ITEM_OF_PO_IN_JW_DC: string = "Item Of PO In JW DC";    
    public static TXN_ITEM_OF_PO_IN_INVOICE: string = "Item Of PO In Invoice";    
    public static TXN_ITEM_OF_DC_IN_INVOICE: string = "Item Of DC In Invoice";
    public static TXN_ITEM_OF_GRN_IN_INVOICE: string ="Item Of GRN In Invoice";
    public static TXN_ITEM_OF_RECENT_QUOTATION: string = "Item Of Recent Quotation";
    public static TXN_ITEM_OF_RECENT_PO: string = "Item Of Recent PO";   
    public static TXN_ITEM_OF_RECENT_DC: string = "Item Of Recent DC"   
    public static TXN_ITEM_OF_RECENT_INVOICE: string ="Item Of Recent Invoice";
    public static TXN_ITEM_OF_DC_IN_DC: string ="Item Of DC In DC";
    public static TXN_ITEM_OF_RECENT_NOTE: string ="Item Of Recent Note";
    public static TXN_ITEM_OF_PI_IN_INVOICE: string = "Item of PI In Invoice"
    public static TXN_ITEM_OF_SOURCE_INVOICE: string = "Item of Source Invoice For Notes";
    public static TXN_ITEM_OF_DC_IN_INCOMING_JW_OUT_DC: string = "Item of Source DC for Jobwork OUT DC";
    public static SAVE_SUCESSFULL_MESSAGE: string =" Saved Successfully";
    public static REGISTERED_SUCESSFULL_MESSAGE: string =" Registered Successfully";
    public static DRAFT_SUCESSFULL_MESSAGE: string =" Drafted Successfully";
    public static SAVE_FAILED_MESSAGE: string= " Failed to save";
    public static REGISTER_FAILED_MESSAGE: string= " Failed to Register";
    public static DELETE_SUCESSFULL_MESSAGE: string =" Deleted Successfully";
    public static DELETE_FAILED_MESSAGE: string= " Failed to delete";
    public static BANK_DELETE_DUPLICATE_ERROR: string= " Bank Already used. Cannot Delete";
    public static DELETE_LAST_ITEM_MESSAGE: string= "Cannot Delete!! You need one default Financial Year";
    public static UPDATE_SUCESSFULL_MESSAGE: string =" Updated Successfully"
    public static NO_DATA_FOUND_MESSAGE: string =" No Data found";
    public static DELETE_CONFIRMATION_MESSAGE: string =" Data cannot be recovered once the transaction is deleted. Are you sure you want to delete?";
    public static DUPLICATE_MATERIAL_CONFIRMATION_MESSAGE: string =" Material(s) with similar name already exists. Do you want to save? ";
    public static PAYMENT_AMOUNT_RESET_MESSAGE: string =" Item level TDS amounts and/or note amounts will be reset. Do you want to continue?";
    public static PRICE_CONFIRMATION_MESSAGE: string =" is more than the entered price... Do you want to continue? \n"
    public static PAYMENT_AMOUNT_MESSAGE : string =" Payment amount should be less than total amount";
    public static DATE_FORMAT : string = 'yyyy-MM-dd';
    public static DATE_FORMAT_DISPLAY : string = 'dd/MM/yyyy';
    public static SAVE_AS_DRAFT_STATUS : string = 'Draft';
    public static SAVE_AS_COMPLTED_STATUS : string = 'Completed';

    public static REPORT_PAGE_SIZE: number = 10;
    public static MAX_IMAGE_SIZE: number = 200;

    public static SUPPLIER_BALANCE_REPORT = "Supplier Balance Report";
    public static CUSTOMER_BALANCE_REPORT = "Customer Balance Report";
    public static PR_ITEM_INVOICE_ITEM = "InvoiceItem";
    public static PR_ITEM_RECENT_ITEM = "RecentItem";
    public static PR_ITEM_PATCH_DUE = "PatchDue";
    public static INVOICE_CANCEL_CONFIRMATION_MESSAGE = "Transaction cannot be reverted once it is cancelled. Are you sure you want to cancel?";

    public static INVOICE_CANCEL_STATUS = "Cancelled";
    public static TERMS_AND_CONDITION_MESSAGE ="Can't make two default values";
    public static STOCK_CONFIRMATION_MESSAGE = "Quantity entered is more than the current available stock. Would you like to continue?";
    public static MINIMUM_STOCK_CONFIRMATION_MESSAGE = "After this transaction, stock quantity for this material will fall below the minimum quantity that you are required to maintain. Please reorder if required.";
    
    public static CURRENT_STOCK_BELOW_MINIMUM_STOCK_CONFIRMATION_MESSAGE = "Current stock quantity is less than the minimum stock quantity that you are required to maintain. Please reorder if required";

    public static UNSAVED_CHANGES_MESSAGE = "You have unsaved changes. Are you sure you want to navigate away?"
    public static MANDOTARY_ERROR_MESSAGE =  " Please enter "
    public static COUNT_INVOICE_MESSAGE =  " Please enter count more than one "
    public static PHONE_UNIQUE_NUMBER_MESSAGE =  "Phone Number already exists!!Please try again."
    public static EMAIL_UNIQUE_NUMBER_MESSAGE =  "E-mail id has already been used. Please use a different e-mail id"
    public static GST_UNIQUE_NUMBER_MESSAGE =  "GST Number already exists!!Please try again."
    public static GST_UNIQUE_NUMBER_CONFIRM_MESSAGE =  "This GST number already exists for another party. Are you sure you want to continue?"
    public static PART_NUMBER_UNIQUE_NUMBER_MESSAGE =  "Duplicate Part Number,Cannot Save"
    public static TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE =  "already exists!!."
    public static PRINT_ORIGINAL = "ORIGINAL"
    public static PRINT_DUPLICATE = "DUPLICATE"   
    public static PRINT_TRIPLICATE = "TRIPLICATE"
    public static PRINT_ALL = "ALL"
    public static CHEQUE_VOUCHER="Cheque Voucher"
    public static PETTY_CASH="PETTY CASH"
    public static CASH_VOUCHER="Cash Voucher"
    public static STATUS_NEW ="New"
    public static STATUS_OPEN ="Open"
    public static LOG_OUT_CONFIRMATION_MESSAGE = "Do you want to Logout from Application?";
    public static TNC_POINTS_LIMIT = 11;
    public static PARTY_POINTS_LIMIT = 3;
    public static TNC_POINTS_ERROR_MESSAGE = "Terms And Conditions cannot be more than 10 lines or 600 characters!";
    public static PARTY_POINTS_ERROR_MESSAGE = "Party address cant be more than 3 lines and in each 150 characters!";
    public static SUB_HEADING_LINE = "this page is used to create and maintain ";

    public static REPORT_SUB_HEADING_LINE = "This displays ";
    public static USER_NAME_AND_PASSWORD_DISTINGUISH_LINE = "username and password can not be same";
    public static ROLE_TYPE_SYSTEM = "SYSTEM";
    public static PARTY_STATUS_CONFIRMATION_MESSAGE = "this party is already used in transaction do you want to change?";
    public static PARTY_CODE_CONFIRMATION_MESSAGE = "This party code is already used do you still want to use?";
    public static EXPENSE_CONFIRMATION_MESSAGE = "Expense already used in transaction do you still want to change?";
    public static PRINT_TEMPLATE_TOP_MARGIN = 180;
    public static SUBCONTRACT_INVOICE_TEXT = "SUBCONTRACT INVOICE";
    public static TAX_INVOICE_TEXT = "TAX INVOICE";
    public static MATERIAL_TYPE_JOBWORK: string = "Jobwork"
    public static PO_OPEN_ENDED_CONFORMATION_MSG: string = " PO quantity will be reset to 1. Are you sure you want to continue?";
    public static FINANCIAL_YEAR_AUTONUMBER_RESET: string = " All transaction related to previous financial year have to be entered in the software before switching to new Financial year  After switching to new financial year, Transaction number will be reset to 1  for the transactions for which Auto reset has been enabled in settings (edited)";
   
    public static PAYMENT_ERROR_MESSAGE =  "Paid amount should not be Negative";
    public static DUPLICATE_INVOICE_MESSAGE =  "Duplicate Invoice exists";
    public static APPROVE_CONFIRMATION_MESSAGE: string ="After Approveing the transaction u can't Edit "
    // public static TRANACTION_TYPE_ID_CUSTOMER: number = 26;
    // public static TRANACTION_TYPE_ID_SUPPLIER: number = 27;
    public static AUTO_TRANSACTION_CONFORMATION_MESSAGE: string = "changes should get affected only to newly generating recurring invoices. Old invoices should remain same";
    public static AUTO_TRANSACTION_STOP_CONFORMATION_MESSAGE: string="Once you stop transaction,you cannot resume it again.Are you sure you want to stop auto generation of invoices?";
    public static REUSE_INVOICE: string="This option is used to change all details of this invoice. This invoice will be deleted and a fresh invoice needs to be created using same invoice number";
    public static STOCK_INCREASE: string="There will be increase in stock, based on the option selected here. Example: If 'GRN' is selected here, when a 'GRN' is created, stock quantity will get increased in the system";
    public static STOCK_DECREASE: string="There will be decrease in stock, based on the option selected here. Example : If 'Customer Invoice' is selected here, when a 'Customer Invoice' is created, stock quantity will get decreased from the system";
    public static ITEM_LEVEL: string="If this option is selected, discount & tax calculation will be at individual item level i.e different items in the same Quotation/PO/Invoice can have different Discount Rates/Amount and different GST rates otherwise  There can be only one Discount Rate/Amount & GST rate for the whole Quotation/PO/Invoice";
    public static INCLUSIVE_TAX: string="If this option is selected, the amount entered for a particular item will be considered as the amount which includes GST also Otherwise It will be considered as Amount + GST";
    public static ROUND_OFF: string="If this option is selected Grand total amount will be rounded off to the nearest rupee in all transactions";
    public static SPECIAL_PRICE: string="If this option is selected, different selling prices/buying prices can be maintained for different customers/suppliers in party master";
    public static STOCK_CHECK: string="If this option is selected, a warning message will be displayed if the user tries to enter Sale Quantity more than the Current Stock quantity available in the system";
    public static PAYMENT_AMMOUNT_MESSAGE =  "Please Enter Payment Amount";


    public static PO_NAME: string = "Po No :";
    public static QUOT_NAME: string = "Quot No :";
    public static DC_NAME: string = "DC No :";
   
    public static GRN_NAME: string = "Grn No :";
    public static CREDIT_NAME: string = "CN No :";
    public static DEBIT_NAME: string = "DN No :";
    public static INV_NAME: string = "Invoice No :";
   
    public static PAY_NAME: string = "Pay No :";
    public static REC_NAME: string = "Rec No :";

    public static PO_DATE: string = "Po Date :";
    public static QUOT_DATE: string = "Quot Date :";
    public static DC_DATE: string = "DC Date :";
   
    public static GRN_DATE: string = "Grn Date :";
    public static CREDIT_DATE: string = "CN Date :";
    public static DEBIT_DATE: string = "DN Date :";
    public static INV_DATE: string = "Invoice Date :";
   
    public static PAY_DATE: string = "Pay Date :";
    public static REC_DATE: string = "Rec Date :";
    public static TCS_CONFIRM: string = "TCS value has been reset as there was some modification in the transaction. If TCS is required,please recalculate and enter the value again";
    public static CLOSE_PO_CONFIRM: string ="Do you Want to close Po?"

}

export interface MaterialPriceType {
    buyingPrice: number;
    sellingPrice: number;
    MRP: number;
}

export interface SpecialPriceCalculation{
    yes: number;
    no: number;
}

export interface StockCheckRequired{
    yes: number;
    no: number;
}


export interface PaymentMethod{
    auto: number;
    manual: number;
}
