export interface PrintCopy{
    id : number;
    deleted : string;
    name : string;
}

export interface PrintCopiesWrapper {
    printCopiesList: PrintCopy[];
}