export interface VoucherHeader{
    id: string;
    
    amount:number;

    voucherNumber : string;
    
    voucherId:Number;
    
    voucherDate:string;
    
    voucherTypeId: number;
    
    comments: string;
    
    paidTo: string;
    
    chequeNumber: string;
    
    bankName: string;
    
    chequeDate : string;

    financialYearId: number;

    companyId: number;
    
    voucherItems : VoucherItem[];
    // ............
    companyName: string;
    companyGstRegistrationTypeId : number;
    companyPinCode: string;
    companyStateId : number;
    companyStateName: string,
    companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
    companyContactPersonNumber : string;
    companyContactPersonName : string;
    companyPrimaryTelephone : string;
    companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
    companyFaxNumber : string;
    companyAddress: string;
    companyTagLine: string;
    companyGstNumber : string;
    companyPanNumber : string;
    companyPanDate : string;
    companyCeritificateImagePath: string;
    companyLogoPath: string;
    createdBy:string;
    updateBy:string;
    createdDate:string;
    updatedDate:string;

}

export interface VoucherItem{
    id : string;
    
    voucherHeaderId: string;
    
    description: string;

    expenseHeaderId:number;

    expenseName:string;
    
    amount: number;
}

export interface VoucherWrapper{
	itemToBeRemove: string[],
	voucherHeader: VoucherHeader
}

export interface ExpenseHeader {
	
	id : number;
	name : string;
	deleted :  string;
}


export interface RecentVoucher {
    
    voucherHeaders: VoucherHeader[],
        totalCount: number;
    
    }

export interface Expense {
	
    id : number;
    name : string;
    deleted :  string;
}
    
export interface ExpenseWrapper {
    expenses : Expense[]
}

export interface VoucherReport {

	voucherReportItems: VoucherReportModel[];
	totalCount: number;
}

export interface VoucherReportModel{
	id: string;
	
	amount: number;

	voucherNumber: string;
	
	voucherId: number;
	
	voucherDate: string;
	
	voucherTypeId: number;
	
	comments: string;
	
	paidTo: string;
	
	chequeNumber: string;
	
	companyId: number;
	
	financialYearId: number;
	
	bankName: string;
	
	chequeDate: string;
	
	itemId: string;
	
	description: string;
	
	itemAmount: number;
	
	expenseHeaderId: number;
	
	expenseName: string;
}


