export interface PayableReceivableHeader {
	id: string;
	payReferenceNumber: string;
	payReferenceId: number;
	payReferenceDate: string;
	partyId: number;
	paymentMethodId: number;
	cardTypeId: number;
	paymentDocumentNumber: string;
	bankName: string;
	paymentDocumentDate: string;
	paymentNote: string;
	clearingMode: string;
	paymentStatus: string;
	statusId: number;
	financialYearId: number;
	companyId: number;
	transactionTypeId: number;
	paymentAmount: number;
	isPost: number; 
	statusName: string;
	partyName: string;
	payableReceivableItems: PayableReceivableItem[];
	creditDebitNumber: string[];
	creditDebitId: string[];
	paymentMode:string;
	// ................
	companyName: string;
    companyGstRegistrationTypeId : number;
    companyPinCode: string;
	companyStateId : number;
	companyStateName: string,
    companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
    companyContactPersonNumber : string;
    companyContactPersonName : string;
    companyPrimaryTelephone : string;
    companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
    companyFaxNumber : string;
    companyAddress: string;
    companyTagLine: string;
    companyGstNumber : string;
    companyPanNumber : string;
    companyPanDate : string;
    companyCeritificateImagePath: string;
    companyLogoPath: string;
    // partyName: string;
    partyContactPersonNumber: string;
    partyPinCode: string;
    partyAreaId: number;
    partyCityId: number;
    partyStateId: number;
    partyCountryId: number;
    partyPrimaryTelephone: string;
    partySecondaryTelephone: string;
    partyPrimaryMobile: string;
    partySecondaryMobile: string;
    partyEmail: string;
    partyWebsite: string;
    partyContactPersonName: string;
    partyBillToAddress: string;
    partyShipAddress: string;
    partyDueDaysLimit: number;
    partyGstRegistrationTypeId: number;
    partyGstNumber: string
    partyPanNumber: string;
	isIgst: number;
	partyCode:string;
	createdBy:string;
    updateBy:string;
    createdDate:string;
    updatedDate:string;
}

export interface PayableReceivableItem {
	id: string;
	payableReceivableHeaderId: string;
	invoiceHeaderId: string;
	invoiceNumber: string;
	invoiceDate: string;
	paymentDueDate: string;
	invoiceAmount: number;
	paidAmount: number;
	dueAmount: number;
	payingAmount: number;
	invoiceStatus: string;
	paymentItemStatus: string;
	tdsPercentage: number;
	tdsAmount: number;
	amountAfterTds: number;
	debitAmount: number;
	creditDebitHeaderId: string[];
	originalDueAmount: number;
	originalDebitAmount: number;
	slNo: number;
	taxableAmount:number;
	taxAmount:number;
	preTdsPer:number;
	preTdsAmount:number;
	roundOff:number;
}

export interface PayableReceivableReportRequest {

	partyId: number;

	transactionFromDate: string;

	transactionToDate: string;

	financialYearId: number;

	materialId: number;

	partyTypeIds: number[];

	statusName:string;
}

export interface PayableReceivableReportModel {

	id: string;
	invoiceNumber: string;
	invoiceDate: string;
	invoiceAmount: string;
	tdsPercentage: number;
	tdsAmount: number;
	amountAfterTds: number;
	paidAmount: number;
	payingAmount: number;
	bankName: string;
	paymentMethodName: string;
	dueAmount: number;
	partyName: string;
	paymentDocumentNumber: string;
	payReferenceDate: string;
	payReferenceNumber: string;
	statusName: string;
}

export interface BalanceReportModel {

	id: string;
	invoiceNumber: string;
	invoiceDate: string;
	invoiceAmount: string;
	paidAmount: number;
	dueAmount: number;
	partyName: string;
	gstin: string;
	statusName: string;
	internelRefNumber:string;
}

export interface PayableReceivableReport {

	payableReceivableReportItems: PayableReceivableReportModel[];
	balanceReportModels: BalanceReportModel[];
	totalCount: number;
}

export interface RecentPayableReceivable {

	payableReceivableHeaders: PayableReceivableHeader[],
	totalCount: number;

}

export interface PayableReceivableWrapper{
	payableReceivableHeader: PayableReceivableHeader,
	itemToBeRemove: string[]
}