import { City } from './city-model';

export interface Area {
    id : number;
    name : string;
    cityId : number;
    deleted : string;
}

export interface AreaWrapper{
    areas: Area[];
}