export interface GrnHeader {

	id: string;
	grnId: number;
	grnNumber: string;
	grnDate: string;
	purchaseOrderHeaderId: string;
	comments: string;
	verbal: number;
	grnStatusId: number;
	statusName: string;
	companyId: number;
	qualityCheckedById: number;
	stockCheckedById: number;
	financialYearId: number;
	supplierId: number;
	deliveryChallanHeaderId: string;
	deliveryChallanNumber:string;
	deliveryChallanDate:string;
	purchaseOrderNumber: string;
	purchaseOrderDate: string;
	termsAndConditionNote: string;
	termsAndConditionId: number;
	invoiceHeaderIds: string[];
	grnTypeId: number;
	grnItems: GrnItem[];
	supplierName: string;
	taxId: number;
	supplierAddress: string;
	supplierGSTN: string;
	stockCheckedByName : String
	qualityCheckedByName : String
	inclusiveTax: number

	// .................
	companyName: string;
	companyGstRegistrationTypeId : number;
	companyPinCode: string;
	companyStateId : number;
	companyStateName: string,
	companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
	companyContactPersonNumber : string;
	companyContactPersonName : string;
	companyPrimaryTelephone : string;
	companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
	companyFaxNumber : string;
	companyAddress: string;
	companyTagLine: string;
	companyGstNumber : string;
	companyPanNumber : string;
	companyPanDate : string;
	companyCeritificateImagePath: string;
	companyLogoPath: string;
	// partyName: string;
	partyContactPersonNumber: string;
	partyPinCode: string;
	partyAreaId: number;
	partyCityId: number;
	partyStateId: number;
	partyCountryId: number;
	partyPrimaryTelephone: string;
	partySecondaryTelephone: string;
	partyPrimaryMobile: string;
	partySecondaryMobile: string;
	partyEmail: string;
	partyWebsite: string;
	partyContactPersonName: string;
	partyBillToAddress: string;
	partyShipAddress: string;
	partyDueDaysLimit: number;
	partyGstRegistrationTypeId: number;
	partyGstNumber: string
	partyPanNumber: string;
	isIgst: number;
	partyCode:string;
	createdBy:string;
	updateBy:string;
	createdDate:string;
	updatedDate:string;
  
}

export interface GrnItem {
	id: string;
	grnHeaderId: string;
	materialId: number;
	materialName: string;
	deliveryChallanQuantity: number;
	acceptedQuantity: number;
	rejectedQuantity: number;
	purchaseOrderHeaderId: string;
	invoiceBalanceQuantity: number;
	remarks: string;
	amount: number;
	deliveryChallanHeaderId: string;
	deliveryChallanItemId: string;
	unitOfMeasurementId: number;
	unitPrice: number;
	materialSpecification: string;
	taxId: number;
	status: string;
	grnItemStatus: string;
	discountPercentage: number;
    discountAmount: number;
    amountAfterDiscount: number;
	sgstTaxPercentage: number;
	sgstTaxAmount: number;
	cgstTaxPercentage: number;
	cgstTaxAmount: number;
	igstTaxPercentage: number;
	igstTaxAmount: number;
	taxAmount: number;
	partNumber: string;
	hsnOrSac: string;
	partName: string;
	uom: string;
	inclusiveTax: number;
	slNo: number;
}

export interface RecentGrn {

	grnHeaders: GrnHeader[],
	totalCount: number;

}

export interface GrnWrapper{
	itemToBeRemove: string[],
	grnHeader: GrnHeader
}