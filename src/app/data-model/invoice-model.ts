import { DeliveryChallanHeader } from './delivery-challan-model';
import { PurchaseOrderHeader } from './purchase-order-model';

export interface InvoiceHeader {
    id: string;
    invoiceDate: string;
    gstNumber: string;
    invoiceNumber: string;
    purchaseOrderDate: string;
    // customerId : number ;
    // customerName : string;
    purchaseOrderNumber: string;
    referenceDate: string;
    address: string;
    internalReferenceNumber: string;
    deliveryChallanDate: string;
    deliveryChallanNumber: string;
    proformaInvoiceNumber: string;
    proformaInvoiceDate: string;
    sourceInvoiceNumber: string;
    sourceInvoiceDate: string;
    grnNumber : string;
    grnDate : string;
    stateId: number;
    stateName: string;
    currencyName:string;
    paymentDueDate: string;
    eWayBillNumber: string;
    totalTaxableAmount: number;
    totalIgstTax: number;
    totalCgstTax: number;
    grandTotal: number;
    billToAddress: string;
    labourChargesOnly: number;
    subTotalAmount: number;
    paymentTerms: string;
    deliveryTerms: string;
    termsAndConditions: string;
    modeOfDispatch: string;
    vehicleNumber: string;
    numOfPackages: string;
    documentThrough: string;
    shipToAddress: string;
    totalDiscount: number;
    netAmount: number;
    transportationCharges: number;
    advanceAmount: number;
    isRoudedOff: string;
    roundOffAmount: number;
    invId: number;
    cessAmount: number;
    cessPercent: number;
    sgstTaxRate: number;
    sgstTaxAmount: number;
    cgstTaxRate: number;
    cgstTaxAmount: number;
    igstTaxRate: number;
    igstTaxAmount: number;

    companyId: number;
    discountAmount: number;
    discountPercent: number;
    dueAmount: number;
    financialYearId: number;
    internalReferenceDate: string;
    //invoiceStatus: string;
    invoiceTypeId: number;
    isReverseCharge: number;
    partyId: number;
    partyName: string;

    invoiceItems: InvoiceItem[];
    taxId: number;
    taxAmount: number;
    timeOfInvoice: string;
    vendorCode: string;
    statusId: number;
    statusName: string;
    //New fields
    roundOffDecimal: string;
    remarks : string;
    inclusiveTax: number;
    deliveryChallanDateString: string;
    
        // .................
        companyName: string;
        companyGstRegistrationTypeId : number;
        companyPinCode: string;
        companyStateId : number;
        companyStateName: string,
        companyCountryId: number;
        companyPrimaryMobile : string;
        companySecondaryMobile : string;
        companyContactPersonNumber : string;
        companyContactPersonName : string;
        companyPrimaryTelephone : string;
        companySecondaryTelephone : string;
        companyWebsite : string;
        companyEmail : string;
        companyFaxNumber : string;
        companyAddress: string;
        companyTagLine: string;
        companyGstNumber : string;
        companyPanNumber : string;
        companyPanDate : string;
        companyCeritificateImagePath: string;
        companyLogoPath: string;
        // partyName: string;
        partyContactPersonNumber: string;
        partyPinCode: string;
        partyAreaId: number;
        partyCityId: number;
        partyStateId: number;
        partyCountryId: number;
        partyPrimaryTelephone: string;
        partySecondaryTelephone: string;
        partyPrimaryMobile: string;
        partySecondaryMobile: string;
        partyEmail: string;
        partyWebsite: string;
        partyContactPersonName: string;
        partyBillToAddress: string;
        partyShipAddress: string;
        partyDueDaysLimit: number;
        partyGstRegistrationTypeId: number;
        partyGstNumber: string
        partyPanNumber: string;
        isIgst: number;
        isReused:number;
        reusedInvoiceId:string;
        partyCode:string;
        transactionStatus:string;
        tcsPercentage:number;
        tcsAmount:number;
        createdBy:string;
        updateBy:string;
        createdDate:string;
        updatedDate:string;
        materialNoteType:number;
        currencyId:number;
      //  exportAmtInWords:string;
        currencyRate:number;
        totalAmountCurrency:number;
        finalDestination:string;
        vesselNumber:string;
        shipper:string;
        cityOfLoading:string;
        cityOfDischarge:string;
        countryOfOriginOfGoods:string;
        countryOfDestination:string;
}

export interface InvoiceItem {
    id: string;

    headerId: string;
    slNo: number;
    materialId: number;
    quantity: number;
    price: number;
    amount: number;
    discountPercentage: number;
    discountAmount: number;
    amountAfterDiscount: number;
    transportationAmount: number;
    cessPercentage: number;
    cessAmount: number;
    igstTaxPercentage: number;
    igstTaxAmount: number;
    sgstTaxPercentage: number;
    sgstTaxAmount: number;
    cgstTaxPercentage: number;
    cgstTaxAmount: number;
    amountAfterTax: number;
    remarks: string;
    purchaseOrderHeaderId: string;
    purchaseOrderItemId: string;
    deliveryChallanHeaderId: string;
    deliveryChallanItemId: string;
    proformaInvoiceHeaderId: string;
    proformaInvoiceItemId: string;
    grnHeaderId: string;
    grnItemId: string;
    unitOfMeasurementId: number;
    taxId: number;
    //new fields
    partNumber: string;
    specification :string;
    hsnOrSac: string;
    partName: string;
    uom: string;
    inclusiveTax: number;
	processId: string;
    processName: string;
    isContainer : number;
    outName: string;
    outPartNumber: string;
  
}

export interface RecentInvoice {

    invoiceHeaders: InvoiceHeader[],
    totalCount: number;

}

export interface InvoiceWrapper{
    invoiceHeader: InvoiceHeader,
    itemToBeRemove: string[]
}


export interface InvoiceReportModel {

    id: string;

    invoiceNumber: string;

    invoiceDate: string;

    sourceInvoiceNumber : string;
    sourceInvoiceDate : string;


    partyName: string;

    totalTaxableAmount: number;

    cgstTaxRate: number;


    cgstTaxAmount: number;


    sgstTaxRate: number;
    statusName: string;

    sgstTaxAmount: number;


    igstTaxRate: number;


    igstTaxAmount: number;


    advanceAmount: number;

    discountAmount: number;
    
    grandTotal: number;

    amount: number;

    inclusiveTax: number;

   tcsPercentage: number;

     tcsAmount: number;

     gstNumber:string;

    
  
}
export interface InvoiceReport {
    
        invoiceReports: InvoiceReportModel[];
        totalCount: number;
    }

export interface InvoiceItemReport {

    invoiceReportItems: InvoiceReportItemModel[];
    totalCount: number;
}

export interface InvoiceReportItemModel {

    id: string;

    invoiceNumber: string;

    invoiceDate: string;


    partyName: string;

    totalTaxableAmount: number;

    cgstTaxRate: number;


    cgstTaxAmount: number;


    sgstTaxRate: number;


    sgstTaxAmount: number;


    igstTaxRate: number;


    igstTaxAmount: number;


    advanceAmount: number;


    grandTotal: number;
    uom: string;
    description: string;
    partNumber: string;
    invoiceHeaderId: string;
    quantity: number;
    price: number;
    discountAmount: number;
    statusName: string;
    amountAfterDiscount: number
	inclusiveTax: number;
    taxableAmount: number;
    gstNumber:string;
    hsnCode:string;
    internalReferenceNumber:string;
}

export interface InvoiceTaxReportModel{
    id: string;
    partyName: string;
    gstNumber: string ;
    invoiceNumber: string;
    invoiceDate: string; 
    netAmount: number;
    cgstTaxPercent: number;
    cgstTaxAmount: number;
    sgstTaxPercent: number;
    sgstTaxAmount: number;
    igstTaxPercent: number;
    igstTaxAmount: number;
    grandTotal: number;
    statusName: string;
 
}

export interface InvoiceTaxReport {
    
    invoiceTaxReports: InvoiceTaxReportModel[];
    totalCount: number;
}


export interface B2BGSTReport{
    id: string;
	gstin: string;
	invoiceNumber: string;
	invoiceDate: string;
	invoiceValue: number;
	stateCode: string;
	reverseCharge: string;
	applicablePercentOfTaxRate: number;
	invoiceType: string;
	eCommerceGstn: string;
	serialNumber: number;
	rate: number;
	taxableValue: number;
	integratedTax: number;
	centralTax: number;
	stateTax: number;
    cess: number;
  
}


export interface B2BGSTReportWrapper{
    b2bGSTReports: B2BGSTReport[];	
	totalCount: number;
}

export interface EwayBillTracking{
    id: number;
    eWayBillNo: string;
    eWayBillDate: string;
    validUpto: string;
    docDate: string;
    docNo: string;
    pdfUrl: string;
    detailedpdfUrl: string;
    jsonData: string;
    invoiceHeaderId: string;
    status: number;
    error: string;
    transporterId: string;
    transporterName: string;
    transDocNo: string;
    transDocDate: string;
    vehicleNo: string;
    portName: string;
    portPin: string;
    portStateId:number;
    cancelledDate: string;
    response: string;
    vehicleType: string;
    reasonCode: number;
    reasonRemarks: string;
    transactionMode: number;
    remainingDistance: number;
    extensionReasonCode: number;
    extensionReasonRemarks: string;
    fromPlace: number;
    fromState: number;
    fromPincode: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    transDistance: number;
    hsnCode: string;
    noOfItems: number;
    isCurrent: number;
    cancelledReason: string;
    cancelledRemarks: string;
 }

export interface EwayBillTrackingWrapper{
    ewayBillTrackinglist: EwayBillTracking[];	
	ewayBillTracking: EwayBillTracking;
}

 
export interface EwayBillInvoiceResponseDTO{
    flag: boolean;
    message: string;
    ewayBillNo: string;
    ewayBillDate: string;
    validUpto: string;
    alert: string;
    docDate: string;
    docNo: string;
    pdfUrl: string;
    detailedpdfUrl: string;
    JsonData: string;
    error_log_id: string;
}


export interface EWayBillRecentReport{
    id: number;
    invoiceHeaderId: string;
    invoiceNumber: string;
    eWayBillNo: string;
    eWayBillDate: string;
    partyName: string;
 
}

export interface EWayBillRecentDTO{
    eWayBillRecentReports: EWayBillRecentReport[];	
	totalCount: number;
}

export interface EWayBillReportModel{
    id: number;
    invoiceHeaderId: string;
    invoiceNumber: string;
    eWayBillNo: string;
    eWayBillDate: string;
    partyName: string;
    supplyType: string;
    invoiceDate: string;
    partyGstNum: string;
    transportedGstNum: string;
    fromAddress: string;
    toAddress: string;
    status: string;
    noOfItems: number;
    hsnCode: string;
    hsnDescription: string;
    assesableValue: number;
    sgst: number;
    cgst: number;
    igst: number;
    cess: number;
    noncess: number;
    otherValue: number;
    invoiceValue: number;
    eWayBillValidity: string;
    pdfUrl: string;
    detailedpdfUrl: string;
}

export interface EWayBillReport{

    ewayBillReports: EWayBillReportModel[];
    totalCount: number;
}

export interface TransactionMode {
    id: number;
    name: string;  
  }

  export interface ReasonCode {
    id: number;
    name: string;  
  }

export interface VehicleType {
    id: number;
    name: string; 
    code: string;
  }

  export interface cancelCode {
    id: number;
    name: string;  
  }


 