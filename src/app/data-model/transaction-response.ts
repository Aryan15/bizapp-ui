
export interface TransactionResponse{
    responseString : string;
    responseStatus : number;
}