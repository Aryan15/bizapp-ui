export interface AutoTransactionGeneration{
id:number;
periodToCreateTransaction:number;
countOfInvoice:number;
startDate:string;
invoiceHeaderId:string;
daysOfWeek:number;
daysOfMonth:number;
invoiceNumber:string;
stopTranaction:number;
email:number;
endDate:string;
totalCount:number; 
sheduledDates: Date[];
partyName:string;
partyId:number;

}

export interface AutoTransactionGenerationList{
    autoTransactionGenerationDTO:AutoTransactionGeneration;
}


export interface RecentAutoTransaction {
    
    autoTransactionGenerationDTOS: AutoTransactionGeneration[];
         totalCount: number;
     
     }