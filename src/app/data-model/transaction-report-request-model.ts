export interface TransactionReportRequest {
    
        partyId: number;
    
        transactionFromDate: string;
    
        transactionToDate: string;
    
        financialYearId: number;
    
        materialId: number;
    
        partyTypeIds: number[];

        createdBy: string;
    }