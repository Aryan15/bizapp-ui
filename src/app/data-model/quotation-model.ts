export interface QuotationHeader{

	id : string;    
    amount: number;
    quotationNumber: string;
    quotationId: number;
    quotationDate: string;
    enquiryNumber: string;
    enquiryDate: string;
    partyId: number;
    subTotalAmount: number;
    totalDiscount: number;
    discountPercent: number;
    totalTaxableAmount: number;
    taxId: number;
    roundOffAmount: number;
    grandTotal: number;
    statusId: number;
    financialYearId: number;
    companyId: number;
    paymentTerms: string;
    deliveryTerms: string;
    termsAndConditions: string;
    taxAmount: number;
    kindAttention: string;
    quotationSubject: string;
    quotationStatus: string;
    isPaymentChecked: string;
    sgstTaxRate: number;
    sgstTaxAmount: number;
    cgstTaxRate: number;
    cgstTaxAmount: number;
    igstTaxRate: number;
    igstTaxAmount: number;
    quotationTypeId : number;
    quotationItems : QuotationItem[];
    statusName: string;
    partyName: string;
    address: string;
    gstNumber:string;
    shipToAddress:string;
    remarks :String;
    inclusiveTax: number;
    // .................
    companyName: string;
    companyGstRegistrationTypeId : number;
    companyPinCode: string;
    companyStateId : number;
    companyStateName: string;
    companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
    companyContactPersonNumber : string;
    companyContactPersonName : string;
    companyPrimaryTelephone : string;
    companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
    companyFaxNumber : string;
    companyAddress: string;
    companyTagLine: string;
    companyGstNumber : string;
    companyPanNumber : string;
    companyPanDate : string;
    companyCeritificateImagePath: string;
    companyLogoPath: string;
    // partyName: string;
    partyContactPersonNumber: string;
    partyPinCode: string;
    partyAreaId: number;
    partyCityId: number;
    partyStateId: number;
    partyCountryId: number;
    partyCurrencyId :number;
    partyPrimaryTelephone: string;
    partySecondaryTelephone: string;
    partyPrimaryMobile: string;
    partySecondaryMobile: string;
    partyEmail: string;
    partyWebsite: string;
    partyContactPersonName: string;
    partyBillToAddress: string;
    partyShipAddress: string;
    partyDueDaysLimit: number;
    partyGstRegistrationTypeId: number;
    partyGstNumber: string
    partyPanNumber: string;
    isIgst: number;
    partyCode:string;
    createdBy:string;
    updateBy:string;
    createdDate:string;
    updatedDate:string;
    currencyName:string;
    
}

export interface QuotationItem{
	id: string;	
	quotationHeaderId: string;
    materialId: number;
    slNo: number;
	quantity: number;
	price: number;
	amount: number;
	status: string;
	taxPercentage: number;
	taxAmount: number;
	amountAfterTax: number;
	discountPercentage: number;
	discountAmount: number;
	amountAfterDiscount: number;
	unitOfMeasurementId: number;
	taxId: number;
	inclusiveOfTax: string;
	taxRate: number;
	remarks: string;
	sgstTaxPercentage: number;
	sgstTaxAmount: number;
	cgstTaxPercentage: number;
	cgstTaxAmount: number;
	igstTaxPercentage: number;
    igstTaxAmount: number;    
    partNumber:string;
    hsnOrSac:string;
    partName:string;
    uom:string;
    inclusiveTax:number;
    specification :string;
   
}

export interface RecentQuotation {

    quotationHeaders: QuotationHeader[],
    totalCount: number;

}

export interface QuotationWrapper{
    quotationHeader: QuotationHeader,
    itemToBeRemove: string[]
}

export interface QuotationSummaryReport {

    id: string;

    quotationNumber: string;

    quotationDate: string;

    

    partyName: string;

    totalTaxableAmount:number;

    cgstTaxRate: number;

    cgstTaxAmount: number;

    sgstTaxRate: number;
    
    statusName: string;

    sgstTaxAmount: number;


    igstTaxRate: number;


    igstTaxAmount: number;


  

    discountAmount: number;
    
    grandTotal: number;

    amount: number;

    inclusiveTax: number;

    
  
}

export interface QuotationReport {
    
    quotationSummaryReports: QuotationSummaryReport[],
    totalCount: number;
}