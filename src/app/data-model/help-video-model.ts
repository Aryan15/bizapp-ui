import { SafeResourceUrl } from "@angular/platform-browser";

export interface HelpVideo {
    id: number;
    header: string;
    description: string;
    order:number;
    activityId:number;
    transationTypeId:number;
    videoLink:string;
    safeUrl: SafeResourceUrl

}

export interface HelpVideoWrapper {
    helpVideo: HelpVideo[];
    totalCount: number;
}

export interface HelpVideoRequest{
    videoHeader:string;
}