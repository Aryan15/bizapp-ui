export interface TransactionType {
    id: number;	
    name: string;
    description: string;
}

export interface TransactionStatus{
    id: number;
    name: string;
    transactionTypeId: number ;
    transactionTypeName: string ;
    deleted: string;
}

export interface TransactionStatusChange{

	transactionTypeId: number;
	transactionTypeName: string;
	transactionId: string;
	transactionNumber: string;
	statusId: number;
	statusName: string;
	latest: string;
	sourceTransactionTypeId: number;
	sourceTransactionTypeName: string;
	sourceTransactionId: string;
	sourceTransactionNumber: string;
	sourceOperation: string;
}