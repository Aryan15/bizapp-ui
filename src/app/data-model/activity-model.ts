import { Company } from './company-model';
import { UserModel, Role } from './user-model';

export interface Menu {

    id: number;
    menuOrder: number;
    name: string;
    company: Company;
    //parentMenu: Menu;
    activities: Activity[];
    menuLevel: number;
}

export interface Activity {
    id: number;
    name: string;
    pageUrl: string;
    urlParams: string;
    imagePath: string;
    description: string;
    deleted: string;
    activityOrder: number;
    company: Company;
    subMenu: Menu;
    activityRoleBindings: ActivityRoleBinding[];
}



export interface ActivityRoleBinding {
    id: number;
    activityId: number;
    //role: Role;
    //user: UserModel;
    roleId: number;
    userId: number;
    deleted: string;
    companyId: number;
	permissionToCreate: number;
	permissionToUpdate: number;
    permissionToDelete: number;
    permissionToCancel: number;
    permissionToApprove: number;
    permissionToPrint: number;
    permissionToDraft: number;
}

// export interface MenuObject{
//     id: number;
//     menuOrder : number;
//     name: string;	
//     company: Company;
//     activities: Activity[];

// }

export interface StatusBasedPermission{
    id: number;
    statusId: number;
    deleted: string;
	permissionToCreate: number;
	permissionToUpdate: number;
    permissionToDelete: number;
    permissionToCancel: number;
    permissionToApprove: number;
    permissionToPrint: number;
    permissionToDraft: number;
}