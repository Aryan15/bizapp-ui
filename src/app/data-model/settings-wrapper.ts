import {Tax} from './tax-model';
import {Uom} from './uom-model';
import {TermsAndCondition} from './terms-condition-model';
import { NumberRangeConfiguration } from './number-range-config-model';
import { Role } from './user-model';





export interface GlobalSetting{
	id : number;
	deleted : string;
	financialYearId : number;
	stockIncreaseOnId : number;
	stockDecreaseOnId : number;
	itemLevelTax : number;
	itemLevelDiscount : number;
	browserPath : string;
	salePrice : number;
	purchasePrice : number;
	specialPriceCalculation : number;
	stockCheckRequired:number;
	inclusiveTax : number;
	barCodeStatus : number;
	printStatus : number;
	includeSignatureStatus : number;
	helpDocument : string;
	sgstPercentage : number;
	cgstPercentage : number;
	disableItemGroup : number;
	cutomerMateriaBinding : number;
	showOnlyCustomerMaterialInSales : number;
	roundOffTaxTransaction: number;
	itemLevelTaxPurchase : number;
	enableTally: number;
	allowReuse:number;
	allowPartyCode:number;
	allowEwayBill: number;
}

export interface SettingsWrapper{
	globalSetting : GlobalSetting;
	numberRangeConfigurations : NumberRangeConfiguration[];
	taxes : Tax[];	
	unitOfMeasurements : Uom[];
    termsAndConditions : TermsAndCondition[];
	roles : Role[];
}


export interface StockUpdateOn {
	id : number;
	deleted: string;
	transactionTypeId : number;
	userTypeId : number;
	transactionTypeName : string;
}

