import { PartyBankMap } from "./party-model";

export interface Company{
    id : number;
	
	name : string;
	
	address : string;
	
	cityId : number;
	
	stateId : number;
	
	countryId : number;
	
	gstNumber : string;
	
	materialIds : number[];
	
	deleted : string;
	
	pinCode : string;
	
	primaryTelephone : string;
	
	secondaryTelephone : string;
	
	tinNumber : string;
	
	tinDate : string;
	
	faxNumber : string;
	
	contactPersonName : string;
	
	primaryMobile : string;
	
	secondaryMobile : string;
	
	email : string;
	
	website : string;
	
	contactPersonNumber : string;
	
	statusId : number;
	
	panNumber : string;
	
	panDate : string;

	companyLogoPath: string;

	tagLine: string;

	gstRegistrationTypeId : number;
	
	addressesListDTO: AddressList[];

	stateCode: string;
	stateName: string;
	bank: string;
	branch: string;
	account: string;
	ifsc: string;
	ceritificateImagePath: string;
	signatureImagePath: string;
	msmeNumber:string;
	cinNumber:string;
	subject:string;
	iecCode:string;
    companyCurrencyId:number;
	creditLimit:number;
	insuranceType:string;
	insuranceRef:string;
	financePerson:string;
	financeEmail:string;
}

export interface AddressList{
    address : string;
    contactPersonName : string;
    contactPersonNumber : string;
    contactPersonDesignation : string;
    deleted: string;
}

export interface CompanyWithBankListDTO{
	companyDTO: Company;	
	partyBankMapDTOList: PartyBankMap[];
	partyBankMapListDeletedIds: number[];
}

export interface AboutCompany{
	productName:string;
	
	productVersion:string;
	
	companyName:string;
	
	companyEmail:string;
	
	companyWebsite:string;
	
	companyContactNumber:string;
}