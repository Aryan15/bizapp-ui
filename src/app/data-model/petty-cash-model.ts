export interface PettyCashHeader{
id : string;
pettyCashDate :string;
pettyCashTypeId :number;
peetCashId:number;
pettyCashNumber :string
totalIncomingAmount :number;
totalOutgoingAmount :number;
isDayClosed: number;
comments:string;
approvedBy:string;
approvedDate:string;
financialYearId:number;
companyId:number;
statusId:number;
statusName:string;
totalAmount:number;
bankAmount:number;
cashAmount:number;
pettyCashItems:PettyCashItem[];
isLatest:number;

}



export interface PettyCashItem{
    id:string;
    headerId:string;
    categoryId:number;
    categoryName:string;
    paymentMethodId:number;
    paymentMethodName:string;
    remarks:string;
    tagId:number;
    tagName:string;
    incomingAmount:number;
     outgoingAmount :number;
  
}


export interface PettyCashWrapper{
	itemToBeRemove: string[],
	pettyCashHeader: PettyCashHeader
}

export interface RecentPettyCash {
    
   pettyCashHeaders: PettyCashHeader[],
        totalCount: number;
    
    }

export interface Category{
    id : number;
    name : string;
    deleted :  string;
}

export interface Tag{
    id : number;
    name : string;
    deleted :  string;
}


export interface PettyCashReportModel{
    
        id: string;
        
        totalAmount: number;
    
        pettyCashNumber: string;
        
        pettyCashId: number;
        
        pettyCashDate: string;

         companyId: number;
        
        financialYearId: number;
        
        itemId: string;

        remarks: string;
        
        description: string;
        
        incomingAmount:number;

        outgoingAmount :number;

        paymentId: number;
        
        paymentName: string;

        categoryName: string;

        categoryId: number;

        tagName: string;

        tagId: number;

        statusId:number;

        statusName:string;
    }

    export interface PettyCashSummaryReportModel{

        pettyCashReports: PettyCashReportModel[];
        totalCount: number;



    }
