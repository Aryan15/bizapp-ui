import { QuotationHeader } from './quotation-model';

export interface PurchaseOrderHeader {

	id : string;
	purchaseOrderNumber: string ;
	purchaseOrderId: number;
	purchaseOrderDate: string;
    quotationNumber: string ;    
    quotationDate: string ;
    internalReferenceNumber: string;
    internalReferenceDate: string;    
    partyId : number;
    subTotalAmount: number;   
    totalDiscount: number;
    discountPercent: number;
    totalTaxableAmount: number;
    taxId: number;
    roundOffAmount: number ;
    grandTotal: number;
    statusId: number;
    financialYearId: number;
    companyId: number;
    purchaseOrderTypeId: number;
    paymentTerms: string;
    deliveryTerms: string;
    termsAndConditions: string;
    taxAmount: number;
    advanceAmount: number;    
    sgstTaxRate: number;
    sgstTaxAmount: number;
    cgstTaxRate: number;
    cgstTaxAmount: number;
    igstTaxRate: number;
    igstTaxAmount: number;
    isReverseCharge: number;
    transportationCharges: number;    
    purchaseOrderItems: PurchaseOrderItem[];
    statusName: string;
    partyName: string;
    address: string;
    gstNumber:string;
    shipToAddress:string;
    purchaseOrderEndDate: string;
    isOpenEnded: number;
    remarks : String
    inclusiveTax: number;
    // address: String
    companyName: string;
    companyGstRegistrationTypeId : number;
    companyPinCode: string;
    companyStateId : number;
    companyStateName: string;
    companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
    companyContactPersonNumber : string;
    companyContactPersonName : string;
    companyPrimaryTelephone : string;
    companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
    companyFaxNumber : string;
    companyAddress: string;
    companyTagLine: string;
    companyGstNumber : string;
    companyPanNumber : string;
    companyPanDate : string;
    companyCeritificateImagePath: string;
    companyLogoPath: string;
    // partyName: string;
    partyContactPersonNumber: string;
    partyPinCode: string;
    partyAreaId: number;
    partyCityId: number;
    partyStateId: number;
    partyCountryId: number;
    partyPrimaryTelephone: string;
    partySecondaryTelephone: string;
    partyPrimaryMobile: string;
    partySecondaryMobile: string;
    partyEmail: string;
    partyWebsite: string;
    partyContactPersonName: string;
    partyBillToAddress: string;
    partyShipAddress: string;
    partyDueDaysLimit: number;
    partyGstRegistrationTypeId: number;
    partyGstNumber: string
    partyPanNumber: string;
    isIgst: number;
    partyCode:string;
    createdBy:string;
    updateBy:string;
    createdDate:string;
    updatedDate:string;
    closePo:number;
    currencyName:string;
    partyCurrencyId:number;
    
}


export interface PurchaseOrderItem{
    id:string;
	headerId:string;
    slNo:number;
    materialId:number;
    quantity:number;
    dcBalanceQuantity:number;
    price:number;
    amount:number;
    discountPercentage:number;
    discountAmount:number;
    amountAfterDiscount:number;
    transportationAmount:number;
    cessPercentage:number;
    cessAmount:number;
    igstTaxPercentage:number;
    igstTaxAmount:number;
    sgstTaxPercentage:number;
    sgstTaxAmount:number;
    cgstTaxPercentage:number;
    cgstTaxAmount:number;
    amountAfterTax:number;
    remarks:string;
    quotationHeaderId:string;
    quotationItemId:string;
	unitOfMeasurementId:number;
    taxId:number;
    partNumber:string;
    hsnOrSac:string;
    partName:string;
    uom:string;
    inclusiveTax:number;
    invoiceBalanceQuantity:number;
    specification :string;
    processId:string;
    processName:string;
    isContainer: number;
    
}

export interface RecentPurchaseOrder {
    
        purchaseOrderHeaders: PurchaseOrderHeader[],
        totalCount: number;
    
    }

    export interface PoBalanceReport {
        poNumber:string;
         purchaseOrderDate:string;
         poQuantity:number;
          price:number;
          dcPendingQuantity:number;
        invoicePendingQuantity:number;
         dcNumber:string;
          dcDate:string;
          jwInDCInvoicePendingQuantity:number;
          jwInDCQuantity:number;
         jwInDCPendingQuantity:number;
         jwInDCIncomingQuantity:number;
         jwOutdcNumber:string;
          jwOutdcDate:string;
          jwOutDcInvoicePendingQuantity:number;
         jwOutDcQuantity:number;
        jwOutDCPendingQuantity:number;
         jwOutDCIncomingQuantity:number;
         partyName:string;
         statusName:string;
         materialName:string;
        partNumber:string;
   
         partyCode:string;
         dcBalanceAmount:number;
          invoiceBalanceAmount:number;
          internelRefNumber:string;
             
    }

    export interface PurchaseOrderReportWrapper {
        purchaseOrderReports: PurchaseOrderReport[],
        poBalanceReport: PoBalanceReport[],
        totalCount: number;
    }
    export interface PurchaseOrderReport {
        id: string;
        purchaseOrderNumber: string;
        purchaseOrderDate: string;
        partyName: string;
        customerName: string;
        materialName: string;
        quantity: number;
        unitOfMeasurementName: string;
        price: number;
        amount: number;
        totalAmount: number;
        discountAmount: number;
        cgstTaxPercentage: number;
        cgstTaxAmount: number;
        sgstTaxPercentage: number;
        sgstTaxAmount: number;
        igstTaxPercentage: number;
        igstTaxAmount: number;
        statusName: string;
        remarks:string;
        internalReferenceNumber:string;
        partyCode:string;

    }

export interface PurchaseOrderWrapper{
    itemToBeRemove: string[],
    purchaseOrderHeader: PurchaseOrderHeader
}