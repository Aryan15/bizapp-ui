export interface PartyType {
    id: number;	
    name: string;
    description: string;
}