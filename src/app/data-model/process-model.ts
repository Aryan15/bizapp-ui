export interface Process{
    id : number;
    deleted : string;
    name : string;
    description :string;
}

export interface ProcessWrapper {
    processList: Process[];
}