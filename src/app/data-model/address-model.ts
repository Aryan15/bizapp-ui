export interface Address{
    id : number;
    partyId : number;
    address: string;
    primaryMobile: string;
    secondaryMobile: string;
    deleted: string;
}