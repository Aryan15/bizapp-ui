
export interface NumberRangeConfiguration{
    id : number;
	deleted : string;
	transactionTypeId : number;
	transactionTypeName: string;
	prefix : string;
	postfix: string;
	startNumber : number;
	autoNumber : number;
	autoNumberReset : number;
	termsAndConditionCheck : number;
	printType : number;
	displayOrder : number;
	companyId : number;
	delimiter : string;
	financialYearCheck: number;
	printTemplateId: number;
	printTemplateTopSize: number;
	isZeroPrefix: number;
	isJasperPrint: string;
	jasperFileName: string;
	  printHeaderText: string;
	  allowShipingAddress: number;
}

export interface PrintTemplate {
	id : number;
	deleted : string;
	name: string;
    isJasperPrint: string;
    jasperFileName: string;
}