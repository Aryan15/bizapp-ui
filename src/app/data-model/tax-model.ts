
export interface Tax {
    id : number;
	name : string;
	rate : number;
	deleted : string;
    taxTypeId : number;
}

export interface TaxType {
	id : number;
	name : string;
	deleted : string;
}