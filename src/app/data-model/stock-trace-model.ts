
export interface StockTraceModel {
    businessDate : string;
    transactionDate : string;
    materialId : number;
    materialName : string;
    transactionNote : string;
    transactionTypeId : number;
    transactionHeaderId : string;
    transactionNumber : string;
    partyId : number;
    partyName : string;
    quantityIn : number;
    quantityOut : number;
    closingStock : number;
    partyCode:string;
    materialPartNumber:string;
  }

  export interface StockTraceRequest {
    fromDate : string;
    toDate: string;
    materialId : number;
  }

  export interface StockTraceReport {
    
        stockReports: StockTraceModel[];
        totalCount: number;
    }