import { Company } from './company-model';

export interface UserModel{
    id : number;
    username : string;
    email : string;
    password : string;
    name : string;
    lastName : string;
    active : number;
    roleIds : number[];
    currentAddress : string;
    permanentAddress: string;
    prefixId : number;
    countryId: number;
    stateId: number;
    cityId: number;
    areaId: number;
    genderId: number;
    pinCode: string;
    employeeNumber: string;
    telephoneNumber: number;
    mobileNumber: number;
    dateOfBirth: string;
    dateOfJoin: string;
    dateOfLeaving: string;
    comments: string;
    employeeStatusId: string;
    panNumber: string;
    displayName: string;
    companyId: number;
    employeeTypeId: number;
    employeeId: number;    
    isLogInRequired: number;
    deleted: string;
    userThemeName: string;
    roles: RoleModel[];
    userLanguage: string;
}

export interface RoleModel
{
    id : number;
    role : string;
    roleType : RoleType;
    deleted : string;

}

export interface RoleType{
	id: number;
	name: string;
	deleted: string;
}

export interface RoleWrapper{
    roles: Role[];
}

export interface Role{
	id : number;
	role : string;
    roleTypeId: number;
    roleTypeName: string;
}

export interface RecentUser{

    users:UserModel[],
    totalCount: number;

}

export interface RegistrationModel{

     companyDTO: Company;
    
     userDTO: UserModel ;

     enableJobwork: number;
}

export interface UserRoleManagement{
    roleId: number;
    menuId: number;
    menuOrder: number;
    menuName: string;
    activityId: number;
    activityOrder: number;
    activityName: string;
    permissionToApprove: number;
    permissionToCancel: number;
    permissionToCreate: number;
    permissionToDelete: number;
    permissionToPrint: number;
    permissionToUpdate: number;

}

export interface UserRoleWrapper{
    userRoleMenus: UserRoleMenu[];
}

export interface UserRoleMenu{

    menuId: number;
    isRoleAssigned: number;
    menuOrder: number;
    menuName: string;
    activities: UserRoleActivity[];

}

export interface UserRoleActivity{
    arbId: number;
    roleId: number;
    isRoleAssigned: number;
    activityId: number;
    activityOrder: number;
    activityName: string;
    submenus: UserRoleMenu[];
    permissionToApprove: number;
    permissionToCancel: number;
    permissionToCreate: number;
    permissionToDelete: number;
    permissionToPrint: number;
    permissionToUpdate: number;
}

export interface UserPassword {
    id : number;
    password :string;
}