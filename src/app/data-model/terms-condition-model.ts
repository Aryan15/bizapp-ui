
export interface TermsAndCondition{
    id: number;
	name: string;
	termsAndCondition: string;
	paymentTerms: string;
	deliveryTerms: string;
	defaultTermsAndCondition: number;
	transactionTypeId: number;
	deleted: string;
}