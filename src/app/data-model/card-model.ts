export interface CardType {
    id: number;
    name: string;
    deleted : string;
}