
export interface FinancialYear {
    id: number;

    startDate: string;

    endDate: string;

    financialYear: string;

    isActive: number;

    deleted: string;

    isModified: number;

}

export interface FinancialYearWrapper {
    financialYearList: FinancialYear[];
}