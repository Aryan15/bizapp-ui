export interface BankModel {
    id: number;
    bankname: string;
    bankAddress: string;
    deleted: string;
}

export interface RecentBank {

    bankModels: BankModel[],
    totalCount: number;

}

export interface BankWrapper{
    banks: BankModel[];
}