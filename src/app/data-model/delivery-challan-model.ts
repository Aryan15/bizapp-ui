import { PurchaseOrderHeader } from './purchase-order-model';

export interface DeliveryChallanHeader {
	id: string;
	deliveryChallanNumber: string;
	deliveryChallanId: number;
	deliveryChallanDate: string;
	internalReferenceNumber: string;
	internalReferenceDate: string;
	purchaseOrderNumber: string;
	purchaseOrderDate: string;
	deliveryChallanTypeId: number;
	partyId: number;
	officeAddress: string;
	deliveryAddress: string;
	dispatchDate: string;
	inspectedBy: string;
	inspectionDate: string;
	modeOfDispatch: string;
	vehicleNumber: string;
	deliveryNote: string;
	totalQuantity: number;
	statusId: number;
	statusName: string;
	financialYearId: number;
	companyId: number;
	numberOfPackages: string;
	deliveryTerms: string;
	termsAndConditions: string;
	paymentTerms: string;
	dispatchTime: string;
	taxId: number;
	labourChargesOnly: string;
	inDeliveryChallanNumber: string;
	inDeliveryChallanDate: string;
	returnableDeliveryChallan: string;
	nonReturnableDeliveryChallan: string;
	notForSale: string;
	forSale: string;
	returnForJobWork: string;
	modeOfDispatchStatus: string;
	vehicleNumberStatus: string;
	numberOfPackagesStatus: string;
	personName: string;
	sgstTaxRate: number;
	sgstTaxAmount: number;
	cgstTaxRate: number;
	cgstTaxAmount: number;
	igstTaxRate: number;
	igstTaxAmount: number;
	taxAmount: number;
	price: number;
	amount: number;
	eWayBill: string;
	balanceQuantity: number;
	partyName: string;
	address: string;
	shipToAddress: string;
	gstNumber: string;
	discountPercentage: number;
    discountAmount: number;
    amountAfterDiscount: number;
	deliveryChallanItems: DeliveryChallanItem[],
	remarks : String
	inclusiveTax: number;
	billToAddress : string;

	// .................
	companyName: string;
	companyGstRegistrationTypeId : number;
	companyPinCode: string;
	companyStateId : number;
	companyStateName: string,
	companyCountryId: number;
	companyPrimaryMobile : string;
	companySecondaryMobile : string;
	companyContactPersonNumber : string;
	companyContactPersonName : string;
	companyPrimaryTelephone : string;
	companySecondaryTelephone : string;
	companyWebsite : string;
	companyEmail : string;
	companyFaxNumber : string;
	companyAddress: string;
	companyTagLine: string;
	companyGstNumber : string;
	companyPanNumber : string;
	companyPanDate : string;
	companyCeritificateImagePath: string;
	companyLogoPath: string;
	// partyName: string;
	partyContactPersonNumber: string;
	partyPinCode: string;
	partyAreaId: number;
	partyCityId: number;
	partyStateId: number;
	partyCurrencyId :number;
	partyCountryId: number;
	partyPrimaryTelephone: string;
	partySecondaryTelephone: string;
	partyPrimaryMobile: string;
	partySecondaryMobile: string;
	partyEmail: string;
	partyWebsite: string;
	partyContactPersonName: string;
	partyBillToAddress: string;
	partyShipAddress: string;
	partyDueDaysLimit: number;
	partyGstRegistrationTypeId: number;
	partyGstNumber: string
	partyPanNumber: string;
	isIgst: number;
	partyCode:string;
	createdBy:string;
    updateBy:string;
    createdDate:string;
    updatedDate:string;
	jwNoteId:string;
}

export interface DeliveryChallanItem {
	id: string;
	headerId: string;
	materialId: number;
	materialName: string;
	slNo: number;
	quantity: number;
	remarks: string;
	status: string;
	purchaseOrderHeaderId: string;
	purchaseOrderItemId: string;
	price: number;
	amount: number;
	unitOfMeasurementId: number;
	taxId: number;
	taxRate: number;
	deliveryChallanType: string;
	deliveryChallanItemStatus: string;
	process: string;
	//balanceQuantity: number;
	batchCode: string;
	purchaseOrderNumber: string;
	batchCodeId: string;
	inDeliveryChallanNumber: string;
	inDeliveryChallanDate: string;
	// sgstTaxRate: number;	
	// sgstTaxAmount: number;	
	// cgstTaxRate: number;	
	// cgstTaxAmount: number;	
	// igstTaxRate: number;	
	// igstTaxAmount: number;	
	// taxAmount: number;
	discountPercentage: number;
    discountAmount: number;
    amountAfterDiscount: number;
	sgstTaxPercentage: number;
	sgstTaxAmount: number;
	cgstTaxPercentage: number;
	cgstTaxAmount: number;
	igstTaxPercentage: number;
	igstTaxAmount: number;
	taxAmount: number;
	partNumber: string;
	hsnOrSac: string;
	partName: string;
	uom: string;
	inclusiveTax: number;
	grnBalanceQuantity:number;
	invoiceBalanceQuantity:number;
	sourceDeliveryChallanHeaderId: string;
	sourceDeliveryChallanItemId: string;
	dcBalanceQuantity: number;
	incomingQuantity: number;
	rejectedQuantity: number;
	specification: string;
	sourceDeliveryChallanNumber: string;
	sourceDeliveryChallanDate: string;
	processId: string;
	processName: string;
	isContainer: number;
	outName: string;
	outPartNumber: string;
}


export interface RecentDeliveryChallan {

	deliveryChallanHeaders: DeliveryChallanHeader[],
	totalCount: number;

}

export interface DeliveryChallanReportRequest {

	partyId: number;

	transactionFromDate: string;

	transactionToDate: string;

	financialYearId: number;

	materialId: number;

	partyTypeIds: number[];
}

export interface DeliveryChallanItemModel {
	id: string;
	deliveryChallanNumber: string;
	deliveryChallanDate: string;
	partyName: string;
	materialName: string;
	remarks: string;
	quantity: number;
	uom: string;
	hsnOrSac: string;
	statusName: string;
}


export interface DeliveryChallanItemReport {

	deliveryChallanReportItems: DeliveryChallanItemModel[];
	totalCount: number;
}

export interface DeliveryChallanHeaderWrapper{
	itemToBeRemove: string[];
	deliveryChallanHeader: DeliveryChallanHeader;
}