
export interface DashboardTransactionSummary{
	month: string;
    transactionName: string;
	transactionAmount: number;
	transactionCount: number;
	dueAmount: number;
}