


export interface Party {
    id: number;
    name: string;
    address: string;
    countryId: number;
    stateId: number;

    stateCode: string;
    stateName: string;
  //  portStateId:number;
    //currencyName:String;
    cityId: number;
    areaId: number;
    pinCode: string;
    primaryTelephone: string;
    secondaryTelephone: string;
    primaryMobile: string;
    secondaryMobile: string;
    email: string;
    contactPersonName: string;
    contactPersonNumber: string;
    webSite: string;
    partyTypeId: number;
    deleted: string;
    billAddress: string;
    panNumber: string;
    gstRegistrationTypeId: number;
    gstNumber: string;
    partyTypeName: string;
    addressesListDTO: AddressList[];
    dueDaysLimit: number;
    isIgst: number;
    areaName: string;
    partyId:number;
    partyCode:string;
    partyCurrencyId:number;
    partyCurrencyName:string;

}

export interface MaterialPriceList {
    id: number;
    partyId: number;
    materialTypeName: string;
    materialName: string;
    partNumber: string;
    //mrp: number;
    //price: number;
    materialId: number;
    discountPercentage: number;
    sellingPrice: number;
    comment:string;
   // comments: string;
    deleted: string;
    currentSellingPrice: number;
    currentBuyingPrice: number;
}

export interface PartyPriceListReport {
    id:number;
    partyName:string;
    materialId:number;
    materialTypeName:string;
    materialName:string;
    partNumber:string;
    partyPrice:number;
    comments:string;
    currentPrice:number;


  
}

export interface PartyBankMap {
    id: number;
	companyId: number;
	partyId: number;
    bankId: number;
    bankname: string;
	branch: string;
	ifsc: string;
	accountNumber: string;
    bankAdCode:number;
	openingBalance: number;
	contactNumber: string;
}

export interface PartyWithPriceList {
    partyDTO: Party;
    materialPriceListDTOList: MaterialPriceList[];
    materialPriceListDeletedIds: number[];
    partyBankMapDTOList: PartyBankMap[];
    partyBankMapDeletedIds: number[];
}

export interface PartyType {
    id: number;
    name: string;
    deleted: string;
}
export interface PartyReport {

    parties: Party[],
    
    totalCount: number;

}
export interface PartyReportRequest {
    fromDate: string;
    toDate: string;
    partyId: number;
}


export interface RecentParty {
    partyWithPriceLists: PartyWithPriceList[],
    totalCount: number;

}

export interface AddressList{
    address : string;
    contactPersonName : string;
    contactPersonNumber : string;
    contactPersonDesignation : string;
    deleted: string;
}





  export interface PartyMaterialPriceListDTO {
    partyPriceListReport:PartyPriceListReport[],
    totalCount: number;

  }

