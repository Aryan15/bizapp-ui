export interface Country {
    id : number;
    name : string;
    deleted : string;
}

export interface CountryWrapper{
    countries: Country[]
}