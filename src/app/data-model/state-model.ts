import { Country } from './country-model';

export interface State {
    id: number;
    name: string;
    stateCode: string;
    countryId: number;
    citiesList: number[];
    deleted: string;
}

export interface StateWrapper {
    states: State[];
}