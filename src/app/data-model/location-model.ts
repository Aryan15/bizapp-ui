export interface Location {
    id : number;
    locationType : number;
    name : string;
    deleted : string;
}