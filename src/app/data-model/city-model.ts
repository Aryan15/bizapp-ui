import { State } from './state-model';

export interface City {
    id: number;
    name: string;
    stateId: number;
    areaList: number[];
    deleted: string;
}

export interface CityWrapper {
    cities: City[]
}