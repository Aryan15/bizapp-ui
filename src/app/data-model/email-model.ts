export interface Email {
    id : number;
    deleted:string;
	emailDebug : string;
    emailFrom  : string;
    emailHost :  string;
    emailPassword :string;
    emailPort : number;
    emailSmtpAuth:string;
    emailSmtpStartTlsEnable:string;
    emailTransportProtocol:string;
    emailUserName:string;
    emailEnabled : number;
    emailText:string;
    emailSubject:string;
    emailType:number


}