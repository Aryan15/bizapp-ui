
export interface ApplicableButtons {
    isCloseButton: boolean;
    isDeleteButton: boolean;
    isCancelButton: boolean;
    isPrintButton: boolean;
    isClearButton: boolean;
    isSaveButton: boolean;
    isApproveButton: boolean;
    isEditButton: boolean;
    isDraftButton: boolean;
}

export interface NamePrefix {
    id: number;
    name: string;
}

export interface Gender {
    id: number;
    name: string;
}

export interface DefaultCheckTC {
    index: number;
    transactionTypeId: number;
}


export interface QuantityChangeEvent {

    index: number;
    materialId: number;
    quantity: number
}

export interface ImageMetadata{
    height: number;
	width: number;
	size: number;
	name: string;
}

export interface TallyExportTracker{
    fromDate: string;
    toDate: string;
    invoiceTypeId: number;
    voucherCount: number;
    stockItemCount: number;
    partyTypeId: number;
    partyCount: number;
    dateAndTime: string;
}

export interface TallyExportTrackerReport {

    tallyExportTrackerItems: TallyExportTracker[];
    totalCount: number;
}


// Message class for displaying messages in the component
export class Message {
    constructor(public content: string, public sentBy: string) {}
  }