export interface Currency {
    id: number
    currencyName:string
    deleted : string
    shortName: string
    currencySymbol: string
    currencyFraction:string
    currencyDecimal:string
}