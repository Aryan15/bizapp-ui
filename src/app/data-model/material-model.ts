export interface Material {
    id : number;
	deleted: string;
	name: string;
	partNumber: string;
	supplyTypeId : number;
	materialTypeId: number;
	unitOfMeasurementId: number;
	unitOfMeasurementName: string;
	price: number;
	specification: string;
	stock: number;
	companyId: number;
	buyingPrice: number;
	taxId: number;
	mrp: number;
	discountPercentage: number;
	partyId: number;
	hsnCode: string;
	cessPercentage: number;
	openingStock: number;
	minimumStock : number;
	isContainer: number;
	outName: string;
	outPartNumber: string;
	materialTypeName: string;
}

export interface SupplyType {
	
	id : number;
	name : string;
	deleted :  string;
}

export interface MaterialReport{
    materials: Material[],
    totalCount: number;
}

export interface MaterialReportRequest {
	fromDate : string;
    toDate: string;
    materialId : number;
	}
	

export interface RecentMaterial{
	
		materials:Material[],
		totalCount: number;
	
	}

	export interface Process{
		id: number,
		deleted: string,
		name: string,
		description: string,
	}

	export interface MaterialStockCheckDTO{
		id: number;
		deleted: string;
		name: string;
		partNumber: string;
		itemQuantity: number;
		minimumStock: number;
	}

	export interface MaterialStockCheckDTOWrapper{
		materialStockCheckDTOs: MaterialStockCheckDTO[];
	}


	export interface MaterialTypeForCreadit {
		id: number,
		name: string,
	  }