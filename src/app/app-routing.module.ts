import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/auth/auth.guard';
import { HomeComponent } from '../app/home/home.component';
import { HomeLayoutComponent } from '../app/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from '../app/layout/login-layout/login-layout.component';
import { LoginComponent } from '../app/login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';


const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        component: HomeComponent,
        data: { state: 'home-comp' }
      },
      {
        path: 'dashboard',
        component: HomeComponent,
        data: { state: 'home-comp' }
      },
      {
        path: 'transaction',
        loadChildren: () => import('./transaction/transaction.module').then(m => m.TransactionModule)
      },
      {
        path: 'master',
        loadChildren: () => import('./master/master.module').then(m => m.MasterModule)
      },
      {
        path: 'report',
        loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule)
      },
      {
        path: 'setting',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)

      },

    ]
  },
  {
    path: '',
    component: LoginLayoutComponent, // {4}
    children: [
      {
        path: 'login',
        component: LoginComponent   // {5}
      },
    ]
  },
  {
    path: 'app-user-registration',
    component: UserRegistrationComponent
  },
  {
    path: 'app-forget-password',
    component: ForgetPasswordComponent,
  },
  {
    path:'404',
    component: NotFoundComponent
  },

  {
    path: '**',
    redirectTo: '/404'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    // preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

