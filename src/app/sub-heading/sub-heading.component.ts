import { Component, OnInit, Input } from '@angular/core';
import { AppSettings } from '../app.settings';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { LanguageService } from '../services/language.service';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sub-heading',
  templateUrl: './sub-heading.component.html',
  styleUrls: ['./sub-heading.component.scss']
})
export class SubHeadingComponent implements OnInit {

  title: string;
  subheading: string
  // @Input() transactionType: TransactionType;
  @Input() transactionNumber;
  @Input() statusName;
  @Input() route: ActivatedRoute;
  //subHeadingLine : string = AppSettings.sub_Heading_Line;
  // @Input() transaction;
  constructor(private userService: UserService,
    private languageService: LanguageService,
    private translate: TranslateService,) {
      translate.setDefaultLang('en');
     }

  ngOnInit() {
    // console.log('in sub-heading init: ', this.route);
    if (this.route) {
      this.userService.getActivityForRoute(this.route)
        .subscribe(resp => {
          // console.log("resp............................>>>>>",resp);
          var splitted = resp.pageUrl.split("");
          // console.log("splitted: ",splitted);
          var i=0;
          var str = ""
          while(splitted[i]!="/"){
            str = str + splitted[i]
            i++;
          }
          // this.title = this.userService.reportHeading ? this.userService.reportHeading + resp.name : resp.name;
          if(str==="report"){
            this.title = this.userService.reportHeading ? this.userService.reportHeading + resp.name : resp.name;
            // this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + resp.name;
          }
          else{
            this.title = this.userService.currentActivity ? this.userService.currentActivity + resp.name : resp.name;

          }
          // this.userService.currentActivity =  menuName + " > " + activityName;
          this.subheading = resp.description;
        });
    }
    this.applyLanguage();
  }


  applyLanguage(){
    this.languageService.getLanguage()
            .subscribe(resp => {
              if(resp===null){

                this.translate.use("en");
              }
              else{
                this.translate.use(resp);
              }
            })
  }

}

// //console.log("str...............................",str);