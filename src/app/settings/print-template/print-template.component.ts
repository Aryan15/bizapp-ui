import { Component, OnInit, Input } from '@angular/core';
import { TransactionType } from '../../data-model/transaction-type';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { NumberRangeConfiguration, PrintTemplate } from '../../data-model/number-range-config-model';
import { AlertService } from 'ngx-alerts';
import { Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { GlobalSettingService } from '../../services/global-setting.service';
import { GlobalSetting } from '../../data-model/settings-wrapper';

@Component({
  selector: 'app-print-template',
  templateUrl: './print-template.component.html',
  styleUrls: ['./print-template.component.scss']
})
export class PrintTemplateComponent implements OnInit {

  @Input() allTransactionTypes: TransactionType[];
  
  // templates = [
  //   {
  //     id: 2,
  //     name: "Standard"
  //   },
  //   {
  //     id: 3,
  //     name: "Provision for two images"
  //   },
  //   {
  //     id: 4,
  //     name: "Pre printed template"
  //   },
  // ];
  templates: PrintTemplate[] = [];

  printTemplateForm: FormGroup;
  //transactionTypes: TransactionType[] = [];
  numberRangeConfiguration: NumberRangeConfiguration[] = [];
  globalSetting: GlobalSetting;
  constructor(
      private fb: FormBuilder,
     private transactionTypeService: TransactionTypeService
     , private numberRangeConfigService: NumberRangeConfigService
     , private alertService: AlertService
     , private _router: Router
     , private globalSettingService: GlobalSettingService
  ) { }

  ngOnInit() {
    this.getPrintTemplates();
    this.getGS();
  }

  getGS(){
    this.globalSettingService.getGlobalSetting()
    .subscribe(response => {
      this.globalSetting = response;
      this.getAllPrintTemplates();
    })

  }
  getPrintTemplates() {
    this.numberRangeConfigService.getAllConfig().subscribe(response => {
      
      response = response.filter(r => this.allTransactionTypes.find(tt => tt.id === r.transactionTypeId))

      let printTemplateWrapper: PrintTemplateWrapper = {
        templates: response
      }
        
      this.printTemplateForm = this.toFormGroup(printTemplateWrapper);
    })
  }

  getAllPrintTemplates(){
    this.numberRangeConfigService.getAllPrintTemplates()
    .subscribe(response => {
      response.forEach(r => {
       
        if(r.jasperFileName === 'InvoiceTaxGroup' || r.jasperFileName === 'InvoiceTaxGroup2'){ 
          if( this.globalSetting.itemLevelTax){
            this.templates.push(r)
          }
        }else{
          this.templates.push(r)
        }
      })
      //console.log("response: ",response);
      // this.templates = response;
      // this.templates
      // .filter(t => t.isJasperPrint)
      // .forEach(t => {
      //   t.name = "Classic-"+t.name
      // })
    })
  }

  toFormGroup(data: PrintTemplateWrapper): FormGroup {

    const templates = new FormArray([]);
    if (data.templates) {
      data.templates.forEach(c => {
        templates.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      templates: templates
    });

    return formGroup;
  }


  makeItem(c: NumberRangeConfiguration): FormGroup {
    return this.fb.group({
      id: [c.id],
      transactionTypeId: [c.transactionTypeId],
      transactionTypeName: [c.transactionTypeName],
      printTemplateId: [c.printTemplateId]
    })
  }

  

  save(model: PrintTemplateWrapper){
    //console.log('saving: ', model);
    this.numberRangeConfigService.updatePrintTemplate(model.templates)
    .subscribe(response => {
      //console.log("response");
      let saved: PrintTemplateWrapper = <PrintTemplateWrapper>{};
      saved.templates = response;
      this.printTemplateForm = this.toFormGroup(saved);
      this.alertService.success("Saved successfully");
      this.closeForm();
    },
    error =>{
      console.error(error);
      this.alertService.danger("Save failed");
    })
    //let 
  }

  closeForm() {
    this._router.navigate(['/']);
    // this._router.navigateByUrl('/dashboard')
  }


  filteredTemplates(tranTypeName: string): PrintTemplate[]{
    let retTemplates: PrintTemplate[] = this.templates.filter(t => !t.isJasperPrint);
    
    let allowedTrans: string[] = [
      AppSettings.PROFORMA_INVOICE,
      AppSettings.CUSTOMER_INVOICE,
      AppSettings.CUSTOMER_QUOTATION,
      AppSettings.INCOMING_JOBWORK_INVOICE,
      AppSettings.CUSTOMER_DC,
      AppSettings.INCOMING_JOBWORK_OUT_DC,
      AppSettings.OUTGOING_JOBWORK_OUT_DC,
     
    ]

    if(allowedTrans.includes(tranTypeName)){
      retTemplates.push(...this.templates.filter(t => t.name === tranTypeName));
    }
    
    

    return retTemplates;
  }
}

export interface PrintTemplateWrapper {
  templates: NumberRangeConfiguration[]
}

// export class PrintTemplateModel {
//   id: number;
//   transactionTypeId: number;
//   transactionTypeName: string;
//   templateId: number;
// }