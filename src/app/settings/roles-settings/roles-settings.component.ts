import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'app-roles-settings',
  templateUrl: './roles-settings.component.html',
  styleUrls: ['./roles-settings.component.scss']
})
export class RolesSettingsComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() idx: number;
  @Input() taxMaxIndex: number;

  @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() addIndex: EventEmitter<number> = new EventEmitter<number>();
  dialogRef: MatDialogRef<ConfirmationDialog>;
  constructor(public dialog: MatDialog,) { }

  ngOnInit() {
  }

  delete(index: number) {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
  });

  this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

  this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
    //console.log("Deleting...", index);
    this.deleteIndex.emit(index);
      }
    
    //this.handleChanges()
    this.dialogRef = null;
});

  }
  add(index: number) {

    //console.log("Adding...", index);
    this.addIndex.emit(index);
  }

}
