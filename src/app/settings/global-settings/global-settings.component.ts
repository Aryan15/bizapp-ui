import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingsWrapper, GlobalSetting, StockUpdateOn } from '../../data-model/settings-wrapper';
import { NumberRangeConfiguration } from '../../data-model/number-range-config-model';
import { Uom } from '../../data-model/uom-model';
import { TermsAndCondition } from '../../data-model/terms-condition-model';
import { Tax, TaxType } from '../../data-model/tax-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { TransactionType } from '../../data-model/transaction-type';
import { AlertService } from 'ngx-alerts';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { TaxService } from '../../services/tax.service';
import { UomService } from '../../services/uom.service';
import { UserService } from '../../services/user.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { TermsAndConditionsService } from '../../services/terms-conditions.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DefaultCheckTC } from '../../data-model/misc-model';
import { Role, UserModel } from '../../data-model/user-model';
import { InvoiceService } from '../../services/invoice.service';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../services/language.service';
import { PartyService } from '../../services/party.service';
@Component({
  selector: 'app-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.scss']
})
export class GlobalSettingsComponent implements OnInit {

  public settingsForm: FormGroup;
  public settingsWrapper: SettingsWrapper;

  public globalSetting: GlobalSetting;
  public numberRangeConfigurations: NumberRangeConfiguration[] = [];
  public numberRangeConfiguration: NumberRangeConfiguration;
  public taxes: Tax[] = [];
  public uoms: Uom[] = [];
  //public roles: Role[] = [];
  public termsAndConditions: TermsAndCondition[] = [];
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  //disableItemLevel: boolean = false;
  allowStockIncrease: boolean = false;
  allowStockDecrease: boolean = false;

  financialYears: FinancialYear[] = [];
  increaseStockUpdateOns: StockUpdateOn[] = [];
  decreaseStockUpdateOns: StockUpdateOn[] = [];
  transactionTypes: TransactionType[] = [];
  printTemplateTransactionTypes: TransactionType[] = [];
  transactionTyp: TransactionType[] = [];

  public transactionType: TransactionType;
  public tncGroup: FormGroup;
  public tncIdx: number;
  public defaultTncCheck: number;
  public currentTncIdx: number = -1;

  saveStatus: boolean = false;
  spinStart: boolean = false;
  isIgst: boolean = false;
  transactionStatus: string = null;
  hintValue: string = null;
  warningStatus: string;
 
  materialMRPPrice: number = AppSettings.MATERIAL_PRICE_TYPE.MRP;
  materialSellingPrice: number = AppSettings.MATERIAL_PRICE_TYPE.sellingPrice;
  materialBuyingPrice: number = AppSettings.MATERIAL_PRICE_TYPE.buyingPrice;

  specialPriceCalculationYes: number = AppSettings.SPECIAL_PRICE_CALCULATION.yes;
  specialPriceCalculationNo: number = AppSettings.SPECIAL_PRICE_CALCULATION.no;

  stockCheckRequiredYes: number = AppSettings.STOCK_CHECK_REQUIRED.yes;
  stockCheckRequiredNo: number = AppSettings.STOCK_CHECK_REQUIRED.no;

  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
  };

  activityRoleBindings: ActivityRoleBinding[];
  selectedFinancialYear: FinancialYear;
  excludedTransctionTypeIds: number[] = [];

  //initComplete: boolean = false;

  themeClass: any;
  language: any;
  currentUser: UserModel;
   partyCount:number=0;
  constructor(private fb: FormBuilder
    , private numberRangeConfigService: NumberRangeConfigService
    , private taxService: TaxService
    , private uomService: UomService
    , private userService: UserService
    , private financialYearService: FinancialYearService
    , private globalSettingService: GlobalSettingService
    , private invoiceService: InvoiceService
    , private termsAndConditionsService: TermsAndConditionsService
    , private transactionTypeService: TransactionTypeService
    , private route: ActivatedRoute
    , private alertService: AlertService
    , public dialog: MatDialog
    , private _router: Router,
    private translate: TranslateService,
    private languageService: LanguageService,
    private partyService: PartyService, ) {
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    //this.initComplete = false;
    translate.setDefaultLang('en');
    this.setActivityRoleBinding();
  }

  ngOnInit() {
    this.spinStart = true;
    this.themeClass = this.currentUser.userThemeName;
    this.language = this.currentUser.userLanguage;
    this.applyLanguage();
    this.initFunctions();
    // this.getFinancialYears();
    this.getStockIncreaseOns();
    this.getStockDecreaseOns();

    this.getTransactionTypes();
    this.handleChanges();
  

    //this.checkIsDefaultForTC();
  }

  // ngAfterViewInit(){
  //   //console.log("init complete")
  //   this.initComplete = true;
  // }
  initFunctions() {
    this.checkPartyCount();
    this.initForm();
    this.initAllSettings();


  }

  applyLanguage() {
    this.languageService.getLanguage()
    .subscribe(resp => {
    if(resp===null){
      this.translate.use("en");
    }
    else{
      this.translate.use(resp);
    }
   
  })
  }


  checkPartyCount(){
  this.partyService.checkPartyCount().subscribe(response => {
    this.partyCount= response.responseStatus
   
     });
    }
  initAllSettings() {

    this.globalSettingService.getAllSettings()
      .subscribe(response => {
        this.settingsWrapper = response;
        this.initGlobalSettings(this.settingsWrapper.globalSetting);

        this.initNumberRangeConfigurations(this.settingsWrapper.numberRangeConfigurations);


        this.initTaxes(this.settingsWrapper.taxes);


        this.initUoms(this.settingsWrapper.unitOfMeasurements);


        this.initTermsAndConditions(this.settingsWrapper.termsAndConditions);
        this.spinStart = false;

      });



  }






  handleChanges() {

    this.settingsForm.controls['globalSetting'].get('itemLevelDiscount').valueChanges.subscribe(id => {
      // //console.log('in itemLevelDiscount change: ', id);
      this.settingsForm.controls['globalSetting'].get('itemLevelTax').patchValue(id, { emitEvent: false });
    });


    this.settingsForm.controls['globalSetting'].get('itemLevelTax').valueChanges.subscribe(it => {
      // //console.log('in itemLevelDiscount change: ', it);
      this.settingsForm.controls['globalSetting'].get('itemLevelDiscount').patchValue(it, { emitEvent: false });
    });


  }

  checkShowOnlyCustomerMaterialInSales() {
    //console.log('in showOnlyCustomerMaterialInSales change: ', this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').value);
    //console.log('in cutomerMateriaBinding change: ', this.settingsForm.controls['globalSetting'].get('cutomerMateriaBinding').value);
    if (this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').value === true) {
      if (this.settingsForm.controls['globalSetting'].get('cutomerMateriaBinding').value === false || this.settingsForm.controls['globalSetting'].get('cutomerMateriaBinding').value === 0) {
        // this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').valueChanges.subscribe(cb => {

        this.alertRef = this.dialog.open(AlertDialog, {
          disableClose: false

        });
        this.alertRef.componentInstance.alertMessage = "Please check Enable customer binding"
        this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').patchValue(false);
      }
    }
    // });
  }

  checkCutomerMateriaBinding() {
    //console.log('in showOnlyCustomerMaterialInSales change: ', this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').value);
    //console.log('in cutomerMateriaBinding change: ', this.settingsForm.controls['globalSetting'].get('cutomerMateriaBinding').value);
    if (this.settingsForm.controls['globalSetting'].get('cutomerMateriaBinding').value === false) {
      this.settingsForm.controls['globalSetting'].get('showOnlyCustomerMaterialInSales').patchValue(false);

    }
  }
  private getStockIncreaseOns() {

    this.globalSettingService.getStockIncreaseOn()
      .subscribe(response => {
        this.increaseStockUpdateOns = response;

        //console.log("stockUpdateOns:", this.increaseStockUpdateOns);
      });

    this.transactionTypeService.allowStockIncrease()
      .subscribe(response => {
        //console.log("allowStockIncrease: ", response)
        this.allowStockIncrease = response;
        if (!response)
          this.settingsForm.get("globalSetting.stockIncreaseOnId").disable();
        else {
          this.settingsForm.get("globalSetting.stockIncreaseOnId").enable();
        }
      })
  }

  private getStockDecreaseOns() {

    this.globalSettingService.getStockDecreaseOn()
      .subscribe(response => {
        this.decreaseStockUpdateOns = response;

        //console.log("stockUpdateOns:", this.increaseStockUpdateOns);
      })

    this.transactionTypeService.allowStockDecrease()
      .subscribe(response => {
        this.allowStockDecrease = response;
        if (!response) {
          this.settingsForm.get("globalSetting.stockDecreaseOnId").disable();
        } else {
          this.settingsForm.get("globalSetting.stockDecreaseOnId").enable();
        }
      })
  }




  private getFinancialYears() {

    this.financialYearService.getFinancialYears()
      .subscribe(response => {
        this.financialYears = response;
        // this.selectedFinancialYear = response;

        // if (this.selectedFinancialYear.id !== undefined) {
        //   this.settingsForm.controls['financialYearId'].patchValue(response.financialYear);
        // }
      })
  }


  private initForm() {

    // this.globalSettingService.getGlobalSetting()
    //   .subscribe(response => {
    //     this.globalSetting = response;

    let data: SettingsWrapper = {
      globalSetting: this.getGlobalSettings(),
      numberRangeConfigurations: null,//this.numberRangeConfigurations,
      taxes: null,
      unitOfMeasurements: null,
      termsAndConditions: null,
      roles: null,
      // printCopies: null
    }
    this.settingsForm = this.toFormGroup(data);
    // });
  }

  private getGlobalSettings(): GlobalSetting {

    let gs: GlobalSetting = {
      id: null,
      deleted: null,
      financialYearId: null,
      stockIncreaseOnId: null,
      stockDecreaseOnId: null,
      itemLevelTax: null,
      itemLevelDiscount: null,
      browserPath: null,
      salePrice: null,
      purchasePrice: null,
      specialPriceCalculation: null,
      inclusiveTax: null,
      barCodeStatus: null,
      printStatus: null,
      includeSignatureStatus: null,
      helpDocument: null,
      sgstPercentage: null,
      cgstPercentage: null,
      disableItemGroup: null,
      cutomerMateriaBinding: null,
      showOnlyCustomerMaterialInSales: null,
      roundOffTaxTransaction: null,
      stockCheckRequired: null,
      itemLevelTaxPurchase: null,
      enableTally: null,
      allowReuse:null,
      allowPartyCode:null,
      allowEwayBill: null
    };

    return gs;
  }

  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;
    })
  }

  private toFormGroup(data: SettingsWrapper): FormGroup {

    const numberRangeConfigurationsArray = new FormArray([]);

    if (data.numberRangeConfigurations) {
      data.numberRangeConfigurations.forEach(item => {

        numberRangeConfigurationsArray.push(this.makeNumberRangeItem(item));
      })

    }

    const taxesArray = new FormArray([]);

    if (data.taxes) {
      data.taxes.forEach(item => {
        taxesArray.push(this.makeTaxItem(item));
      })
    }

    const unitOfMeasurementsArray = new FormArray([]);

    if (data.unitOfMeasurements) {
      data.unitOfMeasurements.forEach(item => {
        unitOfMeasurementsArray.push(this.makeUnitOfMeasurementItem(item));
      })
    }

    const termsAndConditionsArray = new FormArray([]);

    if (data.termsAndConditions) {
      data.termsAndConditions.forEach(item => {
        termsAndConditionsArray.push(this.makeTermsAndConditionItem(item));
      })
    }

    const rolesArray = new FormArray([]);

    // if (data.roles) {
    //   data.roles.forEach(item => {
    //     rolesArray.push(this.makeRolesItem(item));
    //   })
    // }




    const formGroup = this.fb.group({
      globalSetting: this.getBasicSettingsGroup(data.globalSetting),//[data.globalSetting],
      numberRangeConfigurations: numberRangeConfigurationsArray,//[this.buildNumberRange()], //[data.numberRangeConfigurations],
      taxes: taxesArray,//[data.taxes],
      unitOfMeasurements: unitOfMeasurementsArray,// [data.unitOfMeasurements],
      termsAndConditions: termsAndConditionsArray,// [data.termsAndConditions],
      roles: rolesArray,// [data.roles],
    });

    return formGroup;
  }

  private getBasicSettingsGroup(gs: GlobalSetting): FormGroup {

    // let gs : GlobalSetting;

    // gs = this.getGlobalSettings();

    //console.log('incoming data: ', gs);

    const gsGroup = this.fb.group({
      id: gs.id,
      deleted: gs.deleted,
      financialYearId: gs.financialYearId,
      stockIncreaseOnId: gs.stockDecreaseOnId,
      stockDecreaseOnId: gs.stockDecreaseOnId,
      itemLevelTax: gs.itemLevelTax,
      itemLevelDiscount: gs.itemLevelDiscount,
      browserPath: gs.browserPath,
      salePrice: gs.salePrice,
      purchasePrice: gs.purchasePrice,
      specialPriceCalculation: gs.specialPriceCalculation,
      inclusiveTax: gs.inclusiveTax,
      barCodeStatus: gs.barCodeStatus,
      printStatus: gs.printStatus,
      includeSignatureStatus: gs.includeSignatureStatus,
      helpDocument: gs.helpDocument,
      sgstPercentage: gs.sgstPercentage,
      cgstPercentage: gs.cgstPercentage,
      disableItemGroup: gs.disableItemGroup,
      cutomerMateriaBinding: gs.cutomerMateriaBinding,
      showOnlyCustomerMaterialInSales: gs.showOnlyCustomerMaterialInSales,
      roundOffTaxTransaction: gs.roundOffTaxTransaction,
      stockCheckRequired: gs.stockCheckRequired,
      itemLevelTaxPurchase: gs.itemLevelTaxPurchase,
      enableTally: gs.enableTally,
      allowReuse:gs.allowReuse,
      allowPartyCode:gs.allowPartyCode,
      allowEwayBill:gs.allowEwayBill,
    });

    return gsGroup;
  }


  // private buildNumberRange() : FormArray {

  //   const itemArr = new FormArray([]);

  //   if(this.numberRangeConfigurations){
  //     this.numberRangeConfigurations.forEach(item => {
  //       itemArr.push(this.makeNumberRangeItem(item));
  //     })

  //     //console.log('itemArr: ', itemArr);

  //   }

  //   return itemArr;
  // }  


  private makeNumberRangeItem(numberRangeConfiguration: NumberRangeConfiguration): FormGroup {

    return this.fb.group({
      id: [numberRangeConfiguration.id],
      deleted: [numberRangeConfiguration.deleted],
      transactionTypeId: [numberRangeConfiguration.transactionTypeId],
      transactionTypeName: [numberRangeConfiguration.transactionTypeName],
      prefix: [numberRangeConfiguration.prefix],
      postfix: [numberRangeConfiguration.postfix],
      startNumber: [numberRangeConfiguration.startNumber, Validators.pattern('[0-9]+')],
      autoNumber: [numberRangeConfiguration.autoNumber],
      autoNumberReset: [numberRangeConfiguration.autoNumberReset],
      termsAndConditionCheck: [numberRangeConfiguration.termsAndConditionCheck],
      printType: [numberRangeConfiguration.printType],
      displayOrder: [numberRangeConfiguration.displayOrder],
      companyId: [numberRangeConfiguration.companyId],
      delimiter: [numberRangeConfiguration.delimiter],
      financialYearCheck: [numberRangeConfiguration.financialYearCheck],
      printTemplateId: [numberRangeConfiguration.printTemplateId],
      printTemplateTopSize: [numberRangeConfiguration.printTemplateTopSize],
      isZeroPrefix: [numberRangeConfiguration.isZeroPrefix],
      printHeaderText: [numberRangeConfiguration.printHeaderText]


    });
  }

  private makeUnitOfMeasurementItem(uom: Uom): FormGroup {
    return this.fb.group({
      id: [uom.id],
      deleted: [uom.deleted],
      name: [uom.name]
    });
  }

  private makeTermsAndConditionItem(tnc: TermsAndCondition): FormGroup {
    return this.fb.group({
      id: [tnc.id],
      name: [tnc.name],
      termsAndCondition: [tnc.termsAndCondition],
      paymentTerms: [tnc.paymentTerms],
      deliveryTerms: [tnc.deliveryTerms],
      defaultTermsAndCondition: [tnc.defaultTermsAndCondition],
      transactionTypeId: [tnc.transactionTypeId],
      deleted: [tnc.deleted]
    });
  }

  private makeTaxItem(tax: Tax): FormGroup {
    return this.fb.group({
      id: [tax.id],
      name: [tax.name],
      rate: [tax.rate, Validators.compose([Validators.required, Validators.pattern(/^(?:[0-9]{1,2}(?:\.[0-9]{0,2})?)?$/)])],
      deleted: [tax.deleted],
      taxTypeId: [tax.taxTypeId],
    })
  }

  // private makeRolesItem(role: Role): FormGroup {
  //   return this.fb.group({
  //     id: [role.id],
  //     role: [role.role],
  //   })
  // }

  private initGlobalSettings(globalSetting: GlobalSetting) {
    // this.globalSettingService.getGlobalSetting()
    //   .subscribe(response => {
    //this.globalSetting = response;
    //console.log('response before patch: ', response);

    const control = <FormGroup>this.settingsForm.controls['globalSetting'];
    //console.log('control before: ', control);
    //control.patchValue(this.getBasicSettingsGroup(response));
    //control.patchValue({financialYearId: '1'});
    let gs: GlobalSetting = globalSetting; //this.globalSetting;

    ////console.log('gs.financialYearId: ',gs.financialYearId);

    ////console.log('response.financialYearId: ',response.financialYearId);



    control.patchValue({
      id: gs.id,
      deleted: gs.deleted,
      financialYearId: gs.financialYearId,
      stockIncreaseOnId: gs.stockIncreaseOnId,
      stockDecreaseOnId: gs.stockDecreaseOnId,
      itemLevelTax: gs.itemLevelTax,
      itemLevelDiscount: gs.itemLevelDiscount,
      browserPath: gs.browserPath,
      salePrice: gs.salePrice,
      purchasePrice: gs.purchasePrice,
      specialPriceCalculation: gs.specialPriceCalculation,
      inclusiveTax: gs.inclusiveTax,
      barCodeStatus: gs.barCodeStatus,
      printStatus: gs.printStatus,
      includeSignatureStatus: gs.includeSignatureStatus,
      helpDocument: gs.helpDocument,
      sgstPercentage: gs.sgstPercentage,
      cgstPercentage: gs.cgstPercentage,
      disableItemGroup: gs.disableItemGroup,
      cutomerMateriaBinding: gs.cutomerMateriaBinding,
      showOnlyCustomerMaterialInSales: gs.showOnlyCustomerMaterialInSales,
      roundOffTaxTransaction: gs.roundOffTaxTransaction,
      stockCheckRequired: gs.stockCheckRequired,
      itemLevelTaxPurchase: gs.itemLevelTaxPurchase,
      enableTally: gs.enableTally,
      allowReuse:gs.allowReuse,
      allowPartyCode:gs.allowPartyCode,
      allowEwayBill: gs.allowEwayBill,
    });
    //console.log('control after patch: ', control);
    //console.log('patched GS', response);

    // })

  }
  private initNumberRangeConfigurations(numberRangeConfiguration: NumberRangeConfiguration[]) {
   
    //let ncOut : NumberRangeConfiguration[];

    const control = <FormArray>this.settingsForm.controls['numberRangeConfigurations'];

    // this.numberRangeConfigService.getAllConfig()
    //   .subscribe(response => {
     

    numberRangeConfiguration.forEach(item => {
      console.log( "item")
      let fg: FormGroup = this.makeNumberRangeItem(item);
      if (item.autoNumber === 0 || item.autoNumber === null) {
        fg.get('autoNumberReset').disable();
      }
      if (item.transactionTypeName === AppSettings.PARTY_TYPE_CUSTOMER_CODE || item.transactionTypeName === AppSettings.PARTY_TYPE_SUPPLIER_CODE) {
        fg.get('financialYearCheck').disable();
        //fg.get('autoNumber').disable();
        fg.get('autoNumberReset').disable();
        fg.get('isZeroPrefix').disable();
        fg.get('postfix').disable();
        if(this.partyCount===1 || (item.transactionTypeName === AppSettings.PARTY_TYPE_CUSTOMER_CODE && this.partyCount===2)||(item.transactionTypeName === AppSettings.PARTY_TYPE_SUPPLIER_CODE && this.partyCount===3)){
          fg.get('startNumber').disable();
          // fg.get('prefix').disable();
          // fg.get('delimiter').disable();
        }
      
       }
   
      control.push(fg);


    });
    this.disableTransactionType(); 
    this.addBlankNRItem();
       
   

  }
  disableTransactionType(){
    this.settingsForm.controls['numberRangeConfigurations'].valueChanges.subscribe(val => {
  
      this.excludedTransctionTypeIds = [];
      val.forEach(v => {
      this.excludedTransctionTypeIds.push(v.transactionTypeId);
       })
    })
  }



  private initTaxes(taxes: Tax[]) {
    // this.taxService.getAllTaxes()
    //   .subscribe(response => {


    const control = <FormArray>this.settingsForm.controls['taxes'];
    taxes.forEach(item => {
      control.push(this.makeTaxItem(item));
    });

    this.addBlankTaxItem();


    // });
  }



  private initTermsAndConditions(termsAndCondition: TermsAndCondition[]) {

    //console.log('Init TnC 1');

    // this.termsAndConditionsService.getAllTncs()
    //   .subscribe(response => {

    //console.log('Init TnC: ', response);

    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];
    termsAndCondition.forEach(item => {
      control.push(this.makeTermsAndConditionItem(item))
      // });

    });
    this.addBlankTCItem();
  }

  private initUoms(uoms: Uom[]) {
    // this.uomService.getAllUom()
    //   .subscribe(response => {


    const control = <FormArray>this.settingsForm.controls['unitOfMeasurements'];
    uoms.forEach(item => {
      control.push(this.makeUnitOfMeasurementItem(item));
      // });


    });
    this.addBlankUomItem();
  }

  // private initRoles() {
  //   this.userService.getAllRoles()
  //     .subscribe(response => {
  //       this.roles = response;

  //       const control = <FormArray>this.settingsForm.controls['roles'];
  //       this.roles.forEach(item => {
  //         control.push(this.makeRolesItem(item));
  //       });

  //       this.addBlankRoleItem();

  //     })
  // }
  private addBlankUomItem() {
    const control = <FormArray>this.settingsForm.controls['unitOfMeasurements'];

    let item: Uom = {
      id: null,
      deleted: null,
      name: null,
    }

    control.push(this.makeUnitOfMeasurementItem(item));
  }
  private addBlankTaxItem() {
    const control = <FormArray>this.settingsForm.controls['taxes'];
    //Add one blank Item
    let item: Tax = {
      id: null,
      name: null,
      rate: null,
      deleted: null,
      taxTypeId: null,
    };

    control.push(this.makeTaxItem(item));
  }


  private addBlankNRItem() {
    const control = <FormArray>this.settingsForm.controls['numberRangeConfigurations'];
    //Add one blank Item
    let item: NumberRangeConfiguration = {
      id: null,
      deleted: null,
      transactionTypeId: null,
      transactionTypeName: null,
      prefix: null,
      postfix: null,
      startNumber: null,
      autoNumber: null,
      autoNumberReset: null,
      termsAndConditionCheck: null,
      printType: null,
      displayOrder: null,
      companyId: null,
      delimiter: null,
      financialYearCheck: null,
      printTemplateId: null,
      printTemplateTopSize: null,
      isZeroPrefix: null,
      isJasperPrint: null,
      jasperFileName: null,
      printHeaderText: null,
      allowShipingAddress:null
    };

    control.push(this.makeNumberRangeItem(item));
  }
  private addBlankTCItem() {
    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];
    //Add one blank Item
    let item: TermsAndCondition = {
      id: null,
      name: null,
      termsAndCondition: null,
      paymentTerms: null,
      deliveryTerms: null,
      defaultTermsAndCondition: null,
      transactionTypeId: null,
      deleted: null,

    };

    control.push(this.makeTermsAndConditionItem(item));
  }
  // private addBlankRoleItem() {
  //   const control = <FormArray>this.settingsForm.controls['roles'];

  //   let item: Role = {
  //     id: null,
  //     role: null,
  //     roleType: null,
  //   }

  //   control.push(this.makeRolesItem(item));
  // }

  private checkAndAddNewRow(idx: number) {
    //console.log('idx: ', idx);
    const control = <FormArray>this.settingsForm.controls['taxes'];

    //console.log('control max size: ', control.length);

    if (idx === control.length - 1) {
      this.addBlankTaxItem()
    }

  }

  private removeTaxItem(idx: number) {

    const control = <FormArray>this.settingsForm.controls['taxes'];

    //console.log('taxes before delete', control.at(idx).get('id').value);

    let taxId: number = control.at(idx).get('id').value;

    if (!taxId) {
      //Removing just a dummy row
      (<FormArray>this.settingsForm.get('taxes')).removeAt(idx);
      return true;
    }

    this.taxService.delete(taxId).subscribe(response => {
      //console.log('Deleted: ', response);
      if (response.responseStatus === 1) {
        (<FormArray>this.settingsForm.get('taxes')).removeAt(idx);
      }
      else {
        //console.log('Cannot delete!!');
        this.alertService.warning("Tax is being used elsewhere cannot delete!!");
        // this.alertService.warning("Tax already Used!! Cannot delete.");
      }
    },
      error => {
        //console.log('Error: ', error);
        this.alertService.danger('Cannot delete!!');

      })



    return false;
  }
  private removeTCItem(idx: number) {
    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];

    //console.log('TNC before delete', control.at(idx).get('id').value);

    let tcnId: number = control.at(idx).get('id').value;

    if (!tcnId) {
      //Removing just a dummy row
      (<FormArray>this.settingsForm.get('termsAndConditions')).removeAt(idx);
      return true;
    }

    this.termsAndConditionsService.delete(tcnId).subscribe(response => {
      //console.log('Deleted: ', response);

      if (response.responseStatus === 1) {
        (<FormArray>this.settingsForm.get('termsAndConditions')).removeAt(idx);
      }
      else {
        //console.log('Cannot delete!!');
        this.alertService.warning("Terms And Conditions is being used elsewhere cannot delete!!");
      }
    },
      error => {
        //console.log('Error: ', error);
        this.alertService.danger('Cannot delete!!');
      })


    return false;
  }
  private removeNumberRangeItem(idx: number) {

    const control = <FormArray>this.settingsForm.controls['numberRangeConfigurations'];

    //console.log('NRC before delete', control.at(idx).get('id').value);

    let tnxId: number = control.at(idx).get('id').value;
    if (!tnxId) {
      //Removing just a dummy row
      (<FormArray>this.settingsForm.get('numberRangeConfigurations')).removeAt(idx);
      return true;
    }
    this.numberRangeConfigService.delete(tnxId).subscribe(response => {
      //console.log('Deleted: ', response);

      if (response.responseStatus === 1) {
        (<FormArray>this.settingsForm.get('numberRangeConfigurations')).removeAt(idx);
      }
      else {
        //console.log('Cannot delete!!');
        this.alertService.warning("Number Range Configurations is being used elsewhere cannot delete!!");
      }
    },
      error => {
        //console.log('Error: ', error);
        this.alertService.danger('Cannot delete!!');
      })


    return false;
  }

  private checkAndAddNewNRRow(idx: number) {
    //console.log('idx: ', idx);
    const control = <FormArray>this.settingsForm.controls['numberRangeConfigurations'];


    // for(let i=0; i< control.length; i++){
    //   this.transactionTypes = this.transactionTypes.filter(tt => tt.id !== control.at(i).get('transactionTypeId').value)
    // }



    //console.log('control max size: ', control.length);

    if (idx === control.length - 1) {
      this.addBlankNRItem()
    }
  }

  checkAndAddNewUomRow(idx: number) {
    //console.log('idx: ', idx);
    const control = <FormArray>this.settingsForm.controls['unitOfMeasurements'];

    //console.log('control max size: ', control.length);

    if (idx === control.length - 1) {
      this.addBlankUomItem();
    }
  }

  checkAndAddNewTCRow(idx: number) {
    //console.log('idx: ', idx);
    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];
    let tcnTransId: number = control.at(idx).get('transactionTypeId').value;

    //console.log(" tcTransId ", tcnTransId);

    if (tcnTransId)
      //console.log('control max size: ', control.length);

      if (idx === control.length - 1) {
        this.addBlankTCItem();
      }
  }

  removeUomItem(idx: number) {

    const control = <FormArray>this.settingsForm.controls['unitOfMeasurements'];

    let uomId: number = control.at(idx).get('id').value;
    if (!uomId) {
      //Removing just a dummy row
      (<FormArray>this.settingsForm.get('unitOfMeasurements')).removeAt(idx);
      return true;
    }
    if (uomId) {
      this.uomService.delete(uomId).subscribe(response => {

        //console.log("Deleted: ", response);
        if (response.responseStatus === 1) {
          (<FormArray>this.settingsForm.get('unitOfMeasurements')).removeAt(idx);
        }
        else {
          //console.log('Cannot delete!!');
          this.alertService.warning("unit Of Measure is being used elsewhere cannot delete!!");
        }
      },
        error => {
          //console.log("Error: ", error);
          this.alertService.danger('Cannot delete!!');
        });
    }
    return false;
  }

  // checkAndAddNewRoleRow(idx: number) {
  //   //console.log('idx: ', idx);
  //   const control = <FormArray>this.settingsForm.controls['roles'];

  //   //console.log('control max size: ', control.length);

  //   if (idx === control.length - 1) {
  //     this.addBlankRoleItem();
  //   }
  // }

  // removeRoleItem(idx: number) {

  //   const control = <FormArray>this.settingsForm.controls['roles'];

  //   let roleId: number = control.at(idx).get('id').value;
  //   if (!roleId) {
  //     //Removing just a dummy row
  //     (<FormArray>this.settingsForm.get('roles')).removeAt(idx);
  //     return true;
  //   }
  //   if (roleId) {

  //     this.userService.deleteRole(roleId).subscribe(response => {

  //       //console.log(response);
  //       if (response.responseStatus === 1)
  //       {
  //         (<FormArray>this.settingsForm.get('roles')).removeAt(idx);
  //       }
  //       else
  //       {
  //         //console.log('Cannot delete!!');
  //         this.alertService.warning("Role is being used elsewhere cannot delete!!");
  //       }
  //     },
  //       error => {
  //         //console.log('Cannot delete!!', error);
  //         this.alertService.danger('Cannot delete!!');
  //       });

  //   }
  //   return false;
  // }

  getTransactionTypes() {
    this.transactionTypeService.getAllTransactionTypes()
      .subscribe(response => {
        //console.log("response...........................>>>>>>>>>>>>>>",response)
        for (var i = 0; i < response.length; i++) {

          this.transactionTypes.push(response[i])

        }

        let printTransType: string[] = [
          AppSettings.CUSTOMER_INVOICE,
          AppSettings.PROFORMA_INVOICE,
          AppSettings.SUPPLIER_PO,
          AppSettings.DEBIT_NOTE,
          AppSettings.CREDIT_NOTE,
          AppSettings.CUSTOMER_QUOTATION,
          AppSettings.CUSTOMER_DC,
          AppSettings.INCOMING_JOBWORK_INVOICE,
          AppSettings.OUTGOING_JOBWORK_PO,
          AppSettings.INCOMING_JOBWORK_OUT_DC,
          AppSettings.OUTGOING_JOBWORK_OUT_DC,
          AppSettings.INCOMING_JOBWORK_PO
        ]

        this.printTemplateTransactionTypes = response.filter(ttype => printTransType.includes(ttype.name))


        this.checkTransactionCount();
        // this.transactionTypes = response;
        //console.log("this.transactionTypes...........................>>>>>>>>>>>>>>",this.transactionTypes);
      })
  }



  getTnC() {

    //console.log(this.transactionType);

    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];
    //console.log('control: ', control);

    for (let i = 0; i < control.length; i++) {
      //console.log('control.at(i).get(transactionTypeId).value: ', control.at(i).get('transactionTypeId').value)
      //console.log('this.transactionType.id: ', this.transactionType.id);
      if (control.at(i).get('transactionTypeId').value === this.transactionType.id) {
        this.currentTncIdx = i;
        //console.log('this.currentTncIdx: ', this.currentTncIdx);
        //this.tncGroup = <FormGroup>control.at(i);
        break;
      }
      else {
        this.currentTncIdx = -1;
      }
    }


  }

  checkDuplicate(model: SettingsWrapper): boolean {

    //console.log("in duplicate")
    const taxControl = <FormArray>this.settingsForm.controls['taxes'];
    // //console.log("contro;", control.get('rate').value)
    //console.log("this.taxes", this.taxes)
    for (let i = 0; i < taxControl.length - 1; i++) {
      let val = taxControl.at(i).get('rate').value;
      for (let j = i + 1; j < taxControl.length; j++) {
        let val2 = taxControl.at(j).get('rate').value;
        //console.log("val", val, val2)
        if (val2 != null && val == val2) {
          //console.log("dupli")
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false

          });
          this.alertRef.componentInstance.alertMessage = "Tax rate already exists!!!"
          taxControl.at(j).get('rate').patchValue("")
          return false;
        }
      }
    }
    const uomControl = <FormArray>this.settingsForm.controls['unitOfMeasurements'];
    // //console.log("contro;", control.get('rate').value)

    for (let i = 0; i < uomControl.length - 1; i++) {
      let val = uomControl.at(i).get('name').value;
      for (let j = i + 1; j < uomControl.length; j++) {
        let val2 = uomControl.at(j).get('name').value;
        //console.log("val", val, val2)
        if (val2 != null && val == val2) {
          //console.log("dupli")
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false

          });
          this.alertRef.componentInstance.alertMessage = "Unit Of Measure already exists!!!"
          uomControl.at(j).get('name').patchValue("")
          return false;
        }
      }
    }
    const roleControl = <FormArray>this.settingsForm.controls['roles'];
    // //console.log("contro;", control.get('rate').value)
    for (let i = 0; i < roleControl.length - 1; i++) {
      let val = roleControl.at(i).get('role').value;
      for (let j = i + 1; j < roleControl.length; j++) {
        let val2 = roleControl.at(j).get('role').value;
        //console.log("val", val, val2)
        if (val2 != null && val == val2) {
          //console.log("dupli")
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false

          });
          this.alertRef.componentInstance.alertMessage = "Role already exists!!!"
          roleControl.at(j).get('role').patchValue("")
          return false;
        }
      }
    }
    return true;
  }

  private cleanUp(model: SettingsWrapper): SettingsWrapper {

    model.numberRangeConfigurations = model.numberRangeConfigurations.filter(num => num.transactionTypeId);
    model.taxes = model.taxes.filter(tax => tax.taxTypeId);
    model.unitOfMeasurements = model.unitOfMeasurements.filter(unit => unit.name)
    model.roles = model.roles.filter(role => role.role)
    model.termsAndConditions = model.termsAndConditions.filter(tcn => tcn.transactionTypeId)

    return model;

  }

  closeForm() {
    this._router.navigate(['/']);
    // this._router.navigateByUrl('/dashboard')
  }

  onSubmit(model: SettingsWrapper) {


    //console.log('Before clearing ', model);
    //console.log('Before tax: ', model.taxes);
    let checkDuplicate: boolean = this.checkDuplicate(model);
    //console.log("checkduplit return", checkDuplicate)
    if (!checkDuplicate) {
      //console.log("invlid")
      return false;
    }
    //   if(!this.checkDuplicate){
    //     //console.log("inside")
    //    return;
    // }
    let outModel: SettingsWrapper = this.cleanUp(model);
    //console.log('aftr clearing ', outModel);
    //Convert boolean to number for some check boxes

    //console.log(model.globalSetting.showOnlyCustomerMaterialInSales);

    model.globalSetting.itemLevelDiscount = model.globalSetting.itemLevelDiscount ? 1 : 0;
    model.globalSetting.inclusiveTax = model.globalSetting.inclusiveTax ? 1 : 0;
    model.globalSetting.itemLevelTax = model.globalSetting.itemLevelTax ? 1 : 0;
    model.globalSetting.disableItemGroup = model.globalSetting.disableItemGroup ? 1 : 0;
    model.globalSetting.cutomerMateriaBinding = model.globalSetting.cutomerMateriaBinding ? 1 : 0;
    model.globalSetting.showOnlyCustomerMaterialInSales = model.globalSetting.showOnlyCustomerMaterialInSales ? 1 : 0;
    model.globalSetting.roundOffTaxTransaction = model.globalSetting.roundOffTaxTransaction ? 1 : 0;
    model.globalSetting.stockCheckRequired = model.globalSetting.stockCheckRequired ? 1 : 0;
    model.globalSetting.itemLevelTaxPurchase = model.globalSetting.itemLevelTaxPurchase ? 1 : 0;
    model.globalSetting.allowPartyCode=model.globalSetting.allowPartyCode ? 1 : 0;
    model.numberRangeConfigurations.forEach(nrc => {
      nrc.autoNumberReset = nrc.autoNumberReset ? 1 : 0;
      nrc.autoNumber = nrc.autoNumber ? 1 : 0;
      nrc.financialYearCheck = nrc.financialYearCheck ? 1 : 0;
      nrc.isZeroPrefix = nrc.isZeroPrefix ? 1 : 0;




      // nrc.termsAndConditionCheck = nrc.termsAndConditionCheck ? 1 : 0;
    })
    model.termsAndConditions.forEach(tcn => {
      tcn.defaultTermsAndCondition = tcn.defaultTermsAndCondition ? 1 : 0;
      // if(tcnTransId )
    })
    //console.log('Before saving...', outModel);


    // if (checkDuplicate === true) {
    this.spinStart = true;
    this.globalSettingService.saveWrapperSetting(outModel)
      .subscribe(response => {
        //console.log("Saved: ", response);

        //  this.settingsForm.controls['id'].patchValue(response.globalSetting.id);
        // this.settingsForm.controls['termsAndConditions'].patchValue(response.termsAndConditions);

        this.saveStatus = true;
        this.spinStart = false;
        this.transactionStatus = AppSettings.SAVE_SUCESSFULL_MESSAGE
        this.alertService.success(this.transactionStatus);
        //this.bootstrapAlertService.alert(new BootstrapAlert("BootstrapAlert Message!", "alert-success"));

        //Recreate global setting in local storage
        this.globalSettingService.removeLocalGS();
        this.globalSettingService.storeLocalGS(response.globalSetting);
        this.initFunctions();
      },
        error => {
          console.error("Error: ", error);
          this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
          this.alertService.danger(this.transactionStatus);
          this.saveStatus = false;
          this.spinStart = false;
        })
    // }
  }

  checkIsDefaultForTC(event: DefaultCheckTC) {
    // let transTypeId: number;
    //console.log('In checkIsDefaultForTC', event);
    let transTypeId: number = event.transactionTypeId;
    let index: number = event.index;

    const control = <FormArray>this.settingsForm.controls['termsAndConditions'];
    //console.log('i default control: ', control);
    for (let i = 0; i < control.length; i++) {
      //console.log('in default function transaction type id value: ', control.at(i).get('transactionTypeId').value)
      //console.log('in default function is default value: ', control.at(i).get('defaultTermsAndCondition').value)
      if (control.at(i).get('transactionTypeId').value === transTypeId
        && (control.at(i).get('defaultTermsAndCondition').value === 1 ||
          control.at(i).get('defaultTermsAndCondition').value === true) && i != index) {

        this.alertRef = this.dialog.open(AlertDialog, {
          disableClose: false

        });
        this.alertRef.componentInstance.alertMessage = AppSettings.TERMS_AND_CONDITION_MESSAGE
        control.at(index).get('defaultTermsAndCondition').patchValue(false)
      }
      else {
        //console.log("continue")
      }

    }

  }

  checkTransactionCount() {
    let sellTranTypes: string[] = [
      AppSettings.CUSTOMER_PO,
      AppSettings.CUSTOMER_INVOICE,
      AppSettings.CUSTOMER_DC,
      AppSettings.CUSTOMER_QUOTATION,
      AppSettings.PROFORMA_INVOICE,
      AppSettings.CREDIT_NOTE,
      // AppSettings.INCOMING_JOBWORK_INVOICE, 
      // AppSettings.INCOMING_JOBWORK_PO,
      // AppSettings.INCOMING_JOBWORK_INVOICE
    ];
    let buyTranTypes: string[] = [
      AppSettings.SUPPLIER_PO,
      AppSettings.PURCHASE_INVOICE,
      AppSettings.INCOMING_DC,
      AppSettings.GRN_TYPE_SUPPLIER,
      AppSettings.DEBIT_NOTE,
      // AppSettings.OUTGOING_JOBWORK_PO, 
      // AppSettings.OUTGOING_JOBWORK_INVOICE
    ];
    let buyTransIds: number[] = this.transactionTypes
      .filter(t => buyTranTypes.includes(t.name))
      .map(t => t.id);

    let sellTransIds: number[] = this.transactionTypes
      .filter(t => sellTranTypes.includes(t.name))
      .map(t => t.id);


    this.transactionTypeService.checkTransactionExistis(sellTransIds)
      .subscribe(
        response => {
          //console.log("response in count", response);

          if (response) {
            //this.disableItemLevel = true;
            this.hintValue = "You have already created a transaction!! You Can't change value now...";
            this.settingsForm.get("globalSetting.itemLevelDiscount").disable();
            this.settingsForm.get("globalSetting.itemLevelTax").disable();
          } else {
            //this.disableItemLevel = false;
          }
        }
      )

    this.transactionTypeService.checkTransactionExistis(buyTransIds)
      .subscribe(
        response => {
          //console.log("response in count", response);

          if (response) {
            //this.disableItemLevel = true;
            this.hintValue = "You have already created a transaction!! You Can't change value now...";
            this.settingsForm.get("globalSetting.itemLevelTaxPurchase").disable();
          } else {
            //this.disableItemLevel = false;
          }
        }
      )
  }


  onSetTheme(theme) {
    this.themeClass = theme;
    //this.currentUser.userThemeName =theme;

    let localStorageUser: any = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));
    //console.log('localStorageUser: ',localStorageUser);
    let localStorageUserUser = localStorageUser.user;
    //console.log('localStorageUserUser: ',localStorageUserUser);
    let localStorageUserToken = localStorageUser.token;

    localStorageUserUser.userThemeName = theme;
    localStorageUserUser.userLanguage = this.language;
    localStorage.setItem(AppSettings.CURRENT_USER, JSON.stringify({ user: localStorageUserUser, token: localStorageUserToken }));

    //console.log("theme", theme)
    this.save(this.themeClass);
    this.userService.themeClass.next(this.themeClass);
  }

  save(theme: string) {
    let model: UserModel;

    //console.log("in sve theme", theme)
    //console.log("in sve theme", this.currentUser.id)
    this.userService.saveUserTheme(this.currentUser.id, theme).subscribe(response => {
      if (response.responseStatus === 1) {
        this.saveStatus = true;
        //console.log("done")
        // this.alertService.success("Theme saved sucessfully");
      }
      else {
        this.saveStatus = false;
        //console.log("error")
        // this.alertService.danger("Failed to save theme ");
      }
    }



      ,
      error => {
        this.saveStatus = false;
        //console.log("error")
        // this.alertService.danger("Failed to save theme ");
      });




  }


  public setLanguage(lang: string) {
    this.language = lang;
    this.translate.use(lang);
    this.languageService.setLanguage(lang);

    let localStorageUser: any = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER));

    let localStorageUserUser = localStorageUser.user;

    let localStorageUserToken = localStorageUser.token;

    localStorageUserUser.userLanguage = lang;

    localStorage.setItem(AppSettings.CURRENT_USER, JSON.stringify({ user: localStorageUserUser, token: localStorageUserToken }));
    this.saveLanguage(this.language);
    this.userService.language.next(this.language);


  }
  saveLanguage(language: string) {
    let model: UserModel;


    this.userService.saveUserLanguage(this.currentUser.id, language).subscribe(response => {
      if (response.responseStatus === 1) {
        this.saveStatus = true;


      }
      else {
        this.saveStatus = false;

      }
    }



      ,
      error => {
        this.saveStatus = false;
        //console.log("error")
        // this.alertService.danger("Failed to save theme ");
      });




  }
  helpOpen(value:number){
   let helpDetails:string="";
  this.alertRef = this.dialog.open(AlertDialog, {
    disableClose: false
    });
   switch(value){
    case 1:
    helpDetails=AppSettings.STOCK_INCREASE;
      break;

      case 2:
      helpDetails=AppSettings.STOCK_DECREASE;
      break;

      case 3:
      helpDetails=AppSettings.ITEM_LEVEL;
      break;

      case 4:
      helpDetails=AppSettings.INCLUSIVE_TAX;
      break;

      case 5:
      helpDetails=AppSettings.ROUND_OFF;
      break;

      case 6:
      helpDetails=AppSettings.SPECIAL_PRICE;
      break;
      
      case 7:
      helpDetails=AppSettings.STOCK_CHECK;
      break;
   }

this.alertRef.componentInstance.alertMessage = helpDetails;
}

}
