import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PrintCopiesWrapper, PrintCopy } from '../../data-model/print-copies-model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AppSettings } from '../../app.settings';
import { GlobalSettingService } from '../../services/global-setting.service';
import { SettingsWrapper } from '../../data-model/settings-wrapper';
import { AlertService } from 'ngx-alerts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-print-copies',
  templateUrl: './print-copies.component.html',
  styleUrls: ['./print-copies.component.scss']
})
export class PrintCopyComponent implements OnInit {


  printCopiesForm: FormGroup;
  dialogRef: MatDialogRef<ConfirmationDialog>;

  public printCopy: PrintCopy[] = [];
  printCopiesWrapper: PrintCopiesWrapper[] = [];

  transactionStatus: string = null;

  constructor(private fb: FormBuilder
    , public dialog: MatDialog
    , private globalSettingService: GlobalSettingService
    , private alertService: AlertService
    , private _router: Router) { }

  ngOnInit() {
    this.initForm();
    this.getPrintCopies();
    
  }

  private getPrintCopies() {

    this.globalSettingService.getAllPrintCopies()
      .subscribe(response => {
        this.printCopy = response;
        //console.log("PrintCopy :", this.printCopy)


        const control = <FormArray>this.printCopiesForm.controls['printCopiesList'];



        this.printCopy.forEach(item => {
          //console.log("item :", item);
          control.push(this.makePrintCopies(item));
        })
        this.add();
      })
  }

  private initForm() {
    // let itemData: PrintCopies = {
    //   id: null,
    //   name: null,
    //   deleted: null
    // }
    let data: PrintCopiesWrapper = {
      printCopiesList: null,
    }
    this.printCopiesForm = this.toFormGroup(data);
  }

  private makePrintCopies(printCopies: PrintCopy): FormGroup {

    return this.fb.group({
      id: [printCopies.id],
      deleted: [printCopies.deleted],
      name: [printCopies.name]
    });
  }



  private toFormGroup(data: PrintCopiesWrapper): FormGroup {
    const printCopiesArray = new FormArray([]);
    if (data.printCopiesList) {
      data.printCopiesList.forEach(item => {
        printCopiesArray.push(this.makePrintCopies(item));
      })
    }
    //console.log("printCopiesArray", printCopiesArray)
    const formGroup = this.fb.group({
      printCopiesList: printCopiesArray,
    });
    //console.log("formGroup", formGroup)
    return formGroup;
  }

  private add() {
    //console.log("add.......print");

    const control = <FormArray>this.printCopiesForm.controls['printCopiesList'];
    //console.log("control.....", control);
    //Add one blank Item
    let data: PrintCopy = {
      id: null,
      deleted: null,
      name: null,
    };

    //console.log("data.....", data);
    control.push(this.makePrintCopies(data));
  }



  delete(idx: number) {
    //console.log("idx :", idx);
    const control = <FormArray>this.printCopiesForm.controls['printCopiesList'];
    let id: number = control.at(idx).get('id').value;
    //console.log("id :", id);

    if(idx === 0 && control.length <= 2){
      this.alertService.danger('Cannot delete all entries');
      return;
    }
    if (id == null) {
      (<FormArray>this.printCopiesForm.get('printCopiesList')).removeAt(idx);
      return;
    }

    else {
      this.globalSettingService.delete(id)
        .subscribe(response => {
          //console.log('Deleted: ', response);
          if (response.responseStatus === 1) {
            (<FormArray>this.printCopiesForm.get('printCopiesList')).removeAt(idx);
            this.alertService.success("Print copy deleted");
          }
          else {
            //console.log('Cannot delete!!');
            this.alertService.danger('Cannot delete already used ');
          }
        },
          error => {
            //console.log('Error: ', error);
            this.alertService.danger('Cannot delete!!');
          })

      return false;
    }
  }

  private cleanUp(model: PrintCopiesWrapper): PrintCopiesWrapper {
    // Strip out null rows which are added at the end just for ease of user entry purposes.
    //let outModel : SettingsWrapper = <SettingsWrapper>{};

    //outModel.globalSetting = model.globalSetting;

    model.printCopiesList.forEach(pc => {
      if (pc.name === null) {
        model.printCopiesList.pop();
      }
    });
    
    return model;

  }

  onSubmit(model: PrintCopiesWrapper) {
    //console.log('saving: ', model);
    let outModel: PrintCopiesWrapper = this.cleanUp(model);
    this.globalSettingService.save(outModel)
      .subscribe(response => {
        //console.log("response", response);
        this.transactionStatus = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        this.alertService.success(this.transactionStatus);
        setTimeout(() => {          
          this._router.navigate(['/']);
        }, 1000);
      },
        error => {
          console.error(error);
          this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
          this.alertService.danger(this.transactionStatus);
        })
  }



}
