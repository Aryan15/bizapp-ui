import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintCopyComponent } from './print-copies.component';

describe('PrintCopiesComponent', () => {
  let component: PrintCopyComponent;
  let fixture: ComponentFixture<PrintCopyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintCopyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
