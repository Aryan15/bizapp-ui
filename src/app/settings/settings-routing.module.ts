import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { PreventUnsavedChangesGuardSettings } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-global-settings';
import { UserRoleComponent } from '../master/user-role/user-role.component';


const routes: Routes = [
  {
    path: 'global-settings',
    component: GlobalSettingsComponent,
    data: { state: 'global-settings' },
    canDeactivate: [PreventUnsavedChangesGuardSettings]
  },
  // {
  //   path: 'app-po-report/:trantypeId',
  //   component: PoReportComponent,
  //   data: { state: 'app-po-report' }
  // },
  {
    path: 'app-user-role',
    component: UserRoleComponent,
    data: {state: 'app-user-role'},        
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
