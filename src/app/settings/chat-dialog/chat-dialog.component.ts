import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, Subscription, Subject } from 'rxjs';
import { ChatService } from '../../services/chat.service';
import { scan, tap } from 'rxjs/operators';
import { Message } from '../../data-model/misc-model';
import { SpeechService } from '../../services/speech.service';
import SpeechToText from 'speech-to-text';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.scss']
})
export class ChatDialogComponent implements OnInit {

  @ViewChild('scrollMe', {static: true}) private myScrollContainer: ElementRef;
  messages: Subject<Message[]>;
  //action: Observable<string>;
  actionText: string;
  formValue: string;
  commandSub: Subscription;
  listening: boolean = false;
  isOpen: boolean = false;
  

  constructor(public chat: ChatService, private router: Router) { }

  ngOnInit() {
    // appends to array after each new message is added to feedSource
    this.messages = this.chat.conversation.asObservable() 
    .pipe(
      scan((acc, val) => acc.concat(val) ),
      tap(val => {
        //console.log(val);
        this.scrollToBottom();
      })
    ) as Subject<Message[]>;

    this.chat.action.subscribe(resp => {
      if(resp === 'open_balance_report'){
        this.router.navigate(['report/app-balance-report',1]);
      }
    })

        
  }

  sendMessage() {
    ////console.log('send message: ', this.formValue);
    this.chat.converse(this.formValue);
    this.formValue = '';
    this.scrollToBottom();
    
  }

  scrollToBottom(): void {
    //console.log("scrolling...")
    try {
        // this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        // this.myScrollContainer.nativeElement.scrollIntoView();
    } catch(err) { 
      console.error(err);
    }                 
}


  

  onAnythingSaid = text => null;////console.log(`Interim text: ${text}`);
  onFinalised = (text: string) => {
    ////console.log(`Finalised text: ${text}`);
    ////console.log("Finalised text:", text);
    text = text.replace('my boss', 'myBOS');
    this.formValue = text;
    this.sendMessage();
  };

  onFinishedListening = () => {
    //console.log("Finished");
  }

  listener = new SpeechToText(this.onAnythingSaid, this.onFinalised, this.onFinishedListening);
  
  startListening(){
    try {
      
      this.listener.startListening();
      this.listening = true;
    } catch (error) {
      //console.log(error);
    }
  }

  abort(){

    try{
      this.listener.stopListening();
      this.listening = false;
    } catch(error){
      //console.log(error);
    }
  }


  toggleOpen(){
    this.isOpen = !this.isOpen
  }

}


