import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TransactionType } from '../../data-model/transaction-type';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'app-transaction-settings',
  templateUrl: './transaction-settings.component.html',
  styleUrls: ['./transaction-settings.component.scss']
})
export class TransactionSettingsComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() idx: number;
  @Input() allTransactionTypes: TransactionType[];
  @Input() taxMaxIndex: number;
  @Input() excludedTransctionTypeIds: number[];
  @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() addIndex: EventEmitter<number> = new EventEmitter<number>();
  dialogRef: MatDialogRef<ConfirmationDialog>;
  
  constructor(public dialog: MatDialog,) { }
  ngOnInit() {
  
  }

  onAutoNumberCheck(event: any) {
   
    if (event.checked) {
       this.group.get('autoNumberReset').enable();
      
                }
             else {
              this.group.get('autoNumberReset').disable();
              this.group.get('autoNumberReset').setValue(false);
             
            }
       
    }
    

  delete(index: number) {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
  });

  this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

  this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
    //console.log("Deleting...", index);
    this.deleteIndex.emit(index);
      }
    
    //this.handleChanges()
    this.dialogRef = null;
});

  }
  add(index: number) {

    //console.log("Adding...", index);
    this.addIndex.emit(index);
  }
  gettooltip(): string{
    if(this.group.get('transactionTypeName').value === AppSettings.PARTY_TYPE_CUSTOMER_CODE ||this.group.get('transactionTypeName').value===AppSettings.PARTY_TYPE_SUPPLIER_CODE)
    {
          return "Max 3 Char For Prefix and 4 number for StartNum"
    }

    if(this.group.get('transactionTypeId').value === null)
    {
      return "Select Transaction Type";
    }
   
  }

 
  getMaxLength(value:number){
    
    if(this.group.get('transactionTypeName').value === AppSettings.PARTY_TYPE_CUSTOMER_CODE ||this.group.get('transactionTypeName').value===AppSettings.PARTY_TYPE_SUPPLIER_CODE)
    {
      if(value===1){
       
        return 3;
      }
    else if(value===2){
      return 4;
    }
    }

   


  }

 

 

}
