import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TaxType } from '../../data-model/tax-model';
import { TaxService } from '../../services/tax.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'app-tax-settings',
  templateUrl: './tax-settings.component.html',
  styleUrls: ['./tax-settings.component.scss']
})
export class TaxSettingsComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() idx: number;
  @Input() taxMaxIndex: number;

  //@Output() returnIdx: EventEmitter<number> = new EventEmitter<number>();
  @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() addIndex: EventEmitter<number> = new EventEmitter<number>();
  taxTypes: TaxType[] = [];
  dialogRef: MatDialogRef<ConfirmationDialog>;
 
  constructor(public dialog: MatDialog,
    private taxService: TaxService) { }

  ngOnInit() {

    this.getTaxTypes();
  }

  private getTaxTypes() {

    this.taxService.getAllTaxTypes().subscribe(response => {

      this.taxTypes = response;
    })

  }


  delete(index: number) {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
  });

  this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

  this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
    //console.log("Deleting...", index);
    this.deleteIndex.emit(index);
      }
    
    //this.handleChanges()
    this.dialogRef = null;
});

  }
  add(index: number) {
    //console.log("Adding...", index);
    this.addIndex.emit(index);
  }


}
