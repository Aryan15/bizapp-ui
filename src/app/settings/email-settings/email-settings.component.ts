import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Email } from '../../data-model/email-model';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { DatePipe } from '@angular/common';
import { AlertService } from 'ngx-alerts';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from '../../utils/utility-service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { AppSettings } from '../../app.settings';

@Component({
  selector: 'app-email-settings',
  templateUrl: './email-settings.component.html',
  styleUrls: ['./email-settings.component.scss']
})
export class EmailSettingsComponent{
  emailForm: FormGroup;
  email: Email;
  saveStatus: boolean = false;
  spinStart: boolean = false;
 dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
   datePipe = new DatePipe('en-US');
  isLoadingResults = true;
  fieldValidatorLabels = new Map<string, string>();

  transactionStatus: string = null;

  public emailType: emailType[] = [
    { id: 1, typeName:"Transaction"},
   
  ]



  constructor(
    private fb: FormBuilder,
   
    protected alertService: AlertService,
    protected dialog: MatDialog,
    public route: ActivatedRoute,
    private utilityService: UtilityService,
    private globalSettingService: GlobalSettingService,
    private _router: Router





  ) { }

  // ngOnInit() {

  //   this.initForm();

  //  this.getEmailSettingss();
  // }



  private initForm() {
    let data: Email = {
  id: null,
  deleted: null,
  emailDebug:null,
  emailFrom:null,
  emailHost:null,
  emailPassword:null,
  emailPort:null,
  emailSmtpAuth:null,
  emailSmtpStartTlsEnable:null,
  emailTransportProtocol:null,
  emailUserName:null,
  emailEnabled:null,
  emailText:null,
  emailSubject:null,
  emailType:null
};

this.emailForm = this.toFormGroup(data);

}


private toFormGroup(es: Email): FormGroup {

  const esGroup = this.fb.group({
    id: [es.id],
    deleted: [es.deleted],
    emailDebug:[es.emailDebug],
    emailFrom:[es.emailFrom],
    emailHost:[es.emailHost],
    emailPassword:[es.emailPassword],
    emailPort:[es.emailPort],
    emailSmtpAuth:[es.emailSmtpAuth],
    emailSmtpStartTlsEnable:[es.emailSmtpStartTlsEnable],
    emailTransportProtocol:[es.emailTransportProtocol],
    emailUserName:[es.emailUserName],
    emailEnabled:[es.emailEnabled],
    emailText:[es.emailText],
    emailSubject:[es.emailSubject],
    emailType:[es.emailType]



  })

  return esGroup;


}

private getEmailSettingss() {
console.log("pppppppppp")
  this.globalSettingService.getEmailSettings().subscribe(response => {
  if(response){
    this.email =response;
    console.log(response.emailHost+"----------")
    this.toFormGroup(response);
  }
  });

}


onSubmit(model: Email) {
  console.log(model.emailEnabled)
  if( model.emailEnabled){
    model.emailEnabled=1;
  }
  else{
    model.emailEnabled=0;
  }
 
  this.globalSettingService.saveEmail(model)
    .subscribe(response => {
      this.transactionStatus = AppSettings.SAVE_SUCESSFULL_MESSAGE;
      this.alertService.success(this.transactionStatus);
      setTimeout(() => {          
        this._router.navigate(['/']);
      }, 1000);
    },
      error => {
        console.error(error);
        this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
        this.alertService.danger(this.transactionStatus);
      })
}







}
export interface emailType {
  id: number,
  typeName: string,
}