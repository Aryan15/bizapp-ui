import { Component, OnInit, OnChanges, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DefaultCheckTC } from '../../data-model/misc-model';
import { TransactionType } from '../../data-model/transaction-type';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AppSettings } from '../../app.settings';


@Component({
  selector: 'app-tn-csettings',
  templateUrl: './tn-csettings.component.html',
  styleUrls: ['./tn-csettings.component.scss']
})
export class TnCSettingsComponent implements OnInit, OnChanges {
  @Input() group: FormGroup;
  @Input() idx: number;
  @Input() allTransactionTypes: TransactionType[];
  @Input() taxMaxIndex: number;

  @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() addIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() defaultCheckValue: EventEmitter<DefaultCheckTC> = new EventEmitter<DefaultCheckTC>();
  dialogRef: MatDialogRef<ConfirmationDialog>;
  constructor(public dialog: MatDialog,) { }
  ngOnInit() {


  }

  ngOnChanges() {

    // //console.log('incoming group: ', this.group);
  }

  delete(index: number) {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
  });

  this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

  this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
    //console.log("Deleting...", index);
    this.deleteIndex.emit(index);
      }
    
    //this.handleChanges()
    this.dialogRef = null;
});

  }

  add(index: number) {

    //console.log("Adding...", index);
    this.addIndex.emit(index);
  }
  checkIsDefault() {
    //console.log("checked", this.group.controls['defaultTermsAndCondition'].value);
    //console.log("transTypeId", this.group.controls['transactionTypeId'].value);
    let defaultCheckIsTrue = this.group.controls['defaultTermsAndCondition'].value;

    let transTypeId = this.group.controls['transactionTypeId'].value;

    //console.log("defaultCheckIsTrue"+defaultCheckIsTrue+transTypeId);
    let defaultCheckTC: DefaultCheckTC = {
      index: this.idx,
      transactionTypeId: transTypeId,
    }
    if (defaultCheckIsTrue === true ||defaultCheckIsTrue===1 ) {
      this.defaultCheckValue.emit(defaultCheckTC);
    }
    else {
      //console.log("dont emit")

    }
  }

}
