import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-report-format-settings',
  templateUrl: './report-format-settings.component.html',
  styleUrls: ['./report-format-settings.component.scss']
})
export class ReportFormatSettingsComponent implements OnInit {

  @Input() group: FormGroup;
  @Input() idx: number;

  @Output() deleteIndex: EventEmitter<number> = new EventEmitter<number>();
  @Output() addIndex: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  delete(index: number) {

    //console.log("Deleting...", index);
    this.deleteIndex.emit(index);
  }
  add(index: number) {

    //console.log("Adding...", index);
    this.addIndex.emit(index);
  }


}
