import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { TransactionSettingsComponent } from './transaction-settings/transaction-settings.component';
import { TaxSettingsComponent } from './tax-settings/tax-settings.component';
import { UomSettingsComponent } from './uom-settings/uom-settings.component';
import { TnCSettingsComponent } from './tn-csettings/tn-csettings.component';
import { RolesSettingsComponent } from './roles-settings/roles-settings.component';
import { ReportFormatSettingsComponent } from './report-format-settings/report-format-settings.component';
import { UserRoleComponent } from '../master/user-role/user-role.component';
import { AssignPermissionComponent } from '../master/assign-permission/assign-permission.component';
import { PreventUnsavedChangesGuardSettings } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-global-settings';
import { PreventUnsavedChangesGuarduserRole } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-menu';
import { SharedModule } from '../shared/shared.module';
import { ChatDialogComponent } from './chat-dialog/chat-dialog.component';
import { PrintTemplateComponent } from './print-template/print-template.component';
import { PrintCopyComponent } from './print-copies/print-copies.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';


@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
  ],
  declarations: [
    GlobalSettingsComponent,
    TransactionSettingsComponent,
    TaxSettingsComponent,
    UomSettingsComponent,
    TnCSettingsComponent,
    RolesSettingsComponent,
    ReportFormatSettingsComponent,
    UserRoleComponent,
    AssignPermissionComponent,
    PrintTemplateComponent,
    PrintCopyComponent,
    EmailSettingsComponent
   
    
  ],
  providers: [
    PreventUnsavedChangesGuardSettings,
    //PreventUnsavedChangesGuardPo,
    PreventUnsavedChangesGuarduserRole,
  ],
  entryComponents: [AssignPermissionComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class SettingsModule { }
