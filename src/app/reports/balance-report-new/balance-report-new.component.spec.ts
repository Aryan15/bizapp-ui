import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceReportNewComponent } from './balance-report-new.component';

describe('BalanceReportNewComponent', () => {
  let component: BalanceReportNewComponent;
  let fixture: ComponentFixture<BalanceReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
