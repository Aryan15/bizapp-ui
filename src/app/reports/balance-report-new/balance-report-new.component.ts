import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
 import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { BalanceReportModel } from '../../data-model/pr-model';
import { PayableReceivableService } from '../../services/payable-receivable.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FinancialYearService } from '../../services/financial-year.service';
import { PartyService } from '../../services/party.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { AppSettings } from '../../app.settings';
import { Party } from '../../data-model/party-model';

@Component({
  selector: 'app-balance-report-new',
  templateUrl: './balance-report-new.component.html',
  styleUrls: ['./balance-report-new.component.scss']
})
export class BalanceReportNewComponent extends ReportParentComponent implements  AfterViewInit, OnDestroy {
  displayedColumns = ['invoiceNumber','invoiceDate','statusName','partyName','invoiceAmount','paidAmount','dueAmount'];
  subheading: string;
  dataSource = new MatTableDataSource<BalanceReportModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  balanceReportDatabase: BalanceHttpDao | null;
  resultsLength = 0;
  isLoadingResults = true;
  invoiceTypeId: number;
  mainPartyType: string = "customer";

  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private payableReceivableService: PayableReceivableService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService

    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }

  
  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.balanceReportDatabase = new BalanceHttpDao(this.payableReceivableService);

    //If the user changes sort order, reset back to first page
 
   this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    this.getDateRange();
    this.getTransactionType();
    this.getUsers();

   // this.applyLanguage();

  }

  mainPartyTypeChange() {

    //console.log("this.mainPartyType: " + this.mainPartyType)
    //console.log("this.transactionType: " , this.transactionType)
    if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
      //console.log("1");
      this.invoiceTypeId = 5 //Purchase Invoice
    } else if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {
      //console.log("2");
      this.invoiceTypeId = 1; //Customer Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
      //console.log("3");
      this.invoiceTypeId = 18; //Subcontracting Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {
      //console.log("4");
      this.invoiceTypeId = 15; //Jobwork Invoice
    }
    if (this.mainPartyType === "jobworker") {
      this.isJW = true;
      this.getFilteredParties();
    } else {
      this.isJW = false;
      this.getFilteredParties();
       console.log("this.getFilteredParties(): "+this.getFilteredParties())

    }
    // console.log("this.invoiceTypeId: "+this.invoiceTypeId)

  }
  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
        .subscribe(response => {
         console.log('transaction type: ', response);
          this.transactionType = response;
          if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {

            this.isCustomer = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

            //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
          } else if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
            this.isSupplier = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
            //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

          }
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();
            this.mainPartyTypeChange();

          })  
      });
  }

  getData() {
 
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {

          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "invId";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.balanceReportDatabase!.getBalanceReport(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
           console.log('data.materialList: ', data.data);

          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }
 
  getTotalPaidAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.paidAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalBalanceAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.dueAmount).reduce((acc, value) => acc + value, 0);
  }
  getTotalInvoiceAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map  (item => Number(item.invoiceAmount)).reduce((prev, curr) => prev + curr, 0);

  }
  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

    
  close() {
    this._router_in.navigate(['/']);
  }

  clear() {

    this.filterForm.get('financialYearId').patchValue(null);
    this.filterForm.get('partyId').patchValue(null);
    this.filterForm.get('materialId').patchValue(null);
    this.filterForm.get('transactionFromDate').patchValue(null);
    this.filterForm.get('transactionToDate').patchValue(null);
    this.partyControl.patchValue(null);
    this.materialControl.patchValue(null);
    this.dataSource.data = [];
    // this.searchButton$.next('Search');
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };

    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    //console.log('excelPage: ', excelPage);
    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData);

      let reports: ExportDataType[] = [];
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })
      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });
  }

  makeSummaryRow(excelData): ExportDataType {

    return {
      "SL No": null,
      "Invoice Number": "TOTAL",
      "Invoice Date": null,
      "Status": null,
      "Party Name": null,
      "TotalAmount": excelData.map(item => item.invoiceAmount).reduce((prev, curr) => prev + curr, 0),
      "PaidAmount": excelData.map(item => item.paidAmount).reduce((prev, curr) => prev + curr, 0),
      "BalanceAmount": excelData.map(item => item.dueAmount).reduce((prev, curr) => prev + curr, 0),

    }
  }



  public saveAsPdf(): void {

    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.balanceReportHeader();
      let reportItem: any[] = this.balanceReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 1) {
        reportName = "Customer Balance Report"
      }
      if (this.transactionType.id === 5) {
        reportName = "Supplier Balance Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }


  balanceReportHeader() {
    return [
      {

        "SL No": "SL No",
        "Invoice Number": "Invoice Number",
        "Invoice Date": "Invoice Date",
        "Status": "Status",
        "Party Name": "Party Name",
        "TotalAmount": "Total Amount",
        "PaidAmount": "Paid Amount",
        "BalanceAmount": "Balance Amount",
 
      }
 
    ];
  
  }

  balanceReportBody(invoiceReport: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {
      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);
    return reportItem;
  }


  makeReportItem(item: BalanceReportModel, count: number): any {

    return {
      "SL No": count,
      "Invoice Number": item.invoiceNumber,
      "Invoice Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Status":item.statusName, 
      "Party Name": item.partyName,
      "TotalAmount": item.invoiceAmount,
      "PaidAmount": item.paidAmount,
      "BalanceAmount": item.dueAmount,
    }
  }


}

 

export class BalanceHttpDao {

  constructor(private payableReceivableService: PayableReceivableService) { }

  getBalanceReport(invoiceType: number, filterForm: FormGroup, page: Page): Observable<PagedData<BalanceReportModel>> {
 
    return this.payableReceivableService.getBalanceReport(invoiceType, filterForm.value, page);
  }
}


export interface ExportDataType {
  "SL No": number;
  "Invoice Number": string;
  "Invoice Date": string;
  "Status": string;
  "Party Name": string;
  "TotalAmount": number;
  "PaidAmount": number;
  "BalanceAmount": number;

}