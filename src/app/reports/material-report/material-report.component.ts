import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { SupplyType, Material } from '../../data-model/material-model';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';
import { PdfService } from '../../services/pdf.service';


@Component({
  selector: 'app-material-report',
  templateUrl: './material-report.component.html',
  styleUrls: ['./material-report.component.scss'],
})
export class MaterialReportComponent implements OnInit {

  subheading: string;
  rows = [];
  supplyType: SupplyType = { deleted: "N", id: 1, name: "Goods" };
  page = new Page();
  // filterForm = new FormGroup({
  //   fromDate: new FormControl(),
  //   toDate: new FormControl(),
  //   financialYearId: new FormControl(),
  //   partyId: new FormControl()
  // });
  isCustomer: boolean = false;
  isSupplier: boolean = false;
  reorderable: boolean = true;
  constructor(
    private datePipe: DatePipe,
    private _router: Router,
    public route: ActivatedRoute,
    private materialService: MaterialService,
    private excelService: ExcelService,
    public userService: UserService,
    private pdfService: PdfService) {

    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    // this.pageSize = this.page.size;
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }



  ngOnInit() {
    this.setPage({ offset: 0 });
  }

  setPage(pageInfo) {
    //console.log("in setPage: ",pageInfo);
    let filterText = "";
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "name";
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      ////console.log('this.rows: ', this.rows);
    });
  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;
    let filterText = "";
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }
  saveAsExcel(): void {
    let fileName: string = this.supplyType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let filterText = "";
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

   
      this.excelService.exportFromJSON(reports, fileName);

    });


  }


 


  saveAsPdf(): void {
    let pdfPage = { ...this.page };

    let reportObject: any;
    let filterText = "";
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.materialReportHeader();
      let reportItem: any[] = this.materialReportBody(pdfData);


      this.pdfService.exportToPdf("Material Report", reportHeader, reportItem, this.supplyType.name);
    });



  }


  materialReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Material": "Material",
        "PartNumber": "PartNumber",
        "HsnCode": "HsnCode",
        "MRP": "MRP",
        "BuyingPrice": "BuyingPrice",
        "SellingPrice": "SellingPrice",
        "CurrentStock": "CurrentStock",
        "UOM": "UOM"

      },



    ];




  }




  materialReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: any[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
  

    return reportItem;





  }


  makeReportItem(item: Material, count: number): any {
    return {
      "SL No": count,
      "Material": item.name,
      "PartNumber": item.partNumber,
      "HsnCode": item.hsnCode,
      "MRP": item.mrp,
      "BuyingPrice": item.buyingPrice,
      "SellingPrice": item.price,
      "CurrentStock": item.stock,
      "UOM": item.unitOfMeasurementName,

    }
  }



  close() {
    this._router.navigate(['/']);

  }
}

export interface ExportDataType {

  "SL No": number;
  "Material": string;
  "PartNumber": string;
  "HsnCode": string;
  "MRP": string;
  "BuyingPrice": string;
  "SellingPrice": string;
  "CurrentStock": string;
  "UOM": number;

}