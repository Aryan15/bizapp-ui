import { Component, OnInit, Inject, AfterViewInit, ViewChild } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { TallyExportTracker } from "../../data-model/misc-model";
import { InvoiceService } from "../../services/invoice.service";
import { TransactionReportRequest } from "../../data-model/transaction-report-request-model";
import { Page } from "../model/page";
import { Observable, merge, of as observableOf, } from "rxjs";
import { PagedData } from "../model/paged-data";
import { MatPaginator } from "@angular/material/paginator";
import { startWith, switchMap, map, catchError } from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import { TransactionType } from "../../data-model/transaction-type";
import { AppSettings } from "../../app.settings";

@Component({
    selector: 'tally-log-popup',
    templateUrl: './tally-log.html',
    styleUrls: ['./tally-log.scss']
  })
  export class TallyLogComponent implements OnInit, AfterViewInit {

    displayedColumns = ['fromDate','toDate','voucherCount','dateAndTime'];
    dataSource = new MatTableDataSource<TallyExportTracker>();
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    tallyLogDatabase: TallyLogHttpDao | null;
    isLoadingResults: boolean = false;
    transactionType: TransactionType;
    page = new Page();
    filterValue: TransactionReportRequest = {
      createdBy: null,
      financialYearId: null,
      materialId: null,
      partyId: null,
      partyTypeIds: null,
      transactionFromDate: null,
      transactionToDate: null
    };
    resultsLength = 0;
    constructor( private dialogRef: MatDialogRef<TallyLogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private invoiceService: InvoiceService){
        
    }

    ngOnInit(){}

    ngAfterViewInit(){
        this.tallyLogDatabase = new TallyLogHttpDao(this.invoiceService);
        this.getData();
    }

    getData() {
        merge(this.sort.sortChange,this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              this.isLoadingResults = true;
              this.page.sortColumn = this.sort.active ? this.sort.active : "createdDateTime";
              this.page.sortDirection = this.sort.direction ? this.sort.direction : "desc";
              this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
              this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
              return this.tallyLogDatabase!.getTrackers(this.filterValue, this.page);
            }),
            map(data => {
              //Flip flag to show that loading has finished
              this.isLoadingResults = false;
              this.resultsLength = data.page.totalElements;
              // this.page = data.page;
              //console.log('data.materialList: ', data.data);
              //console.log('data.page: ', this.page);
              return data.data;
            }),
            catchError((error) => {
              this.isLoadingResults = false;
              console.log('catchError ',error);
              return observableOf([]);
            })
          ).subscribe(data => {
            this.dataSource.data = data;
            console.log("resultsLength: "+this.resultsLength);
    
          });
      }

      onCloseClick(): void {       
        this.dialogRef.close();
      }

  }

  export class TallyLogHttpDao {

    constructor(private invoiceService: InvoiceService) { }
  
    getTrackers(filterForm: TransactionReportRequest, page: Page): Observable<PagedData<TallyExportTracker>> {
  
      // pageSize = pageSize ? pageSize : PAGE_SIZE;
      // //console.log('filterText: ', filterText);
      //console.log('page: ', page);
      //console.log('filterForm.value: ', filterForm.value);
  
      //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);
      return this.invoiceService.getTallyExportTracker(filterForm, page);
    }
  }