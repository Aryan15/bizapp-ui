import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { FinancialYear } from '../../data-model/financial-year-model';
import { InvoiceReportItemModel } from '../../data-model/invoice-model';
import { Material } from '../../data-model/material-model';
import { Party } from '../../data-model/party-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { TransactionType } from '../../data-model/transaction-type';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { InvoiceService } from '../../services/invoice.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TallyLogComponent } from './tally-log';
import { LanguageService } from '../../services/language.service';
import { TranslateService } from '@ngx-translate/core';
import { RoleModel } from '../../data-model/user-model';
import { PdfService } from '../../services/pdf.service';
declare var jsPDF: any;

// const PAGE_SIZE: number = AppSettings.REPORT_PAGE_SIZE;

@Component({
  selector: 'app-invoice-item-report-new',
  templateUrl: './invoice-item-report-new.component.html',
  styleUrls: ['./invoice-item-report-new.component.scss']
})
export class InvoiceItemReportNewComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {
  displayedColumns = [];
  subheading: string;
  // displayedColumns = ['invoiceNumber'
  // ,'invoiceDate'
  // ,'partyName'
  // ,'statusName'
  // ,'description'
  // ,'quantity'
  // ,'price'
  // ,'totalTaxableAmount'
  // ,'discountAmount'
  // ,'cgstTaxAmount'
  // ,'sgstTaxAmount'
  // ,'igstTaxAmount'
  // ,'grandTotal'
  // ];
  dataSource = new MatTableDataSource<InvoiceReportItemModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isItemLevelTax: boolean;

  invoiceDatabase: InvoiceHttpDao | null;
  resultsLength = 0;
  globalSetting: GlobalSetting;
  isLoadingResults = true;
  // filterForm = new FormGroup({
  //   transactionFromDate: new FormControl(),
  //   transactionToDate: new FormControl(),
  //   financialYearId: new FormControl(),
  //   partyId: new FormControl(),
  //   materialId: new FormControl(),
  //   partyTypeIds: new FormControl([]),  
  // });
  partyControl = new FormControl();
  materialControl = new FormControl();
  searchButton$ = new Subject<string>();
  onDestroy: Subject<boolean> = new Subject();
  isCustomer: boolean = false;
  isSupplier: boolean = false;
  partyPlaceHolder: string;
  enableTallyButton: boolean = false;
  page = new Page();
  transactionType: TransactionType;
  financialYears: FinancialYear[] = [];
  // public parties: Party[] = [];
  // public materials: Material[] = [];
  filteredParties: Observable<Party[]>;
  filteredMaterials: Observable<Material[]>;
  buyOrSell: string;
  xmlData;
  xmlDATAMASTER
  constructor(private invoiceService: InvoiceService
    , public route_in: ActivatedRoute
    , private datePipe: DatePipe
    , private financialYearService_in: FinancialYearService
    , private transactionTypeService_in: TransactionTypeService
    , protected partyService_in: PartyService
    , private materialService_in: MaterialService
    , private globalSettingService: GlobalSettingService
    , private _router_in: Router
    , private excelService: ExcelService
    , public userService: UserService
    , protected dialog: MatDialog,
    private languageService: LanguageService,
    private translate: TranslateService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    translate.setDefaultLang('en');
  }

  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.invoiceDatabase = new InvoiceHttpDao(this.invoiceService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    this.getDateRange();
    this.getTransactionType();
    this.getUsers();
    this.applyLanguage();
   //this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;

    // let roles : RoleModel[] = this.currentUser.roles;
    // // console.log("roles: "+roles);
    // let adminRole = roles.find(role => role.roleType.name === AppSettings.ADMIN_ROLE_TYPE);

    // // console.log("adminRole: "+adminRole.role+adminRole.roleType);
    // this.isAdmin = adminRole ? true : false;


  }

  applyLanguage() {
    this.languageService.getLanguage()
    .subscribe(resp => {
    if(resp===null){
      this.translate.use("en");
    }
    else{
      this.translate.use(resp);
    }
   
  })
  }
  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;
      if (this.buyOrSell === AppSettings.TXN_SELL) {
        this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      } else {
        this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
      }

      if (response.enableTally === 1) {

        this.enableTallyButton = true;
      }
      this.getDisplaycol();

    })
  }
  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
          .subscribe(response => {
            //console.log('transaction type: ', response);
            this.transactionType = response;
            if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {

              this.isCustomer = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

              //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
            } else if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
              this.isSupplier = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
              //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

            }
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();
            this.deriveBuyOrSell()
            this.getGlobalSetting();
          })
      });
  }

  deriveBuyOrSell() {
    let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_INVOICE];
    let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE];
    if (sellTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_SELL;
    } else if (buyTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_PURCHASE;
    }

  }

  getDisplaycol() {
    if (this.isItemLevelTax) {
      //console.log("in")
      this.displayedColumns = ['invoiceNumber'
        , 'invoiceDate'
        , 'partyName'
        , 'gstNumber'
        , 'statusName'
        , 'inclusiveTax'
        , 'description'
        , 'hsnCode'
        , 'internalReferenceNumber'
        , 'quantity'
        , 'price'
        , 'totalTaxableAmount'
        , 'discountAmount'
        , 'amountAfterDiscount'
        , 'taxableAmount'
        , 'cgstTaxAmount'
        , 'sgstTaxAmount'
        , 'igstTaxAmount'
        , 'grandTotal'
      ];
    }
    else {
      //console.log("else")
      this.displayedColumns = ['invoiceNumber'
        , 'invoiceDate'
        , 'partyName'
        , 'gstNumber'
        , 'statusName'
        , 'inclusiveTax'
        , 'description'
        , 'hsnCode'
        , 'internalReferenceNumber'
        , 'quantity'
        , 'price'
        , 'totalTaxableAmount'
        // ,'discountAmount'
        // ,'amountAfterDiscount'
        // ,'taxableAmount'
        // ,'cgstTaxRate'
        // ,'cgstTaxAmount'
        // ,'sgstTaxRate'
        // ,'sgstTaxAmount'
        // ,'igstTaxRate'
        // ,'igstTaxAmount'
        // ,'grandTotal'
      ];
    }
  }
  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "invId";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.invoiceDatabase!.getInvoices(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }
  // private getFinancialYears() {

  //       this.financialYearService.getFinancialYears()
  //         .subscribe(response => {
  //           this.financialYears = response;
  //         })
  //     }

  // getTotalWeight() {
  //   return this.dataSource.data.map(t => t.weight).reduce((acc, value) => acc + value, 0);
  // }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  getTotalTaxableAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.totalTaxableAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalDiscountAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.discountAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalCgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.cgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalSgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.sgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalIgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.igstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalGrandTotal() {
  
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.grandTotal).reduce((acc, value) => acc + value, 0);
  }

  getTotalAmountAfterDiscount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.amountAfterDiscount).reduce((acc, value) => acc + value, 0);
  }

  getSumTaxableAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.taxableAmount).reduce((acc, value) => acc + value, 0);
  }

  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  // getFilteredParties() {
  //   ////console.log('in getFilteredParties')
  //   let partyType: string = this.isCustomer ? AppSettings.PARTY_TYPE_CUSTOMER : AppSettings.PARTY_TYPE_SUPPLIER;
  //   this.filteredParties = this.partyControl.valueChanges
  //       .pipe(
  //       startWith(''),
  //       debounceTime(300),
  //       // tap(val => //console.log('party search: ', val)),
  //       //map(val => this.filterParty(val))
  //       switchMap(value => this.partyService.getPartiesByNameLike(partyType, value) )
  //       );
  // }

  // getFiltredMaterial() {
  //   this.filteredMaterials = this.materialControl.valueChanges
  //       .pipe(
  //       startWith(''),
  //       debounceTime(300),
  //       // map(value => typeof value === 'string' ? value : value.name),
  //       // map(val => this.filterMaterial(val))
  //       switchMap(value => this.materialService.getMaterialByPartNumberOrPartName(value) )
  //       );
  // }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  close() {
    this._router_in.navigate(['/']);
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    let excelPage = { ...this.page };
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.invoiceService.getInvoiceItemReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
         })

        

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);
     
      reports.push(summaryRow);
     
      this.excelService.exportFromJSON(reports, fileName);
    
    });

   
  }

  makeSummaryRow(excelData): ExportDataType {


    
    
    return {
      "SL No": null,
      "Invoice Number": "TOTAL",
      "Invoice Date": null,
      "Party Name": null,
      "Gst Number":null,
      "Status": null,
      "inclusiveTax": null,
      "Material": null,
      "Hsn Code":null,
      "DocumentRef Number":null,
      "Quantity": null,
      "Price": excelData.map(item => item.price).reduce((prev, curr) => prev + curr, 0),
      "Amount": excelData.map(item => item.taxableAmount).reduce((prev, curr) => prev + curr, 0),
      "Discount ": excelData.map(item => item.discountAmount).reduce((prev, curr) => prev + curr, 0),
      "AmountAfterDiscount": excelData.map(item => item.amountAfterDiscount).reduce((prev, curr) => prev + curr, 0),
      "TaxableAmount": excelData.map(item => item.totalTaxableAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal/Excludes Tcs": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0)
      
    }
   
  }


  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.invoiceService.getInvoiceItemReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.invoiceReportHeader();
      let reportItem: any[] = this.invoiceReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 1) {
        reportName = "Sales Detailed Report"
      }
      if (this.transactionType.id === 5) {
        reportName = "Purchase Detailed Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });



  }


  invoiceReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Invoice Number": "Invoice Number",
        "Invoice Date": "Invoice Date",
        "Party Name": "Party Name",
        "Gst Number":"Gst Number",
        "Status": "Status",
        "inclusiveTax": "inclusiveTax",
        "Material": "Material",
        "Hsn Code":"Hsn Code",
        "DocumentRef Number" :"DocumentRef Number",
        "Quantity": "Qty/Uom",
        "Price": "Price",
        "Amount": "Amount",
        "Discount ": "Discount ",
        "AmountAfterDiscount": "Amount After Discount",
        "TaxableAmount": "Taxable Amount",
        "CgstTaxAmount": "Cgst Tax Amount",
        "SgstTaxAmount": "Sgst Tax Amount",
        "IgstTaxAmount": "Igst Tax Amount",
        "GrandTotal/Excludes Tcs": "Grand Total/Excludes Tcs"
      },



    ];




  }




  invoiceReportBody(invoiceReport: any[]) {

    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {
      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);

    return reportItem;





  }


  makeReportItem(item: InvoiceReportItemModel, count: number): ExportDataType {
   if(item.partNumber!=null){
    item.description=item.description+"-"+item.partNumber;
    }
    else{
      item.description=item.description
    }
    return {
      "SL No": count,
      "Invoice Number": item.invoiceNumber,
      "Invoice Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Party Name": item.partyName,
      "Gst Number":item.gstNumber,
      "Status": item.statusName,
      "inclusiveTax": item.inclusiveTax ? 'Yes' : 'No',
      "Material": item.description,
      "Hsn Code":item.hsnCode,
      "DocumentRef Number":item.internalReferenceNumber,
      "Quantity": item.quantity + "" + item.uom,
      "Price": item.price,
      "Amount": item.taxableAmount,
      "Discount ": item.discountAmount,
      "AmountAfterDiscount": item.amountAfterDiscount,
      "TaxableAmount": item.totalTaxableAmount,
      "CgstTaxAmount": item.cgstTaxAmount,
      "SgstTaxAmount": item.sgstTaxAmount,
      "IgstTaxAmount": item.igstTaxAmount,
      "GrandTotal/Excludes Tcs": item.grandTotal,

    }
  }


















  clear() {

    this.filterForm.get('financialYearId').patchValue(null);
    this.filterForm.get('partyId').patchValue(null);
    this.filterForm.get('materialId').patchValue(null);
    this.filterForm.get('transactionFromDate').patchValue(null);
    this.filterForm.get('transactionToDate').patchValue(null);
    this.partyControl.patchValue(null);
    this.materialControl.patchValue(null);
    this.dataSource.data = [];
    // this.searchButton$.next('Search');
  }

 

 
  exportToTally() {

    // console.log("this.transactionType.id ", this.transactionType.id);
    // this.invoiceService.getTallyVoucherXML(this.transactionType.id.toString(), this.filterForm.value)
    //   .subscribe(response => {
    //     this.xmlData = response;
    //     this.download();
    //   })


    console.log("this.transactionType.id ", this.transactionType.id);
    let voucherLog: number
    let partyTypeId: number = this.isCustomer ? 1 : 2;
    this.invoiceService.getTallyMastersXML(partyTypeId)
      .subscribe(response => {
        this.xmlData = response;
        this.download("master");
        this.invoiceService.getTallyVoucherXML(this.transactionType.id.toString(), this.filterForm.value)
          .subscribe(response => {
            this.xmlData = response;
            this.download(this.transactionType.name);
            this.invoiceService.makeEntryInTallyLog(this.transactionType.id.toString(), this.filterForm.value)
              .subscribe(response => {


              })


          })

      })

  }




  download(type: string) {
    const xmlUrl = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.xmlData], { type: 'application/xml' }));
    const anchor = document.createElement('a');
    anchor.href = xmlUrl;
    anchor.setAttribute("download", type);
    anchor.click();
  }

  openLog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };

    // dialogConfig.height='500px';
    // dialogConfig.width='600px';


    this.dialog.open(TallyLogComponent, dialogConfig)
  }
}



export class InvoiceHttpDao {

  constructor(private invoiceService: InvoiceService) { }

  getInvoices(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<InvoiceReportItemModel>> {

    // pageSize = pageSize ? pageSize : PAGE_SIZE;
    // //console.log('filterText: ', filterText);
    //console.log('page: ', page);
    //console.log('filterForm.value: ', filterForm.value);

    //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);
    return this.invoiceService.getInvoiceItemReport(transactionType, filterForm.value, page);
  }
}


export interface ExportDataType {
  "SL No": number;
  "Invoice Number": string;
  "Invoice Date": string;
  "Party Name": string;
  "Gst Number":string;
  "Status": string;
  "inclusiveTax": string;
  "Material": string;
  "Hsn Code":string;
  "DocumentRef Number":string;
  "Quantity": string;
  "Price": number;
  "Amount": number;
  "Discount ": number;
  "AmountAfterDiscount": number;
  "TaxableAmount": number;
  "CgstTaxAmount": number;
  "SgstTaxAmount": number;
  "IgstTaxAmount": number;
  "GrandTotal/Excludes Tcs": number;
   
}