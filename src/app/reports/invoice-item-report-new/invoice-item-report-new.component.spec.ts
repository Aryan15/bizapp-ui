import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceItemReportNewComponent } from './invoice-item-report-new.component';

describe('InvoiceItemReportNewComponent', () => {
  let component: InvoiceItemReportNewComponent;
  let fixture: ComponentFixture<InvoiceItemReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceItemReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceItemReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
