import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceSummaryReportComponent } from './invoice-summary-report.component';

describe('InvoiceSummaryReportComponent', () => {
  let component: InvoiceSummaryReportComponent;
  let fixture: ComponentFixture<InvoiceSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
