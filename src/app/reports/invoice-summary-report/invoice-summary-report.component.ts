//import {HttpClient} from '@angular/common/http';
import { DataSource } from '@angular/cdk/table';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { PartyTypeService } from '../../services/financial-year.service';
import { AppSettings } from '../../app.settings';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { InvoiceService } from '../../services/invoice.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';

@Component({
  selector: 'app-invoice-summary-report',
  templateUrl: './invoice-summary-report.component.html',
  styleUrls: ['./invoice-summary-report.component.scss'],
})
export class InvoiceSummaryReportComponent extends ReportParentComponent implements OnInit {

  reorderable: true;
  globalSetting: GlobalSetting;
  isItemLevelTax: boolean;
  public dataSource: DataSource<any>;


  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService ,
    private datePipe: DatePipe,
    private _router_in: Router,
    private globalSettingService: GlobalSettingService,
    private invoiceService: InvoiceService,
    private route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private userService: UserService) {
      super(
        partyService_in,
        materialService_in,
        financialYearService_in,
        transactionTypeService_in,
        route_in,
        _router_in,
        userService
    );
   //this.getTransactionType();
  }


  ngOnInit() {

  this.initParent();
  this.getGlobalSetting();
  this.initComplete.subscribe(response =>{
    this.onSearch();
  });
  }
  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;
  
  this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
}) }
  saveAsExcel(): void {
    let fileName: string = 'test';
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();

    let excelPage = {...this.page};
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.invoiceService.getInvoiceReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: any[] = [];
      
          excelData.forEach(data => {
            reports.push({
              name: data.invoiceNumber, invoiceDate: this.datePipe.transform(data.invoiceDate, AppSettings.DATE_FORMAT), partyName: data.partyName, description: data.description,
              quantity: data.quantity, price: data.price, totalTaxableAmount: data.totalTaxableAmount, discountAmount: data.discountAmount, cgstTaxAmount: data.cgstTaxAmount, cgstTaxRate: data.cgstTaxRate, sgstTaxAmount: data.sgstTaxAmount, sgstTaxRate: data.sgstTaxRate
              , igstTaxRate: data.igstTaxRate, igstTaxAmount: data.igstTaxAmount, grandTotal: data.grandTotal
            })
          })
      
          this.excelService.exportFromJSON(reports, fileName);
    });

  
  }

  // summayCalculation(cells: string[]){
  //   //console.log("summayCalculation",cells);
  //   if(this.rows)
  //   {
  //   this.rows.forEach(item=>{
  //     //console.log("item",item)
     // if(item.statusName === AppSettings.INVOICE_CANCEL_STATUS){

  //     }
  //     else
  //     {
  //       super.priceSummary(cells)
  //     }
  //   }
  //   )
  // }
  // }

  summayCalculation(rows: any[]){
    
    console.log("--------------------------------")
    //let data: string[]=rows.filter(row => row.statusName =console.log(row.statusName));
   
    let cells: string[] = rows.filter(row => row.statusName != AppSettings.INVOICE_CANCEL_STATUS)
                              .map(row => row.grandTotal);
                       
    console.log('cells: ',cells.values);
    // return super.priceSummary(cells);
    let summaryGrandTotal: number = 0;
    cells.forEach(c => {
      // //console.log("C ",c);
      summaryGrandTotal += c ? +c : 0;
    });

    // let decimalPipe = new DecimalPipe('en-IN');
    
    // let ret = decimalPipe.transform(summaryGrandTotal ? summaryGrandTotal.toFixed(2) : 0,'1.2-2');
    // //console.log('ret :', ret);
    // return summaryGrandTotal; 
  }

  setPage(pageInfo) {
    //console.log("in setPage: ", pageInfo , this.filterForm.value);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";

  //  this.page.size = AppSettings.REPORT_PAGE_SIZE;

    this.invoiceService.getInvoiceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      //this.page.size += 1;
      //console.log("this.rows ", this.rows)

      // this.getTotal();
    });
  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.invoiceService.getInvoiceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }


  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }

 

  onSearch() {

    this.setPage({ offset: 0 });
  }


}
