import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoReportNewComponent } from './po-report-new.component';

describe('PoReportNewComponent', () => {
  let component: PoReportNewComponent;
  let fixture: ComponentFixture<PoReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
