import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
 import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
 import { PurchaseOrderReport } from '../../data-model/purchase-order-model';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PdfService } from '../../services/pdf.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { Material } from '../../data-model/material-model';
import { Party } from '../../data-model/party-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';

@Component({
  selector: 'app-po-report-new',
  templateUrl: './po-report-new.component.html',
  styleUrls: ['./po-report-new.component.scss']
})
export class PoReportNewComponent  extends ReportParentComponent implements AfterViewInit, OnDestroy {

  displayedColumns = [];
  subheading: string;
  dataSource = new MatTableDataSource<PurchaseOrderReport>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isItemLevelTax: boolean;
  globalSetting: GlobalSetting;
  poDetailDatabase: PODetailHttpDao | null;
  resultsLength = 0;
  isLoadingResults = true;
  buyOrSell: string;

  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private poService: PurchaseOrderService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private numberFormatterService: NumberFormatterService,
    private globalSettingService: GlobalSettingService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
   
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
   }


   
  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.poDetailDatabase = new PODetailHttpDao(this.poService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    this.getDateRange();
    this.getTransactionType();
    this.getUsers();
  
  }


  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
          .subscribe(response => {
            //console.log('transaction type: ', response);
            this.transactionType = response;
            if (this.transactionType.name === AppSettings.CUSTOMER_PO) {

              this.isCustomer = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

              //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
            } else if (this.transactionType.name === AppSettings.SUPPLIER_PO) {
              this.isSupplier = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
              //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

            }
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();
            this.getGlobalSetting();

          })
      });
  }
  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;
      if (this.buyOrSell === AppSettings.TXN_SELL) {
        this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      } else {
        this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
      }

     
      this.getDisplaycol();

    })
  }
 
  getDisplaycol() {
    if (this.isItemLevelTax) {
      //console.log("in")
      this.displayedColumns = ['purchaseOrderNumber'
        , 'purchaseOrderDate'
        , 'statusName'
        , 'partyName'
        , 'material'
        , 'internalReferenceNumber'
        , 'quantity'
        , 'price'
        , 'amount'
        , 'discountAmount'
        , 'cgstTaxPercentage'
        , 'cgstTaxAmount'
        , 'sgstTaxPercentage'
        , 'sgstTaxAmount'
        , 'igstTaxPercentage'
        , 'igstTaxAmount'
        , 'grandTotal'
        , 'remarks'
      ];
    }
    else {
      //console.log("else")
      this.displayedColumns = ['purchaseOrderNumber'
      , 'purchaseOrderDate'
      , 'statusName'
      , 'partyName'
      , 'material'
      , 'internalReferenceNumber'
      , 'quantity'
      , 'price'
      , 'amount'
      , 'grandTotal'
        // ,'discountAmount'
        // ,'amountAfterDiscount'
        // ,'taxableAmount'
        // ,'cgstTaxRate'
        // ,'cgstTaxAmount'
        // ,'sgstTaxRate'
        // ,'sgstTaxAmount'
        // ,'igstTaxRate'
        // ,'igstTaxAmount'
        // ,'grandTotal'
      ];
    }
  }
  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "purchaseOrderDate";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.poDetailDatabase!.getReport(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }

  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }

  setPage(pageInfo) {
    //console.log("in setPage: ",pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "purchaseOrderDate";
    this.poService.getPoReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  
  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }




  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  close() {
    this._router_in.navigate(['/']);
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  
  getTotalPrice() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.price).reduce((acc, value) => acc + value, 0);
  } 

  getTotalAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.amount).reduce((acc, value) => acc + value, 0);
  }

  getTotalDiscountAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.discountAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalCgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.cgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalSgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.sgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalIgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.igstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalGrandTotal() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.totalAmount).reduce((acc, value) => acc + value, 0);
  }

 
  

}



export class PODetailHttpDao {

  constructor(private poService: PurchaseOrderService) { }

  getReport(poTypeId: number, filterForm: FormGroup, page: Page): Observable<PagedData<PurchaseOrderReport>> {
 
    return this.poService.getPoReport(poTypeId, filterForm.value, page);
  }
}



export interface ExportDataType {
  "SL No": number;
  "PurchaseOrder Number": string;
  "PurchaseOrder Date": string;
  "Status": string;
  "PartyName": string;
  "Material": string;
  "Quantity": number;
  "UOM": string;
  "Price": number;
  "Amount": number;
  "Discount": number;
  "CgstTaxPercentage": number;
  "CgstTaxAmount": number;
  "SgstTaxPercentage": number;
  "SgstTaxAmount": number;
  "IgstTaxPercentage": number;
  "IgstTaxAmount": number;
  "TotalAmount": number;

}

