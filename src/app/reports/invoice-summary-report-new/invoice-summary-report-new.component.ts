import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { InvoiceService } from '../../services/invoice.service';
import { FormGroup } from '@angular/forms';
import { Page } from '../model/page';
import { Observable, Subject, of as observableOf, merge } from 'rxjs';
import { PagedData } from '../model/paged-data';
import { InvoiceReportModel } from '../../data-model/invoice-model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, startWith, switchMap, map, catchError, filter } from 'rxjs/operators';
import { TransactionType } from '../../data-model/transaction-type';
import { FinancialYear } from '../../data-model/financial-year-model';
import { AppSettings } from '../../app.settings';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PartyService } from '../../services/party.service';
import { MaterialService } from '../../services/material.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { Party } from '../../data-model/party-model';
import { Material } from '../../data-model/material-model';
import { DatePipe } from '@angular/common';
import { ExcelService } from '../../services/excel.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
declare var jsPDF: any;

@Component({
  selector: 'app-invoice-summary-report-new',
  templateUrl: './invoice-summary-report-new.component.html',
  styleUrls: ['./invoice-summary-report-new.component.scss']
})
export class InvoiceSummaryReportNewComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {

  subheading: string;
  searchButton$ = new Subject<string>();
  onDestroy: Subject<boolean> = new Subject();
  displayedColumns = [];

  dataSource = new MatTableDataSource<InvoiceReportModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  invoiceDatabase: InvoiceReportHttpDao | null;
  transactionType: TransactionType;
  financialYears: FinancialYear[] = [];
  isCustomer: boolean = false;
  isSupplier: boolean = false;
  partyPlaceHolder: string;
  isLoadingResults = true;
  page = new Page();
  resultsLength = 0;
  count = -1;
  isItemLevelTax: boolean;
  globalSetting: GlobalSetting;
  isInvoice: boolean;
  buyOrSell: string;
  constructor(private invoiceService: InvoiceService
    , private transactionTypeService_in: TransactionTypeService
    , private financialYearService_in: FinancialYearService
    , public route_in: ActivatedRoute
    , private globalSettingService: GlobalSettingService
    , protected partyService_in: PartyService
    , private materialService_in: MaterialService
    , private _router_in: Router
    , private datePipe: DatePipe
    , private excelService: ExcelService
    , private userService: UserService
    , private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;

    this.getUsers();

  }

  // ngOnInit() {

  //   // this.initParent();
  //   // this.getGlobalSetting();
  //   // this.initComplete.subscribe(response =>{
  //   //   this.onSearch();
  //   // });
  //   }

  ngAfterViewInit() {
    this.initParent();
    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


    this.invoiceDatabase = new InvoiceReportHttpDao(this.invoiceService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    // this.getDateRange();
    // this.getTransactionType();
    this.initComplete.subscribe(response => {
      if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE || this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
        this.isInvoice = true;
      } else {
        this.isInvoice = false;
      }
      //console.log("this.isInvoice: "+this.isInvoice);
      this.deriveBuyOrSell();
      this.getGlobalSetting();
      this.getData();
      this.getDisplaycol();
    });

  }
  getDisplaycol() {
    if (this.isItemLevelTax) {
      //console.log("in")
      this.displayedColumns = ['invoiceNumber'
        , 'invoiceDate'
        , 'sourceInvoiceNumber'
        , 'sourceInvoiceDate'
        , 'statusName'
        , 'partyName'
        ,  'gstNumber'
        , 'inclusiveTax'
        , 'amount'
        , 'discountAmount'
        , 'totalTaxableAmount'
        , 'cgstTaxAmount'
        , 'sgstTaxAmount'
        , 'igstTaxAmount'
        ,'tcsPercentage'
        ,'tcsAmount'
        , 'grandTotal'
      ];
    }
    else {
      //console.log("else")
      this.displayedColumns = ['invoiceNumber'
        , 'invoiceDate'
        , 'sourceInvoiceNumber'
        , 'sourceInvoiceDate'
        , 'statusName'
        , 'partyName'
        ,  'gstNumber'
        , 'inclusiveTax'
        , 'amount'
        , 'discountAmount'
        , 'totalTaxableAmount'
        , 'cgstTaxRate'
        , 'cgstTaxAmount'
        , 'sgstTaxRate'
        , 'sgstTaxAmount'
        , 'igstTaxRate'
        , 'igstTaxAmount'
        ,'tcsPercentage'
        ,'tcsAmount'

        , 'grandTotal'
      ];
    }

    if (this.isInvoice) {
      this.displayedColumns = this.displayedColumns.filter(col => col != 'sourceInvoiceNumber')
        .filter(col => col != 'sourceInvoiceDate')
    }
  }

  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;

      // this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      if (this.buyOrSell === AppSettings.TXN_SELL) {
        this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      } else {
        this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
      }
    })
  }
  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "invId";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.invoiceDatabase!.getInvoices(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ', error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: " + this.resultsLength, this.dataSource.data);

      });
  }

  onSearch() {
    this.searchButton$.next('Search')
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  close() {
    this._router_in.navigate(['/']);
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();

    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;

    this.invoiceService.getInvoiceReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reportItemCreditNote: ExportDataTypeCreditNote[] = [];
      let reportItemDebitNote: ExportDataTypeDebitNote[] = [];
      let reports: ExportDataType[] = [];

      if (this.transactionType.id === 9) {
        excelData.forEach((data, index) => {
          reportItemCreditNote.push(
            this.makeReportItemCreditNote(data, index + 1)
          )
        })
        let summaryRowCredit: ExportDataTypeCreditNote = this.makeSummaryRowCredit(excelData);

        reportItemCreditNote.push(summaryRowCredit);
        this.excelService.exportFromJSON(reportItemCreditNote, fileName);
      }
      if (this.transactionType.id === 1 || this.transactionType.id === 5) {
        excelData.forEach((data, index) => {
          reports.push(
            this.makeReportItem(data, index + 1)
          )
        })
        let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

        reports.push(summaryRow);
        this.excelService.exportFromJSON(reports, fileName);
      }

      if (this.transactionType.id === 10) {
        excelData.forEach((data, index) => {
          reportItemDebitNote.push(
            this.makeReportItemDebitNote(data, index + 1)
          )
        })
        let summaryRowDebit: ExportDataTypeDebitNote = this.makeSummaryRowDebit(excelData);

        reportItemDebitNote.push(summaryRowDebit);
        this.excelService.exportFromJSON(reportItemDebitNote, fileName);
      }




    });




  }

  makeSummaryRow(excelData): ExportDataType {
    return {
      "SL No": null,
      "Invoice No": "TOTAL",
      "Invoice Date": null,
      "Status": null,
      "Party Name": null,
      "Gst Number":null,
      "Incl. Tax": null,
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "Discount": excelData.map(item => item.discountAmount).reduce((prev, curr) => prev + curr, 0),
      "TaxableAmount": excelData.map(item => item.totalTaxableAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "TcsPercentage":excelData.map(item => item.tcsPercentage).reduce((prev, curr) => prev + curr, 0),
      "TcsAmount":excelData.map(item => item.tcsAmount).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0),

    }
  }


  makeSummaryRowCredit(excelData): ExportDataTypeCreditNote {
    return {
      "SL No": null,
      "Credit No": "TOTAL",
      "Credit Date": null,
      "Invoice No.": null,
      "Invoice Date": null,
      "Status": null,
      "Party Name": null,
      "Gst Number":null,
      "Incl. Tax": null,
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "Discount": excelData.map(item => item.discountAmount).reduce((prev, curr) => prev + curr, 0),
      "TaxableAmount": excelData.map(item => item.totalTaxableAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "TcsPercentage":excelData.map(item => item.tcsPercentage).reduce((prev, curr) => prev + curr, 0),
      "TcsAmount":excelData.map(item => item.tcsAmount).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0),

    }
  }



  makeSummaryRowDebit(excelData): ExportDataTypeDebitNote {
    return {
      "SL No": null,
      "Debit No": "TOTAL",
      "Debit Date": null,
      "Invoice No.": null,
      "Invoice Date": null,
      "Status": null,
      "Party Name": null,
      "Gst Number":null,
       "Incl. Tax": null,
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "Discount": excelData.map(item => item.discountAmount).reduce((prev, curr) => prev + curr, 0),
      "TaxableAmount": excelData.map(item => item.totalTaxableAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "TcsPercentage":excelData.map(item => item.tcsPercentage).reduce((prev, curr) => prev + curr, 0),
      "TcsAmount":excelData.map(item => item.tcsAmount).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0),

    }
  }



  public SavePDF(): void {

    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.invoiceService.getInvoiceReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.invoiceReportHeader();
      let reportItem: any[] = this.invoiceReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 1 ) {
        reportName = "Sales Summary Report";
      }

      if(this.transactionType.id === 5){
        reportName = "Purchase Summary Report";
      }

      if (this.transactionType.id === 9) {
        reportName = "Credit Note Report";
      }
      if (this.transactionType.id === 10) {
        reportName = "Debit Note Report";
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }





  // reportBody(invoiceReport: any[]) {
  //  var ReportItem: any[] = [];
  //   invoiceReport.forEach(invoice => {

  //     ReportItem.push(this.makeReportItem(invoice));
  //   });

  //   return ReportItem;





  // }





  invoiceReportHeader() {


    if (this.transactionType.id === 1 || this.transactionType.id === 5) {
      return [
        {

          "SL No": "SL No",
          "Invoice No": "Invoice No",
          "Invoice Date": "Invoice Date",
          "Status": "Status",
          "Party Name": "Party Name",
          "Gst Number":"Gst Number",
          "Incl. Tax": "Incl. Tax",

          "Amount": "Amount",
          "Discount ": "Discount ",
          "TaxableAmount": "Taxable Amount",
          "CgstTaxAmount": "CGST",
          "SgstTaxAmount": "SGST",
          "IgstTaxAmount": "IGST",
          "TcsPercentage":"Tcs Percentage",
          "TcsAmount":"Tcs Amount",
          "GrandTotal": "Grand Total"


        },



      ];

    }


    if (this.transactionType.id === 9) {
      return [
        {
          "SL No": "SL No",
          "Credit No": "Credit No",
          "Credit Date": "Credit Date",
          "Invoice No.": "Invoice No.",
          "Invoice Date": "Invoice Date",

          "Status": "Status",
          "Party Name": "Party Name",
          "Gst Number":"Gst Number",
          "Incl. Tax": "Incl. Tax",

          "Amount": "Amount",
          "Discount ": "Discount ",
          "TaxableAmount": "Taxable Amount",
          "CgstTaxAmount": "CGST",
          "SgstTaxAmount": "SGST",
          "IgstTaxAmount": "IGST",
          "TcsPercentage":"Tcs Percentage",
          "TcsAmount":"Tcs Amount",
          "GrandTotal": "Grand Total"


        },



      ];

    }

    if (this.transactionType.id === 10) {
      return [
        {
          "SL No": "SL No",
          "Debit No": "Debit No",
          "Debit Date": "Debit Date",
          "Invoice No.": "Invoice No.",
          "Invoice Date": "Invoice Date",

          "Status": "Status",
          "Party Name": "Party Name",
          "Gst Number":"Gst Number",
          
          "Incl. Tax": "Incl. Tax",

          "Amount": "Amount",
          "Discount ": "Discount ",
          "TaxableAmount": "Taxable Amount",
          "CgstTaxAmount": "CGST",
          "SgstTaxAmount": "SGST",
          "IgstTaxAmount": "IGST",
          "TcsPercentage":"Tcs Percentage",
          "TcsAmount":"Tcs Amount",
          "GrandTotal": "Grand Total"

        },



      ];

    }


  }


  invoiceReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: ExportDataType[] = [];
    let reportItemCreditNote: ExportDataTypeCreditNote[] = [];
    let reportItemDebitNote: ExportDataTypeDebitNote[] = [];
    if (this.transactionType.id === 1 || this.transactionType.id === 5) {
      invoiceReport.forEach((invoice, index) => {
        reportItem.push(this.makeReportItem(invoice, index + 1));
        
      });
      let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
      reportItem.push(summaryRow);
      return reportItem;
    }

    if (this.transactionType.id === 9) {

      invoiceReport.forEach((invoice, index) => {
        reportItemCreditNote.push(this.makeReportItemCreditNote(invoice, index + 1));
      });
      let summaryRowCredit: ExportDataTypeCreditNote = this.makeSummaryRowCredit(invoiceReport);
      reportItemCreditNote.push(summaryRowCredit);
      return reportItemCreditNote;
    }

    if (this.transactionType.id === 10) {
      invoiceReport.forEach((invoice, index) => {
        reportItemDebitNote.push(this.makeReportItemDebitNote(invoice, index + 1));
      });
      let summaryRowDebit: ExportDataTypeDebitNote = this.makeSummaryRowDebit(invoiceReport);
      reportItemDebitNote.push(summaryRowDebit);
      return reportItemDebitNote;
    }








  }

  makeReportItem(item: InvoiceReportModel, count: number): ExportDataType {

    return {

      "SL No": count,
      "Invoice No": item.invoiceNumber,
      "Invoice Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Status": item.statusName,
      "Party Name": item.partyName,
      "Gst Number":item.gstNumber,
      "Incl. Tax": item.inclusiveTax ? 'Yes' : 'No',
      "Amount": item.amount,
      "Discount": item.discountAmount,
      "TaxableAmount": item.totalTaxableAmount,
      "CgstTaxAmount": item.cgstTaxAmount,
      "SgstTaxAmount": item.sgstTaxAmount,
      "IgstTaxAmount": item.igstTaxAmount,
      "TcsPercentage":item.tcsPercentage,
      "TcsAmount":item.tcsAmount,
      "GrandTotal": item.grandTotal,
      // "slNo": quo.,
    }


  }



  makeReportItemCreditNote(item: InvoiceReportModel, count: number): ExportDataTypeCreditNote {
    console.log("entering here")
    return {
      "SL No": count,
      "Credit No": item.invoiceNumber,
      "Credit Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Invoice No.": item.sourceInvoiceNumber,
      "Invoice Date": this.datePipe.transform(item.sourceInvoiceDate, 'dd-MM-yyyy'),
      "Status": item.statusName,
      "Party Name": item.partyName,
      "Gst Number":item.gstNumber,
      "Incl. Tax": item.inclusiveTax ? 'Yes' : 'No',
      "Amount": item.amount,
      "Discount": item.discountAmount,
      "TaxableAmount": item.totalTaxableAmount,
      "CgstTaxAmount": item.cgstTaxAmount,
      "SgstTaxAmount": item.sgstTaxAmount,
      "IgstTaxAmount": item.igstTaxAmount,
      "TcsPercentage":item.tcsPercentage,
      "TcsAmount":item.tcsAmount,
      "GrandTotal": item.grandTotal,
      // "slNo": quo.,
    }


  }


  makeReportItemDebitNote(item: InvoiceReportModel, count: number): ExportDataTypeDebitNote {
    return {
      "SL No": count,
      "Debit No": item.invoiceNumber,
      "Debit Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Invoice No.": item.sourceInvoiceNumber,
      "Invoice Date": this.datePipe.transform(item.sourceInvoiceDate, 'dd-MM-yyyy'),
      "Status": item.statusName,
      "Party Name": item.partyName,
      "Gst Number":item.gstNumber,
      "Incl. Tax": item.inclusiveTax ? 'Yes' : 'No',
      "Amount": item.amount,
      "Discount": item.discountAmount,
      "TaxableAmount": item.totalTaxableAmount,
      "CgstTaxAmount": item.cgstTaxAmount,
      "SgstTaxAmount": item.sgstTaxAmount,
      "IgstTaxAmount": item.igstTaxAmount,
      "TcsPercentage":item.tcsPercentage,
      "TcsAmount":item.tcsAmount,
      "GrandTotal": item.grandTotal,
     
      // "slNo": quo.,
    }


  }


  clear() {

    // this.filterForm.get('financialYearId').patchValue(null);
    // this.filterForm.get('partyId').patchValue(null);
    // this.filterForm.get('materialId').patchValue(null);
    // this.filterForm.get('transactionFromDate').patchValue(null);
    // this.filterForm.get('transactionToDate').patchValue(null);
    // // this.searchButton$.next('Search');
    // this.materialControl.patchValue(null);
    // this.partyControl.patchValue(null);
    super.clear();
    this.dataSource.data = [];

  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  getTotalAmount() {
   
return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.amount).reduce((acc, value) => acc + value, 0);
  }
  getTotalTaxableAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.totalTaxableAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalDiscountAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.discountAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalCgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.cgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalSgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.sgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalIgstTaxAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.igstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalGrandTotal() {
   
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.grandTotal).reduce((acc, value) => acc + value, 0);
    
    
  }

  getTotalTcsAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.tcsAmount).reduce((acc, value) => acc + value, 0);
  }

  deriveBuyOrSell() {
    let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_INVOICE];
    let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE];
    if (sellTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_SELL;
    } else if (buyTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_PURCHASE;
    }

  }

}


export class InvoiceReportHttpDao {

  constructor(private invoiceService: InvoiceService) { }

  getInvoices(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<InvoiceReportModel>> {

    // pageSize = pageSize ? pageSize : PAGE_SIZE;
    // //console.log('filterText: ', filterText);
    //console.log('page: ', page);

    //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);

    return this.invoiceService.getInvoiceReport(transactionType, filterForm.value, page);
  }
}

export interface ExportDataType {
  "SL No": number;
  "Invoice No": string;
  "Invoice Date": string;
  "Status": string;
  "Party Name": string;
  "Gst Number":string;
  "Incl. Tax": string;
  "Amount": number;
  "Discount": number;
  "TaxableAmount": number;
  "CgstTaxAmount": number;
  "SgstTaxAmount": number;
  "IgstTaxAmount": number;
  "TcsPercentage":number;
   "TcsAmount":number;
  "GrandTotal": number;
}

export interface ExportDataTypeCreditNote {
  "SL No": number;
  "Credit No": string;
  "Credit Date": string;
  "Invoice No.": string;
  "Invoice Date": string;
  "Status": string;
  "Party Name": string;
  "Gst Number":string;
  "Incl. Tax": string;
  "Amount": number;
  "Discount": number;
  "TaxableAmount": number;
  "CgstTaxAmount": number;
  "SgstTaxAmount": number;
  "IgstTaxAmount": number;
  "TcsPercentage":number;
  "TcsAmount":number;
  "GrandTotal": number;
}

export interface ExportDataTypeDebitNote {
  "SL No": number;
  "Debit No": string;
  "Debit Date": string;
  "Invoice No.": string;
  "Invoice Date": string;
  "Status": string;
  "Party Name": string;
  "Gst Number":string;
  "Incl. Tax": string;
  "Amount": number;
  "Discount": number;
  "TaxableAmount": number;
  "CgstTaxAmount": number;
  "SgstTaxAmount": number;
  "IgstTaxAmount": number;
  "TcsPercentage":number;
   "TcsAmount":number;
  "GrandTotal": number;
}