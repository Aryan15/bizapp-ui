import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceSummaryReportNewComponent } from './invoice-summary-report-new.component';

describe('InvoiceSummaryReportNewComponent', () => {
  let component: InvoiceSummaryReportNewComponent;
  let fixture: ComponentFixture<InvoiceSummaryReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceSummaryReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSummaryReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
