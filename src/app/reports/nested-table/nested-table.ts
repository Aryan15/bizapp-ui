import { OnInit, Component } from "@angular/core";
import { ExcelService } from "../../services/excel.service";


export interface InvoicePay {    
    invoiceNumber: string;
    invoiceDate: string;
    partyName: string;
    totalAmount: number;
    payDetails: PayDetails[]
  }

  export interface PayDetails{
      slNo: number;
      paidDate: string;
      payNumber: string;
      status: string;
      amountPaid: number;
      balanceAmount: number;      
  }
  const ELEMENT_DATA: InvoicePay[] = [
    {
        invoiceNumber: "CIN/19-20/1/test",
        invoiceDate: "01/05/2019",
        partyName: "Test Customer",
        totalAmount: 1770.00,
        payDetails: [
            {
                slNo: 1,
                paidDate: "30/04/2019",
                payNumber: "CUR4",
                status: "Completed",
                amountPaid: 118.00,
                balanceAmount: 1652.00
            },
            {
                slNo: 2,
                paidDate: "09/05/2019",
                payNumber: "CUR5",
                status: "Completed",
                amountPaid: 1000.00,
                balanceAmount: 652.00
            },
        ]
    },
    {
        invoiceNumber: "CIN/18-19/3/test",
        invoiceDate: "12/03/2019",
        partyName: "Test Customer",
        totalAmount: 8850.00,
        payDetails: [
            {
                slNo: 1,
                paidDate: "01/05/2019",
                payNumber: "CUR1",
                status: "Completed",
                amountPaid: 8850.00,
                balanceAmount: 0.00
            }
        ]
    }
  ];
@Component({
    selector: 'nested-table',
    templateUrl: './nested-table.html',
    styleUrls: ['./nested-table.scss'],
  })
export class NestedTable implements OnInit {

    displayedColumns: string[] = [ 'invoiceNumber', 'invoiceDate', 'partyName',  'payDetails','totalAmount'];
    // displayedColumns: string[] = [ 'invoiceNumber',  'totalAmount', 'payDetails'];
    // displayedDetailsColumns: string[] = [ 'slNo', 'amount'];
    dataSource = ELEMENT_DATA;
    
    constructor(private excelService: ExcelService,){

    }
    ngOnInit(){

        
    }

    saveAsExcel(): void {
        let fileName: string = 'test';
        // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
        //this.excelService.export();
        let filterText= "";
        
        let excelData: any[] = [];
        
          excelData = this.dataSource;
          //console.log("this.rows ", excelData)
        
    
        let reports: any[] = [];
    
        excelData.forEach(data => {
        let payDetails: any[] = [];
        let payDetail = {
            "SL No": data.payDetails[0].slNo,
            "Amount": data.payDetails[0].amountPaid
        }

        payDetails.push(payDetail);
          reports.push({
            "Invoice Number": data.invoiceNumber,
            "Payment Details": payDetails,            
          })
        });

        //console.log("reports: ", reports);
    
        this.excelService.exportFromJSON(reports, fileName);
        
      }

}


/*
{name: 'Invoice', 
    grandTotal: 1000.00,
    details: [
        {
            slNo : 1,
            amount: 500.00
        },
        {
            slNo : 2,
            amount: 500.00
        }
    ]
    },
    {name: 'Invoice 2', 
    grandTotal: 3000.00,
    details: [
        {
            slNo : 1,
            amount: 1500.00
        },
        {
            slNo : 2,
            amount: 1500.00
        }
    ]
    },
    {name: 'Invoice 3', 
    grandTotal: 5000.00,
    details: [
        {
            slNo : 1,
            amount: 2000.00
        },
        {
            slNo : 2,
            amount: 3000.00
        },
        {
            slNo : 3,
            amount: 500.00
        }
    ]
    },
    {name: 'Invoice 4', 
    grandTotal: 1000.00,
    details: [
        {
            slNo : 1,
            amount: 500.00
        }
    ]
    },
*/