import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayableReceivableReportNewComponent } from './payable-receivable-report-new.component';

describe('PayableReceivableReportNewComponent', () => {
  let component: PayableReceivableReportNewComponent;
  let fixture: ComponentFixture<PayableReceivableReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayableReceivableReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayableReceivableReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
