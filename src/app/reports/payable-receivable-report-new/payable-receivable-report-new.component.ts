import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Party } from '../../data-model/party-model';
import { PayableReceivableReportModel } from '../../data-model/pr-model';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PdfService } from '../../services/pdf.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PayableReceivableService } from '../../services/payable-receivable.service';

@Component({
  selector: 'app-payable-receivable-report-new',
  templateUrl: './payable-receivable-report-new.component.html',
  styleUrls: ['./payable-receivable-report-new.component.scss']
})
export class PayableReceivableReportNewComponent  extends ReportParentComponent implements  AfterViewInit, OnDestroy {
  
  displayedColumns = ['payReferenceDate','payReferenceNumber','statusName','invoiceNumber','invoiceDate','partyName','invoiceAmount','dueAmount','tdsPercentage','tdsAmount','amountAfterTds','paidAmount','paymentMethodName','cheque'];
  subheading: string;
  dataSource = new MatTableDataSource<PayableReceivableReportModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  payableRecievableReportDatabase: PayableReceivableHttpDao | null;
  resultsLength = 0;
  isLoadingResults = true;
  invoiceTypeId: number;
  mainPartyType: string = "customer";

  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private payableReceivableService: PayableReceivableService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService

    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }

  
  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.payableRecievableReportDatabase = new PayableReceivableHttpDao(this.payableReceivableService);

    //If the user changes sort order, reset back to first page
 
   this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

     this.getDateRange();
    this.getTransactionType();
    this.getUsers();

 
  }

  mainPartyTypeChange() {

    //console.log("this.mainPartyType: " + this.mainPartyType)
    //console.log("this.transactionType: " , this.transactionType)
    if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {
      //console.log("1");
      this.invoiceTypeId = 5 //Purchase Invoice
    } else if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      //console.log("2");
      this.invoiceTypeId = 1; //Customer Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {
      //console.log("3");
      this.invoiceTypeId = 18; //Subcontracting Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      //console.log("4");
      this.invoiceTypeId = 15; //Jobwork Invoice
    }
    if (this.mainPartyType === "jobworker") {
      this.isJW = true;
      this.getFilteredParties();
    } else {
      this.isJW = false;
      this.getFilteredParties();
    }
     console.log("this.invoiceTypeId: "+this.invoiceTypeId)

  }
  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
        .subscribe(response => {
         console.log('transaction type: ', response);
          this.transactionType = response;
          if (this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {

            this.isCustomer = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

            //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
          } else if (this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {
            this.isSupplier = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
            //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

          }
        
            this.mainPartyTypeChange();
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();

          })  
      });
  }

  getData() {
 
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {

          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "invoiceDate";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.payableRecievableReportDatabase!.getReport(this.transactionType.id,this.invoiceTypeId,this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
           console.log('data.materialList: ', data.data);

          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }
 
  getTotalPaidAmount() {
    return this.dataSource.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.paidAmount).reduce((acc, value) => acc + value, 0);
  }

  
  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

    
  close() {
    this._router_in.navigate(['/']);
  }

  clear() {

    this.filterForm.get('financialYearId').patchValue(null);
    this.filterForm.get('partyId').patchValue(null);
    this.filterForm.get('materialId').patchValue(null);
    this.filterForm.get('transactionFromDate').patchValue(null);
    this.filterForm.get('transactionToDate').patchValue(null);
    this.partyControl.patchValue(null);
    this.materialControl.patchValue(null);
    this.dataSource.data = [];
    // this.searchButton$.next('Search');
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name.replace(' ', '_');
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });

  }

  makeSummaryRow(excelData): ExportDataType {
    return {
      "SL No": null,
      "Paid Date": null,
      "Payment Number": "TOTAL",

      "Status": null,
      "InvoiceNumber": null,
      "InvoiceDate": null,
      "PartyName": null,
      "TotalAmount": excelData.map(item => item.invoiceAmount).reduce((prev, curr) => prev + curr, 0),
      "BalanceAmount": excelData.map(item => item.dueAmount).reduce((prev, curr) => prev + curr, 0),
      "TDSPercentage": null,
      "TDSAmount": excelData.map(item => item.tdsAmount).reduce((prev, curr) => prev + curr, 0),
      "AmountAfterTds": excelData.map(item => item.amountAfterTds).reduce((prev, curr) => prev + curr, 0),
      "AmountPaid": excelData.map(item => item.paidAmount).reduce((prev, curr) => prev + curr, 0),
      "PaymentMode": null,
      "Cheque No/Bank Name": null
    }
  }


  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.payableReportHeader();
      let reportItem: any[] = this.payableReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 4) {
        reportName = "Payable-Receivable Report"
      }
      if (this.transactionType.id === 3) {
        reportName = "Receivable Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });



  }


  payableReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Paid Date": "Paid Date",
        "Payment Number": "Payment Number",

        "Status": "Status",
        "InvoiceNumber": "InvoiceNumber",

        "InvoiceDate": "InvoiceDate",
        "PartyName": "PartyName",
        "TotalAmount": "TotalAmount",
        "BalanceAmount": "BalanceAmount",
        "TDSPercentage": "TDSPercentage",
        "TDSAmount": "TDSAmount",
        "AmountAfterTds": "AmountAfterTds",
        "AmountPaid": "AmountPaid",
        "PaymentMode": "PaymentMode",
        "Cheque No/Bank Name": "Cheque No/Bank Name",


      },



    ];




  }




  payableReportBody(invoiceReport: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);
    return reportItem;





  }


  makeReportItem(item: PayableReceivableReportModel, count: number): any {
    if (item.paymentDocumentNumber === null) {
      item.paymentDocumentNumber = ''
    }
    if (item.bankName === null) {
      item.bankName = ''
    }
    if(item.bankName){
      item.bankName = "/"+item.bankName
    }

   
    return {
      "SL No": count,

      "Paid Date": this.datePipe.transform(item.payReferenceDate, 'dd-MM-yyyy'),
      "Payment Number": item.payReferenceNumber,
      "Status": item.statusName,
      // "slNo": quo.,
      "InvoiceNumber": item.invoiceNumber,
      "InvoiceDate": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "PartyName": item.partyName,
      "TotalAmount": item.invoiceAmount,
      "BalanceAmount": item.dueAmount,
      "TDSPercentage": item.tdsPercentage,
      "TDSAmount": item.tdsAmount,
      "AmountAfterTds": item.amountAfterTds,
      "AmountPaid": item.paidAmount,
      "PaymentMode": item.paymentMethodName,
      "Cheque No/Bank Name": item.paymentDocumentNumber+item.bankName,
    }
  }

  getExcel() {
    this.payableReceivableService.downloadPayableReceivableItemReport(this.transactionType.id, this.filterForm.value, this.page)
      .subscribe(response => {
        this.exportData(response);
      },
        error => {
          //console.log("error :", error);
        })

  }

  exportData(data) {
    let blob = data;
    let a = document.createElement("a");
    a.setAttribute('style', 'display: none');
    a.href = URL.createObjectURL(blob);
    a.download = 'fileName.xlsx';
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
}
 
 

export class PayableReceivableHttpDao {

  constructor(private payableReceivableService: PayableReceivableService) { }

  getReport(transactionTypeId: number,invoiceTypeId: number, filterForm: FormGroup, page: Page): Observable<PagedData<PayableReceivableReportModel>> {
    console.log("invoiceTypeId re"+invoiceTypeId)
    return this.payableReceivableService.getPayableReceivableItemReport(transactionTypeId, invoiceTypeId,filterForm.value, page);
  }
}


export interface ExportDataType {

  "SL No": number;

  "Paid Date": string;
  "Payment Number": string;
  "Status": string;
  "InvoiceNumber": string;

  "InvoiceDate": string;
  "PartyName": string;
  "TotalAmount": number;
  "BalanceAmount": number;
  "TDSPercentage": number;
  "TDSAmount": number;
  "AmountAfterTds": number;
  "AmountPaid": number;
  "PaymentMode": string;
  "Cheque No/Bank Name": string;
}