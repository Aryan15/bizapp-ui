import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FinancialYearService } from '../../services/financial-year.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { PartyService } from '../../services/party.service';
import { MaterialService } from '../../services/material.service';
import { UserService } from '../../services/user.service';
import { InvoiceService } from '../../services/invoice.service';
import { FormGroup } from '@angular/forms';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { InvoiceTaxReportModel } from '../../data-model/invoice-model';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, flatMap, debounceTime, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Party } from '../../data-model/party-model';
import { Material } from '../../data-model/material-model';
import { ExcelService } from '../../services/excel.service';
import { AlertService } from 'ngx-alerts';
@Component({
  selector: 'app-tax-report-new',
  templateUrl: './tax-report-new.component.html',
  styleUrls: ['./tax-report-new.component.scss']
})
export class TaxReportNewComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {

  pivotDArray = new MatTableDataSource<InvoiceTaxReportModel>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  // @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  invoiceDatabase: TaxReportHttpDao | null;
  isItemLvelTax: boolean = false;
  isLoadingResults: boolean = false;
  resultsLength: number;
  isSupplier: boolean = false;
  displayedColumns = [
    'invoiceNumber',
    'invoiceDate',
    'partyName',
    'gstNumber',
    'statusName',
    'netAmount'
  ];
  taxColumns = [];
  constructor(
    public route_in: ActivatedRoute
    , private datePipe: DatePipe
    , private financialYearService_in: FinancialYearService
    , private transactionTypeService_in: TransactionTypeService
    , protected partyService_in: PartyService
    , private materialService_in: MaterialService
    , private _router_in: Router
    , public userService: UserService
    , private excelService: ExcelService
    , private alertService: AlertService
    , private invoiceService: InvoiceService
  ) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
  }

  ngAfterViewInit() {

    this.pivotDArray.sort = this.sort;

    this.invoiceDatabase = new TaxReportHttpDao(this.invoiceService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    this.getDateRange();
    this.getTransactionType();
    this.getUsers();

  }

  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
          .subscribe(response => {
            //console.log('transaction type: ', response);
            this.transactionType = response;
            if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {

              this.isCustomer = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

              //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
            } else if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
              this.isSupplier = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
              //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

            }
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();
            // this.deriveBuyOrSell()
            // this.getGlobalSetting();
          })
      });
  }

  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "invId";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.invoiceDatabase!.getInvoices(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.pivotDArray.data = this.pivotData(data)
        //console.log("resultsLength: "+this.resultsLength);

      });
  }

  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  getTotalGrandTotal() {
    return this.pivotDArray.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.grandTotal).reduce((acc, value) => acc + value, 0);
  }

  getTotalTaxCol(taxCol: string) {
    return this.pivotDArray.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t[taxCol]).reduce((acc, value) => acc + value, 0);
  }

  getTotalNetAmount() {
    return this.pivotDArray.data
      .filter(inv => inv.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.netAmount).reduce((acc, value) => acc + value, 0);
  }

  close() {
    this._router_in.navigate(['/']);
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name.replace(' ', '_');

    let excelPage = { ...this.page };
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.invoiceDatabase.getInvoices(this.transactionType.id, this.filterForm.value, excelPage)
      .subscribe(pagedData => {

        // pagedData

        let reports: any[] = [];
        if (pagedData.data.length > 0) {
          //console.log(" inside pagedData.data ", pagedData.data)
          this.pivotDArray.data = this.pivotData(pagedData.data)
        } else {
          //console.log(" in else pagedData.data ", pagedData.data)
          this.pivotDArray = null
        }
        // this.pivotDArray = this.pivotData()
        excelData = this.pivotDArray.data;
        //console.log("this.pagedData ", pagedData.data)
        //console.log("this.excelData ", excelData)
        if (excelData != null) {
          //console.log("in ")
          this.excelService.exportFromJSON(excelData, fileName);
        }
        else {
          //console.log("in else ")
          this.alertService.danger("No data found!!Cannot export");
        }

      });
  }


  pivotData(rows: any): any {
    let prevInvNo = '';
    let pivotD: any;
    let pivotDArray: any = [];
    //console.log("in pivottara");
    rows.forEach(d => {

      // //console.log('row: ', d);
      if (prevInvNo !== d.invoiceNumber) {
        ////console.log(d.invoice, d.taxPercent)
        //console.log("pivotD.............",pivotD);

        if (pivotD)
          pivotDArray.push(pivotD);
        pivotD = {};


        pivotD["invoiceNumber"] = d.invoiceNumber;
        pivotD["invoiceDate"] = this.datePipe.transform(d.invoiceDate, AppSettings.DATE_FORMAT),
          pivotD["partyName"] = d.partyName;
        pivotD["gstNumber"] = d.gstNumber;


        pivotD["statusName"] = d.statusName;
        pivotD["netAmount"] = d.netAmount;
        pivotD["cgst" + d.cgstTaxPercent] = d.cgstTaxAmount;
        pivotD["sgst" + d.sgstTaxPercent] = d.sgstTaxAmount;
        //pivotD["'igst" + d.igstTaxPercent+"'"] = d.igstTaxAmount;
        pivotD["grandTotal"] = d.grandTotal;
        prevInvNo = d.invoiceNumber;

      }
      else if (this.isItemLvelTax) {


        if (pivotD["cgst" + d.cgstTaxPercent]) {

          pivotD["cgst" + d.cgstTaxPercent] = +pivotD["cgst" + d.cgstTaxPercent] + d.cgstTaxAmount;
        } else {
          pivotD["cgst" + d.cgstTaxPercent] = d.cgstTaxAmount;
        }

        if (pivotD["sgst" + d.sgstTaxPercent]) {
          pivotD["sgst" + d.sgstTaxPercent] = +pivotD["sgst" + d.sgstTaxPercent] + d.sgstTaxAmount;
        } else {
          pivotD["sgst" + d.sgstTaxPercent] = d.sgstTaxAmount;
        }

        if (pivotD["igst" + d.igstTaxPercent]) {
          pivotD["igst" + d.igstTaxPercent] = +pivotD["igst" + d.igstTaxPercent] + d.igstTaxAmount;
        } else {
          pivotD["igst" + d.igstTaxPercent] = d.igstTaxAmount;
        }
      }
      else {

        pivotD["cgst" + d.cgstTaxPercent] = d.cgstTaxAmount;

        pivotD["sgst" + d.sgstTaxPercent] = d.sgstTaxAmount;

        pivotD["igst" + d.igstTaxPercent] = d.igstTaxAmount;

      }

      //populate column headers
      //console.log('this.displayedColumns.find(col => col.name === "taxPer"+d.taxPercent): ',this.displayedColumns.find(col => col.name === "taxPer"+d.taxPercent));
      if (!this.displayedColumns.find(col => col === "cgst" + d.cgstTaxPercent) && d.cgstTaxPercent) {
        this.displayedColumns.push("cgst" + d.cgstTaxPercent)
        this.taxColumns.push("cgst" + d.cgstTaxPercent)
      }

      if (!this.displayedColumns.find(col => col === "sgst" + d.sgstTaxPercent) && d.sgstTaxPercent) {
        this.displayedColumns.push("sgst" + d.sgstTaxPercent)
        this.taxColumns.push("sgst" + d.sgstTaxPercent)
      }

      // if (!this.displayedColumns.find(col => col === "igst" + d.igstTaxPercent) && d.igstTaxPercent) {
      //   this.displayedColumns.push("'igst" + d.igstTaxPercent+"'")
      // }



    });


    this.displayedColumns.push("grandTotal");

    pivotDArray.push(pivotD);
    console.log('pivot Data: ', pivotDArray);
    return pivotDArray;
  }

  getDisplaycol() {

  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

}


export class TaxReportHttpDao {

  constructor(private invoiceService: InvoiceService) { }

  getInvoices(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<InvoiceTaxReportModel>> {

    // pageSize = pageSize ? pageSize : PAGE_SIZE;
    // //console.log('filterText: ', filterText);
    //console.log('page: ', page);
    //console.log('filterForm.value: ', filterForm.value);

    //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);
    return this.invoiceService.getTaxInvoiceReport(transactionType, filterForm.value, page);
  }
}