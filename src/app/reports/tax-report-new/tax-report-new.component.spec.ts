import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxReportNewComponent } from './tax-report-new.component';

describe('TaxReportNewComponent', () => {
  let component: TaxReportNewComponent;
  let fixture: ComponentFixture<TaxReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
