import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { B2BGSTReport, B2BGSTReportWrapper } from '../../data-model/invoice-model';
import { TransactionType } from '../../data-model/transaction-type';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GstReportService } from '../../services/gst-report.service';
import { InvoiceService } from '../../services/invoice.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';

@Component({
  selector: 'app-gst-report',
  templateUrl: './gst-report.component.html',
  styleUrls: ['./gst-report.component.scss']
})
export class GstReportComponent extends ReportParentComponent implements OnInit {

  transactionType: TransactionType = {
    id: 1,
    name: 'GSTR',
    description: 'GSTR'
  };
 // gstReportRequest: TransactionReportRequest = { financialYearId: null, materialId: null, partyId: null, transactionFromDate: null, transactionToDate: null, partyTypeIds: [] };
  
 gstForm: FormGroup;
 downloadJsonHref: any;
  constructor(
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private gstReportService: GstReportService,
    private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService ,
    private datePipe: DatePipe,
    private _router_in: Router,
    private invoiceService: InvoiceService,
    private route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private userService: UserService) {
      super(
        partyService_in,
        materialService_in,
        financialYearService_in,
        transactionTypeService_in,
        route_in,
        _router_in,
        userService
    );
   }

  ngOnInit() {
    this.initParent();
    this.setPage({ offset: 0 });
    this.initForm();

  }
  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }

  setPage(pageInfo){
    //console.log("in setPage: ",pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";
   // this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.gstReportService.getB2BReport(this.transactionType.id,this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      //console.log('this.rows: ', this.rows);
      this.gstForm = this.toFormGroup({ b2bGSTReports: this.rows, totalCount: 0 });
    });
  }
  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.gstReportService.getB2BReport(this.transactionType.id,this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      this.gstForm = this.toFormGroup({ b2bGSTReports: this.rows, totalCount: 0 });
    });

  }

  saveAsJSON(){
    //console.log("report: ", this.gstForm.value);

    var theJSON = JSON.stringify(this.gstForm.value);
    var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    this.downloadJsonHref = uri;

  }
  saveAsExcel(): void {
    let fileName: string = 'test';
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = {...this.page};
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.gstReportService.getB2BReport(this.transactionType.id,this.filterForm.value, excelPage).subscribe(pagedData => {
     // this.page = pagedData.page;
     // this.rows = pagedData.data;
      //console.log('this.rows: ', this.rows);
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)

      let reports: any[] = [];

      excelData.forEach(data => {
        reports.push({
          "GSTIN": data.gstin,
          "Invoice Number": data.invoiceNumber, 
          "Invoice Date": this.datePipe.transform(data.invoiceDate, AppSettings.DATE_FORMAT), 
          "Invoice Value": data.invoiceValue, "State Code": data.stateCode,
          "Reverse Charge": data.reverseCharge, 
          "Invoice Type": data.invoiceType,
          "Applicable Percent Of Tax Rate" : data.applicablePercentOfTaxRate,
           "ECommerce GSTIN": data.eCommerceGstn ,
           "Serial Number" : data.serialNumber,
          "Rate": data.rate, "Taxable Value": data.taxableValue,
           "Integrated Tax": data.integratedTax,"Central Tax" : data.centralTax,
          "State Tax" : data.stateTax, "Cess" : data.cess,
        })
      })

      this.excelService.exportFromJSON(reports, fileName);

    });


  }

  onSearch() {
    
     //this.deliveryChallanReportRequest.partyTypeIds = partyTypeIds;
    
        this.gstReportService.getB2BReport(this.transactionType.id,this.filterForm.value, this.page).subscribe(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
        });
      }
    

      initForm() {
        let data: B2BGSTReportWrapper = {
          b2bGSTReports: null,
          totalCount: null,
        };
    
        this.gstForm = this.toFormGroup(data);
      }
    
      toFormGroup(data: B2BGSTReportWrapper): FormGroup {
    
        const b2bGSTReports = new FormArray([]);
        if (data.b2bGSTReports) {
          data.b2bGSTReports.forEach(c => {
            b2bGSTReports.push(this.makeItem(c))
          })
        }
    
        const formGroup = this.fb.group({
          b2bGSTReports: b2bGSTReports
        });
    
        return formGroup;
      }
    
    
      makeItem(a: B2BGSTReport): FormGroup {
        return this.fb.group({
          id: a.id,
          gstin: a.gstin,
          invoiceNumber: a.invoiceNumber,
          invoiceDate: a.invoiceDate,
          invoiceValue: a.invoiceValue,
          stateCode: a.stateCode,
          reverseCharge: a.reverseCharge,
          applicablePercentOfTaxRate: a.applicablePercentOfTaxRate,
          invoiceType: a.invoiceType,
          eCommerceGstn: a.eCommerceGstn,
          serialNumber: a.serialNumber,
          rate: a.rate,
          taxableValue: a.taxableValue,
          integratedTax: a.integratedTax,
          centralTax: a.centralTax,
          stateTax: a.stateTax,
          cess: a.cess
        })
      }

}
