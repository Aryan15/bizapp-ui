import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, Subject, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { EWayBillReport, EWayBillReportModel } from '../../data-model/invoice-model';
import { Party } from '../../data-model/party-model';
import { EWayBillService } from '../../services/e-way-bill.service';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PdfService } from '../../services/pdf.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ExportDataType } from '../balance-report/balance-report.component';
 import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';

@Component({
  selector: 'app-eway-bill-report',
  templateUrl: './eway-bill-report.component.html',
  styleUrls: ['./eway-bill-report.component.scss']
})
export class EwayBillReportComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {

  displayedColumns = ['eWayBillNo', 'eWayBillDate', 'supplyType','invoiceNumber',  'invoiceDate', 'partyName', 'partyGstNum',
  'transportedGstNum', 'fromAddress', 'toAddress', 'status' ,'noOfItems', 'hsnCode', 'hsnDescription', 'assesableValue',
   'sgst', 'cgst', 'igst', 'cess', 'noncess', 'otherValue', 'invoiceValue', 'eWayBillValidity','detailedpdfUrl' ];
  subheading: string;
  dataSource = new MatTableDataSource<EWayBillReportModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  EwayBillDatabase: EwayBillHttpDao | null;
  resultsLength = 0;
  isLoadingResults = true;
  page = new Page();
  searchButton$ = new Subject<string>();
  onDestroy: Subject<boolean> = new Subject();
  filterForm = new FormGroup({
    transactionFromDate: new FormControl(),
    transactionToDate: new FormControl(),
    financialYearId: new FormControl(),
    partyId: new FormControl(),
    materialId: new FormControl(),
    partyTypeIds: new FormControl([]),
    createdBy: new FormControl(),
    statusName:new FormControl()
  });
 
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService,
    private eWayBillService: EWayBillService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }
  ngAfterViewInit() {
 
    this.dataSource.sort = this.sort;

    this.EwayBillDatabase = new EwayBillHttpDao(this.eWayBillService);

    //If the user changes sort order, reset back to first page
    this.isCustomer = true;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.getDateRange();
    this.getData();
    this.getFilteredParties();

  }

  
  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          let filterText = "";
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "eWayBillDate";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.EwayBillDatabase!.getReport(this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        data.forEach(rep =>{
          console.log("rep.hsnCode : "+rep.hsnCode );
        })
       

      });
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  
  saveAsExcel(): void {
     let filterText = "";
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.eWayBillService.getEwayBillReport(this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      this.excelService.exportFromJSON(reports, "E-Way Bill Report");

    });

  }

  saveAsPdf(): void {
    let pdfPage = { ...this.page };

    let filterText = "";
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.eWayBillService.getEwayBillReport(this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.materialReportHeader();
      let reportItem: any[] = this.EwayBillReportBody(pdfData);

      this.pdfService.exportToPdf("E-Way Bill Report", reportHeader, reportItem, "");
    });

  }


  materialReportHeader() {
    return [
      {
      "SL No": "SL No",
      "eWayBillNo": "E-Way Bill No",
      "eWayBillDate": "E-Way Bill Date",
      "supplyType": "Supply Type",
      "invoiceNumber": "Invoice Number",
      "invoiceDate": "Invoic Date",
      "partyName": "Party Name",
      "partyGstNum": "Party Gst Number",
      "transportedGstNum": "Transported Gst Number",
      "fromAddress": "From Address",
      "toAddress": "To Address",
      "status": "Status",
      "noOfItems": 'No Of Items',
      "hsnCode": "HsnCode",
      "hsnDescription": 'Hsn Description',
      "assesableValue": "Assesable Value",
      "sgst": "Sgst",
      "cgst": "Cgst",
      "igst": "Igst",
      "cess": "Cess",
      "noncess": "Non Cess",
      "otherValue": "Other Value",
      "invoiceValue": "Invoice Value",
      "eWayBillValidity": "E-Way Bill Validity",

      },

    ];

  }


  EwayBillReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: any[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
  

    return reportItem;





  }


  makeReportItem(item: EWayBillReportModel, count: number): any {
    return {
      "SL No": count,
      "eWayBillNo": item.eWayBillNo,
      "eWayBillDate": this.datePipe.transform(item.eWayBillDate, 'dd-MM-yyyy'),
      "supplyType": item.supplyType,
      "invoiceNumber": item.invoiceNumber,
      "partyName": item.partyName,
      "invoiceDate": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "partyGstNum": item.partyGstNum,
      "transportedGstNum": item.transportedGstNum,
      "fromAddress": item.fromAddress,
      "toAddress": item.toAddress,
      "status": item.status,
      "noOfItems": item.noOfItems,
      "hsnCode": item.hsnCode,
      "hsnDescription": item.hsnDescription,
      "assesableValue": item.assesableValue,
      "sgst": item.sgst,
      "cgst": item.cgst,
      "igst": item.igst,
      "cess": item.cess,
      "noncess": item.noncess,
      "otherValue": item.otherValue,
      "invoiceValue": item.invoiceValue,
      "eWayBillValidity": this.datePipe.transform(item.eWayBillValidity, 'dd-MM-yyyy'),

    }
  }

  
  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }


  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

 

  close() {
    this._router_in.navigate(['/']);

  }

  goToLink(url) {
   // console.log(url);
    var win = window.open(url, '_blank');
    win.focus();
  }

  
}



export class EwayBillHttpDao {

  constructor(private ewayBillService: EWayBillService) { }

  getReport( filterForm: FormGroup, page: Page): Observable<PagedData<EWayBillReportModel>> {
 
    return this.ewayBillService.getEwayBillReport(filterForm.value, page);
  }
}