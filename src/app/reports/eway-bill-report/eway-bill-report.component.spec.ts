import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EwayBillReportComponent } from './eway-bill-report.component';

describe('EwayBillReportComponent', () => {
  let component: EwayBillReportComponent;
  let fixture: ComponentFixture<EwayBillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EwayBillReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EwayBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
