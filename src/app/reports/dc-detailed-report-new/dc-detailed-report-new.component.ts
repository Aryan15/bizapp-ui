import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DeliveryChallanItemModel } from '../../data-model/delivery-challan-model';
import { InvoiceReportItemModel } from '../../data-model/invoice-model';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { FinancialYearService } from '../../services/financial-year.service';
import { PartyService } from '../../services/party.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { AppSettings } from '../../app.settings';
import { catchError, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Party } from '../../data-model/party-model';
import { Material } from '../../data-model/material-model';

@Component({
  selector: 'app-dc-detailed-report-new',
  templateUrl: './dc-detailed-report-new.component.html',
  styleUrls: ['./dc-detailed-report-new.component.scss']
})
export class DcDetailedReportNewComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {
  
  displayedColumns = ['dcNumber','dcDate','status','partyName','material','hsnCode','quantity','remarks'];
  subheading: string;
  dataSource = new MatTableDataSource<DeliveryChallanItemModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isItemLevelTax: boolean;

  dcDetailDatabase: DCDetailHttpDao | null;
  resultsLength = 0;
   isLoadingResults = true;
   constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private deliveryChallanService: DeliveryChallanService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }


  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dcDetailDatabase = new DCDetailHttpDao(this.deliveryChallanService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
 
    this.getDateRange();
    this.getTransactionType();
    this.getUsers();
   
  }


  private getTransactionType() {
    this.route_in.params
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(params => {
        this.transactionTypeService_in.getTransactionType(+params['trantypeId'])
          .subscribe(response => {
             console.log('transaction type: ', response);
            this.transactionType = response;
            if (this.transactionType.name === AppSettings.CUSTOMER_DC) {

              this.isCustomer = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

              //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
            } else if (this.transactionType.name === AppSettings.INCOMING_DC) {
              this.isSupplier = true;
              this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
              //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

            }
            this.getData();
            this.getFilteredParties();
            this.getFiltredMaterial();
            this.getFinancialYears();
           })
      });
  }

  
  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "deliveryChallanDate";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.dcDetailDatabase!.getReport(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }

  onSearch() {
    //console.log("this.partyControl.value", this.partyControl.value);

    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    if (!this.partyControl.value) {
      this.filterForm.get('partyId').patchValue(null);
    }
    this.searchButton$.next('Search')
  }

  
  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  close() {
    this._router_in.navigate(['/']);
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  
  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)

      let reports: ExportDataType[] = [];


      excelData.forEach((data, index) => {
        reports.push(
          this.makeDcReportItem(data, index + 1)
        )
      })

   

    
      this.excelService.exportFromJSON(reports, fileName);

    });



  }



  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;
      let reportHeader: any[] = this.dcReportHeader();
      let reportItem: any[] = this.dcReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 7) {
        reportName = "Sales DC Detailed Report"
      }
      if (this.transactionType.id === 6) {
        reportName = "Purchase DC Detailed Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }

  dcReportHeader() {

    return [
      {
        "SL No": "SL No",
        "DC Number": "DC Number",
        "DC Date": "DC Date",
        "Status": "Status",
        "Party Name": "Party Name",
        "Material": "Material",
        "Hsn Code": "Hsn Code",
        "Quantity": "Qty/Units",
       
        "Remarks": "Remarks"
      },
    ];
  }
  dcReportBody(pdfData: any[]) {
    let count: number = 0
    let reportItem: any[] = [];
    pdfData.forEach((po, index) => {

      reportItem.push(this.makeDcReportItem(po, index + 1));
    });
   
    return reportItem;

  }

  makeDcReportItem(item: DeliveryChallanItemModel,
    count: number): any {

    return {
      "SL No": count,
      "DC Number": item.deliveryChallanNumber,
      "DC Date": this.datePipe.transform(item.deliveryChallanDate, 'dd-MM-yyyy'),
      "Status": item.statusName,
      "Party Name": item.partyName,
      "Material": item.materialName,
      "Hsn Code": item.hsnOrSac,
      "Quantity": item.quantity+""+item.uom,
      "Remarks": item.remarks,

    }
}
}

export class DCDetailHttpDao {

  constructor(private deliveryChallanService: DeliveryChallanService) { }

  getReport(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<DeliveryChallanItemModel>> {
 
    return this.deliveryChallanService.getDeliveryChallanItemReport(transactionType, filterForm.value, page);
  }
}


export interface ExportDataType {


  "SL No": number;
  "DC Number": string;
  "DC Date": string;
  "Status": string;
  "Party Name": string;
  "Material": string;
  "Hsn Code": string;
  "Quantity": number;
 
  "Remarks": number;

}