import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcDetailedReportNewComponent } from './dc-detailed-report-new.component';

describe('DcDetailedReportNewComponent', () => {
  let component: DcDetailedReportNewComponent;
  let fixture: ComponentFixture<DcDetailedReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcDetailedReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcDetailedReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
