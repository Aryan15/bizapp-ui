import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcDetailedReportComponent } from './dc-detailed-report.component';

describe('DcDetailedReportComponent', () => {
  let component: DcDetailedReportComponent;
  let fixture: ComponentFixture<DcDetailedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcDetailedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcDetailedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
