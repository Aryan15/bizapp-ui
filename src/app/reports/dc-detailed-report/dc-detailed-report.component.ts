import { DataSource } from '@angular/cdk/table';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { PartyTypeService } from '../../services/financial-year.service';
import { AppSettings } from '../../app.settings';
import { Material } from '../../data-model/material-model';
import { DeliveryChallanService } from '../../services/delivery-challan.service';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PdfService } from '../../services/pdf.service';
import { DeliveryChallanItemReport, DeliveryChallanItemModel } from '../../data-model/delivery-challan-model';
declare var jsPDF: any;

@Component({
  selector: 'app-dc-detailed-report',
  templateUrl: './dc-detailed-report.component.html',
  styleUrls: ['./dc-detailed-report.component.scss'],
})
export class DcDetailedReportComponent extends ReportParentComponent implements OnInit {
  public materials: Material[] = [];
  subheading: string;
  public dataSource: DataSource<any>;

  // deliveryChallanReportRequest: DeliveryChallanReportRequest = { financialYearId: null, materialId: null, partyId: null, transactionFromDate: null, transactionToDate: null, partyTypeIds: [] };
  //pageSize: number;
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private deliveryChallanService: DeliveryChallanService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }

  ngOnInit() {
    this.initParent();
    this.initComplete.subscribe(response => {
      this.onSearch();
    });

  }
  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  //  onSearch(){
  //     this.searchButton$.next('Search')
  //   }

  setPage(pageInfo) {
    //console.log("in setPage: ", pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "deliveryChallanDate";
    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)

      let reports: ExportDataType[] = [];


      excelData.forEach((data, index) => {
        reports.push(
          this.makeDcReportItem(data, index + 1)
        )
      })

   

    
      this.excelService.exportFromJSON(reports, fileName);

    });



  }



  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;
      let reportHeader: any[] = this.dcReportHeader();
      let reportItem: any[] = this.dcReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 7) {
        reportName = "Sales DC Detailed Report"
      }
      if (this.transactionType.id === 6) {
        reportName = "Purchase DC Detailed Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }

  dcReportHeader() {

    return [
      {
        "SL No": "SL No",
        "DC Number": "DC Number",
        "DC Date": "DC Date",
        "Status": "Status",
        "Party Name": "Party Name",
        "Material": "Material",
        "Hsn Code": "Hsn Code",
        "Quantity": "Qty/Units",
       
        "Remarks": "Remarks"
      },
    ];
  }
  dcReportBody(pdfData: any[]) {
    let count: number = 0
    let reportItem: any[] = [];
    pdfData.forEach((po, index) => {

      reportItem.push(this.makeDcReportItem(po, index + 1));
    });
   
    return reportItem;

  }

  makeDcReportItem(item: DeliveryChallanItemModel,
    count: number): any {

    return {
      "SL No": count,
      "DC Number": item.deliveryChallanNumber,
      "DC Date": this.datePipe.transform(item.deliveryChallanDate, 'dd-MM-yyyy'),
      "Status": item.statusName,
      "Party Name": item.partyName,
      "Material": item.materialName,
      "Hsn Code": item.hsnOrSac,
      "Quantity": item.quantity+""+item.uom,
      "Remarks": item.remarks,

    }
  }





























  onSearch() {

    this.deliveryChallanService.getDeliveryChallanItemReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

}
export interface ExportDataType {


  "SL No": number;
  "DC Number": string;
  "DC Date": string;
  "Status": string;
  "Party Name": string;
  "Material": string;
  "Hsn Code": string;
  "Quantity": number;
 
  "Remarks": number;

}