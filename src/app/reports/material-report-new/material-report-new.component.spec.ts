import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialReportNewComponent } from './material-report-new.component';

describe('MaterialReportNewComponent', () => {
  let component: MaterialReportNewComponent;
  let fixture: ComponentFixture<MaterialReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
