import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Material, SupplyType } from '../../data-model/material-model';
import { ExcelService } from '../../services/excel.service';
import { PdfService } from '../../services/pdf.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { MaterialService } from '../../services/material.service';

@Component({
  selector: 'app-material-report-new',
  templateUrl: './material-report-new.component.html',
  styleUrls: ['./material-report-new.component.scss']
})
export class MaterialReportNewComponent implements  AfterViewInit, OnDestroy {

  displayedColumns = ['name','partNumber','hsnCode','mrp','buyingPrice','price','stock','unitOfMeasurementName'];
  subheading: string;
  dataSource = new MatTableDataSource<Material>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
   supplyType: SupplyType = { deleted: "N", id: 1, name: "Goods" };
   isCustomer: boolean = false;
   isSupplier: boolean = false;
  materialDatabase: MaterialHttpDao | null;
  resultsLength = 0;
   isLoadingResults = true;
   page = new Page();
   searchButton$ = new Subject<string>();
   onDestroy: Subject<boolean> = new Subject();

   constructor(
    private datePipe: DatePipe,
    private _router: Router,
    public route: ActivatedRoute,
    private materialService: MaterialService,
    private excelService: ExcelService,
    public userService: UserService,
    private pdfService: PdfService) {
 
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

  
  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.materialDatabase = new MaterialHttpDao(this.materialService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
 
    this.getData();
    
  }
 

  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          let filterText = "";
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "name";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.materialDatabase!.getReport(this.supplyType.id, filterText, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  saveAsExcel(): void {
    let fileName: string = this.supplyType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let filterText = "";
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

   
      this.excelService.exportFromJSON(reports, fileName);

    });


  }


 


  saveAsPdf(): void {
    let pdfPage = { ...this.page };

    let reportObject: any;
    let filterText = "";
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.materialService.getAllMaterialsForReport(this.supplyType.id, filterText, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.materialReportHeader();
      let reportItem: any[] = this.materialReportBody(pdfData);


      this.pdfService.exportToPdf("Material Report", reportHeader, reportItem, this.supplyType.name);
    });



  }


  materialReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Material": "Material",
        "PartNumber": "PartNumber",
        "HsnCode": "HsnCode",
        "MRP": "MRP",
        "BuyingPrice": "BuyingPrice",
        "SellingPrice": "SellingPrice",
        "CurrentStock": "CurrentStock",
        "UOM": "UOM"

      },



    ];




  }




  materialReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: any[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
  

    return reportItem;





  }


  makeReportItem(item: Material, count: number): any {
    return {
      "SL No": count,
      "Material": item.name,
      "PartNumber": item.partNumber,
      "HsnCode": item.hsnCode,
      "MRP": item.mrp,
      "BuyingPrice": item.buyingPrice,
      "SellingPrice": item.price,
      "CurrentStock": item.stock,
      "UOM": item.unitOfMeasurementName,

    }
  }



  close() {
    this._router.navigate(['/']);

  }
}



export class MaterialHttpDao {

  constructor(private materialService: MaterialService) { }

  getReport(supplyType: number, filterText: string, page: Page): Observable<PagedData<Material>> {
 
    return this.materialService.getAllMaterialsForReport(supplyType, filterText, page);
  }
}

export interface ExportDataType {

  "SL No": number;
  "Material": string;
  "PartNumber": string;
  "HsnCode": string;
  "MRP": string;
  "BuyingPrice": string;
  "SellingPrice": string;
  "CurrentStock": string;
  "UOM": number;

}