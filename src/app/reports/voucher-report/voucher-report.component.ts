import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
//import { PartyTypeService } from '../../services/financial-year.service';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { ExpenseHeader, VoucherReportModel } from '../../data-model/voucher-model';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { VoucherService } from '../../services/voucher.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PdfService } from '../../services/pdf.service';

@Component({
  selector: 'app-voucher-report',
  templateUrl: './voucher-report.component.html',
  styleUrls: ['./voucher-report.component.scss']
})
export class VoucherReportComponent extends ReportParentComponent implements OnDestroy, AfterViewInit {
  isLoadingResults = true;
  isCheque = false;
  voucherDatabase: VoucherReportHttpDao | null;
  dataSource = new MatTableDataSource<VoucherReportModel>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  resultsLength = 0;
  displayedColumns = [];
  expenses: ExpenseHeader[] = [];
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private voucherService: VoucherService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private numberFormatterService: NumberFormatterService,
    private userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    //this.getTransactionType();
  }

  ngAfterViewInit() {
    this.initParent();
    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


    this.voucherDatabase = new VoucherReportHttpDao(this.voucherService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.getData();
    // this.getDateRange();
    // this.getTransactionType();
    this.initComplete.subscribe(response => {
      if (this.transactionType.name === AppSettings.CHEQUE_VOUCHER) {
        this.isCheque = true;
      } else {
        this.isCheque = false;
      }
      //console.log("this.isCheque: "+this.isCheque);
      this.getData();
      this.getDisplaycol();
      this.getExpense();
    });

  }

  clear() {
    super.clear();
    this.dataSource.data = [];
  }

  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "voucherDate";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.voucherDatabase!.getVouchers(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          //console.log('data.materialList: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchEr"Party Name": data.partyName,ror ', error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: " + this.resultsLength, this.dataSource.data);

      });
  }

  getExpense() {
    this.voucherService.getAllExpense().subscribe(
      response => {

        this.expenses = response;
        //console.log("this.expenses" + this.expenses);
      }
    )
  }

  getDisplaycol() {
    if (!this.isCheque) {
      //console.log("in")
      this.displayedColumns = [
        'voucherNumber'
        , 'voucherDate'
        , 'expenseName'
        , 'paidTo'
        , 'description'
        , 'amount'
      ];
    }
    else {
      //console.log("else")
      this.displayedColumns = [
        'voucherNumber'
        , 'voucherDate'
        , 'expenseName'
        , 'paidTo'
        , 'description'
        , 'amount'
        , 'chequeNumber'
        , 'chequeDate'
        , 'bankName'
      ];
    }
  }




  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }





  onSearch() {
    this.searchButton$.next('Search')
  }

  getTotalAmount() {
    return this.dataSource.data
      .map(t => t.amount).reduce((acc, value) => acc + value, 0);
  }

  saveAsExcel(): void {
    let fileName: string = 'test';


    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;

    this.voucherService.getVoucherReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)

      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });




  }

  makeSummaryRow(excelData): ExportDataType {
    return {
      "SL No": null,
      "VoucherNumber": "TOTAL",
      "VoucherDate": null,
      "Expenses": null,
      "PaidTo": null,
      "Description": null,
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "BankName": null,
      "ChequeNumber": null,
      "ChequeDate": null,

    }
  }


  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.voucherService.getVoucherReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.voucherReportHeader();
      let reportItem: any[] = this.voucherReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 22) {
        reportName = "Cash Voucher Report"
      }
      if (this.transactionType.id === 23) {
        reportName = "Cheque Voucher Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });



  }


  voucherReportHeader() {
    if (this.isCheque) {
      return [
        {
          "SL No": "SL No",
          "VoucherNumber": "VoucherNumber",
          "VoucherDate": "VoucherDate",
          "Expenses": "Expenses",
          "PaidTo": "PaidTo",
          "Description": "Description",
          "Amount": "Amount",
          "BankName": "BankName",
          "ChequeNumber": "ChequeNumber",
          "ChequeDate": "ChequeDate",

        },



      ];



    }



    else {
      return [
        {
          "SL No": "SL No",
          "VoucherNumber": "VoucherNumber",
          "VoucherDate": "VoucherDate",
          "Expenses": "Expenses",
          "PaidTo": "PaidTo",
          "Description": "Description",
          "Amount": "Amount",



        },



      ];


    }
  }




  voucherReportBody(invoiceReport: any[]) {
    let count: number = 0

    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    return reportItem;





  }


  makeReportItem(item: VoucherReportModel, count: number): any {
    if (this.isCheque) {
      return {
        "SL No": count,
        "VoucherNumber": item.voucherNumber,
        "VoucherDate": this.datePipe.transform(item.voucherDate, 'dd-MM-yyyy'),
        "Expenses": item.expenseName,
        "PaidTo": item.paidTo,
        "Description": item.description,
        "Amount": item.amount,
        "BankName": item.bankName,
        "ChequeNumber": item.chequeNumber,
        "ChequeDate": this.datePipe.transform(item.chequeDate, 'dd-MM-yyyy'),

      }
    }
    else {
      return {
        "SL No": count,
        "VoucherNumber": item.voucherNumber,
        "VoucherDate": this.datePipe.transform(item.voucherDate, 'dd-MM-yyyy'),
        "Expenses": item.expenseName,
        "PaidTo": item.paidTo,
        "Description": item.description,
        "Amount": item.amount,
      }
    }
  }


}


export class VoucherReportHttpDao {

  constructor(private voucherService: VoucherService) { }

  getVouchers(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<VoucherReportModel>> {

    // pageSize = pageSize ? pageSize : PAGE_SIZE;
    // //console.log('filterText: ', filterText);
    //console.log('page: ', page);

    //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);

    return this.voucherService.getVoucherReport(transactionType, filterForm.value, page);
  }
}



export interface ExportDataType {
  "SL No": number;
  "VoucherNumber": string,
  "VoucherDate": string,
  "Expenses": string,
  "PaidTo": string,
  "Description": string,
  "Amount": number,
  "BankName": string,
  "ChequeNumber": string,
  "ChequeDate": string,

}