import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Material } from '../../data-model/material-model';
import { StockTraceModel, StockTraceRequest } from '../../data-model/stock-trace-model';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { PdfService } from '../../services/pdf.service';
import { StockTraceService } from '../../services/stock-trace.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';

@Component({
  selector: 'app-stock-trace-new',
  templateUrl: './stock-trace-new.component.html',
  styleUrls: ['./stock-trace-new.component.scss']
})
export class StockTraceNewComponent implements  AfterViewInit, OnDestroy {
  displayedColumns = [ 'businessDate','transactionDate','materialName','transactionNote','transactionNumber','partyName','quantityIn','quantityOut','closingStock'];
  subheading: string;
  dataSource = new MatTableDataSource<StockTraceModel>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  private stockTraceRequest: StockTraceRequest = { fromDate: null, toDate: null, materialId: null };
  public materials: Material[] = [];

  stockTraceDatabase: StockTraceHttpDao | null;
  resultsLength = 0;
   isLoadingResults = true;
   pageSize: number=10;
   page = new Page();
   searchButton$ = new Subject<string>();
   onDestroy: Subject<boolean> = new Subject();
   stockTraceForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    materialId: new FormControl()
  });
   constructor(private stockTraceService: StockTraceService
    , private datePipe: DatePipe,
    private _router: Router,
    public route: ActivatedRoute,
    private excelService: ExcelService,
    private materialService: MaterialService,
    public userService: UserService,
    private pdfService: PdfService) {
 
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

  ngAfterViewInit() {


    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.stockTraceDatabase = new StockTraceHttpDao(this.stockTraceService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

     this.getDateRange();
    this.getData();
    this.getMaterial();
    
  
  }
  
  getData() {
 
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {

              
          let fromDate = this.stockTraceForm.get('fromDate').value;
          let toDate = this.stockTraceForm.get('toDate').value;
          let materialId = this.stockTraceForm.get('materialId').value;

          ////console.log('from date: ',fromDate, this.datePipe.transform(fromDate, 'dd/MM/yyyy'));
          ////console.log('from date: ',this.datePipe.transform(this.datePipe.transform(fromDate, 'MM/dd/yyyy'),'yyyy-MM-dd'));

          this.stockTraceRequest.fromDate = this.datePipe.transform(fromDate, AppSettings.DATE_FORMAT);  //fromDate | date:'yyyy-MM-dd';
          this.stockTraceRequest.toDate = this.datePipe.transform(toDate, AppSettings.DATE_FORMAT);//toDate;
          this.stockTraceRequest.materialId = materialId;
          return this.stockTraceDatabase!.getReport(this.stockTraceRequest);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
           this.resultsLength = data.length;
           console.log('data.materialList: ', data);

          return data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }
 
  onSearch() {
    
    this.searchButton$.next('Search')
  }
  getDateRange() {
    var date = new Date();
    let dateOfMonth = date.getDate();
    //console.log("date", date.getDate());

    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    if (dateOfMonth < 5) {
      firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
      lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);

    }
    this.stockTraceForm.get('fromDate').patchValue(firstDay);
    this.stockTraceForm.get('toDate').patchValue(lastDay);

  }
  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }
  close() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }

  getMaterial() {
    this.materialService.getAllMaterials()
      .subscribe(response => this.materials = response.filter(m => m.materialTypeName !== AppSettings.MATERIAL_TYPE_JOBWORK));
  }


  saveAsExcel(): void {
    let fileName: string = 'Stock-trace';
    let excelData: any[] = [];
    let reports: ExportDataType[] = [];


    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe(pagedData => {
      excelData = pagedData;
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

    
      this.excelService.exportFromJSON(reports, fileName);

    });



  }












  saveAsPdf(): void {
    let pdfPage = { ...this.page };

    let reportObject: any;
    let filterText = "";
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe(pagedData => {
      pdfData = pagedData;

      let reportHeader: any[] = this.stockReportHeader();
      let reportItem: any[] = this.stockReportBody(pdfData);


      this.pdfService.exportToPdf("StockTraceReport", reportHeader, reportItem, "Stock-trace");
    });



  }


  stockReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Business Date": "Business Date",
        "Transaction Date": "Transaction Date",
        "Material": "Material",
        "PartyName": "PartyName",
        "TransactionNote": "TransactionNote",
        "TransactionNumber": "TransactionNumber",
        "InQuantity": "InQuantity",
        "OutQuantity": "OutQuantity",
        "ClosingStock": "ClosingStock",



      },



    ];




  }




  stockReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
   
    return reportItem;





  }


  makeReportItem(item: StockTraceModel, count: number): any {

    return {
      "SL No": count,
      "Business Date": this.datePipe.transform(item.businessDate, AppSettings.DATE_FORMAT),
      "Transaction Date": this.datePipe.transform(item.transactionDate, AppSettings.DATE_FORMAT),
      "Material": item.materialName,
      "PartyName": item.partyName,
      "TransactionNote": item.transactionNote,
      "TransactionNumber": item.transactionNumber,
      "InQuantity": item.quantityIn,
      "OutQuantity": item.quantityOut,
      "ClosingStock": item.closingStock,

    }
  }
  clear() {
     this.stockTraceForm.get('materialId').patchValue(null);
    this.stockTraceForm.get('fromDate').patchValue(null);
    this.stockTraceForm.get('toDate').patchValue(null);
    this.dataSource.data = [];
    this.page.totalElements = 0;
  }


}


export class StockTraceHttpDao {
 
  constructor(private stockTraceService: StockTraceService) { }
   
  getReport(stockTraceRequest : StockTraceRequest): Observable<StockTraceModel[]> {
     
    return  this.stockTraceService.getStockTraceReport(stockTraceRequest);

  }
}



export interface ExportDataType {
  "SL No": number;
  "Business Date": string;
  "Transaction Date": string;
  "Material": string;
  "PartyName": string;
  "TransactionNote": string;
  "TransactionNumber": string;
  "InQuantity": number;
  "OutQuantity": number;
  "ClosingStock": number;

}