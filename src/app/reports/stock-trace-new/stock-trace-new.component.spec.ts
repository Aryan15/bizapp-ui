import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTraceNewComponent } from './stock-trace-new.component';

describe('StockTraceNewComponent', () => {
  let component: StockTraceNewComponent;
  let fixture: ComponentFixture<StockTraceNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockTraceNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTraceNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
