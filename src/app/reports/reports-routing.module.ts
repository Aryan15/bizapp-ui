import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartComponent } from '../graphical-reports/charts/ngx-chart.component';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { BalanceReportComponent } from './balance-report/balance-report.component';
import { DcDetailedReportComponent } from './dc-detailed-report/dc-detailed-report.component';
import { GstReportComponent } from './gst-report/gst-report.component';
import { InvoiceItemReportNewComponent } from './invoice-item-report-new/invoice-item-report-new.component';
import { InvoiceSummaryReportNewComponent } from './invoice-summary-report-new/invoice-summary-report-new.component';
import { JrContainerComponent } from './jr-container/jr-container.component';
import { MaterialReportComponent } from './material-report/material-report.component';
import { PartyDetailedReportComponent } from './party-detailed-report/party-detailed-report.component';
import { PayableReceivableReportComponent } from './payable-receivable-report/payable-receivable-report.component';
import { PoReportComponent } from './po-report/po-report.component';
import { StockTraceComponent } from './stock-trace/stock-trace.component';
import { TaxReportComponent } from './tax-report/tax-report.component';
import { VoucherReportComponent } from './voucher-report/voucher-report.component';
import { TaxReportNewComponent } from './tax-report-new/tax-report-new.component';
import { QuotationSummaryReportComponent } from './quotation-summary-report/quotation-summary-report.component';
import { PettyCashReportComponent } from './petty-cash-report/petty-cash-report.component';
import { HelpVideosComponent } from '../help-videos/help-videos.component';
import { InvoiceItemReportComponent } from './invoice-item-report/invoice-item-report.component';
import { PoBalanceReportComponent } from './po-balance-report/po-balance-report.component';

import { PriceListReportComponent } from './price-list-report/price-list-report.component';
import { EwayBillReportComponent } from './eway-bill-report/eway-bill-report.component';




const routes: Routes = [
  {
    path: 'app-invoice-item-report-new/:trantypeId',
    component: InvoiceItemReportNewComponent,
    data: { state: 'app-invoice-item-report-new' }
  },

  {
    path: 'app-payable-receivable-report/:trantypeId',
    component: PayableReceivableReportComponent,
    data: { state: 'app-payable-receivable-report' }
  },
  {
    path: 'app-po-report/:trantypeId',
    component: PoReportComponent,
    data: { state: 'app-po-report' }
  },
  
  {
    path: 'app-material-report',
    component: MaterialReportComponent,
    data: { state: 'app-material-report' }
  },
  {
    path: 'stock-trace',
    component: StockTraceComponent,
    data: { state: 'stock-trace' },
  },
  
  {
    path: 'app-dc-detailed-report/:trantypeId',
    component: DcDetailedReportComponent,
    data: { state: 'app-dc-detailed-report' }
  },
  {
    path: 'app-party-detailed-report/:trantypeId',
    component:  PartyDetailedReportComponent,
    data: { state: 'app-party-detailed-report' }
  },
  {
    path: 'app-balance-report/:trantypeId',
    component: BalanceReportComponent,
    data: { state: 'app-balance-report' }
  },
  {
    path: 'app-balance-report/:trantypeId',
    component: BalanceReportComponent,
    data: { state: 'app-balance-report' }
  },
  {
    path: 'app-invoice-summary-report/:trantypeId',
    component: InvoiceSummaryReportNewComponent,
    data: { state: 'app-invoice-summary-report' }
  },
  // {
  //   path: 'app-tax-report-new/:trantypeId',
  //   component: TaxReportNewComponent,
  //   data: { state: 'app-tax-report-new'}
  // },
  {
    path: 'app-tax-report/:trantypeId',
    component: TaxReportComponent,
    data: { state: 'app-tax-report'}
  },
  { path: 'app-ngx-chart',
    component: NgxChartComponent, 
    data: { state: 'app-ngx-chart' }
  },
 {
   path:'app-voucher-report/:trantypeId',
   component: VoucherReportComponent,
   data: {state: 'app-voucher-report'}
 },
  {
    path:'app-gst-report/:trantypeId',
    component: GstReportComponent,
    data: {state: 'GstReportComponent'}
  },
  {
    path:'app-about-company',
    component: AboutCompanyComponent,
    data: {state: 'app-about-company'}
  },
  {
    path:'jr-container/:trantypeId/:counterTTypeId/:jasperReportName',
    component: JrContainerComponent,
    data: {state: 'jr-container'}
  },
  {
    path:'app-quotation-summary-report/:trantypeId',
    component: QuotationSummaryReportComponent,
    data: {state: 'app-quotation-summary-report'}
  },

  {
    path:'app-petty-cash-report/:trantypeId',
    component: PettyCashReportComponent,
    data: {state: 'app-petty-cash-report'}
  },
  {
    path:'app-help-videos',
    component: HelpVideosComponent,
    data: {state: 'app-help-videos'}
  },
  {
    path:'app-po-balance-report/:trantypeId',
    component: PoBalanceReportComponent,
    data: {state: 'app-po-balance-report'}
  },

  {
    path:'app-price-list-report',
    component: PriceListReportComponent,
    data: {state: 'app-price-list-report'}
  },

  
  {
    path:'app-eway-bill-report',
    component: EwayBillReportComponent,
    data: {state: 'app-eway-bill-report'}
  }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
