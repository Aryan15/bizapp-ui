import { Component, OnDestroy, AfterViewInit, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs';
import { StockTraceModel, StockTraceRequest, StockTraceReport } from '../../data-model/stock-trace-model';
import { Material } from '../../data-model/material-model';
import { StockTraceService } from '../../services/stock-trace.service';
import { MaterialService } from '../../services/material.service';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';

import { AppSettings } from '../../app.settings';
import { ExcelService } from '../../services/excel.service';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PartyService } from '../../services/party.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { PdfService } from '../../services/pdf.service';
@Component({
  selector: 'stock-trace',
  templateUrl: 'stock-trace.component.html',
  styleUrls: ['stock-trace.component.scss'],
})
export class StockTraceComponent {

  subheading: string;
  page = new Page();
  rows = [];
  private stockTraceRequest: StockTraceRequest = { fromDate: null, toDate: null, materialId: null };
  public materials: Material[] = [];
  public dataSource: DataSource<any>;
  reorderable: true;
  stockTraceForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    materialId: new FormControl()
  });

  constructor(private stockTraceService: StockTraceService
    , private datePipe: DatePipe,
    private _router: Router,
    public route: ActivatedRoute,
    private excelService: ExcelService,
    private materialService: MaterialService,
    public userService: UserService,
    private pdfService: PdfService) {

    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

  ngOnInit() {
    this.getDateRange();
    this.getMaterial();
    this.onSearch();

  }

  getMaterial() {
    this.materialService.getAllMaterials()
      .subscribe(response => this.materials = response.filter(m => m.materialTypeName !== AppSettings.MATERIAL_TYPE_JOBWORK));
  }

  close() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }

  // displayedColumns = ['businessDate', 'transactionDate', 'materialName', 'transactionNote', 'partyName', 'quantityIn', 'quantityOut', 'closingStock'];
  //displayedColumns = [ 'transactionDate', 'materialName', 'transactionNote', 'partyId', 'quantityIn', 'quantityOut', 'closingStock']; 

  onSearch() {

    let fromDate = this.stockTraceForm.get('fromDate').value;
    let toDate = this.stockTraceForm.get('toDate').value;
    let materialId = this.stockTraceForm.get('materialId').value;

    ////console.log('from date: ',fromDate, this.datePipe.transform(fromDate, 'dd/MM/yyyy'));
    ////console.log('from date: ',this.datePipe.transform(this.datePipe.transform(fromDate, 'MM/dd/yyyy'),'yyyy-MM-dd'));

    this.stockTraceRequest.fromDate = this.datePipe.transform(fromDate, AppSettings.DATE_FORMAT);  //fromDate | date:'yyyy-MM-dd';
    this.stockTraceRequest.toDate = this.datePipe.transform(toDate, AppSettings.DATE_FORMAT);//toDate;
    this.stockTraceRequest.materialId = materialId;
    //console.log('before search: ', this.stockTraceRequest);
    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe((pagedData) => {
      this.rows = pagedData;

      //console.log("row details", this.rows)
    });
    // this.dataSource = new StockTraceDataSource(this.stockTraceService, this.stockTraceRequest);
    //this.setPage({ offset: 0 });
  }
  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe(pagedData => {
      this.rows = pagedData;

    });

  }

  saveAsExcel(): void {
    let fileName: string = 'Stock-trace';
    let excelData: any[] = [];
    let reports: ExportDataType[] = [];


    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe(pagedData => {
      excelData = pagedData;
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

    
      this.excelService.exportFromJSON(reports, fileName);

    });



  }












  saveAsPdf(): void {
    let pdfPage = { ...this.page };

    let reportObject: any;
    let filterText = "";
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.stockTraceService.getStockTraceReport(this.stockTraceRequest).subscribe(pagedData => {
      pdfData = pagedData;

      let reportHeader: any[] = this.stockReportHeader();
      let reportItem: any[] = this.stockReportBody(pdfData);


      this.pdfService.exportToPdf("StockTraceReport", reportHeader, reportItem, "Stock-trace");
    });



  }


  stockReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Business Date": "Business Date",
        "Transaction Date": "Transaction Date",
        "Material": "Material",
        "MaterialCode": "MaterialCode",
        "PartyName": "PartyName",
        "PartyCode":"PartyCode",
        "TransactionNote": "TransactionNote",
        "TransactionNumber": "TransactionNumber",
        "InQuantity": "InQuantity",
        "OutQuantity": "OutQuantity",
        "ClosingStock": "ClosingStock",



      },



    ];




  }




  stockReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
   
    return reportItem;





  }


  makeReportItem(item: StockTraceModel, count: number): any {

    return {
      "SL No": count,
      "Business Date": this.datePipe.transform(item.businessDate, AppSettings.DATE_FORMAT),
      "Transaction Date": this.datePipe.transform(item.transactionDate, AppSettings.DATE_FORMAT),
      "Material": item.materialName,
      "Material Code":item.materialPartNumber,
      "PartyName": item.partyName,
      "PartyCode":item.partyCode,
      "TransactionNote": item.transactionNote,
      "TransactionNumber": item.transactionNumber,
      "InQuantity": item.quantityIn,
      "OutQuantity": item.quantityOut,
      "ClosingStock": item.closingStock,

    }
  }







  getDateRange() {
    var date = new Date();
    let dateOfMonth = date.getDate();
    //console.log("date", date.getDate());

    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    if (dateOfMonth < 5) {
      firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
      lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);

    }
    this.stockTraceForm.get('fromDate').patchValue(firstDay);
    this.stockTraceForm.get('toDate').patchValue(lastDay);

  }

  clear() {
    this.page.totalElements = 0;
    this.stockTraceForm.get('materialId').patchValue(null);
    this.stockTraceForm.get('fromDate').patchValue(null);
    this.stockTraceForm.get('toDate').patchValue(null);
    this.rows = [];
    this.page.totalElements = 0;
  }


}

// excelData.forEach(data => {
//   //console.log("export data",data)
//   reports.push({
//     businessDate: data.businessDate, invoiceDate: data.transactionDate, materialName: data.materialName, partyName: data.partyName,
//     quantityIn: data.quantityIn, quantityOut: data.quantityOut, closingStock: data.closingStock, transactionNote: data.transactionNote        // name: data.invoiceNumber, invoiceDate: data.invoiceDate, partyName: data.partyName, description: data.description,

//   })
// const data: StockTraceModel[] = [
//   {businessDate : '12/11/2017', transactionDate : '12/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, partyName: 'Tst', quantityIn: 1, quantityOut : 1, closingStock :1 },
//   {businessDate : '15/11/2017', transactionDate : '15/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Sell', transactionTypeId: 1, transactionHeaderId : '', partyId: 1,  partyName: 'Tst', quantityIn: 1, quantityOut : 1, closingStock :1 },
//   {businessDate : '20/11/2017', transactionDate : '20/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, partyName: 'Tst',  quantityIn: 1, quantityOut : 1, closingStock :1 },
//   {businessDate : '22/11/2017', transactionDate : '22/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1,  partyName: 'Tst', quantityIn: 1, quantityOut : 1, closingStock :1 },
// ];

// const data: StockTraceModel[] = [
//   { transactionDate : '12/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, quantityIn: 1, quantityOut : 1, closingStock :1 },
//   { transactionDate : '15/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Sell', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, quantityIn: 1, quantityOut : 1, closingStock :1 },
//   { transactionDate : '20/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, quantityIn: 1, quantityOut : 1, closingStock :1 },
//   { transactionDate : '22/11/2017', materialId :1, materialName: 'Test', transactionNote : 'Purchase', transactionTypeId: 1, transactionHeaderId : '', partyId: 1, quantityIn: 1, quantityOut : 1, closingStock :1 },
// ];

// export class StockTraceDataSource extends DataSource<any> {

//   constructor(private stockTraceService: StockTraceService
//     , private stockTraceRequest: StockTraceRequest) {
//     super();
//   }

//   //stockTraceRequest : StockTraceRequest = {fromDate: '2017-11-01', toDate: '2017-11-30', materialId:1};
//   /** Connect function called by the table to retrieve one stream containing the data to render. */
//   connect(): Observable<StockTraceModel[]> {
//     return this.stockTraceService.getStockTrace(this.stockTraceRequest);//Observable.of(data);
//   }

//   disconnect() { }
// }

export interface ExportDataType {
  "SL No": number;
  "Business Date": string;
  "Transaction Date": string;
  "Material": string;
  "Material Code": string;
  "PartyName": string;
  "PartyCode":string;
  "TransactionNote": string;
  "TransactionNumber": string;
  "InQuantity": number;
  "OutQuantity": number;
  "ClosingStock": number;

}