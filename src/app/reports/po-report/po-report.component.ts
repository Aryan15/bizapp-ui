import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PdfService } from '../../services/pdf.service';
import { PurchaseOrderReport } from '../../data-model/purchase-order-model';
declare var jsPDF: any;
@Component({
  selector: 'app-po-report',
  templateUrl: './po-report.component.html',
  styleUrls: ['./po-report.component.scss'],
})
export class PoReportComponent extends ReportParentComponent implements OnInit {


  subheading: string;
  isLoadingResults = true;
  // page = new Page();
  globalSetting: GlobalSetting;
  isItemLevelTax: boolean;
  buyOrSell: string;


  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private poService: PurchaseOrderService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private numberFormatterService: NumberFormatterService,
    private globalSettingService: GlobalSettingService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    //this.getGlobalSetting();
    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.pageSize = this.page.size;
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }


  ngOnInit() {
    //console.log("pageSize", this.pageSize, this.page.size);
    this.initParent();
    this.initComplete.subscribe(response => {
      this.deriveBuyOrSell();
      this.getGlobalSetting();
      this.onSearch();
    });
  }

  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;

      // this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      if (this.buyOrSell === AppSettings.TXN_SELL) {
        this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      } else {
        this.isItemLevelTax = this.globalSetting.itemLevelTaxPurchase === 1 ? true : false;
      }

    })
  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }



  setPage(pageInfo) {
    this.isLoadingResults = true;
    //console.log("in setPage: ", pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "purchaseOrderDate";

    // this.page.size = AppSettings.REPORT_PAGE_SIZE;
    //console.log('this.filterForm.value: ', this.filterForm.value);

    this.poService.getPoReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;

      //this.rows.push(this.getTotal());
      // this.page.size += 1;
      //console.log("this.rows ", this.rows)

      this.isLoadingResults = false;
      //this.getTotal();
    });
  }


  onSort(event) {
    this.isLoadingResults = true;
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.poService.getPoReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;



      this.isLoadingResults = false;
      //this.getTotal();
    });

  }

  onSearch() {
    if (!this.materialControl.value) {
      this.filterForm.get('materialId').patchValue(null);
    }
    this.setPage({ offset: 0 });
  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name.replace(' ', '_');
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();

    let excelPage = { ...this.page };

    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    //console.log('excelPage: ', excelPage);
    this.poService.getPoReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makePoReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });



  }



  makeSummaryRow(excelData): ExportDataType {
    return {
      "SL No": null,
      "PurchaseOrder Number": "TOTAL",
      "PurchaseOrder Date": null,
      "Status": null,
      "PartyName": null,
      "Material": null,
      "InternalRefNum":null,
      "Quantity": null,
      "UOM": null,
      "Price": excelData.map(item => item.price).reduce((prev, curr) => prev + curr, 0),
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "Discount": excelData.map(item => item.discountAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxPercentage": null,
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxPercentage": null,
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxPercentage": null,
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "TotalAmount": excelData.map(item => item.totalAmount).reduce((prev, curr) => prev + curr, 0),
       "Remarks":null
    }
  }




  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.poService.getPoReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.poReportHeader();
      let reportItem: any[] = this.poReportBody(pdfData);

      let reportName: string
      if (this.transactionType.id === 11) {
        reportName = "Customer PO Detailed Report"
      }
      if (this.transactionType.id === 2) {
        reportName = "Supplier PO Detailed Report"
      }
      if (this.transactionType.id === 20) {
        reportName = "JobWork PO Detailed Report"
      }

      if (this.transactionType.id === 21) {
        reportName = "Subcontract PO Detailed Report"
      }
      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);

    });

  }
  poReportHeader() {
    if (this.isItemLevelTax) {
      return [


        {
          "SL No": "SL No",
          "PurchaseOrder Number": "PurchaseOrder Number",
          "PurchaseOrder Date": "PurchaseOrder Date",
          "Status": "Status",
          "PartyName": "Party Name",
          "Material": "Material",
          "InternalRefNum":"InternalRefNum",
          "Quantity": "Quantity",
          "UOM": "UOM",
          "Price": "Price",
          "Amount": "Amount",
          "Discount": "Discount",
          "CgstTaxPercentage": "Cgst Tax Percentage",
          "CgstTaxAmount": "Cgst Tax Amount",
          "SgstTaxPercentage": "Sgst Tax Percentage",
          "SgstTaxAmount": "Sgst Tax Amount",
          "IgstTaxPercentage": "Igst Tax Percentage",
          "IgstTaxAmount": "Igst Tax Amount",
          "TotalAmount": "Total Amount",
          "Remarks":"Remarks"
        },



      ]


    }


    else {

      return [


        {
          "SL No": "SL No",
          "PurchaseOrder Number": "PurchaseOrder Number",
          "PurchaseOrder Date": "PurchaseOrder Date",
          "Status": "Status",
          "PartyName": "Party Name",
          "Material": "Material",
          "InternalRefNum":"InternalRefNum",
          "Quantity": "Quantity",
          "UOM": "UOM",
          "Price": "Price",
          "Amount": "Amount",
          "TotalAmount": "Total Amount",
          "Remarks":"Remarks"

        },



      ]


    }


  }
  poReportBody(pdfData: any[]) {
    let reportItem: ExportDataType[] = [];
    let count: number = 0
    pdfData.forEach((po, index) => {

      reportItem.push(this.makePoReportItem(po, index + 1));
    });
    let summaryRow: ExportDataType = this.makeSummaryRow(pdfData);
    reportItem.push(summaryRow);

    return reportItem;

  }

  makePoReportItem(item: PurchaseOrderReport, count: number): any {

    if (this.isItemLevelTax) {
      return {
        "SL No": count,
        "PurchaseOrder Number": item.purchaseOrderNumber,
        "PurchaseOrder Date": this.datePipe.transform(item.purchaseOrderDate, 'dd-MM-yyyy'),
        "Status": item.statusName,
        "PartyName": item.customerName,
        "Material": item.materialName,
        "InternalRefNum":item.internalReferenceNumber,
        "Quantity": item.quantity,
        "UOM": item.unitOfMeasurementName,
        "Price": item.price,
        "Amount": item.amount,
        "Discount": item.discountAmount,
        "CgstTaxPercentage": item.cgstTaxPercentage,
        "CgstTaxAmount": item.cgstTaxAmount,
        "SgstTaxPercentage": item.sgstTaxPercentage,
        "SgstTaxAmount": item.sgstTaxAmount,
        "IgstTaxPercentage": item.igstTaxPercentage,
        "IgstTaxAmount": item.igstTaxAmount,
        "TotalAmount": item.totalAmount,
        "Remarks":item.remarks
      }
    }

    else {

      return {
        // "slNo": quo.,
        "SL No": count,
        "PurchaseOrder Number": item.purchaseOrderNumber,
        "PurchaseOrder Date": this.datePipe.transform(item.purchaseOrderDate, 'dd-MM-yyyy'),
        "Status": item.statusName,
        "PartyName": item.customerName,
        "Material": item.materialName,
        "InternalRefNum":item.internalReferenceNumber,
        "Quantity": item.quantity,
        "UOM": item.unitOfMeasurementName,
        "Price": item.price,
        "Amount": item.amount,
        "TotalAmount": item.totalAmount,
        "Remarks":item.remarks
      }

    }

  }


















  setPageAfterPageLimit() {
    //console.log("this.pageSize" + this.pageSize);
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }


  deriveBuyOrSell() {
    let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_INVOICE];
    let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE];
    if (sellTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_SELL;
    } else if (buyTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_PURCHASE;
    }

  }

}


export interface ExportDataType {
  "SL No": number;
  "PurchaseOrder Number": string;
  "PurchaseOrder Date": string;
  "Status": string;
  "PartyName": string;
  "Material": string;
  "InternalRefNum":string,
  "Quantity": number;
  "UOM": string;
  "Price": number;
  "Amount": number;
  "Discount": number;
  "CgstTaxPercentage": number;
  "CgstTaxAmount": number;
  "SgstTaxPercentage": number;
  "SgstTaxAmount": number;
  "IgstTaxPercentage": number;
  "IgstTaxAmount": number;
  "TotalAmount": number;
  "Remarks":string;

}
