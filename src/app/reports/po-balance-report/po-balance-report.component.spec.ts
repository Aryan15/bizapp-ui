import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoBalanceReportComponent } from './po-balance-report.component';

describe('PoBalanceReportComponent', () => {
  let component: PoBalanceReportComponent;
  let fixture: ComponentFixture<PoBalanceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoBalanceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoBalanceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
