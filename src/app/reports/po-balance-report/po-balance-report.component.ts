import { Component, OnInit } from '@angular/core';
import { FinancialYearService } from '../../services/financial-year.service';
import { PartyService } from '../../services/party.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { PayableReceivableService } from '../../services/payable-receivable.service';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { DataSource } from '@angular/cdk/table';
import { AppSettings } from '../../app.settings';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { PoBalanceReport } from '../../data-model/purchase-order-model';

@Component({
  selector: 'app-po-balance-report',
  templateUrl: './po-balance-report.component.html',
  styleUrls: ['./po-balance-report.component.scss']
})
export class PoBalanceReportComponent extends ReportParentComponent implements OnInit {
  isLoadingResults = true;
  subheading: string;
  public dataSource: DataSource<any>;
  mainPartyType: string = "customer";
  invoiceTypeId: number;
  reportName:string;
  
  // reportHeading: string= '';
  // reportName: string = '';
  // public statuss: status[] = [
  //   { id: 2, name: "New" },
  //   { id: 6, name: "Completed" },

  // ]
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private poService: PurchaseOrderService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService

    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }

  ngOnInit() {
    this.initParent();
    this.initComplete.subscribe(response => {
      this.getDateRange();

      this.onSearch();
    });
  }
  getDateRange() {
    let date = new Date();
    let dateOfMonth = date.getDate();
    //console.log("date", dateOfMonth ,date );
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    // console.log ("1 firstDay  lastDay ",firstDay ,lastDay)
    // if (dateOfMonth < 5) {
    firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);
    // console.log ("2 firstDay  lastDay ",firstDay ,lastDay)
    // }

    //  console.log ("3 firstDay  lastDay ",firstDay ,lastDay)
    this.filterForm.get('transactionFromDate').patchValue(firstDay);
    this.filterForm.get('transactionToDate').patchValue(lastDay);

  }

  onSearch() {
    this.setPage({ offset: 0 });

  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;
    this.page.size=500;
    this.poService.getPoBalanceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data.filter(r => r.invoicePendingQuantity != 0.0);
    });

  }




  setPage(pageInfo) {
    //console.log("in setPage: ",pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "purchaseOrderDate";
   this.page.size=500;
    this.poService.getPoBalanceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data.filter(r => r.invoicePendingQuantity != 0.0);
    });
  }

  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }
  saveAsExcel(): void {
    let reportName: string ;
    if (this.transactionType.id === 11) {
      reportName = "Sales Po Balance Report"
    }
    if (this.transactionType.id === 2) {
      reportName = "Purchase Po Balance Report"
    }

    if (this.transactionType.id === 20) {
      reportName = "Jobwork Po Balance Report"
    }

    if (this.transactionType.id === 21) {
      reportName = "Subcontract Po Balance Report"
    }
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };

    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    //console.log('excelPage: ', excelPage);
    this.poService.getPoBalanceReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data.filter(r => r.invoicePendingQuantity != 0.0);
      //console.log("this.rows ", excelData);

      let reports: ExportDataType[] = [];
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })
      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, reportName);

    });
  }


  public saveAsPdf(): void {

    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.poService.getPoBalanceReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data.filter(r => r.invoicePendingQuantity != 0.0);

      let reportHeader: any[] = this.balanceReportHeader();
      let reportItem: any[] = this.balanceReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 11) {
        reportName = "Sales Po Balance Report"
      }
      if (this.transactionType.id === 2) {
        reportName = "Purchase Po Balance Report"
      }

      if (this.transactionType.id === 20) {
        reportName = "Jobwork Po Balance Report"
      }

      if (this.transactionType.id === 21) {
        reportName = "Subcontract Po Balance Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, reportName);
    });
  }

  makeReportItem(item: PoBalanceReport, count: number): any {

    return {
      "SL No": count,
      "Po Number": item.poNumber,
      "Po Date": this.datePipe.transform(item.purchaseOrderDate, 'dd-MM-yyyy'),
      "Party Name": item.partyName,
      "Internel Ref Number" :item.internelRefNumber,
      //"Status": item.statusName,
      "MaterialName": item.materialName,
     "Po Quantity": item.poQuantity,
      "Price": item.price,
      "Dc Balance Quantity": item.dcPendingQuantity,
      "Invoice Balance Quantity": item.invoicePendingQuantity,
      "Dc Balance Amount": item.dcBalanceAmount,
      "Invoice Balance Amount": item.invoiceBalanceAmount
    }
  }
  makeSummaryRow(excelData): ExportDataType {

    return {
      "SL No": null,
      "Po Number": "Total",
      "Po Date": null,
      "Party Name": null,
      "Internel Ref Number":null,
      //"Status": null,
      "MaterialName": null,
      "Po Quantity": excelData.map(item => item.poQuantity).reduce((prev, curr) => prev + curr, 0),
      "Price": excelData.map(item => item.price).reduce((prev, curr) => prev + curr, 0),
      "Dc Balance Quantity": excelData.map(item => item.dcPendingQuantity).reduce((prev, curr) => prev + curr, 0),
      "Invoice Balance Quantity": excelData.map(item => item.invoicePendingQuantity).reduce((prev, curr) => prev + curr, 0),
      "Dc Balance Amount": excelData.map(item => item.dcBalanceAmount).reduce((prev, curr) => prev + curr, 0),
      "Invoice Balance Amount": excelData.map(item => item.invoiceBalanceAmount).reduce((prev, curr) => prev + curr, 0),
    }
  }

  balanceReportHeader() {
    return [
      {

        "SL No": "SL No",
        "Po Number": "Po Number",
        "Po Date": "Po Date",
        "Party Name": "Party Name",
        "Internel Ref Number" :"Internel Ref Number",
        //"Status": "Status",
        "MaterialName": "Material Name",
        "Po Quantity": "Po Quantity",
        "Price": "Price",
        "Dc Balance Quantity": "Dc Balance Quantity",
        "Invoice Balance Quantity": "Invoice Balance Quantity",
        "Dc Balance Amount": "Dc Balance Amount",
        "Invoice Balance Amount": "Invoice Balance Amount",


      }







    ];




  }

  balanceReportBody(poReport: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    poReport.forEach((po, index) => {
      reportItem.push(this.makeReportItem(po, index + 1));
    });

    let summaryRow: ExportDataType = this.makeSummaryRow(poReport);
    reportItem.push(summaryRow);
    return reportItem;
  }

}

export interface ExportDataType {
  "SL No": number;
  "Po Number": string;
  "Po Date": string;
  "Party Name": string;
  "Internel Ref Number":string;
  //"Status": string;
  "MaterialName": string;
  "Po Quantity": number;
  "Price": number;
  "Dc Balance Quantity": number;
  "Invoice Balance Quantity": number;
  "Dc Balance Amount": number;
  "Invoice Balance Amount": number;

}
