import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { ReportParentComponent } from '../report-parent/report-parent.component';
//import { Subject, Observable, merge } from 'rxjs';
import { Observable, Subject, of as observableOf, merge } from 'rxjs';
import { QuotationSummaryReport } from '../../data-model/quotation-model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { QuotationService } from '../../services/quotation.service';
import { FormGroup } from '@angular/forms';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { TransactionType } from '../../data-model/transaction-type';
import { FinancialYear } from '../../data-model/financial-year-model';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalSettingService } from '../../services/global-setting.service';
import { PartyService } from '../../services/party.service';
import { MaterialService } from '../../services/material.service';
import { DatePipe } from '@angular/common';
import { ExcelService } from '../../services/excel.service';
import { UserService } from '../../services/user.service';
import { AppSettings } from '../../app.settings';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Party } from '../../data-model/party-model';
import { Material } from '../../data-model/material-model';
import { PdfService } from '../../services/pdf.service';

@Component({
  selector: 'app-quotation-summary-report',
  templateUrl: './quotation-summary-report.component.html',
  styleUrls: ['./quotation-summary-report.component.scss']
})
export class QuotationSummaryReportComponent extends ReportParentComponent implements AfterViewInit, OnDestroy {
  subheading: string;
  searchButton$ = new Subject<string>();
  onDestroy: Subject<boolean> = new Subject();
  displayedColumns = [];
  dataSource = new MatTableDataSource<QuotationSummaryReport>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  quotationDatabase: QuotationReportHttpDao | null;
  transactionType: TransactionType;
  financialYears: FinancialYear[] = [];
  partyPlaceHolder: string;
  isLoadingResults = true;
  isCustomer: boolean = false;
  isSupplier: boolean = false;
  page = new Page();
  resultsLength = 0;
  isItemLevelTax: boolean;
  globalSetting: GlobalSetting;
  isQuotation: boolean;
  buyOrSell: string;


  constructor(private quotationService: QuotationService
    , private transactionTypeService_in: TransactionTypeService
    , private financialYearService_in: FinancialYearService
    , public route_in: ActivatedRoute
    , private globalSettingService: GlobalSettingService
    , protected partyService_in: PartyService
    , private materialService_in: MaterialService
    , private _router_in: Router
    , private datePipe: DatePipe
    , private excelService: ExcelService
    , private userService: UserService
    , private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );

    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    this.getUsers();
  }
  ngAfterViewInit() {
    this.initParent();
    this.dataSource.sort = this.sort;

    this.quotationDatabase = new QuotationReportHttpDao(this.quotationService);

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.initComplete.subscribe(response => {
      if (this.transactionType.name === AppSettings.CUSTOMER_QUOTATION) {
        this.isQuotation = true;
      } else {
        this.isQuotation = false;
      }

      this.deriveBuyOrSell();
      this.getGlobalSetting();
      this.getData();
      this.getDisplaycol();
    });

  }
  getDisplaycol() {
    if (this.isItemLevelTax) {
      //console.log("in")
      this.displayedColumns = ['quotationNumber'
        , 'quotationDate'

        , 'statusName'
        , 'partyName'
        , 'inclusiveTax'
        , 'amount'
        , 'discountAmount'
        , 'totalTaxableAmount'
        , 'cgstTaxAmount'
        , 'sgstTaxAmount'
        , 'igstTaxAmount'
        , 'grandTotal'
      ];
    }
    else {
      //console.log("else")
      this.displayedColumns = ['quotationNumber'
        , 'quotationDate'

        , 'statusName'
        , 'partyName'
        , 'inclusiveTax'
        , 'amount'
        , 'discountAmount'
        , 'totalTaxableAmount'
        , 'cgstTaxRate'
        , 'cgstTaxAmount'
        , 'sgstTaxRate'
        , 'sgstTaxAmount'
        , 'igstTaxRate'
        , 'igstTaxAmount'
        , 'grandTotal'
      ];
    }


  }



  getGlobalSetting() {
    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;


      if (this.buyOrSell === AppSettings.CUSTOMER_QUOTATION) {
        this.isItemLevelTax = this.globalSetting.itemLevelTax === 1 ? true : false;
      }
    })
  }
  deriveBuyOrSell() {


    let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_INVOICE];
    let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE];
    if (sellTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.CUSTOMER_QUOTATION;
    }


  }


  getData() {
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "quotationId";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          console.log(this.transactionType.id + "this.transactionType.id")
          return this.quotationDatabase!.getQuotation(this.transactionType.id, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
          // this.page = data.page;
          console.log('data.data: ', data.data);
          //console.log('data.page: ', this.page);
          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ', error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        console.log("resultsLength: " + this.resultsLength, this.dataSource.data);

      });
  }

  onSearch() {
    this.searchButton$.next('Search')
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }

  close() {
    this._router_in.navigate(['/']);
  }

  saveAsExcel(): void {
    let fileName: string = 'test';
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();

    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;

    this.quotationService.getQuotationReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });




  }



  makeSummaryRow(excelData): ExportDataType {




    return {
      "SL No": null,
      "Quotation Number": "TOTAL",
      "Quotation Date": null,
      "Party Name": null,
      "Status": null,
      "InclusiveTax": null,
      "Amount": excelData.map(item => item.amount).reduce((prev, curr) => prev + curr, 0),
      "Discount": null,
      "AmountAfterDiscount": excelData.map(item => item.totalTaxableAmount).reduce((prev, curr) => prev + curr, 0),
      "CgstTaxAmount": excelData.map(item => item.cgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "SgstTaxAmount": excelData.map(item => item.sgstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "IgstTaxAmount": excelData.map(item => item.igstTaxAmount).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0),

    }
  }







  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.quotationService.getQuotationReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.quotationReportHeader();
      let reportItem: any[] = this.quotationReportBody(pdfData);


      this.pdfService.exportToPdf("Quotation Report", reportHeader, reportItem, this.transactionType.name);
    });



  }


  quotationReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Quotation Number": "Quotation Number",
        "Quotation Date": "Quotation Date",
        "Party Name": "Party Name",
        "Status": "Status",
        "InclusiveTax": "InclusiveTax",
        "Amount": "Amount",
        "Discount": "Discount",
        "AmountAfterDiscount": "Amount After Discount",
        "CgstTaxAmount": "Cgst Tax Amount",
        "SgstTaxAmount": "Sgst Tax Amount",
        "IgstTaxAmount": "Igst Tax Amount",
        "GrandTotal": "Grand Total",


      },



    ];




  }




  quotationReportBody(invoiceReport: any[]) {
    let count: number = 0


    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);
    return reportItem;





  }


  makeReportItem(item: QuotationSummaryReport, count: number): any {
    return {
      "SL No": count,
      "Quotation Number": item.quotationNumber,
      "Quotation Date": this.datePipe.transform(item.quotationDate, 'dd-MM-yyyy'),
      "Party Name": item.partyName,
      "Status": item.statusName,
      "InclusiveTax": item.inclusiveTax ? 'Yes' : 'No',
      "Amount": item.amount,
      "Discount ": item.discountAmount,
      "AmountAfterDiscount": item.totalTaxableAmount,
      "CgstTaxAmount": item.cgstTaxAmount,
      "SgstTaxAmount": item.sgstTaxAmount,
      "IgstTaxAmount": item.igstTaxAmount,
      "GrandTotal": item.grandTotal,
    }
  }











  clear() {

    // this.filterForm.get('financialYearId').patchValue(null);
    // this.filterForm.get('partyId').patchValue(null);
    // this.filterForm.get('materialId').patchValue(null);
    // this.filterForm.get('transactionFromDate').patchValue(null);
    // this.filterForm.get('transactionToDate').patchValue(null);
    // // this.searchButton$.next('Search');
    // this.materialControl.patchValue(null);
    // this.partyControl.patchValue(null);
    super.clear();
    this.dataSource.data = [];

  }

  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }

  getTotalAmount() {
    return this.dataSource.data
      // .filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.amount).reduce((acc, value) => acc + value, 0);
  }
  getTotalTaxableAmount() {
    return this.dataSource.data
      //.filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.totalTaxableAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalDiscountAmount() {
    return this.dataSource.data
      // .filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.discountAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalCgstTaxAmount() {
    return this.dataSource.data
      //.filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.cgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalSgstTaxAmount() {
    return this.dataSource.data
      //.filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.sgstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalIgstTaxAmount() {
    return this.dataSource.data
      // .filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.igstTaxAmount).reduce((acc, value) => acc + value, 0);
  }

  getTotalGrandTotal() {
    return this.dataSource.data
      //.filter(quo => quo.statusName !== AppSettings.INVOICE_CANCEL_STATUS)
      .map(t => t.grandTotal).reduce((acc, value) => acc + value, 0);
  }



}
// ngOnInit() {
// }


export class QuotationReportHttpDao {

  constructor(private quotationService: QuotationService) { }

  getQuotation(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<QuotationSummaryReport>> {

    // pageSize = pageSize ? pageSize : PAGE_SIZE;
    // //console.log('filterText: ', filterText);
    //console.log('page: ', page);

    //return this.invoiceService.getRecentInvoices(transactionType, filterText, sortColumn, sortDirection, page, pageSize);

    return this.quotationService.getQuotationReport(transactionType, filterForm.value, page);
  }
}

export interface ExportDataType {
  "SL No": number;
  "Quotation Number": string;
  "Quotation Date": string;
  "Party Name": string;
  "Status": string;
  "InclusiveTax": string;
  "Amount": number;
  "Discount": number;
  "AmountAfterDiscount": number;
  "CgstTaxAmount": number;
  "SgstTaxAmount": number;
  "IgstTaxAmount": number;
  "GrandTotal": number;




}