import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationSummaryReportComponent } from './quotation-summary-report.component';

describe('QuotationSummaryReportComponent', () => {
  let component: QuotationSummaryReportComponent;
  let fixture: ComponentFixture<QuotationSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotationSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotationSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
