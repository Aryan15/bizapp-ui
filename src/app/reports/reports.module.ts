import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { MaterialReportComponent } from './material-report/material-report.component';
import { DcDetailedReportComponent } from './dc-detailed-report/dc-detailed-report.component';
import { TaxReportComponent } from './tax-report/tax-report.component';
import { PayableReceivableReportComponent } from './payable-receivable-report/payable-receivable-report.component';
import { BalanceReportComponent } from './balance-report/balance-report.component';
import { InvoiceItemReportComponent } from './invoice-item-report/invoice-item-report.component';
import { InvoiceSummaryReportComponent } from './invoice-summary-report/invoice-summary-report.component';
import { StockTraceComponent } from './stock-trace/stock-trace.component';
import { PoReportComponent } from './po-report/po-report.component';
import { PartyDetailedReportComponent } from './party-detailed-report/party-detailed-report.component';
import { SharedModule } from '../shared/shared.module';
//import { DashboardComponent } from '../dashboard/dashboard.component';
import { NgxChartComponent } from '../graphical-reports/charts/ngx-chart.component';
import { GstReportComponent } from './gst-report/gst-report.component';
import { InvoiceItemReportNewComponent } from './invoice-item-report-new/invoice-item-report-new.component';
import { ReportParentComponent } from './report-parent/report-parent.component';
import { VoucherReportComponent } from './voucher-report/voucher-report.component';
import { InvoiceSummaryReportNewComponent } from './invoice-summary-report-new/invoice-summary-report-new.component';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { JrContainerComponent } from './jr-container/jr-container.component';
import { TaxReportNewComponent } from './tax-report-new/tax-report-new.component';
import { TallyLogComponent } from './invoice-item-report-new/tally-log';
import { QuotationSummaryReportComponent } from './quotation-summary-report/quotation-summary-report.component';
import { PettyCashReportComponent } from './petty-cash-report/petty-cash-report.component';



import { BalanceReportNewComponent } from './balance-report-new/balance-report-new.component';
import { PartyDetailedReportNewComponent } from './party-detailed-report-new/party-detailed-report-new.component';
import { DcDetailedReportNewComponent } from './dc-detailed-report-new/dc-detailed-report-new.component';
import { PoReportNewComponent } from './po-report-new/po-report-new.component';
import { PayableReceivableReportNewComponent } from './payable-receivable-report-new/payable-receivable-report-new.component';
import { MaterialReportNewComponent } from './material-report-new/material-report-new.component';
import { StockTraceNewComponent } from './stock-trace-new/stock-trace-new.component';
  // import { NestedTable } from './nested-table/nested-table';
import { PoBalanceReportComponent } from './po-balance-report/po-balance-report.component';

import { PriceListReportComponent } from './price-list-report/price-list-report.component';
import { EwayBillReportComponent } from './eway-bill-report/eway-bill-report.component';


// import { NestedTable } from './nested-table/nested-table';


@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
  ],
  declarations: [
    MaterialReportComponent,
    DcDetailedReportComponent,
    TaxReportComponent,
    PayableReceivableReportComponent,
    BalanceReportComponent,
    InvoiceItemReportComponent,
    InvoiceSummaryReportComponent,
    StockTraceComponent,
    PoReportComponent,
    PartyDetailedReportComponent,
    //DashboardComponent,
    NgxChartComponent,
    GstReportComponent,
    InvoiceItemReportNewComponent,
    ReportParentComponent,
    VoucherReportComponent,
    InvoiceSummaryReportNewComponent,
    AboutCompanyComponent,
    JrContainerComponent,
    TaxReportNewComponent,
    TallyLogComponent,
    QuotationSummaryReportComponent,
    PettyCashReportComponent,

    PoBalanceReportComponent,
    PriceListReportComponent,


    BalanceReportNewComponent,
    PartyDetailedReportNewComponent,
    DcDetailedReportNewComponent,
    PoReportNewComponent,
    PayableReceivableReportNewComponent,
    MaterialReportNewComponent,
    StockTraceNewComponent,
     // NestedTable,

     PoBalanceReportComponent,
     EwayBillReportComponent


    
    // NestedTable,

  ],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [TallyLogComponent]
})
export class ReportsModule { }
