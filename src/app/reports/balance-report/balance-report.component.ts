import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { PartyService } from '../../services/party.service';
import { PayableReceivableService } from '../../services/payable-receivable.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
//import { PartyTypeService } from '../../services/financial-year.service';
import { AppSettings } from '../../app.settings';
import { ExcelService } from '../../services/excel.service';
import { Router } from '@angular/router';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { MaterialService } from '../../services/material.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { BalanceReportModel } from '../../data-model/pr-model';

@Component({
  selector: 'app-balance-report',
  templateUrl: './balance-report.component.html',
  styleUrls: ['./balance-report.component.scss'],
})
export class BalanceReportComponent extends ReportParentComponent implements OnInit {

  isLoadingResults = true;
  subheading: string;
  public dataSource: DataSource<any>;
  mainPartyType: string = "customer";
  invoiceTypeId: number;
  // reportHeading: string= '';
  // reportName: string = '';
  public statuss: status[] = [
    { id: 2, name: "New" },
    { id: 6, name: "Completed" },
   
  ]
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private payableReceivableService: PayableReceivableService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService

    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
    //this.getTransactionType();
  }


  columns = [
    { name: 'invoiceAmount', summaryFunc: (cells) => this.summaryForBalance(cells), },
  ];

  private summaryForBalance(cells: string[]) {

    let summaryBalance: number = 0;
    cells.forEach(c => {
      summaryBalance += +c;
    });

    return summaryBalance;
  }

  ngOnInit() {
    this.initParent();
    this.initComplete.subscribe(response => {
      this.getDateRange();
      this.mainPartyTypeChange();
      this.onSearch();
    });
    // this.getFinancialYears();
    // this.getParties();
    //this.setPage({ offset: 0 });
  }

  getDateRange() {
    let date = new Date();
    let dateOfMonth = date.getDate();
    //console.log("date", dateOfMonth ,date );
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
   // console.log ("1 firstDay  lastDay ",firstDay ,lastDay)
    // if (dateOfMonth < 5) {
      firstDay = new Date(date.getFullYear(), date.getMonth() -1, 1);
      lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);
     // console.log ("2 firstDay  lastDay ",firstDay ,lastDay)
    // }

  //  console.log ("3 firstDay  lastDay ",firstDay ,lastDay)
      this.filterForm.get('transactionFromDate').patchValue(firstDay);
      this.filterForm.get('transactionToDate').patchValue(lastDay);
    
  }


  setPage(pageInfo) {
    //console.log("in setPage: ",pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";
    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  mainPartyTypeChange() {

    //console.log("this.mainPartyType: " + this.mainPartyType)
    //console.log("this.transactionType: " , this.transactionType)
    if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
      //console.log("1");
      this.invoiceTypeId = 5 //Purchase Invoice
    } else if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {
      //console.log("2");
      this.invoiceTypeId = 1; //Customer Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
      //console.log("3");
      this.invoiceTypeId = 18; //Subcontracting Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {
      //console.log("4");
      this.invoiceTypeId = 15; //Jobwork Invoice
    }
    if (this.mainPartyType === "jobworker") {
      this.isJW = true;
      this.getFilteredParties();
    } else {
      this.isJW = false;
      this.getFilteredParties();
    }
    // console.log("this.invoiceTypeId: "+this.invoiceTypeId)

  }

  saveAsExcel(): void {
    let fileName: string = this.transactionType.name;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };

    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    //console.log('excelPage: ', excelPage);
    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData);

      let reports: ExportDataType[] = [];
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })
      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });
  }

  makeSummaryRow(excelData): ExportDataType {

    return {
      "SL No": null,
      "Invoice Number": "TOTAL",
      "Invoice Date": null,
      "Status": null,
      "Party Name": null,
      "TotalAmount": excelData.map(item => item.invoiceAmount).reduce((prev, curr) => prev + curr, 0),
      "PaidAmount": excelData.map(item => item.paidAmount).reduce((prev, curr) => prev + curr, 0),
      "BalanceAmount": excelData.map(item => item.dueAmount).reduce((prev, curr) => prev + curr, 0),

    }
  }



  public saveAsPdf(): void {

    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.balanceReportHeader();
      let reportItem: any[] = this.balanceReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 1) {
        reportName = "Customer Balance Report"
      }
      if (this.transactionType.id === 5) {
        reportName = "Supplier Balance Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }



  balanceReportHeader() {
    return [
      {

        "SL No": "SL No",
        "Invoice Number": "Invoice Number",
        "Invoice Date": "Invoice Date",
        "Status": "Status",
        "Party Name": "Party Name",
        "TotalAmount": "Total Amount",
        "PaidAmount": "Paid Amount",
        "BalanceAmount": "Balance Amount",


      }







    ];




  }

  balanceReportBody(invoiceReport: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {
      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);
    return reportItem;
  }


  makeReportItem(item: BalanceReportModel, count: number): any {

    return {
      "SL No": count,
      "Invoice Number": item.invoiceNumber,
      "Invoice Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Status":item.statusName, 
      "Party Name": item.partyName,
      "TotalAmount": item.invoiceAmount,
      "PaidAmount": item.paidAmount,
      "BalanceAmount": item.dueAmount,
    }
  }


  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.payableReceivableService.getBalanceReport(this.invoiceTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }


  onSearch() {
    this.setPage({ offset: 0 });

  }

  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }
}


export interface ExportDataType {
  "SL No": number;
  "Invoice Number": string;
  "Invoice Date": string;
  "Status": string;
  "Party Name": string;
  "TotalAmount": number;
  "PaidAmount": number;
  "BalanceAmount": number;

}

export interface status {
  id: number,
  name: string,
}