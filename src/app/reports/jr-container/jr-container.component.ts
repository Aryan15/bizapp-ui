import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { InvoiceService } from '../../services/invoice.service';
import { JasperPrintService } from '../../services/jasper-print.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';

@Component({
  selector: 'jr-container',
  templateUrl: './jr-container.component.html',
  styleUrls: ['./jr-container.component.scss']
})
export class JrContainerComponent extends ReportParentComponent implements OnInit {

  pdfData: any;
  pdfGenerated: boolean = false;

  constructor(private financialYearService_in: FinancialYearService,
    public route_in: ActivatedRoute,
    private partyService_in: PartyService ,
    private datePipe: DatePipe,
    private _router_in: Router,
    private globalSettingService: GlobalSettingService,
    private invoiceService: InvoiceService,    
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private userService: UserService,
    private jasperPrintService: JasperPrintService) {
      super(
        partyService_in,
        materialService_in,
        financialYearService_in,
        transactionTypeService_in,
        route_in,
        _router_in,
        userService
    );
   //this.getTransactionType();
  }

  ngOnInit() {

    this.initParent();
    this.initComplete.subscribe(response =>{
      this.onSearch();
    });
  }

  onSearch() {

    // this.setPage({ offset: 0 });
    this.jasperPrintService.jasperReport(
     this.transactionType.id, this.counterTTypeId, this.jasperReportName, this.filterForm.value, this.page
    )
    .subscribe(response => {
      //console.log("Got pdf")
      this.pdfData = response;
      this.pdfGenerated = true;
    })
  }

  setPageAfterPageLimit(){
    this.page.size = this.pageSize;
    //this.setPage({ offset: 0 });
  }


  saveAsExcel() {

    // this.setPage({ offset: 0 });
    this.jasperPrintService.jasperReportExcel(
     this.transactionType.id, this.counterTTypeId, this.jasperReportName, this.filterForm.value, this.page
    )
    .subscribe(response => {
            this.exportExcel(response);
        }, error => {
            //console.log("error: ", error)
    })
  }

  exportExcel(data){       
        let blob = data;
        let a = document.createElement("a");
        a.setAttribute('style', 'display: none');
        a.href = URL.createObjectURL(blob);
        a.download = this.transactionType.name+'.xlsx';
        document.body.appendChild(a);
        a.click();        
        a.remove();
    }

    print(){
      // var blob = new Blob([this.pdfData], {type: 'application/pdf'});
      // var blob = new Blob([this.pdfData]);
      const blob = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.pdfData], { type: 'application/pdf' }));
      //const blobUrl = URL.createObjectURL(blob);
        const iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        iframe.src = blob;
        document.body.appendChild(iframe);
        iframe.contentWindow.print();
    }

    download(){
      const pdfUrl = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.pdfData], { type: 'application/pdf' }));
      const anchor = document.createElement('a');
      anchor.href = pdfUrl;
      anchor.setAttribute("download", this.transactionType.name);
      anchor.click();
    }
}
