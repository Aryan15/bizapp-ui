
import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Page } from '../model/page';
import { PagedData } from '../model/paged-data';
import { NumberFormatterService } from '../../utils/number-formatter.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { UserService } from '../../services/user.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PettyCashHeader, PettyCashItem, Tag, Category, PettyCashWrapper, PettyCashReportModel } from '../../data-model/petty-cash-model';

import { PettyCashService } from '../../services/pettyCash.service';

import { PaymentMethod } from '../../data-model/payment-method';
@Component({
  selector: 'app-petty-cash-report',
  templateUrl: './petty-cash-report.component.html',
  styleUrls: ['./petty-cash-report.component.scss']
})
export class PettyCashReportComponent extends  ReportParentComponent implements OnDestroy, AfterViewInit {
  isLoadingResults = true;
  isPettyCash = false;
 // pettyCashDatabase: PettyCashReportHttpDao | null;
  dataSource = new MatTableDataSource<PettyCashReportModel>();


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  resultsLength = 0;
  displayedColumns = [];
  categorys:Category[]=[];
  tags:Tag[]=[];











  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private pettyCashService: PettyCashService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    private numberFormatterService: NumberFormatterService,
    private userService: UserService) {

      super(
        partyService_in,
        materialService_in,
        financialYearService_in,
        transactionTypeService_in,
        route_in,
        _router_in,
        userService
      );
     }

     ngAfterViewInit() {
      this.initParent();
      // this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
  
     // this.pettyCashDatabase = new PettyCashReportHttpDao(this.pettyCashService);
  
      //If the user changes sort order, reset back to first page
  
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  
      // this.getData();
      // this.getDateRange();
      // this.getTransactionType();
      this.initComplete.subscribe(response =>{
        if(this.transactionType.name === AppSettings.PETTY_CASH){
          this.isPettyCash = true;
        }else{
          this.isPettyCash = false;
        }
        //console.log("this.isCheque: "+this.isCheque);
      //  this.getData();
        this.getDisplaycol();
        this.getCategory();
      // this.getPaymentMethod();
      });
  
    }





    getDisplaycol() {
      
        //console.log("in")
        this.displayedColumns = [
          'pettyCashNumber'
        , 'pettyCashDate'
        , 'totalAmount'
        , 'categoryName'
        , 'tagName'
        , 'paymentName'
        , 'incomingAmount'
        , 'outgoingAmount'
        , 'remarks'
        ];
      }
    
    
      getCategory() {
        this.pettyCashService.getAllCategory().subscribe(
            response => {
    
                this.categorys = response;
                //console.log("this.expenses" + this.expenses);
            }
        )
      }

}


// export class PettyCashReportHttpDao {

//   constructor(private pettyCashService: PettyCashService) { }

//   getPettyCash(transactionType: number, filterForm: FormGroup, page: Page): Observable<PagedData<PettyCashReportModel>> {

    

//    // return this.pettyCashService.getPettyCashReport(transactionType, filterForm.value, page);
//   }
// }