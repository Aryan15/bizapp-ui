import { DataSource } from '@angular/cdk/table';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { PartyTypeService } from '../../services/financial-year.service';
import { AppSettings } from '../../app.settings';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { PayableReceivableService } from '../../services/payable-receivable.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { FormControl } from '@angular/forms';
import { PdfService } from '../../services/pdf.service';
import { PayableReceivableReportModel } from '../../data-model/pr-model';
import { InvoiceService } from '../../services/invoice.service';
@Component({
  selector: 'payable-receivable-report',
  templateUrl: './payable-receivable-report.component.html',
  styleUrls: ['./payable-receivable-report.component.scss'],
})
export class PayableReceivableReportComponent extends ReportParentComponent implements OnInit {

  public dataSource: DataSource<any>;
  subheading: string;
  
  // columns = [
  //   { name: 'dueAmount', summaryFunc: (cells) => this.summaryForBalance(cells), },
  // ];

  // private summaryForBalance(cells: string[]) {

  //   let summaryBalance: number = 0;
  //   cells.forEach(c => {
  //     summaryBalance += +c;
  //   });

  //   return summaryBalance;
  // }
  invoiceTypeId: number;
  mainPartyType: string = "customer";
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private payableReceivableService: PayableReceivableService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService,
   ) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService,
      
    );

    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

  mainPartyTypeChange() {

    //console.log("this.mainPartyType: " + this.mainPartyType)
    //console.log("this.transactionType: " , this.transactionType)
    if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {
      //console.log("1");
      this.invoiceTypeId = 5 //Purchase Invoice
    } else if (this.mainPartyType === "customer" && this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      //console.log("2");
      this.invoiceTypeId = 1; //Customer Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.SUPPLIER_PAYABLE) {
      //console.log("3");
      this.invoiceTypeId = 18; //Subcontracting Invoice
    } else if (this.mainPartyType === "jobworker" && this.transactionType.name === AppSettings.CUSTOMER_RECEIVABLE) {
      //console.log("4");
      this.invoiceTypeId = 15; //Jobwork Invoice
    }
    if (this.mainPartyType === "jobworker") {
      this.isJW = true;
      this.getFilteredParties();
    } else {
      this.isJW = false;
      this.getFilteredParties();
    }
    //console.log("this.invoiceTypeId: "+this.invoiceTypeId)

  }
  ngOnInit() {

    this.initParent();
    this.initComplete.subscribe(response => {
      this.mainPartyTypeChange();
      this.onSearch();

    });

  }

  setPage(pageInfo) {
    //console.log("in setPage: ", pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";
    //  this.payableReceivableReportRequest.financialYearId = this.payableReceivableReportRequest.financialYearId ? this.payableReceivableReportRequest.financialYearId : this.financialYears.filter(fy => fy.isActive === 1)[0].id;
    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data
     
      //console.log('this.rows: ', this.rows);
    });
  }



  saveAsExcel(): void {
    let fileName: string = this.transactionType.name.replace(' ', '_');
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });

  }

  makeSummaryRow(excelData): ExportDataType {
    return {
      "SL No": null,
      "Paid Date": null,
      "Payment Number": "TOTAL",

      "Status": null,
      "InvoiceNumber": null,
      "InvoiceDate": null,
      "PartyName": null,
      "TotalAmount": excelData.map(item => item.invoiceAmount).reduce((prev, curr) => prev + curr, 0),
      "BalanceAmount": excelData.map(item => item.dueAmount).reduce((prev, curr) => prev + curr, 0),
      "TDSPercentage": null,
      "TDSAmount": excelData.map(item => item.tdsAmount).reduce((prev, curr) => prev + curr, 0),
      "AmountAfterTds": excelData.map(item => item.amountAfterTds).reduce((prev, curr) => prev + curr, 0),
      "AmountPaid": excelData.map(item => item.paidAmount).reduce((prev, curr) => prev + curr, 0),
      "PaymentMode": null,
      "Cheque No/Bank Name": null
    }
  }


  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.payableReportHeader();
      let reportItem: any[] = this.payableReportBody(pdfData);
      let reportName: string
      if (this.transactionType.id === 4) {
        reportName = "Payable-Receivable Report"
      }
      if (this.transactionType.id === 3) {
        reportName = "Receivable Report"
      }

      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });



  }


  payableReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Paid Date": "Paid Date",
        "Payment Number": "Payment Number",

        "Status": "Status",
        "InvoiceNumber": "InvoiceNumber",

        "InvoiceDate": "InvoiceDate",
        "PartyName": "PartyName",
        "TotalAmount": "TotalAmount",
        "BalanceAmount": "BalanceAmount",
        "TDSPercentage": "TDSPercentage",
        "TDSAmount": "TDSAmount",
        "AmountAfterTds": "AmountAfterTds",
        "AmountPaid": "AmountPaid",
        "PaymentMode": "PaymentMode",
        "Cheque No/Bank Name": "Cheque No/Bank Name",


      },



    ];




  }




  payableReportBody(invoiceReport: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });
    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);
    return reportItem;





  }


  makeReportItem(item: PayableReceivableReportModel, count: number): any {
    if (item.paymentDocumentNumber === null) {
      item.paymentDocumentNumber = ''
    }
   
 
   
    return {
      "SL No": count,

      "Paid Date": this.datePipe.transform(item.payReferenceDate, 'dd-MM-yyyy'),
      "Payment Number": item.payReferenceNumber,
      "Status": item.statusName,
      // "slNo": quo.,
      "InvoiceNumber": item.invoiceNumber,
      "InvoiceDate": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "PartyName": item.partyName,
      "TotalAmount": item.invoiceAmount,
      "BalanceAmount": item.dueAmount,
      "TDSPercentage": item.tdsPercentage,
      "TDSAmount": item.tdsAmount,
      "AmountAfterTds": item.amountAfterTds,
      "AmountPaid": item.paidAmount,
      "PaymentMode": item.paymentMethodName,
      "Cheque No/Bank Name": item.paymentDocumentNumber,
    }
  }





  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }


  onSort(event) {
  
    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.payableReceivableService.getPayableReceivableItemReport(this.transactionType.id, this.invoiceTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
     
    });

  }

  onSearch() {

    this.setPage({ offset: 0 });

  }

  summaryTotal() {
    return this.rows
      .filter(row => row.statusName != 'Draft')
      .map(row => row.payingAmount)
      .reduce((res, cell) => res += cell, 0);
  }

  getExcel() {
    this.payableReceivableService.downloadPayableReceivableItemReport(this.transactionType.id, this.filterForm.value, this.page)
      .subscribe(response => {
        this.exportData(response);
      },
        error => {
          //console.log("error :", error);
        })

  }

  exportData(data) {
    let blob = data;
    let a = document.createElement("a");
    a.setAttribute('style', 'display: none');
    a.href = URL.createObjectURL(blob);
    a.download = 'fileName.xlsx';
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
}
export interface ExportDataType {

  "SL No": number;

  "Paid Date": string;
  "Payment Number": string;
  "Status": string;
  "InvoiceNumber": string;

  "InvoiceDate": string;
  "PartyName": string;
  "TotalAmount": number;
  "BalanceAmount": number;
  "TDSPercentage": number;
  "TDSAmount": number;
  "AmountAfterTds": number;
  "AmountPaid": number;
  "PaymentMode": string;
  "Cheque No/Bank Name": string;
}