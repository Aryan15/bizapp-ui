import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayableReceivableReportComponent } from './payable-receivable-report.component';

describe('SupplierPayableReportComponent', () => {
  let component: PayableReceivableReportComponent;
  let fixture: ComponentFixture<PayableReceivableReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayableReceivableReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayableReceivableReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
