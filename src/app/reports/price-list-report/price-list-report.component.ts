import { Component, OnInit } from '@angular/core';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { DataSource } from '@angular/cdk/table';
import { FinancialYearService } from '../../services/financial-year.service';
import { PartyService } from '../../services/party.service';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { AppSettings } from '../../app.settings';
import { Page } from '../model/page';
import { PartyType, Party, PartyPriceListReport } from '../../data-model/party-model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-price-list-report',
  templateUrl: './price-list-report.component.html',
  styleUrls: ['./price-list-report.component.scss']
})
export class PriceListReportComponent extends ReportParentComponent implements OnInit {
  subheading: string;
  reorderable: true;
  mainPartyType: string = "customer";
  partyType: number = 1;
  allowAllMaterial = new FormControl(false);
  allowAllMaterials:number=0;
  constructor(
    private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService
  ) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService

    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;


  }

  ngOnInit() {
    this.partyTypeChange();
    this.onSearch();



  }

  getRowClass = (row) => {
    if(row.partyName){
      return {
        'row-color': true
      };
    }
    //console.log('rowClass')
   
  }


  partyTypeChange() {

    if (this.mainPartyType === "customer") {
      this.partyType = 1
      this.isCustomer = true;
      this.getFilteredParties();
    }
    else {
      this.partyType = 2;
      this.isCustomer = false;
      this.getFilteredParties();
    }


  }





  onSearch() {
    console.log(this.allowAllMaterial.value+"-----------------");
    this.setPage({ offset: 0 });
   

  }

  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }

  setPage(pageInfo) {
    if(this.allowAllMaterial.value===true){
      this.allowAllMaterials=1;
    }
    else{
      this.allowAllMaterials=0;
    }


    this.page.pageNumber = pageInfo.offset;
    
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "partyName";
    this.partyService_in.getPriceListReport(this.partyType, this.filterForm.value, this.page,this.allowAllMaterials).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  onSort(event) {
    if(this.allowAllMaterial.value===true){
      this.allowAllMaterials=1;
    }
    else{
      this.allowAllMaterials=0;
    }

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;
    // this.partyService_in.getPriceListReport(this.priceListRequest).subscribe((pagedData) => {

    this.partyService_in.getPriceListReport(this.partyType, this.filterForm.value, this.page, this.allowAllMaterials).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }





  close() {
    this._router_in.navigate(['/']);
  }
  saveAsExcel(): void {
    if(this.allowAllMaterial.value===true){
      this.allowAllMaterials=1;
    }
    else{
      this.allowAllMaterials=0;
    }

    let fileName: string = "Party Price List";
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };

    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    //console.log('excelPage: ', excelPage);
    this.partyService_in.getPriceListReport(this.partyType, this.filterForm.value, this.page,this.allowAllMaterials).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData);

      let reports: ExportDataType[] = [];
      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      this.excelService.exportFromJSON(reports, fileName);

    });
  }

  public saveAsPdf(): void {
    if(this.allowAllMaterial.value===true){
      this.allowAllMaterials=1;
    }
    else{
      this.allowAllMaterials=0;
    }

    let pdfPage = { ...this.page };
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.partyService_in.getPriceListReport(this.partyType, this.filterForm.value, this.page,this.allowAllMaterials).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.balanceReportHeader();
      let reportItem: any[] = this.balanceReportBody(pdfData);
      let reportName: string

      reportName = "Party Price List"



      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, "PriceListReport");
    });
  }
  balanceReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Party Name": "Party Name",
        "MaterialTypeName": "MaterialTypeName",
        "MaterialName": "MaterialName",
        "PartNumber": "PartNumber",
        "PartyPrice": "PartyPrice",
        "CurrentPrice": "CurrentPrice",
        "Comments": "Comments",
      }
    ]
  }

  balanceReportBody(partyPriceList: any[]) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    partyPriceList.forEach((po, index) => {
      reportItem.push(this.makeReportItem(po, index + 1));
    });


    return reportItem;
  }


  makeReportItem(item: PartyPriceListReport, count: number): any {

    return {
      "SL No": count,
      "Party Name": item.partyName,
      "MaterialTypeName": item.materialTypeName,
      "MaterialName": item.materialName,
      "PartNumber": item.partNumber,
      "PartyPrice": item.partyPrice,
      "CurrentPrice": item.currentPrice,
      "Comments": item.comments,

    }
  }





}


export interface ExportDataType {
  "SL No": number;
  "Party Name": string;
  "MaterialTypeName": string;
  "MaterialName": string;
  "PartNumber": string;
  "PartyPrice": number;
  "CurrentPrice": number;
  "Comments": string;
}








