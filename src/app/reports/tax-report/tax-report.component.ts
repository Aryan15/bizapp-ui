
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { InvoiceTaxReport, InvoiceTaxReportModel } from '../../data-model/invoice-model';
import { TransactionType } from '../../data-model/transaction-type';
import { InvoiceService } from '../../services/invoice.service';
import { PartyService } from '../../services/party.service';
import { MaterialService } from '../../services/material.service';
import { Material } from '../../data-model/material-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { Party } from '../../data-model/party-model';

import { ExcelService } from '../../services/excel.service';
import { Page } from '../model/page';
import { AppSettings } from '../../app.settings';

import { TransactionReportRequest } from '../../data-model/transaction-report-request-model';
import { FinancialYearService } from '../../services/financial-year.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { DatePipe } from '@angular/common';
import { AlertService } from 'ngx-alerts';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { UserService } from '../../services/user.service';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { GlobalSettingService } from '../../services/global-setting.service';
import { PdfService } from '../../services/pdf.service';

@Component({
  selector: 'app-tax-report',
  templateUrl: './tax-report.component.html',
  styleUrls: ['./tax-report.component.scss'],
})
export class TaxReportComponent extends ReportParentComponent implements OnInit {

  pivotDArray: any = [];
  subheading: string;
  isLoadingResults = true;

  tableMessages = {
    // Message to show when array is presented
    // but contains no values
    emptyMessage: 'No data to display',

    // Footer total message
    totalMessage: 'total(indication only not accurate)'
  }

  // transactionType: TransactionType;
  // financialYears: FinancialYear[] = [];
  // public parties: Party[] = [];
  // public partyTypeIds: number[] = [];
  // public materials: Material[] = [];
  // invoiceReportRequest: TransactionReportRequest = { financialYearId: null, materialId: null, partyId: null, transactionFromDate: null, transactionToDate: null, partyTypeIds: [] };
  // filterForm = new FormGroup({
  //   fromDate: new FormControl(),
  //   toDate: new FormControl(),
  //   financialYearId: new FormControl(),
  //   partyId: new FormControl(),
  //   materialId: new FormControl()
  // });
  // isCustomer: boolean = false;
  // isSupplier: boolean = false;
  // partyPlaceHolder: string;
  isItemLvelTax: boolean = false;
  buyOrSell: string;
  constructor(
    private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    private invoiceService: InvoiceService,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private alertService: AlertService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private globalSettingService: GlobalSettingService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

  // private getTransactionType() {
  //   this.route.params.subscribe(params => {
  //     //this.transactionTypeService.getTransactionType(+params['trantypeId'])
  //     this.transactionTypeService.getTransactionType(1)
  //       .subscribe(response => {
  //         //console.log('transaction type: ', response);
  //         this.transactionType = response;
  //         if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {

  //           this.isCustomer = true;
  //           this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

  //           //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
  //         } else if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
  //           this.isSupplier = true;
  //           this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
  //           //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

  //         }

  //         this.getParties();
  //         this.getPartyTypes();
  //         this.getFinancialYears();
  //         this.getAllMaterials();
  //       })
  //   });
  // }
  ngOnInit() {
    this.initParent();
    this.initComplete.subscribe(response => {
      this.deriveBuyOrSell();
      this.globalSettingService.getGlobalSetting()
        .subscribe(response => {
          // this.isItemLvelTax = response.itemLevelTax ? true : false;
          if (this.buyOrSell === AppSettings.TXN_SELL) {
            this.isItemLvelTax = response.itemLevelTax === 1 ? true : false;
          } else {
            this.isItemLvelTax = response.itemLevelTaxPurchase === 1 ? true : false;
          }
         
          this.onSearch();
        });
    })

  }

  deriveBuyOrSell() {
    let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.INCOMING_JOBWORK_PO, AppSettings.INCOMING_JOBWORK_INVOICE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.OUTGOING_JOBWORK_INVOICE];
    let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE];
    if (sellTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_SELL;
    } else if (buyTranTypes.includes(this.transactionType.name)) {

      this.buyOrSell = AppSettings.TXN_PURCHASE;
    }

  }

  // private getFinancialYears() {

  //   this.financialYearService.getFinancialYears()
  //     .subscribe(response => {
  //       this.financialYears = response;
  //     })
  // }

  // getPartyTypes() {

  //   //let partyTypeIds: number[] = [];

  //   this.partyService.getAllPartyTypes().subscribe(response => {
  //     //console.log("res ", response);
  //     response.forEach(pType => {
  //       if (this.isCustomer && (pType.name === AppSettings.PARTY_TYPE_CUSTOMER || pType.name === AppSettings.PARTY_TYPE_BOTH)) {
  //         this.partyTypeIds.push(pType.id);
  //       } else if (this.isSupplier && (pType.name === AppSettings.PARTY_TYPE_SUPPLIER || pType.name === AppSettings.PARTY_TYPE_BOTH)) {
  //         this.partyTypeIds.push(pType.id);
  //       }
  //     });

  //     this.invoiceReportRequest.partyTypeIds = this.partyTypeIds;
  //     this.setPage({ offset: 0 });

  //   });



  // }

  // getParties() {

  //   if (this.isCustomer) {
  //     this.partyService.getCustomers().subscribe(
  //       response => {
  //         this.parties = response;
  //       }
  //     );
  //     this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
  //   }
  //   else if (this.isSupplier) {
  //     this.partyService.getSuppliers().subscribe(
  //       response => {
  //         this.parties = response;
  //       }
  //     );
  //     this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
  //   }
  // }
  // getAllMaterials() {
  //   this.materialService.getAllMaterials().subscribe(
  //     response => {
  //       this.materials = response;
  //       //console.log("this.materils", this.materials)
  //     }
  //   )
  // }

  setPage(pageInfo) {
    this.isLoadingResults = true;
    //console.log("in setPage: ", pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";

    // this.page.size = AppSettings.REPORT_PAGE_SIZE;

    this.invoiceService.getTaxInvoiceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;

      //this.page.size += 1;
      //console.log("pagedData.data ", pagedData.data.length)
      // this.pivotDArray = this.rows ?  this.pivotData()  : null;
      if (pagedData.data.length >= 1) {
        console.log(" inside pagedData.data ", pagedData.data)
        if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE){
          this.isItemLvelTax =true;
        }
        this.pivotDArray = this.pivotData(this.rows)
      } else {
        //console.log(" in else pagedData.data ", pagedData.data)
        this.pivotDArray = null
      }

      // this.pivotDArray = this.pivotData();
      //console.log('this.pivotDArray: ', this.pivotDArray);
      //console.log('columns: ', this.columns);
      this.isLoadingResults = false;
    });
  }

  pivotData(rows: any): ExportDataType {
    let prevInvNo = '';
    let pivotD: any;
    let pivotDArray: any = [];
    //console.log("in pivottara");
    rows.forEach(d => {

      let cgstCol = "cgst" + ((d.cgstTaxPercent !== null) ? d.cgstTaxPercent.toString().replace(/\./g, '') : null);
      let sgstCol = "sgst" + ((d.sgstTaxPercent !== null) ? d.sgstTaxPercent.toString().replace(/\./g, '') : null);
      let igstCol = "igst" + ((d.igstTaxPercent !== null) ? d.igstTaxPercent.toString().replace(/\./g, '') : 0);

      let cgstColHead = "CGST " + d.cgstTaxPercent + "%"
      let sgstColHead = "SGST " + d.sgstTaxPercent + "%"

      let igstColHead = "IGST " + d.igstTaxPercent + "%"

      console.log('row: ', d);
      console.log("cgstCol: " + cgstCol);
      console.log("sgstCol: " + sgstCol);
      console.log("igstCol: " + igstCol);
      if (prevInvNo !== d.invoiceNumber) {
        ////console.log(d.invoice, d.taxPercent)
        //console.log("pivotD.............",pivotD);

        if (pivotD)
          pivotDArray.push(pivotD);
        pivotD = {};


        pivotD["invoiceNumber"] = d.invoiceNumber;
        pivotD["invoiceDate"] = this.datePipe.transform(d.invoiceDate, AppSettings.DATE_FORMAT),
          pivotD["partyName"] = d.partyName;
        pivotD["gstNumber"] = d.gstNumber;


        pivotD["statusName"] = d.statusName;
        pivotD["netAmount"] = d.netAmount;

        pivotD[cgstCol] = d.cgstTaxAmount;
        pivotD[sgstCol] = d.sgstTaxAmount;
        pivotD[igstCol] = d.igstTaxAmount;
        pivotD["grandTotal"] = d.grandTotal;
        prevInvNo = d.invoiceNumber;

      }
     
      else if (this.isItemLvelTax) {
        if (pivotD[cgstCol]) {

          pivotD[cgstCol] = +pivotD[cgstCol] + d.cgstTaxAmount;
        } else {
          pivotD[cgstCol] = d.cgstTaxAmount;
        }

        if (pivotD[sgstCol]) {
          pivotD[sgstCol] = +pivotD[sgstCol] + d.sgstTaxAmount;
        } else {
          pivotD[sgstCol] = d.sgstTaxAmount;
        }

        if (pivotD[igstCol]) {
          pivotD[igstCol] = +pivotD[igstCol] + d.igstTaxAmount;
        } else {
          pivotD[igstCol] = d.igstTaxAmount;
        }
      }
      else {

        pivotD[cgstCol] = d.cgstTaxAmount;

        pivotD[sgstCol] = d.sgstTaxAmount;

        pivotD[igstCol] = d.igstTaxAmount;
      }

      //populate column headers
      ////console.log('this.columns.find(col => col.name === "taxPer"+d.taxPercent): ',this.columns.find(col => col.name === "taxPer"+d.taxPercent));
      if (!this.columns.find(col => col.name === cgstCol) && d.cgstTaxPercent) {
        this.columns.push({ name: cgstCol, summaryFunc: null, columnHeader: cgstColHead })
      }

      if (!this.columns.find(col => col.name === sgstCol) && d.sgstTaxPercent) {
        this.columns.push({ name: sgstCol, summaryFunc: null, columnHeader: sgstColHead })
      }

      if (!this.columns.find(col => col.name === igstCol) && d.igstTaxPercent) {
        this.columns.push({ name: igstCol, summaryFunc: null, columnHeader: igstColHead })
      }



    });


    // this.columns.push({ name: "grandTotal", summaryFunc: (cells) => this.summaryForGrandTotal(cells), })

    pivotDArray.push(pivotD);
    ////console.log('pivot Data: ', pivotDArray);
    return pivotDArray;
  }

  private summaryForGrandTotal(cells: string[]) {

    // let summaryGrandTotal: number = 0;
    // cells.forEach(c => {
    //   summaryGrandTotal += +c;
    // });
    //console.log('in summaryForGrandTotal ')
    return this.priceSummary(cells);
  }


  // rows = [
  //   {
  //     name: 'Claudine Neal',
  //     gender: 'female',
  //     company: 'Sealoud'
  //   },
  //   {
  //     name: 'Beryl Rice',
  //     gender: 'female',
  //     company: 'Velity'
  //   }
  // ];

  // close() {
  //   this._router.navigate(['/'])
  // }
  getTaxColumns() {

    let retColumns: any = [];

    this.columns.forEach(col => {
      if (col.name.startsWith('cgst') || col.name.startsWith('sgst') || col.name.startsWith('igst')) {
        retColumns.push(col);
      }
    });

    return retColumns;

  }

  columns = [
    { name: 'invoiceNumber', summaryFunc: null, columnHeader: null },
    { name: 'invoiceDate', summaryFunc: null, columnHeader: null },
    { name: 'partyName', summaryFunc: null, columnHeader: null },
    { name: 'gstNumber', summaryFunc: null, columnHeader: null },
    { name: 'netAmount', summaryFunc: null, columnHeader: null },
  ];


  onSearch() {

    this.setPage({ offset: 0 });
  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;
    //console.log("this.page.sortColumn",  this.page.sortColumn  );
    this.invoiceService.getTaxInvoiceReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {

      this.page = pagedData.page;
      this.rows = pagedData.data;

      if (pagedData.data.length >= 1) {
        //console.log(" inside pagedData.data ", pagedData.data)
        if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE){
          this.isItemLvelTax =true;
        }
        this.pivotDArray = this.pivotData(this.rows)
      } else {
        //console.log(" in else pagedData.data ", pagedData.data)
        this.pivotDArray = null
      }
    });

  }
  saveAsExcel(): void {
    let fileName: string = this.transactionType.name.replace(' ', '_');

    let excelPage = { ...this.page };
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.invoiceService.getTaxInvoiceReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      let reports: ExportDataType[] = [];

      if (pagedData.data.length > 0) {
        
        if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE){
          this.isItemLvelTax =true;
        }
        this.pivotDArray.data = this.pivotData(pagedData.data)
      } else {
        console.log(" in else pagedData.data ", this.pivotDArray.data)
        this.pivotDArray = null
      }

      excelData = this.pivotDArray.data




      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });







  }


  makeSummaryRow(excelData): ExportDataType {
    excelData.forEach(item => {
      if (!item.cgst0125) {
        item.cgst0125 = 0
        item.sgst0125 = 0
      }
      if (!item.igst025) {
        item.igst025 = 0
      }
      if (!item.cgst25) {
        item.cgst25 = 0
        item.sgst25 = 0
      }
      if (!item.igst5) {
        item.igst5 = 0
      }
      if (!item.cgst6) {
        item.cgst6 = 0
        item.sgst6 = 0
      }
      if (!item.igst12) {
        item.igst12 = 0
      }
      if (!item.cgst9) {
        item.cgst9 = 0
        item.sgst9 = 0
      }
      if (!item.igst18) {
        item.igst18 = 0
      }
      if (!item.cgst14) {
        item.cgst14 = 0
        item.sgst14 = 0
      }
      if (!item.igst28) {
        item.igst28 = 0
      }


    })
    return {
      "SL No": null,
      "Invoice Number": "TOTAL",
      "Invoice Date": null,
      "Party Name": null,
      "GstNumber": null,
      "Status": null,
      "NetAmount": excelData.map(item => item.netAmount).reduce((prev, curr) => prev + curr, 0),
      "Cgst0.125": excelData.map(item => item.cgst0125).reduce((prev, curr) => prev + curr, 0),
      "Sgst0.125": excelData.map(item => item.cgst0125).reduce((prev, curr) => prev + curr, 0),
      "Igst0.25": excelData.map(item => item.igst025).reduce((prev, curr) => prev + curr, 0),
      "Cgst2.5": excelData.map(item => item.cgst25).reduce((prev, curr) => prev + curr, 0),
      "Sgst2.5": excelData.map(item => item.sgst25).reduce((prev, curr) => prev + curr, 0),
      "Igst5": excelData.map(item => item.igst5).reduce((prev, curr) => prev + curr, 0),
      "Cgst6": excelData.map(item => item.cgst6).reduce((prev, curr) => prev + curr, 0),
      "Sgst6": excelData.map(item => item.sgst6).reduce((prev, curr) => prev + curr, 0),
      "Igst12": excelData.map(item => item.igst12).reduce((prev, curr) => prev + curr, 0),
      "Cgst9": excelData.map(item => item.cgst9).reduce((prev, curr) => prev + curr, 0),
      "Sgst9": excelData.map(item => item.sgst9).reduce((prev, curr) => prev + curr, 0),
      "Igst18": excelData.map(item => item.igst18).reduce((prev, curr) => prev + curr, 0),
      "Cgst14": excelData.map(item => item.cgst14).reduce((prev, curr) => prev + curr, 0),
      "Sgst14": excelData.map(item => item.sgst14).reduce((prev, curr) => prev + curr, 0),
      "Igst28": excelData.map(item => item.igst28).reduce((prev, curr) => prev + curr, 0),
      "GrandTotal/Includes Tcs": excelData.map(item => item.grandTotal).reduce((prev, curr) => prev + curr, 0),

    }
  }


  public saveAsPdf(): void {

    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.invoiceService.getTaxInvoiceReport(this.transactionType.id, this.filterForm.value, pdfPage).subscribe(pagedData => {

      pdfData = pagedData.data;
      if (pagedData.data.length > 0) {
        if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE || this.transactionType.name === AppSettings.OUTGOING_JOBWORK_INVOICE){
          this.isItemLvelTax =true;
        }
          this.pivotDArray.data = this.pivotData(pagedData.data)
      } else {
        console.log(" in else pagedData.data ", this.pivotDArray.data)
        this.pivotDArray = null
      }

      pdfData = this.pivotDArray.data;


      let reportHeader: any[] = this.taxReportHeader();
      let reportItem: any[] = this.taxReportBody(this.pivotDArray.data, this.pivotDArray);
      let reportName: string
      if (this.transactionType.id === 1) {
        reportName = "Customer Tax Report"
      }
      if (this.transactionType.id === 5) {
        reportName = "Supplier Tax Report"
      }

      if (this.transactionType.id === 15) {
        reportName = "Jobwork Tax Report"
      }

      if (this.transactionType.id === 18) {
        reportName = "Subcontract Tax Report"
      }
      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.transactionType.name);
    });
  }



  taxReportHeader() {
    return [
      {
        "SL No": "SL No",
        "Invoice Number": "Invoice Number",
        "Invoice Date": "Invoice Date",
        "Party Name": "Party Name",
        "GstNumber": "GstNumber",
        "Status": "Status",
        "NetAmount": "Net Amount",
        "Cgst0.125": "Cgst0.125%",
        "Sgst0.125": "Sgst0.125%",
        "Igst0.25": "Igst0.25%",
        "Cgst2.5": "Cgst2.5%",
        "Sgst2.5": "Sgst2.5%",
        "Igst5": "Igst5%",
        "Cgst6": "Cgst6%",
        "Sgst6": "Sgst6%",
        "Igst12": "Igst12%",
        "Cgst9": "Cgst9%",
        "Sgst9": "Sgst9%",
        "Igst18": "Igst18%",
        "Cgst14": "Cgst14%",
        "Sgst14": "Sgst14%",
        "Igst28": "Igst28%",
        "GrandTotal/Includes Tcs": "Grand Total/Includes Tcs",

      },



    ];




  }


  taxReportBody(invoiceReport: any[], invoiceList) {
    let count: number = 0
    let reportItem: ExportDataType[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    console.log(invoiceReport)
    let summaryRow: ExportDataType = this.makeSummaryRow(invoiceReport);
    reportItem.push(summaryRow);

    return reportItem;



  }


  makeReportItem(item: any, count: number) {

    return {
      "SL No": count,
      "Invoice Number": item.invoiceNumber,
      "Invoice Date": this.datePipe.transform(item.invoiceDate, 'dd-MM-yyyy'),
      "Party Name": item.partyName,
      "GstNumber": item.gstNumber,
      "Status": item.statusName,
      "NetAmount": item.netAmount,
      "Cgst0.125": item.cgst0125,
      "Sgst0.125": item.sgst0125,
      "Igst0.25": item.igst025,
      "Cgst2.5": item.cgst25,
      "Sgst2.5": item.sgst25,
      "Igst5": item.igst5,
      "Cgst6": item.cgst6,
      "Sgst6": item.sgst6,
      "Igst12": item.igst12,
      "Cgst9": item.cgst9,
      "Sgst9": item.sgst9,
      "Igst18": item.igst18,
      "Cgst14": item.cgst14,
      "Sgst14": item.sgst14,
      "Igst28": item.igst28,
      "GrandTotal/Includes Tcs": item.grandTotal,
    }
  }




  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }
  clear() {
    this.pivotDArray = [];
    this.page.totalElements = 0;
    super.clear();
  }

}

export interface ExportDataType {

  "SL No": number;
  "Invoice Number": string;
  "Invoice Date": string;
  "Party Name": string;
  "GstNumber": string;
  "Status": string;
  "NetAmount": number;
  "Cgst0.125": number;
  "Sgst0.125": number;
  "Igst0.25": number;
  "Cgst2.5": number;
  "Sgst2.5": number;
  "Igst5": number;
  "Cgst6": number;
  "Sgst6": number;
  "Igst12": number;
  "Cgst9": number;
  "Sgst9": number;
  "Igst18": number;
  "Cgst14": number;
  "Sgst14": number;
  "Igst28": number;
  "GrandTotal/Includes Tcs": number;

}