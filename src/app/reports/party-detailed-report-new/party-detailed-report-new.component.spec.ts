import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyDetailedReportNewComponent } from './party-detailed-report-new.component';

describe('PartyDetailedReportNewComponent', () => {
  let component: PartyDetailedReportNewComponent;
  let fixture: ComponentFixture<PartyDetailedReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyDetailedReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyDetailedReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
