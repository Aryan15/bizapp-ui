import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PartyService } from '../../services/party.service';
import { Page } from '../model/page';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { merge, Observable, of as observableOf, Subject } from 'rxjs';
import { PagedData } from '../model/paged-data';
import { FinancialYearService } from '../../services/financial-year.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from '../../services/excel.service';
import { MaterialService } from '../../services/material.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { PdfService } from '../../services/pdf.service';
import { AppSettings } from '../../app.settings';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import {  Party } from '../../data-model/party-model';

@Component({
  selector: 'app-party-detailed-report-new',
  templateUrl: './party-detailed-report-new.component.html',
  styleUrls: ['./party-detailed-report-new.component.scss']
})
export class PartyDetailedReportNewComponent  extends ReportParentComponent implements  AfterViewInit, OnDestroy  {
  displayedColumns = ['name','address','gstNumber','email','contactPersonName','contactPersonNumber','primaryMobile'];
  dataSource = new MatTableDataSource<Party>();//ELEMENT_DATA;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  partyReportDatabase: PartyHttpDao | null;
  resultsLength = 0;
  isLoadingResults = true;
  public partyTypeId: number;
  partyName: string;
  subheading: string;

  
  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }

   
  ngAfterViewInit() {

    // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.partyReportDatabase = new PartyHttpDao(this.partyService_in);

    //If the user changes sort order, reset back to first page
 
   this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
 
    this.getPartyTypes();
    this.getUsers();
 
  }

  getPartyTypes() {

    //let partyTypeIds: number[] = [];
    this.route_in.params.subscribe(params => {
      this.partyService.getAllPartyTypes().subscribe(response => {
        //console.log("res ", response);
        response.forEach(pType => {
          //console.log("pType"+pType)
          if (pType.id === +params['trantypeId']) {
            this.partyTypeId = pType.id;

            if (pType.name === AppSettings.PARTY_TYPE_CUSTOMER) {
              this.partyName = AppSettings.PARTY_TYPE_CUSTOMER
            }
            else {
              this.partyName = AppSettings.PARTY_TYPE_SUPPLIER
            }
          }
          this.getData();
        });
        

      });
    });
  }


  getData() {
 
    merge(this.sort.sortChange, this.paginator.page, this.searchButton$)
      .pipe(
        startWith({}),
        switchMap(() => {

          this.isLoadingResults = true;
          this.page.sortColumn = this.sort.active ? this.sort.active : "name";
          this.page.sortDirection = this.sort.direction ? this.sort.direction : "asc";
          this.page.pageNumber = this.paginator.pageIndex ? this.paginator.pageIndex : 0;
          this.page.size = this.paginator.pageSize ? this.paginator.pageSize : AppSettings.REPORT_PAGE_SIZE;
          return this.partyReportDatabase!.getPartyReport(this.partyTypeId, this.filterForm, this.page);
        }),
        map(data => {
          //Flip flag to show that loading has finished
          this.isLoadingResults = false;
          this.resultsLength = data.page.totalElements;
         //  console.log('data.materialList: ', data.data);

          return data.data;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          //console.log('catchError ',error);
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataSource.data = data;
        //console.log("resultsLength: "+this.resultsLength);

      });
  }

  onSearch() {
   
    this.searchButton$.next('Search')
  }
  
  
  close() {
    this._router_in.navigate(['/']);
  }
  
  
  saveAsExcel(): void {
    let fileName: string = this.partyName;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let filterText = "";
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      //  let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      // reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });

  }

 
  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.partyReportHeader();
      let reportItem: any[] = this.partyReportBody(pdfData);

      let reportName: string
      if (this.partyTypeId === 1) {
        reportName = "Customer Detailed Report"
      }
      if (this.partyTypeId === 2) {
        reportName = "Supplier Detailed Report"
      }
      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.partyName);
    });
 
  }


  partyReportHeader() {
    return [
      {
        "SL No": "SL No",
        "CompanyName": "CompanyName",
        "Address": "Address",
        "GstNumber": "GstNumber",
        "Email": "Email",
        "ContactPersonName": "ContactPersonName",
        "ContactPersonNumber": "ContactPersonNumber",
        "PrimaryMobile": "PrimaryMobile",

      },
 
    ];
 
  }




  partyReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: any[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    return reportItem;
 

  }


  makeReportItem(item: Party, count: number): any {

    return {
      "SL No": count,
      "CompanyName": item.name,
      "Address": item.address,
      "GstNumber": item.gstNumber,
      "Email": item.email,
      "ContactPersonName": item.contactPersonName,
      "ContactPersonNumber": item.contactPersonNumber,
      "PrimaryMobile": item.primaryMobile,
 
    }
  }


}

export class PartyHttpDao {

  constructor(private partyService: PartyService) { }

  getPartyReport(partyType: number, filterForm: FormGroup, page: Page): Observable<PagedData<Party>> {

        return this.partyService.getAllPartiesForReport(partyType, filterForm.value, page);
  }
}

export interface ExportDataType {
  "SL No": number;
  "CompanyName": string;
  "Address": string;
  "GstNumber": string;
  "Email": string;
  "ContactPersonName": string;
  "ContactPersonNumber": string;
  "PrimaryMobile": number;

}