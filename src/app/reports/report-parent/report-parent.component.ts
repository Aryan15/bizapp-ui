import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, merge, of as observableOf, Subject } from 'rxjs';
import { catchError, map, flatMap, debounceTime, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Party } from '../../data-model/party-model';
import { Material } from '../../data-model/material-model';
import { FormGroup, FormControl } from '@angular/forms';
import { AppSettings } from '../../app.settings';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { FinancialYear } from '../../data-model/financial-year-model';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { TransactionType } from '../../data-model/transaction-type';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { DatePipe, DecimalPipe } from '@angular/common';
import { Page } from '../model/page';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../data-model/user-model';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { ReferenceLinksComponent } from '../../reference-links/reference-links.component';
import { InvoiceHeader, InvoiceReportItemModel } from '../../data-model/invoice-model';
import { InvoiceItemReportComponent } from '../invoice-item-report/invoice-item-report.component';
import { InvoiceReportLetterHeadService } from '../../services/letter-head/invoice-report-letter-head';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-report-parent',
  templateUrl: './report-parent.component.html',
  styleUrls: ['./report-parent.component.scss']
})
export class ReportParentComponent implements OnInit, OnDestroy {
  rows = [];
  partyControl = new FormControl();
  isCustomer: boolean;
  isSupplier: boolean = false;

  pageSize: number;
  transaction: string;
  pageNumbers: number[] = [1,5,10,15,25,100];
  // pageNumbers: PageNum [] = [
  //   {id:"1",value: "1"},
  //   {id:"5",value: "5"},
  //   {id:"10",value: "10"},
  //   {id:"15",value: "15"},
  //   {id:"20",value: "25"},
  //   {id:"25",value: "100"}
  // ];
  public partyTypeIds: number[] = [];
  reportName: string;
  page = new Page();
  tranType: string;
  link: string;
  public parties: Party[] = [];
  partyPlaceHolder: string;
  financialYears: FinancialYear[] = [];
  transactionType: TransactionType;
  materialControl = new FormControl();
  invoiceControl =new FormControl();
  searchButton$ = new Subject<string>();
  initComplete: Subject<boolean> = new Subject();
  onDestroy: Subject<boolean> = new Subject();
  MinDate: Date;
  MaxDate: Date;
  planModel: any = { start_time: new Date() };
  filteredParties: Observable<Party[]>;

  filteredMaterials: Observable<Material[]>;
  filteredInvoices: Observable<InvoiceHeader[]>;
  filterForm = new FormGroup({
    transactionFromDate: new FormControl(),
    transactionToDate: new FormControl(),
    financialYearId: new FormControl(),
    partyId: new FormControl(),
    materialId: new FormControl(),
    partyTypeIds: new FormControl([]),
    createdBy: new FormControl(),
    statusName:new FormControl(),
    invoiceNumber:new FormControl(),

  });
  employees: UserModel[] = [];
  isJW: boolean = false;
  counterTTypeId: number = 0;
  jasperReportName: string;
  constructor(protected partyService: PartyService
    , private materialService: MaterialService
    , private financialYearService: FinancialYearService
    , private transactionTypeService: TransactionTypeService
    , private route: ActivatedRoute
    , private _router: Router
    , private userServiceParent: UserService
    
  ) { 
    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.pageSize=this.page.size; 
    //console.log("in cont", this.pageSize)
  }

  ngOnInit() {
  
  }

  initParent() {
     this.getDateRange();
    this.route.params.pipe(takeUntil(this.onDestroy)
      , flatMap(result => {
        if(+result['counterTTypeId']){
          this.counterTTypeId = +result['counterTTypeId']
        }
        if(result['jasperReportName']){
          this.jasperReportName = result['jasperReportName']
        }
        return this.transactionTypeService.getTransactionType(+result['trantypeId'])
      })
    )
      .subscribe(result => {
        //console.log('result2: ', result);
        this.transactionType = result;
        let sellTranTypes: string[] = [AppSettings.CUSTOMER_PO, AppSettings.CUSTOMER_INVOICE, AppSettings.CUSTOMER_DC, AppSettings.CUSTOMER_QUOTATION, AppSettings.PROFORMA_INVOICE, AppSettings.CREDIT_NOTE, AppSettings.CUSTOMER_RECEIVABLE, AppSettings.INCOMING_JOBWORK_INVOICE];
        let buyTranTypes: string[] = [AppSettings.SUPPLIER_PO, AppSettings.PURCHASE_INVOICE, AppSettings.INCOMING_DC, AppSettings.DEBIT_NOTE, AppSettings.OUTGOING_JOBWORK_PO, AppSettings.SUPPLIER_PAYABLE];
        //console.log('this.transactionType.name: ', this.transactionType.name);
        
        if (sellTranTypes.includes(this.transactionType.name)) {
          this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
          this.isCustomer = true;
          this.reportName = AppSettings.CUSTOMER_BALANCE_REPORT;
        } else if (buyTranTypes.includes(this.transactionType.name)) {
          this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
          this.isCustomer = false;
          this.reportName = AppSettings.SUPPLIER_BALANCE_REPORT;
        }
        //console.log('this.transactionType.name: ', this.transactionType.name,  this.isCustomer );
        if(this.transactionType.name === AppSettings.CUSTOMER_INVOICE ||this.transactionType.name === AppSettings.PURCHASE_INVOICE )
       {
        this.transaction = "Invoice"
       }
       else if(this.transactionType.name === AppSettings.CREDIT_NOTE)
       {
        this.transaction = "Credit Note"
       }
       else
       {
        this.transaction = "Debit Note"
       }

       let JW_TXN_LIST: string[]=[
        AppSettings.INCOMING_JOBWORK_INVOICE
      , AppSettings.INCOMING_JOBWORK_IN_DC
      , AppSettings.INCOMING_JOBWORK_OUT_DC
      , AppSettings.INCOMING_JOBWORK_PO
      , AppSettings.OUTGOING_JOBWORK_INVOICE
      , AppSettings.OUTGOING_JOBWORK_IN_DC
      , AppSettings.OUTGOING_JOBWORK_OUT_DC
      , AppSettings.OUTGOING_JOBWORK_PO
      ]
      // if(this.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE){
      if(JW_TXN_LIST.includes(this.transactionType.name)){
          this.partyPlaceHolder = AppSettings.PARTY_TYPE_VENDOR
          this.isJW = true;
      }else{
          this.isJW = false;
      }

        this.initComplete.next(true);
     
     
       
      }
    
    )
    this.initComplete.subscribe(response =>{
      
    this.getFinancialYears();
    this.getFilteredParties();
    this.getFiltredMaterial();
    //this.getFilteredInvoice();
    })

   
  }
  ngOnDestroy() {
    this.searchButton$.complete();
    this.searchButton$.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
  }
  getFilteredParties() {
    ////console.log('in getFilteredParties')
    let partyType: string = this.isCustomer ? AppSettings.PARTY_TYPE_CUSTOMER : AppSettings.PARTY_TYPE_SUPPLIER;
   if(this.isJW){
     partyType = AppSettings.PARTY_TYPE_ALL;
   }
   console.log("partyType",partyType);
    this.filteredParties = this.partyControl.valueChanges
      .pipe(
      startWith(''),
      debounceTime(300),
      map(value => typeof value === 'string' ? value : value.name),
      // tap(val => //console.log('party search: ', val)),
      //map(val => this.filterParty(val))
      switchMap(value => this.partyService.getPartiesByNameLike(partyType, value))
      );
  }

  // getFilteredInvoice() {
  //   let invType: string = this.isCustomer ? AppSettings.CUSTOMER_INVOICE : AppSettings.PURCHASE_INVOICE;
  
  //   this.filteredInvoices = this.invoiceControl.valueChanges
  //     .pipe(
  //     startWith(''),
  //     debounceTime(300),
  //     map(value => typeof value === 'string' ? value : value.invoiceNumber),
  //     // tap(val => //console.log('party search: ', val)),
  //     //map(val => this.filterParty(val))
  //     switchMap(value => this.invoieServie.getInvoiceByInvoiceId(invType, value))
  //     );
  // }


  getFinancialYears() {

    this.financialYearService.getFinancialYears()
      .subscribe(response => {
        this.financialYears = response;
      })
  }
  getFiltredMaterial() {
    this.filteredMaterials = this.materialControl.valueChanges
      .pipe(
      startWith(''),
      debounceTime(300),
      map(value => typeof value === 'string' ? value : value.name),
      
      // map(val => this.filterMaterial(val))
      switchMap(value => this.materialService.getMaterialByPartNumberOrPartName(value))
      );
  }

  partyChange(_event: any, party: Party) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(party.id);
    }
  }

  invoiceChange(_event: any, invoiceHeader: InvoiceHeader) {
    if (_event.isUserInput) {

      this.filterForm.get('partyId').patchValue(invoiceHeader.id);
    }
  }


  materialChange(_event: any, material: Material) {
    if (_event.isUserInput) {

      this.filterForm.get('materialId').patchValue(material.id);
    }
  }
  
  
  routerType : routerType[] = [
    {
      tranType:AppSettings.CUSTOMER_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.PURCHASE_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.PROFORMA_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.CUSTOMER_QUOTATION,
      link: "transaction/app-new-quotation"
    },
    {
      tranType:AppSettings.CUSTOMER_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.SUPPLIER_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.CUSTOMER_DC,
      link: "transaction/dc-form"
    },
    {
      tranType:AppSettings.INCOMING_DC,
      link: "transaction/dc-form"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_IN_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_OUT_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_IN_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_OUT_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.GRN_TYPE_SUPPLIER,
      link: "transaction/grn-form"
    },
    {
      tranType:AppSettings.GRN_TYPE_SUPPLIER,
      link: "transaction/grn-form"
    },

    {
      tranType:AppSettings.CUSTOMER_RECEIVABLE,
      link: "transaction/app-new-payable-receivable"
    },

    {
      tranType:AppSettings.SUPPLIER_PAYABLE,
      link: "transaction/app-new-payable-receivable"
    },

    {
      tranType:AppSettings.CREDIT_NOTE,
      link: "transaction/app-new-invoice"
    },

    {
      tranType:AppSettings.DEBIT_NOTE,
      link: "transaction/app-new-invoice"
    },
  ];
 

 

  showReference(id:string){
  let url: string = this.routerType.find(rl => rl.tranType === this.transactionType.name).link; //"app-new-invoice/"+this.data.tranTypeId;
  url = url+"/"+this.transactionType.id;
  
  this._router.navigate([url, {id:id}]);
  }
         
  displayFnParty(party?: Party): string | undefined {
    return party ?party.partyCode+"-"+party.name : undefined;
  } 
  
  displayFnInvoice(invoice?: InvoiceHeader): string | undefined {
    return invoice ?invoice.invoiceNumber : undefined;
  } 

  displayFnMaterial(material?: Material): string | undefined {
    return material ? material.name + (material.partNumber ? '[' + material.partNumber + ']' : '') : undefined;
  }

  // onSearch(transactionReportRequest:any) {

  //       let fromDate = this.filterForm.get('transactionFromDate').value;
  //       let toDate = this.filterForm.get('transactionToDate').value;
  //       let financialYearId = this.filterForm.get('financialYearId').value;
  //       let partyId = this.filterForm.get('partyId').value;
  //       let materialId = this.filterForm.get('materialId').value;


  //       this.transactionReportRequest.financialYearId = financialYearId;
  //       this.transactionReportRequest.partyId = partyId;
  //       this.transactionReportRequest.transactionFromDate = this.datePipe.transform(fromDate, AppSettings.DATE_FORMAT);
  //       this.transactionReportRequest.transactionToDate = this.datePipe.transform(toDate, AppSettings.DATE_FORMAT);
  //       //this.deliveryChallanReportRequest.partyTypeIds = partyTypeIds;

  //       this.deliveryChallanService.getDeliveryChallanItemReport(this.deliveryChallanReportRequest, this.page).subscribe(pagedData => {
  //         this.page = pagedData.page;
  //         this.rows = pagedData.data;
  //       });
  //     }




  clear() {
    
    this.filterForm.get('financialYearId').patchValue(null);
    this.filterForm.get('partyId').patchValue(null);
    this.filterForm.get('materialId').patchValue(null);
    this.partyControl.patchValue('');
    this.materialControl.patchValue('');
    this.filterForm.get('transactionFromDate').patchValue(null);
    this.filterForm.get('transactionToDate').patchValue(null);
    this.filterForm.get('createdBy').patchValue(null);
    this.filterForm.get('statusName').patchValue(null);
    this.rows = [];
    this.page.totalElements = 0;
    this.pageSize = AppSettings.REPORT_PAGE_SIZE;
  }

  close() {
    this._router.navigate(['/'])
  }
   priceSummary(cells: string[]) {
    
        let summaryGrandTotal: number = 0;
        cells.forEach(c => {
          // //console.log("C ",c);
          summaryGrandTotal += c ? +c : 0;
        });
    
        let decimalPipe = new DecimalPipe('en-IN');
        
        let ret = decimalPipe.transform(summaryGrandTotal ? summaryGrandTotal.toFixed(2) : 0,'1.2-2');
        // //console.log('ret :', ret);
        return ret; 
      }
  getDateRange() {
    let date = new Date();
    let dateOfMonth = date.getDate();
    //console.log("date", dateOfMonth ,date );
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
   // console.log ("1 firstDay  lastDay ",firstDay ,lastDay)
    if (dateOfMonth < 5) {
      firstDay = new Date(date.getFullYear(), date.getMonth() -1, 1);
      lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0);
     // console.log ("2 firstDay  lastDay ",firstDay ,lastDay)
    }

  //  console.log ("3 firstDay  lastDay ",firstDay ,lastDay)
      this.filterForm.get('transactionFromDate').patchValue(firstDay);
      this.filterForm.get('transactionToDate').patchValue(lastDay);
    
  }


  getUsers() {

    this.userServiceParent.getRecentUser(null, null, null,null,null).subscribe(
        response => {
            this.employees = response.users.filter(user => user.username);            
        }
    )

}




}

export interface routerType{
  tranType: string;
  link: string;
}