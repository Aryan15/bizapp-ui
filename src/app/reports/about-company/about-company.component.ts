import { Component, OnInit } from '@angular/core';
import { AboutCompany } from '../../data-model/company-model';
import { CompanyService } from '../../services/company.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about-company',
  templateUrl: './about-company.component.html',
  styleUrls: ['./about-company.component.scss']
})
export class AboutCompanyComponent implements OnInit {

  constructor(private companyService: CompanyService
    , private _router: Router) { }

  ngOnInit() {
    this.getAboutData();

  }
     aboutCompany: AboutCompany = {
      productName:null,
      productVersion:null,
      companyName:null,
      companyEmail:null,
      companyWebsite:null,
     companyContactNumber:null,
    };

  getAboutData(){

    this.companyService.getAboutCompany().subscribe(response => {
      //console.log('got response: ', response);
      this.aboutCompany=response;
      
      
    });
  }

  
  close() {
    this._router.navigate(['/'])
  }


}
