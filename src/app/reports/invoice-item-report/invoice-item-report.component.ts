import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { FinancialYear } from '../../data-model/financial-year-model';
import { Material } from '../../data-model/material-model';
import { Party } from '../../data-model/party-model';
import { TransactionType } from '../../data-model/transaction-type';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { InvoiceService } from '../../services/invoice.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { Page } from '../model/page';
import { LanguageService } from '../../services/language.service';

import { TranslateService } from '@ngx-translate/core';

const PAGE_SIZE: number = AppSettings.REPORT_PAGE_SIZE;

@Component({
  selector: 'app-invoice-item-report',
  templateUrl: './invoice-item-report.component.html',
  styleUrls: ['./invoice-item-report.component.scss'],
})
export class InvoiceItemReportComponent implements OnInit {



  rows = [];

  financialYears: FinancialYear[] = [];
  public parties: Party[] = [];
  public materials: Material[] = [];
  isLoadingResults = true;
  partyPlaceHolder: string;

  page = new Page();

  reorderable: true;
  pageSize: number;


  filterForm = new FormGroup({
    transactionFromDate: new FormControl(),
    transactionToDate: new FormControl(),
    financialYearId: new FormControl(),
    partyId: new FormControl(),
    materialId: new FormControl(),
    partyTypeIds: new FormControl([]),
  });
  isCustomer: boolean = false;
  isSupplier: boolean = false;
  transactionType: TransactionType;
  constructor(private financialYearService: FinancialYearService,
    private partyService: PartyService,
    private datePipe: DatePipe,
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    private _router: Router,
    private excelService: ExcelService,
    private materialService: MaterialService,
    private transactionTypeService: TransactionTypeService,
    private languageService: LanguageService,
    private translate: TranslateService,
   ) {
    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.pageSize = this.page.size;
    translate.setDefaultLang('en');
    this.getTransactionType();
  }

  ngOnInit() {

    this.getFinancialYears();

   // this.applyLanguage();

  }

  applyLanguage() {
    this.languageService.getLanguage()
      .subscribe(resp => {
        console.log("language changed: " + resp)
        this.translate.use(resp);
      })
  }

  setPage(pageInfo) {
    this.isLoadingResults = true;
    //console.log("in setPage: ", pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "invoiceDate";

    // this.page.size = AppSettings.REPORT_PAGE_SIZE;

    this.invoiceService.getInvoiceItemReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;

      this.page.size += 1;
      //console.log("this.rows ", this.rows)

      this.isLoadingResults = false;
    });
  }

  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.invoiceService.getInvoiceItemReport(this.transactionType.id, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }

  private getTransactionType() {
    this.route.params.subscribe(params => {
      this.transactionTypeService.getTransactionType(+params['trantypeId'])
        .subscribe(response => {
          //console.log('transaction type: ', response);
          this.transactionType = response;
          if (this.transactionType.name === AppSettings.CUSTOMER_INVOICE) {

            this.isCustomer = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER

            //console.log("this.isCustomer ", this.isCustomer, this.partyPlaceHolder);
          } else if (this.transactionType.name === AppSettings.PURCHASE_INVOICE) {
            this.isSupplier = true;
            this.partyPlaceHolder = AppSettings.PARTY_TYPE_SUPPLIER
            //console.log("this.isSupplier ", this.isSupplier, this.partyPlaceHolder);

          }
          this.setPage({ offset: 0 });
          this.getParties();
          this.getFinancialYears();
          this.getAllMaterials();
        })
    });
  }
  private getFinancialYears() {

    this.financialYearService.getFinancialYears()
      .subscribe(response => {
        this.financialYears = response;
      })
  }



  getParties() {

    if (this.isCustomer) {
      this.partyService.getCustomers().subscribe(
        response => {
          this.parties = response;
        }
      );
      this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
    }
    else if (this.isSupplier) {
      this.partyService.getSuppliers().subscribe(
        response => {
          this.parties = response;
        }
      );
      this.partyPlaceHolder = AppSettings.PARTY_TYPE_CUSTOMER
    }
  }
  getAllMaterials() {
    this.materialService.getAllMaterials().subscribe(
      response => {
        this.materials = response;
        //console.log("this.materils",   this.materials)
      }
    )
  }
  saveAsExcel(): void {
    let fileName: string = 'test';
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.invoiceService.getInvoiceItemReport(this.transactionType.id, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)

      let reports: any[] = [];

      excelData.forEach(data => {
        reports.push({
          "Invoice Number": data.invoiceNumber,
          "Invoice Date": this.datePipe.transform(data.invoiceDate, AppSettings.DATE_FORMAT),
          "Party Name": data.partyName,
          "Status": data.statusName,
          "Quantity": data.quantity,
          "Price": data.price,
          "Total Taxable Amount": data.totalTaxableAmount,
          "Discount Amount": data.discountAmount,
          "Cgst Tax Amount": data.cgstTaxAmount,
          "Sgst Tax Amount": data.sgstTaxAmount,
          "Igst Tax Amount": data.igstTaxAmount,
          "Grand Total": data.grandTotal
        })
      })

      this.excelService.exportFromJSON(reports, fileName);

    });


  }



  onSearch() {


    this.setPage({ offset: 0 });

  }

  setPageAfterPageLimit() {
    this.page.size = this.pageSize;
    this.setPage({ offset: 0 });
  }
  close() {
    this._router.navigate(['/']);
  }
  getCellClass({ row, column, value }) {
    return value > 10 ? "color:red" : "color:green";
  }

  clear() {

    this.filterForm.get('financialYearId').patchValue(null);
    this.filterForm.get('partyId').patchValue(null);
    this.filterForm.get('materialId').patchValue(null);
    this.filterForm.get('transactionFromDate').patchValue(null);
    this.filterForm.get('transactionToDate').patchValue(null);
    this.setPage({ offset: 0 });
  }


}

