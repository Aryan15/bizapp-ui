import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceItemReportComponent } from './invoice-item-report.component';

describe('InvoiceItemReportComponent', () => {
  let component: InvoiceItemReportComponent;
  let fixture: ComponentFixture<InvoiceItemReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceItemReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceItemReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
