import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyDetailedReportComponent } from './party-detailed-report.component';

describe('PartyDetailedReportComponent', () => {
  let component: PartyDetailedReportComponent;
  let fixture: ComponentFixture<PartyDetailedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyDetailedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyDetailedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
