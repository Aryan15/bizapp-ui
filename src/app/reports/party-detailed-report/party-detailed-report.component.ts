import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { PartyType, Party } from '../../data-model/party-model';
import { ExcelService } from '../../services/excel.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { MaterialService } from '../../services/material.service';
import { PartyService } from '../../services/party.service';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { UserService } from '../../services/user.service';
import { Page } from '../model/page';
import { ReportParentComponent } from '../report-parent/report-parent.component';
import { PdfService } from '../../services/pdf.service';
import { JasperPrintService } from '../../services/jasper-print.service';

@Component({
  selector: 'app-party-detailed-report',
  templateUrl: './party-detailed-report.component.html',
  styleUrls: ['./party-detailed-report.component.scss'],
})
export class PartyDetailedReportComponent extends ReportParentComponent implements OnInit {
  rows = [];
  partyType: PartyType;
  pdfData: any;
  public partyTypeId: number;
  partyName: string;
  page = new Page();
  reorderable: true;
  subheading: string;
  xmlData;

  constructor(private financialYearService_in: FinancialYearService,
    private partyService_in: PartyService,
    private datePipe: DatePipe,
    private _router_in: Router,
    public route_in: ActivatedRoute,
    private excelService: ExcelService,
    private materialService_in: MaterialService,
    private transactionTypeService_in: TransactionTypeService,
    public userService: UserService,
    private pdfService: PdfService,
    private jasperPrintService: JasperPrintService) {
    super(
      partyService_in,
      materialService_in,
      financialYearService_in,
      transactionTypeService_in,
      route_in,
      _router_in,
      userService
    );
    this.page.pageNumber = 0;
    this.page.size = AppSettings.REPORT_PAGE_SIZE;
    this.subheading = AppSettings.REPORT_SUB_HEADING_LINE + this.userService.reportName;
  }


  ngOnInit() {
    this.initParent();

    this.getPartyTypes();
    //this.setPage({ offset: 0 });
    this.getUsers();

  }

  onSearch() {
    this.setPage({ offset: 0 });
  }
  // private getFinancialYears() {

  //   this.financialYearService.getFinancialYears()
  //     .subscribe(response => {
  //       this.financialYears = response;
  //       this.setPage({ offset: 0 });
  //     })
  // }

  getPartyTypes() {

    //let partyTypeIds: number[] = [];
    this.route_in.params.subscribe(params => {
      this.partyService.getAllPartyTypes().subscribe(response => {
        //console.log("res ", response);
        response.forEach(pType => {
          //console.log("pType"+pType)
          if (pType.id === +params['trantypeId']) {
            this.partyTypeId = pType.id;

            if (pType.name === AppSettings.PARTY_TYPE_CUSTOMER) {
              this.partyName = AppSettings.PARTY_TYPE_CUSTOMER
            }
            else {
              this.partyName = AppSettings.PARTY_TYPE_SUPPLIER
            }
          }
        });
        this.setPage({ offset: 0 });

      });
    });
  }

  setPage(pageInfo) {
    //console.log("in setPage: ", pageInfo);
    let filterText = "";
    this.page.pageNumber = pageInfo.offset;
    this.page.sortColumn = this.page.sortColumn ? this.page.sortColumn : "name";
    //console.log("in setPage this.partyTypeId", this.partyTypeId)
    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      //console.log('this.rows: ', this.rows);
    });
  }
  onSort(event) {
    //console.log('onSort: ', event);

    //console.log('column: ', event.column.name);
    //console.log('column prop: ', event.column.prop);
    //console.log('order: ', event.newValue);

    this.page.pageNumber = 0;
    this.page.sortColumn = event.column.prop;
    this.page.sortDirection = event.newValue;

    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, this.page).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

  }
  saveAsExcel(): void {
    let fileName: string = this.partyName;
    // this.excelService.exportAsExcelFile(this.pagedData.data, fileName);
    //this.excelService.export();
    let filterText = "";
    let excelPage = { ...this.page };
    //console.log('excelPage: ', excelPage);
    let excelData: any[] = [];
    excelPage.size = -99;
    excelPage.pageNumber = 0;
    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, excelPage).subscribe(pagedData => {
      excelData = pagedData.data;
      //console.log("this.rows ", excelData)
      let reports: ExportDataType[] = [];

      excelData.forEach((data, index) => {
        reports.push(
          this.makeReportItem(data, index + 1)
        )
      })

      //  let summaryRow: ExportDataType = this.makeSummaryRow(excelData);

      // reports.push(summaryRow);
      this.excelService.exportFromJSON(reports, fileName);

    });

  }


  public savePartyAddressAsPdf(){
    this.jasperReportName="PartyAddressReport";

    this.jasperPrintService.jasperReport(
      this.partyTypeId, this.counterTTypeId, this.jasperReportName, this.filterForm.value, this.page
     )
     .subscribe(response => {
      
       this.pdfData = response;
       console.log(this.pdfData+" this.pdfData");
       this.download();
       //this.pdfGenerated = true;
     })

    


  }

  download(){
    const pdfUrl = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.pdfData], { type: 'application/pdf' }));
    const anchor = document.createElement('a');
    anchor.href = pdfUrl;
    anchor.setAttribute("download", "PartyAddressReport");
    anchor.click();
  }

 

  saveAsPdf(): void {
    let pdfPage = { ...this.page };
    console.log('pdfPage: ');
    let reportObject: any;
    pdfPage.size = -99;
    pdfPage.pageNumber = 0;
    let pdfData: any[] = [];
    this.partyService.getAllPartiesForReport(this.partyTypeId, this.filterForm.value, pdfPage).subscribe(pagedData => {
      pdfData = pagedData.data;

      let reportHeader: any[] = this.partyReportHeader();
      let reportItem: any[] = this.partyReportBody(pdfData);

      let reportName: string
      if (this.partyTypeId === 1) {
        reportName = "Customer Detailed Report"
      }
      if (this.partyTypeId === 2) {
        reportName = "Supplier Detailed Report"
      }
      this.pdfService.exportToPdf(reportName, reportHeader, reportItem, this.partyName);
    });



  }


  partyReportHeader() {
    return [
      {
        "SL No": "SL No",
        "CompanyName": "CompanyName",
        "PartyCode":"PartyCode",
        "Address": "Address",
        "GstNumber": "GstNumber",
        "Email": "Email",
        "ContactPersonName": "ContactPersonName",
        "ContactPersonNumber": "ContactPersonNumber",
        "PrimaryMobile": "PrimaryMobile",

      },



    ];




  }




  partyReportBody(invoiceReport: any[]) {

    let count: number = 0

    let reportItem: any[] = [];
    invoiceReport.forEach((invoice, index) => {

      reportItem.push(this.makeReportItem(invoice, index + 1));
    });

    return reportItem;





  }


  makeReportItem(item: Party, count: number): any {

    return {
      "SL No": count,
      "CompanyName": item.name,
      "PartyCode":item.partyCode,
      "Address": item.address,
      "GstNumber": item.gstNumber,
      "Email": item.email,
      "ContactPersonName": item.contactPersonName,
      "ContactPersonNumber": item.contactPersonNumber,
      "PrimaryMobile": item.primaryMobile,







    }
  }




  //  exportToTally(){

  //      console.log("this.transactionType.id ", this.transactionType.id);
  //      this.partyService.getTallyPartiesXML(this.partyTypeId.toString())
  //     .subscribe(response => {
  //       this.xmlData = response;
  //    this.download();
  //   })
  // }

  // exportToTallyUOM(){

  //   this.partyService.getTallyUomXML()
  //   .subscribe(response => {
  //     this.xmlData = response;
  //     this.download();
  //   })
  // }

  // exportToTallyGST(){

  //   this.partyService.getTallyGstXML()
  //   .subscribe(response => {
  //     this.xmlData = response;
  //     this.download();
  //   })
  // }

  // exportToTallySTOCK(){

  //   this.partyService.getTallyStockXML()
  //   .subscribe(response => {
  //     this.xmlData = response;
  //     this.download();
  //   })
  // }

  // exportToTallyBANK(){

  //   this.partyService.getTallyBankXML()
  //   .subscribe(response => {
  //     this.xmlData = response;
  //     this.download();
  //   })
  // }
  // download(){
  //   const xmlUrl = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.xmlData], { type: 'application/xml' }));
  //   const anchor = document.createElement('a');
  //   anchor.href = xmlUrl;
  //   anchor.setAttribute("download", this.transactionType.name);
  //   anchor.click();
  // }
 

}

export interface ExportDataType {
  "SL No": number;
  "CompanyName": string;
  "PartyCode":string;
  "Address": string;
  "GstNumber": string;
  "Email": string;
  "ContactPersonName": string;
  "ContactPersonNumber": string;
  "PrimaryMobile": number;

}

