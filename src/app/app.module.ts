import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import localeEnIn from '@angular/common/locales/en-IN';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { LoginComponent } from './login/login.component';
import { MenuItemComponent } from './menu/menu-item.component';
import { MenuComponent } from './menu/menu.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SecurePipe } from './pipes/secure.pipe';
import { NestedTable } from './reports/nested-table/nested-table';
import { BankService } from './services/bank.service';
import { CompanyService } from './services/company.service';
import { DeliveryChallanReportService } from './services/delivery-challan-report.service';
import { DeliveryChallanService } from './services/delivery-challan.service';
import { ExcelService } from './services/excel.service';
import { FileUploadClientService } from './services/file-client.service';
import { FinancialYearService } from './services/financial-year.service';
import { GlobalSettingService } from './services/global-setting.service';
import { GrnService } from './services/grn.service';
import { ImgMaxPXSizeService } from './services/image.service';
import { InvoiceService } from './services/invoice.service';
import { LocationService } from './services/location.service';
import { MaterialTypeService } from './services/material-type.service';
import { MaterialService } from './services/material.service';
import { MenuSettingService } from './services/menu-setting.service';
import { NumberRangeConfigService } from './services/number-range-config.service';
import { NumberToWordsService } from './services/number-to-words.service';
import { PartyService } from './services/party.service';
import { PayableReceivableItemService } from './services/payable-receivable-item.service';
import { PayableReceivableService } from './services/payable-receivable.service';
import { PaymentMethodService } from './services/payment-method.service';
import { PrintWrapperService } from './services/print-warpper.service';
import { PurchaseOrderService } from './services/purchase-order.service';
import { QuotationService } from './services/quotation.service';
import { StockTraceService } from './services/stock-trace.service';
import { TaxService } from './services/tax.service';
import { InvoiceReportService } from './services/template1/invoice-report.service';
import { PurchaseOrderReportService } from './services/template1/purchase-order-report.service';
import { QuotationReportService } from './services/template1/quotation-report.service';
import { CustomerReceiptstService } from './services/template2/customer-receipts.service';
import { DeliveryChallanSecondReportService } from './services/template2/delivery-challan-report.service';
import { GrnReportService } from './services/template2/grn-report.service';
import { InvoiceReportSecondService } from './services/template2/invoice-report.service';
import { PurchaseOrderSecondReportService } from './services/template2/purchase-order-report.service';
//import { QuotationSecondReportImageService } from './services/template2/quotation-report-image.service';
import { QuotationSecondReportService } from './services/template2/quotation-report.service';
import { VoucherSecondReportService } from './services/template2/voucher-report.service';
import { TermsAndConditionsService } from './services/terms-conditions.service';
import { TokenInterceptor } from './services/token.interceptor';
import { TokenService } from './services/token.service';
import { TransactionItemService } from './services/transaction-item.service';
import { TransactionSummaryService } from './services/transaction-summary.service';
import { TransactionTypeService } from './services/transaction-type.service';
import { UomService } from './services/uom.service';
import { CurrencyService } from './services/currency.service';
import { UrlHelperService } from './services/url-helper.service';
import { UserService } from './services/user.service';
import { VoucherService } from './services/voucher.service';
import { ChatDialogComponent } from './settings/chat-dialog/chat-dialog.component';
import { SharedModule } from './shared/shared.module';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { AlertDialog } from './utils/alert-dialog';
import { ConfirmationDialog } from './utils/confirmation-dialog';
import { DisableControlDirective } from './utils/disable-control.directive';
import { PreventDoubleSubmitDirective } from './utils/double-submit.directive';
import { PrintDialog } from './utils/print-dialog';
import { PrintDialogContainer } from './utils/print-dialog-container';
import { UtilityService } from './utils/utility-service';

import { PettyCashService } from './services/pettyCash.service';



import { PettyCashItemComponent } from './transaction/petty-cash/petty-cash-item.component';
import { PdfService } from './services/pdf.service';
import { HelpVideosComponent } from './help-videos/help-videos.component';

import {MatDialogModule, MatDialogTitle, MatDialogRef} from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material'
import { AutoTransactionMasterComponent } from './master/auto-transaction-master/auto-transaction-master.component';
import { RecentAutoTransactionComponent } from './master/auto-transaction-master/recent-auto-transaction.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ExportNumberToWordsService } from './services/export-number-currency-to-words.service ';
import {CurrencyPipe} from '@angular/common';



// import { DeleteBottomSheet } from './action-panel/action-panel.component';
// import { MasterSubHeadingComponent } from './master/master-sub-heading/master-sub-heading.component';
registerLocaleData(localeEnIn);
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http,'./assets/i18n/', '.json');
 
 }
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    MenuComponent,
    MenuItemComponent,
    DashboardComponent,
    ConfirmationDialog,
    AlertDialog,
    PrintDialog,
    PrintDialogContainer,
    // SecurePipe,
    UserRegistrationComponent,
    PreventDoubleSubmitDirective,
    DisableControlDirective,
    DashboardComponent,
    NotFoundComponent,
    ChatDialogComponent,
    ForgetPasswordComponent,
    NestedTable,
 
    
    
    
  
   
    
 
   

  ],

  imports: [

    BrowserModule,
    SharedModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
   
    TranslateModule.forRoot({
     
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
    },
  
      
    }),
    

    Ng2ImgMaxModule,
    
     
    
  
  ],
  providers: [AuthService, 
    AuthGuard, 
    UserService,
    TokenService,
    { provide: MatDialogTitle, useValue: {} }, { provide: MatDialogRef, useValue: {} }, { provide: MAT_DIALOG_DATA, useValue: [] },
    {
    
      provide: HTTP_INTERCEPTORS, 
      useClass: TokenInterceptor,
      multi: true
    },
    CustomerReceiptstService,
    InvoiceService,
    PartyService, MaterialService, UomService,CurrencyService,TaxService,ExportNumberToWordsService,
    FinancialYearService, PurchaseOrderService, 
    TransactionItemService, DeliveryChallanService, QuotationService,
    PayableReceivableService, PayableReceivableItemService,UtilityService,
    TransactionTypeService,  GrnService, TermsAndConditionsService, VoucherService,
    CompanyService, UrlHelperService,MaterialTypeService,StockTraceService, 
    DatePipe, NumberRangeConfigService, GlobalSettingService,LocationService,BankService,
    MenuSettingService,NumberToWordsService,InvoiceReportService,InvoiceReportSecondService,
    PurchaseOrderReportService,PurchaseOrderSecondReportService,QuotationSecondReportService,
    QuotationReportService, DeliveryChallanReportService, DeliveryChallanSecondReportService,
    GrnReportService,CurrencyPipe,
    ExcelService,VoucherSecondReportService,PettyCashService,PdfService,

    {provide: 'Window', useValue: window}, TransactionSummaryService, PaymentMethodService,
     
    FileUploadClientService,
    PrintWrapperService,
    { provide: LOCALE_ID, useValue: "en-IN" },
    ImgMaxPXSizeService,
   
  ],
  
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialog, AlertDialog, PrintDialog, PrintDialogContainer,HelpVideosComponent]
  

})
export class AppModule { 
  
}
