import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { DashboardTransactionSummary } from '../data-model/dashboard-transaction-summary-model';


@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
 
  @Input() dashboardTransactionSummary: DashboardTransactionSummary;
  @Input() paymentType: string;
  @Input() secondSection: boolean;
  @Input() recievedOrPayedAmount: number
  
  amountDifferenceMessage: string;
  ngOnInit(){

    if(this.dashboardTransactionSummary.transactionAmount - this.dashboardTransactionSummary.dueAmount != this.recievedOrPayedAmount){
      this.amountDifferenceMessage = "Difference could be because of Credit/Debit notes";
    }else{
      this.amountDifferenceMessage = "";
    }
  }


}