import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { MaterialComponent } from '../master/material/material.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';

export interface FormComponent {
    materialForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardMaterial implements CanDeactivate<MaterialComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: MaterialComponent) {

        //console.log("component.materialForm.dirty Name:", component.saveStatus)
        if (component.materialForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}