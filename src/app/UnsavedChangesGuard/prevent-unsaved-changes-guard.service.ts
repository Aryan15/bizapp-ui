import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { EmployeeManagementComponent } from '../master/employee-management/employee-management.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {

    employeeManagementForm: FormGroup;

}
@Injectable()
export class PreventUnsavedChangesGuard implements CanDeactivate<EmployeeManagementComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: EmployeeManagementComponent) {

        //console.log("component Name:", component.employeeManagementForm)
        if (component.employeeManagementForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}