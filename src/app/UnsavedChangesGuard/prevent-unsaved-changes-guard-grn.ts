import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { GrnComponent } from '../transaction/grn/grn.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    grnForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardGrn implements CanDeactivate<GrnComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: GrnComponent) {

        //console.log("component Name:", component.grnForm)
        if (component.grnForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}