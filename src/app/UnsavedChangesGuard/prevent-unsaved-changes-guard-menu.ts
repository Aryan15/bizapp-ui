import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { UserRoleComponent } from '../master/user-role/user-role.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    userRoleForm: FormGroup;

}
@Injectable()
export class PreventUnsavedChangesGuarduserRole implements CanDeactivate<UserRoleComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: UserRoleComponent) {

        // //console.log("component Name:", component.userRoleForm)
        // if (component.userRoleForm.dirty && component.saveStatus === false) {
        //     this.dialogRef = this.dialog.open(ConfirmationDialog, {
        //         disableClose: false
        //     });

        //     this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
        //     return this.dialogRef.afterClosed();

        // }
        return true;
    }

}