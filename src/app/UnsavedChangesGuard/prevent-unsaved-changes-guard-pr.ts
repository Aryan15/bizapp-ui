import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { NewPayableReceivableComponent } from '../transaction/new-payable-receivable/new-payable-receivable.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    prForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardPr implements CanDeactivate<NewPayableReceivableComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: NewPayableReceivableComponent) {

        //console.log("component Name:", component.prForm)
        if (component.prForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}