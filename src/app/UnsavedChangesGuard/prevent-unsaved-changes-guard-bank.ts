import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { BankMasterComponent } from '../master/bank-master/bank-master.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    bankForm: FormGroup;
   
}

@Injectable()
export class PreventUnsavedChangesGuardBank implements CanDeactivate<BankMasterComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: BankMasterComponent) {

        // //console.log("component Name:", component.bankForm)
        if (component && component.bankForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}