import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { JobWorkDeliveryChallanComponent} from '../transaction/job-work-delivery-challan/job-work-delivery-challan.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    tranForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardJobWorkDc implements CanDeactivate<JobWorkDeliveryChallanComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: JobWorkDeliveryChallanComponent) {

        //console.log("component Name:", component.tranForm)
        //console.log("component.saveStatus:", component.saveStatus)
        if (component.dcForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}