import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { PartyComponent } from '../master/party/party.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    partyForm: FormGroup;
   
}

@Injectable()
export class PreventUnsavedChangesGuardParty implements CanDeactivate<PartyComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: PartyComponent) {

        //console.log("component Name:", component.partyForm)
        if (component.partyForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}