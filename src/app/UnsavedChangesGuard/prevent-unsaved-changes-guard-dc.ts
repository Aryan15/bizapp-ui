import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { DeliveryChallanComponent } from '../transaction/delivery-challan/dc.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    dcForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardDC implements CanDeactivate<DeliveryChallanComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: DeliveryChallanComponent) {

        //console.log("component Name:", component.dcForm)
        if (component.dcForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}