import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { GlobalSettingsComponent } from '../settings/global-settings/global-settings.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    settingsForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardSettings implements CanDeactivate<GlobalSettingsComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: GlobalSettingsComponent) {

        //console.log("component Name:", component.settingsForm)
        if (component.settingsForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}