import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { NewPoComponentComponent } from '../transaction/new-po-component/new-po-component.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    tranForm: FormGroup;

}
@Injectable()
export class PreventUnsavedChangesGuardPo implements CanDeactivate<NewPoComponentComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: NewPoComponentComponent) {

        //console.log("component Name:", component.tranForm)
        if (component.tranForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}