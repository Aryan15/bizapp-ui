import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate } from '@angular/router';
import { AppSettings } from '../app.settings';
import { CompanyComponent } from '../master/company/company.component';
import { ConfirmationDialog } from '../utils/confirmation-dialog';
export interface FormComponent {
    companyForm: FormGroup;
   
}
@Injectable()
export class PreventUnsavedChangesGuardCompany implements CanDeactivate<CompanyComponent> {
    dialogRef: MatDialogRef<ConfirmationDialog>;
    constructor(public dialog: MatDialog) {

    }

    canDeactivate(component: CompanyComponent) {

        //console.log("component Name:", component.companyForm)
        if (component.companyForm.dirty && component.saveStatus === false) {
            this.dialogRef = this.dialog.open(ConfirmationDialog, {
                disableClose: false
            });

            this.dialogRef.componentInstance.confirmMessage = AppSettings.UNSAVED_CHANGES_MESSAGE
            return this.dialogRef.afterClosed();

        }
        return true;
    }

}