import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppSettings } from '../app.settings';
import { TransactionTypeService } from '../services/transaction-type.service';
import { TransactionStatusChange } from '../data-model/transaction-type';

@Component({
  selector: 'app-reference-links',
  templateUrl: './reference-links.component.html',
  styleUrls: ['./reference-links.component.scss']
})
export class ReferenceLinksComponent implements OnInit {

  routerLinks : RouterLink[] = [
    {
      tranType:AppSettings.CUSTOMER_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.PURCHASE_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.PROFORMA_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_INVOICE,
      link: "transaction/app-new-invoice"
    },
    {
      tranType:AppSettings.CUSTOMER_QUOTATION,
      link: "transaction/app-new-quotation"
    },
    {
      tranType:AppSettings.CUSTOMER_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.SUPPLIER_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_PO,
      link: "transaction/app-new-po-component"
    },
    {
      tranType:AppSettings.CUSTOMER_DC,
      link: "transaction/dc-form"
    },
    {
      tranType:AppSettings.INCOMING_DC,
      link: "transaction/dc-form"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_IN_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.INCOMING_JOBWORK_OUT_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_IN_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.OUTGOING_JOBWORK_OUT_DC,
      link: "transaction/app-job-work-delivery-challan"
    },
    {
      tranType:AppSettings.GRN_TYPE_SUPPLIER,
      link: "transaction/grn-form"
    },
    {
      tranType:AppSettings.GRN_TYPE_SUPPLIER,
      link: "transaction/grn-form"
    },
  ];

  prevReferenceLinks: TransactionStatusChange[] = [];
  linkReferenceLinks: TransactionStatusChange[] = [];

  
  // prevReferenceLinks: ReferenceLink[] = [
  //   {
  //     sourceTransactionTypeId: 1,
  //     sourceTransactionType: AppSettings.CUSTOMER_INVOICE,
  //     sourceTransactionId: "8f162feb-3a7d-4e80-bff7-8549acf8fae6",
  //     linkTransactionId: "abc",
  //     sourceNumber: "PO01",
  //     linkNumber: "DC01",
  //     sourceStatus: "DC Generated",
  //     linkStatus: "New",
  //     datetime:"2018-07-05",
  //   },
  //   {
  //     sourceTransactionTypeId: 7,
  //     sourceTransactionType: AppSettings.CUSTOMER_DC,
  //     sourceTransactionId: "bbbb",
  //     linkTransactionId: "cccc",
  //     sourceNumber: "DC01",
  //     linkNumber: "INV01",
  //     sourceStatus: "Invoiced",
  //     linkStatus: "New",
  //     datetime:"2018-07-05",
  //   },
  // ]

  // nextReferenceLinks: ReferenceLink[] = [
  //   {
  //     sourceTransactionTypeId: 1,
  //     sourceTransactionType: AppSettings.CUSTOMER_INVOICE,
  //     sourceTransactionId: "jjj",
  //     linkTransactionId: "sss",
  //     sourceNumber: "INV01",
  //     linkNumber: "PR01",
  //     sourceStatus: "Invoice Complete",
  //     linkStatus: "New",
  //     datetime:"2018-07-06",
  //   }
  // ]
  constructor(private dialogRef: MatDialogRef<ReferenceLinksComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private transactionTypeService: TransactionTypeService) { }

  ngOnInit() {

    //console.log('in init: ', this.data);
    if(this.data.tranId){
      this.transactionTypeService.getSourceTransactionStatuses(this.data.tranId,this.data.tranTypeId).subscribe(response => {
        this.prevReferenceLinks = response;
        //console.log('response: ', response);
      });

      this.transactionTypeService.getLinkTransactionStatuses(this.data.tranId).subscribe(response => {
        this.linkReferenceLinks = response;
        //console.log('response: ', response);
      });
    }
  }

  close(): void {
    //console.log('close clicked');
    this.dialogRef.close();
    // this.dialogRef.close();
  }

  openTran(transactionId: string, transactionTypeName: string, transactionTypeId: number){
    //console.log('transactionTypeName: ',transactionTypeName);
    let url: string = this.routerLinks.find(rl => rl.tranType === transactionTypeName).link; //"app-new-invoice/"+this.data.tranTypeId;
    url = url+"/"+transactionTypeId;
    //console.log('in openTran: ',transactionId, url);
    this.dialogRef.close({url: url, id: transactionId});
    
  }

}



export interface RouterLink{
 tranType: string;
 link: string; 
}