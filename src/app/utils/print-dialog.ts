import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'print-dialog',
    templateUrl: 'print-dialog.html',
  })
  export class PrintDialog {
  
    printCopy = new FormControl('1');

    constructor(
      public dialogRef: MatDialogRef<PrintDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) { 
        //console.log('copy: ', data.printCopy)
        //this.printCopy.patchValue(data.printCopy);
      }
  
    onCloseClick(): void {
      this.dialogRef.close();
    }
  
  }