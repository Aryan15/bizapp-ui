
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { AppSettings } from '../app.settings';
import { FinancialYear } from '../data-model/financial-year-model';
import { FinancialYearService } from '../services/financial-year.service';
@Injectable()
export class UtilityService {
    financialYear: FinancialYear;
    minDate: Date;
    maxDate: Date;
    date: Date;
    constructor(private financialYearService: FinancialYearService, ) {
        // this.getFinancialYear();
    }

    getCurrentDate(): string {
        ////console.log("date")
        let dp = new DatePipe(navigator.language);
        //let dtr = dp.transform(new Date(), 'yyyy-MM-dd');
        //console.log("dp", dp)
        let dtr = dp.transform(new Date(), AppSettings.DATE_FORMAT);
        // this.getFinancialYear();
        //console.log("dp.transform(dtr, AppSettings.DATE_FORMAT))", dp.transform(dtr, AppSettings.DATE_FORMAT));
        this.date = new Date(dtr);
        this.financialYearService.getFinancialYear().subscribe(
            response => {
                //console.log("response in finyr",response)
                this.financialYear = response;
                this.minDate = new Date(response.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                this.maxDate = new Date(response.endDate);
                //console.log("this.minDate", this.minDate)
                //console.log("this.maxDate", this.maxDate)


        
                if (this.minDate <= this.date  &&  this.maxDate >= this.date) {
                    //console.log("this.date1", this.date)
                    return dtr;
                }
                else {
                    //console.log("this.date2", this.date)
                    return null;
                }
              
            }
        );

        //setTimeout( () => {
            return null;
        //}, 3000)
        

    }


    getCurrentDateObs(): Observable<string> {
        ////console.log("date")
        let returnDate: Observable<string> = null;
        let dp = new DatePipe(navigator.language);
        //let dtr = dp.transform(new Date(), 'yyyy-MM-dd');
        //console.log("dp", dp)
        let dtr = dp.transform(new Date(), AppSettings.DATE_FORMAT);
        // this.getFinancialYear();
        //console.log("dp.transform(dtr, AppSettings.DATE_FORMAT))", dp.transform(dtr, AppSettings.DATE_FORMAT));
        this.date = new Date(dtr);
        
        returnDate = new Observable(observer => {
            this.financialYearService.getFinancialYear().subscribe(
                response => {
                    //console.log("response in finyr",response)
                    this.financialYear = response;
                    this.minDate = new Date(response.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
                    this.maxDate = new Date(response.endDate);
                    //console.log("this.minDate", this.minDate)
                    //console.log("this.maxDate", this.maxDate)
    
    
            
                    if (this.minDate <= this.date  &&  this.maxDate >= this.date) {
                        //console.log("this.date1", this.date)
                        observer.next(dtr);
                    }
                    else {
                        //console.log("this.date2", this.date)
                        observer.next(null);
                    }
                  
                }
            );
        }) 


        return returnDate;

        

    }

    findInvalidControlsRecursive(formToInvestigate:FormGroup|FormArray):string[] {
       // console.log("formToInvestigate"+formToInvestigate);

        var invalidControls:string[] = [];
        let recursiveFunc = (form:FormGroup|FormArray ) => {
          Object.keys(form.controls).forEach(field => { 
            const control = form.get(field);
           //console.log("control"+control +"field"+field);
            if (control.invalid) 
            {
               
                    invalidControls.push(field);
               
           
            }
            if (control instanceof FormGroup) {
              recursiveFunc(control);
            } else if (control instanceof FormArray) {
              recursiveFunc(control);
            }        
          });
        }
        recursiveFunc(formToInvestigate);
        return invalidControls;
      }

    // getFinancialYear() {
    //     this.financialYearService.getFinancialYear().subscribe(
    //         response => {
    //             this.financialYear = response;
    //             alert("response"+ this.financialYear)
    //             this.minDate = new Date(response.startDate);//this.datePipe.transform(startDate, AppSettings.DATE_FORMAT);
    //             this.maxDate = new Date(response.endDate);
    //             //console.log("this.minDate", this.minDate)
    //             //console.log("this.maxDate", this.maxDate)
    //             this.getCurrentDate();
    //         }
    //     )
    // }
}