import { Injectable } from "@angular/core";
import { DecimalPipe } from "@angular/common";

@Injectable({
    providedIn: 'root'
  })
export class NumberFormatterService {

    decimalPipe = new DecimalPipe('en-IN');
    decimalPipe1 = new DecimalPipe('en-US');

    numberF(input: number): string {
        return this.decimalPipe.transform(input,'1.2-2');
    }

    numberI(input:number):string{
        return this.decimalPipe.transform(input,'1.0-0');
    }
     
    numberC(input:number):string{
        return this.decimalPipe1.transform(input,'3.2-2');
    }
}