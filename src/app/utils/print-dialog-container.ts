import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppSettings } from '../app.settings';
import { PrintCopiesWrapper, PrintCopy } from '../data-model/print-copies-model';
import { UserModel } from '../data-model/user-model';
import { GlobalSettingService } from '../services/global-setting.service';
import { JasperPrintService } from '../services/jasper-print.service';
import { NumberRangeConfigService } from '../services/number-range-config.service';
import { NumberToWordsService } from '../services/number-to-words.service';
import { PrintWrapperService } from '../services/print-warpper.service';
import { PrintService } from '../services/print.service';
import { LanguageService } from '../services/language.service';
import { TranslateService } from '@ngx-translate/core';
import { Company } from '../data-model/company-model';
import { CompanyService } from '../services/company.service';
import { ExportNumberToWordsService } from '../services/export-number-currency-to-words.service ';

@Component({
  selector: 'print-dialog-container',
  templateUrl: 'print-dialog-container.html',
})
export class PrintDialogContainer implements OnInit, OnDestroy {

  header_length: number = 27;
  public printCopys: PrintCopy[] = [];
  printCopiesWrapper: PrintCopiesWrapper[] = [];
  printCopy = new FormControl('Original');
  topMargin = new FormControl(AppSettings.PRINT_TEMPLATE_TOP_MARGIN);
  printHeaderText = new FormControl();
  signature = new FormControl(true);
  shipAddress = new FormControl(true);

  pdfData;
  pdfGenerated: boolean = false;
  spinStart: boolean = true;
  exportToExcel: boolean = false;
  exportToWord: boolean = false;
  signatureImage: boolean = false;
  printHeaderTextValue: boolean = false;
  dispalyShippingAddressCheckBox:boolean=false;
  themeClass: any;
  currentUser: UserModel;
  showMargin: boolean = false;
  signatureVal: number = 0;
  allowshipAddress:number =1;
  signatureValue: string;
  public company: Company;
  filenameForDownload: string = "file";
  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<PrintDialogContainer>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private printWrapperService: PrintWrapperService,
    private printService: PrintService,
    private languageService: LanguageService,
    private numberRangeConfigService: NumberRangeConfigService,
    private globalSettingService: GlobalSettingService,
    private jasperPrintService: JasperPrintService,
    private exportNumberToWordsService:ExportNumberToWordsService,
    private numberToWordsService: NumberToWordsService,
    private translate: TranslateService,
    private companyService: CompanyService

  ) {
    //console.log('copy: ', data.printCopy)
    //this.printCopy.patchValue(data.printCopy);
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;
    this.languageService.setLanguage(this.currentUser.userLanguage);
    this.filenameForDownload = this.data.transactionType.name;
    translate.setDefaultLang('en');
  }


  ngOnInit() {
 
  let topMargin: number = this.data.numberRangeConfiguration ? this.data.numberRangeConfiguration.printTemplateTopSize : 

AppSettings.PRINT_TEMPLATE_TOP_MARGIN

    if (this.data.numberRangeConfiguration && this.data.numberRangeConfiguration.printTemplateId === 4) {
      this.showMargin = true;
      this.printHeaderTextValue=false;
    } else {
      this.printHeaderTextValue=true;
      this.showMargin = false;
    }
    if(this.data.numberRangeConfiguration.transactionTypeId===7||this.data.numberRangeConfiguration.transactionTypeId===6||

this.data.numberRangeConfiguration.transactionTypeId===14||this.data.numberRangeConfiguration.transactionTypeId===16){
      this.dispalyShippingAddressCheckBox=true;
    
    }




    this.topMargin.patchValue(topMargin)
    this.getPrintHeaderText(this.data.transactionType.id);
    this.applyLanguage();





  }

  applyLanguage() {
    this.languageService.getLanguage()
      .subscribe(resp => {
        if (resp === null) {
          this.translate.use("en");
        }
        else {
          this.translate.use(resp);
        }

      })
  }


  getPrintHeaderText(transactionTypeId: number) {
    console.log(transactionTypeId);
    this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(transactionTypeId)
      .subscribe(response => {
        this.printHeaderText.patchValue(response.printHeaderText);
        if(response.allowShipingAddress && response.allowShipingAddress===1){
          this.shipAddress.patchValue(true)
          this.allowshipAddress=1
        }
        else{
          this.shipAddress.patchValue(false)
          this.allowshipAddress=0
        }
      
        this.getAllPrintCopies();
      })
  }


  getAllPrintCopies() {
    this.globalSettingService.getAllPrintCopies()
      .subscribe(response => {
        this.printCopys = response;
        //console.log("this.printCopys :",this.printCopys);
        this.printCopys.push({
          deleted: "N",
          id: 99,
          name: AppSettings.PRINT_ALL
        })

        this.printCopy.patchValue(response[0].name)
        this.generate();
      })
  }

  onCheckClick() {


  }

  generate() {


    this.spinStart = true;
    this.pdfData = null;
    this.pdfGenerated = false;
    //let signature: number = 0;
    if(this.shipAddress.value===false){
      this.allowshipAddress=0
    }
    if(this.shipAddress.value===true){
      this.allowshipAddress=1
    }
    this.signatureValue = this.companyService.getCompany().signatureImagePath;
    if (this.signatureValue && (this.data.transactionType.name ===AppSettings.CUSTOMER_INVOICE || this.data.transactionType.name === 

AppSettings.PROFORMA_INVOICE || this.data.transactionType.name === AppSettings.INCOMING_JOBWORK_INVOICE||

this.data.transactionType.name===AppSettings.CUSTOMER_QUOTATION || this.data.transactionType.name ===AppSettings.SUPPLIER_PO || 

this.data.transactionType.name ===AppSettings.OUTGOING_JOBWORK_PO || this.data.transactionType.name ===AppSettings.INCOMING_JOBWORK_PO|| 

this.data.transactionType.name ===AppSettings.CUSTOMER_DC)) {
      this.signatureImage = true;
      if (this.signature.value === true) {
        this.signatureVal = 1;
      }
      if (this.signature.value === false) {
        this.signatureVal = 0;
      }
    }


    if (this.data.numberRangeConfiguration && this.data.numberRangeConfiguration.isJasperPrint) {

      console.log("Call Jasper: "+ this.data.numberRangeConfiguration.jasperFileName)


      if (this.data.numberRangeConfiguration.isItemLevelTax) {
        //console.log("Call Jasper Item: ")
      } else if(this.data.printDataModel.partyCountryId==1) {
      //  console.log(this.data.printDataModel.partyCountryId+"========"+this.data.printDataModel.currencyId)
      console.log("print jasper report for normal")

        //console.log("Call Jasper Header: ")
        this.jasperPrintService.jasperPrint(
          this.data.printDataModel.id
          , this.data.numberRangeConfiguration.jasperFileName
          , this.printCopy.value
          , this.numberToWordsService.number2text(this.data.printDataModel.grandTotal)
          , this.printHeaderText.value
          , this.signatureVal
          ,this.allowshipAddress)
          .subscribe(response => {
            //console.log("got response");
            this.pdfData = response;
            this.pdfGenerated = true;
            this.exportToExcel = true;
            this.exportToWord = true;
            setTimeout(() => {
              this.spinStart = false;
            }, 1500);
          })
      }//this works for Quotation and po
      if(this.data.printDataModel.partyCountryId!=1 && this.data.printDataModel.partyCurrencyId!=0 && this.data.transactionType.name===AppSettings.CUSTOMER_QUOTATION ||this.data.transactionType.name=== AppSettings.SUPPLIER_PO)
        {//quaotation
     // console.log(this.data.printDataModel.partyCountryId+"="+this.data.printDataModel.partyCurrencyId)+"numberRange"+"="+"isJasperPrint"+this.data.numberRangeConfiguration.isJasperPrint))


       console.log(" print jasper report  export Invoice exportNmber2text for po and quo"+this.data.printDataModel.partyCurrencyId)
      this.jasperPrintService.jasperPrint(
        this.data.printDataModel.id
        , this.data.numberRangeConfiguration.jasperFileName
        , this.printCopy.value
        , this.exportNumberToWordsService.exportNmber2text(this.data.printDataModel.grandTotal,this.data.printDataModel.partyCurrencyId)
        , this.printHeaderText.value
        , this.signatureVal
        ,this.allowshipAddress)
        .subscribe(response => {
          //console.log("got response");
          this.pdfData = response;
          this.pdfGenerated = true;
          this.exportToExcel = true;
          this.exportToWord = true;
          setTimeout(() => {
            this.spinStart = false;
          }, 1500);
        })
    
    }
    else if(this.data.printDataModel.partyCountryId!=1 && this.data.printDataModel.currencyId!=0 && this.data.transactionType.name===AppSettings.CUSTOMER_INVOICE ||this.data.transactionType.name===AppSettings.PROFORMA_INVOICE)
    {
      let jasperFileName;
      console.log(" hardcoded for the export inv number2text for costumer inv"+this.data.printDataModel.currencyId)
      if(this.data.transactionType.name===AppSettings.CUSTOMER_INVOICE)
      {
        jasperFileName ="Export_Invoice";
      }
      else{
        jasperFileName = this.data.numberRangeConfiguration.jasperFileName;
      }
     
      this.jasperPrintService.jasperPrint(
        this.data.printDataModel.id
        , jasperFileName
        , this.printCopy.value
        , this.exportNumberToWordsService.exportNmber2text(this.data.printDataModel.totalAmountCurrency,this.data.printDataModel.currencyId)
         , this.printHeaderText.value
        , this.signatureVal
        ,this.allowshipAddress)
        .subscribe(response => {
          //console.log("got response");
          this.pdfData = response;
          this.pdfGenerated = true;
          this.exportToExcel = true;
          this.exportToWord = true;
          setTimeout(() => {
            this.spinStart = false;
          }, 1500);
        })
    }
    else{
      console.log("jasper report for normal Inv number2text"+this.data.printDataModel.partyCountryId +"--"+this.data.transactionType.name);
      this.jasperPrintService.jasperPrint(
        this.data.printDataModel.id
        , this.data.numberRangeConfiguration.jasperFileName
        , this.printCopy.value
        , this.numberToWordsService.number2text(this.data.printDataModel.grandTotal)       
         , this.printHeaderText.value
        , this.signatureVal
        ,this.allowshipAddress)
        .subscribe(response => {
          //console.log("got response");
          this.pdfData = response;
          this.pdfGenerated = true;
          this.exportToExcel = true;
          this.exportToWord = true;
          setTimeout(() => {
            this.spinStart = false;
          }, 1500);
        })
    }
  }else {

   console.log("end")
      this.printWrapperService.print(
        this.data.transactionType
        , this.data.printDataModel
        , this.printCopy.value
        , this.data.globalSetting
        , this.data.numberRangeConfiguration
        , this.data.billAddress
        , +this.topMargin.value
        , this.printCopys
        , this.data.isItemLevelTax
        , this.printHeaderText.value
        , this.signatureVal
        ,this.allowshipAddress
      );
      this.printService.printData
        .subscribe(response => {
          if (response) {
            this.pdfData = response;
            this.pdfGenerated = true;
            setTimeout(() => {
              this.spinStart = false;
            }, 1500);
          }
        })


    }
    if (this.topMargin.value != this.data.numberRangeConfiguration.printTemplateTopSize || this.printHeaderText.value != null) {
       
      this.numberRangeConfigService.updatePrintTopMargin(this.data.numberRangeConfiguration.id, this.topMargin.value, 

this.printHeaderText.value,this.allowshipAddress)
        .subscribe(response => {
          //console.log("top margin updated?: ", response)
        })
    }

  }
  onCloseClick(): void {
    this.printService.printData.next(null);
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.printService.printData.next(null);
  }

  saveAsExcel() {

     console.log(this.data.printDataModel.partyCurrencyId)
    this.jasperPrintService.jasperReportExcelForm(this.data.printDataModel.id
      , this.data.numberRangeConfiguration.jasperFileName
      , this.printCopy.value
      , this.numberToWordsService.number2text(this.data.printDataModel.grandTotal)
      , this.printHeaderText.value)


      .subscribe(response => {
        this.exportExcel(response);
      }, error => {

      })
  }

  exportExcel(data) {
    let blob = data;
    let a = document.createElement("a");
    a.setAttribute('style', 'display: none');
    a.href = URL.createObjectURL(blob);
    a.download = this.data.transactionType.name + '.xlsx';
    document.body.appendChild(a);
    a.click();
    a.remove();
  }


  saveAsWord() {


    this.jasperPrintService.jasperReportWordForm(this.data.printDataModel.id
      , this.data.numberRangeConfiguration.jasperFileName
      , this.printCopy.value
      , this.numberToWordsService.number2text(this.data.printDataModel.grandTotal)
      , this.printHeaderText.value)


      .subscribe(response => {
        this.exportWord(response);
      }, error => {

      })
  }


  exportWord(data) {
    let blob = data;
    let a = document.createElement("a");
    a.setAttribute('style', 'display: none');
    a.href = URL.createObjectURL(blob);
    a.download = this.data.transactionType.name + '.docx';
    document.body.appendChild(a);
    a.click();
    a.remove();
  }

  print() {
    // var blob = new Blob([this.pdfData], {type: 'application/pdf'});
    // var blob = new Blob([this.pdfData]);
    const blob = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.pdfData], { type: 'application/pdf' }));
    //const blobUrl = URL.createObjectURL(blob);
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = blob;
    document.body.appendChild(iframe);
    iframe.contentWindow.print();
  }

  download() {
    const pdfUrl = (window.URL || window['webkitURL']).createObjectURL(new Blob([this.pdfData], { type: 'application/pdf' }));
    const anchor = document.createElement('a');
    anchor.href = pdfUrl;
    anchor.setAttribute("download", this.data.transactionType.name);
    anchor.click();
  }





}