import { Injectable, NgZone, Injector } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class RoutingUtilityService {

constructor(
    private injector: Injector) {

     }

     close(){

        const _router: Router = this.injector.get(Router);
        const zone: NgZone = this.injector.get(NgZone);

        zone.run( () => {
            _router.navigate(['/']);
          })
     }
}     