import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { trigger, animate, style, group, animateChild, query, stagger, transition } from '@angular/animations';
import { FormControl } from '@angular/forms';
import { Activity } from '../data-model/activity-model';
import { UserService } from '../services/user.service';
import { map, startWith, take, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserModel } from '../data-model/user-model';
import { AlertService } from 'ngx-alerts';
import { AppSettings } from '../app.settings';
import { SpeechService } from '../services/speech.service';
import { LanguageService } from '../services/language.service';
import { CompanyService } from '../services/company.service';
import { environment } from '../../environments/environment';
import { ConnectionService } from '../services/connection.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { AlertDialog } from '../utils/alert-dialog';

export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ], { optional: true }),
    ])
  ])
])

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [routerTransition]
})
export class HeaderComponent implements OnInit, OnDestroy {

  themeClass: any;
  
  isLoggedIn$: Observable<boolean>;
  //users: UserModel[] = [];
  currentUser: UserModel;
  activityControl: FormControl = new FormControl();
  filteredActivities: Observable<Activity[]>;
  saveStatus: boolean;
  activityList: Activity[] = [];
  selectedUser: UserModel;
  // commandSub: Subscription;
  serverStatus: String;
  warningStatus: String;
  public companyLogoBase64: string;
  public companyLogoPath: string;
  public companyLogoUrl: string = environment.baseServiceUrl+environment.filesCompanyApi;
  onDestroy : Subject<boolean> = new Subject();
  alertRef: MatDialogRef<AlertDialog>;
  constructor(private authService: AuthService,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router, 
    private translate: TranslateService,
    public speech: SpeechService,
    private languageService: LanguageService,
    public companyService: CompanyService,
    private connectionService: ConnectionService,
    protected dialog: MatDialog, ) {
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    translate.setDefaultLang('en');
  }

  ngOnInit() {
    //this.getAllUsers();
    this.themeClass = this.currentUser.userThemeName;
    this.languageService.setLanguage(this.currentUser.userLanguage);
    //console.log("themeClass", this.themeClass)
    //console.log(" this.currentUser ", this.currentUser.username)
    this.isLoggedIn$ = this.authService.isLoggedIn();
    this.userService.getMenusForUser(this.currentUser.username).subscribe(menus => {
      ////console.log('menus: ',menus);
      menus.forEach(menu => {

        menu.activities.forEach(act => {
          if(act.pageUrl){
            // act.name = menu.name+ "->"+act.name;
            //act.name = act.name;
            this.activityList.push(act);    
          }
        })
        //this.activityList.push(...menu.activities);
      });
      ////console.log('this.activityList: ',this.activityList);
      this.filteredActivities = this.activityControl.valueChanges
        .pipe(
        startWith<string | Activity>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(val => this.activityFilter(val))
        
        );

    });
    // this.speech.init();
    // this.listenCommands();
    this.watchTheme();
    this.watchConnection();
    this.watchWarning();
    this.applyLanguage();
    // if(!this.companyLogoPath)
      // this.getCompanyLogo()
    // this.getCompanyLogoBase64();
  }

  watchConnection(){
    this.connectionService.connectionInfoData
    .pipe(
      takeUntil(this.onDestroy)
    )
    .subscribe(response => {      
      // //console.log("this.connectionService.connectionInfoData: ", response);
      if(response){
        this.serverStatus = response;
      }else{
        this.serverStatus = "";
      }
      
    })

  }

  watchWarning(){
    this.connectionService.warningInfoData
    .pipe(
      takeUntil(this.onDestroy)
    )
    .subscribe(response => {      
      // //console.log("this.connectionService.connectionInfoData: ", response);
      if(response){
        this.warningStatus = response;
      }else{
        this.warningStatus = "";
      }
      
    })

  }

  // getCompanyLogo(){
  //   this.userService.companyLogoPathData
  //   .take(1)
  //   .subscribe(response => {
  //     if(response){
  //       this.companyLogoPath = response;
  //     }
  //     else{
  //       this.companyService.getCurrentCompanyLogo(1)
  //       .subscribe(response => {
  //         //console.log("response: ", response);
  //         if(response.responseString && response.responseStatus)
  //         {
  //           this.companyLogoPath = this.companyLogoUrl + "/"  + response.responseString;
  //           this.userService.companyLogoPath.next(this.companyLogoPath);
  //         }
  //         //console.log("this.companyLogoPath ", this.companyLogoPath)
  //       })
  //     }
  //   })
    
  // }

  // getCompanyLogoBase64(){

  //   this.companyService.getCompanyLogoAsBase64(1).subscribe(response => {
  //     this.companyLogoBase64 = response;
  //   });

  // }

  watchTheme(){
    this.userService.themeData.subscribe(response => {
      if(response){
        this.themeClass = response;
      }
      
    })
  }
  // private listenCommands(){

  //   this.commandSub = this.speech.commands$
  //   .subscribe(command => {
  //     //console.log("command: ", command);

  //     this.processCommand(command);
  //   })
  // }

  applyLanguage(){
    this.languageService.getLanguage()
            .subscribe(resp => {
                if(resp===null){
                
                  this.translate.use("en");
              
                }
                else{
                  this.translate.use(resp);
                 
                }
               
            })
  }


  private processCommand(command: string){

    let invoiceReportPath: string = 'report/app-invoice-summary-report/1';
    let balanceReportPath: string = 'report/app-balance-report/5';

    switch (command) {

      case 'invoice' :
              this.router.navigate([invoiceReportPath]);    
              break;  
      case 'balance' :
              this.router.navigate([balanceReportPath]);      
              break;
      default:
            null;
            break;
    }

  }

  activityFilter(val: string): Activity[] {

    return this.activityList.filter(act =>
      act.name.toLowerCase().includes(val.toLowerCase()));

  }

  displayActivityName(act?: Activity): string | undefined {
    return act ? act.name : undefined;
  }

  onActivityChange(activity: Activity) {
    //console.log('act: ', activity);
    //console.log('act: ', activity.pageUrl);
    let path: string = activity.pageUrl;
    path = path + (activity.urlParams ? '/' + activity.urlParams : '');
    //console.log('path: ', path);
    this.router.navigate([path]);
    //this.activityControl.patchValue('');
  }

  onLogout() {
    this.authService.logOut();
  }
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }


  

  ngOnDestroy() {
    
    // this.commandSub.unsubscribe();
    this.onDestroy.next(true);
    this.onDestroy.complete();
    
  }

  openDialog(){
    console.log("in openDialog()")
    this.alertRef = this.dialog.open(AlertDialog, {
          disableClose: false
      });
      this.alertRef.componentInstance.alertMessage = this.warningStatus.toString();
  }
  // public setLanguage(lang: string) {
  //   this.translatorContainer.language = lang;
  //   this.languageService.setLanguage(lang);
  // }

}