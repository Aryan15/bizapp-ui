import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { MaterialType } from '../../data-model/material-type-model';
import { Uom } from '../../data-model/uom-model';
import { Tax } from '../../data-model/tax-model';
import { Material, SupplyType } from '../../data-model/material-model';
import { MaterialService } from '../../services/material.service';
import { UomService } from '../../services/uom.service';
import { TaxService } from '../../services/tax.service';
import { MaterialTypeService } from '../../services/material-type.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { PartyService } from '../../services/party.service';
import { Party } from "../../data-model/party-model";
import { GlobalSettingService } from '../../services/global-setting.service';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { UtilityService } from '../../utils/utility-service';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';
@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {
  @ViewChild('partNumber', {static: false}) partNumberElement: ElementRef;

  title : String = "Company Master";
  subheading : String = "";
  
  item_length : number = 50;
  desc_length : number= 2000;
  public materialForm: FormGroup;
  public material: Material;
  selectedParty: Party;
  public supplyType: SupplyType;
  isCustomerBinded: boolean = false;
  globalSetting: GlobalSetting;
  saveStatus: boolean = false;
  recentMaterialLoadCounter: number = 0;
  spinStart: boolean = false;
  stockReadonly:boolean =false;
  fieldValidatorLabels = new Map<string, string>();
  //transactionTypeStr : string = "Material";
  formHeader: string;
  transactionStatus: string = null;
  isMaterial: boolean = true;
  public parties: Party[] = [];
  activityId:number=0;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;

  public materialTypes: MaterialType[] = [];
  public uoms: Uom[] = [];
  public taxes: Tax[] = [];

  isJobwork: boolean = false;
  priceName : string = "";


  

  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: true,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: true,
    isDraftButton: false,
};

  activityRoleBindings: ActivityRoleBinding[];
  isPartNumberVerified: boolean = false;

  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private materialTypeService: MaterialTypeService,
    private uomService: UomService,
    private taxService: TaxService,
    private userService: UserService,
    private _router: Router,
    private alertService: AlertService,
    private utilityService:UtilityService,
    public dialog: MatDialog,
    private globalSettingService: GlobalSettingService,
    private partyService: PartyService,
    public route: ActivatedRoute, ) {
    this.setActivityRoleBinding();
  }

  ngOnInit() {
    this.setSupplyType();
    this.getMaterialTypes();
    this.getUoms();
    this.getTaxes();
    this.getParties();
    
    this.getGlobalSetting();
    this.initForm();
  }

  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;
      this.activityRoleBindings.forEach(activityRoleBinding => {
        this.activityId=activityRoleBinding.activityId;
        });
      //console.log("activityRoleBindings: ", this.activityRoleBindings)
    })
  }

  getGlobalSetting() {
    
            this.globalSettingService.getGlobalSetting().subscribe(response => {
                this.globalSetting = response;
                //console.log("before how ",this.globalSetting.cutomerMateriaBinding ," .isCustomerBinded" ,this.isCustomerBinded)
                this.isCustomerBinded = this.globalSetting.cutomerMateriaBinding === 1 ? true : false;
                //console.log("after how " ,this.globalSetting.cutomerMateriaBinding ,"for  .isCustomerBinded" ,this.isCustomerBinded)
              })
        }

  setSupplyType() {
    this.route.params.subscribe(params => {
      this.supplyType = {
        id: +params['supplyTypeId'],
        name: '',
        deleted: 'N'
      };

      console.log("this.supplyType.....................", this.supplyType, "this.supplyType.id......",this.supplyType.id);
      
      if (this.supplyType.id === 1) {
        this.formHeader = "Material Master";
        this.subheading  = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
        this.isMaterial = true;
        this.priceName = "SellingPrice";
      } else {
        this.formHeader = "Service Master";
        this.subheading  =  AppSettings.SUB_HEADING_LINE + this.userService.activityName;
        this.isMaterial = false;

      }
    });
  }

  getMaterialTypes() {
    this.materialTypeService.getAllMaterialTypes()
      .subscribe(response => {
        this.materialTypes = response;
      })
  }
  getUoms() {
    this.uomService.getAllUom().subscribe(
      response => {
        this.uoms = response;
      }
    )
  }
  edit(){
          this.disableEnableForm()
      
}
  getTaxes() {
    this.taxService.getAllTaxes().subscribe(
      response => {
        this.taxes = response;
      }
    )
  }
  private initForm() {
    let data: Material = {
      id: null,
      deleted: null,
      name: null,
      partNumber: null,
      supplyTypeId: null,
      materialTypeId: null,
      unitOfMeasurementId: null,
      unitOfMeasurementName: null,
      price: null,
      specification: null,
      stock: null,
      companyId: null,
      buyingPrice: null,
      taxId: null,
      mrp: null,
      discountPercentage: null,
      partyId: null,
      hsnCode: null,
      cessPercentage: null,
      openingStock: null,
      minimumStock: null,
      isContainer: null,
      outName: null,
      outPartNumber: null,
      materialTypeName: null,
    };

    this.materialForm = this.toFormGroup(data);


    
  }

  OnChange($event){
    //console.log("onchange........................",$event);

    if($event.checked===true){
      this.materialForm.controls['isContainer'].patchValue(true)
      this.materialForm.controls['price'].patchValue(0)
      this.materialForm.controls['buyingPrice'].patchValue(0)
      
    }
    else{
      this.materialForm.controls['isContainer'].patchValue(false)

    }

}

onSelect(_event: any){
  //console.log("_event.........", _event);

    let materialType = this.materialTypes.find(m => m.id===_event.value).name

    //console.log("supplyType.........", this.supplyType);
    if(this.supplyType.id===1 && materialType===AppSettings.MATERIAL_TYPE_JOBWORK){
      this.isJobwork=true;
      // this.materialForm.controls['openingStock'].patchValue(0)
      // this.materialForm.controls['stock'].patchValue(0)

      this.priceName = "Jobwork price";
    }
    else if(this.supplyType.id===2){
      this.priceName = "Service Charge";
    }
    else{
      this.priceName = "Selling price";
      this.isJobwork=false;
  
    }
}


  private toFormGroup(data: Material): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      deleted: [data.deleted],
      name: [data.name, [Validators.required]],
      partNumber: [data.partNumber],
      supplyTypeId: [data.supplyTypeId],
      materialTypeId: [data.materialTypeId, [Validators.required]],
      unitOfMeasurementId: [data.unitOfMeasurementId, [Validators.required]],
      unitOfMeasurementName: [data.unitOfMeasurementName],
      price: [data.price],
      specification: [data.specification],
      stock: [data.stock],
      companyId: [data.companyId],
      buyingPrice: [data.buyingPrice],
      taxId: [data.taxId],
      mrp: [data.mrp],
      discountPercentage: [data.discountPercentage],
      partyId: [data.partyId],
      hsnCode: [data.hsnCode],
      cessPercentage: [data.cessPercentage],
      openingStock: [data.openingStock],
      minimumStock : [data.minimumStock],
      isContainer: [data.isContainer],
      outName: [data.outName],
      outPartNumber: [data.outPartNumber],
      materialTypeName:[data.materialTypeName]
    });

    this.fieldValidatorLabels.set("name", "Name");
    this.fieldValidatorLabels.set("unitOfMeasurementId", "Unit Of Measurement");
    this.fieldValidatorLabels.set("materialTypeId", "Material Type");
    return formGroup;

  }


  
  

  delete(model: Material, formDirective: FormGroupDirective) {

    if (model.id) {
      this.spinStart = true;
      model.deleted = 'Y';
      //console.log('Before delete:', model);

      this.materialService.delete(model.id)
        .subscribe(respose => {
          if (respose.responseStatus === 1) {
            this.saveStatus = true;
            this.recentMaterialLoadCounter++;
            this.spinStart = false;
            this.transactionStatus = AppSettings.DELETE_SUCESSFULL_MESSAGE;
            this.alertService.success(this.transactionStatus);
            formDirective.resetForm();
            this.initForm();
            this.materialForm.markAsPristine();
          }
          else {
          console.log("Error", respose);
            this.transactionStatus = respose.responseString;
            this.saveStatus = false;
            this.spinStart = false;
            this.alertService.danger(this.transactionStatus);
            
          }
        },
        error => {
          //console.log("Error ", error);
          this.transactionStatus ="Material is being used elsewhere cannot delete";
          this.alertService.danger(this.transactionStatus);
          
          this.saveStatus = false;
          this.spinStart = false;
        })
    }
  }

  clearFormNoStatusChange() {
    this.initForm();

    //   this.handleChanges();
    //  this.selectedParty = null;
  }




  closeForm() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }

  clearForm(formDirective: FormGroupDirective) {
    this.saveStatus = false;
    this.spinStart = false;
    this.stockReadonly = false;
    this.transactionStatus = null;
    formDirective.resetForm();
    this.initForm();
    this.materialForm.markAsPristine();
    this.isPartNumberVerified = false;
    // this.disableEnableForm();

  }

  validateAndSubmit(material: Material){
     //on edit of a material, if material type is changed to jobwork or vice versa, 
    //see if this material already used in transaction. if yes, stop from saving

    console.log("this.material: ", this.material);
    console.log("material.id: ",material.id);
    console.log("material.materialTypeId: ",material.materialTypeId);
    if(material.id && 
        (
          (this.material.materialTypeId === 4 && material.materialTypeId !== 4) ||
          (this.material.materialTypeId !== 4 && material.materialTypeId === 4)
        )
      )
      {

      this.materialService.materialExistsinTransactions(material.id)
      .subscribe(response => {
        if(response >= 1){
          this.alertService.danger("Material is used in transaction. Material type cannot be changed");
          return false;
        }else{
          this.onSubmitWrapper(material);
        }
      })
    } else {
      this.onSubmitWrapper(material);
    }
  }
  onSubmitWrapper(material: Material){


    //we do validation
    //console.log("before validate: ", material);
    this.materialService.checkMaterialAvailability(material.name,material.id)
    .subscribe(response => {

      //console.log("response in valdate ", response)
      if(response && response.length > 0 ){
        //call confirmation
        this.dialogRef = this.dialog.open(ConfirmationDialog, {
          disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = AppSettings.DUPLICATE_MATERIAL_CONFIRMATION_MESSAGE

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.onSubmit(material);
            }
            
        });
      }else{
        this.onSubmit(material);
      }

    })
    
  }
  
  onSubmit(material: Material) {

   

    if(material.openingStock===null && material.stock===null && material.id===null){
      material.openingStock=0;
      material.stock=0;
    }
    
    if(!this.isPartNumberVerified && this.materialForm.get('partNumber').value ){
      return false;
    }
    if(!this.isMaterial){
      this.materialForm.get('materialTypeId').clearValidators;
      this.materialForm.get('materialTypeId').setErrors(null);
      this.materialForm.get('unitOfMeasurementId').clearValidators;  
      this.materialForm.get('unitOfMeasurementId').setErrors(null);      
      this.materialForm.updateValueAndValidity();
    }
    
   
     if(!this.materialForm.valid){
      const controls = this.materialForm;
      let invalidFieldList: string[] =this.utilityService.findInvalidControlsRecursive(controls);
      //console.log("invalidFieldList ", invalidFieldList)

      let invalidFiledLabels: string[] = [];
      
      invalidFieldList.forEach(field => {
        if (this.fieldValidatorLabels.get(field))
          invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
      })

      //console.log("invalidFiledLabels ", invalidFiledLabels)

      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE+invalidFiledLabels);
      return false;
  }
    this.spinStart = true;
    let message: string;
    if (material.id === null) {
        message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
    }
    else {
        message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
    }
    material.companyId = 1; //hack TODO
    material.supplyTypeId = this.supplyType.id;
    material.isContainer = this.materialForm.get("isContainer").value ? 1 : 0;
    //console.log("Before submit: ", material);
    this.materialService.save(material)
      .subscribe(response => {
        this.saveStatus = true;
       
        this.recentMaterialLoadCounter++;
        this.spinStart = false;
        if (this.supplyType.id === 1)
          this.transactionStatus = "Material " + response.name + message;
        else
          this.transactionStatus = "Service " + response.name + message;
          this.alertService.success(this.transactionStatus);
         
        //  this.initForm();
        //this.materialForm.controls['id'].patchValue(response.id);
        //console.log("saved data: ", response);
        this.materialForm = this.toFormGroup(response);
        this.material = response;
        this.disableEnableForm();
      },
      error => {
        console.error("Error: ", error);
        
         if(error instanceof HttpErrorResponse){
            var re = /DataIntegrityViolationException/gi;
            if(error.error && error.error.exception && error.error.exception.search(re) !== -1)
            this.transactionStatus = "Duplicate Entry!! Cannot save";
        }
        else
        {
        this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
        }
        this.alertService.danger(this.transactionStatus);
        this.saveStatus = false;
        this.spinStart = false;

      });
   
  }

  recentMaterialGet(event) {
    //console.log('In recentMaterialGet', event);
    if(event.materialTypeName==="Jobwork"){
      this.priceName = "jobwork price";
      this.isJobwork=true;
    }
    else{
      this.isJobwork=false;
    }
    
    this.material = event;
    if(this.material.id){
      this.stockReadonly = true;
    }
    this.materialForm = this.toFormGroup(this.material);
    this.isPartNumberVerified = true;
    this.disableEnableForm();
    // this.materialForm.disable();

  }

  disableEnableForm() {
    
        if (this.materialForm.disabled) {
          this.materialForm.enable();
          //     this.disableParty = false;
       
        } else {
    
          this.materialForm.disable();
          //  this.disableParty = true;
          //console.log("this.materialForm.dirty == false",this.materialForm.dirty == false)
          this.materialForm.markAsPristine;
          //console.log("this.materialForm.markAsPristine == false",this.materialForm.markAsPristine)
        }
    
    
      }
  whenNoDataFound(noDataFound: boolean) {


    if (noDataFound) {
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });

      this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE;
    }
  }
  getParties() {
   
        this.partyService.getAllPartiesWithPriceList()
        .pipe(
          map(resp => resp.map(r => r.partyDTO))
        )
        .subscribe(
            response => {
                this.parties = response;
            }    
        );
      }

      checkPartNumberAvailability() {
        let partNumber: string = this.materialForm.get('partNumber').value
        //console.log("partNumber: ", partNumber);
        if (partNumber && partNumber.length > 0) {
          this.materialService.checkPartNumberAvailability(partNumber).subscribe(response => {
    
            //console.log("is partNumber avialable? ", response);
            if (response.responseStatus === 1) {
              //console.log("Yes");
              this.isPartNumberVerified = true;
            } else {
              //console.log("No");
              this.isPartNumberVerified = false;
              this.alertService.danger(AppSettings.PART_NUMBER_UNIQUE_NUMBER_MESSAGE);
                // this.materialForm.controls['partNumber'].patchValue('');
              this.partNumberElement.nativeElement.focus();
        
            }
    
          });
        }
    
      }

      checkServiceNameAvailablity(){
        let name: string = this.materialForm.get('name').value
        //console.log("name: ", name);
        // if (name && name.length > 0) {
        //   this.materialService.checkServiceNameAvailablity(name).subscribe(response => {
    
        //     //console.log("is name avialable? ", response);
        //     if (response.responseStatus === 1) {
        //       //console.log("Yes");
        //     } else {
        //       //console.log("No");
        //       this.alertService.danger(AppSettings.PART_NUMBER_UNIQUE_NUMBER_MESSAGE);
        //         this.materialForm.controls['name'].patchValue('');
        //        this.serviceNameElement.nativeElement.focus();
        
        //     }
    
        //   });
        // }
    
      }


      updateStock(){
        this.materialForm.get('stock').patchValue(this.materialForm.get('openingStock').value); 
      }


      helpVideos() {
        
        let status: string = "";
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.id =this.activityId.toString();
     
        
        const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(
            data => {
    
            });
    
    
    
        
    }

}
