import { Component, Input, OnChanges, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, merge, of as observableOf } from 'rxjs';
import { catchError, map, debounceTime, startWith, switchMap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { MaterialService } from '../../services/material.service';
import { Material, RecentMaterial } from '../../data-model/material-model';
import { MaterialType } from '../../data-model/material-type-model';

const PAGE_SIZE: number = 10;

@Component({
  selector: 'recent-material-table',
  templateUrl: './recent-material-table.component.html',
  styleUrls: ['./recent-material-table.component.scss']
})
export class RecentMaterialTable implements OnInit, OnChanges {

  @Input()
  recentMaterialLoadCounter: number;
  @Input()
  supplyType: MaterialType;
  @Output()
  material: EventEmitter<Material> = new EventEmitter<Material>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();


  isSupplyTypeMaterial: Boolean = false;
  displayedColumns: string[] =[];
  materialMasterDatabase: MaterialMasterHttpDao | null;

  dataSource = new MatTableDataSource<Material>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private MaterialService: MaterialService) { }


  ngOnInit() {
    //console.log(" @Input()", this.supplyType)
    if (this.supplyType.id === 1) {
      this.isSupplyTypeMaterial = true;
      this.displayedColumns = ['name', 'partNumber', 'stock'];
    }
    else {
      this.isSupplyTypeMaterial = false;
      this.displayedColumns = ['name', 'price'];
    }
    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.materialMasterDatabase = new MaterialMasterHttpDao(this.MaterialService);

    //If the Material changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.materialMasterDatabase!.getMaterials(this.supplyType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished 
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        //console.log('data.Materials: ', data.materials);
        //console.log('data.totalCount: ', data.totalCount);
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.materials;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        //console.log('catchError error');
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);

  }

  ngOnChanges() {
    //console.log('in onchanage: ', this.recentMaterialLoadCounter);

    if (this.recentMaterialLoadCounter > 0) {
      this.materialMasterDatabase = new MaterialMasterHttpDao(this.MaterialService);

      this.materialMasterDatabase!.getMaterials(this.supplyType.id, this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.materials);
          //console.log('data.totalCount: ', data.totalCount);
          if (this.filterForm.get('filterText').value != null) {
            if (data.totalCount === 0) {
              this.noDataFound.emit(true);
            }
          }
          return data.materials;
        })).subscribe(data => this.dataSource.data = data);
    }
  }

  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.material.emit(row);
    //console.log('done');
  }

}


export class MaterialMasterHttpDao {

  constructor(private MaterialService: MaterialService) { }

  getMaterials(supplyTypeId: number, filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentMaterial> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);

    return this.MaterialService.getRecentMaterials(supplyTypeId, filterText, sortColumn, sortDirection, page, pageSize);
  }
}

