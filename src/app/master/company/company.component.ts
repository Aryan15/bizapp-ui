import { Component, OnInit } from '@angular/core';
import { HttpEventType } from '@angular/common/http';

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Company, AddressList, CompanyWithBankListDTO } from '../../data-model/company-model';
import { State } from '../../data-model/state-model';
import { Country } from '../../data-model/country-model';
import { Area } from '../../data-model/area-model';
import { City } from '../../data-model/city-model';
import { CompanyService } from '../../services/company.service';
import { LocationService } from '../../services/location.service';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { UserService } from '../../services/user.service';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { DatePipe } from '@angular/common';
import { FileUploadClientService } from '../../services/file-client.service';
import { AlertService } from 'ngx-alerts';
import { PartyBankMap } from '../../data-model/party-model';
import { BankModel } from '../../data-model/bank-model';
import { BankService } from '../../services/bank.service';
import { RoutingUtilityService } from '../../utils/routing-utility.service';
import { UtilityService } from '../../utils/utility-service';
import { ImgMaxPXSizeService } from '../../services/image.service';
import { InvoiceService } from '../../services/invoice.service';
import { Currency } from '../../data-model/currency-model';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  disableSelect: boolean = false;//to disable company type 
  hintValue: string = null;

  item_length: number = 250;

  public companyForm: FormGroup;
  //public company: Company;
  public companyWithBankListDTO: CompanyWithBankListDTO;
  fileUploadSub: any;
  uploadProgress: number = 0;
  uploadComplete: boolean = false;
  uploadingProgressing: boolean = false;
  signatureUploadingProgressing: boolean = false;
  serverResponse: any;
  isGstRegistered: boolean = false;
  public myHeaders: Headers;
  bankLocal: BankModel;
  bankMapItemToBeRemove: number[] = [];

  //public imageUrl: string;
  public companyLogoUrl: string = environment.baseServiceUrl + environment.filesCompanyApi;//"http://localhost:8081/api/files";
  public certificateImageUrl: string = environment.baseServiceUrl + environment.filesCerrtificateImageApi;//"http://localhost:8081/api/files";
  public companyLogoPath: string; //= "http://localhost:8081/api/files/house.jpg"
  public certImagePath: string; //= "http://localhost:8081/api/files/house.jpg"
  public signatureUrl: string = environment.baseServiceUrl + environment.filesSignatureImageApi;//"http://localhost:8081/api/files";
  public signaturePath: string; //= "http://localhost:8081/api/files/house.jpg"

  maxDate: Date;
  countries: Country[] = [];
  states: State[] = [];
  cities: City[] = [];
  allStates: State[] = [];
  areas: Area[] = [];
  allCities: City[] = [];
  allCurrensys: Currency[] = [];
  currencys: Currency[] =[];
  public gstRegistrationTypes: GSTRegistrationType[] = [
    { id: 1, name: "Registered" },
    { id: 2, name: "Composition" },
    { id: 3, name: "Unregistered" }
  ]

  transactionStatus: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;

  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
  };

  activityRoleBindings: ActivityRoleBinding[];
  enableSaveButton: boolean = false;
  fieldValidatorLabels = new Map<string, string>();
  public banks: BankModel[] = [];
  bankInputLength: number = 100;
  branchInputLength: number = 100;
  //uploadedImages: UploadedImages[] = [];
  constructor(private fb: FormBuilder,
    private _router: Router,
    private companyService: CompanyService,
    private invoiceService: InvoiceService,
    private userService: UserService,
    public route: ActivatedRoute,
    private locationService: LocationService,
    private alertService: AlertService,
    private fileUploadService: FileUploadClientService,
    private bankService: BankService,
    private utilityService: UtilityService,
    // private zone: NgZone,
    private routingUtilityService: RoutingUtilityService,
    private imgMaxPXSizeService: ImgMaxPXSizeService) {
    this.setActivityRoleBinding();
  }

  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {

        ////console.log('in action panel: ', activityRoleBinding.permissionToUpdate);
        ////console.log('in action panel: ', activityRoleBinding.permissionToCreate);
        ////console.log('in action panel activityRoleBinding.permissionToCancel: ', activityRoleBinding.permissionToCancel);

        this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
      });
    })
  }


  ngOnInit() {
   
    this.checkTransactionCount();
    this.companyService.getCurrentCompanyLogo(1)
      .subscribe(response => {
        //console.log("response: ", response);
        if (response.responseString && response.responseStatus)
          this.companyLogoPath = this.companyLogoUrl + "/" + response.responseString;
        //console.log("this.companyLogoPath ", this.companyLogoPath)
      })


    this.companyService.getCurrentCompanyCertImage(1)
      .subscribe(response => {
        //console.log("response: ", response);
        if (response.responseString && response.responseStatus)
          this.certImagePath = this.companyLogoUrl + "/" + response.responseString;
        //console.log("this.certImagePath ", this.certImagePath)
      })

    this.companyService.getSignatureImage(1)
      .subscribe(response => {
        //console.log("response: ", response);
        if (response.responseString && response.responseStatus)
          this.signaturePath = this.companyLogoUrl + "/" + response.responseString;
        //console.log("this.companyLogoPath ", this.companyLogoPath)
      })
    // this.myHeaders = this.companyService.getHeaders();
    //this.imageUrl = this.companyService.getCompanyLogoUrl();
    ////console.log('got imageURL to load: ', this.imageUrl);
    this.initForm();
    this.getCountries();
    this.getAllStates();
    this.getAllCities();
    this.getBanks();
    this.getAllCurrencys();
    // this.getCities();
    // this.getAreas();
    this.initialPopulation();

    this.companyForm.get('companyDTO.countryId').valueChanges.subscribe(countryId => {

      this.getStates(countryId);
    })
    this.maxDate = new Date();
  }

  private getAllCities() {

    this.locationService.getCities()
      .subscribe(response => {
        this.cities = response;
        this.allCities = response;
      })
  }

  checkTransactionCount() {
    this.invoiceService.getTransactionCount().subscribe(response => {
      //console.log("response in count", response);

      if (response > 0) {
        this.disableSelect = true;
        this.hintValue = "You have already created a transaction!! You Can't change value now...";
      }
    });
  }
  private getCountries() {

    this.locationService.getCountries()
      .subscribe(response => {
        this.countries = response;
      })
  }

  private getBanks() {
    this.bankService.getAllBanks()
      .subscribe(response => {
        this.banks = response;
      })
  }

  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
        this.allStates = response;
      })
  }
  private getAllCurrencys() {

    this.locationService.getCurrencys()
      .subscribe(response => {
        this.currencys = response;
        this.allCurrensys = response;
      })
  }
  private getStates(countryId) {

    // this.locationService.getStatesForCountry(countryId)
    //   .subscribe(response => {
    //     this.states = response;
    //   })
    this.states = this.allStates.filter(state => state.countryId === countryId);

  }

  private getCities() {

    this.locationService.getCities()
      .subscribe(response => {
        this.cities = response;
      })
  }

  private getAreas() {

    this.locationService.getAreas()
      .subscribe(response => {
        this.areas = response;
      })
  }

  private initForm() {

    let data: CompanyWithBankListDTO = {
      companyDTO: this.initCompany(),
      partyBankMapDTOList: null,
      partyBankMapListDeletedIds: null,
    };

    this.companyForm = this.toFormGroup(data);
    //this.getRegstrationType();
  }

  initCompany(): Company {
    let data: Company = {
      id: null,
      name: null,
      address: null,
      cityId: null,
      stateId: null,
      countryId: null,
      gstNumber: null,
      materialIds: null,
      deleted: null,
      pinCode: null,
      primaryTelephone: null,
      secondaryTelephone: null,
      tinNumber: null,
      tinDate: null,
      faxNumber: null,
      contactPersonName: null,
      primaryMobile: null,
      secondaryMobile: null,
      email: null,
      website: null,
      contactPersonNumber: null,
      statusId: null,
      panNumber: null,
      panDate: null,
      companyLogoPath: null,
      tagLine: null,
      gstRegistrationTypeId: null,
      addressesListDTO: null,
      stateCode: null,
      stateName: null,
      bank: null,
      branch: null,
      account: null,
      ifsc: null,
      ceritificateImagePath: null,
      signatureImagePath: null,
      msmeNumber: null,
      cinNumber: null,
      subject:null,
      iecCode:null,
      companyCurrencyId:null,
      creditLimit:null,
      insuranceType:null,
      insuranceRef:null,
      financePerson:null,
      financeEmail:null,
    };

    return data;
  }

  datePipe = new DatePipe('en-US');

  private toFormGroup(data: CompanyWithBankListDTO): FormGroup {
    const partyBankMapArray = new FormArray([]);

    if (data.partyBankMapDTOList) {
      data.partyBankMapDTOList.forEach(item => {
        partyBankMapArray.push(this.bankMapItem(item))
      })
    }


    const formGroup = this.fb.group({
      companyDTO: this.toCompanyFormGroup(data.companyDTO),
      partyBankMapDTOList: partyBankMapArray
    });

    return formGroup;

  }


  private bankMapItem(data: PartyBankMap): FormGroup {

    //console.log('data: ', data);
    const formGroup = this.fb.group({
      id: [data.id],
      companyId: [data.companyId],
      partyId: null,
      bankId: [data.bankId],
      bankname: [data.bankname],
      branch: [data.branch],
      ifsc: [data.ifsc],
      accountNumber: [data.accountNumber],
      bankAdCode :[data.bankAdCode],
      openingBalance: [data.openingBalance],
      contactNumber: [data.contactNumber],

    });

    return formGroup;

  }

  addBankMap() {

    //console.log("in addBankMap: ");

    //Build MaterialPriceList

    const partyBankMap: PartyBankMap = {
      id: null,
      companyId: null,
      partyId: null,
      bankId: this.bankLocal.id,
      bankname: this.bankLocal.bankname,
      branch: null,
      ifsc: null,
      accountNumber: null,
      bankAdCode:null,
      openingBalance: null,
      contactNumber: null,
    }
    //Get control

    const control = <FormArray>this.companyForm.controls['partyBankMapDTOList'];

    //Get Form group of BankMap
    //Push to control

    control.push(this.bankMapItem(partyBankMap));

    this.bankLocal = null;

  }

  deleteBankMap(index: number) {

    let itemId: number = (<FormArray>this.companyForm.get('partyBankMapDTOList')).at(index).get("id").value;

    if (itemId) {
      this.bankMapItemToBeRemove.push(itemId);
      (<FormArray>this.companyForm.get('partyBankMapDTOList')).removeAt(index);
    } else {
      (<FormArray>this.companyForm.get('partyBankMapDTOList')).removeAt(index);
    }


    return false;
  }


  private toCompanyFormGroup(data: Company): FormGroup {
    const addressesArray = new FormArray([]);
    if (data.addressesListDTO) {

      data.addressesListDTO.forEach(address => {

        //let item : MaterialPriceList = 
        addressesArray.push(this.mapAddress(address));
      });
    }

    const formGroup = this.fb.group({
      id: [data.id],
      name: [data.name, Validators.required],
      address: [data.address, Validators.required],
      cityId: [data.cityId],
      stateId: [data.stateId, Validators.required],
      countryId: [data.countryId],
      gstNumber: [data.gstNumber, Validators.compose([Validators.minLength(20), Validators.maxLength(20)])],
      materialIds: [data.materialIds],
      deleted: [data.deleted],
      pinCode: [data.pinCode, Validators.maxLength(7)],
      primaryTelephone: [data.primaryTelephone],
      secondaryTelephone: [data.secondaryTelephone],
      tinNumber: [data.tinNumber],
      tinDate: [data.tinDate],
      faxNumber: [data.faxNumber],
      contactPersonName: [data.contactPersonName],
      primaryMobile: [data.primaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      secondaryMobile: [data.secondaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      email: [data.email, Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])],
      website: [data.website],
      contactPersonNumber: [data.contactPersonNumber],
      statusId: [data.statusId],
      panNumber: [data.panNumber, Validators.pattern('^[A-Za-z]{5}[0-9]{4}[A-Za-z]$')],
      panDate: [this.datePipe.transform(data.panDate, AppSettings.DATE_FORMAT)],
      companyLogoPath: [data.companyLogoPath],
      tagLine: [data.tagLine],
      gstRegistrationTypeId: [data.gstRegistrationTypeId],
      addressesListDTO: addressesArray,
      ceritificateImagePath: [data.ceritificateImagePath],
      signatureImagePath: [data.signatureImagePath],
      msmeNumber: [data.msmeNumber],
      cinNumber: [data.cinNumber],
      subject: [data.subject],
      iecCode: [data.iecCode],
      companyCurrencyId:[data.companyCurrencyId],
      creditLimit:[data.creditLimit , Validators.maxLength(8)],
      insuranceType:[data.insuranceType ,Validators.maxLength(10)],
      insuranceRef:[data.insuranceRef ,Validators.maxLength(10)],
      financePerson:[data.financePerson ,Validators.maxLength(10)],
      financeEmail: [data.email, Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])],

    });

    this.fieldValidatorLabels.set("name", "Company Name");
    this.fieldValidatorLabels.set("address", "Address");
    this.fieldValidatorLabels.set("stateId", "State");
    this.fieldValidatorLabels.set("gstNumber", "GST Number");
    this.fieldValidatorLabels.set("email", "E-mail");

    return formGroup;

  }


  private mapAddress(data: AddressList): FormGroup {

    const formGroup = this.fb.group({
      address: [data.address],
      contactPersonName: [data.contactPersonName],
      contactPersonNumber: [data.contactPersonNumber],
      contactPersonDesignation: [data.contactPersonDesignation],
      deleted: [data.deleted],
    });
    return formGroup;
  }

  initialPopulation() {
    //Get the company

    //let comp: Company;
    this.companyService.getWithBankMap(1).subscribe(response => {
      //console.log('got response: ', response);
      this.companyWithBankListDTO = response;
      this.companyForm = this.toFormGroup(this.companyWithBankListDTO);
      this.getRegstrationType();
    });



  }

  // onUploadFinished(event) {

  //   //console.log('Upload finished: ', event);
  //   //console.log('file name: ', event.file.name);

  //   this.companyLogoPath = this.companyLogoUrl + "/" + event.file.name;

  //   //save logo file name to corresponding form control

  //   this.companyForm.controls['companyLogoPath'].patchValue(event.file.name);
  // }

  handleFileInput(event) {
    this.uploadingProgressing = true;
   let images = [].slice.call(event.target.files);
    this.resizeAndPush(images);


  }

  uploadCertificateImage(event) {
    this.uploadingProgressing = true;
    let images = [].slice.call(event.target.files);
    this.resizeAndPushCertificateImage(images);


  }

  uploadSignatureImage(event) {

    this.signatureUploadingProgressing = true;
    let images = [].slice.call(event.target.files);
    this.resizeAndPushSiganture(images);


  }


  deleteImage() {
    this.fileUploadService.deleteFile(this.companyWithBankListDTO.companyDTO.companyLogoPath, this.companyWithBankListDTO.companyDTO.id)
      .subscribe(response => {
        if (response.responseStatus === 0) {
          this.alertService.danger("Error deleting logo");
        } else {
          this.alertService.success("Image deleted");
          this.companyLogoPath = null;
        }
      })
  }

  deleteCertImage() {
    console.log(this.companyWithBankListDTO.companyDTO.ceritificateImagePath);
    this.fileUploadService.deleteCertImage(this.companyWithBankListDTO.companyDTO.ceritificateImagePath, this.companyWithBankListDTO.companyDTO.id)
      .subscribe(response => {
        if (response.responseStatus === 0) {
          this.alertService.danger("Error deleting logo");
        } else {
          this.alertService.success("Image deleted");
          this.certImagePath = null;
        }
      })
  }



  deleteSignatureImage() {
    this.fileUploadService.deleteSignatureImage(this.companyWithBankListDTO.companyDTO.signatureImagePath, this.companyWithBankListDTO.companyDTO.id)
      .subscribe(response => {
        if (response.responseStatus === 0) {
          this.alertService.danger("Error deleting logo");
        } else {
          this.alertService.success("Image deleted");
          this.signaturePath = null;
          
          
        }
      })
  }

  handleProgress(event, fileItem, fileType: string = "LOGO") {
    if (event.type === HttpEventType.DownloadProgress) {
      // this.uploadingProgressing =true
      this.uploadProgress = Math.round(100 * event.loaded / event.total)
    }

    if (event.type === HttpEventType.UploadProgress) {
      // this.uploadingProgressing =true
      this.uploadProgress = Math.round(100 * event.loaded / event.total)
      //console.log("uploadProgress: ", this.uploadProgress);

    }

    if (event.type === HttpEventType.Response) {
      // //console.log(event.body);
      this.uploadComplete = true
      this.serverResponse = event.body

      if (fileType === "CERT") {
        this.certImagePath = this.companyLogoUrl + "/" + fileItem.name;
        this.companyForm.get('companyDTO.ceritificateImagePath').patchValue(fileItem.name);
        this.companyWithBankListDTO.companyDTO.ceritificateImagePath = fileItem.name;

      } else if (fileType === "SIGN") {

        this.signaturePath = this.companyLogoUrl + "/" + fileItem.name;
        this.companyForm.get('companyDTO.signatureImagePath').patchValue(fileItem.name);
        this.companyWithBankListDTO.companyDTO.signatureImagePath = fileItem.name;


      }

      else {

        this.companyLogoPath = this.companyLogoUrl + "/" + fileItem.name;
        this.companyForm.get('companyDTO.companyLogoPath').patchValue(fileItem.name);
        this.companyWithBankListDTO.companyDTO.companyLogoPath = fileItem.name;

      }




      //console.log('form: ', this.companyForm);
      // this.companyForm.controls['companyLogoPath'].patchValue(fileItem.name);

      //console.log('this.companyLogoPath: ',this.companyLogoPath);
      this.alertService.success("LOGO Uploaded Sucessfully");
    }
  }

  private cleanUp(model: Company): Company {

    model.addressesListDTO = model.addressesListDTO.filter(add => add.address);

    return model;

  }

  addAddress() {

    const addressList: AddressList = {
      address: null,
      contactPersonName: null,
      contactPersonNumber: null,
      contactPersonDesignation: null,
      deleted: null,
    }
    const control = <FormArray>this.companyForm.controls['companyDTO'].get('addressesListDTO');
    // const control = <FormArray>this.companyForm.controls['addressesListDTO'];
    //console.log("control"+control);
    control.push(this.mapAddress(addressList));
  }


  validateTagLine(tagLine: string): boolean {
    ////console.log("termsAndConditions..........................", termsAndConditions);
    if (tagLine != null) {
      let tncArray = tagLine.split("\n");
      ////console.log("tncArray.length: ", tncArray.length);

      if (tncArray.length >= 3) {
        this.alertService.danger("More than two lines of TAG LINE is not supported");
        return false;
      }
    }

    return true;
  }


  submit(model: CompanyWithBankListDTO) {

    //model.partyBankMapDTOList.forEach(field =>{
     // console.log("=============",field.bankAdCode); 
   // })
    if (!this.validateTagLine(model.companyDTO.tagLine)) {
      return false;
    }
    if (!this.companyForm.valid) {
      const controls = this.companyForm;
      let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
      //console.log("invalidFieldList ", invalidFieldList)

      let invalidFiledLabels: string[] = [];

      invalidFieldList.forEach(field => {
        if (this.fieldValidatorLabels.get(field))
          invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
      })

      //console.log("invalidFiledLabels ", invalidFiledLabels)

      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
      return false;
    }
    console.log('Before save: ', model.companyDTO.cinNumber);
    //let outModel: Company = this.cleanUp(model.companyDTO);
    model.companyDTO.panDate = this.datePipe.transform(model.companyDTO.panDate, 'yyyy-MM-dd HH:mm:ss');
    model.companyDTO = this.cleanUp(model.companyDTO);
    model.partyBankMapListDeletedIds = this.bankMapItemToBeRemove;
    
    this.spinStart = true;

    let message: string;
    if (model.companyDTO.id === null) {
      message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
    }
    else {
      message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
    }
    if(this.signaturePath===null){
      model.companyDTO.signatureImagePath =null;
    }
    if(this.certImagePath===null){
      model.companyDTO.ceritificateImagePath=null;
    }

    if(this.companyLogoPath===null){
      model.companyDTO.companyLogoPath=null;
    }

   console.log("model.companyDTO.companyCurrencyId = "+model.companyDTO.companyCurrencyId)
    this.companyService.save(model)
      .subscribe(response => {
        this.saveStatus = true;
        this.spinStart = false;
        this.transactionStatus = message;
        this.alertService.success(this.transactionStatus);
        localStorage.setItem(AppSettings.CURRENT_COMPANY, JSON.stringify(response));
      },
        error => {
          //console.log("Error ", error);
          this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;

          this.saveStatus = false;
          this.spinStart = false;
          this.alertService.danger(this.transactionStatus);
        })

  }

  clearForm() {

  }

  closeForm() {
    // this.zone.run( () => {
    //   this._router.navigate(['/']);
    // })
    this.routingUtilityService.close();
  }
  deleteAddress(index: number) {
    ////console.log("fvvfvffv", index);
    (<FormArray>this.companyForm.controls['companyDTO'].get('addressesListDTO')).removeAt(index);
    //controls['addressesListDTO']).removeAt(index);

    return false;
  }

  getRegstrationType() {
    // this.companyForm.controls['companyDTO'].get('gstRegistrationTypeId').valueChanges.subscribe(items => {
    //console.log("gstRegistrationTypeId" + this.companyForm.get('companyDTO.gstRegistrationTypeId').value)
    //console.log(' before isGstRegistered: ', this.isGstRegistered);
    let isGstRegistered = false;

    if (+this.companyForm.get('companyDTO.gstRegistrationTypeId').value === 3) {
      this.isGstRegistered = false;
    }
    else {
      this.isGstRegistered = true;
    }
    //console.log(' afetr isGstRegistered: ', this.isGstRegistered);
    // })

    if (this.isGstRegistered) {
      //console.log(' afetr isGstRegistered: false case ', this.isGstRegistered);
      this.companyForm.controls['companyDTO'].get('gstNumber').setValidators(Validators.compose([Validators.minLength(20), Validators.maxLength(20)]));
      //  this.companyForm.controls['companyDTO'].get('gstNumber').setValidators( Validators.pattern('[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}'));
      this.companyForm.controls['companyDTO'].get('gstNumber').updateValueAndValidity();
      this.companyForm.controls['companyDTO'].updateValueAndValidity();


    }
    else {
      this.companyForm.controls['companyDTO'].get('gstNumber').patchValue("");
      this.companyForm.controls['companyDTO'].get('gstNumber').setValidators(null);
      this.companyForm.controls['companyDTO'].get('gstNumber').updateValueAndValidity();
      this.companyForm.controls['companyDTO'].updateValueAndValidity();
    }
    //console.log('this.companyForm: ', this.companyForm.valid);
    //console.log('this.companyForm: ', this.companyForm);
  }

  validateGST() {
    let gstNum: string = this.companyForm.get('companyDTO.gstNumber').value;
    console.log("gstNum"+gstNum)
    let stateId: number = this.companyForm.get('companyDTO.stateId').value;
    let state: State = this.states.find(s => s.id === stateId)
    //console.log("fvv",gstNum,stateId,state,this.states);
    //console.log("state code: ", state.stateCode, gstNum.substr(1,2), +gstNum.substr(0,2));
    if (+gstNum.substr(0, 2) !== +state.stateCode) {
      //console.log("Incorrect GST state Code");
      this.alertService.warning("Incorrect GST state Code");
      this.companyForm.get('companyDTO.gstNumber').setErrors({ 'incorrect': true });
    }


  }


  resizeAndPush(images: any[]) {
    //console.log('in resizeAndPush : ', images.length);

    this.imgMaxPXSizeService.resize(images, 10000, AppSettings.MAX_IMAGE_SIZE).subscribe(
      result => {
        //console.log('uploading...', result.name);
        //let uploadedImage: File = new File([result], result.name);
        //this.uploadedImages.push({ name: result.name, file: uploadedImage });
        ////console.log("this.uploadedImages: "+this.uploadedImages);
        let fileItem = new File([result], result.name);
        //console.log("fileItem: "+fileItem);
        //console.log("file input has changed. The file is", fileItem.type)
        //this.fileToUpload = fileItem
        // if(fileItem.type.includes('image')){
        this.fileUploadSub = this.fileUploadService.fileUpload(
          this.companyLogoUrl,
          fileItem,
          null).subscribe(
            event => {
              this.handleProgress(event, fileItem)
              //this.uploadedImages = [];
              this.companyService.getCompanyLogoAsBase64(1)
                .subscribe(response => {
                  this.companyService.companyLogoBase64 = response;
                  this.uploadingProgressing = false;
                })
            },
            error => {
              //console.log("Server error: ", error);
              this.uploadingProgressing = false;
            });
        // }
        // else{
        //   this.alertService.danger('Only image type is accepted')
        // }

      },
      error => {
        //console.log('😢 Oh no!', error);
        this.uploadingProgressing = false;
        // this.isImageUploadProgress = false;
      }
    );
  }


  resizeAndPushCertificateImage(images: any[]) {
    //console.log('in resizeAndPush : ', images.length);

    this.imgMaxPXSizeService.resize(images, 10000, AppSettings.MAX_IMAGE_SIZE).subscribe(
      result => {
        //console.log('uploading...', result.name);
        let fileItem = new File([result], result.name);
        //console.log("fileItem: "+fileItem);
        //console.log("file input has changed. The file is", fileItem.type)

        this.fileUploadSub = this.fileUploadService.fileUpload(
          this.certificateImageUrl,
          fileItem,
          null).subscribe(
            event => {
              this.handleProgress(event, fileItem, "CERT")

              this.uploadingProgressing = false;
              this.companyService.getCertImageAsBase64(1)
                .subscribe(response => {
                  this.companyService.certImageBase64 = response;
                  this.uploadingProgressing = false;
                })

            },
            error => {
              //console.log("Server error: ", error);
              this.uploadingProgressing = false;
            });
        // }
        // else{
        //   this.alertService.danger('Only image type is accepted')
        // }

      },
      error => {
        //console.log('😢 Oh no!', error);
        this.uploadingProgressing = false;
        // this.isImageUploadProgress = false;
      }
    );
  }

  resizeAndPushSiganture(images: any[]) {
    //console.log('in resizeAndPush : ', images.length);

    this.imgMaxPXSizeService.resize(images, 10000, AppSettings.MAX_IMAGE_SIZE).subscribe(
      result => {
        //console.log('uploading...', result.name);
        //let uploadedImage: File = new File([result], result.name);
        //this.uploadedImages.push({ name: result.name, file: uploadedImage });
        ////console.log("this.uploadedImages: "+this.uploadedImages);
        let fileItem = new File([result], result.name);
        //console.log("fileItem: "+fileItem);
        //console.log("file input has changed. The file is", fileItem.type)
        //this.fileToUpload = fileItem
        // if(fileItem.type.includes('image')){
        this.fileUploadSub = this.fileUploadService.fileUpload(
          this.signatureUrl,
          fileItem,
          null).subscribe(
            event => {
              this.handleProgress(event, fileItem, "SIGN")
              //this.uploadedImages = [];
              this.companyService.getSignatureImageAsBase64(1,1)
                .subscribe(response => {
                  this.companyService.signatureLogoBase64 = response;
                  this.signatureUploadingProgressing = false;
                })
            },
            error => {
              console.log("Server error: ", error);
              this.signatureUploadingProgressing = false;
            });
        // }
        // else{
        //   this.alertService.danger('Only image type is accepted')
        // }

      },
      error => {
        //console.log('😢 Oh no!', error);
        this.signatureUploadingProgressing = false;
        // this.isImageUploadProgress = false;
      }
    );
  }






  checkStateChange() {
    //console.log("checkStateChange(): "+this.companyWithBankListDTO.companyDTO.id);
    if (this.companyWithBankListDTO.companyDTO.id) {
      this.companyService.getCompanyTransactions()
        .subscribe(response => {
          if (response.responseStatus !== 1) {
            //console.log("Transactions exists");
            this.alertService.danger(response.responseString);
            this.companyForm.get('companyDTO.stateId').patchValue(this.companyWithBankListDTO.companyDTO.stateId);
          }
        })
    }
  }

}

export interface GSTRegistrationType {
  id: number,
  name: string,
}

export interface UploadedImages {
  name: string;
  file: File;

}