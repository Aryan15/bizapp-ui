import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FinancialYear, FinancialYearWrapper } from '../../data-model/financial-year-model';
import { FinancialYearService } from '../../services/financial-year.service';
import { AppSettings } from '../../app.settings'
import { Router, ActivatedRoute } from '@angular/router';
import { AlertDialog } from '../../utils/alert-dialog';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertService } from 'ngx-alerts';
import { UserService } from '../../services/user.service';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivityRoleBinding } from '../../data-model/activity-model';
@Component({
  selector: 'app-financial-master',
  templateUrl: './financial-master.component.html',
  styleUrls: ['./financial-master.component.scss']
})
export class FinancialMasterComponent implements OnInit {



  public financialForm: FormGroup;
  public financialYear: FinancialYear;
  public financialYears: FinancialYear[] = [];
  financialYearWrapper: FinancialYearWrapper[] = [];



  transactionStatus: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  isActiveCheck: boolean = false;
  datePipe = new DatePipe('en-US');
  alertRef: MatDialogRef<AlertDialog>;
  dialogRef: MatDialogRef<ConfirmationDialog>;

  applicableButtons: ApplicableButtons ={
    isApproveButton: false,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
};

activityRoleBindings: ActivityRoleBinding[];

  constructor(private fb: FormBuilder,
    private _router: Router,
    public route: ActivatedRoute,
    public dialog: MatDialog,
    public alertService: AlertService,
    public userService: UserService,
    private financialYearService: FinancialYearService) {
      this.setActivityRoleBinding();
     }

  ngOnInit() {
    this.initForm();
    this.getFinancialYears();
    this.initFinancialYear();
  }

  private getFinancialYears() {

    this.financialYearService.getAllFinancialYears()
      .subscribe(response => {
        this.financialYearWrapper = response;
        //console.log("this.financialYearsList", this.financialYearWrapper)

        //  this.patchForm();
      })
  }

  setActivityRoleBinding(){
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {

        ////console.log('in action panel: ', activityRoleBinding.permissionToUpdate);
        ////console.log('in action panel: ', activityRoleBinding.permissionToCreate);
        ////console.log('in action panel activityRoleBinding.permissionToCancel: ', activityRoleBinding.permissionToCancel);

        // this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
      });
  })
}


  private initForm() {


    let data: FinancialYearWrapper = {
      // financialYearList: this.getFinancialYear(),
      financialYearList: null,
    }
    this.financialForm = this.toFormGroup(data);
    // });
  }
  private getFinancialYear(): FinancialYear {

    let fs: FinancialYear = {
      id: null,
      deleted: null,
      startDate: null,
      endDate: null,
      financialYear: null,
      isActive: null,
      isModified: null,
    }
    return fs;
  }

  private toFormGroup(data: FinancialYearWrapper): FormGroup {
    const financialYearArray = new FormArray([]);
    if (data.financialYearList) {
      data.financialYearList.forEach(item => {
        financialYearArray.push(this.makeFinancialYear(item));
      })
    }
    //console.log("financialYearArray", financialYearArray)
    const formGroup = this.fb.group({

      financialYearList: financialYearArray,//[this.buildNumberRange()], //[data.numberRangeConfigurations],

    });
    //console.log("formGroup", formGroup)
    return formGroup;

  }

  private makeFinancialYear(financialYear: FinancialYear): FormGroup {

    return this.fb.group({
      id: [financialYear.id],
      startDate: [this.datePipe.transform(financialYear.startDate, AppSettings.DATE_FORMAT)],
      endDate: [this.datePipe.transform(financialYear.endDate, AppSettings.DATE_FORMAT)],
      financialYear: [financialYear.financialYear],
      isActive: [financialYear.isActive],
      deleted: [financialYear.deleted],
      isModified: [financialYear.isModified],
    });
  }


  closeForm() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }

  private cleanUp(model: FinancialYearWrapper): FinancialYearWrapper {
    // Strip out null rows which are added at the end just for ease of user entry purposes.
    //let outModel : SettingsWrapper = <SettingsWrapper>{};

    //outModel.globalSetting = model.globalSetting;

    model.financialYearList.forEach(fin => {
      if (fin.financialYear === null) {
        model.financialYearList.pop();
      }
    });
    return model;

  }
  beforeSubmit(model: FinancialYearWrapper){

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
  });
  this.dialogRef.componentInstance.confirmMessage = AppSettings.FINANCIAL_YEAR_AUTONUMBER_RESET
  this.dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.onSubmit(model)

      }
      else {
         
          return;
      }
  });

      
  }


  onSubmit(model: FinancialYearWrapper) {

   let checkDuplicate: boolean = this.checkDuplicate(model);
    //console.log("checkduplit return", checkDuplicate)
    if (!checkDuplicate) {
      return false;
    }


    let checkDefault: boolean = this.checkDefault(model);
    //console.log("checkduplit return", checkDefault)
    if (!checkDefault) {
      return false;
    }

    if (!this.financialForm.valid) {
      //console.log("invalid")
      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE);
      return false;
    }
    // this.financialYearWrapper.financialYearList.forEach(proj => {
    //   if (proj.ComparedDate >=value1 && proj.ComparedDate<=value2)
    //   {
    //        return proj.title;
    //   }
    // }
    // model.financialYearList.forEach(item => {
    //   if (item.startDate >=value1 && item.endDate<=value2)
    //     {
    //          return proj.title;
    //     }
    // })
    //console.log('Before save: ', model);
    this.spinStart = true;
    model.financialYearList.forEach(item => {
      item.isActive = item.isActive ? 1 : 0;
    })
    //console.log('Before saveing: ', model);
    let outModel: FinancialYearWrapper = this.cleanUp(model);
    // model.i = model.globalSetting.itemLevelTax ? 1 : 0;
    this.financialYearService.save(outModel)
      .subscribe(response => {
        this.saveStatus = true;
        this.spinStart = false;
        this.transactionStatus = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        this.alertService.success(this.transactionStatus);
      },
      error => {
        //console.log("Error ", error);
        this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
        this.alertService.danger(this.transactionStatus);
        this.saveStatus = false;
        this.spinStart = false;
      })

  }



  delete(idx: number) {
    //console.log("idx..........",idx);
    const control = <FormArray>this.financialForm.controls['financialYearList'];

    //console.log('fin before delete', control.at(idx).get('id').value);
    //console.log('actv before delete', control.at(idx).get('isActive').value);
    let id: number = control.at(idx).get('id').value;
    let active: number = control.at(idx).get('isActive').value;
    if (id == null) {
      (<FormArray>this.financialForm.get('financialYearList')).removeAt(idx);
      return;
    }

    if (active === 1) {

      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false

      });
      this.alertRef.componentInstance.alertMessage = "Cannot delete Active Financial Year"

    }
    else {
      this.financialYearService.delete(id).subscribe(response => {
        //console.log('Deleted: ', response);

        if (response.responseStatus === 1) {
          (<FormArray>this.financialForm.get('financialYearList')).removeAt(idx);
          this.alertService.success("Financial Year Deleted");
        }
        else {
          //console.log('Cannot delete!!');
          this.alertService.danger('Cannot delete already used Financial Year');
        }
      },
        error => {
          //console.log('Error: ', error);
          this.alertService.danger('Cannot delete!!');
        })
    }

    return false;
  }

  checkIsDefault(index: number) {
    const control = <FormArray>this.financialForm.controls.financialYearList;
    this.isActiveCheck = control.at(index).get('isActive').value;
    //console.log("checked", this.isActiveCheck)

    // if (this.isActiveCheck === true) {
    for (let i = 0; i < control.length; i++) {
      //console.log('in default function is default value: ', control.at(i).get('isActive').value)
      if ((control.at(i).get('isActive').value === 1 || control.at(i).get('isActive').value === "Y" ||
        control.at(i).get('isActive').value === true) && i != index) {

        // this.alertRef = this.dialog.open(AlertDialog, {
        //   disableClose: false

        // });
        // this.alertRef.componentInstance.alertMessage = AppSettings.TERMS_AND_CONDITION_MESSAGE
        // control.at(index).get('isActive').patchValue(false)

        this.dialogRef = this.dialog.open(ConfirmationDialog, {
          disableClose: false
        });

        this.dialogRef.componentInstance.confirmMessage = "Do you want to change financial year?"

        this.dialogRef.afterClosed().subscribe(result => {
          if (result) {
            control.at(i).get('isActive').patchValue(false);
            control.at(index).get('isActive').patchValue(true);
          }
          else {
            control.at(index).get('isActive').patchValue(false);
          }
        }
        );

      }
      else {
        //console.log("continue")
      }

    }

  }
  private initFinancialYear() {

    //let ncOut : NumberRangeConfiguration[];

    this.financialYearService.getFinancialYears()
      .subscribe(response => {
        //console.log('response: ', response);
        //ncOut = response;
        this.financialYears = response;
        const control = <FormArray>this.financialForm.controls['financialYearList'];

        this.financialYears.forEach(item => {
          control.push(this.makeFinancialYear(item));
        })

        this.addBlankFinItem();
      });

  }
  checkDefault(model: FinancialYearWrapper): boolean {
    let count:number=0;
        //console.log("in default", count)
        const finControl = <FormArray>this.financialForm.controls['financialYearList'];
        //console.log("contro;", finControl.length)
        for (let i = 0; i < finControl.length; i++) {
          let val = finControl.at(i).get('isActive').value;
         //console.log("val",val)
            if (+val !=+ 0 || val == true) {
              //console.log("in",val)
              count++;
            }
          }
            //console.log("count",count)
            if(count === 0)
            {
              this.alertRef = this.dialog.open(AlertDialog, {
                disableClose: false
    
              });
              this.alertRef.componentInstance.alertMessage = "Please choose active financial year!!!"
              return false;
            }
    
        
    
        return true;
      }
  checkDuplicate(model: FinancialYearWrapper): boolean {

    //console.log("in duplicate")
    const finControl = <FormArray>this.financialForm.controls['financialYearList'];
    //console.log("contro;", finControl.length)
    for (let i = 0; i < finControl.length; i++) {
      let val = finControl.at(i).get('financialYear').value;
      for (let j = i + 1; j < finControl.length; j++) {
        let val2 = finControl.at(j).get('financialYear').value;
        //console.log("val", val, val2)
        if (val == val2) {
          //console.log("dupli")
          this.alertRef = this.dialog.open(AlertDialog, {
            disableClose: false

          });
          this.alertRef.componentInstance.alertMessage = "Financial Year already exists!!!"
          finControl.at(j).get('financialYear').patchValue("")
          finControl.at(j).get('startDate').patchValue("")
          finControl.at(j).get('endDate').patchValue("")
          finControl.at(j).get('isActive').patchValue("")
          return false;
        }

      }
      model.financialYearList.forEach(item => {
        item.startDate
      })
    }

    return true;
  }
  private addBlankFinItem() {
    const control = <FormArray>this.financialForm.controls['financialYearList'];
    //Add one blank Item
    let item: FinancialYear = {
      id: null,
      deleted: null,
      startDate: null,
      endDate: null,
      financialYear: null,
      isActive: null,
      isModified: null,
    };

    control.push(this.makeFinancialYear(item));
  }
  addOneYear(event, index: number) {
    const control = <FormArray>this.financialForm.controls.financialYearList;
    // //console.log("control", control);
    // for (let i = 0; i < control.length; i++) {
    //   let val1 = control.at(i).get('startDate').value;
    //   let val2 = control.at(i).get('endDate').value;

    // }
    // this.financialYearWrapper.financialYearList.forEach(proj => {
    //   if (proj.ComparedDate >=value1 && proj.ComparedDate<=value2)
    //   {
    //        return proj.title;
    //   }
    // }
    let x = 12;
    let strDate: string
    let strMonth: string
    let strYear: string
    let dayOfOneDigit: number = event.targetElement.value.indexOf("/", 0);
    let dayOfTwoDigit: number = event.targetElement.value.indexOf("/", 0)
    let monthOfOneDigit: number = event.targetElement.value.indexOf("/", +event.targetElement.value.indexOf("/", 0) + 1);
    // let monthOfTwoDigit: number = event.targetElement.value.indexOf("/", +)
    //console.log("event", event.targetElement.value)
    //console.log("dayOfOneDigit", event.targetElement.value.indexOf("/", 0))
    //console.log("monthOfOneDigit", event.targetElement.value.indexOf("/", +event.targetElement.value.indexOf("/", 0) + 1))
    if (dayOfOneDigit === 1 && monthOfOneDigit === 3) {
      strDate = event.targetElement.value.substring(0, 1);
      strMonth = event.targetElement.value.substring(2, 3);
      strYear = event.targetElement.value.substring(4);
    }
    if (dayOfOneDigit === 1 && monthOfOneDigit === 4) {
      strDate = event.targetElement.value.substring(0, 1);
      strMonth = event.targetElement.value.substring(2, 4);
      strYear = event.targetElement.value.substring(5);
    }
    if (dayOfOneDigit === 2 && monthOfOneDigit === 4) {
      strDate = event.targetElement.value.substring(0, 2);
      strMonth = event.targetElement.value.substring(3, 4);
      strYear = event.targetElement.value.substring(5);
    }
    if (dayOfOneDigit === 2 && monthOfOneDigit === 5) {
      strDate = event.targetElement.value.substring(0, 2);
      strMonth = event.targetElement.value.substring(3, 5);
      strYear = event.targetElement.value.substring(6);
    }
    let date = strYear + " " + strMonth + " " + strDate;
    //console.log("strDate", strDate)
    //console.log("strMonth", strMonth)
    //console.log("Date", date)
    let CurrentDate = new Date(date);

    CurrentDate.setMonth(CurrentDate.getMonth() + x);
    CurrentDate.setDate(CurrentDate.getDate() - 1);
    //console.log("endDate", CurrentDate.getDate() + "/" + (CurrentDate.getMonth() + 1) + "/" + CurrentDate.getFullYear())
    // let nextDate:string = CurrentDate.getDate() + "/" + (CurrentDate.getMonth() + 1) + "/" + CurrentDate.getFullYear();
    let nextDate: string = CurrentDate.getFullYear() + "/" + (CurrentDate.getMonth() + 1) + "/" + CurrentDate.getDate();
    //console.log("endDate", nextDate)
    let da = this.datePipe.transform(nextDate, AppSettings.DATE_FORMAT)
    //console.log("da", da)
    control.at(index).get('endDate').patchValue(da);
  }

}
