import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserPassword, UserModel } from '../../data-model/user-model';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { AlertService } from 'ngx-alerts';
import { AppSettings } from '../../app.settings';
import { AlertDialog } from '../../utils/alert-dialog';
import { UtilityService } from '../../utils/utility-service';

@Component({
  selector: 'app-password-popup',
  templateUrl: './password-popup.component.html',
  styleUrls: ['./password-popup.component.scss']
})
export class PasswordPopupComponent implements OnInit {

  public passwordPopUpForm: FormGroup;
  // public userModel: UserPassword;

  currentUser: any;
  themeClass: any;

  confirmPassword : FormControl = new FormControl('', Validators.required);
  hide: boolean = true;
  isNewEntry: boolean = false;
  alertRef: MatDialogRef<AlertDialog>;
  emailVerified: boolean = false;
  usernameVerified: boolean = false;
  isEmailReadOnly: boolean = false;
  isUsernameReadOnly: boolean = false;

  constructor(private fb: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: UserModel,
    private userService: UserService,
    private dialogRef: MatDialogRef<PasswordPopupComponent>,
    public dialog: MatDialog,
    private alertService: AlertService,
    private utilityService: UtilityService) {
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;
     }

  
  ngOnInit() {
    
    this.isNewEntry = this.data.username ? false : true;
    this.isEmailReadOnly = this.data.email ? true : false;
    this.emailVerified = this.isEmailReadOnly ? true : false;
    this.usernameVerified = this.isEmailReadOnly ? true : false;
    this.isUsernameReadOnly = this.data.username ? true : false;
    this.initForm();
  }

  private initForm() {
    let data: UserModel = <UserModel>{};
    //console.log("data: ", this.data);
    data.id = this.data.id;
    data.username = this.data.username;
    data.email = this.data.email;
    this.passwordPopUpForm = this.toFormGroup(data);
  }

  private toFormGroup(data: UserModel): FormGroup {

    const formGroup = this.fb.group({
      email: [data.email, Validators.compose([Validators.required, Validators.email, this.noWhitespaceValidator])],
      id: [data.id],
      username: [data.username,Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.noWhitespaceValidator])],
      password: [data.password,Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(12), this.noWhitespaceValidator])],
    });
    
    return formGroup;
  }


  checkEmailAvailability() {
    let email: string = this.passwordPopUpForm.get('email').value
    //console.log("email: ", email);
    if (email && email.length > 0) {
      this.userService.checkEmpEmailAvailability(email).subscribe(response => {

        //console.log("is email avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
          this.emailVerified = true;
        } else {
          //console.log("No");
          // this.alertService.danger(response.responseString + AppSettings.EMAIL_UNIQUE_NUMBER_MESSAGE);          
          // this.passwordPopUpForm.get('email').patchValue('');
          this.passwordPopUpForm.get('email').setErrors({ 'emailTaken': true });
          // this.emailElement.nativeElement.focus();
          this.emailVerified = false;
        }

      });
    }

  }

  //this method is use to change password
  save(){

    if(!this.emailVerified){
      //console.log("this.emailVerified: ", this.emailVerified);
      return;
    }
    if(!this.usernameVerified){
      //console.log("this.usernameVerified: ", this.usernameVerified);
      return;
    }
    if(this.passwordPopUpForm.valid){

      if(this.isNewEntry){
        this.data.username = this.passwordPopUpForm.get('username').value;
        this.data.email = this.passwordPopUpForm.get('email').value;
        this.userService.newEmployee(this.data)
        .subscribe(response => {
          if(response && response.responseStatus == 1){
            this.userService.changePassword(this.passwordPopUpForm.value)
            .subscribe(presponse => {
                    if(presponse){
                      this.alertService.success( "Password Changed successfully");
                      this.dialogRef.close(this.passwordPopUpForm.value);
                    }
                    else{
                      this.alertService.danger("Failed to change password");
                    }
                  },
                  error => {
                    this.alertService.danger("Failed to change password");
                  })
          }else{
            this.alertService.danger("Failed to change password");
          }
        })
      }else{
        this.userService.changePassword(this.passwordPopUpForm.value)
            .subscribe(presponse => {
                    if(presponse){
                      this.alertService.success( "Password Changed successfully");
                      this.dialogRef.close(this.passwordPopUpForm.value);
                    }
                    else{
                      this.alertService.danger("Failed to change password");
                    }
                  })
      }
    }else{
      //console.log("form invalid");
      const controls = this.passwordPopUpForm;
      let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
      //console.log("invalidFieldList ", invalidFieldList)
    }
    
  }

  public noWhitespaceValidator(control: FormControl) {
    // const isWhitespace = (control.value || '').indexOf(' ') > 0
    const isWhitespace = /\s/g.test(control.value || '');
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  validatePassword() {
    //console.log("confirmPassword" + this.confirmPassword.value)

    if (this.confirmPassword.value !== "") {
      if (this.passwordPopUpForm.get('password').value !== this.confirmPassword.value) {
        //console.log('Not matching: ', this.passwordPopUpForm.get('password').value, this.confirmPassword);
        this.passwordPopUpForm.get('password').setErrors({ 'validateEqual': true });
        this.passwordPopUpForm.get('password').setErrors({ 'incorrect': true });
        this.confirmPassword.setErrors({ 'incorrect': true });
        this.passwordPopUpForm.get('password').markAsTouched();

      }
      else {
        //console.log('Matching');
      
        this.passwordPopUpForm.get('password').setErrors(null);
        this.passwordPopUpForm.get('password').setErrors(null);
      }
    }
    
    if(this.passwordPopUpForm.get('password').value === this.passwordPopUpForm.get('username').value){
      this.passwordPopUpForm.get('password').setErrors({ 'sameAsUsername': true });
    }else{
      this.passwordPopUpForm.get('password').setErrors(null);
    }

  }


  checkUsernameAvailability() {
    let username: string = this.passwordPopUpForm.controls['username'].value;
    if (username === "" || (username === this.data.username) || !this.passwordPopUpForm.get('username').valid )
    {
      this.usernameVerified = false;
      return;
    }
    if (username && username.length > 0) {
      this.userService.checkUsernameAvailability(username).subscribe(response => {

        //console.log("is user avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
          this.usernameVerified = true;
        } else {
          //console.log("No");
          this.usernameVerified = false;

          // this.alertRef = this.dialog.open(AlertDialog, {
          //   disableClose: false
          // });

          // this.alertRef.componentInstance.alertMessage = response.responseString;

          // this.alertRef.afterClosed().subscribe(result => {
          //   this.passwordPopUpForm.controls['username'].patchValue('');
          //   // this.usernameElement.nativeElement.focus();
          // })

          this.passwordPopUpForm.get('username').setErrors({ 'taken': true });
        }

      });
    }

  }
}