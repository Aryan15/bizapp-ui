import { Component, Input, OnChanges, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable ,  merge ,  of as observableOf } from 'rxjs';
import { catchError ,  map ,  debounceTime ,  startWith ,  switchMap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { PartyService } from '../../services/party.service';
import { PartyWithPriceList, RecentParty } from '../../data-model/party-model';

const PAGE_SIZE: number = 10;

@Component({
  selector: 'recent-parties-table',
  templateUrl: './recent-parties-table.component.html',
  styleUrls: ['./recent-parties-table.component.scss']
})
export class RecentPartiesMasterTableComponent implements OnInit, OnChanges {
  @Input()
  recentPartiesLoadCounter: number;

  @Output()
  Party: EventEmitter<PartyWithPriceList> = new EventEmitter<PartyWithPriceList>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();

  displayedColumns = ['partyTypeName', 'partyCode' , 'name', 'primaryMobile'];
  partyMasterDatabase: PartyMasterHttpDao | null;

  dataSource = new MatTableDataSource<PartyWithPriceList>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private partyService: PartyService) { }


  ngOnInit() {

    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.partyMasterDatabase = new PartyMasterHttpDao(this.partyService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.partyMasterDatabase!.getParties(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        ////console.log('data.materialList: ', data.partyWithPriceLists);
        ////console.log('data.totalCount: ', data.totalCount);
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.partyWithPriceLists;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        //console.log('catchError error');
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);

  }

  ngOnChanges() {
    //this.onChangeEvent.
    // //console.log('this.recentInvoiceLoadCounterObservable: ',this.recentInvoiceLoadCounterObservable);
    // this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);
    //console.log('in onchanage: ', this.recentPartiesLoadCounter);

    if (this.recentPartiesLoadCounter > 0) {
      this.partyMasterDatabase = new PartyMasterHttpDao(this.partyService);

      this.partyMasterDatabase!.getParties(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.partyWithPriceLists);
          //console.log('data.totalCount: ', data.totalCount);
          if (this.filterForm.get('filterText').value != null) {
            if (data.totalCount === 0) {
              this.noDataFound.emit(true);
            }
          }
          return data.partyWithPriceLists;
        })).subscribe(data => this.dataSource.data = data);
    }
  }


  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.Party.emit(row);
    //console.log('done');
  }

}


export class PartyMasterHttpDao {

  constructor(private partyService: PartyService) { }

  getParties(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentParty> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);


    return this.partyService.getRecentgetParties(filterText, sortColumn, sortDirection, page, pageSize);
  }
}