import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Party, PartyWithPriceList, MaterialPriceList, AddressList, PartyType, PartyBankMap, } from '../../data-model/party-model';
import { MaterialType } from '../../data-model/material-type-model';
import { Material } from '../../data-model/material-model';
import { State } from '../../data-model/state-model';
import { Country } from '../../data-model/country-model';
import { Area } from '../../data-model/area-model';
import { City } from '../../data-model/city-model';
import { LocationService } from '../../services/location.service';
import { GlobalSettingService } from '../../services/global-setting.service';
import { GlobalSetting } from '../../data-model/settings-wrapper';
import { PartyService } from '../../services/party.service';
import { MaterialService } from '../../services/material.service';
import { MaterialTypeService } from '../../services/material-type.service';
import { ParamMap, Router } from '@angular/router';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { BankModel } from '../../data-model/bank-model';
import { BankService } from '../../services/bank.service';
//import { TranslatorContainer, provideTranslator, Translator } from 'angular-translator';
import { UtilityService } from '../../utils/utility-service';
import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { Currency } from '../../data-model/currency-model';
import { TransactionType } from '../../data-model/transaction-type';
//import { constants } from 'fs';

@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  // providers: [provideTranslator('menu')],
  styleUrls: ['./party.component.scss']
})
export class PartyComponent implements OnInit, OnDestroy {

  @ViewChild('email', { static: true }) emailElement: ElementRef;
  @ViewChild('phone', { static: true }) phoneElement: ElementRef;
  @ViewChild('gstNumber', { static: true }) GSTElement: ElementRef;
  @ViewChild('partyNumber', { static: true }) PartyElement: ElementRef;
  partyUsedInTransactons: boolean = false;

  title: String;
  subheading: string;

  selectedMaterialId: number[] = [];

  selectedPartyType: Party;
  public partyForm: FormGroup;
  //public party: Party;
  public partyWithPriceList: PartyWithPriceList;
  recentPartiesLoadCounter: number = 0;
  public myHeaders: Headers;
  transactionType: TransactionType;
  //public imageUrl: string;
  public partyTypes: PartyType[] = [];
  public partyType:PartyType;
  fieldValidatorLabels = new Map<string, string>();
  public materialTypes: MaterialType[] = [];
  public materials: Material[] = [];
  public filteredMaterials: Material[] = [];
  public banks: BankModel[] = [];
  public uniqueMaterialNames: String[] = [];
  onDestroy: Subject<boolean> = new Subject();
  //public materialPartNumbers: Material[] = [];
  isSpecialPriceApplicable: boolean = false;
  isGstRegistered: boolean = false;
  globalSetting: GlobalSetting;
  activityId: number = 0;
  partyIdValue:string;
  public gstRegistrationTypes: GSTRegistrationType[] = [
    { id: 1, name: "Normal" },
    { id: 2, name: "Composition" },
    { id: 3, name: "Unregistered / URP" }
  ]
  material: Material;
  //materialNameLocal: string;
  materialLocal: Material;
  bankLocal: BankModel;
  materialPartNumber: Material;
  materialType: MaterialType;
  
  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  id:number;
  countries: Country[] = [];
  currencys: Currency[] =[];
  states: State[] = [];
  allStates: State[] = [];
  allCities: City[] = [];
  allAreas: Area[] = [];
  allcountries: Country[] = [];
  allCurrensys: Currency[] = [];
  cities: City[] = [];
  areas: Area[] = [];
  public parties: Party[] = [];
  tempTransactionNumber: string = "";
  transactionStatus: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  isCust: boolean = false;
  oldGstNumber: string = " ";
  isAutoNumber: boolean = false;
  isImportExport:boolean=false;
  hideIgst :boolean=false;
  taxNumberText:string ="GST Number";

  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: true,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: true,
    isDraftButton: false,
  };

  activityRoleBindings: ActivityRoleBinding[];
  //statusBasedPermission: StatusBasedPermission;

  itemToBeRemove: number[] = [];
  bankMapItemToBeRemove: number[] = [];
  constructor(private fb: FormBuilder,
    private _router: Router,
    private partyService: PartyService,
    public dialog: MatDialog,
    public route: ActivatedRoute,
    private userService: UserService,
    // private translatorContainer: TranslateLoader,
    private materialTypeService: MaterialTypeService,
    private materialService: MaterialService,
    private alertService: AlertService,
    private globalSettingService: GlobalSettingService,
    private locationService: LocationService,
    private utilityService: UtilityService,
    private translate: TranslateService,
    private bankService: BankService,
    private transactionTypeService: TransactionTypeService,
    private numberRangeConfigService: NumberRangeConfigService) {

    this.title = this.userService.activityName
    this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
    this.setActivityRoleBinding();
    translate.setDefaultLang('en');
  }

  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {
        this.activityId = activityRoleBinding.activityId;


      });




    })
  }
  ngOnInit() {
    //this.myHeaders = this.partyService.getHeaders();
   this.setPartyType();
    this.getPartyTypes();
    //this.handlePartytypeChanges();
    this.getMaterialTypes();
    this.getBanks();
    this.getAllCountries();
    this.getAllStates();
    this.getAllCities();
    this.getAllAreas();
    this.getAllCurrencys();
    //this.getAllMaterials();
    this.initForm();
    this.getGlobalSetting();
    //this.getType();
    this.handleChanges();
    // this.getRegstrationType();
    this.checkOrVerifyCurrency();
   
    
    
  }

  ngOnDestroy() {
    ////console.log('ngOnDestory');
    this.onDestroy.next(true);
    this.onDestroy.complete();
    //this.tranCommSub.unsubscribe();

  }


  private getBanks() {
    this.bankService.getAllBanks()
      .subscribe(response => {
        this.banks = response;
      })
  }

  private getAllCities() {

    this.locationService.getCities()
      .subscribe(response => {
        this.cities = response;
        this.allCities = response;
      })
  }

  private getAllAreas() {

    this.locationService.getAreas()
      .subscribe(response => {
        this.areas = response;
        this.allAreas = response;
      })
  }
  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
        this.allStates = response;
      })
  }

  private getAllCountries() {

    this.locationService.getCountries()
      .subscribe(response => {
        this.countries = response;
        this.allcountries = response;
      })
  }

  
  private getAllCurrencys() {

    this.locationService.getCurrencys()
      .subscribe(response => {
        this.currencys = response;
        this.allCurrensys = response;
      })
  }

  getCities() {
    let areaId = this.partyForm.controls['partyDTO'].get('areaId').value;
    //console.log("areaId", areaId)
    let area = this.areas.find(p => p.id === areaId);

    //console.log("city id in area", area.cityId)
    //console.log("this.cities for area", this.cities.find(p => p.id === area.cityId))
    let value = this.cities.find(p => p.id === area.cityId);
    //console.log("value", value.name)
    this.partyForm.controls['partyDTO'].get('cityId').patchValue(value.id);
    this.getStates();
  }
  getStates() {
    let cityId = this.partyForm.controls['partyDTO'].get('cityId').value;
    //console.log("areaId", cityId)
    let city = this.cities.find(p => p.id === cityId);

    //console.log("city id in area", city.stateId)
    //console.log("this.cities for area", this.states.find(p => p.id === city.stateId))
    let value = this.states.find(p => p.id === city.stateId);
    //console.log("value", value.name)
    this.partyForm.controls['partyDTO'].get('stateId').patchValue(value.id);
    this.getCountries();
  }

  getCountries() {
    let stateId = this.partyForm.controls['partyDTO'].get('stateId').value;
    let state = this.states.find(p => p.id === stateId);

    //console.log("city id in area", state.countryId)
    //console.log("this.cities for area", this.countries.find(p => p.id === state.countryId))
    let value = this.countries.find(p => p.id === state.countryId);
    //console.log("value", value.name)
    this.partyForm.controls['partyDTO'].get('countryId').patchValue(value.id);

     this.checkOrVerifyCurrency();

  }



  private getPartyTypes() {

    this.partyService.getAllPartyTypes()
      .subscribe(response => {
        this.partyTypes = response;
      })
  }

  getMaterialTypes() {
    this.materialTypeService.getAllMaterialTypes()
      .subscribe(response => {
        this.materialTypes = response.filter(mt => mt.id != 4); //Exclude Jobwork material types
      })
  }

  getMaterialsByMaterialTypeId() {
    var existingMaterialIds: number[] = []
    //console.log("this.selectedMaterialId............", this.selectedMaterialId);

    this.partyForm.value.materialPriceListDTOList.forEach(mat => {
      existingMaterialIds.push(mat.materialId)
    })


    //clear material and part number

    //this.materialPartNumbers = [];
    this.uniqueMaterialNames = [];
    //this.materialPartNumber = null;

    const id = this.materialType.id;
    const names: string[] = [];
    //console.log("id: ", id);
    this.materialService.getMaterialByMaterialTypeId(id).subscribe(
      response => {
        // this.materials = response;
        //console.log("response.........................", response);
        this.materials = response.filter(fm => !existingMaterialIds.includes(fm.id));

        this.materials.forEach(material => {
          names.push(material.name);
        })
        this.uniqueMaterialNames = Array.from(new Set(names)); //[...new Set(names)];
        this.filteredMaterials = [...this.materials];
      }
    );

    const control = <FormArray>this.partyForm.controls['materialPriceListDTOList'];

    //console.log('control: ', control);
  }

  // getMaterialPartNumber() {

  //   this.materialPartNumbers = [];
  //   let name = this.materialNameLocal;

  //   this.materials.filter(item => item.name === name)
  //     .map(item => this.materialPartNumbers.push(item));

  //   //console.log("part num: ", this.materialPartNumbers);

  // }
  getGlobalSetting() {

    this.globalSettingService.getGlobalSetting().subscribe(response => {
      this.globalSetting = response;
      //console.log("before how ", this.globalSetting.specialPriceCalculation, " .isSpecialPriceApplicable", this.isSpecialPriceApplicable)
      this.isSpecialPriceApplicable = this.globalSetting.specialPriceCalculation === 1 ? true : false;
      //console.log("after how ", this.globalSetting.specialPriceCalculation, "for  .isSpecialPriceApplicable", this.isSpecialPriceApplicable)
    })
  }
  private initForm() {

    const data: PartyWithPriceList = {
      partyDTO: this.initPartyDTO(),
      materialPriceListDTOList: null,
      materialPriceListDeletedIds: [],
      partyBankMapDTOList: null,
      partyBankMapDeletedIds: []
    }
    this.partyForm = this.toFormGroup(data);
  }

  private initPartyDTO(): Party {
    const data: Party = {
      id: null,
      name: null,
      address: null,
      countryId: null,
      stateId: null,
      stateCode: null,
      stateName: null,
     // portStateId : null,
      //currencyName:null,//currencyName
      cityId: null,
      areaId: null,
      pinCode: null,
      primaryTelephone: null,
      secondaryTelephone: null,
      primaryMobile: null,
      secondaryMobile: null,
      email: null,
      contactPersonName: null,
      contactPersonNumber: null,
      webSite: null,
      partyTypeId: null,
      deleted: null,
      billAddress: null,
      panNumber: null,
      gstRegistrationTypeId: null,
      gstNumber: null,
      partyTypeName: null,
      addressesListDTO: null,
      dueDaysLimit: null,
      isIgst: null,
      areaName: null,
      partyId: null,
      partyCode: null,
      partyCurrencyId:null,
      partyCurrencyName:null
    };

    return data;
  }

  private toFormGroup(data: PartyWithPriceList): FormGroup {

    const materialPriceListDTOArray = new FormArray([]);
    const partyBankMapArray = new FormArray([]);


    if (data.materialPriceListDTOList) {
      data.materialPriceListDTOList.forEach(item => {
        //let item : MaterialPriceList = 
        materialPriceListDTOArray.push(this.mapItem(item));
      })
    }

    if (data.partyBankMapDTOList) {
      data.partyBankMapDTOList.forEach(item => {
        partyBankMapArray.push(this.bankMapItem(item))
      })
    }

    const formGroup = this.fb.group({
      partyDTO: this.toPartyFormGroup(data.partyDTO),
      materialPriceListDTOList: materialPriceListDTOArray,
      partyBankMapDTOList: partyBankMapArray
    });

    return formGroup;
  }


  private mapItem(data: MaterialPriceList): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      partyId: [data.partyId],
      materialTypeName: [data.materialTypeName],
      materialName: [data.materialName],
      partNumber: [data.partNumber],
      //mrp: [data.mrp],
      //price: [data.price],
      materialId: [data.materialId],
      discountPercentage: [data.discountPercentage],
      sellingPrice: [data.sellingPrice],
      comment: [data.comment],
      deleted: [data.deleted],
      currentSellingPrice: [data.currentSellingPrice],
      currentBuyingPrice: [data.currentBuyingPrice]
    });

    return formGroup;

  }

  private bankMapItem(data: PartyBankMap): FormGroup {

    //console.log('data: ', data);
    const formGroup = this.fb.group({
      id: [data.id],
      companyId: null,
      partyId: [data.partyId],
      bankId: [data.bankId],
      bankname: [data.bankname],
      branch: [data.branch],
      ifsc: [data.ifsc],
      accountNumber: [data.accountNumber],
      bankAdCode   :[data.bankAdCode],
      openingBalance: [data.openingBalance],
      contactNumber: [data.contactNumber],
    });

    return formGroup;

  }

  private mapAddress(data: AddressList): FormGroup {

    const formGroup = this.fb.group({
      address: [data.address],
      contactPersonName: [data.contactPersonName],
      contactPersonNumber: [data.contactPersonNumber],
      contactPersonDesignation: [data.contactPersonDesignation],
      deleted: [data.deleted],
    });
    return formGroup;
  }

  private toPartyFormGroup(data: Party): FormGroup {
    const addressesArray = new FormArray([]);
    if (data.addressesListDTO) {

      data.addressesListDTO.forEach(address => {

        //let item : MaterialPriceList = 
        addressesArray.push(this.mapAddress(address));
      });
    }

    const formGroup = this.fb.group({
      id: [data.id],
      name: [data.name, Validators.required],
      address: [data.address, Validators.required],
      countryId: [data.countryId, Validators.required],
      stateId: [data.stateId, Validators.required],
     // portStateId :[data.portStateId],
      stateCode: [data.stateCode],
      stateName: [data.stateName],
      cityId: [data.cityId],
      areaId: [data.areaId],
      pinCode: [data.pinCode],
      primaryTelephone: [data.primaryTelephone],
      secondaryTelephone: [data.secondaryTelephone],
      primaryMobile: [data.primaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      secondaryMobile: [data.secondaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      // email: [data.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')],
      email: [data.email ? (data.email.includes("notnull") ? null : data.email) : null],
      contactPersonName: [data.contactPersonName],
      contactPersonNumber: [data.contactPersonNumber],
      webSite: [data.webSite],
      partyTypeId: [data.partyTypeId, Validators.required],
      deleted: [data.deleted],
      billAddress: [data.billAddress],
      panNumber: [data.panNumber],
      gstRegistrationTypeId: [data.gstRegistrationTypeId, Validators.required],
      gstNumber: [data.gstNumber, Validators.pattern('[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}')],
     // gstNumber: [data.gstNumber, data.gstRegistrationTypeId !== 3 ? Validators.compose([Validators.minLength(15), Validators.maxLength(15)]) : null],
      partyTypeName: [data.partyTypeName],
      addressesListDTO: addressesArray,
      dueDaysLimit: [data.dueDaysLimit],
      isIgst: [data.isIgst],
      partyId: [data.partyId],
      partyCode: [data.partyCode,Validators.maxLength(10)],
      partyCurrencyId:[data.partyCurrencyId],
      partyCurrencyName:[data.partyCurrencyName],
    });
    
    if( data.gstRegistrationTypeId === 3 && this.isImportExport )
    {
      this.isGstRegistered = true;
    }
    else{
      this.isGstRegistered = data.gstRegistrationTypeId !== 3 ? true : false;

    }
    console.log("toPartyFormGroup isImportExport ="+this.isImportExport+"this.isGstRegistered = "+this.isGstRegistered)

    this.fieldValidatorLabels.set("partyTypeId", "Party type");
    this.fieldValidatorLabels.set("name", "Party Name");
    this.fieldValidatorLabels.set("address", "Address");
    this.fieldValidatorLabels.set("stateId", "State");
    this.fieldValidatorLabels.set("gstNumber", "GST Number");
    // this.fieldValidatorLabels.set("email", "E-mail");
    this.fieldValidatorLabels.set("gstRegistrationTypeId", "Gst Registration Type");
    this.fieldValidatorLabels.set("countryId", "Country");
    this.fieldValidatorLabels.set("partyCode", "Party Code");

    return formGroup;
  }

  private cleanUp(model: PartyWithPriceList): PartyWithPriceList {

    model.partyDTO.addressesListDTO = model.partyDTO.addressesListDTO.filter(add => add.address);

    return model;

  }

  validateAndSubmit(model: PartyWithPriceList) {
   
   // console.log("currencys"+currencys)
    let gstNumber: string = this.partyForm.get('partyDTO.gstNumber').value
    let countryId = this.partyForm.get('partyDTO.countryId').value
    let partyNumber: string = this.partyForm.get('partyDTO.partyCode').value

     if (!partyNumber && !this.isAutoNumber) {
      this.alertService.danger("Please Enter Party Code");
       return;
    }
    if(gstNumber===null && countryId===1)
    {
       this.alertService.danger("Please Enter the Gst Number ");
    }
   else if (gstNumber && gstNumber.length > 0 && gstNumber != this.oldGstNumber) {
      this.userService.checkValidGST(gstNumber).subscribe(response => {

        if (response.responseStatus === 0) {

          this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
          });

          this.dialogRef.componentInstance.confirmMessage = AppSettings.GST_UNIQUE_NUMBER_CONFIRM_MESSAGE

          this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.submitWrapper(model);
            }

          });

        }
        else {
          this.submitWrapper(model);
        }
      })
    } else {
      this.submitWrapper(model);
    }






  }

  submitWrapper(model: PartyWithPriceList) {
    //console.log("model.partyDTO.name..........>>>>>",model.partyDTO.name);
    ////console.log("this.partyWithPriceList.partyDTO.name .........:",this.partyWithPriceList.partyDTO.name);


    if (this.partyUsedInTransactons === true && this.partyWithPriceList && model.partyDTO.name != this.partyWithPriceList.partyDTO.name) {
      this.dialogRef = this.dialog.open(ConfirmationDialog, {
        disableClose: false
      });
      this.dialogRef.componentInstance.confirmMessage = AppSettings.PARTY_STATUS_CONFIRMATION_MESSAGE;
      this.dialogRef.afterClosed().subscribe(result => {

        if (result) {
          this.checkPartyCode(model);
        }

      });
    } else {
      console.log(" ---------- else");
      this.checkPartyCode(model);
    }
  }

  checkPartyCode(model: PartyWithPriceList) {
    let partyNumber: string = this.partyForm.get('partyDTO.partyCode').value

    if (partyNumber && !this.isAutoNumber && partyNumber!=this.partyIdValue) {
      this.partyService.checkPartyNumber(partyNumber, model.partyDTO.partyTypeId).subscribe(response => {
        if (response.responseStatus === 0) {
          this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
          });
          this.dialogRef.componentInstance.confirmMessage = partyNumber + "-" + AppSettings.PARTY_CODE_CONFIRMATION_MESSAGE;
          this.dialogRef.afterClosed().subscribe(result => {

            if (result) {
              this.submit(model);
            }
            else {
              return;
            }

          });
        }
        else {
          this.submit(model);
        }

      })
    }

    else {
      this.submit(model);
    }
  }


  submit(model: PartyWithPriceList) {
    //let partStateId: string = this.partyForm.get('partyDTO.portStateId').value
    //console.log("--partStateId"+partStateId);
    console.log('submit')
    if (!this.partyForm.valid) {
      const controls = this.partyForm;
      let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);

      let invalidFiledLabels: string[] = [];

      invalidFieldList.forEach(field => {
       
        if (this.fieldValidatorLabels.get(field))
          invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
      })

      //console.log("invalidFiledLabels ", invalidFiledLabels)

      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
      return false;
    }
    let countryId = this.partyForm.get('partyDTO.countryId').value
    if(countryId!=1)
    {
      let partyCurrencyId: string = this.partyForm.get('partyDTO.partyCurrencyId').value
     
         if(partyCurrencyId===null)
         {
            this.alertService.danger("Please select currency");
          return;
         }
    }

    //console.log('Before clean: ', model);
    if (model.partyDTO.isIgst) {
      //console.log("Assign 1");
      model.partyDTO.isIgst = 1;
    } else {
      //console.log("Assign 0");
      model.partyDTO.isIgst = 0;
    }
    let outModel: PartyWithPriceList = this.cleanUp(model);
    outModel.materialPriceListDeletedIds = this.itemToBeRemove;
    outModel.partyBankMapDeletedIds = this.bankMapItemToBeRemove;
    //  let checkDuplicate: boolean = this.checkDuplicate(model);
    ////console.log("checkduplit return", checkDuplicate)

    this.spinStart = true;
    //console.log('Before save: ', outModel);

    this.partyService.savePartyWithPriceList(outModel)
      .subscribe(response => {
        if (!response.partyDTO) {
          this.transactionStatus = "If Party is already used in a transaction, state cannot be updated";
          this.alertService.danger(this.transactionStatus);
          this.saveStatus = false;
          this.enableForm();
          this.spinStart = false;
          this.itemToBeRemove = [];
          this.bankMapItemToBeRemove = [];
          return;
        }
        this.saveStatus = true;
        this.spinStart = false;
        //this.partyForm.get('partyDTO')['controls'].id.patchValue(response.partyDTO.id);
       this.partyIdValue =response.partyDTO.partyCode;
        this.partyForm = this.toFormGroup(response);
        this.transactionStatus = "Party " + response.partyDTO.name + AppSettings.SAVE_SUCESSFULL_MESSAGE;
        this.alertService.success(this.transactionStatus);
        this.recentPartiesLoadCounter++;
        this.disableForm();
        this.itemToBeRemove = [];
        this.bankMapItemToBeRemove = [];
       // this.isImportExport = false;
      },
        error => {
          //console.log("Error ", error);
          //console.log("Error ", error, error.exception);

          if (error instanceof HttpErrorResponse) {
            var re = /DataIntegrityViolationException/gi;
            if (error.error && error.error.exception && error.error.exception.search(re) !== -1)
              this.transactionStatus = "Duplicate Entry!! Cannot save";
          }
          else {
            this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
          }
          this.alertService.danger(this.transactionStatus);
          this.saveStatus = false;
          this.enableForm();
          this.spinStart = false;
          this.itemToBeRemove = [];
          this.bankMapItemToBeRemove = [];
        });

  }

  deleteParty(model: PartyWithPriceList, formDirective: FormGroupDirective) {

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE;

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (model.partyDTO.id) {
          this.spinStart = true;
          model.partyDTO.deleted = 'Y';
          //console.log('Before delete: ', model);

          this.partyService.deleteParty(model.partyDTO.id)
            .subscribe(response => {
              if (response.responseStatus === 1) {
                this.saveStatus = true;

                this.recentPartiesLoadCounter++;
                this.spinStart = false;
                this.transactionStatus = AppSettings.DELETE_SUCESSFULL_MESSAGE;
                this.alertService.success(this.transactionStatus);
                formDirective.resetForm();
                this.initForm();
                this.partyForm.markAsPristine();
              }
              else {
                //console.log("Error ", response);
                this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE;
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
                if (response.responseString.search('ConstraintViolationException') > 0) {
                  this.transactionStatus = "Cannot delete. Party " + model.partyDTO.name + " is already used in transactions"
                }
                this.alertService.danger(this.transactionStatus);

              }
            },
              error => {
                //console.log("Error ", error);
                this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE;
                this.alertService.danger(this.transactionStatus);
                this.saveStatus = false;
                this.spinStart = false;
              })

        }

      }
      this.dialogRef = null;
    });
  }

  edit() {
    this.oldGstNumber = this.partyForm.get('partyDTO.gstNumber').value
    this.enableForm()

  }

  disableEnableForm() {

    if (this.partyForm.disabled) {
      this.partyForm.enable();
      //     this.disableParty = false;

    } else {

      this.partyForm.disable();
      //  this.disableParty = true;

    }


  }


  clearFormNoStatusChange() {
    this.initForm();
    //   this.handleChanges();
    //  this.selectedParty = null;
  }

  closeForm() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }
  clearForm(formDirective: FormGroupDirective) {
    this.saveStatus = false;
    this.spinStart = false;
    this.transactionStatus = null;
    // this.partyForm.reset();
    this.transactionStatus = null;
    this.tempTransactionNumber = null;
    formDirective.resetForm();
    this.initForm();
    this.partyForm.markAsPristine();
    this.initPartyDTO();
    this.handleChanges();


    // this.materialPriceListDTOList= null;
  }

  addAddress() {

    const addressList: AddressList = {
      address: null,
      contactPersonName: null,
      contactPersonNumber: null,
      contactPersonDesignation: null,
      deleted: null,
    }
    const control = <FormArray>this.partyForm.controls['partyDTO'].get('addressesListDTO');
    control.push(this.mapAddress(addressList));
  }
  addMaterial() {

    //console.log("in add: ", this.materialPartNumber);

    //Build MaterialPriceList

    const materialPriceList: MaterialPriceList = {
      id: null,
      partyId: null,
      materialTypeName: this.materialType.name,
      materialName: this.materialLocal.name,
      partNumber: this.materialLocal.partNumber,
      //mrp: this.materialPartNumber.mrp,
      //price: this.materialPartNumber.price,
      materialId: this.materialLocal.id,
      discountPercentage: null,
      sellingPrice: null,
      comment:null,
      
      deleted: null,
      currentSellingPrice: this.materialLocal.price,
      currentBuyingPrice: this.materialLocal.buyingPrice
    }

    // this.selectedMaterialId.push(this.materialLocal.id)
    // //console.log("this.selectedMaterialId........................................", this.selectedMaterialId);

    //Get control

    const control = <FormArray>this.partyForm.controls['materialPriceListDTOList'];

    //Get Form group of MaterialPriceList
    //Push to control

    // //console.log("this.filteredMaterials.............",this.filteredMaterials);
    // //console.log("this.materialLocals.............",this.materialLocal);
    // this.filteredMaterials = this.filteredMaterials.filter(fm => fm.id != this.materialLocal.id);

    control.push(this.mapItem(materialPriceList));
    this.materialType = null;
    this.materialLocal = null;
    this.materialPartNumber = null;



  }

  addBankMap() {

    //console.log("in addBankMap: ");

    //Build MaterialPriceList

    const partyBankMap: PartyBankMap = {
      id: null,
      companyId: null,
      partyId: null,
      bankId: this.bankLocal.id,
      bankname: this.bankLocal.bankname,
      branch: null,
      ifsc: null,
      accountNumber: null,
      bankAdCode:null,
      openingBalance: null,
      contactNumber: null,
    }

    //Get control

    const control = <FormArray>this.partyForm.controls['partyBankMapDTOList'];

    //Get Form group of BankMap
    //Push to control

    control.push(this.bankMapItem(partyBankMap));

    this.bankLocal = null;

  }

  delete(index: number) {
    // arr.splice(i, 1);
    // this.selectedMaterialId.splice(index, 1)
    //console.log("index.........", index);


    let itemId: number = (<FormArray>this.partyForm.get('materialPriceListDTOList')).at(index).get("id").value;
    let Ids: number = (<FormArray>this.partyForm.get('materialPriceListDTOList')).at(index).get("materialId").value
    //console.log("itemId...............................", (<FormArray>this.partyForm.get('materialPriceListDTOList')).at(index).get("materialId").value);

    // for (var i = 0; i < this.selectedMaterialId.length; i++) {
    //   if (this.selectedMaterialId[i] === Ids) {
    //     this.selectedMaterialId.splice(i, 1);
    //   }
    // }



    if (itemId) {
      this.itemToBeRemove.push(itemId);
      (<FormArray>this.partyForm.get('materialPriceListDTOList')).removeAt(index);

    } else {
      (<FormArray>this.partyForm.get('materialPriceListDTOList')).removeAt(index);

    }


    return false;
  }

  deleteBankMap(index: number) {

    let itemId: number = (<FormArray>this.partyForm.get('partyBankMapDTOList')).at(index).get("id").value;

    if (itemId) {
      this.bankMapItemToBeRemove.push(itemId);
      (<FormArray>this.partyForm.get('partyBankMapDTOList')).removeAt(index);
    } else {
      (<FormArray>this.partyForm.get('partyBankMapDTOList')).removeAt(index);
    }


    return false;
  }

  deleteAddress(index: number) {
    //console.log("fvvfvffv", index);
    (<FormArray>this.partyForm.controls['partyDTO'].get('addressesListDTO')).removeAt(index);

    return false;
  }

  // setStatusBasedPermission(statusId: number) {

  //   if (statusId) {
  //     //console.log('status id: ', statusId);
  //     this.userService.getStatusBasedPermission(statusId).subscribe(response => {
  //       this.statusBasedPermission = response;
  //     })
  //   } else {
  //     //console.log("no status id: ", statusId);
  //   }
  // }

  // disableEnableForms(){
  //   this.partyForm.disable({ onlySelf: true, emitEvent: false});
  //  this.partyForm.controls['partyDTO'].get('addressesListDTO').valueChanges.subscribe({
  //     response=>{
  //       Response = this.parties;
  //     }

  //   })
  // }

  disableForm() {
    this.partyForm.disable({ onlySelf: true, emitEvent: false });

  }

  enableForm() {
    this.partyForm.enable({ onlySelf: true, emitEvent: false });

  }

  recentPartiesGet(event) {
    this.partyService.checkPartyStatus(event.partyDTO.id)
      .subscribe(response => {
        this.partyUsedInTransactons = response;
        //console.log("partyStatus :", this.partyUsedInTransactons);
      })
   

    //console.log('In recentPartiesGet', event.partyDTO.id);
    //console.log("material..........", event.materialPriceListDTOList);


    this.partyWithPriceList = event;
    let partyType: number;
    this.partyIdValue=this.partyWithPriceList.partyDTO.partyCode
    partyType = this.partyWithPriceList.partyDTO.partyTypeName === AppSettings.PARTY_TYPE_CUSTOMER ? AppSettings.PARTY_CTYPE : AppSettings.PARTY_STYPE 
   
    this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(partyType)
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(response => {
        this.isAutoNumber = response.autoNumber === 1 ? true : false;

      })

    this.partyForm = this.toFormGroup(this.partyWithPriceList);
    this.handleChanges();
    this.disableEnableForm();
    this.checkOrVerifyCurrency();

    //  this.partyForm.disable();
  }

  handleChanges() {
    // this.selectedMaterialId = [];
    this.partyForm.controls['partyDTO'].get('partyTypeId').valueChanges.subscribe(items => {
      //console.log("partyTypeId" + this.partyForm.get('partyDTO.partyTypeId').value)
      //console.log(' before isCust: ', this.isCust);
      let isCust = false;

      if (this.partyForm.get('partyDTO.partyTypeId').value === 1) {

        this.isCust = true;
      }
      else {
        this.isCust = false;
      }


      ////console.log('statusbasedperm', this.setStatusBasedPermission(this.partyForm.controls['statusId'].value));

      //this.setStatusBasedPermission(this.partyForm.controls['partyDTO'].get('statusId').value);
      //console.log(' afetr isCust: ', this.isCust);
    })

  }


  whenNoDataFound(noDataFound: boolean) {


    if (noDataFound) {
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });

      this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE;
    }
  }

  getRegstrationType() {
    // this.partyForm.controls['partyDTO'].get('gstRegistrationTypeId').valueChanges.subscribe(items => {
    //console.log("gstRegistrationTypeId" + this.partyForm.get('partyDTO.gstRegistrationTypeId').value)
    //console.log(' before isGstRegistered: ', this.isGstRegistered);
    console.log("getRegstrationType");
    let isGstRegistered = false;
    if (+this.partyForm.get('partyDTO.gstRegistrationTypeId').value === 3 && !this.isImportExport) {
      this.isGstRegistered = false;
      console.log("getRegstrationType false");
    }
    else {
      console.log("getRegstrationType true");
      this.isGstRegistered = true;
    }
    //console.log(' afetr isGstRegistered: ', this.isGstRegistered);
    // })
    console.log("before"+this.isImportExport + this.isGstRegistered);
    if (this.isGstRegistered ||this.isImportExport) {
      //console.log(' afetr isGstRegistered: false case ', this.isGstRegistered);
      this.partyForm.controls['partyDTO'].get('gstNumber').setValidators(Validators.compose([Validators.minLength(15), Validators.maxLength(15)]));
      //  this.partyForm.controls['partyDTO'].get('gstNumber').setValidators( Validators.pattern('[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}'));
      this.partyForm.controls['partyDTO'].get('gstNumber').updateValueAndValidity();
      this.partyForm.controls['partyDTO'].updateValueAndValidity();
      console.log("1"+this.isGstRegistered+"--"+this.isImportExport)

    }
   /*  else if(this.isImportExport){
      this.partyForm.controls['partyDTO'].get('gstNumber').setValidators(Validators.compose([Validators.minLength(5), Validators.maxLength(40)]));
      //  this.partyForm.controls['partyDTO'].get('gstNumber').setValidators( Validators.pattern('[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}'));
      this.partyForm.controls['partyDTO'].get('gstNumber').updateValueAndValidity();
      this.partyForm.controls['partyDTO'].updateValueAndValidity();
      console.log("2"+this.isGstRegistered+"--"+this.isImportExport)
    } */
    else {
   
      this.partyForm.controls['partyDTO'].get('gstNumber').patchValue("");
      this.partyForm.controls['partyDTO'].get('gstNumber').setValidators(null);
      this.partyForm.controls['partyDTO'].get('gstNumber').updateValueAndValidity();
      this.partyForm.controls['partyDTO'].updateValueAndValidity();
      
    }
    //console.log('this.partyForm: ', this.partyForm.valid);
    //console.log('this.partyForm: ', this.partyForm);
     //if(this.partyForm.get('partyDTO.gstRegistrationTypeId').value===4)
     //{
      //   this.isGstRegistered = false;
      //this.partyForm.controls['partyDTO'].get('gstNumber').patchValue("URP");
   //  }
    
  }

  checkEmailAvailability() {
    let email: string = this.partyForm.get('partyDTO.email').value
    //console.log("email: ", email);
    if (email && email.length > 0) {
      this.userService.checkEmailAvailability(email).subscribe(response => {

        //console.log("is email avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
        } else {
          //console.log("No");
          this.alertService.danger(AppSettings.EMAIL_UNIQUE_NUMBER_MESSAGE);
          this.partyForm.controls['partyDTO'].get('email').patchValue('');
          this.emailElement.nativeElement.focus();

        }

      });
    }

  }

  checkNumberAvailability() {
    let primaryMobile: string = this.partyForm.get('partyDTO.primaryMobile').value
    //console.log("primaryMobile: ", primaryMobile);
    if (primaryMobile && primaryMobile.length > 0) {
      this.userService.checkPhoneNumberAvailability(primaryMobile).subscribe(response => {

        //console.log("is primaryMobile avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
        } else {
          this.alertService.danger(AppSettings.PHONE_UNIQUE_NUMBER_MESSAGE);
          this.partyForm.controls['partyDTO'].get('primaryMobile').patchValue('');
          this.phoneElement.nativeElement.focus();
        }

      });

    }

  }

  onSelect(_event: any) {
    let transactionType: string;
   console.log()
    let partyType: number;
    if (_event.value === 1) {
      transactionType = AppSettings.PARTY_TYPE_CUSTOMER_CODE;
      partyType = AppSettings.PARTY_CTYPE;
    }
    else {
      transactionType = AppSettings.PARTY_TYPE_SUPPLIER_CODE;
      partyType = AppSettings.PARTY_STYPE;
    }
    this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(partyType)
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(response => {
        this.isAutoNumber = response.autoNumber === 1 ? true : false;
        if (response && this.isAutoNumber === true) {
          this.getTempTransactionNumber(transactionType)
        }
      })

  }
  protected getTempTransactionNumber(transactionName) {
    this.transactionTypeService.getTransactionNumber(transactionName)
      .pipe(
        takeUntil(this.onDestroy)
      )
      .subscribe(response => {
        this.tempTransactionNumber = response.responseString;
        console.log(response.responseString)
      })
  }

  // checkValidGST() {
  //   let gstNumber: string = this.partyForm.get('partyDTO.gstNumber').value
  //   //console.log("gstNumber: ", gstNumber);
  //   //console.log('form validity 1: ', this.partyForm.valid)
  //   if (gstNumber && gstNumber.length > 0) {

  //     this.userService.checkValidGST(gstNumber).subscribe(response => {

  //       //console.log("is gstNumber avialable? ", response);
  //       if (response.responseStatus === 1) {
  //         //console.log("Yes");
  //         this.partyForm.controls['partyDTO'].get('gstNumber').setErrors(null);
  //         this.partyForm.controls['partyDTO'].get('gstNumber').setValidators(Validators.pattern('[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}'));
  //         this.partyForm.controls['partyDTO'].get('gstNumber').updateValueAndValidity();
  //         this.partyForm.controls['partyDTO'].updateValueAndValidity();

  //       } else {
  //         //console.log("No");
  //         // this.alertService.danger(response.responseString);
  //         this.alertService.danger(AppSettings.GST_UNIQUE_NUMBER_MESSAGE);
  //         this.partyForm.controls['partyDTO'].get('gstNumber').patchValue('');
  //         this.GSTElement.nativeElement.focus();
  //         this.partyForm.controls['partyDTO'].get('gstNumber').setErrors({ status: 'incorrect' });
  //         //console.log('form validity 3: ', this.partyForm.valid)
  //         return;
  //       }

  //     });
  //   }
  // }

  helpVideos() {

    let status: string = "";
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = this.activityId.toString();


    const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {

      });




  }
  checkOrVerifyCurrency(){
    
   // this.isImportExport=false;

    if (+this.partyForm.get('partyDTO.countryId').value === 1) {
      this.isImportExport = false;
    }
    else {
      this.isImportExport = true;
    }
    this.hideIgstField();
    this.setGstRegisterType();
  }
   hideIgstField()
   {
    // this.hideIgst=false;
     console.log("hideIgstField")
     if (!this.isImportExport) {
      this.hideIgst = true;
    }
    else {
      this.hideIgst = false;
    }

   }
   setGstRegisterType(){
    if (this.isImportExport){
      this.partyForm.controls['partyDTO'].get('gstRegistrationTypeId').patchValue(3);
     // this.getRegstrationType();
     this.isGstRegistered = true;
      this.taxNumberText="CIN Number";
    }
    else
    {
      this.partyForm.controls['partyDTO'].get('gstRegistrationTypeId').patchValue(0);

      this.taxNumberText="GST Number *";
    }
   }
   setPartyType(){
     this.route.paramMap.subscribe((params:ParamMap)=>{
      this.id =+params.get('id');
       console.log("---"+this.id)
    }) 
    
   /*  this.route.params.subscribe(params =>
      {
        this.partyType = {
          id: +params['partyTypeId'],
          name: '',
          deleted: 'N'
        };
        console.log("===="+this.partyType.id)
      }); */


    


   }

}

export interface GSTRegistrationType {
  id: number,
  name: string,
}