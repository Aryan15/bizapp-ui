import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Area, AreaWrapper } from '../../../data-model/area-model';
import { City } from '../../../data-model/city-model';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../../utils/confirmation-dialog';
import { AppSettings } from '../../../app.settings';
import { LocationService } from '../../../services/location.service';
import { AlertService } from 'ngx-alerts';

import { AlertDialog } from '../../../utils/alert-dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../../data-model/activity-model';
import { UserService } from '../../../services/user.service';
import { ApplicableButtons } from '../../../data-model/misc-model';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  @ViewChild('myTable', {static: false}) table: any;

  title : String;
  subheading : String;

  areaForm: FormGroup;
  //countryWrapper: CountryWrapper = ;
  areas: Area[] = [];
  cities: City[] = [];

  selected: Area[] = [];
  tempArray: Area[]
  dialogRef: MatDialogRef<ConfirmationDialog>;
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;
  activityRoleBindings: ActivityRoleBinding[];
  alertRef: MatDialogRef<AlertDialog>; 
  applicableButtons: ApplicableButtons ={
    isApproveButton: true,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
};
enableSaveButton: boolean = false;
  constructor(private fb: FormBuilder,
    private locationService: LocationService,
    private dialog: MatDialog,
    private userService: UserService,
    private alertService: AlertService,
    public route:ActivatedRoute,
    private _router: Router, ) {
      this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
      this.title = this.userService.activityName
      this.setActivityRoleBinding();
     }
  //    setActivityRoleBinding(){
  //     this.userService.setActivityRoleBinding(this.route).subscribe(response => {
  //       this.activityRoleBindings = response;
  //   })
  // }
  setActivityRoleBinding(){
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {
       this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
      });
  })
}
  ngOnInit() {
    this.getAreas();
    this.getCities();
    this.initForm();
  }

  getAreas() {
    this.locationService.getAreas().subscribe(response => {
      this.areas = response;
      this.areaForm = this.toFormGroup({ areas: this.areas });
      this.tempArray = [...response];
    })
  }


  getCities() {
    this.locationService.getCities().subscribe(response => {
      this.cities = response;
    })
  }

  initForm() {
    let data: AreaWrapper = {
      areas: null
    };

    this.areaForm = this.toFormGroup(data);
  }

  toFormGroup(data: AreaWrapper): FormGroup {

    const areas = new FormArray([]);
    if (data.areas) {
      data.areas.forEach(c => {
        areas.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      areas: areas
    });

    return formGroup;
  }


  makeItem(a: Area): FormGroup {
    return this.fb.group({
      id: a.id,
      name: a.name,
      cityId: a.cityId,
      deleted: a.deleted
    })
  }

  submit(model: AreaWrapper) {
    //console.log('before save: ', model);
    this.locationService.createAreas(model.areas).subscribe(response => {
      //console.log("saved response: ", response);
      if (response) {
        this.areaForm = this.toFormGroup({ areas: response });
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
      }
    });
  }

  addNew() {
    const control = <FormArray>this.areaForm.controls['areas'];
    //Add one blank Item
    let item: Area = {
      id : null,
    name : null,
    cityId : null,
    deleted : null,
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length / this.pageSize);
  }

  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }



  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: Area[] = this.areaForm.get('areas').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.locationService.deleteArea(itemIdList).subscribe(response => {

            //console.log("3: ", response);
            if (response.responseStatus === 1) {
              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.areaForm = this.toFormGroup({ areas: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{

          items = items.filter(i => (i.id != null &&!(this.selected.find(s => s.name === i.name))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.areaForm = this.toFormGroup({ areas: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }


  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    ////console.log("this.tempArray: ",this.tempArray)
    // filter our data
    const temp: Area[] = this.tempArray.filter(d => d.name.toLowerCase().indexOf(val) != -1 || !val)

    // update the rows
    this.areaForm = this.toFormGroup({ areas: temp });

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;


  }

  checkDuplicate(event) {
    const val = event.target.value.toLowerCase();
    //console.log(val);
     if (this.tempArray.find(area=>area.name.toLowerCase()===val))
     {
       //console.log("No");
  
       this.alertRef = this.dialog.open(AlertDialog, {
         disableClose: false
       });
  
       this.alertRef.componentInstance.alertMessage = AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE;
       this.alertRef.afterClosed().subscribe(result => {
         event.target.value="";
       })
  
     }
  
  
   }
  close()
  {
    this._router.navigate(['/'])
  }

  
}
