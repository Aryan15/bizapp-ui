import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { CountryWrapper, Country } from '../../../data-model/country-model';
import { LocationService } from '../../../services/location.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../../utils/confirmation-dialog';
import { AppSettings } from '../../../app.settings';
import { AlertService } from 'ngx-alerts';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertDialog } from '../../../utils/alert-dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../../data-model/activity-model';
import { UserService } from '../../../services/user.service';
import { ApplicableButtons } from '../../../data-model/misc-model';


@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
})
export class CountryComponent implements OnInit {

  @ViewChild(DatatableComponent, {static: false}) table: DatatableComponent;
  
  title : String;
  subheading : String;

  countryForm: FormGroup;
  //countryWrapper: CountryWrapper = ;
  countries: Country[] = [];
  selected = [];
  tempArray: Country[]
  dialogRef: MatDialogRef<ConfirmationDialog>;
  
  alertRef: MatDialogRef<AlertDialog>;
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;

  activityRoleBindings: ActivityRoleBinding[];
  
  applicableButtons: ApplicableButtons ={
    isApproveButton: true,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
};
enableSaveButton: boolean = false;
  constructor(private fb: FormBuilder,
    private locationService: LocationService,
    private dialog: MatDialog, 
    private userService: UserService,
    public route:ActivatedRoute,
    private alertService: AlertService,
    private _router: Router,) {
      this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
      this.title = this.userService.activityName
        this.setActivityRoleBinding();
    }

  //    setActivityRoleBinding(){
  //     this.userService.setActivityRoleBinding(this.route).subscribe(response => {
  //       this.activityRoleBindings = response;
  //   })
  // }
  setActivityRoleBinding(){
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {
       this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
      });
  })
}

  ngOnInit() {
    this.getCountries();
    this.initForm();
  }

  getCountries() {
    this.locationService.getCountries().subscribe(response => {
      this.countries = response;
      //console.log('got countries: ', this.countries);
      this.countryForm = this.toFormGroup({ countries: this.countries });
      this.tempArray = [...response];
    })
  }

  initForm() {
    let data: CountryWrapper = {
      countries: null
    };

    this.countryForm = this.toFormGroup(data);
  }

  toFormGroup(data: CountryWrapper): FormGroup {

    const countries = new FormArray([]);
    if (data.countries) {
      data.countries.forEach(c => {
        countries.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      countries: countries
    });

    return formGroup;
  }


  submit(model: CountryWrapper) {
    //console.log('before save: ', model);
    this.locationService.createCountries(model.countries).subscribe(response => {
      if(response){
        this.countryForm = this.toFormGroup({countries: response});
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
      }
    });
  }

  makeItem(c: Country): FormGroup {
    return this.fb.group({
      id: c.id,
      name: [c.name, Validators.required],
      deleted: c.deleted
    })
  }
  addNew() {
    const control = <FormArray>this.countryForm.controls['countries'];
    //Add one blank Item
    let item: Country = {
      id: null,
      name: null,
      deleted: null,
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length/this.pageSize);
  }

  displayCheck(row) {
    return row.name !== 'India';
  }

  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // delete() {
  //   ////console.log(this.selected);

  //   this.dialogRef = this.dialog.open(ConfirmationDialog, {
  //     disableClose: false
  //   });

  //   this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

  //   this.dialogRef.afterClosed().subscribe(result => {
  //     if (result) {

  //       let items: Country[] = this.countryForm.get('countries').value;

  //       items.forEach((item, idx) => {
          
  //         if (this.selected.find(s => s.id === item.id)) {
  //           if(item.id){
  //             this.locationService.deleteCountry(item.id).subscribe(response => {
  //               if(response.responseStatus === 1){
  //                 (<FormArray>this.countryForm.get('countries')).removeAt(idx);
  //                 this.tempArray.splice(idx,1);
  //                 this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
  //               }else{
  //                 this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
  //               }
                
  //             })
  //           }
  //           else{
  //             (<FormArray>this.countryForm.get('countries')).removeAt(idx);
  //                 this.tempArray.splice(idx,1);
  //                 this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
  //           }
            
            
  //         }

  //       });

  //       this.selected = [];

  //     }

  //   });



  // }


  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: Country[] = this.countryForm.get('countries').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.locationService.deleteCountry(itemIdList).subscribe(response => {

            //console.log("3: ", response);
            if (response.responseStatus === 1) {
              // items.forEach((item, idx) => {

              //   if (this.selected.find(s => s.id === item.id)) {
              //     (<FormArray>this.stateForm.get('states')).removeAt(idx);
              //         this.tempArray.splice(idx,1);

              //   }

              // })
              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.countryForm = this.toFormGroup({ countries: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{
          // items.forEach((item, idx) => {
          //   if(!item.id)
          //     (<FormArray>this.stateForm.get('states')).removeAt(idx)
          // })
          items = items.filter(i => (i.id != null &&!(this.selected.find(s => s.name === i.name))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.countryForm = this.toFormGroup({ countries: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }


  updateFilter(event){

    const val = event.target.value.toLowerCase();

    ////console.log("this.tempArray: ",this.tempArray)
    // filter our data
    const temp: Country[] = this.tempArray.filter(d => d.name.toLowerCase().indexOf(val) != -1 || !val)

    // update the rows
    this.countryForm = this.toFormGroup({ countries: temp });

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;


  }

  checkDuplicate(event) {
    const val = event.target.value.toLowerCase();
    //console.log(val);
     if (this.tempArray.find(area=>area.name.toLowerCase()===val))
     {
       //console.log("No");
  
       this.alertRef = this.dialog.open(AlertDialog, {
         disableClose: false
       });
  
       this.alertRef.componentInstance.alertMessage = AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE;
       this.alertRef.afterClosed().subscribe(result => {
         event.target.value="";
       })
  
     }
  
  
   }

  close()
  {
    this._router.navigate(['/'])
  }
}
