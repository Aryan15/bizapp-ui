import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { State, StateWrapper } from '../../../data-model/state-model';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../../utils/confirmation-dialog';
import { LocationService } from '../../../services/location.service';
import { AlertService } from 'ngx-alerts';
import { AppSettings } from '../../../app.settings';
import { Country } from '../../../data-model/country-model';
import { AlertDialog } from '../../../utils/alert-dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../../data-model/activity-model';
import { UserService } from '../../../services/user.service';
import { ApplicableButtons } from '../../../data-model/misc-model';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss'],
})
export class StateComponent implements OnInit {

  @ViewChild('myTable', {static: false}) table: any;

  title : String;
  subheading : String;

  stateForm: FormGroup;
  //countryWrapper: CountryWrapper = ;
  states: State[] = [];
  countries: Country[] = [];
  selected: State[] = [];
  tempArray: State[]
  dialogRef: MatDialogRef<ConfirmationDialog>;
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;

  alertRef: MatDialogRef<AlertDialog>;
  activityRoleBindings: ActivityRoleBinding[];
  
  applicableButtons: ApplicableButtons ={
    isApproveButton: false,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: false,
    isEditButton: false,
    isDraftButton: false,
};
  constructor(private fb: FormBuilder,
    private locationService: LocationService,
    private dialog: MatDialog,
    private userService: UserService,
    public route:ActivatedRoute,
    private alertService: AlertService, 
    private _router: Router,) {
      this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName;
      this.title = this.userService.activityName
        this.setActivityRoleBinding();
    }
    setActivityRoleBinding(){
     this.userService.setActivityRoleBinding(this.route).subscribe(response => {
       this.activityRoleBindings = response;
   })
  }
  ngOnInit() {

    this.getStates();
    this.getCountries();
    this.initForm();
   
  }

  getStates() {
    this.locationService.getStates().subscribe(response => {
      this.states = response;
      //console.log('got states: ', this.states);
      this.stateForm = this.toFormGroup({ states: this.states });
      this.stateForm.disable();
      this.tempArray = [...response];
    })
  }

  getCountries() {
    this.locationService.getCountries().subscribe(response => {
      this.countries = response;
    })
  }

  initForm() {
    let data: StateWrapper = {
      states: null
    };

    this.stateForm = this.toFormGroup(data);
   
  }

  toFormGroup(data: StateWrapper): FormGroup {

    const states = new FormArray([]);
    if (data.states) {
      data.states.forEach(c => {
        states.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      states: states
    });

    return formGroup;
  }

  submit(model: StateWrapper) {
    //console.log('before save: ', model);
    this.locationService.createStates(model.states).subscribe(response => {
      //console.log("saved response: ", response);
      if (response) {
        this.stateForm = this.toFormGroup({ states: response });
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
      }
    });
  }

  makeItem(s: State): FormGroup {
    return this.fb.group({
      id: s.id,
      name: [s.name, Validators.required],
      stateCode: [s.stateCode, Validators.required],
      countryId: [s.countryId, Validators.required],
      citiesList: null,
      deleted: s.deleted
    })
  }

  addNew() {
    const control = <FormArray>this.stateForm.controls['states'];
    //Add one blank Item
    let item: State = {
      id: null,
      name: null,
      stateCode: null,
      countryId: null,
      citiesList: null,
      deleted: null
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length / this.pageSize);
  }

  displayCheck(row) {
    return row.name !== 'Karnataka';
  }


  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }


  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: State[] = this.stateForm.get('states').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.locationService.deleteState(itemIdList).subscribe(response => {

            //console.log("3: ", response);
            if (response.responseStatus === 1) {
              // items.forEach((item, idx) => {

              //   if (this.selected.find(s => s.id === item.id)) {
              //     (<FormArray>this.stateForm.get('states')).removeAt(idx);
              //         this.tempArray.splice(idx,1);

              //   }

              // })
              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.stateForm = this.toFormGroup({ states: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{
          // items.forEach((item, idx) => {
          //   if(!item.id)
          //     (<FormArray>this.stateForm.get('states')).removeAt(idx)
          // })
          items = items.filter(i => (i.id != null &&!(this.selected.find(s => s.name === i.name))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.stateForm = this.toFormGroup({ states: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }


  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    ////console.log("this.tempArray: ",this.tempArray)
    // filter our data
    const temp: State[] = this.tempArray.filter(d => d.name.toLowerCase().indexOf(val) != -1 || !val)

    // update the rows
    this.stateForm = this.toFormGroup({ states: temp });

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;


  }

  checkDuplicate(event) {
    const val = event.target.value.toLowerCase();
    //console.log(val);
     if (this.tempArray.find(area=>area.name.toLowerCase()===val))
     {
       //console.log("No");
  
       this.alertRef = this.dialog.open(AlertDialog, {
         disableClose: false
       });
  
       this.alertRef.componentInstance.alertMessage = AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE;
       this.alertRef.afterClosed().subscribe(result => {
         event.target.value="";
       })
  
     }
  
  
   }

  close()
  {
    this._router.navigate(['/'])
  }
}
