import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { Process } from '../../data-model/material-model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { ProcessWrapper } from '../../data-model/process-model';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { ProcessService } from '../../services/process.service';
import { HelpVideosComponent } from '../../help-videos/help-videos.component';

@Component({
  selector: 'app-process-master',
  templateUrl: './process-master.component.html',
  styleUrls: ['./process-master.component.scss']
})
export class ProcessMasterComponent implements OnInit {

  processForm: FormGroup;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  public process: Process[] = [];
  processWrapper: ProcessWrapper[] = [];
  transactionStatus: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  activityId:number=0;
  constructor(private fb: FormBuilder
    , public dialog: MatDialog,
    private _router: Router,
    public userService: UserService,
    public route: ActivatedRoute,
    private processService: ProcessService,
    private alertService: AlertService) {
    this.setActivityRoleBinding();
  }

  applicableButtons: ApplicableButtons ={
    isApproveButton: false,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: false,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
};
  activityRoleBindings: ActivityRoleBinding[];
  ngOnInit() {
    this.initForm();
    this.getAllProcess();
  }

  private initForm() {
    // let itemData: PrintCopies = {
    //   id: null,
    //   name: null,
    //   deleted: null
    // }
    let data: ProcessWrapper = {
      processList: null,
    }
    this.processForm = this.toFormGroup(data);
  }

  private makeProcesses(processes: Process): FormGroup {

    return this.fb.group({
      id: [processes.id],
      deleted: [processes.deleted],
      name: [processes.name],
      description: [processes.description]
    });
  }

  private toFormGroup(data: ProcessWrapper): FormGroup {
    const processArray = new FormArray([]);
    if (data.processList) {
      data.processList.forEach(item => {
        processArray.push(this.makeProcesses(item));
      })
    }
    //console.log("printCopiesArray", processArray)
    const formGroup = this.fb.group({
      processList: processArray,
    });
    //console.log("formGroup", formGroup)
    return formGroup;
  }

  private getAllProcess() {

    this.processService.getAllProcess()
      .subscribe(response => {
        this.process = response;
        //console.log("process :", this.process)


        const control = <FormArray>this.processForm.controls['processList'];



        this.process.forEach(item => {
          //console.log("item :", item);
          control.push(this.makeProcesses(item));
        })
        this.add();
      })
  }

  private add() {
    //console.log("add.......print");

    const control = <FormArray>this.processForm.controls['processList'];
    //console.log("control.....", control);
    //Add one blank Item
    let data: Process = {
      id: null,
      deleted: null,
      name: null,
      description: null,
    };

    //console.log("data.....", data);
    control.push(this.makeProcesses(data));
  }

  delete(idx: number) {
    //console.log("idx :", idx);
    const control = <FormArray>this.processForm.controls['processList'];
    let id: number = control.at(idx).get('id').value;
    //console.log("id :", id);

    if (id == null) {
      (<FormArray>this.processForm.get('processList')).removeAt(idx);
      return;
    }

    else {
      this.processService.delete(id)
        .subscribe(response => {
          //console.log('Deleted: ', response);
          if (response.responseStatus === 1) {
            (<FormArray>this.processForm.get('processList')).removeAt(idx);
            this.alertService.success("Print copy deleted");
          }
          else {
            //console.log('Cannot delete!!');
            this.alertService.danger('Cannot delete already used ');
          }
        },
          error => {
            //console.log('Error: ', error);
            this.alertService.danger('Cannot delete!!');
          })

      return false;
    }
  }

  private cleanUp(model: ProcessWrapper): ProcessWrapper {
    // Strip out null rows which are added at the end just for ease of user entry purposes.
    //let outModel : SettingsWrapper = <SettingsWrapper>{};

    //outModel.globalSetting = model.globalSetting;

    model.processList.forEach(pr => {
      if (pr.name === null) {
        model.processList.pop();
      }
    });
    return model;

  }

  onSubmit(model: ProcessWrapper) {
    //console.log('saving: ', model);
    // let outModel: PrintCopiesWrapper = this.cleanUp(model);
    this.spinStart = true;
    model = this.cleanUp(model);
    this.processService.save(model)
      .subscribe(response => {
        // console.log("response", response);
        this.transactionStatus = AppSettings.SAVE_SUCESSFULL_MESSAGE;
        this.alertService.success(this.transactionStatus);
        let returnValue : ProcessWrapper = {
          processList: response
        }
        this.processForm = this.toFormGroup(returnValue);
        this.spinStart = false;   
        this.saveStatus = true;     
      },
        error => {
          console.error(error);
          this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
          this.alertService.danger(this.transactionStatus);
          this.spinStart = false;
          this.saveStatus = false;
        })
  }

  setActivityRoleBinding(){
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;
      this.activityRoleBindings.forEach(activityRoleBinding => {
        this.activityId=activityRoleBinding.activityId;

      });
  })
}

closeForm() {
  this._router.navigate(['/']);
}

helpVideos() {
        
  let status: string = "";
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.id =this.activityId.toString();

  
  const dialogRef = this.dialog.open(HelpVideosComponent, dialogConfig);
  dialogRef.afterClosed().subscribe(
      data => {

      });



  
}



}
