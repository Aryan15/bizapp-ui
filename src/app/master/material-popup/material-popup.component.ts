import { Component, OnInit, Inject, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AppSettings } from '../../app.settings';
import { MaterialType } from '../../data-model/material-type-model';
import { Uom } from '../../data-model/uom-model';
import { Tax } from '../../data-model/tax-model';
import { Material, SupplyType } from '../../data-model/material-model';
import { MaterialService } from '../../services/material.service';
import { UomService } from '../../services/uom.service';
import { TaxService } from '../../services/tax.service';
import { MaterialTypeService } from '../../services/material-type.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UtilityService } from '../../utils/utility-service';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertService } from 'ngx-alerts';
@Component({
  selector: 'app-material-popup',
  templateUrl: './material-popup.component.html',
  styleUrls: ['./material-popup.component.scss']
})
export class MaterialPopupComponent implements OnInit {
  public materialPopUpForm: FormGroup;
  public material: Material;
  public materialTypes: MaterialType[] = [];
  public uoms: Uom[] = [];
  public taxes: Tax[] = [];
  isPartNumberVerified: boolean = false;

  public supplyType: SupplyType;




  currentUser: any;
  themeClass: any;

  transactionStatus: string = null;
  isMaterial: boolean = true;
  formHeader: string = "";
  mdialogRef: MatDialogRef<ConfirmationDialog>;


  constructor(private fb: FormBuilder,
    private materialService: MaterialService,
    private materialTypeService: MaterialTypeService,
    private uomService: UomService,
    private taxService: TaxService,
    private utilityService: UtilityService,
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<MaterialPopupComponent>,
    private alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

   this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
   this.themeClass = this.currentUser.userThemeName;
    //console.log('in material popup cons');
  }

  ngOnInit() {
    // this.setSupplyType();
    //console.log('in material popup init');
    this.getMaterialTypes();

    this.getUoms();
    this.getTaxes();
    this.initForm();
    //console.log("this.supplyTypeControl", this.data);

  }

  getMaterialTypes() {
    this.materialTypeService.getAllMaterialTypes()
      .subscribe(response => {
        if(this.data.materialTypeId){          
          this.materialTypes = response.filter(r => r.id === this.data.materialTypeId);
        }else{
          this.materialTypes = response.filter(r => r.id !=4);
        }
        
      })
  }
  getUoms() {
    this.uomService.getAllUom().subscribe(
      response => {
        this.uoms = response;
      }
    )
  }

  getTaxes() {
    this.taxService.getAllTaxes().subscribe(
      response => {
        this.taxes = response;
      }
    )
  }

  private initForm() {
    let data: Material = {
      id: null,
      deleted: null,
      name: null,
      partNumber: null,
      supplyTypeId: null,
      materialTypeId: null,
      unitOfMeasurementId: null,
      unitOfMeasurementName: null,
      price: null,
      specification: null,
      stock: null,
      companyId: null,
      buyingPrice: null,
      taxId: null,
      mrp: null,
      discountPercentage: null,
      partyId: null,
      hsnCode: null,
      cessPercentage: null,
      openingStock: null,
      minimumStock :null,
      isContainer: null,
      outName: null,
      outPartNumber: null,
      materialTypeName: null
    };

    this.materialPopUpForm = this.toFormGroup(data);
    this.materialPopUpForm.controls['supplyTypeId'].patchValue(this.data.supplyTypeId);
    this.CheckUpdate();
  }


  private toFormGroup(data: Material): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      deleted: [data.deleted],
      name: [data.name, [Validators.required]],
      partNumber: [data.partNumber],
      supplyTypeId: [data.supplyTypeId],
      materialTypeId: [data.materialTypeId, [Validators.required]],
      unitOfMeasurementId: [data.unitOfMeasurementId, [Validators.required]],
      unitOfMeasurementName: [data.unitOfMeasurementName],
      stock: [data.stock],
      taxId: [data.taxId, [Validators.required]],
      companyId: [data.companyId],
      hsnCode: [data.hsnCode],
      price: [data.price],
      openingStock: [data.openingStock],
      isContainer: [data.isContainer],
      outName: [data.outName],
      outPartNumber: [data.outPartNumber],
      mrp: [data.mrp],
      buyingPrice: [data.buyingPrice],
      materialTypeName: [data.materialTypeName]
    });

    return formGroup;

  }

  CheckUpdate() {
    //console.log("this.materialPopUpForm.controls['supplyTypeId'].value" + this.materialPopUpForm.controls['supplyTypeId'].value)
    let val: number = 0;

    val = this.materialPopUpForm.controls['supplyTypeId'].value;

   
    if (val === 1) {
      this.formHeader = "Material";
     this.isMaterial = true;
    } else {
      this.formHeader = "Service";
      this.isMaterial = false;
    }
    if (!this.isMaterial) {

      this.materialPopUpForm.get('materialTypeId').clearValidators;
      this.materialPopUpForm.get('materialTypeId').setErrors(null);
      this.materialPopUpForm.get('unitOfMeasurementId').clearValidators;
      this.materialPopUpForm.get('unitOfMeasurementId').setErrors(null);
      this.materialPopUpForm.get('taxId').clearValidators;
      this.materialPopUpForm.get('taxId').setErrors(null);

      this.materialPopUpForm.updateValueAndValidity();
    }



  }

  saveWrapper(){

    let material: Material = this.materialPopUpForm.value;

    if(!this.isPartNumberVerified && this.materialPopUpForm.get('partNumber').value ){
      this.alertService.danger(AppSettings.PART_NUMBER_UNIQUE_NUMBER_MESSAGE);
      this.materialPopUpForm.get("partNumber").patchValue("");
      this.materialPopUpForm.get("partNumber").setErrors({ 'duplicate': true })
      return false;
    }

    this.materialService.checkMaterialAvailability(material.name,material.id)
    .subscribe(response => {

      if(response && response.length > 0){
        //call confirmation
        this.mdialogRef = this.dialog.open(ConfirmationDialog, {
          disableClose: false
        });

        this.mdialogRef.componentInstance.confirmMessage = AppSettings.DUPLICATE_MATERIAL_CONFIRMATION_MESSAGE

        this.mdialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.save();
            }
            
        });
      }else{
        this.save();
      }

    })
  }

  checkPartNumberAvailability() {
    let partNumber: string = this.materialPopUpForm.get('partNumber').value
    //console.log("partNumber: ", partNumber);
    if (partNumber && partNumber.length > 0) {
      this.materialService.checkPartNumberAvailability(partNumber).subscribe(response => {

        //console.log("is partNumber avialable? ", response);
        if (response.responseStatus === 1) {
          //console.log("Yes");
          this.isPartNumberVerified = true;
        } else {
          //console.log("No");
          this.isPartNumberVerified = false;
          this.alertService.danger(AppSettings.PART_NUMBER_UNIQUE_NUMBER_MESSAGE);
            // this.materialForm.controls['partNumber'].patchValue('');
          //this.partNumberElement.nativeElement.focus();
    
        }

      });
    }

  }

  save() {

    //console.log('in material popup save click' + this.materialPopUpForm.value.supplyTypeId);

    this.dialogRef.close(this.materialPopUpForm.value);
  }

  close() {
    //console.log('in material popup close click');
    this.dialogRef.close();
  }

  updateStock(){
    this.materialPopUpForm.get('stock').patchValue(this.materialPopUpForm.get('openingStock').value); 
  }


}
