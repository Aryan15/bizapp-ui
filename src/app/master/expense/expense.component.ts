import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { LocationService } from '../../services/location.service';
import { VoucherService } from '../../services/voucher.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertDialog } from '../../utils/alert-dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { UserService } from '../../services/user.service';
import { ApplicableButtons } from '../../data-model/misc-model';
import { Expense, ExpenseWrapper } from '../../data-model/voucher-model';


@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {

  @ViewChild(DatatableComponent, {static: false}) table: DatatableComponent;
  
  transactionStatus: string = null;
  expenseForm: FormGroup;
  //countryWrapper: CountryWrapper = ;
  expenses: Expense[] = [];
  selected = [];
  tempArray: Expense[]
  dialogRef: MatDialogRef<ConfirmationDialog>;
  saveStatus:boolean=false;

  alertRef: MatDialogRef<AlertDialog>;
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;

  activityRoleBindings: ActivityRoleBinding[];
  
  applicableButtons: ApplicableButtons ={
    isApproveButton: true,
    isClearButton: false,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: false,
    isDraftButton: false,
};
enableSaveButton: boolean = false;
  constructor(private fb: FormBuilder,
    private locationService: LocationService,
    private dialog: MatDialog, 
    private userService: UserService,
    public route:ActivatedRoute,
    private alertService: AlertService,
    private _router: Router,
    private voucherService :VoucherService) {

        this.setActivityRoleBinding();
    }
    setActivityRoleBinding(){
      this.userService.setActivityRoleBinding(this.route).subscribe(response => {
        this.activityRoleBindings = response;
  
        this.activityRoleBindings.forEach(activityRoleBinding => {
         this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
        });
    })
  }
  ngOnInit() {
    this.getAllExpense();
    this.initForm();
  }

  getAllExpense() {
    this.voucherService.getAllExpense().subscribe(response => {
      this.expenses = response;
      //console.log('got expenses: ', this.expenses);
      this.expenseForm = this.toFormGroup({ expenses: this.expenses });
      this.tempArray = [...response];
    })
  }

  initForm() {
    let data: ExpenseWrapper= {
      expenses: null
    };

    this.expenseForm = this.toFormGroup(data);
  }

  toFormGroup(data: ExpenseWrapper): FormGroup {

    const expenses = new FormArray([]);
    if (data.expenses) {
      data.expenses.forEach(c => {
        expenses.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      expenses: expenses
    });

    return formGroup;
  }


  submitWrapper(model: ExpenseWrapper){

    var modifiedExpenseIds : number[] = []; 
    // //console.log('before save: ', model.expenses);
    // //console.log("this.tempArray...........>>>",this.tempArray);

    model.expenses.forEach(obj =>{
      for(var i=0; i<this.tempArray.length; i++){
        if(obj.id===this.tempArray[i].id && obj.name != this.tempArray[i].name){
          modifiedExpenseIds.push(this.tempArray[i].id)
        }
      }
    })
    
    //console.log("modifiedExpenseIds :",modifiedExpenseIds.length);
    if(modifiedExpenseIds.length!=0){
      this.voucherService.checkExpenseStatus(modifiedExpenseIds)
      .subscribe(response =>{
        //console.log("response :" ,response);
        if(response){
          this.dialogRef = this.dialog.open(ConfirmationDialog, {
            disableClose: false
          });
          this.dialogRef.componentInstance.confirmMessage = AppSettings.EXPENSE_CONFIRMATION_MESSAGE;
          this.dialogRef.afterClosed().subscribe(result => {
    
            if (result) {
              this.onSubmit(model);
            }
    
          });
        }
        else{
          this.onSubmit(model);
        }
        
      })
    }
    else{
      this.onSubmit(model);
    }

  }

  onSubmit(model: ExpenseWrapper) {

    this.voucherService.saveExpense(model.expenses).subscribe(response => {
      if(response){
        this.expenseForm = this.toFormGroup({expenses: response});
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
      }
    });
  }

  makeItem(c: Expense): FormGroup {
    return this.fb.group({
      id: c.id,
      name: [c.name, Validators.required],
      deleted: c.deleted
    })
  }
  addNew() {
    const control = <FormArray>this.expenseForm.controls['expenses'];
    //Add one blank Item
    let item: Expense = {
      id: null,
      name: null,
      deleted: null,
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length/this.pageSize);
  }

  displayCheck(row) {
    return row.name !== 'India';
  }

  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    
  }



  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: true
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: Expense[] = this.expenseForm.get('expenses').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.voucherService.deleteExpense(itemIdList).subscribe(response => {

            //console.log("3aaaaa: ", response);
            if (response.responseStatus === 1) {
              
              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.expenseForm = this.toFormGroup({ expenses: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              if (response.responseString.search('ConstraintViolationException') > 0) {
                this.transactionStatus = "Cannot delete it is already used in transactions"
              }
              this.alertService.danger(this.transactionStatus);
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{
          items = items.filter(i => (!(this.selected.find(s => s.name === i.name))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.expenseForm = this.toFormGroup({ expenses: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }


  updateFilter(event){

    const val = event.target.value.toLowerCase();
    // filter our data
    const temp: Expense[] = this.tempArray.filter(d => d.name.toLowerCase().indexOf(val) != -1 || !val)

    // update the rows
    this.expenseForm = this.toFormGroup({ expenses: temp });

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;


  }

  checkDuplicate(event) {
    
    const val = event.target.value.toLowerCase();
    //console.log(val);
     if (this.tempArray.find(area=>area.name.toLowerCase()===val))
     {
       //console.log("No");
  
       this.alertRef = this.dialog.open(AlertDialog, {
         disableClose: false
       });
  
       this.alertRef.componentInstance.alertMessage = AppSettings.TRNSACTION_NUMBER_UNIQUE_NUMBER_MESSAGE;
       this.alertRef.afterClosed().subscribe(result => {
         event.target.value="";
       })
  
     }
  
  
   }

  close()
  {
    this._router.navigate(['/'])
  }

}
