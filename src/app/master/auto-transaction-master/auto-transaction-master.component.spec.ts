import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoTransactionMasterComponent } from './auto-transaction-master.component';

describe('AutoTransactionMasterComponent', () => {
  let component: AutoTransactionMasterComponent;
  let fixture: ComponentFixture<AutoTransactionMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoTransactionMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoTransactionMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
