import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentAutoTransactionComponent } from './recent-auto-transaction.component';

describe('RecentAutoTransactionComponent', () => {
  let component: RecentAutoTransactionComponent;
  let fixture: ComponentFixture<RecentAutoTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentAutoTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentAutoTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
