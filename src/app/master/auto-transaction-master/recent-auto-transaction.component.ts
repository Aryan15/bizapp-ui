import { Component, OnInit, OnChanges, Input, Output, ViewChild,EventEmitter } from '@angular/core';


import {Observable, merge, of as observableOf} from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, startWith, switchMap, catchError, map } from 'rxjs/operators';
import { InvoiceService } from '../../services/invoice.service';
import { AutoTransactionGeneration, RecentAutoTransaction } from '../../data-model/autotransaction-popup-model';
const PAGE_SIZE: number = 10;
@Component({
  selector: 'app-recent-auto-transaction',
  templateUrl: './recent-auto-transaction.component.html',
  styleUrls: ['./recent-auto-transaction.component.scss']
})
export class RecentAutoTransactionComponent  implements OnInit, OnChanges {

  @Input()
  recentAutoTransactionLoadCounter: number;

  @Output()
  autoTransactionModel: EventEmitter<AutoTransactionGeneration> = new EventEmitter<AutoTransactionGeneration>();

    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    displayedColumns = ['invoiceNumber','partyName','startDate', 'countOfInvoice'];
    autoTransactionDatabase: AutoTransactionHttpDao | null; 

    dataSource = new MatTableDataSource<AutoTransactionGeneration>();

    filterForm = new FormGroup ({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
  
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private invoiceService: InvoiceService) { }

  ngOnInit() {
    this.displayedColumns = ['invoiceNumber','partyName','startDate', 'countOfInvoice'];
    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
    
    this.autoTransactionDatabase = new AutoTransactionHttpDao(this.invoiceService);

 

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.autoTransactionDatabase!.getRecentTransaction(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished 
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        //console.log('data.Materials: ', data.materials);
        //console.log('data.totalCount: ', data.totalCount);
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.autoTransactionGenerationDTOS;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        //console.log('catchError error');
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);


  }


  ngOnChanges(){
    if (this.recentAutoTransactionLoadCounter > 0) {
      this.autoTransactionDatabase = new AutoTransactionHttpDao(this.invoiceService);

      this.autoTransactionDatabase!.getRecentTransaction( this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, (this.paginator.pageIndex - 1), this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.materials);
          //console.log('data.totalCount: ', data.totalCount);
          if (this.filterForm.get('filterText').value != null) {
            if (data.totalCount === 0) {
              this.noDataFound.emit(true);
            }
          }
          return data.autoTransactionGenerationDTOS;
        })).subscribe(data => this.dataSource.data = data);
    }
   

    
  }









  onRowClick(row: any){
   
    this.autoTransactionModel.emit(row);
   
}

}


export class AutoTransactionHttpDao{
    
  constructor(private invoiceService: InvoiceService) { }

  getRecentTransaction(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentAutoTransaction>{

    pageSize = pageSize ? pageSize : PAGE_SIZE;
  console.log(this.invoiceService.getRecentTransaction(filterText, sortColumn, sortDirection, page, pageSize));

    return this.invoiceService.getRecentTransaction(filterText, sortColumn, sortDirection, page, pageSize);
  }
}