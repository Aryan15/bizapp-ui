import { Component, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormGroupDirective, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material/dialog";
import { AutoTransactionGeneration, AutoTransactionGenerationList } from '../../data-model/autotransaction-popup-model';
import { FinancialYear } from '../../data-model/financial-year-model';
import { ApplicableButtons } from '../../data-model/misc-model';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { UserService } from '../../services/user.service';
import { InvoiceHeader } from '../../data-model/invoice-model';
import { InvoiceService } from '../../services/invoice.service';
import { AlertService } from 'ngx-alerts';
import { Subject, Observable } from 'rxjs';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { UtilityService } from '../../utils/utility-service';
import { FinancialYearService } from '../../services/financial-year.service';
import { DatePipe } from '@angular/common';
import { Party } from '../../data-model/party-model';
import { startWith, debounceTime, map, filter, switchMap } from 'rxjs/operators';
import { PartyService } from '../../services/party.service';
@Component({
  selector: 'app-auto-transaction-master',
  templateUrl: './auto-transaction-master.component.html',
  styleUrls: ['./auto-transaction-master.component.scss']
})
export class AutoTransactionMasterComponent implements OnInit, OnDestroy {
  public autoTransactionForm: FormGroup;
  public autoTransactionModel: AutoTransactionGeneration;
  currentUser: any;
  date: Date;
  themeClass: any;
  errorMessage: string = null;
  defaultDate: string;
  financialYear: FinancialYear;
  financialMinDate: Date;
  financialMaxDate: Date;
  lastDate:String="";
  datePipe = new DatePipe('en-US');
  saveStatus: boolean = false;
  spinStart: boolean = false;
  recentAutoTransactionStop: boolean = false;
  disables: boolean = false;
  disablesInvoice: boolean = false;
  disablePeriod: boolean = false;
  allowReadOnly:boolean =false;
  invoiceNumbervalue: string;
  periodValue:string;
  daysValue:number;
  weekValue:string;
  formHeader: string;
  recentAutoTransactionLoadCounter: number = 0;
  transactionStatus: string = null;
  tempPartyName:string =null;
  public sourceInvoices: InvoiceHeader[] = [];
  public partys:Party[]=[];
  itemToBeRemove: string[] = [];
  onDestroy: Subject<boolean> = new Subject();
  dialogRefs: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;
  fieldValidatorLabels = new Map<string, string>();
  allowEmail: boolean = false;
  weeks = true;
  dateToGenerate = true;
  invoiceCount: number;
  days: string;
  period: string;
  dates: string;
  week: string;
  tags: boolean = true;
  disableStop: boolean = false;
  allowPreview: boolean = true;
  allowSave: boolean = false;
  partyFormControl: AbstractControl;
  filteredParties: Observable<Party[]>;
  selectedParty: Party;
  readonlyParty:boolean = false;
  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: true,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: true,
    isDraftButton: false,
  };
  activityRoleBindings: ActivityRoleBinding[];

  public periodToCreateTransactions: periodToCreateTransaction[] = [
    { id: 1, name: "Monthly" },
    { id: 2, name: "Quarterly" },
    { id: 3, name: "Weekly" },
    { id: 4, name: "Day" },
  ]


  public daysOfWeeks: daysOfWeek[] = [
    { id: 1, name: "Sunday" },
    { id: 2, name: "Monday" },
    { id: 3, name: "Tuesday" },
    { id: 4, name: "Wednesday" },
    { id: 5, name: "Thursday" },
    { id: 6, name: "Friday" },
    { id: 7, name: "Saturday" },
  ]



  public dateToGenerateInvoices: dateToGenerateInvoice[] = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
    { id: 8 },
    { id: 9 },
    { id: 10 },
    { id: 11 },
    { id: 12 },
    { id: 13 },
    { id: 14 },
    { id: 15 },
    { id: 16 },
    { id: 17 },
    { id: 18 },
    { id: 19 },
    { id: 20 },
    { id: 21 },
    { id: 22 },
    { id: 23 },
    { id: 24 },
    { id: 25 },
    { id: 26 },
    { id: 27 },
    { id: 28 },
    { id: 29 },
    { id: 30 },
    { id: 31 },


  ]


  scheduledDates: Date[] = [];
  completedScheduledDate: Date[] = [];
  pendingScheduledDate: Date[] = [];
  constructor(private fb: FormBuilder,
    public _router: Router,
    public route: ActivatedRoute,
    private userService: UserService,
    private invoiceService: InvoiceService,
    private utilityService: UtilityService,
    private alertService: AlertService,
    private financialYearService: FinancialYearService,
    public dialog: MatDialog,
    private invoiceService_in: InvoiceService,
    protected partyService: PartyService,
    private dialogRef: MatDialogRef<AutoTransactionMasterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.setActivityRoleBinding();
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }

  ngOnInit() {
    this.disables=false;
    this.initForm();
    this.getDefaultDate();

    if (this.dialogRef.id) {
      this.tags = false;

      this.getAutoTransaction();
    }
    else {
      this.readonlyParty =false;
      this.tags = true;
      this.getAllParty();
      
    }
  }

  ngOnDestroy() {

    this.onDestroy.next(true);
    this.onDestroy.complete();
  }


  private initForm() {
    let data: AutoTransactionGeneration = {
      id: null,
      periodToCreateTransaction: null,
      countOfInvoice: 2,
      startDate: this.defaultDate,
      invoiceHeaderId: null,
      daysOfWeek: null,
      daysOfMonth: null,
      invoiceNumber: null,
      stopTranaction: null,
      email: null,
      endDate: null,
      totalCount: null,
      sheduledDates: null,
      partyName:null,
      partyId:null,

    }
    this.autoTransactionForm = this.toFormGroup(data);
  }


  private toFormGroup(data: AutoTransactionGeneration): FormGroup {



    const formGroup = this.fb.group({
      id: [data.id],
      periodToCreateTransaction: [data.periodToCreateTransaction, Validators.compose([Validators.required])],
      countOfInvoice: [data.countOfInvoice, Validators.compose([Validators.required, Validators.min(2), Validators.max(12)])],
      startDate: [this.datePipe.transform(data.startDate, AppSettings.DATE_FORMAT)],
      invoiceHeaderId: [data.invoiceHeaderId],
      invoiceNumber: [data.invoiceNumber],
      daysOfWeek: [data.daysOfWeek],
      daysOfMonth: [data.daysOfMonth],
      stopTranaction: [data.stopTranaction],
      email: [data.email],
     // endDate: [this.datePipe.transform(data.endDate, AppSettings.DATE_FORMAT)],
      totalCount: [data.totalCount],
      sheduledDates: [data.sheduledDates],
      partyName:[data.partyName],
      partyId:[data.partyId]


    });
    this.fieldValidatorLabels.set("countOfInvoice", "Count");
   
    this.fieldValidatorLabels.set("periodToCreateTransaction", "Period To Create Transaction");
    //this.fieldValidatorLabels.set("startDate", "Start Date");
    return formGroup;

  }

  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;
      //console.log("activityRoleBindings: ", this.activityRoleBindings)
    })
  }

  getDefaultDate() {

    this.utilityService.getCurrentDateObs().subscribe(response => {
      this.defaultDate = this.datePipe.transform(response, AppSettings.DATE_FORMAT);
      this.financialMinDate = new Date(this.defaultDate);
      this.initForm();
    });

    this.financialYearService.getFinancialYear().subscribe(response => {

      this.financialYear = response;
      // this.financialMaxDate = new Date(this.financialYear.endDate);
      // this.financialMinDate = new Date(this.financialYear.startDate)

    });
  }





  delete(model: AutoTransactionGeneration, formDirective: FormGroupDirective) {

    this.dialogRefs = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRefs.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE


    this.dialogRefs.afterClosed().subscribe(result => {
      if (result) {

        this.spinStart = true;
        let id: string = this.autoTransactionForm.controls['id'].value;//this.quotationHeader.id;
        //console.log("deleteing..." + id);
        this.invoiceService.deleteTransaction(this.autoTransactionModel.id)
          .subscribe(response => {
            console.log(response.responseStatus);
            if (response.responseStatus === 1) {
              this.transactionStatus = response.responseString;
              this.saveStatus = true;
              this.recentAutoTransactionLoadCounter++;
              this.spinStart = false;
              this.clearFormNoStatusChange();
              this.alertService.success(this.transactionStatus);
              this.clearForm(formDirective);
            } else {
              this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
              this.transactionStatus = response.responseString;
              this.saveStatus = false;
              this.spinStart = false;
              this.alertService.danger(response.responseString);
            }
          },
            error => {
              //console.log("Error ", error);
              this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE
              this.alertService.danger(this.transactionStatus);
              this.saveStatus = false;
              this.spinStart = false;
            });
      }
      this.dialogRefs = null;
    });

  }

  clearFormNoStatusChange() {
    this.initForm();

    //   this.handleChanges();
    //  this.selectedParty = null;
  }




  onSelect(_event: any) {
    this.scheduledDates = [];
    this.disables = false;
    console.log(_event.value)
    if (_event.value === 4) {

      this.weeks = false;
      this.dateToGenerate = false;

    }

    if (_event.value === 3) {

      this.weeks = true;
      this.dateToGenerate = false;

    }

    if (_event.value === 1 || _event.value === 2) {

      this.weeks = false;
      this.dateToGenerate = true;
    }


  }

  closeForm() {
    // this._router.navigateByUrl('/dashboard')
    this._router.navigate(['/']);
  }

  clearForm(formDirective: FormGroupDirective) {
    this.saveStatus = false;
    this.spinStart = false;

    this.transactionStatus = null;
    formDirective.resetForm();
    this.initForm();
    this.autoTransactionForm.markAsPristine();
    this.invoiceNumbervalue = null;
    this.disablesInvoice = false;
    this.weeks = true;
    this.dateToGenerate = true;
    this.recentAutoTransactionStop = false;
    this.scheduledDates = [];
    this.disables=false;
    this.completedScheduledDate = [];
    this.pendingScheduledDate = [];
    this.allowPreview = true;
    this.allowReadOnly=false;
    this.periodValue=null;
    this.daysValue=null;
    this.weekValue=null;
    this.readonlyParty=false;
   
  }


  recentTransactionGet(event) {
    this.completedScheduledDate = [];
    this.pendingScheduledDate = [];
    this.allowPreview = false;
    this.autoTransactionModel = event;
    this.disablesInvoice = true;
    this.readonlyParty =true;
    this.lastDate=this.datePipe.transform(event.startDate, AppSettings.DATE_FORMAT);
   
    if (this.autoTransactionModel.daysOfMonth === null) {
      this.dateToGenerate = false;

    }

    if (this.autoTransactionModel.daysOfWeek === null) {
      this.weeks = false;
    }

    this.recentAutoTransactionStop = true;

    this.invoiceCount = event.totalCount;
    if ((this.invoiceCount && this.invoiceCount === event.countOfInvoice) || this.autoTransactionModel.stopTranaction === 1) {


      this.disableForm();
      this.applicableButtons.isEditButton = false;
      
    }
  else if (this.invoiceCount && this.invoiceCount != event.countOfInvoice) {
    this.applicableButtons.isEditButton = true;
    this.allowReadOnly=true;
      this.disables = true;
      this.disablePeriod = true;
      this.periodToCreateTransactions.forEach(value =>{
        if(value.id===this.autoTransactionModel.periodToCreateTransaction){
          this.periodValue =value.name;
        }
      })

      this.daysOfWeeks.forEach(value =>{
        if(value.id===this.autoTransactionModel.daysOfWeek){
          this.weekValue =value.name;
        }
      })
     this.daysValue=this.autoTransactionModel.daysOfMonth
      // this.dateToGenerateInvoices.forEach(value =>{
      //   if(value.id===this.autoTransactionModel.daysOfMonth){
      //     this.daysValue =value.id;
      //   }
      // })




    }

    else {

      this.applicableButtons.isEditButton = true;
      this.disables = false;
    }


    this.invoiceNumbervalue = this.autoTransactionModel.invoiceNumber;

    let curDate: Date = new Date();


    this.autoTransactionModel.sheduledDates.forEach(date => {

      let sheaduledDate: Date = new Date(date);


      if (sheaduledDate > curDate) {
        this.pendingScheduledDate.push(sheaduledDate);
      }
      else if (sheaduledDate < curDate) {
        this.completedScheduledDate.push(sheaduledDate);
      }
    })
     this.tempPartyName =this.autoTransactionModel.partyName
    this.autoTransactionForm = this.toFormGroup(this.autoTransactionModel);



    this.disableForm();
  }


  enableForm() {
    this.autoTransactionForm.enable({ onlySelf: true, emitEvent: false });

  }


  edit() {
   this.enableForm();

  }



  disableForm() {
    this.autoTransactionForm.disable({ onlySelf: true, emitEvent: false });
    // this.disableParty = true;
  }

  getAllInvoice(partyId:number) {


    this.invoiceService.getInvoiceByTransactionType(partyId,1).subscribe(response => {

      this.sourceInvoices = response;
  
    });

  }


  stopTransaction() {

    this.dialogRefs = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRefs.componentInstance.confirmMessage = AppSettings.AUTO_TRANSACTION_STOP_CONFORMATION_MESSAGE;

    this.dialogRefs.afterClosed().subscribe(result => {
      if (!result) {

        return
      }
      else {
        this.invoiceService.stopAutoTransaction(this.autoTransactionModel)
          .subscribe(response => {

            if (response.stopTranaction === 1) {
              this.disableStop = true;
              this.applicableButtons.isEditButton = false;
              this.transactionStatus = "Transaction is Terminated";
              this.alertService.success(this.transactionStatus);
              this.disableForm();

            }
          });
      }
    });
  }
  whenNoDataFound(noDataFound: boolean) {


    if (noDataFound) {
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });

      this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE;
    }
  }
  onChange() {

    this.scheduledDates = [];
    this.allowPreview = true;
  }






  save(model: AutoTransactionGeneration) {

    if (!model.id && this.scheduledDates.length === 0) {
      
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });
      this.alertRef.componentInstance.alertMessage = "Please Check Scheduled Dates Before Saving";
    }
    else {

      if (this.dialogRef.id) {
        model.invoiceHeaderId = this.dialogRef.id

      }
      if (!this.autoTransactionForm.valid) {

        const controls = this.autoTransactionForm;
        let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
        ////console.log("invalidFieldList ", invalidFieldList)

        let invalidFiledLabels: string[] = [];

        invalidFieldList.forEach(field => {
          if (this.fieldValidatorLabels.get(field))
            invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
        })

        ////console.log("invalidFiledLabels ", invalidFiledLabels)

        this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
        return false;
      }
      let currentDate:string=model.startDate;
     if(this.lastDate!=this.datePipe.transform(currentDate, AppSettings.DATE_FORMAT)){
      if (this.datePipe.transform(currentDate, AppSettings.DATE_FORMAT)<this.defaultDate){
        this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + "valid Start Date"); 
        return false;
      
      }
   
    }


      console.log(model.email + "model.email");
      model.email = model.email ? 1 : 0;

      console.log(model.countOfInvoice + "model.email");


      //  if(!this.invoiceCount){
      //   if(this.defaultDate>=this.autoTransactionForm.get('startDate').value){
      //   this.dialogRef = this.dialog.open(ConfirmationDialog, {
      //     disableClose: false
      //   });

      //   this.dialogRef.componentInstance.confirmMessage = AppSettings.AUTO_TRANSACTION_STOP_CONFORMATION_MESSAGE;

      //   this.dialogRef.afterClosed().subscribe(result => {
      //     if (!result) {

      //       return
      //     }



      //  })
      // }
      // }






      this.spinStart = true;
      //console.log("before save: ", model);
      let message: string;
      if (model.id === null) {
        message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
      }
      else {
        message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
      }

      let AutoTransactionGenerationList: AutoTransactionGenerationList = { autoTransactionGenerationDTO: model };

      console.log(model.invoiceHeaderId)

      AutoTransactionGenerationList.autoTransactionGenerationDTO.sheduledDates = this.scheduledDates;




      this.invoiceService.saveAutoTransactionDetail(AutoTransactionGenerationList)
        .subscribe(response => {
          if (response.autoTransactionGenerationDTO.id === 0) {

            this.alertService.danger("Invoice Is Alerdy Exists");
            this.spinStart = false;
          }
          else {
            this.autoTransactionModel = response.autoTransactionGenerationDTO;
            this.transactionStatus = "Transaction Saved Succefully";
            this.alertService.success(this.transactionStatus);
            this.saveStatus = true;
            this.recentAutoTransactionStop = true;
            this.spinStart = false;
            this.recentAutoTransactionLoadCounter++;
                this.readonlyParty=true;
            // this.disableForm();
            this.tempPartyName =this.autoTransactionModel.partyName

            this.autoTransactionForm = this.toFormGroup(this.autoTransactionModel);
            this.disableForm();
          }
        },
          error => {
            //console.log("Error ", error);
            this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE
            this.alertService.danger(this.transactionStatus);
            this.saveStatus = false;
            this.spinStart = false;
          });
   }

  }



  getAutoTransaction() {

    if (this.dialogRef.id) {

      this.invoiceService_in.getAutoTransaction(this.dialogRef.id).subscribe(
        response => {

          if (response.id) {
           this.allowPreview = false;
            this.autoTransactionModel = response;
            
            let curDate: Date = new Date();


            this.autoTransactionModel.sheduledDates.forEach(date => {

              let sheaduledDate: Date = new Date(date);


              if (sheaduledDate > curDate) {
                this.pendingScheduledDate.push(sheaduledDate);
              }
              else if (sheaduledDate < curDate) {
                this.completedScheduledDate.push(sheaduledDate);
              }
            })







            this.autoTransactionForm = this.toFormGroup(this.autoTransactionModel);
            if (response.periodToCreateTransaction === 1 || response.periodToCreateTransaction === 2) {
              this.weeks = false;
              this.dateToGenerate = true;
            }
            if (response.periodToCreateTransaction === 4) {
              this.weeks = false;
              this.dateToGenerate = false;
            }
            if (response.periodToCreateTransaction === 3) {

              this.dateToGenerate = false;
              this.weeks = true;
            }
            this.invoiceCount = response.totalCount;
            if (response.totalCount === response.countOfInvoice) {
              this.disables = true;
              this.disablePeriod = true;
              this.autoTransactionForm.disable();
            }
            else if (response.totalCount && response.totalCount != response.countOfInvoice) {

              this.disables = true;
              this.disablePeriod = true;

              // this.autoTransactionPopUpForm.controls['countOfInvoice'].enable();


            }

            else if (response.stopTranaction === 1) {
              this.autoTransactionForm.disable();
              this.disableStop = true;
            }



          }

        }

      )
    }
  }

  closePopup(): void {
    //console.log('close clicked');
    this.dialogRef.close();
    // this.dialogRef.close();
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  generateMonthlyDates(dayOfMonth: number, occurances: number, startDate: Date): Date[] {

    let returnDates: Date[] = [];
    let dateCounter: number = 1;
    let loopCounter: number = 1;
    while (true) {
      let curDate: Date = new Date();
      let loopDate: Date = this.addDays(curDate, loopCounter);

      let loopDayOfMonth: number = loopDate.getDate();

      if (loopDayOfMonth === dayOfMonth && loopDate > startDate) {

        returnDates.push(loopDate);
        dateCounter++;

      }
      loopCounter++;

      if (dateCounter > occurances) {
        break; //exit loop
      }

      if (loopCounter > 1000) {
        break;
      }

    }

    return returnDates;
  }

  generateWeeklyDates(daysOfWeek: number, occurances: number, startDate: Date): Date[] {

    let returnDates: Date[] = [];
    let dateCounter: number = 1;
    let loopCounter: number = 1;
    while (true) {
      let curDate: Date = new Date();
      let loopDate: Date = this.addDays(curDate, loopCounter);

      //  let loopDayOfMonth: number = loopDate.getDate();
      let loopDayOfWeek: number = loopDate.getDay();
      loopDayOfWeek = loopDayOfWeek + 1;

      if (loopDayOfWeek === daysOfWeek && loopDate > startDate) {

        returnDates.push(loopDate);
        dateCounter++;

      }
      loopCounter++;

      if (dateCounter > occurances) {
        break; //exit loop
      }

      if (loopCounter > 1000) {
        break;
      }

    }

    return returnDates;
  }

  generateDailyDates(occurances: number, startDate: Date): Date[] {

    let returnDates: Date[] = [];
    let dateCounter: number = 1;
    let loopCounter: number = 1;
    while (true) {
      let curDate: Date = new Date();
      let loopDate: Date = this.addDays(curDate, loopCounter);

      if (loopDate > startDate) {

        returnDates.push(loopDate);
        dateCounter++;

      }
      loopCounter++;

      if (dateCounter > occurances) {
        break; //exit loop
      }

      if (loopCounter > 1000) {
        break;
      }

    }

    return returnDates;
  }

  generateQuterlyDates(dayOfMonth: number, occurances: number, startDate: Date): Date[] {

    let returnDates: Date[] = [];
    let dateCounter: number = 1;
    let loopCounter: number = 1;
    let monthCounter: number = 2;
    let nextMonth = 0;
    while (true) {
      let curDate: Date = new Date();

      let loopDate: Date = this.addMonth(curDate, loopCounter, monthCounter);


      let loopDayOfMonth: number = loopDate.getDate();


      if (loopDayOfMonth === dayOfMonth && loopDate > startDate) {

        returnDates.push(loopDate);
        dateCounter++;
        monthCounter += 2;
      }
      loopCounter++;


      if (dateCounter > occurances) {
        break; //exit loop
      }

      if (loopCounter > 1000) {
        break;
      }

    }

    return returnDates;
  }


  addMonth(date: Date, days: number, month: number): Date {
    date.getDate() + 1;
    date.setDate(date.getDate() + days);

  
     date.setMonth(date.getMonth() + month+1);
   

    return date;
  }



  getScheduledDates(model: AutoTransactionGeneration) {
    this.allowSave = true;
    let count:number=0;
    count = model.countOfInvoice - 1;
    switch (model.periodToCreateTransaction) {
      case 1:
        this.scheduledDates = this.generateMonthlyDates(model.daysOfMonth, count, new Date(model.startDate));
        break;
      case 2:
        this.scheduledDates = this.generateQuterlyDates(model.daysOfMonth, count, new Date(model.startDate));
        break;

      case 3:
        this.scheduledDates = this.scheduledDates = this.generateWeeklyDates(model.daysOfWeek, count, new Date(model.startDate));
        break;


      case 4:
        this.scheduledDates = this.generateDailyDates(count, new Date(model.startDate));
        break;


    }

  }
  getAllParty(){
    this.partyService.getCustomers().subscribe(response => {
       this.partys = response
    })
  }

  onSelectParty(_event: any){
   let partyId = _event.value;
   this.getAllInvoice(partyId);

  }
//   getFilteredParties() {

//     let partyType: string = AppSettings.PARTY_TYPE_CUSTOMER;
    
//     this.filteredParties = this.partyFormControl.valueChanges
//         .pipe(
//         startWith<string | Party>(''),
//         debounceTime(300),
//         map(value => typeof value === 'string' ? value : value.name),
//         filter(value => value != (this.selectedParty ? this.selectedParty.name : '')),
//         //// tap(val => //console.log('party search: ', val)),
//         //map(val => this.filterParty(val))
//         switchMap(value => this.partyService.getPartiesByNameLike(partyType, value) )
//         );
// }
// displayFnParty(party?:Party): string | undefined {
//     return party ? party.partyCode+"-"+party.name + (party.areaName ? '['+party.areaName+']' : ''): undefined;
// }


}


export interface periodToCreateTransaction {
  id: number,
  name: string,
}

export interface limitToCreateTransaction {
  id: number,
  name: string,
}

export interface daysOfWeek {
  id: number,
  name: string,
}
export interface dateToGenerateInvoice {
  id: number,

}