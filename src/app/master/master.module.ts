import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { MaterialModule } from '../module/material.module';

import { MasterRoutingModule } from './master-routing.module';
// import { BankMasterComponent } from './bank-master/bank-master.component';
import { RecentBankMasterTableComponent } from './bank-master/recent-bank-table.component';
import { RecentMaterialTable } from './material/recent-material-table.component';
import { MaterialComponent } from './material/material.component';
import { CompanyComponent } from './company/company.component';
import { PartyComponent } from './party/party.component';
import { RecentPartiesMasterTableComponent } from './party/recent-parties-table.component';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
import { RecentEmployeeMasterTableComponent } from './employee-management/recent-employ-table.component';
import { CountryComponent } from './location/country/country.component';
import { StateComponent } from './location/state/state.component';
import { CityComponent } from './location/city/city.component';
import { AreaComponent } from './location/area/area.component';
import { SharedModule } from '../shared/shared.module';
import { FinancialMasterComponent } from './financial-master/financial-master.component';
import { PreventUnsavedChangesGuardBank } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-bank';
import { PreventUnsavedChangesGuardCompany } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-company';
import { PreventUnsavedChangesGuardFinancial } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-financial';
import { PreventUnsavedChangesGuard } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard.service';
import { PreventUnsavedChangesGuardParty } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-party';
import { PreventUnsavedChangesGuardMaterial } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-material';
import { ProcessPopupComponent } from './process-popup/process-popup.component';
import { ExpenseComponent } from './expense/expense.component';
import { PreventUnsavedChangesGuardExpense } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-expense';
import { PasswordPopupComponent } from './password-popup/password-popup.component';
import { ProcessMasterComponent } from './process-master/process-master.component';
import { SecurePipe } from '../pipes/secure.pipe';


// import { MasterSubHeadingComponent } from '../master/master-sub-heading/master-sub-heading.component';
//import { ExpensePopupComponent } from './expense-popup/expense-popup.component';
// import { PartyPopupComponent } from './party-popup/party-popup.component';
// import { MaterialPopupComponent } from './material-popup/material-popup.component';

@NgModule({
  imports: [
    CommonModule,
    MasterRoutingModule,
    //MaterialModule,
    SharedModule,
    
  ],
  declarations: [
    // BankMasterComponent,
    RecentBankMasterTableComponent,
    RecentMaterialTable,
    MaterialComponent,
    CompanyComponent,
    PartyComponent,
    RecentPartiesMasterTableComponent,
    EmployeeManagementComponent,
    RecentEmployeeMasterTableComponent,
    CountryComponent,
    StateComponent,
    CityComponent,
    AreaComponent,
    FinancialMasterComponent,
    ProcessPopupComponent,
    ExpenseComponent,
    PasswordPopupComponent,
    ProcessMasterComponent,
    
   
 
    // MasterSubHeadingComponent,
   // ExpensePopupComponent,
    // PartyPopupComponent,
    // MaterialPopupComponent,
    SecurePipe,
  ],
  providers: [
    PreventUnsavedChangesGuardBank,
    PreventUnsavedChangesGuardCompany,
    PreventUnsavedChangesGuardFinancial,
    PreventUnsavedChangesGuard,
    PreventUnsavedChangesGuardParty,
    PreventUnsavedChangesGuardExpense,
    PreventUnsavedChangesGuardMaterial,
  ],
  entryComponents: [PasswordPopupComponent]
})
export class MasterModule { }
