import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { BankMasterComponent } from "./bank-master.component";
import { PreventUnsavedChangesGuardBank } from "../../UnsavedChangesGuard/prevent-unsaved-changes-guard-bank";
import { BankMasterRoutingModule } from "./bank-master-routing.module";

@NgModule({
    imports: [
      CommonModule,
      BankMasterRoutingModule,
      SharedModule,
      
    ],
    declarations: [
      BankMasterComponent,
    ],
    providers: [
      PreventUnsavedChangesGuardBank,
    ]
  })
  export class BankMasterModule { }