import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BankMasterComponent } from "./bank-master.component";
import { PreventUnsavedChangesGuardBank } from "../../UnsavedChangesGuard/prevent-unsaved-changes-guard-bank";

const routes: Routes = [

    {
      path: '',
      component: BankMasterComponent,
      data: { state: 'app-bank-master' },
      canDeactivate: [PreventUnsavedChangesGuardBank]
    },
  
 
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class BankMasterRoutingModule { }
  