import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BankModel, BankWrapper } from '../../data-model/bank-model';
import { BankService } from '../../services/bank.service';
import { Router } from '@angular/router';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { LanguageService } from '../../services/language.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bank-master',
  templateUrl: './bank-master.component.html',
  styleUrls: ['./bank-master.component.scss']
})

export class BankMasterComponent implements OnInit {

  @ViewChild(DatatableComponent, {static: true}) table: DatatableComponent;

  title : String;
  subheading : string;

  bankForm: FormGroup;
  //countryWrapper: CountryWrapper = ;
  banks: BankModel[] = [];
  selected = [];
  tempArray: BankModel[]
  dialogRef: MatDialogRef<ConfirmationDialog>;
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;
  saveStatus: boolean = false;
  activityRoleBindings: ActivityRoleBinding[];
  enableSaveButton: boolean = false;
  constructor(private fb: FormBuilder,
    private bankService: BankService,
    private dialog: MatDialog, 
    private alertService: AlertService,
    private userService: UserService,
    private _router: Router,
    private languageService: LanguageService,
    private translate: TranslateService,
    public route: ActivatedRoute) {
    this.subheading = AppSettings.SUB_HEADING_LINE + this.userService.activityName
    this.title = this.userService.activityName 
      this.setActivityRoleBinding();
      translate.setDefaultLang('en');
  }

  ngOnInit() {
    this.applyLanguage();
    this.getBanks();
    this.initForm();
  }

  applyLanguage() {
    this.languageService.getLanguage()
    .subscribe(resp => {
    if(resp===null){
      this.translate.use("en");
    }
    else{
      this.translate.use(resp);
    }
   
  })
  }

  getBanks() {
    this.bankService.getAllBanks().subscribe(response => {
      this.banks = response;
      //console.log('got banks: ', this.banks);
      this.bankForm = this.toFormGroup({ banks: this.banks });
      this.tempArray = [...response];
    })
  }

  setActivityRoleBinding(){
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      this.activityRoleBindings = response;

      this.activityRoleBindings.forEach(activityRoleBinding => {

        ////console.log('in action panel: ', activityRoleBinding.permissionToUpdate);
        ////console.log('in action panel: ', activityRoleBinding.permissionToCreate);
        ////console.log('in action panel activityRoleBinding.permissionToCancel: ', activityRoleBinding.permissionToCancel);

        this.enableSaveButton = (activityRoleBinding.permissionToUpdate === 1 || activityRoleBinding.permissionToCreate === 1 || this.enableSaveButton) ? true : false;
      });
  })
}

  initForm() {
    let data: BankWrapper = {
      banks: null
    };

    this.bankForm = this.toFormGroup(data);
  }

  toFormGroup(data: BankWrapper): FormGroup {

    const banks = new FormArray([]);
    if (data.banks) {
      data.banks.forEach(c => {
        banks.push(this.makeItem(c))
      })
    }

    const formGroup = this.fb.group({
      banks: banks
    });

    return formGroup;
  }


  onSubmit(model: BankWrapper) {
    //console.log('before save: ', model);
    this.bankService.createBanks(model.banks).subscribe(response => {
      if(response){
        this.bankForm = this.toFormGroup({banks: response});
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
        this.saveStatus = true;
      }
    });
  }

  makeItem(c: BankModel): FormGroup {
    return this.fb.group({
      id: c.id,
      bankname: [c.bankname, Validators.required],
      bankAddress: [c.bankAddress, Validators.required],
      deleted: c.deleted
    })
  }
  addNew() {
    const control = <FormArray>this.bankForm.controls['banks'];
    //Add one blank Item
    let item: BankModel = {
      id: null,
      bankname: null,
      bankAddress: null,
      deleted: null,
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length/this.pageSize);
  }

  
  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

 


  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: BankModel[] = this.bankForm.get('banks').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.bankService.deleteBanks(itemIdList).subscribe(response => {

            //console.log("3: ", response);
            if (response.responseStatus === 1) {

              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.bankForm = this.toFormGroup({ banks: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              if(response.responseString.includes('ConstraintViolationException')){
                this.alertService.danger(AppSettings.BANK_DELETE_DUPLICATE_ERROR);
              }else{
                this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
              }
              
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{
          // items.forEach((item, idx) => {
          //   if(!item.id)
          //     (<FormArray>this.stateForm.get('states')).removeAt(idx)
          // })
          items = items.filter(i => (i.id != null &&!(this.selected.find(s => s.name === i.bankname))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.bankForm = this.toFormGroup({ banks: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }


  updateFilter(event){

    const val = event.target.value.toLowerCase();

    ////console.log("this.tempArray: ",this.tempArray)
    // filter our data
    const temp: BankModel[] = this.tempArray.filter(d => d.bankname.toLowerCase().indexOf(val) != -1 || !val)

    // update the rows
    this.bankForm = this.toFormGroup({ banks: temp });

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;


  }


  close()
  {
    this._router.navigate(['/'])
  }

}
