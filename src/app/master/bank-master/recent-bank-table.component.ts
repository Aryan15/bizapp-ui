import { Component,Input,OnChanges, OnInit,Output,EventEmitter, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Observable, merge, of as observableOf} from 'rxjs';
import {catchError,  map,  debounceTime , startWith, switchMap} from 'rxjs/operators';
import { BankService } from '../../services/bank.service';
import { BankModel, RecentBank } from '../../data-model/bank-model';
import { FormControl, FormGroup } from '@angular/forms';


const PAGE_SIZE: number = 10;

@Component({
    selector: 'recent-bank-table',
    templateUrl: './recent-bank-table.component.html',
    styleUrls: ['./recent-bank-table.component.scss']
  })
  export class RecentBankMasterTableComponent implements OnInit, OnChanges {

    @Input()
    recentBankLoadCounter: number;

    @Output()
    BankModel: EventEmitter<BankModel> = new EventEmitter<BankModel>();

    @Output()
    noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();
    displayedColumns = ['bankName', 'accountNumber'];
    bankMasterDatabase: BankMasterHttpDao | null; 

    dataSource = new MatTableDataSource<BankModel>();

    filterForm = new FormGroup ({
        filterText: new FormControl()
    });

    resultsLength = 0;
    isLoadingResults = true;
    //isRateLimitReached = false;
  
    pageSize: number = PAGE_SIZE;
    filterTextObservable: Observable<string>;


    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private bankService: BankService) { }


    ngOnInit(){


        this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));
    
        this.bankMasterDatabase = new BankMasterHttpDao(this.bankService);

        //If the user changes sort order, reset back to first page

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
        .pipe(
          startWith({}),
          switchMap(() => {
            this.isLoadingResults = true;
            return this.bankMasterDatabase!.getBanks(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction,(this.paginator.pageIndex - 1), this.paginator.pageSize);
          }),
          map(data => {
            //Flip flag to show that loading has finished
            this.isLoadingResults = false;
            this.resultsLength = data.totalCount;
            //console.log('data.materialList: ', data.bankModels);
            //console.log('data.totalCount: ', data.totalCount);
            if( this.filterForm.get('filterText').value != null)
            {
             if(data.totalCount === 0)
             {
               this.noDataFound.emit(true);
             }
            }
            return data.bankModels;
          }),
          catchError(() => {
            this.isLoadingResults = false;
            //console.log('catchError error');
            return observableOf([]);
          })
        ).subscribe(data => this.dataSource.data = data);

    }
    ngOnChanges(){
      //this.onChangeEvent.
      // //console.log('this.recentInvoiceLoadCounterObservable: ',this.recentInvoiceLoadCounterObservable);
      // this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);
      //console.log('in onchanage: ', this.recentBankLoadCounter);
  
      if(this.recentBankLoadCounter > 0 ){
        this.bankMasterDatabase = new BankMasterHttpDao(this.bankService);
  
        this.bankMasterDatabase!.getBanks(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.bankModels);
          //console.log('data.totalCount: ', data.totalCount);
          if( this.filterForm.get('filterText').value != null)
          {
          if(data.totalCount === 0)
          {
            this.noDataFound.emit(true);
          }
          }
          return data.bankModels;
        })).subscribe(data => this.dataSource.data = data);
      }
    }
  
    onRowClick(row: any){
        //console.log('row clicked: ', row );
        this.BankModel.emit(row);
        //console.log('done' );
    }

  }


  export class BankMasterHttpDao{
    
      constructor(private bankService: BankService) { }
    
      getBanks(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentBank>{
    
        pageSize = pageSize ? pageSize : PAGE_SIZE;
        //console.log('filterText: ', filterText);
    
    
        // this.materialService.getAllMaterialsForReport(sortColumn, sortDirection, page, pageSize).subscribe(response => {
        //   ////console.log("response: ", response);
        // })
    
        return this.bankService.getRecentBanks(filterText, sortColumn, sortDirection, page, pageSize);
      }
    }