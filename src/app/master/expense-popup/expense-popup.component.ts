import { Component, OnInit, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AppSettings } from '../../app.settings';
import { MatDialogRef } from '@angular/material/dialog';
import { ExpenseHeader } from '../../data-model/voucher-model';
import { VoucherService } from '../../services/voucher.service';
@Component({
  selector: 'app-expense-popup',
  templateUrl: './expense-popup.component.html',
  styleUrls: ['./expense-popup.component.scss']
})
export class ExpensePopupComponent implements OnInit {
  public expensePopUpForm: FormGroup;
  public expense: ExpenseHeader;

  currentUser:any;
  themeClass:any;
  transactionStatus: string = null;
  isExpenseHeader: boolean = true;



  constructor(private fb: FormBuilder,
   private voucherService: VoucherService,
    private dialogRef: MatDialogRef<ExpensePopupComponent>,
    ) { 
      this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
      this.themeClass = this.currentUser.userThemeName;
      //console.log('in expense popup cons');
    }

  ngOnInit() {
    // this.setSupplyType();
    //console.log('in expense popup init');
    this.getExpense();
    this.initForm();
  }

  getExpense(){

  }
  private initForm() {
    let data: ExpenseHeader = {
      id: null,
      deleted: null,
      name: null,
    };

    this.expensePopUpForm = this.toFormGroup(data);
  }


  private toFormGroup(data: ExpenseHeader): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      deleted: [data.deleted],
      name: [data.name, [Validators.required]],
    });

    return formGroup;

  }

  save() {
    //console.log('in expense popup save click'+this.expensePopUpForm.value);
    this.dialogRef.close(this.expensePopUpForm.value);
   
 
  }

  close() {
    //console.log('in expense popup close click');
    this.dialogRef.close();
  }


}
