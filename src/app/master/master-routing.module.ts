import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialComponent } from './material/material.component';
import { PreventUnsavedChangesGuardMaterial } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-material';
import { CompanyComponent } from './company/company.component';
import { PreventUnsavedChangesGuardCompany } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-company';
import { PreventUnsavedChangesGuardBank } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-bank';
import { PartyComponent } from './party/party.component';
import { PreventUnsavedChangesGuardParty } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-party';
import { EmployeeManagementComponent } from './employee-management/employee-management.component';
import { PreventUnsavedChangesGuard } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard.service';
import { FinancialMasterComponent } from './financial-master/financial-master.component';
import { PreventUnsavedChangesGuardFinancial } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-financial';
import { CountryComponent } from './location/country/country.component';
import { StateComponent } from './location/state/state.component';
import { CityComponent } from './location/city/city.component';
import { AreaComponent } from './location/area/area.component';
import { ExpenseComponent } from './expense/expense.component';
import { PreventUnsavedChangesGuardExpense } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-expense';
import { ProcessMasterComponent } from './process-master/process-master.component';
import { PreventUnsavedChangesGuardProcess } from '../UnsavedChangesGuard/prevent-unsaved-changes-guard-Process';



const routes: Routes = [
  {
    path: 'material-master/:supplyTypeId',
    component: MaterialComponent,
    data: { state: 'app-material' },
    canDeactivate: [PreventUnsavedChangesGuardMaterial]
  },
  {
    path: 'company-master',
    component: CompanyComponent,
    data: { state: 'company-master' },
    canDeactivate: [PreventUnsavedChangesGuardCompany]
  },
  {
    path: 'app-bank-master',
    loadChildren: './bank-master/bank-master.module#BankMasterModule',
    data: { state: 'app-bank-master' },
    canDeactivate: [PreventUnsavedChangesGuardBank]
  },
  {
    path: 'party-master/:partytypeId',
    component: PartyComponent,
    data: { state: 'party-master' },
    canDeactivate: [PreventUnsavedChangesGuardParty]
  },
  {
    path: 'process-master',
    component: ProcessMasterComponent,
    data: { state: 'app-process-master' },
    canDeactivate: [PreventUnsavedChangesGuardProcess]
  },

  {
    path: 'employee-management',
    component: EmployeeManagementComponent,
    data: { state: 'employee-management' },
    canDeactivate: [PreventUnsavedChangesGuard]
  },
  {
    path: 'app-financial-master',
    component: FinancialMasterComponent,
    data: { state: 'app-financial-master' }
    , canDeactivate: [PreventUnsavedChangesGuardFinancial]
  },
  {
    path: 'country-new',
    component: CountryComponent,
    data: { state: 'country-new' }
  }, {
    path: 'app-state-master',
    component: StateComponent,
    data: { state: 'app-state-master' }
  }, {
    path: 'app-city-master',
    component: CityComponent,
    data: { state: 'app-city-master' }
  }, {
    path: 'app-area-master',
    component: AreaComponent,
    data: { state: 'app-area-master' }
  },
  {
    path: 'app-expense',
    component: ExpenseComponent,
    data: { state: 'app-expense' },
    canDeactivate: [PreventUnsavedChangesGuardExpense]
  },

 

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
