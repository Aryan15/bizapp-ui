import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Party, PartyType, AddressList } from '../../data-model/party-model';
import { State } from '../../data-model/state-model';
import { Country } from '../../data-model/country-model';
import { LocationService } from '../../services/location.service';
import { Router } from '@angular/router';
import { PartyService } from '../../services/party.service';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../app.settings';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material/dialog";
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { TransactionTypeService } from '../../services/transaction-type.service';
import { NumberRangeConfigService } from '../../services/number-range-config.service';
import { AlertService } from 'ngx-alerts';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { Currency } from '../../data-model/currency-model';

// import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/Party";
@Component({
  selector: 'app-party-popup',
  templateUrl: './party-popup.component.html',
  styleUrls: ['./party-popup.component.scss']
})
export class PartyPopupComponent implements OnInit, OnDestroy {

  public partyPopUpForm: FormGroup;
  public partyModel: Party;
  OnDestroy: Subject<boolean> = new Subject();
  countries: Country[] = [];
  states: State[] = [];
  allStates: State[] = [];
  //currencys: Currency[] =[];
  //allCurrensys: Currency[] = [];

  hide: boolean = true;
  onDestroy: Subject<boolean> = new Subject();
  tempTransactionNumber: string = "";
  //public partyTypes: PartyType[] = [];
  currentUser: any;
  themeClass: any;
  partyType: PartyType;
  transactionStatus: string = null;
  errorMessage: string = null;
  confirmPassword: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  isGstRegistered: boolean = false;
  transactionType: string = "";
  isAutoNumber: boolean = false;
  allowCurrency:boolean=false;

  dialogReff: MatDialogRef<ConfirmationDialog>;
  public gstRegistrationTypes: GSTRegistrationType[] = [
    { id: 1, name: "Normal" },
    { id: 2, name: "Composition" },
    { id: 3, name: "Unregistered /URP" }
  ]
  constructor(private fb: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private locationService: LocationService,
    private partyService: PartyService,
    private dialogRef: MatDialogRef<PartyPopupComponent>,
    private transactionTypeService: TransactionTypeService,
    private numberRangeConfigService: NumberRangeConfigService,
    private alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;

    // this.description = data.description;
  }
  ngOnInit() {

    this.initForm();

    this.getCountries();
    //this.getAllStates();
    this.getPartyTypes();
    this.getAllStates()
    //this.getAllCurrencys();


    this.getRegstrationType();
  }

  private getCountries() {

    this.locationService.getCountries()
      .subscribe(response => {
        this.countries = response;
      })
  }
  //private getAllCurrencys() {

 //   this.locationService.getCurrencys()
    //  .subscribe(response => {
    //    this.currencys = response;
     //   this.allCurrensys = response;
     // })
 // }
  private getPartyTypes() {
    let partyType: number;
    this.partyService.getAllPartyTypes()
      .subscribe(response => {
        this.partyType = response.find(pt => pt.id === this.data.partyTypeId);

        if (this.partyType.id === 1) {
          this.transactionType = AppSettings.PARTY_TYPE_CUSTOMER_CODE;
          partyType = AppSettings.PARTY_CTYPE;
        }
        else {
          this.transactionType = AppSettings.PARTY_TYPE_SUPPLIER_CODE;
          partyType = AppSettings.PARTY_STYPE;
        }


        if (partyType) {
          this.numberRangeConfigService.getNumberRangeConfigurationForTransactionType(partyType)
            .pipe(
              takeUntil(this.onDestroy)
            )
            .subscribe(response => {
              this.isAutoNumber = response.autoNumber === 1 ? true : false;
              if (response && this.isAutoNumber === true) {
                this.getTempTransactionNumber(this.transactionType)
              }
            })
        }

      })



  }
  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
        this.allStates = response;
      })
  }

  ngOnDestroy() {
    ////console.log('ngOnDestory');
    this.OnDestroy.next(true);
    this.OnDestroy.complete();
    //this.tranCommSub.unsubscribe();

  }


  onStateChange() {
    let stateId = this.partyPopUpForm.get('stateId').value;
    let state = this.states.find(p => p.id === stateId);

    ////console.log("city id in area", state.countryId)
    ////console.log("this.cities for area", this.countries.find(p => p.id === state.countryId))
    let value = this.countries.find(p => p.id === state.countryId);
    //console.log("value", value.name)
    this.partyPopUpForm.get('countryId').patchValue(value.id);

  }
  private getStates(countryId) {
    this.states = this.allStates.filter(state => state.countryId === countryId);
  }


  private initForm() {
    let data: Party = {
      id: null,
      name: null,
      address: null,
      countryId: null,
      stateId: null,
      stateCode: null,
      stateName: null,
     // portStateId:null,
     partyCurrencyName:null,
      cityId: null,
      areaId: null,
      pinCode: null,
      primaryTelephone: null,
      secondaryTelephone: null,
      primaryMobile: null,
      secondaryMobile: null,
      email: null,
      contactPersonName: null,
      contactPersonNumber: null,
      webSite: null,
      partyTypeId: null,
      deleted: null,
      billAddress: null,
      panNumber: null,
      gstRegistrationTypeId: null,
      gstNumber: null,
      partyTypeName: null,
      addressesListDTO: null,
      dueDaysLimit: null,
      isIgst: null,
      areaName: null,
      partyId: null,
      partyCode: null,
      partyCurrencyId: null,
    };

    this.partyPopUpForm = this.toFormGroup(data);
  }

  private mapAddress(data: AddressList): FormGroup {

    const formGroup = this.fb.group({
      address: [data.address],
      contactPersonName: [data.contactPersonName],
      contactPersonNumber: [data.contactPersonNumber],
      contactPersonDesignation: [data.contactPersonDesignation],
      deleted: [data.deleted],
    });
    return formGroup;
  }

  private toFormGroup(data: Party): FormGroup {

    const addressesArray = new FormArray([]);
    if (data.addressesListDTO) {

      data.addressesListDTO.forEach(address => {

        //let item : MaterialPriceList = 
        addressesArray.push(this.mapAddress(address));
      });
    }

    const formGroup = this.fb.group({
      id: [data.id],
      name: [data.name, Validators.required],
      address: [data.address, Validators.required],
      countryId: [data.countryId, Validators.required],
      stateId: [data.stateId, Validators.required],
      stateCode: [data.stateCode],
      stateName: [data.stateName],
      primaryMobile: [data.primaryMobile, Validators.pattern('^[0-9\-\+]{9,15}$')],
      email: [data.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')],
      partyTypeId: [data.partyTypeId],
      deleted: [data.deleted],
      gstRegistrationTypeId: [data.gstRegistrationTypeId, Validators.required],
      gstNumber: [data.gstNumber, Validators.compose([Validators.minLength(15), Validators.maxLength(15)])],
      partyTypeName: [data.partyTypeName],
      addressesListDTO: addressesArray,
      isIgst: [data.isIgst],
      partyId: [data.partyId],
      partyCode: [data.partyCode],
     currencyId:[data.partyCurrencyId]
    });

    return formGroup;

  }

  save() {


   
    if (this.partyPopUpForm.controls['isIgst'].value) {
      this.partyPopUpForm.controls['isIgst'].patchValue(1);
    } else {
      this.partyPopUpForm.controls['isIgst'].patchValue(0);
    }
    let partyNumber: string = this.partyPopUpForm.get('partyCode').value;
    if (!partyNumber && !this.isAutoNumber) {
      this.alertService.danger("Please Enter Party Code");
       return;
    }
    if (partyNumber && !this.isAutoNumber) {
      this.partyService.checkPartyNumber(partyNumber, this.partyType.id).subscribe(response => {
        if (response.responseStatus === 0) {
          this.dialogReff = this.dialog.open(ConfirmationDialog, {
            disableClose: false
          });
          this.dialogReff.componentInstance.confirmMessage = partyNumber + "-" + AppSettings.PARTY_CODE_CONFIRMATION_MESSAGE;
          this.dialogReff.afterClosed().subscribe(result => {

            if (result) {
              this.partyPopUpForm.controls['partyTypeId'].patchValue(this.partyType.id);
              this.dialogRef.close(this.partyPopUpForm.value);
            }
            else {
              return;
            }

          });


        }
        else {
          this.partyPopUpForm.controls['partyTypeId'].patchValue(this.partyType.id);
          this.dialogRef.close(this.partyPopUpForm.value);
        }
      })
    }
    else {
      this.partyPopUpForm.controls['partyTypeId'].patchValue(this.partyType.id);
      this.dialogRef.close(this.partyPopUpForm.value);
    }


    //his.invoiceForm.controls['partyId'].patchValue(response.partyDTO.id);

  }

  close(): void {
    //console.log('close clicked');
    this.dialogRef.close();
    // this.dialogRef.close();
  }


  //   submit(party: Party) {
  //       // }
  // this.spinStart = true;
  // //console.log('Before save: ', party);

  // this.partyService.save(party)
  //   .subscribe(response => {
  //     this.saveStatus = true;
  //     this.spinStart = false;
  //     this.transactionStatus = "Party " + response.name + AppSettings.SAVE_SUCESSFULL_MESSAGE;
  //     // this.partyForm.get('partyDTO')['controls'].id.patchValue(response.partyDTO.id);
  //     // this.transactionStatus = "Party " + response.partyDTO.name + AppSettings.SAVE_SUCESSFULL_MESSAGE;
  //     // this.alertService.success(this.transactionStatus);
  //   },
  //   error => {
  //     //console.log("Error ", error);
  //     this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
  //     // this.alertService.danger(this.transactionStatus);
  //     this.saveStatus = false;
  //     this.spinStart = false;
  //   });



  // }
  getRegstrationType() {
    this.partyPopUpForm.controls['gstRegistrationTypeId'].valueChanges.subscribe(items => {
      // //console.log("gstRegistrationTypeId" + this.partyForm.get('partyDTO.gstRegistrationTypeId').value)
      //console.log(' before isGstRegistered: ', this.isGstRegistered);
      let isGstRegistered = false;

      if (this.partyPopUpForm.get('gstRegistrationTypeId').value === 3) {

        this.isGstRegistered = true;
      }
      else {
        this.isGstRegistered = false;
      }
      //console.log(' afetr isGstRegistered: ', this.isGstRegistered);
    })
  }
  checkOrVerifyCurrency(){
    
    console.log(' i am')
    this.allowCurrency=false;

    if (+this.partyPopUpForm.get('partyDTO.countryId').value === 1) {
      this.allowCurrency = false;
    }
    else {
      this.allowCurrency = true;
    }

  }

  protected getTempTransactionNumber(transactionName) {
    this.transactionTypeService.getTransactionNumber(transactionName)
      .pipe(
        takeUntil(this.OnDestroy)
      )
      .subscribe(response => {
        this.tempTransactionNumber = response.responseString;
        console.log(response.responseString)

      })
  }


}

export interface GSTRegistrationType {
  id: number,
  name: string,
}