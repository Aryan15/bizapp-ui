import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyPopupComponent } from './party-popup.component';

describe('PartyPopupComponent', () => {
  let component: PartyPopupComponent;
  let fixture: ComponentFixture<PartyPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
