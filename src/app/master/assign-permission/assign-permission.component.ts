import { Component, OnInit,  Inject } from '@angular/core';
import { UserRoleMenu, UserRoleWrapper, UserRoleActivity, UserModel } from '../../data-model/user-model';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';
import { AppSettings } from '../../app.settings';
import { Menu, ActivityRoleBinding } from '../../data-model/activity-model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-assign-permission',
  templateUrl: './assign-permission.component.html',
  styleUrls: ['./assign-permission.component.scss']
})
export class AssignPermissionComponent implements OnInit {

  public userRoleForm: FormGroup;
  transactionStatus: string = null;
  saveStatus : boolean =false;
  currentUser : UserModel;
  public menus: Menu[];
  urMenus: UserRoleMenu[] = [];
  spinStart: boolean = true;
  themeClass: any;
  activityRoleBindings: ActivityRoleBinding[];
  enableSave: boolean = true;
  //PASSED_IN_ROLE: number = 1;
  constructor(private fb: FormBuilder,
    private _router: Router,
    // public route: ActivatedRoute,
    private alertService: AlertService,
     private userService: UserService,
     @Inject(MAT_DIALOG_DATA) public data: any,
     private dialogRef: MatDialogRef<AssignPermissionComponent>,) { 
    this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
    this.themeClass = this.currentUser.userThemeName;

    //console.log("current user: ", this.currentUser);
    this.currentUser.roles.forEach(rl => {
      //console.log('role type', rl.roleType.name)
      if(rl.roleType.name === AppSettings.ROLE_TYPE_SYSTEM){
        this.enableSave = false;
      }
    })
  }

  ngOnInit() {
    
    // this.setActivityRoleBinding();

    this.userService.getActivityRoleBindingForRole(this.data.id)
    .subscribe(response => {
      // //console.log("response......................................",response);
      

      this.userService.getAllMenu()
      .subscribe(menus => {
        // //console.log("menus......................................",menus);
          this.menus = menus;

          menus.filter(menu => menu.menuLevel === 1)
          .forEach(menu => {
            // //console.log("menu filter......................................",menu);
            this.urMenus.push(this.mapMenuToRoleBinding(menu, response));            
            
          })
  

          this.initForm();
          this.spinStart = false;
      });
    })
    
  }


  // setActivityRoleBinding(){
  //     this.userService.setActivityRoleBinding(this.route).subscribe(response => {
  //       this.activityRoleBindings = response;

  //       this.activityRoleBindings.forEach(arb => {
  //         if(arb.permissionToCreate === 1 || arb.permissionToUpdate === 1){
  //           this.enableSave = true;
  //         }else{
  //           this.enableSave = false;
  //         }
  //       })
  //   })
  // }

  mapMenuToRoleBinding(menu: Menu, arbList: ActivityRoleBinding[]): UserRoleMenu{

    let urMenu: UserRoleMenu=
    {
      menuId: menu.id,
      isRoleAssigned: 0,
      menuOrder: menu.menuOrder,
      menuName: menu.name,
      activities: []
    };

   
    
    menu.activities
    // .filter(act => act.name != 'Logout')
    // .filter(act => act.name != 'About')
    .forEach(act =>{
      // //console.log("act.............",act);
        let arb: ActivityRoleBinding = arbList.find(arb => arb.activityId === act.id);
        // //console.log("ActivityRoleBinding......................................",arb);

        if(arb){
          urMenu.activities.push({
            arbId: arb.id,
            roleId: arb.roleId,
            isRoleAssigned: 1,
            activityId: act.id,
            activityOrder: act.activityOrder,
            activityName: act.name,
            submenus: act.subMenu ? [this.mapMenuToRoleBinding(act.subMenu, arbList)] : null, 
            permissionToApprove: arb.permissionToApprove,
            permissionToCancel: arb.permissionToCancel,
            permissionToCreate: arb.permissionToCreate,
            permissionToDelete: arb.permissionToDelete,
            permissionToPrint: arb.permissionToPrint,
            permissionToUpdate: arb.permissionToUpdate
          });     
        }else{
          urMenu.activities.push({
            arbId: null,
            roleId: this.data.id,
            isRoleAssigned: 0,
            activityId: act.id,
            activityOrder: act.activityOrder,
            activityName: act.name,
            submenus: act.subMenu ? [this.mapMenuToRoleBinding(act.subMenu, arbList)] : null, 
            permissionToApprove: 0,
            permissionToCancel: 0,
            permissionToCreate: 0,
            permissionToDelete: 0,
            permissionToPrint: 0,
            permissionToUpdate: 0
          });
        }
        
       
      });
      

    

    if(urMenu.activities.find(act => act.isRoleAssigned === 1)){
      urMenu.isRoleAssigned = 1;
    }

    ////console.log('urMenu: ', urMenu);
    return urMenu;


  }
  closeForm() {

    this.dialogRef.close();
  }
  
  initForm(){
    let data: UserRoleWrapper = {
      userRoleMenus: this.urMenus//this.userRoleMenu
    };

    this.userRoleForm = this.toFormGroup(data);
  }

  toFormGroup(data: UserRoleWrapper): FormGroup{
    const itemArr = new FormArray([]);
    data.userRoleMenus.forEach(urm =>{
      itemArr.push(this.toItemGroup(urm))
    })
    
    const formGroup: FormGroup = this.fb.group({
      userRoleMenus: itemArr
    });
    return formGroup;
  }

  toItemGroup(menu: UserRoleMenu): FormGroup{
    const activitiesArr = new FormArray([]);
    if(menu.activities){
      menu.activities.forEach(activity =>{

        const submenuarr = new FormArray([]);
        if(activity.submenus){
          activity.submenus.forEach(act => {
            submenuarr.push(this.toItemGroup(act));
          })
        }
        let actGroup: FormGroup = this.fb.group({
          arbId: activity.arbId,
          roleId: activity.roleId,
          isRoleAssigned: activity.isRoleAssigned,
          activityId: activity.activityId,
          activityOrder: activity.activityOrder,
          activityName: activity.activityName,
          submenus: submenuarr,
          permissionToApprove: activity.permissionToApprove,
          permissionToCancel: activity.permissionToCancel,
          permissionToCreate: activity.permissionToCreate,
          permissionToDelete: activity.permissionToDelete,
          permissionToPrint: activity.permissionToPrint,
          permissionToUpdate: activity.permissionToUpdate
        });

        activitiesArr.push(actGroup);
      });

    }

    const itemGroup: FormGroup =  this.fb.group({
      menuId: menu.menuId,
      isRoleAssigned: menu.isRoleAssigned,
      menuOrder: menu.menuOrder,
      menuName: menu.menuName,
      activities: activitiesArr
    });

    return itemGroup;
  }

  onSubmit(model: UserRoleWrapper){

    //console.log('in save: ', model);

    ////console.log('before save');

    model.userRoleMenus.forEach(userRoleMenus =>{
      userRoleMenus.isRoleAssigned = userRoleMenus.isRoleAssigned ? 1 : 0;
      userRoleMenus.activities.forEach(act => {
        act = this.makeBooleanToNumeric(act);
        if(act.submenus){
          act.submenus.forEach(submenu => {
            submenu.isRoleAssigned = submenu.isRoleAssigned ? 1 : 0;
            submenu.activities.forEach(act => {
              act = this.makeBooleanToNumeric(act);
            })
          })
        }
      })

      });
    //console.log('after processing: ', model);
      this.userService.saveUserRoleMapping(model, this.data.id).subscribe(response => {
        //console.log("Saved: ", response);
        this.transactionStatus =  AppSettings.SAVE_SUCESSFULL_MESSAGE;
        this.alertService.success(this.transactionStatus);
        this.saveStatus = true;
      },
      error => {
        //console.log("Error ", error);
        this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE;
        this.alertService.danger(this.transactionStatus);
       this.saveStatus = false;
      })
  }



  toggleLevelTwo(act: FormGroup){
    //console.log("act.................",act);

    ////console.log(act.parent.parent);//.get('isRoleAssigned').value);
    
    const c1 = <FormArray>act.controls['submenus'];
    //console.log("const c1...........",c1);

    let  currentValue: boolean = act.get('isRoleAssigned').value;
    //console.log('current value level 2: ', currentValue);
    
    let parentValue: boolean = act.parent.parent.get('isRoleAssigned').value;
    if(!parentValue)
      act.parent.parent.get('isRoleAssigned').patchValue(currentValue);
    
      act.get('isRoleAssigned').patchValue(currentValue);
      act.get('permissionToApprove').patchValue(currentValue);
      act.get('permissionToCancel').patchValue(currentValue);
      act.get('permissionToCreate').patchValue(currentValue);
      act.get('permissionToDelete').patchValue(currentValue);
      act.get('permissionToPrint').patchValue(currentValue);
      act.get('permissionToUpdate').patchValue(currentValue);

    c1.controls.forEach(element => {
      //console.log("element.get('activities')..............",element.get('activities'));
      const c2 = <FormArray>element.get('activities');
      
      element.get('isRoleAssigned').patchValue(currentValue);
      c2.controls.forEach(el => {
        
        el.get('isRoleAssigned').patchValue(currentValue);
        el.get('isRoleAssigned').patchValue(currentValue);
        el.get('permissionToApprove').patchValue(currentValue);
        el.get('permissionToCancel').patchValue(currentValue);
        el.get('permissionToCreate').patchValue(currentValue);
        el.get('permissionToDelete').patchValue(currentValue);
        el.get('permissionToPrint').patchValue(currentValue);
        el.get('permissionToUpdate').patchValue(currentValue);
      })
    });
  }


  toggleLevelThree(subAct:FormGroup,  act: FormGroup){

    const c1 = <FormArray>act.controls['submenus'];
    let  currentValue: boolean = subAct.get('isRoleAssigned').value;
    
    let parentValue: boolean = act.get('isRoleAssigned').value;

    if(!parentValue){
      act.get('isRoleAssigned').patchValue(currentValue);
    }

    let parentValueMain: boolean = act.parent.parent.get('isRoleAssigned').value;
    if(!parentValueMain){
      act.parent.parent.get('isRoleAssigned').patchValue(currentValue);
    }

    c1.controls.forEach(element => {
      //console.log("element.get('activities')..............",element.get('activities'));
      const c2 = <FormArray>element.get('activities');

      if(!parentValue){
      element.get('isRoleAssigned').patchValue(currentValue);
      }
        subAct.get('isRoleAssigned').patchValue(currentValue);
        subAct.get('isRoleAssigned').patchValue(currentValue);
        subAct.get('permissionToApprove').patchValue(currentValue);
        subAct.get('permissionToCancel').patchValue(currentValue);
        subAct.get('permissionToCreate').patchValue(currentValue);
        subAct.get('permissionToDelete').patchValue(currentValue);
        subAct.get('permissionToPrint').patchValue(currentValue);
        subAct.get('permissionToUpdate').patchValue(currentValue);
    });
  }

  // ........................................................................

  toggleLevelOne(index: number){

    ////console.log('toggle: ', this.userRoleForm.controls['userRoleMenus']);
    const control = <FormArray>this.userRoleForm.controls['userRoleMenus'];
    ////console.log(control.at(index));

    ////console.log(control.at(index).get('activities'));

    const actControls = <FormArray>control.at(index).get('activities');
    let  currentValue: boolean = control.at(index).get('isRoleAssigned').value;

    //console.log('current value: ', currentValue);
    actControls.controls.forEach(ctr => {
      ////console.log(ctr);
      
      ctr.get('isRoleAssigned').patchValue(currentValue);
      ctr.get('permissionToApprove').patchValue(currentValue);
      ctr.get('permissionToCancel').patchValue(currentValue);
      ctr.get('permissionToCreate').patchValue(currentValue);
      ctr.get('permissionToDelete').patchValue(currentValue);
      ctr.get('permissionToPrint').patchValue(currentValue);
      ctr.get('permissionToUpdate').patchValue(currentValue);

      // do next level if exists
      const controlsLevel2 = <FormArray>ctr.get('submenus');
      ////console.log(controlsLevel2);

      controlsLevel2.controls.forEach(element => {
        ////console.log(element.get('activities'))
        const actControlsLevel2 = <FormArray>element.get('activities');

        element.get('isRoleAssigned').patchValue(currentValue);
        actControlsLevel2.controls.forEach(ctrLevel2 => {
          //let  currentValueL2: boolean = currentValue;//ctrLevel2.get('isRoleAssigned').value;
          ctrLevel2.get('isRoleAssigned').patchValue(currentValue);
          ctrLevel2.get('permissionToApprove').patchValue(currentValue);
          ctrLevel2.get('permissionToCancel').patchValue(currentValue);
          ctrLevel2.get('permissionToCreate').patchValue(currentValue);
          ctrLevel2.get('permissionToDelete').patchValue(currentValue);
          ctrLevel2.get('permissionToPrint').patchValue(currentValue);
          ctrLevel2.get('permissionToUpdate').patchValue(currentValue);
        })
      });
      //const actControlsLevel2 = <FormArray>controlsLevel2['activities'];
      ////console.log(actControlsLevel2);

    })

  }

  makeBooleanToNumeric(act: UserRoleActivity): UserRoleActivity{
    act.isRoleAssigned = act.isRoleAssigned ? 1 : 0;
    act.permissionToApprove = act.permissionToApprove ? 1 : 0;
    act.permissionToCancel = act.permissionToCancel ? 1 : 0;
    act.permissionToCreate = act.permissionToCreate ? 1 : 0;
    act.permissionToDelete = act.permissionToDelete ? 1 : 0;
    act.permissionToPrint = act.permissionToPrint ? 1 : 0;
    act.permissionToUpdate = act.permissionToUpdate ? 1 : 0;
    return act;
  }
  

}
