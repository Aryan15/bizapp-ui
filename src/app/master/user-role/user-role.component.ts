import { Component, OnInit, ViewChild } from '@angular/core';
import { Role, RoleWrapper, RoleType, UserModel } from '../../data-model/user-model';
import { UserService } from '../../services/user.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogConfig, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AssignPermissionComponent } from '../assign-permission/assign-permission.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../../app.settings';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertService } from 'ngx-alerts';
import { Router } from '@angular/router';
import { LanguageService } from '../../services/language.service';
import { TranslateService } from '@ngx-translate/core';
//import { Role } from '../../data-model/settings-wrapper';

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.scss']
})
export class UserRoleComponent implements OnInit {

  @ViewChild(DatatableComponent, {static: true}) table: DatatableComponent;
  roleTypes: RoleType[] = [];
  roleForm: FormGroup;
  roleWrapper: RoleWrapper;
  roles: Role[] = [];
  selected = [];
  pageSize: number = AppSettings.REPORT_PAGE_SIZE;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  tempArray: Role[]
  currentUser : UserModel;
  enableSave: boolean = true;
  constructor(private userService: UserService,
    private fb: FormBuilder,
    protected dialog: MatDialog,
    private alertService: AlertService, 
    private languageService: LanguageService,
    private translate: TranslateService,
    private _router: Router,) {
      this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
      this.currentUser.roles.forEach(rl => {
        //console.log('role type', rl.roleType.name)
        if(rl.roleType.name === AppSettings.ROLE_TYPE_SYSTEM){
          this.enableSave = false;
        }
      })
      translate.setDefaultLang('en');
     }

  ngOnInit() {
    this.applyLanguage();
    this.getAllRoleTypes();
    this.getAllRoles();
    this.initForm();
  }

  getAllRoleTypes() {
    this.userService.getAllRoleTypes().subscribe(response => {
      this.roleTypes = response;
    })
  }
  applyLanguage() {
    this.languageService.getLanguage()
    .subscribe(resp => {
    if(resp===null){
      this.translate.use("en");
    }
    else{
      this.translate.use(resp);
    }
   
  })
  }


  getAllRoles() {
    this.userService.getAllRoles().subscribe(response => {
      this.roles = response;
      //console.log('got roles: ', this.roles);
      this.roleForm = this.toFormGroup({ roles: this.roles });
    })
  }

  openPermissionDialog(id: number) {
    //console.log('open dialog: ', id);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: id
    };

    dialogConfig.height = '600px';
    dialogConfig.width = '1000px';


    const dialogRef = this.dialog.open(AssignPermissionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        //console.log('dialog closed: ', data)
      });

  }



  initForm() {

    let data: RoleWrapper = {
      roles: null
    }


    this.roleForm = this.toFormGroup(data);
  }

  toFormGroup(data: RoleWrapper): FormGroup {

    const roleArray = new FormArray([]);
    if (data.roles) {
      data.roles.forEach(role => {
        roleArray.push(
        //   this.fb.group({
        //   id: role.id,
        //   role: role.role,
        //   roleTypeId: role.roleTypeId,
        //   roleTypeName: role.roleTypeName
        // })
        this.makeItem(role)
        )
      })
    }

    const formGroup = this.fb.group({
      roles: roleArray
    });

    return formGroup;

  }

  makeItem(role: Role): FormGroup {
    return this.fb.group({
      id: role.id,
      role: role.role,
      roleTypeId: role.roleTypeId,
      roleTypeName: role.roleTypeName
    })
  }

  onSelect({ selected }) {
    //console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  displayCheck(row) {
    return row.roleTypeName !== 'ADMIN';
  }


  addNew() {
    const control = <FormArray>this.roleForm.controls['roles'];
    //Add one blank Item
    let item: Role = {
      id: null,
      role: null,
      roleTypeId: null,
      roleTypeName: null
    };

    control.push(this.makeItem(item));
    // Whenever the filter changes, always go back to the first page
    this.table.offset = Math.round(control.length/this.pageSize);
  }

  onSubmit(model: RoleWrapper) {
    //console.log('before save: ', model);
    this.userService.createRole(model.roles).subscribe(response => {
      //console.log("saved response: ", response);
      if (response) {
        this.roleForm = this.toFormGroup({ roles: response });
        this.selected = [];
        this.tempArray = [...response];
        this.alertService.success(AppSettings.SAVE_SUCESSFULL_MESSAGE);
      }
    });
  }



  delete() {
    //console.log(this.selected);

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        let items: Role[] = this.roleForm.get('roles').value;

        //console.log("1 ", items);
        let itemIdList: number[] = [];

        this.selected.forEach(s => {
          if (s.id) {
            itemIdList.push(s.id)
          }
        })

        //console.log("2: ", itemIdList);

        if (itemIdList.length > 0) {


          this.userService.deleteRole(itemIdList).subscribe(response => {

            //console.log("3: ", response);
            if (response.responseStatus === 1) {
              // items.forEach((item, idx) => {

              //   if (this.selected.find(s => s.id === item.id)) {
              //     (<FormArray>this.stateForm.get('states')).removeAt(idx);
              //         this.tempArray.splice(idx,1);

              //   }

              // })
              //console.log("selected: "+this.selected);
              let tempItems = items.filter(i => this.selected.filter(s => s.id === i.id).length === 0);
              //console.log("4: ", tempItems)
              this.tempArray = [...tempItems];
              this.roleForm = this.toFormGroup({ roles: tempItems });
              this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
            } else {
              //console.log("5")
              this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            }

            this.selected = [];

          }, error => {
            //console.log("6");
            this.alertService.danger(AppSettings.DELETE_FAILED_MESSAGE);
            this.selected = [];
          })
        }else{
          // items.forEach((item, idx) => {
          //   if(!item.id)
          //     (<FormArray>this.stateForm.get('states')).removeAt(idx)
          // })
          items = items.filter(i => (i.id != null &&!(this.selected.find(s => s.role === i.role))));
          //console.log("7: ",items);
          this.tempArray = [...items];
          this.roleForm = this.toFormGroup({ roles: items });
          this.alertService.success(AppSettings.DELETE_SUCESSFULL_MESSAGE);
          this.selected = [];
        }


        

      }

    });



  }

  close()
  {
    this._router.navigate(['/'])
  }



}
