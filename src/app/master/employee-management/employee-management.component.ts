import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserModel, Role } from '../../data-model/user-model';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { LocationService } from '../../services/location.service';
import { State } from '../../data-model/state-model';
import { Country } from '../../data-model/country-model';
import { Area } from '../../data-model/area-model';
import { City } from '../../data-model/city-model';
import { ConfirmationDialog } from '../../utils/confirmation-dialog';
import { AlertDialog } from '../../utils/alert-dialog';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivityRoleBinding } from '../../data-model/activity-model';
import { ApplicableButtons, NamePrefix, Gender } from '../../data-model/misc-model';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AppSettings } from '../../app.settings';
import { AlertService } from 'ngx-alerts';
import { UtilityService } from '../../utils/utility-service';
import { PasswordPopupComponent } from '../password-popup/password-popup.component';
import { HttpErrorResponse } from '@angular/common/http';
//import { EqualTextValidator } from 'angular2-text-equality-validator';
@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.component.html',
  styleUrls: ['./employee-management.component.scss']
})
export class EmployeeManagementComponent implements OnInit {

  public employeeManagementForm: FormGroup;
  public userModel: UserModel;
  dirty: boolean;
  recentEmployeesLoadCounter: number = 0;
  transactionStatus: string = null;
  saveStatus: boolean = false;
  spinStart: boolean = false;
  // isLogIn: boolean = false;
  isPasswordVisible: boolean = false;
  confirmPassword = new FormControl('');
  errorMessageForCP: string = '';
  errorMessage: string;
  datePipe = new DatePipe('en-US');
  displayName = "";
  maxDate: Date;
  countries: Country[] = [];
  states: State[] = [];
  cities: City[] = [];
  roles: Role[] = [];
  areas: Area[] = [];
  allStates: State[] = [];
  allCities: City[] = [];
  fieldValidatorLabels = new Map<string, string>();
  allAreas: Area[] = [];
  namePrefixes: NamePrefix[] = [
    { id: 1, name: 'Mr' },
    { id: 2, name: 'Mrs' },
    { id: 3, name: 'Ms' },
    { id: 4, name: 'Dr' },
  ];

  genders: Gender[] = [
    { id: 1, name: 'Male' },
    { id: 2, name: 'Female' }
  ]

  hide: boolean = true;

  dialogRef: MatDialogRef<ConfirmationDialog>;
  alertRef: MatDialogRef<AlertDialog>;

  applicableButtons: ApplicableButtons = {
    isApproveButton: false,
    isClearButton: true,
    isCloseButton: true,
    isCancelButton: false,
    isDeleteButton: true,
    isPrintButton: false,
    isSaveButton: true,
    isEditButton: true,
    isDraftButton: false,
  };

  eTypes: any[]=[
    { id: 1, name: "Permanent"},
    { id: 2, name: "Temporary"},
    { id: 3, name: "Part Time"},
  ]

  activityRoleBindings: ActivityRoleBinding[];
  showLoginCredential: boolean = false;
  showDisableLogin: boolean = false;
  currentUser: any;
  constructor(private fb: FormBuilder,
    private _router: Router,
    public route: ActivatedRoute,
    private userService: UserService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private utilityService: UtilityService,
    private locationService: LocationService, ) {
      this.currentUser = JSON.parse(localStorage.getItem(AppSettings.CURRENT_USER)).user;
      this.setActivityRoleBinding();
  }
  setActivityRoleBinding() {
    this.userService.setActivityRoleBinding(this.route).subscribe(response => {
      //console.log('activityRoleBindings: ', response);
      this.activityRoleBindings = response;
    })
  }
  ngOnInit() {

    this.getAllAreas();
    this.getAllCities();
    this.getAllStates();
    this.getRoles();
    this.initForm();

    this.employeeManagementForm.controls['name'].valueChanges.subscribe(displayName => {
      displayName = this.employeeManagementForm.controls['name'].value;
      // //console.log("displayName",displayName)
      this.employeeManagementForm.controls['displayName'].patchValue(displayName);
    });


    this.maxDate = new Date();
    //console.log(" this.maxDate ", this.maxDate)
  }


  getCities() {
    let areaId = this.employeeManagementForm.get('areaId').value;
    //console.log("areaId", areaId)
    let area = this.areas.find(p => p.id === areaId);

    //console.log("city id in area", area.cityId)
    //console.log("this.cities for area", this.cities.find(p => p.id === area.cityId))
    let value = this.cities.find(p => p.id === area.cityId);
    //console.log("value", value.name)
    this.employeeManagementForm.get('cityId').patchValue(value.id);
    this.getStates();
  }
  getStates() {
    let cityId = this.employeeManagementForm.get('cityId').value;
    //console.log("areaId", cityId)
    let city = this.cities.find(p => p.id === cityId);

    //console.log("city id in area", city.stateId)
    //console.log("this.cities for area", this.states.find(p => p.id === city.stateId))
    let value = this.states.find(p => p.id === city.stateId);
    //console.log("value", value.name)
    this.employeeManagementForm.get('stateId').patchValue(value.id);
    //  this.getCountries();
  }

  getCountries() {
    let stateId = this.employeeManagementForm.get('stateId').value;
    let state = this.states.find(p => p.id === stateId);

    //console.log("city id in area", state.countryId)
    //console.log("this.cities for area", this.countries.find(p => p.id === state.countryId))
    let value = this.countries.find(p => p.id === state.countryId);
    //console.log("value", value.name)
    this.employeeManagementForm.get('countryId').patchValue(value.id);

  }

  private getRoles() {
    this.userService.getAllRoles()
      .subscribe(response => {
        this.roles = response;


        const control = <FormArray>this.employeeManagementForm.controls['roles'];
        this.roles.forEach(item => {
          // control.push(this.mapItem(item));
        });
      })


  }

  private getAllCities() {

    this.locationService.getCities()
      .subscribe(response => {
        this.cities = response;
        this.allCities = response;
      })
  }

  private getAllAreas() {

    this.locationService.getAreas()
      .subscribe(response => {
        this.areas = response;
        this.allAreas = response;
      })
  }
  private getAllStates() {

    this.locationService.getStates()
      .subscribe(response => {
        this.states = response;
        this.allStates = response;
      })
  }



  private initForm() {
    const data: UserModel = {
      id: null,
      username: null,
      email: null,
      password: null,
      name: null,
      lastName: null,
      active: null,
      roleIds: null,//[{ id: 1, role: 'Admin', roleType: { id: 1, name: 'ADMIN', deleted: 'N'} }],
      currentAddress: null,
      permanentAddress: null,
      prefixId: null,
      countryId: null,
      stateId: null,
      cityId: null,
      areaId: null,
      genderId: null,
      pinCode: null,
      employeeNumber: null,
      telephoneNumber: null,
      mobileNumber: null,
      dateOfBirth: null,
      dateOfJoin: null,
      dateOfLeaving: null,
      comments: null,
      employeeStatusId: null,
      panNumber: null,
      displayName: null,
      companyId: null,
      employeeTypeId: null,
      employeeId: null,
      isLogInRequired: null,
      deleted: null,
      userThemeName: null,
      roles: null,
      userLanguage: null,
    };
    this.employeeManagementForm = this.toFormGroup(data);
    // this.getPasswordEnable();
  }

  private toFormGroup(data: UserModel): FormGroup {

    const formGroup = this.fb.group({
      id: [data.id],
      username: [data.username],
      name: [data.name, Validators.required],
      prefixId: [data.prefixId, Validators.required],
      email: [data.email],
      password: [data.password],
      lastName: [data.lastName],
      active: [data.active],
      roleIds: [data.roleIds, Validators.required],
      currentAddress: [data.currentAddress],
      permanentAddress: [data.permanentAddress, Validators.required],
      countryId: [data.countryId],
      stateId: [data.stateId],
      cityId: [data.cityId],
      areaId: [data.areaId],
      genderId: [data.genderId, Validators.required],
      pinCode: [data.pinCode],
      employeeNumber: [data.employeeNumber, Validators.required],
      telephoneNumber: [data.telephoneNumber],
      mobileNumber: [data.mobileNumber, Validators.pattern('^[0-9\-\+]{9,15}$')],
      dateOfBirth: [this.datePipe.transform(data.dateOfBirth, AppSettings.DATE_FORMAT)],
      dateOfJoin: [this.datePipe.transform(data.dateOfJoin, AppSettings.DATE_FORMAT)],
      dateOfLeaving: [this.datePipe.transform(data.dateOfLeaving, AppSettings.DATE_FORMAT)],
      comments: [data.comments],
      employeeStatusId: [data.employeeStatusId],
      panNumber: [data.panNumber],
      displayName: [data.displayName],
      companyId: [data.companyId],
      employeeTypeId: [data.employeeTypeId, Validators.required],
      employeeId: [data.employeeId],
      isLogInRequired: [data.isLogInRequired],
      deleted: [data.deleted],
      userThemeName: [data.userThemeName],
      userLanguage:[data.userLanguage],
    });

    this.fieldValidatorLabels.set("employeeTypeId", "Employ type");
    this.fieldValidatorLabels.set("name", "Name");
    this.fieldValidatorLabels.set("permanentAddress", "Address");
    this.fieldValidatorLabels.set("stateId", "State");
    this.fieldValidatorLabels.set("roleIds", "Role");
    // this.fieldValidatorLabels.set("email", "E-mail");
    this.fieldValidatorLabels.set("genderId", "Gender Type");
    this.fieldValidatorLabels.set("prefixId", "Prefix");
    this.fieldValidatorLabels.set("username", "Username");
    this.fieldValidatorLabels.set("displayName", "Display Name");
    this.fieldValidatorLabels.set("password", "Password");
    this.fieldValidatorLabels.set("employeeNumber", "Employee Code");

    return formGroup;

  }


  closeForm() {
    this._router.navigate(['/']);
  }
  clearForm() {

    this.initForm();

    this.saveStatus = false;
    this.spinStart = false;
    this.transactionStatus = null;
    this.confirmPassword.patchValue("");
    this.employeeManagementForm.markAsPristine();
    this.showDisableLogin = false;
    this.showLoginCredential = false;
  }


  findInvalidControls() {
    const invalid = [];
    const controls = this.employeeManagementForm.controls;
    //console.log("controls " + controls)
    for (const name in controls) {
      //console.log("name " + controls[name].value)
      if (controls[name].invalid) {
        //console.log("name of cntr " + controls[name])
        invalid.push(name);
        //console.log("invalid " + invalid);
      }
    }
    return invalid;
  }
  submit(model: UserModel) {
    this.spinStart = true;
    if (!this.employeeManagementForm.valid || this.roles.length < 0) {
      const controls = this.employeeManagementForm;
      let invalidFieldList: string[] = this.utilityService.findInvalidControlsRecursive(controls);
      //console.log("invalidFieldList ", invalidFieldList)

      let invalidFiledLabels: string[] = [];

      invalidFieldList.forEach(field => {
        if (this.fieldValidatorLabels.get(field))
          invalidFiledLabels.push(this.fieldValidatorLabels.get(field));
      })

      //console.log("invalidFiledLabels ", invalidFiledLabels)

      this.alertService.danger(AppSettings.MANDOTARY_ERROR_MESSAGE + invalidFiledLabels);
      this.spinStart = false;
      return false;

    }
    
    model.companyId = 1; //hack
    //console.log('Before save: ', model);
    let message: string;
    if (model.id === null) {
      message = AppSettings.SAVE_SUCESSFULL_MESSAGE;
    }
    else {
      message = AppSettings.UPDATE_SUCESSFULL_MESSAGE;
    }

   

    this.userService.save(model)
      .subscribe(response => {
        this.saveStatus = true;
        
        //console.log('save() : ', this.employeeManagementForm.disabled);
        this.transactionStatus = "User " + response.name + message;
        this.recentEmployeesLoadCounter++;
        this.showLoginCredential = true;
        this.showDisableLogin = (response.isLogInRequired === 1 && this.currentUser.id !== this.userModel.id) ? true : false;                
        this.alertService.success(this.transactionStatus);
        this.userModel = response;
        this.employeeManagementForm = this.toFormGroup(this.userModel);
        this.disableForm();
        this.spinStart = false;
      }
        ,
        error => {
          //console.log("Error ", error);
          if(error instanceof HttpErrorResponse){ 
            
            var re = "ConstraintViolationException";
            if(error.error && error.error.message.includes(re)){
                this.transactionStatus = "Duplicate Entry!! Cannot save";
            }
        }
          // this.transactionStatus = AppSettings.SAVE_FAILED_MESSAGE

          this.saveStatus = false;
          this.spinStart = false;
          this.alertService.danger(this.transactionStatus);
        });


  }

  disableForm() {
    this.employeeManagementForm.disable({ onlySelf: true, emitEvent: false });
    //console.log("this.materialForm.dirty == false", this.employeeManagementForm.dirty == false)
    this.employeeManagementForm.markAsPristine;
    //console.log("this.materialForm.markAsPristine == false", this.employeeManagementForm.markAsPristine)
  }

  enableForm() {
    this.employeeManagementForm.enable({ onlySelf: true, emitEvent: false });

  }

  delete() {

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = AppSettings.DELETE_CONFIRMATION_MESSAGE;

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.spinStart = true;
        let id: string = this.employeeManagementForm.controls['id'].value;//this.quotationHeader.id;
        let name: string = this.employeeManagementForm.controls['name'].value;
        //console.log("deleteing..." + id);
        this.userService.delete(id)
          .subscribe(response => {
            if (response.responseStatus === 1) {
              this.saveStatus = true;

              this.recentEmployeesLoadCounter++;
              this.spinStart = false;
              this.transactionStatus = AppSettings.DELETE_SUCESSFULL_MESSAGE;
              this.alertService.success(this.transactionStatus);
              this.initForm();

            }
            else {
              //console.log("Error ", response);
              this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE;

              this.saveStatus = false;
              this.spinStart = false;
              this.alertService.danger(this.transactionStatus);
              if (response.responseString.search('ConstraintViolationException') > 0) {
                this.transactionStatus = "Cannot delete. Party " + name + " is already used in transactions"
              }
            }

          },
            error => {
              //console.log("Error ", error);
              this.transactionStatus = AppSettings.DELETE_FAILED_MESSAGE;
              this.alertService.danger(this.transactionStatus);
              this.saveStatus = false;
              this.spinStart = false;
            });
      }
      this.dialogRef = null;
    });
  }


  changePassword() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(PasswordPopupComponent, {
      data: this.userModel
    });


    dialogRef.afterClosed().subscribe(
      data => {
          if (data) {

            //console.log(data);
            this.employeeManagementForm.get('email').patchValue(data.email)
            this.employeeManagementForm.get('username').patchValue(data.username)
            this.employeeManagementForm.get('password').patchValue(data.username)

          }
        }
    );

  }


  edit() {
    this.enableForm();
  }



  recentEmployeesGet(event) {
    //console.log('In recentEmployessGet', event);
    this.userModel = event;
    this.employeeManagementForm = this.toFormGroup(this.userModel);
    this.saveStatus = false;
    this.spinStart = false;
    this.transactionStatus = null;

    this.confirmPassword.patchValue(this.userModel.password);
    this.showLoginCredential = true;
    this.showDisableLogin = (this.userModel.isLogInRequired === 1 && this.currentUser.id !== this.userModel.id) ? true : false;
  
    this.disableForm();

  }
  disableEnableForm() {

    if (this.employeeManagementForm.disabled) {
      this.employeeManagementForm.enable();
      //     this.disableParty = false;

    } else {

      this.employeeManagementForm.disable();
      //  this.disableParty = true;

    }


  }

  
  
  whenNoDataFound(noDataFound: boolean) {
    if (noDataFound) {
      this.alertRef = this.dialog.open(AlertDialog, {
        disableClose: false
      });

      this.alertRef.componentInstance.alertMessage = AppSettings.NO_DATA_FOUND_MESSAGE;
    }
  }
 



  disableLogin() {
    //console.log("Calling disable login service");

    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    this.dialogRef.componentInstance.confirmMessage = "Do you want to disable login of "+this.userModel.name+" ?";

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.userService.disableLogin(this.userModel)
          .subscribe(response => {
            if (response) {
              this.alertService.success("Login disabled for " + this.userModel.name);
              this.showDisableLogin = false;
            } else {
              this.alertService.danger("Failed to disable login for " + this.userModel.name);
            }
          })
      }
    }
    );
  }
}
