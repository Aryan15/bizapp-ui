import { Component, Input, OnChanges, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
//import {HttpClient} from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable ,  merge ,  of as observableOf } from 'rxjs';
import { catchError ,  map ,  debounceTime ,  startWith ,  switchMap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { UserModel, RecentUser } from '../../data-model/user-model';
import { UserService } from '../../services/user.service';

const PAGE_SIZE: number = 10;

@Component({
  selector: 'recent-employ-table',
  templateUrl: './recent-employ-table.component.html',
  styleUrls: ['./recent-employ-table.component.scss']
})
export class RecentEmployeeMasterTableComponent implements OnInit, OnChanges {

  @Input()
  recentEmployeesLoadCounter: number;

  @Output()
  UserModel: EventEmitter<UserModel> = new EventEmitter<UserModel>();

  @Output()
  noDataFound: EventEmitter<boolean> = new EventEmitter<boolean>();


  displayedColumns = ['name', 'employeeNumber'];
  employeeMasterDatabase: UserMasterHttpDao | null;

  dataSource = new MatTableDataSource<UserModel>();

  filterForm = new FormGroup({
    filterText: new FormControl()
  });

  resultsLength = 0;
  isLoadingResults = true;
  //isRateLimitReached = false;

  pageSize: number = PAGE_SIZE;
  filterTextObservable: Observable<string>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private userService: UserService) { }


  ngOnInit() {


    this.filterTextObservable = this.filterForm.get('filterText').valueChanges.pipe(debounceTime(1000));

    this.employeeMasterDatabase = new UserMasterHttpDao(this.userService);

    //If the user changes sort order, reset back to first page

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.filterTextObservable)
      .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.employeeMasterDatabase!.getUsers(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        //Flip flag to show that loading has finished
        this.isLoadingResults = false;
        this.resultsLength = data.totalCount;
        //console.log('data.user: ', data.users);
        //console.log('data.totalCount: ', data.totalCount);
        if (this.filterForm.get('filterText').value != null) {
          if (data.totalCount === 0) {
            this.noDataFound.emit(true);
          }
        }
        return data.users;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        //console.log('catchError error');
        return observableOf([]);
      })
      ).subscribe(data => this.dataSource.data = data);

  }

  ngOnChanges() {
    //this.onChangeEvent.
    // //console.log('this.recentInvoiceLoadCounterObservable: ',this.recentInvoiceLoadCounterObservable);
    // this.recentInvoiceLoadCounterObservable = Observable.of(this.recentInvoiceLoadCounter);
    //console.log('in onchanage: ', this.recentEmployeesLoadCounter);

    if (this.recentEmployeesLoadCounter > 0) {
      this.employeeMasterDatabase = new UserMasterHttpDao(this.userService);

      this.employeeMasterDatabase!.getUsers(this.filterForm.get('filterText').value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize).pipe(
        map(data => {
          this.resultsLength = data.totalCount;
          //console.log('data.materialList: ', data.users);
          //console.log('data.totalCount: ', data.totalCount);
          if (this.filterForm.get('filterText').value != null) {
            if (data.totalCount === 0) {
              this.noDataFound.emit(true);
            }
          }
          return data.users;
        })).subscribe(data => this.dataSource.data = data);
    }
  }
  onRowClick(row: any) {
    //console.log('row clicked: ', row);
    this.UserModel.emit(row);
    //console.log('done');
  }

}


export class UserMasterHttpDao {

  constructor(private userService: UserService) { }

  getUsers(filterText: string, sortColumn: string, sortDirection: string, page: number, pageSize: number): Observable<RecentUser> {

    pageSize = pageSize ? pageSize : PAGE_SIZE;
    //console.log('filterText: ', filterText);


    // this.materialService.getAllMaterialsForReport(sortColumn, sortDirection, page, pageSize).subscribe(response => {
    //   ////console.log("response: ", response);
    // })

    return this.userService.getRecentUser(filterText, sortColumn, sortDirection, page, pageSize);
  }
}